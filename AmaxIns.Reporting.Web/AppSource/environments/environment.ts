// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
//baseApiUrl:"http://18.216.112.189:8082/api/  http://localhost:44305/api/   http://18.216.112.189:8085/api/"

export const environment = {
  production: false,
  baseApiUrl: "http://18.216.112.189:8082/api/"
 };
