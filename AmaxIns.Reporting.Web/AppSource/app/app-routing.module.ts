import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HourlyLoadComponent } from './Modules/HourlyLoad/hourlyLoad.component';

const routes: Routes = [
  { path: '', component: HourlyLoadComponent },
  { path: 'NewModifiedQuotes', loadChildren: "app/Modules/NewOldQuotes/newOldQuotes.module#NewOldQuotesModule"}
 ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
