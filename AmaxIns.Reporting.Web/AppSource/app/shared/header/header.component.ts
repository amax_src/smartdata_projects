import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() PageName: string;
  userName: string = "";
  display: boolean = false;
  ResetPasswordForm: FormGroup;
  isValidPassword = true;
  constructor( public cmnSrv: CommonService,
    private messageService: MessageService,private router: Router) {
  }

  ngOnInit() {
    this.ResetPasswordForm = new FormGroup({
      password: new FormControl("", [
        Validators.required,
      ]),
      newPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(8)
      ]),
      repeatPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  onSubmit() {
    
  }

  Logout() {
   
  }

  ResetPassword() {
    this.display = true;
  }

  ValidateCurrentPassword() {
    
  }
}

