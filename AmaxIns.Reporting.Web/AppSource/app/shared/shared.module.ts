import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { NavmenuComponent } from './navmenu/navmenu.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonService } from '../core/common.service';
import { RmZmLocationDropdownComponent } from './rm-zm-location-dropdown/rm-zm-location-dropdown.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog'
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast'
import { MessageService } from 'primeng/api';


@NgModule({
  declarations: [HeaderComponent, NavmenuComponent, RmZmLocationDropdownComponent],
  exports: [HeaderComponent, NavmenuComponent, RmZmLocationDropdownComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MultiSelectModule,
    FormsModule,
    DialogModule,
    DropdownModule,
    InputTextModule,
    ReactiveFormsModule,
    ToastModule
  ],
  providers: [CommonService, MessageService]
})
export class SharedModule { }
