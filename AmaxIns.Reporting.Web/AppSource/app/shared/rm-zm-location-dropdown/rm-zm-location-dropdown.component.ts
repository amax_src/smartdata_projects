import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RegionalManager } from '../..//model/RegionalManager';
import { SelectItem } from 'primeng/api';
import { ZonalManager } from '../../model/ZonalManager';
import { Location } from '../../model/Location'
import { UserService } from '../../Modules/Services/UserService';
import { AgencyService } from '../../Modules/Services/agencyService';

@Component({
  selector: 'app-rm-zm-location-dropdown',
  templateUrl: './rm-zm-location-dropdown.component.html',
  styleUrls: ['./rm-zm-location-dropdown.component.css']
})
export class RmZmLocationDropdownComponent implements OnInit {

  @Output() RmChangeEmitter = new EventEmitter();
  @Output() ZmChangeEmitter = new EventEmitter();
  @Output() LocationChangeEmitter = new EventEmitter();
  @Output() AllLocationEmitter = new EventEmitter();
  @Output() singleLocationEmitter = new EventEmitter();


  @Output() ShowLoaderEmitter = new EventEmitter();

  showloader: boolean = false;
  regionalManager: Array<RegionalManager>;
  regionalManagerSelectItem: SelectItem[] = [];
  selectedRegionalManager: Array<number> = [];
  @Input() SinglelocationSelected: number;
  zonalManager: Array<ZonalManager>;
  zonalManagerSelectItem: SelectItem[] = [];
  selectedzonalManager: Array<number> = [];

  location: Array<Location>;
  locationSelectItem: SelectItem[] = [];
  @Input() selectedlocation: Array<number> = [];
  @Input() ShowLocationDropDown: boolean = false;
  @Input() ShowLocationMultiMaxFiveSelect: boolean = false;
  constructor(private userService: UserService, private agencyService: AgencyService) {
    this.regionalManager = new Array<RegionalManager>();
    this.zonalManager = new Array<ZonalManager>();
    this.location = new Array<Location>();
    this.selectedRegionalManager = new Array<number>();

    this.getAllRegionalManagers();
    this.getAllZonalManagers();
    this.getAllLocations();




  }

  ngOnInit() {
  }

  onLocationchange(e) {
    this.LocationChangeEmitter.emit(this.selectedlocation);
  }

  onZonalManagerchange(e) {
    this.location = new Array<Location>();
    this.selectedlocation = new Array<number>();
    this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedzonalManager);
    this.ZmChangeEmitter.emit(this.selectedzonalManager);
  }

  onRegionalManagerchange(e) {
    this.selectedzonalManager = new Array<number>();
    this.location = new Array<Location>();
    if (this.selectedRegionalManager.length > 0) {
      this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
    }
    else {
      this.getAllZonalManagers();
    }
    this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedzonalManager);
    this.RmChangeEmitter.emit(this.selectedRegionalManager);
  }

  onLocationDropDownchange() {
    this.singleLocationEmitter.emit(this.SinglelocationSelected);
  }

  getAllRegionalManagers() {
    this.userService.getAllRegionalManagers().subscribe(m => {
      this.regionalManager = m;
      this.regionalManager.forEach(i => {
        this.regionalManagerSelectItem.push(
          {
            label: i.regionalManager, value: i.regionalManagerId
          })
      })
    });
  }

  getAllZonalManagers() {
    this.ShowLoaderEmitter.emit(true);
    this.zonalManagerSelectItem = [];
    this.userService.getAllZonalManagers().subscribe(m => {
      this.zonalManager = m;
      this.zonalManager.forEach(i => {
        this.zonalManagerSelectItem.push(
          {
            label: i.zonalManager, value: i.zonalManagerId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocations() {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
    this.agencyService.getAllLocations().subscribe(m => {
      this.location = m;
      this.AllLocationEmitter.emit(m);
      this.location.forEach(i => {
        this.locationSelectItem.push(
          {
            label: i.agencyName, value: i.agencyId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocationsRegionalManager(ids: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
    this.agencyService.getAllLocationsRegionalManager(ids).subscribe(m => {
      this.location = m;
      this.AllLocationEmitter.emit(m);
      this.location.forEach(i => {
        this.locationSelectItem.push(
          {
            label: i.agencyName, value: i.agencyId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocationsRegionalZonalManager(Rids: Array<number>, Zids: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
    this.selectedlocation = [];
    if ((Zids && Zids.length > 0) && (Rids && Rids.length > 0)) {
      this.agencyService.getAllLocationsRegionalZonalManager(Rids, Zids).subscribe(m => {
        this.location = m;
        this.AllLocationEmitter.emit(m);
        this.location.forEach(i => {
          this.locationSelectItem.push(
            {
              label: i.agencyName, value: i.agencyId
            })
        })
        this.ShowLoaderEmitter.emit(false);
      });
    }
    else if ((Rids && Rids.length > 0)) {
      this.getAllLocationsRegionalManager(Rids)
    }
    else if ((Zids && Zids.length > 0)) {
      this.agencyService.getAllLocationsRegionalZonalManager(Rids, Zids).subscribe(m => {
        this.location = m;
        this.AllLocationEmitter.emit(m);
        this.location.forEach(i => {
          this.locationSelectItem.push(
            {
              label: i.agencyName, value: i.agencyId
            })
        })
        this.ShowLoaderEmitter.emit(false);
      });
    }
    else {
      this.getAllLocations();
    }
  }


  getZonalManagerByRegionalManagers(selectedManager: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.zonalManagerSelectItem = [];
    this.userService.getZonalManagersByRegionalManager(selectedManager).subscribe(m => {
      this.zonalManager = m;
      this.zonalManager.forEach(i => {
        this.zonalManagerSelectItem.push(
          {
            label: i.zonalManager, value: i.zonalManagerId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }
}
