import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { debug } from 'util';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements OnInit {
  HourlyLoadclass = 'active';
  NewModifiedQuotesclass = '';
  

  constructor(public cmnSrv: CommonService, private router: Router
    ) { }

  ngOnInit() {
    this.setSelectedMenu(this.router.url.replace('/', ''));
  }

  Navigate(e) {
    this.HourlyLoadclass = '';
    this.NewModifiedQuotesclass = '';
    if (e === 'HourlyLoad') {
      this.HourlyLoadclass = "active";
      this.router.navigateByUrl('/')
    }
    else if (e === 'NewModifiedQuotes') {
      this.NewModifiedQuotesclass = "active";
      this.router.navigateByUrl('/NewModifiedQuotes')
    }
   
  }

  setSelectedMenu(e) {
    this.HourlyLoadclass = '';
    this.NewModifiedQuotesclass = '';
    if (e === '') {
      this.HourlyLoadclass = "active";
    }
    else if (e === 'NewModifiedQuotes') {
      this.NewModifiedQuotesclass = "active";
    }
  }
}
