import { Component } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  deviceInfo = null;
  title = 'app';
  showHeader = false;

  constructor(private deviceService: DeviceDetectorService) {
    this.epicFunction();
  }

  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    if (this.deviceInfo.browser === 'IE') {
      this.showHeader = true;
      alert('You are using Internet Explorer, For best experince kindly use Google Chrome or Firefox latest version.')
    }
    
  }
  
}
