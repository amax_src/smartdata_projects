import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { HourlyLoadComponent } from './hourlyLoad.component';


const routes: Routes = [
  { path: 'Hourlyload', component: HourlyLoadComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HourlyLoadRoutingModule { }
