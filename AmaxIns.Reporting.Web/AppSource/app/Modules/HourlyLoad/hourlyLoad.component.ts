import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import * as _ from 'underscore';
import { HourlyLoadService } from '../Services/hourlyLoadService';
import { MonthModel } from '../../model/MonthModel'
import { HourlyLoadModel } from '../../model/HourlyLoadModel'
import { HourlyNewModifiedQuotes } from '../../model/hourlyNewModifiedQuotesModel';
import { MergedQuotePolicyModel } from '../../model/MergedQuotePolicyModel';
import { PageService } from '../Services/pageService';
import { PeakHoursModel } from '../../model/PeakHoursModel';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-hourlyLoad',
  templateUrl: './hourlyLoad.component.html',
    styleUrls: ['./hourlyLoad.component.scss'],
    providers: [DatePipe]
})

export class HourlyLoadComponent implements OnInit, AfterViewChecked {

  singleLocationSelected: boolean = false;
  singleLocation: number;
  location: Array<Location> = new Array<Location>();
  hourlyData: Array<HourlyLoadModel> = new Array<HourlyLoadModel>();
  hourlyQuoteData: Array<HourlyNewModifiedQuotes> = new Array<HourlyNewModifiedQuotes>();
  mergedQuoteData: Array<HourlyNewModifiedQuotes> = new Array<HourlyNewModifiedQuotes>();
  peakHourData: Array<PeakHoursModel> = new Array<PeakHoursModel>();
  TransactionChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };
  PremiumChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };
  PoliciesChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };
  AgencyFeeChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };

  ConsolatedChartData: any;
  QuoteChart: any;

  NewQuoteChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };
  ModifiedQuoteChart: { labels: any[]; datasets: { backgroundColor: string; borderColor: string; fill: boolean; data: any[]; }[]; };
  RawModifiedNewChartData: Array<{ ModifiedQuotes: any, NewQuotes: any, Intervel: any }>;
  ChartModifiedNewChartQuote: Array<any> = new Array<any>();
  PageName: string = "Hourly WorkLoad";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  seletedIndex: number = 0;
  ChartData: Array<any> = new Array<any>();
  showLoader: boolean = true;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedRegionalManager: [] = [];
  MergerChatData: Array<{ Policy: any, Transactions: any, AgencyFee: any, Premium: any, NewQuotes: any, ModifiedQuotes: any, hourInterval: any, Agents: any }>;
  totalPolicy: number = 0;
  totalAgencyFee: number = 0;
  totalPremium: number = 0;
  totalTransactions: number = 0;
  totalAgents: number = 0;

  month: Array<MonthModel>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  date: Array<string>;
  dateSelectItem: SelectItem[] = [];
  selecteddate: string = "";
  selectedNextdate: string = "";
  BackRoundColor: Array<any> = ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#FF6384', '#657EB8']
  BorderColor: Array<any> = ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#FF6384', '#657EB8']
  year: string = '2022';
  YearSelectItem: SelectItem[] = [];
  consolidatedData: Array<MergedQuotePolicyModel> = new Array<MergedQuotePolicyModel>();
  pager: any = {};
  pagedItems: any[] = [];
  pagerPeakHour: any = {};
  pagedItemsPeakHour: any[] = [];
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'black',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context && (context.dataset.label === "Agency Fee" || context.dataset.label === "Premium")) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
          else {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          if (data && data.datasets && data.datasets.length > 0 && (data.datasets[tooltipItem.datasetIndex].label === "Agency Fee" || data.datasets[tooltipItem.datasetIndex].label === "Premium")) {
            return data.datasets[tooltipItem.datasetIndex].label + `- $${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
          else {
            return data.datasets[tooltipItem.datasetIndex].label + `- ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

          }
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: "#000"
        },
        scaleLabel: {
          display: true,
          fontColor: "#000"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#000"
        }
      }]
    }

  }
  chartOptionsWithoutDoller: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50,
        height: 80
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'black',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    legend: { display: false },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label; // 1000 + 'k';
          },
          fontColor: "#000"
        },
        scaleLabel: {
          display: true,
          // labelString: '1k = 1000',
          fontColor: "#000"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#000"
        }
      }]
    }

  }
  chartOptionsWithDoller: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50,
        height: 80
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'black',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    legend: { display: false },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label; // 1000 + 'k';
          },
          fontColor: "#000"
        },
        scaleLabel: {
          display: true,
          // labelString: '1k = 1000',
          fontColor: "#000"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#000"
        }
      }]
    }

  }


  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
      private hourlyservice: HourlyLoadService, private _pageservice: PageService, private datepipe:DatePipe) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.fillYear();
    this.getMonth();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
    }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //if (this.seletedIndex === 5) {
    //  this.ConsolidatedData();
    //}
    //if (this.seletedIndex === 0) {
    //  this.PeakHourData();
    //}
  }

  SetSingleLocation(e) {
    this.singleLocation = e;
    //this.getDataForAllTabs();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = e;
    if (this.seletedIndex !== 6) {
      this.singleLocation = e[0].agencyId;
    }
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }


    getMonth() {
        this.monthSelectItem = [];
    this.hourlyservice.getMonth(this.year).subscribe(m => {
      this.month = m;
      let first: boolean = true;
      this.month.forEach(m => {
        this.monthSelectItem.push({ label: m.name, value: m.id })
        if (first) {
          this.selectedmonth = m.id;
          first = false;
        }
      })
      this.getDate();
    })
  }

  getDate() {
    this.dateSelectItem = [];
      this.hourlyservice.getDate(this.selectedmonth, this.year).subscribe(m => {
          this.showLoader = false;
      this.date = m;
      let first: boolean = true;
      let Counter: number = 0;
      this.date.forEach(m => {
        Counter = Counter + 1;
        this.dateSelectItem.push({ label: this.getDateString(m), value: m })
        if (first) {
            this.selecteddate = m;
            this.selectedNextdate = m;
          first = false;
        }
        if (Counter === 7) {
          this.selectedNextdate = m
        }
      })
      //this.getDataForAllTabs();
    })
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[1] - 1)];
    date = month + " " + (parts[2]);
    return date;
  }

  getDateStringBybackslash(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onMonthchange(e) {
    this.getDate();
  }

  onDatechange(e) {
    //this.getDataForAllTabs();
  }

  getdata(date: string, agencyId: number) {
    this.hourlyData = new Array<HourlyLoadModel>();
    this.showLoader = true;
    this.hourlyservice.getMergeData(date, agencyId).subscribe(m => {
      this.hourlyData = m;
      this.CreateChartForHorlyLoad()
      this.showLoader = false;
    })
  }

  ShowAgentName(e) {
    let retstring = '';
    if (e) {
      let res: Array<string> = new Array<string>();
      res = e.split(",");

      if (res.length > 2) {
        retstring = res[0] + " , " + res[1] + "...";
      }
      else {
        retstring = e
      }

    }
    return retstring;
  }

  ShowAgentNameToolTip(e:string) {
    let x = '';
    if (e) {
      let res: Array<string> = new Array<string>();
      res = e.split(',');
      for (let code of res) {
        x += `<span>${code}</span><br>`;
      }
      //x = e.replace(',', ' , ');
    }
    else {
      x = '';
    }
   
    return x;
  }

  gethourlyQuoteData(date: string, agencyId: number) {
    this.hourlyQuoteData = new Array<HourlyNewModifiedQuotes>();
    this.showLoader = true;
    this.hourlyservice.getQuoteHourlyData(date, agencyId).subscribe(m => {
      this.hourlyQuoteData = m;
      this.CreateQuoteChartForHorlyLoad();
      this.showLoader = false;
    })
  }

    mergedData(date: string, agencyId: number) {
        this.showLoader = true;
    this.totalPolicy = 0;
    this.totalAgencyFee = 0;
    this.totalPremium = 0;
    this.totalTransactions = 0;
    this.totalAgents = 0;
    this.MergerChatData = [];
    this.ChartData = [];
    let Data: Array<MergedQuotePolicyModel> = new Array<MergedQuotePolicyModel>();
    let HourInterval: any, Policy: any, Transactions: any, AgencyFee: any, Premium: any, NewQuotes: any, ModifiedQuotes: any, agents: any;
    let DataSet: Array<any> = new Array<any>()
    this.hourlyservice.getMergeData(date, agencyId).subscribe(m => {
        Data = m;
        console.log(date + " " + agencyId);
      Data.forEach(m => {
        HourInterval = m.hourIntervel;
        Policy = {
          label: "Policy",
          backgroundColor: this.BackRoundColor[0],
          borderColor: this.BorderColor[0],
          data: [m.policies]
        }
        Transactions = {
          label: "Transactions",
          backgroundColor: this.BackRoundColor[1],
          borderColor: this.BorderColor[1],
          data: [m.transactions]
        }
        AgencyFee = {
          label: "Agency Fee",
          backgroundColor: this.BackRoundColor[2],
          borderColor: this.BorderColor[2],
          data: [m.agencyfee]
        }
        Premium = {
          label: "Premium",
          backgroundColor: this.BackRoundColor[3],
          borderColor: this.BorderColor[3],
          data: [m.premium]
        }
        NewQuotes = {
          label: "NewQuotes",
          backgroundColor: this.BackRoundColor[4],
          borderColor: this.BorderColor[4],
          data: [m.newQuotes]
        }
        ModifiedQuotes = {
          label: "ModifiedQuotes",
          backgroundColor: this.BackRoundColor[5],
          borderColor: this.BorderColor[5],
          data: [m.modifiedQuotes]
        }
        agents = {
          label: "Agents",
          backgroundColor: this.BackRoundColor[6],
          borderColor: this.BorderColor[6],
          data: [m.agentCount]
        }
        this.totalPolicy = this.totalPolicy + m.policies;
        this.totalAgencyFee = this.totalAgencyFee + m.agencyfee;
        this.totalPremium = this.totalPremium + m.premium;
        this.totalTransactions = this.totalTransactions + m.transactions;
        this.totalAgents = this.totalAgents + m.agentCount;
        this.MergerChatData.push({ Policy: Policy, Transactions: Transactions, AgencyFee: AgencyFee, Premium: Premium, NewQuotes: NewQuotes, ModifiedQuotes: ModifiedQuotes, hourInterval: HourInterval, Agents: agents })
      });
      this.MergerChatData.forEach(m => {
        this.ChartData.push(
          {
            ChatName: m.hourInterval,
            datasets: [m.Policy, m.Transactions, m.AgencyFee, m.Premium, m.NewQuotes, m.ModifiedQuotes, m.Agents]
          }
        )
      });

      this.showLoader = false;
    });
  }


  getDataForAllTabs() {
    if (this.seletedIndex === 6) {
      this.ConsolidatedData();
    }
    if (this.seletedIndex === 0) {
      this.PeakHourData();
    }
    else {
      this.mergedData(this.selecteddate, this.singleLocation)
      this.getdata(this.selecteddate, this.singleLocation);
      this.gethourlyQuoteData(this.selecteddate, this.singleLocation);
    }
  }

  CreateChartForHorlyLoad() {
    //let dataArray = [];
    let Intervel = [];
    let Transactions = []
    let Premium = []
    let Policies = []
    let AgencyFee = []
    this.hourlyData.forEach((e, i) => {
      Intervel.push(e.hourIntervel);
      Transactions.push(e.transactions)
      Premium.push(e.premium)
      Policies.push(e.policies)
      AgencyFee.push(e.agencyfee)

    })

    this.ConsolatedChartData = {
      labels: Intervel,
      datasets: [
        {
          label: 'Policies',
          data: Policies,
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'Agency Fee',
          data: AgencyFee,
          fill: false,
          borderColor: '#565656'
        }
        ,
        {
          label: 'Premium',
          data: Premium,
          fill: false,
          borderColor: '#42A5F5'
        }
        ,
        {
          label: 'Transactions',
          data: Transactions,
          fill: false,
          borderColor: '#9CCC65'
        }
      ]
    }

  }


  //ChartForTransaction
  chartTransaction(transactionArray: any[]) {
    let interval = [];
    let transaction = [];
    transactionArray.forEach((e, i) => {
      interval.push(e.Intervel);
      transaction.push(e.Transactions)
    })
    this.TransactionChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          fill: false,
          data: transaction
        }
      ]
    }
  }
  //ChartForPremium
  chartPremium(premiumArray: any[]) {
    let interval = [];
    let premium = [];
    premiumArray.forEach((e, i) => {
      interval.push(e.Intervel);
      premium.push(e.Premium)
    })
    this.PremiumChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#9CCC65',
          borderColor: '#1E88E5',
          fill: false,
          data: premium
        }
      ]
    }
  }
  //ChartForAgencyFee
  chartAgencyFee(agenctFeeArray: any[]) {
    let interval = [];
    let agencyFee = [];
    agenctFeeArray.forEach((e, i) => {
      interval.push(e.Intervel);
      agencyFee.push(e.AgencyFee)
    })
    this.AgencyFeeChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#FFCE56',
          borderColor: '#1E88E5',
          fill: false,
          data: agencyFee
        }
      ]
    }
  }
  //ChartForPolicies
  chartPolicies(policiesArray: any[]) {
    let interval = [];
    let policies = [];
    policiesArray.forEach((e, i) => {
      interval.push(e.Intervel);
      policies.push(e.Policies)
    })
    this.PoliciesChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#4bc0c0',
          borderColor: '#1E88E5',
          fill: false,
          data: policies
        }
      ]
    }
  }




  //ChartForQuotes
  CreateQuoteChartForHorlyLoad() {
    let Intervel = [];
    let ModifiedQuote = []
    let NewQuote = []
    this.hourlyQuoteData.forEach((e, i) => {
      Intervel.push(e.hourIntervel);
      ModifiedQuote.push(e.modifiedQuotes)
      NewQuote.push(e.newQuotes)
    })
    this.QuoteChart = {
      labels: Intervel,
      datasets: [
        {
          label: 'Modified Quote',
          data: ModifiedQuote,
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'New Quote',
          data: NewQuote,
          fill: false,
          borderColor: '#565656'
        }
      ]
    }
  }

  //ChartForNewQuote
  chartNewQuote(newQuoteArray: any[]) {
    let interval = [];
    let newQuote = [];
    newQuoteArray.forEach((e, i) => {
      interval.push(e.Intervel);
      newQuote.push(e.NewQuote)
    })
    this.NewQuoteChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          fill: false,
          data: newQuote
        }
      ]
    }
  }
  //ChartForModifiedQuote
  chartModifiedQuote(modifiedArray: any[]) {
    let interval = [];
    let modifiedQuote = [];
    modifiedArray.forEach((e, i) => {
      interval.push(e.Intervel);
      modifiedQuote.push(e.ModifiedQuote)
    })
    this.ModifiedQuoteChart = {
      labels: interval,
      datasets: [
        {
          backgroundColor: '#FF6384',
          borderColor: '#1E88E5',
          fill: false,
          data: modifiedQuote
        }
      ]
    }
  }

  TabChange(e) {
    this.seletedIndex = e;
    this.setTabData();
  }
    Search() {
        //let latest_date = this.datepipe.transform(this.selecteddate, 'yyyy-MM-dd');
        //let latest_date1 = this.datepipe.transform(this.selectedNextdate, 'yyyy-MM-dd');

        //if (latest_date > latest_date1) {
        //    alert("You are selected invalid date");
        //}
        //else {
        //    this.setTabData();
        //}

        this.setTabData();
    }

    setTabData() {
        console.log("this.seletedIndex", this.seletedIndex);
    this.singleLocationSelected = true;
    if (this.seletedIndex === 1 || this.seletedIndex === 2) {
      this.getDataForAllTabs();
    }

    if (this.seletedIndex === 0) {
      this.singleLocationSelected = false;
      this.PeakHourData();
    }
    else if (this.seletedIndex === 3) {
      //this.CreateChartForHorlyLoad();
        this.getdata(this.selecteddate, this.singleLocation);
    }
    else if (this.seletedIndex === 4) {
        this.gethourlyQuoteData(this.selecteddate, this.singleLocation);
      //this.CreateQuoteChartForHorlyLoad();
    }
    else if (this.seletedIndex === 5) {
        this.gethourlyQuoteData(this.selecteddate, this.singleLocation);
        //this.CreateQuoteChartForHorlyLoad();
    }
    else if (this.seletedIndex === 6) {
      this.singleLocationSelected = false;
      this.ConsolidatedData();
    }
  }

  fillYear() {
      this.YearSelectItem = [];
      this.YearSelectItem.push(
          {
              label: '2021', value: '2021'
          })
    this.YearSelectItem.push(
      {
        label: '2022', value: '2022'
      })
  }

  onYearchange() {
    this.getMonth();
  }

  ConsolidatedData() {
    this.totalPolicy = 0;
    this.totalAgencyFee = 0;
    this.totalPremium = 0;
    this.totalTransactions = 0;
    let Data: Array<MergedQuotePolicyModel> = new Array<MergedQuotePolicyModel>();
    let tempData: Array<MergedQuotePolicyModel> = new Array<MergedQuotePolicyModel>();
    this.consolidatedData = new Array<MergedQuotePolicyModel>();
      this.showLoader = true;
      console.log("dddddsss");
      this.hourlyservice.ConsolidateData(this.selecteddate, this.selecteddate).subscribe(m => {
        Data = m;
        console.log("ddddd");
      if (this.selectedlocation.length > 0) {
        this.selectedlocation.forEach(e => {
          tempData = new Array<MergedQuotePolicyModel>();
          tempData = Data.filter(m => m.agencyid === e)
          tempData.forEach(x => {
            this.consolidatedData.push(x);
          })
        })
      }
      else {
        this.consolidatedData = Data;
      }
      this.consolidatedData.forEach(m => {
        this.totalPolicy = this.totalPolicy + m.policies;
        this.totalAgencyFee = this.totalAgencyFee + m.agencyfee;
        this.totalPremium = this.totalPremium + m.premium;
        this.totalTransactions = this.totalTransactions + m.transactions;
      })
      this.pager = {};
      this.pagedItems = [];
      this.setPage(1)
      this.showLoader = false;
    });
  }

  PeakHourData() {
    this.totalPolicy = 0;
    this.totalAgencyFee = 0;
    this.totalPremium = 0;
    this.totalTransactions = 0;
    this.showLoader = true;
    this.hourlyservice.PeakHourData(this.selecteddate, this.selectedNextdate, this.selectedlocation).subscribe(m => {
      this.peakHourData = m;
      this.peakHourData.forEach(m => {
        this.totalPolicy = this.totalPolicy + m.policies;
        this.totalAgencyFee = this.totalAgencyFee + m.agencyfee;
        this.totalPremium = this.totalPremium + m.premium;
        this.totalTransactions = this.totalTransactions + m.transactions;
      })
      this.pagerPeakHour = {};
      this.pagedItemsPeakHour = [];
      this.setPageForPeakHour(1);
      this.showLoader = false;
    });

  }


  DownloadExcel() {
    this.hourlyservice.ConsolidateCSV(this.selecteddate, this.selectedNextdate);
  }
  DownloadCSV() {
    this.hourlyservice.PeakDataCSV(this.selecteddate, this.selectedNextdate);
  }


  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this._pageservice.getPager(this.consolidatedData.length, page);
    this.pagedItems = this.consolidatedData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setPageForPeakHour(page: number) {
    if (page < 1 || page > this.pagerPeakHour.totalPages) {
      return;
    }

    this.pagerPeakHour = this._pageservice.getPager(this.peakHourData.length, page);
    this.pagedItemsPeakHour = this.peakHourData.slice(this.pagerPeakHour.startIndex, this.pagerPeakHour.endIndex + 1);
  }
}
