import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { newOldQuotesComponent } from './newOldQuotes.component';
import { NewOldQuotesRoutingModule } from './newOldQuotes-routing.module';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    NewOldQuotesRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [newOldQuotesComponent],
  exports: []
 })
export class NewOldQuotesModule { }
