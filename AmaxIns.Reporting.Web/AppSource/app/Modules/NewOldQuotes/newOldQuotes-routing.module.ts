import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { newOldQuotesComponent } from './newOldQuotes.component';


const routes: Routes = [
  { path: 'NewModifiedQuotes', component: newOldQuotesComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NewOldQuotesRoutingModule { }
