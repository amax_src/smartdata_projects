import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { Location } from '../../model/location';
import * as _ from 'underscore';
import { MonthModel } from '../../model/MonthModel';
import { QuotesService } from '../Services/quotesService';
import { NewModifiedQuotes } from '../../model/NewModifiedQuotes';
import { SelectItem } from 'primeng/api';
import { PageService } from '../Services/pageService';
import { DateService } from '../Services/dateService';
import { QuotesParam } from '../../model/hourlyNewModifiedQuotesModel';




@Component({
  selector: 'app-newOldQuotes',
  templateUrl: './newOldQuotes.component.html',
  styleUrls: ['./newOldQuotes.component.scss']
})

export class newOldQuotesComponent implements OnInit, AfterViewChecked {
  location: Array<Location> = new Array<Location>();
  PageName: string = "New and Modified Quotes";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<string> = new Array<string>();
  setLocation: Array<number> = new Array<number>();

   QuotesDistinctDate: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  QuotesData: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  NewQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  ModifiedQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  NewQuoteCount: number = 0;
  ModifiedQuoteCount: number = 0;
  NewPolicyCount: number = 0;
  ModifiedPolicyCount: number = 0;
  PolicyCount: number = 0;
  selectedRegionalManager: [] = [];
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];

    IsShowBOLTiels: boolean = false;
    VisitedCarrierWebsite: number = 0;
    TotalSoldPolicy: number = 0;

  year: string = '2022';
  YearSelectItem: SelectItem[] = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<any> = new Array<any>();
  selectedTab: number = 0;
  NewQuotesfilter: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  ModifiedQuotesfilter: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();

    QuotesParam: QuotesParam;

  pagerNewQuotes: any = {};
  pagedNewQuotesItems: any[];

  pagerModifiedQuotes: any = {};
  pagedModifiedQuotesItems: any[];

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private Service: QuotesService, private _pageservice: PageService, private _DateService: DateService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

    ngOnInit() {
        this.QuotesParam = new QuotesParam();

        this.fillYear();
        this.getMonth();

        this.router.events.subscribe((obj: any) => {
            if (obj instanceof RouteConfigLoadStart) {
                NProgress.start();
                NProgress.set(0.4);
            } else if (obj instanceof RouteConfigLoadEnd) {
                NProgress.set(0.9);
                setTimeout(() => {
                    NProgress.done();
                    NProgress.remove();
                }, 500);
            } else if (obj instanceof NavigationEnd) {
                this.cmnSrv.dashboardState.navbarToggle = false;
                this.cmnSrv.dashboardState.sidebarToggle = true;
                window.scrollTo(0, 0);
            }
        });
    }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.GetData();
  }

  GetAllLocations(e) {
    this.setLocation = [];
    this.selectedlocation = [];
    this.location = e;
    this.selectedlocation.push(this.location[0].agencyId.toString());
    this.setLocation.push(this.location[0].agencyId);
    this.GetData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

    fillYear() {
        this.YearSelectItem = [];
        this.YearSelectItem.push(
            {
                label: '2021', value: '2021'
            })
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
    }

    SearchReport() {
        this.GetData();
    }

  GetData() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.QuotesData = new Array<NewModifiedQuotes>();
    this.NewQuotes = new Array<NewModifiedQuotes>();
    this.ModifiedQuotes = new Array<NewModifiedQuotes>();
      this.QuotesParam = new QuotesParam();
      if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
          this.QuotesParam.agencyids = this.selectedlocation;
          this.QuotesParam.dates = this.selectedDate;

          this.Service.GetModifiedNewQuotes_New(this.QuotesParam, this.selectedmonth, this.year).subscribe(m => {
              this.QuotesData = m;
              this.NewQuotes = this.QuotesData.filter(m => m.quoteType === 'new')
              this.ModifiedQuotes = this.QuotesData.filter(m => m.quoteType === 'modified');
              console.log("this.QuotesData ", this.QuotesData)
              this.NewQuotesfilter = this.NewQuotes
              this.ModifiedQuotesfilter = this.ModifiedQuotes
              this.filterData();
              this.ischartDataLoaded = true;
              //this.showLoader = false;
          });
      }
      else {
          this.showLoader = false;
      }
      
  }

  filterData() {
    this.pagerNewQuotes = {};
    this.pagedNewQuotesItems = [];

    this.pagerModifiedQuotes = {};
    this.pagedModifiedQuotesItems = [];

    if (this.selectedDate.length > 0) {
      let tempNewQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
      let tempModifiedQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
      this.NewQuotesfilter = [];
      this.ModifiedQuotesfilter = [];
      this.selectedDate.forEach(m => {
        tempNewQuotes = this.NewQuotes.filter(x => x.createDate === m)
        tempModifiedQuotes = this.ModifiedQuotes.filter(x => x.createDate === m)
        tempNewQuotes.forEach(x => {
          this.NewQuotesfilter.push(x);
        })

        tempModifiedQuotes.forEach(x => {
          this.ModifiedQuotesfilter.push(x);
        })

      })

    }
    else {
      this.NewQuotesfilter = this.NewQuotes
      this.ModifiedQuotesfilter = this.ModifiedQuotes
    }

    this.setNewQuotesPaging(1);
    this.setModifiedQuotesPaging(1);
    this.getTileData();
    //this.setDates();
  }

  getTileData() {
    let NewQuoteswithpolicy: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
    NewQuoteswithpolicy = this.NewQuotesfilter.filter(m => m.policyNumber)
    //this.NewPolicyCount = NewQuoteswithpolicy.length;
    let ModifiedQuoteswithpolicy: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
    ModifiedQuoteswithpolicy = this.ModifiedQuotesfilter.filter(m => m.policyNumber)
    //this.ModifiedPolicyCount = ModifiedQuoteswithpolicy.length;
    this.NewQuoteCount = this.NewQuotesfilter.length;
    this.ModifiedQuoteCount = this.ModifiedQuotesfilter.length;
      this.showLoader = true;
      this.TotalSoldPolicy = 0;
      this.QuotesParam = new QuotesParam();
      if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
          this.QuotesParam.agencyids = this.selectedlocation;
          this.QuotesParam.dates = this.selectedDate;

          this.Service.ModifiedQuotesSoldTiles(this.QuotesParam, this.selectedmonth, this.year).subscribe(m => {
              this.TotalSoldPolicy = m;
              console.log("this.TotalSoldPolicy", this.TotalSoldPolicy);
              //this.ModifiedPolicyCount = ModifiedQuoteswithpolicy.length;
              this.NewPolicyCount = NewQuoteswithpolicy.length;

              this.ModifiedPolicyCount = (this.TotalSoldPolicy - this.NewPolicyCount);
              if (this.ModifiedPolicyCount <= 0) {
                  this.ModifiedPolicyCount = 0;
              }
              this.showLoader = false;
          });
      }

      this.IsShowBOLTiels = false;
      if (this.selectedlocation.length == 1 && this.selectedlocation[0] == '221') {
          this.IsShowBOLTiels = true;
          this.VisitedCarrierWebsite = this.NewQuotesfilter.filter(m => m.cat === 1).length;
          console.log("this.VisitedCarrierWebsite", this.VisitedCarrierWebsite);
          console.log("this.NewQuotesfilter", this.NewQuotesfilter);
      }
  }


    setDates() {

        this.QuotesDistinctDate = new Array<NewModifiedQuotes>();
        this.Service.GetDistinctDateQuotes(this.selectedmonth, this.year).subscribe(m => {
            this.QuotesDistinctDate = m;
            this.GetDate(this.QuotesDistinctDate);
            console.log("this.QuotesDistinctDate", this.QuotesDistinctDate);
        });

        //if (this.selectedTab == 0) {
        //    this.GetDate(this.NewQuotes)
        //}
        //else {
        //    this.GetDate(this.ModifiedQuotes)
        //}
    }

  GetDate(x: Array<NewModifiedQuotes>) {
    this.dateSelectItem = []
    let dt: Array<string> = new Array<string>();
    x.forEach(i => {
      if (dt.length === 0) {
        dt.push(i.createDate);
      }
      else {
        let isUnique: boolean = true;
        dt.forEach(c => {
          if (c === i.createDate) {
            isUnique = false;
          }
        })
        if (isUnique) {
          dt.push(i.createDate);
        }
      }
    })
    dt.forEach(i => {
      this.dateSelectItem.push(
        {
          label: this.getDateString(i), value: i
          //label: i, value: i

        })
    });

  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  OndateChanged() {
    //this.filterData();
  }

  tabChange(e) {
    this.selectedTab = e;
    this.setDates();
  }


  onYearchange() {
    this.getMonth();
  }

  getMonth() {
    this._DateService.getMonthKeyValue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      let tempmonths: Array<MonthModel> = new Array<MonthModel>();
      months = m;
      if (months.length > 2) {
        tempmonths = months.slice(months.length - 3, months.length);
      }
      months = tempmonths.reverse();

      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })

      this.selectedmonth = months[0].id
      //this.GetData();
        this.setDates();
    })
    //this.filterData();
  }

    onMonthchange() {
        this.selectedDate = new Array<any>()
        this.setDates();
        //this.GetData();
    }

  setNewQuotesPaging(page: number) {
    if (page < 1 || page > this.pagerNewQuotes.totalPages) {
      return;
    }

    this.pagerNewQuotes = this._pageservice.getPager(this.NewQuotesfilter.length, page, 50);

    this.pagedNewQuotesItems = this.NewQuotesfilter.slice(this.pagerNewQuotes.startIndex, this.pagerNewQuotes.endIndex + 1);
  }

  setModifiedQuotesPaging(page: number) {
    if (page < 1 || page > this.pagerModifiedQuotes.totalPages) {
      return;
    }

    this.pagerModifiedQuotes = this._pageservice.getPager(this.ModifiedQuotesfilter.length, page, 50);

    this.pagedModifiedQuotesItems = this.ModifiedQuotesfilter.slice(this.pagerModifiedQuotes.startIndex, this.pagerModifiedQuotes.endIndex + 1);
  }
}
