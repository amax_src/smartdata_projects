import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { QuotesParam } from '../../model/hourlyNewModifiedQuotesModel';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
       // 'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  

  GetModifiedNewQuotes(AgencyIds: Array<string>, month: string) {
    const url = `${this.baseApiUrl}Quotes/GetNewModiFiedQuotes?month=` + month;
    let headers = this.header;
    return this.http.post<any>(url, AgencyIds, { headers })
      .pipe(map(x => {
        return x;
      }));
    }


    GetModifiedNewQuotes_New(quoteParam: QuotesParam, month: string, year: string) {
        const url = `${this.baseApiUrl}Quotes/GetNewAndModifiedData_New?month=` + month + `&year=` + year;
        let headers = this.header;
        return this.http.post<any>(url, quoteParam, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetDistinctDateQuotes(month: string, year: string) {
        const url = `${this.baseApiUrl}Quotes/GetDistinctDateQuotes?month=` + month + `&year=` + year;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    ModifiedQuotesSoldTiles(quoteParam: QuotesParam, month: string, year: string) {
        const url = `${this.baseApiUrl}Quotes/ModifiedQuotesSoldTiles?month=` + month + `&year=` + year;
        let headers = this.header;
        return this.http.post<any>(url, quoteParam, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
  
}

