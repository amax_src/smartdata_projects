import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json'
       // 'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getYear() {
    const url = `${this.baseApiUrl}Date/GetYears`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonth(year: string) {
    const url = `${this.baseApiUrl}/Date/GetMonth/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonthKeyValue(year: string) {
    const url = `${this.baseApiUrl}/Date/GetMonthKeyValue/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetMonthsCurrentYear() {
    const url = `${this.baseApiUrl}/Date/GetMonthsCurrentYear`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  
}
