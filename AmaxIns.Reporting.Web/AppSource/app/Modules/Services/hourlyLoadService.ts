import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';


@Injectable({
  providedIn: 'root'
})
export class HourlyLoadService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json'
      // 'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  getDate(month: string, year: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetDate?month=` + month + '&year=' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonth(year: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetMonth/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getData(date: string, agencyId: number) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetData?Date=` + date + "&AgencyId=" + agencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  getQuoteHourlyData(date: string, agencyId: number) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetHourlyNewModifiedQuotes?Date=` + date + "&AgencyId=" + agencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMergeData(date: string, agencyId: number) {
    const url = `${this.baseApiUrl}/HourlyLoad/MergeData?Date=` + date + "&AgencyId=" + agencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  ConsolidateData(StartDate: string, EndDate: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/ConsolidatedHourlyData?StartDate=` + StartDate + "&EndDate=" + EndDate;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  PeakDataCSV(StartDate: string, EndDate: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/ExportPeakData?StartDate=` + StartDate + "&EndDate=" + EndDate;
    window.open(url, "_blank");
  }

  ConsolidateCSV(StartDate: string, EndDate: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/ExportHourlyData?StartDate=` + StartDate + "&EndDate=" + EndDate;
    window.open(url, "_blank");
  }

  PeakHourData(StartDate: string, EndDate: string, AgencyId: Array<number>) {
    const url = `${this.baseApiUrl}/HourlyLoad/PeakHoursData?StartDate=` + StartDate + "&EndDate=" + EndDate;
    let headers = this.header;
    return this.http.post<any>(url, AgencyId, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

}
