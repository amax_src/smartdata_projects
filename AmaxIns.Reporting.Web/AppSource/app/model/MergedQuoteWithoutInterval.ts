export class MergedQuoteWithoutInterval {
  agencyfee: number = 0;
  agencyid: number = 0;
  modifiedQuotes: number = 0;
  newQuotes: number = 0;
  policies: number = 0;
  premium: number = 0;
  transactions: number = 0;
}
