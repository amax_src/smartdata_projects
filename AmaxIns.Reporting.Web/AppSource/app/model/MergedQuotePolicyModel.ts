export class MergedQuotePolicyModel {
  agencyfee: number = 0;
  agencyid: number = 0;
  hourIntervel: string = '';
  modifiedQuotes: number = 0;
  newQuotes: number = 0;
  policies: number = 0;
  premium: number = 0;
  transactions: number = 0;
  date: string = ''
  location: string = ''
  agentCount: number = 0;
}


