export class HourlyLoadModel {
  public policies :number = 0;
  public agencyfee: number = 0;
  public premium: number = 0;
  public transactions: number = 0;
  public location: string = '';
  public agencyid: number = 0;
  public paydate: string = '';
  public hourIntervel: string = '';
  public dayofweek: string = '';
  public agentCount: number = 0;
  public agentNames:string ='';
}
