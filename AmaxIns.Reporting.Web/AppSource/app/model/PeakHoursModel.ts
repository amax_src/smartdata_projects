export class PeakHoursModel {
  agencyId: number = 0;
  location: string = '';
  payDate: string = '';
  peakhourpremium: string = '';
  premium: number = 0;
  peakhourafee: string = '';
  agencyfee: number = 0;
  peakhourpolicies: string = '';
  policies: number = 0;
  peakhourtransactions: string = '';
  transactions: number = 0;
  peakhourmodifiedquotes: string = '';
  modifiedquotes: number = 0;
  peakhournewquotes: string = '';
  newquotes: number = 0;
}
