﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Dates
{
    public interface IYearService: IBaseService
    {
        Task<IEnumerable<string>> GetYears();
    }
}
