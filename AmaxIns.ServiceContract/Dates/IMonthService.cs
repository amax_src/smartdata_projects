﻿using AmaxIns.DataContract.CommanModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Dates
{
    public interface IMonthService: IBaseService
    {
        Task<List<string>> GetMonths(int Year);
        Task<List<MonthModel>> GetMonthKeyValue(int Year);
        Task<List<MonthModel>> GetExtraMonthKeyValue(int Year);
        Task<List<MonthModel>> GetMonthPlanner();
        Task<List<string>> GetDates();
        Task<List<string>> GetDatesByMonth(string month, string year);
    }
}
