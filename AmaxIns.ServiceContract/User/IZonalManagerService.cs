﻿using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.User
{
    public interface IZonalManagerService: IBaseService
    {
        Task<ZonalManagerModel> GetById(int Id);
        Task<IEnumerable<ZonalManagerModel>> GetAll();
        Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id);
        Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(List<int> Id);
    }
}
