﻿using System.Collections.Generic;
using AmaxIns.DataContract.User;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.User
{
    public interface ISaleDirectorService : IBaseService
    {
        Task<SaleDirectorModel> GetById(int Id);
        Task<IEnumerable<SaleDirectorModel>> GetAll();
    }
}
