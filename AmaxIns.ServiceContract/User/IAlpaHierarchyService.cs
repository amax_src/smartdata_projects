﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.User
{
    public interface IAlpaHierarchyService : IBaseService
    {
        Task<IEnumerable<SaleDirectorModel>> GetAllAlpaSaleDirector();
        Task<IEnumerable<RegionalManagerModel>> RegionalManagerALPA();
        Task<IEnumerable<ZonalManagerModel>> ZonalManagerALPA();
        Task<IEnumerable<AgencyModel>> GetAllAgency();
        Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(List<int> Id);
        Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(List<int> Id);
        Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(List<int> RegionalManagerId, List<int> ZonalManagerId);
        Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(List<int> RegionalManagerId);
        Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id);
    }
}
