﻿using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.User
{
    public interface IRegionalManagerService: IBaseService
    {
        Task<RegionalManagerModel> GetById(int Id);
        Task<IEnumerable<RegionalManagerModel>> GetAll();
        Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id);
        Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(List<int> Id);
    }
}
