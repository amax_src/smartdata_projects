﻿using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.Revenue;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.EPR
{
    public interface IEPRService : IBaseService
    {
        Task<IEnumerable<EPRModel>> GetEPRData();
        Task<IEnumerable<string>> GetEPRmonths();
        Task<IEnumerable<EPRModel>> GetEPRTotalData();
        Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string _year, string location, string _selectedagent);

    }
}
