﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.AlphaTop5Agent;
using AmaxIns.RepositoryContract.AlphaTop5Agent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.AlphaTop5Agent
{
    public interface ITopAgentService : IBaseService
    {
        Task<IEnumerable<AgencyModel>> GetAgency();
        Task<IEnumerable<int>> GetYear();
        Task<IEnumerable<TopFiveDataModel>> GetPremiumData(TopFiveAgentUserModel model);

        Task<IEnumerable<TopFiveDataModel>> GetAgencyFeeData(TopFiveAgentUserModel model);
        Task<IEnumerable<TopFiveDataModel>> GetPolicyCount(TopFiveAgentUserModel model);

    }
}
