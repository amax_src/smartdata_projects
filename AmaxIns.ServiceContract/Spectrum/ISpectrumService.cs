﻿using AmaxIns.DataContract.Spectrum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Spectrum
{
    public interface ISpectrumService : IBaseService
    {
        Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumDailySummary();
        Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumDailyDetail(string date);
        Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumMonthlySummary();
        Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumMonthlyDetail(string month, int? agencyId);
        Task<ConsolidatedCallVolumeModel> GetSpectrumCallVolume();
        Task<IEnumerable<CallCountModel>> GetCallCount(string date);
        Task<IEnumerable<CallCountModel>> GetCallCountOutBound(string date);
        Task<IEnumerable<RepeatedCallersModel>> GetRepeatedCallers(string date);
        Task<IEnumerable<string>> GetDates();
    }
}
