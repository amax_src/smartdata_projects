﻿using AmaxIns.DataContract.APILoad;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.APILoad
{
    public interface IApiLoadService:IBaseService
    {
        void Save(ApiLoadModel model);
    }
}
