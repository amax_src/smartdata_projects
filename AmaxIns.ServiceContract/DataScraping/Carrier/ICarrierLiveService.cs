﻿using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using AmaxIns.DataContract.DataScraping.Seaharbor;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.DataScraping.Carrier
{
    public interface ICarrierLiveService : IBaseService
    {

        IEnumerable<AspenLiveModel> GetAspenLiveData();
        IEnumerable<IPModel> GetAspenIPData();

        IEnumerable<SeaharborLiveModel> GetSeaharborLiveData();
        IEnumerable<IPModel> GetSeaharborIPData();
    }
}
