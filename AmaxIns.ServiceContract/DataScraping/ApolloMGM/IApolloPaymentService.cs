﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.DataScraping.ApolloMGM
{
    public interface IApolloPaymentService: IBaseService
    {
        IEnumerable<ApolloLiveModel> GetApolloLiveData();
        IEnumerable<IPModel> GetApolloIPData();
    }
}
