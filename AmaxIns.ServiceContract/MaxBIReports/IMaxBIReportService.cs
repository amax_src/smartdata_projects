﻿using AmaxIns.DataContract.MaxBIReports;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.MaxBIReports
{
    public interface IMaxBIReportService : IBaseService
    {
        Task<IEnumerable<CatalogInfo>> GetCatalogs(Request request);
        Task<IEnumerable<HierarchyInfo>> GetHierarchyLevel(HierarchyRequest request);
        Task<IEnumerable<AgentCarrierAFeePremium>> GetAgentCarrierAFeePremium(Request request);
        Task<IEnumerable<AgencyPerPolicy>> GetAFeePremiumPerPolicy(Request request);
        Task<decimal> GetAgencyPerPolicyReportSummary(Request request);
        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotes(Request request);
        Task<decimal> GetNewModifiedQuotesSummary(Request request);
        Task<IEnumerable<InboundOutboundCalls>> GetInboundOutboundCalls(Request request);
        Task<decimal> GetInboundOutboundCallsSummary(Request request);

        Task<IEnumerable<PayrollFilterData>> GetPayrollDashboardFilterData(PayrollRequest request);
        Task<IEnumerable<DataContract.MaxBIReports.Payroll>> GetPayrollDashboardData(PayrollRequest request);

        Task<IEnumerable<EmployeeInfo>> GetmployeeSensitiveInfo(Request request);

        Task<IEnumerable<HourlyProduction>> GetAgentHourlyProduction(Request request);
        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPR(Request request);
        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPRGraph(Request request);
    }
}