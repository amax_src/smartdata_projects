﻿using AmaxIns.DataContract.Agency;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Agency
{
    public interface IAgencyService : IBaseService
    {
        Task<IEnumerable<AgencyModel>> Get();
        Task<IEnumerable<AgencyModel>> GetSaleOfDirector(List<int> SaleDirectorId);
        Task<IEnumerable<AgencyModel>> Get(List<int> RegionalManagerId);
        Task<IEnumerable<AgencyModel>> Get(List<int> RegionalManagerId, List<int> ZonalManagerId);
        Task<IEnumerable<AgencyModel>> GetBSMAgency(List<int> BsmId);
        Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id);
    }
}
