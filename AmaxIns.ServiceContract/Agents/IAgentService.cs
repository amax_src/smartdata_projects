﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Agents
{
    public interface IAgentService : IBaseService
    {
        Task<IEnumerable<string>> GetAgents();
    }
}
