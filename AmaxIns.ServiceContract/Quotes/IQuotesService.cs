﻿using AmaxIns.DataContract.Quotes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Quotes
{
    public interface IQuotesService : IBaseService
    {
        Task<IEnumerable<string>> GetPaydate(string year, string month);
        //Task<IEnumerable<QuotesModel>> GetBoundQuotes(string paydate);
        IEnumerable<QuotesCount> GetBoundQuotes(string paydate);
        IEnumerable<QuotesCount> GetBoundNoTurboratorQuotes(string paydate);

        IEnumerable<QuotesSale> GetQuotesSale();

        Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotes(List<string> Agencyids, string month = null,string year=null);
        string ExportNewModiFiedQuotes(IEnumerable<NewModifiedQuotes> list, string pathRoot, int reportType);
        Task<IEnumerable<NewModifiedQuotes>> GetNewAndModifiedData_New(QuotesParam quotesParam, string month = null, string year = null);
        Task<IEnumerable<NewModifiedQuotes>> GetDistinctDateQuotes(string month = null, string year = null);
        Task<IEnumerable<NewAndModifiedQuotesProj>> GetNewModiFiedQuotesProjection(string month = null, string year = null);
        Task<IEnumerable<QuotesProjection>> GetQuotesProjection();
        Task<int> ModifiedQuotesSoldTiles(QuotesParam quotesParam, string month = null, string year = null);
    }
}
