﻿using AmaxIns.DataContract.AIS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.AIS
{
    public interface IAISService : IBaseService
    {
        Task<IEnumerable<AISModel>> GetAISData();
        Task<IEnumerable<AISModel>> GetAISDataCsv();
    }
}
