﻿using AmaxIns.DataContract.Calendar;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Calendar
{
    public interface ICalendarService : IBaseService
    {
        int AddEvents(CalendarEvents addEvent);
        List<CalendarEvents> GetEvents(List<int> agencyId, string month);

    }
}
