﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.Quotes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.HourlyLoad
{
    public interface IHourlyLoadService : IBaseService
    {
        Task<IEnumerable<HourlyLoadModel>> GetData(string date, int agencyId);
        Task<IEnumerable<string>> GetDates(int month, string year);
        Task<IEnumerable<MonthModel>> GetMonth(string year);
        Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string date, int agencyId);
        IEnumerable<AgentCountModel> MergeData(string date, int agencyId);
        IEnumerable<AgentCountModel> MergeData(string startdate, string enddate);
        Task<IEnumerable<PeakHoursModel>> GetPeakHoursData(string startdate, string enddate, List<int> agencyid);
    }
}
