﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.Notification
{
    public interface INotificationService : IBaseService
    {
        string GetNotification();
    }
}
