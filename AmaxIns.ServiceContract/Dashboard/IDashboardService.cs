﻿using AmaxIns.DataContract.Dashboard;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Dashboard
{
    public interface IDashboardService : IBaseService
    {
        Task<List<DashboardQuotesModel>> GetQuoteData(List<int> agencyid, int month, int year);
        Task<List<DashboardRevenueModel>> GetSalesData(List<int> agencyid, int month, int year);
        Task<List<DashboardEPRModel>> GetEprData(List<int> agencyid, int month, int year);
        Task<IEnumerable<DashboadSpectrumModel>> GetCallsData(List<int> agencyid, int month, int year);
        Task<IEnumerable<DashboardPayrollModel>> GetPayrollDashboardData(List<int> agencyid, int month, int year);
        Task<DashboardEprDetails> GetEprDetails(List<int> agencyid, int month, int year);

    }
}
