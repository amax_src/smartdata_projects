﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Report
{
    public interface ICarrierMixNewService : IBaseService
    {
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixI(int Year, string month);
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixPremium(int Year, string month);
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixAgencyFee(int Year, string month);
        string ExportExcelCarrierMix(IEnumerable<IDictionary<string, object>> list, string pathRoot,int reportType);
    }
}
