﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Report
{
    public interface IReportService : IBaseService
    {
        Task<IEnumerable<AvgAgencyFee>> GetAvgAgencyFees(string Year, string month, string location);
        Task<IEnumerable<AvgAgencyFeeII>> GetAvgAgencyFeesII(string Year, string month, string location);
        Task<IEnumerable<AvgAgencyFeeIII>> GetAvgAgencyFeesIII(string Year, string month, string location);

        // Agency Breakdown Report Methods
        Task<IEnumerable<AgencyBreakdownI>> GetAgencyBreakdownI(string Year, string month, string location);
        Task<IEnumerable<AgencyBreakdownII>> GetAgencyBreakdownII(string Year, string month, string location);
        Task<IEnumerable<AgencyBreakdownIII>> GetAgencyBreakdownIII(string Year, string month, string location);
        

        // Overtime Report
        Task<IEnumerable<OvertimeI>> GetOvertimeI(string Year, string month,string location);
        Task<IEnumerable<OvertimeII>> GetOvertimeII(string Year, string month, string location);
        Task<IEnumerable<OvertimeIII>> GetOvertimeIII(string Year, string month, string location);

      

        // Tracker

        Task<IEnumerable<TrackerRender>> GetRenterTracker(string Year, string month,string location);
        Task<IEnumerable<TrackerProduction>> GetProductionTracker(string Year, string month, string location);
        Task<IEnumerable<TrackerActiveCustomer>> GetActiveCustomerTracker(string Year, string month, string location);

        // Agent Performance
        Task<IEnumerable<AgentPerformance>> GetAgentPerformaceI(string Year, string month, string location);
        Task<IEnumerable<AgentPerformance>> GetAgentPerformaceII(string Year, string month, string location);

        //Excel
        string ExportExcelAgencyFeeI(IEnumerable<AvgAgencyFee> list, string pathRoot);
        string ExportExcelAgencyFeeII(IEnumerable<AvgAgencyFeeII> list, string pathRoot);
        string ExportExcelAgencyFeeIII(IEnumerable<AvgAgencyFeeIII> list, string pathRoot);

        string ExportExcelAgencyBreakDownByOfficeI(IEnumerable<AgencyBreakdownI> list, string pathRoot);
        string ExportExcelAgencyBreakDownByOfficeII(IEnumerable<AgencyBreakdownII> list, string pathRoot);
        string ExportExcelAgencyBreakDownByOfficeIII(IEnumerable<AgencyBreakdownIII> list, string pathRoot);

        string ExportExcelGetOvertimeI(IEnumerable<OvertimeI> list, string pathRoot);
        string ExportExcelGetOvertimeII(IEnumerable<OvertimeII> list, string pathRoot);
        string ExportExcelGetOvertimeIII(IEnumerable<OvertimeIII> list, string pathRoot);

        string ExportRenterTracker(IEnumerable<TrackerRender> list, string pathRoot);
        string ExportProductionTracker(IEnumerable<TrackerProduction> list, string pathRoot);
        string ExportActiveCustomerTracker(IEnumerable<TrackerActiveCustomer> list, string pathRoot);

        string ExportAgentPerformaceI(IEnumerable<AgentPerformance> list, string pathRoot);
        string ExportAgentPerformaceII(IEnumerable<AgentPerformance> list, string pathRoot);
    }
}
