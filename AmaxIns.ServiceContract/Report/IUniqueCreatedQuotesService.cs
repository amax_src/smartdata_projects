﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Report
{
    public interface IUniqueCreatedQuotesService : IBaseService
    {
        Task<IEnumerable<UniqueCreatedQuotes>> GetUniqueCreatedQuotes(int Year, string month,string location);
        Task<IEnumerable<Market>> GetAllMarket();
        string ExportExcelUniqueCretedQuotes(IEnumerable<UniqueCreatedQuotes> list, string pathRoot);
    }
}