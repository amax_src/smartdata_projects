﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Report
{
    public interface IRentersReportService : IBaseService
    {
        Task<IEnumerable<RentersData>> GetRenterData(int Year, string month,string location);
        Task<IEnumerable<ActiveCustomerData>> GetActiveCustomerData(int Year, string month, string location);
        string ExportExcelRenterData(IEnumerable<RentersData> list, string pathRoot);
        string ExportExcelActiveCustomer(IEnumerable<ActiveCustomerData> list, string pathRoot);
    }
}
