﻿using AmaxIns.DataContract.PBX;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.PBX
{
    public  interface IPbxService:IBaseService
    {
        Task<IEnumerable<string>> GetMonth();
        Task<IEnumerable<string>> GetExtention();
        Task<IEnumerable<string>> GetDate();
        Task<IEnumerable<PbxModel>> GetPBXData();

        Task<IEnumerable<PbxModel>> GetPBXDailyData();
    }
}
