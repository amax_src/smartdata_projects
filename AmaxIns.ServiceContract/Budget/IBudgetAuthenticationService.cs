﻿using AmaxIns.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Budget
{
    public interface IBudgetAuthenticationService : IBaseService
    {
        Task<LoginUser> LogInAsync(LoginModel login);
    }
}
