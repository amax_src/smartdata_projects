﻿using AmaxIns.DataContract.Budget;
using AmaxIns.DataContract.Response;
using Microsoft.AspNetCore.Http;
using Syncfusion.XlsIO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Budget
{
    public interface IBudgetService:IBaseService
    {
        Task<IEnumerable<string>> GetBudgetCategories(BudgetSearch obj);
        Task<IEnumerable<string>> GetBudgetDept(int userId);
        Task<IEnumerable<string>> GetBudgetBudgetName(BudgetSearch obj);
        Task<BudgetModel> GetBudgetByMonth(BudgetFormModel budget);
        Task<IEnumerable<BudgetModel>> GetBudgetAnnual(BudgetFormModel budget);
        Task<IEnumerable<BudgetTransactionModel>> GetBudgetTrx(BudgetFormModel budget);
        Task<IEnumerable<BudgetOverspendModel>> GetBudgetSpending(BudgetFormModel budget);
        Task<IEnumerable<string>> GetBudgetUser(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetLocation(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetSource(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetCode(BudgetSearch budgetSearch);
        Task<IEnumerable<BudgetTrancModel>> GetBudgetTranc(BudgetFormModel budget);
        Task<IEnumerable<BudgetSummaryModel>> GetBudgetSummary(BudgetFormModel budget);
        ResponseViewModel UploadExcelFile(IFormCollection formCollection);
        Task<IEnumerable<string>> GetLatestMonthValue(int year);
        string ExportAccountDetals(IEnumerable<BudgetOverspendModel> list, string pathRoot);
    }
}
