﻿using AmaxIns.DataContract.Authentication;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Authentication
{
    public interface IAuthenticationService : IBaseService
    {
        Task<LoginUser> LogInAsync(LoginModel login);
        Task<LoginUser> FDLogInAsync(LoginModel login);
        Task<bool> LogOutAsync(string userName);

        string UpdatePassword(ResetPassword login);
        Task<bool> ValidateUserName(string email, string LoginType);

        Task<bool> SendPasswordEmail(string email, string LoginType);
        Task<DesktopLoginUser> LogInDesktopAsync(string userName, string password);

        Task<LoginUser> LogInAsyncUsingOTPRequest(LoginModel login);
    }
}
