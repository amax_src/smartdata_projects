﻿using AmaxIns.DataContract.CustomerRetention;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract
{
    public interface ICustomerRetentionService: IBaseService
    {
        Task<ListResultModel> GetAllPagingAsync(ListQueryModel queryModel);
        Task<CountListForZoneResultModel> GetAllCountForZonePagingAsync(CountListForZoneQueryModel queryModel);
        Task<CountListForRegionResultModel> GetAllCountForRegionPagingAsync(CountListForRegionQueryModel queryModel);
        Task<CountListForHodResultModel> GetAllCountForHodPagingAsync(CountListForHodQueryModel queryModel);
        Task<string> GetAllPagingExportAsync(ListQueryModel queryModel, string pathRoot);
        Task<string> GetAllCountForZonePagingAsyncExport(CountListForZoneQueryModel queryModel, string pathRoot);
        Task<string> GetAllCountForRegionPagingAsyncExport(CountListForRegionQueryModel queryModel, string pathRoot);
        Task<string> GetAllCountForHodPagingAsyncExport(CountListForHodQueryModel queryModel, string pathRoot);
    }
}
