﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.LiveDataApp
{
    public interface ISpectrumLiveDataService : IBaseService
    {
        List<SpectrumLiveData> GetSpectrumLiveData();
        //List<SpectrumLiveData> GetSpectrumLiveDataV1();
        //List<SpectrumLiveData> GetStatsLiveDataAgentWise();
        //List<SpectrumLiveData> GetStatsWeeklyDataAgentWise(int weeklyId);
        List<SpectrumLiveData> GetSpectrumLiveDataV2();
        List<SpectrumLiveData> GetStatsLiveDataAgentWiseV2();
        List<SpectrumLiveData> GetStatsWeeklyDataAgentWiseV2(SpectrumParam data);
        List<SpectrumAgentCall> GetSpectrumAgentCallDetails(string extension);
        string ExportSpectrumCallLocationWise(IEnumerable<SpectrumLiveData> list, string pathRoot);
        string ExportSpectrumCallExtensionWise(IEnumerable<SpectrumLiveData> list, string pathRoot);
        string ExportLiveStratusAgentLog(IEnumerable<AgencyAgentLogLive> list, string pathRoot);
        string ExportStratusWeeklyDataAgentWise(IEnumerable<SpectrumLiveData> list, string pathRoot);
        string ExportStratusDataAgentExtensionWise(IEnumerable<SpectrumLiveData> list, string pathRoot);

        List<AgencyAgentLogLive> GetSpectrumLiveAgentLogV2();
        List<SpectrumAgentExtensionLog> GetSpectrumLiveAgentExtensionLogV2(string extension);
        List<AgentCallTransfer> GetCallTransferDetails(string extension, int AgencyId);
        List<SpectrumLiveData> GetStatsLiveDataAgentWiseVSTeam_selV2();
        List<SpectrumAgentCall> GetSpectrumExtensionWiseCallDetails(StratusExtensionParam stratusExtension);


        List<SpectrumLiveData> GetSpectrumLiveDataCAV2();
        List<AgentCallTransfer> GetCallTransferDetailsCA(string extension, int AgencyId);
        List<SpectrumLiveData> GetStatsLiveDataAgentWiseCAV2();
        List<AgencyAgentLogLive> GetSpectrumLiveAgentLogCAV2();
        List<SpectrumLiveData> GetStatsWeeklyDataAgentWiseCAV2(SpectrumParam data);
        List<SpectrumAgentCall> GetSpectrumExtensionWiseCallDetailsCA(StratusExtensionParam stratusExtension);

    }
}
