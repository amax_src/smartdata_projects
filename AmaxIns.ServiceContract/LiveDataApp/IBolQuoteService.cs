﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.LiveDataApp
{
   public interface IBolQuoteService
    {
        Task<IEnumerable<BOLQuoteData>> GetDataBolData(List<string> date,string state);
        List<MonthModel> GetCarrierName();
        Task<BOLQuoteData> GetSingleQuote(string quoteNumber);
        Task<IEnumerable<BOLQuoteData>> GetDataBolData_Quotes(List<string> date, string state);
        Task<IEnumerable<EODLive>> GetEODLiveReport(List<string> date);
        string AmaxHourlyDataAgencyWise(IEnumerable<AgentCountModel> list, string pathRoot);
        string AmaxHourlyData(IEnumerable<AgentCountModel> list, string pathRoot);
        Task<IEnumerable<CustomerJourneyInfo>> GetCustomerJourneyInfoReport(List<string> date);
        string ExportCustomerJourneyInfo(List<string> date, string pathRoot);
        Task<IEnumerable<RetailCustomerJourneyInfo>> GetRetailCustomerJourneyInfoReport(List<string> date);
        string ExportOnlineQuote(IEnumerable<ExportBOLModel> list, string pathRoot);
    }
}
