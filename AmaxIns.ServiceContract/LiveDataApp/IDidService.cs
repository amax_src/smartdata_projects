﻿using AmaxIns.DataContract.LiveData.DidConsolatedData;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.LiveDataApp
{
    public interface IDidService : IBaseService
    {
        List<DidNumberData> GetSummaryDIDData(string sday, string smonth, string syear, string eday, string emonth, string eyear);

    }
}
