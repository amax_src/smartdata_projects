﻿using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.LiveData;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.LiveDataApp
{
    public interface ILiveDataAppService : IBaseService
    {
        List<LiveDataAgency> GetData(out List<AgentCountModel> AmaxHourlyData, out List<AgentCountModel> AmaxHourlyDataAgencyWise);
        ConsolidatedDidData GetConsolidatedDIDData(string day,string month, string year, string endday, string endmonth, string endyear);
        
    }
}
