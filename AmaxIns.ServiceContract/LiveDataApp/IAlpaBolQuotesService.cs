﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.LiveDataApp
{
    public interface IAlpaBolQuotesService
    {
        List<MonthModel> GetAlpaCarrierName();
        Task<IEnumerable<BOLQuoteData>> GetAlpaDataBolData_Quotes(List<string> date, string state);
    }
}
