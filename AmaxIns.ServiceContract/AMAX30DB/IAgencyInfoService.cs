﻿using AmaxIns.DataContract.AMAX30DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.AMAX30DB
{
    public interface IAgencyInfoService : IBaseService
    {
        Task<CountListForAgencyInfoResultModel> GetAgencyInfo(CountListAgencyQueryModel queryModel);
        Task<CountListForAgentInfoResultModel> GetAgentInfo(CountListAgentQueryModel countListAgentQueryModel);
        Task<CountListForClientInfoResultModel> GetClientInfo(CountListClientInfoQueryModel countListAgentQueryModel);
        Task<CountListForPaymentInfoResultModel> GetClientPaymentInfo(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel);


        // Demo
        Task<CountListForAgencyInfoResultModel> GetAgencyInfo_V1(CountListAgencyQueryModel queryModel);
        Task<CountListForAgentInfoResultModel> GetAgentInfo_V1(CountListAgentQueryModel countListAgentQueryModel);
        Task<CountListForClientInfoResultModel> GetClientInfo_V1(CountListClientInfoQueryModel countListAgentQueryModel);
        Task<CountListForPaymentInfoResultModel> GetClientPaymentInfo_V1(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel);
    }
}
