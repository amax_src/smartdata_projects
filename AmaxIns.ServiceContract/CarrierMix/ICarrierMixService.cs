﻿
using AmaxIns.DataContract.CarrierMix;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.CarrierMix
{
   public  interface ICarrierMixService:IBaseService
    {
        Task<IEnumerable<string>> GetCarrier(string Month,string year);
        Task<IEnumerable<string>> GetDates(string Month, string year);
        Task<IEnumerable<CarrierMixModel>> GetCarrierMixData(CarrierMixUserModel model);
    }
}
