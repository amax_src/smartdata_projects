﻿using AmaxIns.DataContract.Revenue;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Revenue
{
    public interface IRevenueService:IBaseService
    {
        Task<IEnumerable<RevenueModel>> GetRevenueData();
        Task<string> GetDataRefreshDate();
        Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData();
        Task<IEnumerable<TierPercentageModel>> GetTierPercentage();
        Task<IEnumerable<string>> Getmonths(string year);
    }
}
