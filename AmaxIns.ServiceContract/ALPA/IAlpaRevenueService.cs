﻿using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.ALPA
{
    public interface IAlpaRevenueService : IBaseService
    {
        Task<IEnumerable<RevenueModel>> GetAlpaRevenueData();
        Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData();
        Task<IEnumerable<DailyTransactionPayments>> DailyTransactionPayments(QuotesParam quotesParam, string month = null, string year = null);
        string ExportDailyTransactonPayment(IEnumerable<DailyTransactionPayments> list, string pathRoot);

    }
}
