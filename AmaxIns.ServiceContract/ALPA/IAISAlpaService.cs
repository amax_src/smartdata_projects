﻿using AmaxIns.DataContract.AIS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.ALPA
{
    public interface IAISAlpaService : IBaseService
    {
        Task<IEnumerable<AISModel>> GetAISData();
    }
}
