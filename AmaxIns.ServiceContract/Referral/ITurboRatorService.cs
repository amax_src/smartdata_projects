﻿using AmaxIns.DataContract.Referral.Turborator;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.Referral
{
    public interface ITurboratorService:IBaseService
    {
        void SaveData(TurboratorModel turboratorModel);
        IEnumerable<TurboratorModel> TurboratorReferralReport(int month, int year, int agencyId);
    }
}
