﻿using AmaxIns.DataContract.Referral;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.Referral
{
    public interface IReferralService : IBaseService
    {
        void AddReferral(ReferralModel referralModel, string emailtoCustomer, string emailtoReferal, string subject,string subjectRef);
        void AddReferralByAgent(AgentRefModel referralModel, string emailtoCustomer, string emailtoReferal,string subject,string subjectRef);
        IEnumerable<string> GetAgents();
        bool ValidateEmailAddress(string Email);
        bool ValidatePhoneNumber(string PhoneNumber);
        IEnumerable<ReferralReportModel> ReferralReport(int month, int year, string type, string zip, int agencyId);
        IEnumerable<CommentsModel> GetComments(int ReferenceId);
        void Addcomments(CommentsModel comments);
        bool ValidateZip(string zip);
        IEnumerable<ReferralReportModel> ReferralTouchedUnTouchedReport(int month, bool isTouched = false);

        void AddTurboratorcomments(CommentsModel comments);
        IEnumerable<CommentsModel> GetTurboratorComments(int turboraterId);

    }
}
