﻿using AmaxIns.DataContract.Referral.Language;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.Referral
{
    public interface ILanguageService : IBaseService
    {
        Task<IEnumerable<LanguageModel>> GetLanguage();
        Task<IEnumerable<ResourceValueModel>> GetLanguageResoure(int id, string section);
    }
}
