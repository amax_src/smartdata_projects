﻿using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.SearchFilter;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.Referral
{
    public interface IWinbackService : IBaseService
    {
        IEnumerable<WinbackModel> WinbackReport(List<int> month, int year, string monthstr, int agencyId, string searchKeyWork, int pageNumber, out int totalpage, string direction = "desc", string sortkeyword = "id", bool sorting = false);
        WinbackCommentWrapper GetComments(int ReferenceId);
        void Addcomments(WinbackCommentWrapper comments);
        int GetLatestMonthDataAvaliable(int agencyId);

        void UpdateWinBack(WinbackModel model, int agencyId);
        IEnumerable<WinbackModel> WinbackReportCsv(string monthstr, int year);
        void UpdateSearchFilter(SearchFilterModel search);
        IEnumerable<WinbackAPIModel> GetWinBackDataForAPI(int monthId, int year);
    }
}
