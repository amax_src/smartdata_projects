﻿using AmaxIns.DataContract.Referral.AgencyLogin;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.DataContract.Referral.Token;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.ServiceContract.Referral
{
   public interface IAgencyLoginService: IBaseService
    {
        AgencyLoginModel validateAgency(AgencyLoginModel model);
        AgentLoginModel validateAgent(AgentLoginModel model);
        SearchFilterModel GetSearchParameters(AgentLoginModel model);
        void UpdateToken(TokenModel token);
        bool ValidateToken(TokenModel model);
    }
}
