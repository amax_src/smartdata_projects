﻿using AmaxIns.DataContract.WorkingDays;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.ServiceContract.WorkingDays
{
    public interface IWorkingDaysService : IBaseService
    {
        Task<IEnumerable<WorkingdayModel>> GetWorkingDaysOfMonth();
    }
}
