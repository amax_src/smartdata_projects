﻿using Amax.Desktop.Services.Login;
using Amax.Desktop.Utility;
using Amax.Desktop.Views;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading;

namespace Amax.Desktop.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
       private SynchronizationContext synchronizationContext; 
        public RelayCommand LoginCommand { get; private set; }
        public event Action LoginDataRequested = delegate { };

        private string _userName;
        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                PropertyChange("UserName");
            }

        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                PropertyChange("Password");
            }

        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                _errorMessage = value;
                PropertyChange("ErrorMessage");
            }

        }
        ILoginService loginService;
        public LoginViewModel(ILoginService _loginService)
        {
            loginService = _loginService;
            synchronizationContext = SynchronizationContext.Current;
            LoginCommand = new RelayCommand(ValidateUser);
        }

        public void ValidateUser()
        {
            ErrorMessage = "";
            if (!string.IsNullOrWhiteSpace(UserName) && !string.IsNullOrWhiteSpace(UserName))
            {
                AsyncCall.Invoke(() =>
                {
                    var data = loginService.Validate(UserName, Password);
                    if (data == null)
                    {
                        ErrorMessage = "Invalid username and password";
                    }
                    else
                    {
                        synchronizationContext.Send(new SendOrPostCallback(o =>
                        {
                            Messenger.Default.Send(new NavigationMessageData() { NavigationTarget = ViewNames.BaseLayoutView });
                        }), null);

                    }
                });
            }
            else
            {
                ErrorMessage = "Username and password cannot left blank";
            }

        }


    }
}
