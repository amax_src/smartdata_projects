﻿using Amax.Desktop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.ViewModel
{
  public static  class AsyncCall
    {
        public static async void Invoke(Action action, bool showLoader = true)
        {
            if (showLoader)
                GlobalData.Default.LoaderVisibility = true;
            await Task.Run(action);
            if (showLoader)
                GlobalData.Default.LoaderVisibility = false;
        }
    }
}
