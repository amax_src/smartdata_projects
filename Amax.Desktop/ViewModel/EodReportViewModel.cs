﻿using Amax.Desktop.Model;
using Amax.Desktop.Model.LiveDataModels;
using Amax.Desktop.Services;
using Amax.Desktop.Services.Dashboard;
using Amax.Desktop.Services.EODReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Amax.Desktop.ViewModel
{
    public class EodReportViewModel : BaseViewModel
    {
        public RelayCommand RefreshDataCommad { get; private set; }
        public event Action RefreshDataRequested = delegate { };
        private string _quoteRatio;
        public string QuoteRatio
        {
            get => _quoteRatio;//((int)MtdPolicy + PolicyCount);
            set
            {
                _quoteRatio = value;
                PropertyChange("QuoteRatio");
            }

        }

        private int _differencePolicy;
        public int DifferencePolicy
        {
            get => _differencePolicy;//((int)MtdPolicy + PolicyCount);
            set
            {
                _differencePolicy = value;
                PropertyChange("DifferencePolicy");
            }

        }

        private double _differenceAgencyFee;
        public double DifferenceAgencyFee
        {
            get => _differenceAgencyFee;//((int)MtdPolicy + PolicyCount);
            set
            {
                _differenceAgencyFee = value;
                PropertyChange("DifferenceAgencyFee");
            }

        }

        private double _differencePremium;
        public double DifferencePremium
        {
            get => _differencePremium;//((int)MtdPolicy + PolicyCount);
            set
            {
                _differencePremium = value;
                PropertyChange("DifferencePremium");
            }

        }

        private int _mTDPolicyCalculation;
        public int MTDPolicyCalculation
        {
            get => _mTDPolicyCalculation;//((int)MtdPolicy + PolicyCount);
            set
            {
                _mTDPolicyCalculation = value;
                PropertyChange("MTDPolicyCalculation");
            }

        }

        private double _mTDPre;
        public double MTDPreCalculation
        {
            get => _mTDPre;//((int)MtdPolicy + PolicyCount);
            set
            {
                _mTDPre = value;
                PropertyChange("MTDPreCalculation");
            }

        }

        private double _mTDAfee;
        public double MTDAfeeCalculation
        {
            get => _mTDAfee;//((int)MtdPolicy + PolicyCount);
            set
            {
                _mTDAfee = value;
                PropertyChange("MTDAfeeCalculation");
            }

        }

        private List<Livedata> _livedateList;
        public List<Livedata> LivedateList
        {
            get => _livedateList;
            set
            {
                _livedateList = value;
                PropertyChange("LivedateList");
            }
        }

        private List<WalkoutCustomerModel> _walkoutCustomerModel;
        public List<WalkoutCustomerModel> WalkoutCustomerModel
        {
            get => _walkoutCustomerModel;
            set
            {
                _walkoutCustomerModel = value;
                PropertyChange("WalkoutCustomerModel");
            }
        }


        private Livedata _livedata;
        public Livedata Livedata
        {
            get => _livedata;
            set
            {
                _livedata = value;
                PropertyChange("Livedata");
            }
        }

        List<string> _agency;
        public List<string> Agency
        {
            get => _agency;
            set
            {
                _agency = value;
                PropertyChange("Agency");
            }

        }

        string _refreshTime;
        public string RefreshTime
        {
            get => _refreshTime;
            set
            {
                _refreshTime = value;
                PropertyChange("RefreshTime");
            }

        }

        public string _seletedAgency;
        public string SeletedAgency
        {
            get => _seletedAgency;
            set
            {
                _seletedAgency = value;
                Livedata = LivedateList.Where(m => m.AgencyName == _seletedAgency).FirstOrDefault();

                MTDPolicyCalculation = (int)Livedata.MtdPolicy + Livedata.PolicyCount;
                MTDPreCalculation = (int)Livedata.MtdPremium + Livedata.Premium;
                MTDAfeeCalculation = (int)Livedata.MtdAgencyFee + Livedata.AgencyFee;
                DifferenceAgencyFee = Livedata.AgencyFee - Livedata.TodayAgencyFee;
                DifferencePolicy = Livedata.PolicyCount - Livedata.TodayPolicyCount;
                DifferencePremium = Livedata.Premium - Livedata.TodayPremium;
                QuoteRatio = (Livedata.NewQuotes > 0 ? ((Livedata.PolicyCount * 100) / Livedata.NewQuotes) : 0) + "%";
                PropertyChange("SeletedAgency");
            }
        }

        IEodService _eodService;
        ISessionService _sessionService;
        public EodReportViewModel(IEodService eodService, ISessionService sessionService)
        {
            _eodService = eodService;
            _sessionService = sessionService;
            RefreshDataCommad = new RelayCommand(GetData);
            GetData();
        }

        public void GetData()
        {
           
            var UserData = _sessionService.GetUserData();
            var agencyIds= UserData.Agencies.Select(m => m.AgencyId).ToList();
            DashboardService dashboardService = new DashboardService();
            var z = dashboardService.GetQuotesDashboard(07, 2020, agencyIds);
            if (UserData != null)
            {
                Agency = UserData.Agencies.Select(m => m.AgencyName).ToList();
                AsyncCall.Invoke(() =>
               {
                   LivedateList = _eodService.GetData();
                   //Agency = LivedateList.Select(m => m.AgencyName).ToList();
                   //SeletedAgency = LivedateList.FirstOrDefault().AgencyName;
                   Livedata = LivedateList.FirstOrDefault();
                   RefreshTime = _eodService.GetRefeshTime();
                   MTDPolicyCalculation = (int)Livedata.MtdPolicy + Livedata.PolicyCount;
                   MTDPreCalculation = (int)Livedata.MtdPremium + Livedata.Premium;
                   MTDAfeeCalculation = (int)Livedata.MtdAgencyFee + Livedata.AgencyFee;

                   DifferenceAgencyFee = Livedata.AgencyFee - Livedata.TodayAgencyFee;
                   DifferencePolicy = Livedata.PolicyCount - Livedata.TodayPolicyCount;
                   DifferencePremium = Livedata.Premium - Livedata.TodayPremium;
                   QuoteRatio = (Livedata.NewQuotes > 0 ? ((Livedata.PolicyCount * 100) / Livedata.NewQuotes) : 0) + "%";

                   WalkoutCustomerModel = _eodService.GetWalkOutCustomer(SeletedAgency);
                   SeletedAgency = Agency.FirstOrDefault();
               });
            }
        }

    }
}
