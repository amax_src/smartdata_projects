﻿using Amax.Desktop.Model.Dashboard;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services.Dashboard
{
    public class DashboardService : BaseService, IDashboardService
    {
        public List<DashboardQuotesModel> GetQuotesDashboard(int month, int year, List<int> agencyid)
        {
            using (var client = new HttpClient())
            {
                List<DashboardQuotesModel> _data = new List<DashboardQuotesModel>();
                try
                {
                    var uri = APIURL + "Dashboard/GetQuotesDashboard/" + month + "/" + year;
                    var agencies = string.Join(", ", agencyid);
                    
                    HttpContent content = new StringContent("["+agencies+"]", Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", Token);
                    var result = client.PostAsync(uri, content).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        _data = JsonConvert.DeserializeObject<List<DashboardQuotesModel>>(result.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {

                }
                return _data;
            }
        }
    }
}