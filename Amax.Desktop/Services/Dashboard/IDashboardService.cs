﻿using Amax.Desktop.Model.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services.Dashboard
{
   public interface IDashboardService
    {
        List<DashboardQuotesModel> GetQuotesDashboard(int month, int year, List<int> agencyid);
    }
}
