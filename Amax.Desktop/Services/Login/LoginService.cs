﻿using Amax.Desktop.Model.LoginModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services.Login
{
    public class LoginService : BaseService,ILoginService
    {
        //DesktopLoginUser

        public DesktopLoginUser Validate(string userName, string password)
        {
            using (var client = new HttpClient())
            {
                DesktopLoginUser data = null;
                try
                {
                    var uri = APIURL + "Authentication/ValidateDesktop";
                    string json = JsonConvert.SerializeObject(new
                    {
                        UserName = userName,
                        Password = password
                    });

                    HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                    var result = client.PostAsync(uri, content).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        data = JsonConvert.DeserializeObject<DesktopLoginUser>(result.Content.ReadAsStringAsync().Result);
                        App.Current.Properties["LoginData"] = data;
                    }
                }
                catch (Exception ex)
                {

                }
                return data;
            }
        }
    }
}
