﻿using Amax.Desktop.Model.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services.Login
{
    public interface ILoginService
    {
        DesktopLoginUser Validate(string userName, string password);
    }
}
