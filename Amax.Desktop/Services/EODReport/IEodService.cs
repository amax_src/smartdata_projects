﻿using Amax.Desktop.Model.LiveDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services.EODReport
{
    public interface IEodService
    {
        List<Livedata> GetData();
        string GetRefeshTime();
        List<WalkoutCustomerModel> GetWalkOutCustomer(string Agency);
    }
}
