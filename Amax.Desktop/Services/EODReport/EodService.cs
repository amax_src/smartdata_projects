﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Amax.Desktop.Model.LiveDataModels;
using Newtonsoft.Json;

namespace Amax.Desktop.Services.EODReport
{
    public class EodService : BaseService, IEodService
    {
        public List<Livedata> GetData()
        {

            using (var client = new HttpClient())
            {
                List<Livedata> data = null;
                try
                {
                    var uri = APIURL + "Livedata";
                    var result = client.GetAsync(uri).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        data = JsonConvert.DeserializeObject<List<Livedata>>(result.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {

                }
                return data;
            }
        }


        public string GetRefeshTime()
        {

            using (var client = new HttpClient())
            {
                string data = null;
                try
                {
                    var uri = APIURL + "Livedata/GetRefreshTime";
                    var result = client.GetAsync(uri).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        data = result.Content.ReadAsStringAsync().Result;//JsonConvert.DeserializeObject<List<Livedata>>();
                    }
                }
                catch (Exception ex)
                {

                }
                return data;
            }
        }

        public List<WalkoutCustomerModel> GetWalkOutCustomer(string Agency)
        {
            using (var client = new HttpClient())
            {
                List<WalkoutCustomerModel> data = null;
                try
                {
                    var uri = "http://18.222.69.131:8080/api/LocationNotes";
                    string json = JsonConvert.SerializeObject(new
                    {
                        agencyName = Agency
                    });

                    HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                    var result = client.PostAsync(uri, content).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        data = JsonConvert.DeserializeObject<List<WalkoutCustomerModel>>(result.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {

                }
                return data;
            }
        }
    }
}
