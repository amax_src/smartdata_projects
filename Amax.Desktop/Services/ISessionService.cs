﻿using Amax.Desktop.Model.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services
{
    public interface ISessionService
    {
        DesktopLoginUser GetUserData();
    }
}
