﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amax.Desktop.Model.LoginModel;

namespace Amax.Desktop.Services
{
    public class SessionService : ISessionService
    {
        public DesktopLoginUser GetUserData()
        {
            var data = App.Current.Properties["LoginData"] as DesktopLoginUser;
            return data;
        }
    }
}
