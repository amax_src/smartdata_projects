﻿using Amax.Desktop.Model.LoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Services
{
    public class BaseService
    {
        protected string APIURL
        {
            get
            {
                return "http://18.216.112.189:8082/api/";
                //return "http://localhost:8081/api/";
            }
        }

        protected string Token
        {
            get
            {
                var data = App.Current.Properties["LoginData"] as DesktopLoginUser;
                return  data.Token;
            }
        }


    }
}
