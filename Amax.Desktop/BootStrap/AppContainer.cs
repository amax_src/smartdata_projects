﻿using Amax.Desktop.Services;
using Amax.Desktop.Services.Dashboard;
using Amax.Desktop.Services.EODReport;
using Amax.Desktop.Services.Login;
using Amax.Desktop.ViewModel;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.BootStrap
{
    public class AppContainer
    {
        public static IContainer _container;

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            //view Model
            builder.RegisterType<EodReportViewModel>();
            builder.RegisterType<LoginViewModel>();
            //Data Service
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<EodService>().As<IEodService>();
            builder.RegisterType<LoginService>().As<ILoginService>();
            builder.RegisterType<DashboardService>().As<IDashboardService>();

            
                        _container = builder.Build();
        }

        public static object Resolve(Type typeName)
        {
            return _container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
