﻿using Amax.Desktop.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Amax.Desktop.Views
{
    /// <summary>
    /// Interaction logic for EodReport.xaml
    /// </summary>
    public partial class EodReport : UserControl
    {
        public EodReport()
        {
            InitializeComponent();
            DataContext =new ViewModelLocator().EodReportViewModel;
            //var dep = AppContainer.Resolve<IEodService>();
            //DataContext = new EodReportViewModel(dep);
        }
    }
}
