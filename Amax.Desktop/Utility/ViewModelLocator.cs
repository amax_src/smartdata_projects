﻿using Amax.Desktop.BootStrap;
using Amax.Desktop.Services;
using Amax.Desktop.Services.EODReport;
using Amax.Desktop.Services.Login;
using Amax.Desktop.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Utility
{
   public  class ViewModelLocator
    {
        public LoginViewModel LoginViewModel {
            get {
                return new LoginViewModel(AppContainer.Resolve<ILoginService>());
            }
        }

        public EodReportViewModel EodReportViewModel
        {
            get
            {
                return new EodReportViewModel(AppContainer.Resolve<IEodService>(), AppContainer.Resolve<ISessionService>()); 
            }
        }
        //public  EodReportViewModel EodReportViewModel { get; set; } = new EodReportViewModel(AppContainer.Resolve<IEodService>(), AppContainer.Resolve<ISessionService>());

    }
}
