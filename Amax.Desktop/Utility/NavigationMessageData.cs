﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Utility
{
   public class NavigationMessageData
    {
        public ViewNames NavigationTarget { get; set; }
        public Dictionary<string, object> NavigationData { get; set; }
    }
}
