﻿using Amax.Desktop.Views;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Amax.Desktop.Utility
{
    public class ApplicationBase : Window
    {
        public LoginView loginView { get; set; }
        public BaseLayoutView _baseLayoutView { get; set; }
        public static MainWindow _MainWindow { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationBase"/> class.
        /// </summary>
        public ApplicationBase()
        {
            Messenger.Default.Register<NavigationMessageData>(this, NavigateWindowTo);
            Messenger.Default.Send(new NavigationMessageData() { NavigationTarget = ViewNames.LoginView });
        }

        public UserControl CurrentViewObject
        {
            get { return (UserControl)GetValue(CurrentViewObjectProperty); }
            set { SetValue(CurrentViewObjectProperty, value); }
        }

        public static readonly DependencyProperty CurrentViewObjectProperty =
            DependencyProperty.Register("CurrentViewObject", typeof(UserControl), typeof(ApplicationBase), new PropertyMetadata(null));

        /// <summary>
        /// Navigates the window automatic.
        /// </summary>
        /// <param name="messageData">The message data.</param>
        private void NavigateWindowTo(NavigationMessageData messageData)
        {
            switch (messageData.NavigationTarget)
            {
                case ViewNames.LoginView:
                    if (loginView == null)
                    {
                        loginView = new LoginView();
                        CurrentViewObject = loginView;
                        _MainWindow.Transition.LoadControl(new ContentControl { Content = CurrentViewObject }, Transition.Default);
                    }
                    break;
                case ViewNames.BaseLayoutView:
                    if (_baseLayoutView == null)
                    {
                        _baseLayoutView = new BaseLayoutView();
                        CurrentViewObject = _baseLayoutView;
                        _MainWindow.Transition.LoadControl(new ContentControl { Content = CurrentViewObject }, Transition.Default);
                    }
                    break;
                default:
                    break;
            }
        }
    }

}
