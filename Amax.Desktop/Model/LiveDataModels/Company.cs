﻿namespace Amax.Desktop.Model.LiveDataModels
{
    public class Company : BaseModel
    {
        private string _companyName;
        public string CompanyName {
            get =>_companyName;
            set {
                _companyName = value;
                RasePropertyChange("CompanyName");
            }
        }

        private int _count;
        public int Count { get=> _count;
            set
        {
                _count = value;
                RasePropertyChange("Count");
            }
        }
    }
}