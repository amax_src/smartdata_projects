﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Model.LiveDataModels
{
    public class Livedata : BaseModel
    {
        private string _agencyName;
        public string AgencyName
        {
            get => _agencyName;
            set
            {
                _agencyName = value;
                RasePropertyChange("AgencyName");
            }
        }

        private double _long;
        public double Long
        {
            get=> _long;
            set
            {
                _long = value;
                RasePropertyChange("Long");
            }
        }
        private double _lat;
        public double Lat
        {
            get=> _lat;
            set
            {
                _lat = value;
                RasePropertyChange("Lat");
            }
        }


        private double _premium;
        public double Premium
        {
            get=> _premium;
            set
            {
                _premium = value;
                RasePropertyChange("Premium");
            }
        }
        private int _policyCount;
        public int PolicyCount
        {
            get=> _policyCount;
            set
            {
                _policyCount = value;
                RasePropertyChange("PolicyCount");
            }
        }


        private double _agencyFee;
        public double AgencyFee
        {
            get=> _agencyFee;
            set
            {
                _agencyFee = value;
                RasePropertyChange("AgencyFee");
            }
        }


        private int _modifiedQuotes;
        public int ModifiedQuotes
        {
            get=> _modifiedQuotes;
            set
            {
                _modifiedQuotes = value;
                RasePropertyChange("ModifiedQuotes");
            }
        }

        private int _newQuotes;
        public int NewQuotes
        {
            get=> _newQuotes;
            set
            {
                _newQuotes = value;
                RasePropertyChange("NewQuotes");
            }
        }


        private double _todayPremium;
        public double TodayPremium
        {
            get=> _todayPremium;
            set
            {
                _todayPremium = value;
                RasePropertyChange("TodayPremium");
            }
        }

        private int _todayPolicyCount;
        public int TodayPolicyCount
        {
            get=> _todayPolicyCount;
            set
            {
                _todayPolicyCount = value;
                RasePropertyChange("TodayPolicyCount");
            }
        }

        private double _todayAgencyFee;
        public double TodayAgencyFee
        {
            get=> _todayAgencyFee;
            set
            {
                _todayAgencyFee = value;
                RasePropertyChange("TodayAgencyFee");
            }
        }

        private List<Agent> _agent;
        public List<Agent> Agent
        {
            get=> _agent;
            set
            {
                _agent = value;
                RasePropertyChange("Agent");
            }
        }

        private int _agentTotalPolicy;
        public int AgentTotalPolicy
        {
            get=> _agentTotalPolicy;
            set
            {
                _agentTotalPolicy = value;
                RasePropertyChange("AgentTotalPolicy");
            }
        }


        private Double _agentTotalPremium;
        public Double AgentTotalPremium
        {
            get=> _agentTotalPremium;
            set
            {
                _agentTotalPremium = value;
                RasePropertyChange("AgentTotalPremium");
            }

        }

        private Double _agentTotalAgencyFee;
        public Double AgentTotalAgencyFee
        {
            get => _agentTotalAgencyFee;
            set
            {
                _agentTotalAgencyFee = value;
                RasePropertyChange("AgentTotalAgencyFee");
            }
        }

        private int _agentTotalTrans;
        public int AgentTotalTrans
        {
            get=> _agentTotalTrans;
            set
            {
                _agentTotalTrans = value;
                RasePropertyChange("AgentTotalTrans");
            }
        }

        private int _endorsementCount;
        public int EndorsementCount
        {
            get=> _endorsementCount;
            set
            {
                _endorsementCount = value;
                RasePropertyChange("EndorsementCount");
            }
        }

        private double _cashLogTotal;
        public double CashLogTotal
        {
            get=> _cashLogTotal;
            set
            {
                _cashLogTotal = value;
                RasePropertyChange("CashLogTotal");
            }
        }

       
        public string Date { get { return DateTime.UtcNow.ToShortDateString(); } }

        private double _premiumGoal;
        public double PremiumGoal
        {
            get=> _premiumGoal;
            set
            {
                _premiumGoal = value;
                RasePropertyChange("PremiumGoal");
            }
        }

        private int _policyGoal;
        public int PolicyGoal
        {
            get=> _policyGoal;
            set
            {
                _policyGoal = value;
                RasePropertyChange("PolicyGoal");
            }
        }

        private double _agencyFeeGoal;
        public double AgencyFeeGoal
        {
            get=> _agencyFeeGoal;
            set
            {
                _agencyFeeGoal = value;
                RasePropertyChange("AgencyFeeGoal");
            }
        }

        private double _mtdPremium;
        public double MtdPremium
        {
            get => _mtdPremium;
            set
            {
                _mtdPremium = value;
                RasePropertyChange("MtdPremium");
            }
        }

        private double _mtdPolicy;
        public double MtdPolicy
        {
            get => _mtdPolicy;
            set
            {
                _mtdPolicy = value;
                RasePropertyChange("MtdPolicy");
            }

        }

        private double _mtdAgencyFee;
        public double MtdAgencyFee
        {
            get=> _mtdAgencyFee;
            set
            {
                _mtdAgencyFee = value;
                RasePropertyChange("MtdAgencyFee");
            }
        }

        private int _totalWorkingdays;
        public int TotalWorkingdays
        {
            get=> _totalWorkingdays;
            set
            {
                _totalWorkingdays = value;
                RasePropertyChange("TotalWorkingdays");
            }
        }

        private List<Company> _companies;
        public List<Company> Companies
        {
            get => _companies;
            set
            {
                _companies = value;
                RasePropertyChange("Companies");
            }
        }


        private int _totalCompaniesSale;
        public int TotalCompaniesSale
        {
            get=> _totalCompaniesSale;
            set
            {
                _totalCompaniesSale = value;
                RasePropertyChange("TotalCompaniesSale");
            }
        }

        private double _creditCardLog;
        public double CreditCardLog
        {
            get=> _creditCardLog;
            set
            {
                _creditCardLog = value;
                RasePropertyChange("CreditCardLog");
            }
        }

        private int _walkIn;
        public int WalkIn
        {
            get=> _walkIn;
            set
            {
                _walkIn = value;
                RasePropertyChange("WalkIn");
            }
        }

        private int _phoneInternet;
        public int PhoneInternet
        {
            get=> _phoneInternet;
            set
            { _phoneInternet = value;
                RasePropertyChange("PhoneInternet");

            }
        }

        private int _walkInSold;
        public int WalkInSold
        {
            get=> _walkInSold;
            set
            {
                _walkInSold = value;
                RasePropertyChange("WalkInSold");
            }
        }


        private int _phoneInternetSold;
        public int PhoneInternetSold
        {
            get=> _phoneInternetSold;
            set
            {
                _phoneInternetSold = value;
                RasePropertyChange("PhoneInternetSold");
            }
        }

        private int _remainingDays;
        public int RemainingDays
        {
            get=> _remainingDays;
            set
            {
                _remainingDays = value;
                RasePropertyChange("RemainingDays");
            }
        }

        private int _agentTotalcall;
        public int AgentTotalcall
        {
            get=> _agentTotalcall;
            set
            {
                _agentTotalcall = value;
                RasePropertyChange("AgentTotalcall");
            }
        }

        private AmPmDepositModel _aMPMDeposit;
        public AmPmDepositModel AMPMDeposit
        {
            get => _aMPMDeposit;
            set
            {
                _aMPMDeposit = value;
                RasePropertyChange("AMPMDeposit");
            }
        }

        private List<PaymentMethod> _paymentMethod;
        public List<PaymentMethod> PaymentMethod
        {
            get => _paymentMethod;
            set
            {
                _paymentMethod = value;
                RasePropertyChange("PaymentMethod");
            }
        }
       
    }


}
