﻿namespace Amax.Desktop.Model.LiveDataModels
{
    public class Agent : BaseModel
    {

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RasePropertyChange("Name");

            }
        }

        private int _policyCount;
        public int PolicyCount
        {
            get => _policyCount; set
            {
                _policyCount = value;
                RasePropertyChange("PolicyCount");
            }
        }

        private double _agencyFee;
        public double AgencyFee
        {
            get => _agencyFee;
            set
            {
                _agencyFee = value;
                RasePropertyChange("AgencyFee");
            }
        }

        private double _premium;
        public double Premium {
            get =>_premium;
            set {
                _premium = value;
                RasePropertyChange("Premium");
            }
        }

        private int _trans;
        public int Trans {
            get=>_trans;
            set {
                _trans = value;
                RasePropertyChange("Trans");
            }
        }

        private int _calls;
        public int Calls {
            get=>_calls;
            set {
                _calls = value;
                RasePropertyChange("Calls");
            } }
    }
}