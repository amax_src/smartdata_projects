﻿namespace Amax.Desktop.Model.LiveDataModels
{
    public class AmPmDepositModel : BaseModel
    {
        private string _agencyName;
        public string AgencyName
        {
            get => _agencyName;
            set
            {
                _agencyName = value;
                RasePropertyChange("AgencyName");
            }
        }
        private double _amDeposit;
        public double AmDeposit
        {
            get => _amDeposit;
            set
            {
                _amDeposit = value;
                RasePropertyChange("AmDeposit");
            }
        }

        private double _pmDeposit;
        public double PmDeposit
        {
            get => _pmDeposit;
            set
            {
                _pmDeposit = value;
                RasePropertyChange("PmDeposit");
            }
        }
    }
}