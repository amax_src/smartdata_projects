﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Model.LiveDataModels
{
    public class WalkoutCustomerModel : BaseModel
    {

        private string _customer_last_name;
        public string Customer_last_name {
            get=> _customer_last_name;
            set
            {
                _customer_last_name = value;
                RasePropertyChange("Customer_last_name");
            }
        }

        private string _agent_name;
        public string Agent_name {
            get=>_agent_name;
            set
            {
                _agent_name = value;
                RasePropertyChange("Agent_name");
            }
        }

        private string _note;
        public string Note {
            get=> _note;
            set
            {
                _note = value;
                RasePropertyChange("Note");
            }
        }

        private string _agent_Location;
        public string Agent_Location {
            get=> _agent_Location;
            set
            {
                _agent_Location = value;
                RasePropertyChange("Agent_Location");
            }
        }
    }
}
