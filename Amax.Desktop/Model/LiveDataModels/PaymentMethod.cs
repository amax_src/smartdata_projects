﻿namespace Amax.Desktop.Model.LiveDataModels
{
    public class PaymentMethod : BaseModel
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RasePropertyChange("Name");

            }
        }

        public double _amount;
        public double Amount
        {
            get => _amount;
            set
            {
                _amount = value;
                RasePropertyChange("Amount");
            }
        }
    }
}