﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amax.Desktop.Model.Dashboard
{
    public  class DashboardQuotesModel
    {

        public int agencyID { get; set; }
        public string location { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public int newQuotes { get; set; }
        public decimal closingRatioNewQuotes { get; set; }
        public decimal closingRatioModQuotes { get; set; }
        public string groupName { get; set; }

    }
}
