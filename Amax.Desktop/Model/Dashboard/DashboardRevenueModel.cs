﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amax.Desktop.Model.Dashboard
{
    public class DashboardRevenueModel
    {

        public int AgencyID { get; set; }
        public string Location { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public decimal PerDiffPolicies { get; set; }
        public decimal PerDiffAFee { get; set; }
        public decimal PerDiffPremium { get; set; }
        public int SortMonthNum { get; set; }
        public string  GroupName { get; set; }

    }
}
