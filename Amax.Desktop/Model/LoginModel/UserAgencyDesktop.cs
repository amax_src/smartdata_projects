﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amax.Desktop.Model.LoginModel
{
    public class UserAgencyDesktop
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
    }
}
