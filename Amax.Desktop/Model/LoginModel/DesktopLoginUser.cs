﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amax.Desktop.Model.LoginModel
{
    public class DesktopLoginUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public List<UserFeatureDesktop> Features { get; set; }
        public List<UserAgencyDesktop> Agencies { get; set; }
    }
}
