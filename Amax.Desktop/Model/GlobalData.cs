﻿using Amax.Desktop.Model.LiveDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amax.Desktop.Model
{
    public class  GlobalData: BaseModel
    {
        private GlobalData()
        {

        }

        private bool _loaderVisibility;
        public  bool LoaderVisibility
        {
            get=>_loaderVisibility;
            set {
                _loaderVisibility = value;
                RasePropertyChange("LoaderVisibility");
            }
        }

        private static GlobalData m_Instance;
        public static GlobalData Default
        {
            get
            {
                return m_Instance ?? (m_Instance = new GlobalData());
            }
        }
    }
}
