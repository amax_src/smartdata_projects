#pragma checksum "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "17106f8b142d4aa47e9c61c8e1d859f91f176275"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_ApolloCommissions), @"mvc.1.0.view", @"/Views/Home/ApolloCommissions.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/ApolloCommissions.cshtml", typeof(AspNetCore.Views_Home_ApolloCommissions))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\_ViewImports.cshtml"
using AmaxCarrierInsProUI;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"17106f8b142d4aa47e9c61c8e1d859f91f176275", @"/Views/Home/ApolloCommissions.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4406e890718122f8d989c2dbc3e043622946def9", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_ApolloCommissions : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<AmaxCarrierInsProUI.Models.ApolloCommissionPagedModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(62, 395, true);
            WriteLiteral(@"<div class=""content-area"">
    <div class=""card"">
        <div class=""card-body"">
            <h5 class=""card-title"">Apollo Commissions</h5>

            <div class=""row searchbar mb-5"">
                <div class=""col-lg-4 col-md-4 col-sm-12"">
                    <div class=""row align-items-center"">
                        <div class=""col-md-6 col-sm-4"">
                            ");
            EndContext();
            BeginContext(458, 139, false);
#line 11 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                       Write(Html.DropDownList("AgencyId", ViewBag.Locations as IEnumerable<SelectListItem>, htmlAttributes: new { Class = "form-control", id = "loc" }));

#line default
#line hidden
            EndContext();
            BeginContext(597, 1216, true);
            WriteLiteral(@"
                        </div>
                        <div class=""col-sm-6 col-md-4"">
                            <input type=""button"" class=""btn btn-primary"" value=""Select Location"" onclick=""Search(0)"" />
                        </div>
                    </div>
                </div>
            </div>

            <input type=""hidden"" name=""currentPageIndex"" value=""1"" />
            <div class=""table-responsive table-outer-wrap"">

                <table class=""table"">
                    <thead class=""thead-light"">
                        <tr>


                            <th>Location </th>
                            <th>Name         </th>
                            <th>Policy  </th>
                            <th>Premium  </th>
                            <th>Comm Due  </th>
                            <th>Comm Paid </th>
                            <th>Effective </th>
                            <th>Paid Premium   </th>
                            <th>Prem Paid in Month</th");
            WriteLiteral(">\r\n                            <th>Rate  </th>\r\n                            <th>Type         </th>\r\n\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n");
            EndContext();
#line 43 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                         foreach (AmaxIns.DataContract.DataScraping.ApolloMGM.ApolloCommissionsModel apollo in Model.apolloCommissionsModels)
                        {


#line default
#line hidden
            BeginContext(1985, 70, true);
            WriteLiteral("                            <tr>\r\n                                <td>");
            EndContext();
            BeginContext(2056, 15, false);
#line 47 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Location);

#line default
#line hidden
            EndContext();
            BeginContext(2071, 44, true);
            WriteLiteral(" </td>\r\n                                <td>");
            EndContext();
            BeginContext(2116, 11, false);
#line 48 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Name);

#line default
#line hidden
            EndContext();
            BeginContext(2127, 52, true);
            WriteLiteral("         </td>\r\n                                <td>");
            EndContext();
            BeginContext(2180, 13, false);
#line 49 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Policy);

#line default
#line hidden
            EndContext();
            BeginContext(2193, 45, true);
            WriteLiteral("  </td>\r\n                                <td>");
            EndContext();
            BeginContext(2239, 14, false);
#line 50 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Premium);

#line default
#line hidden
            EndContext();
            BeginContext(2253, 45, true);
            WriteLiteral("  </td>\r\n                                <td>");
            EndContext();
            BeginContext(2299, 14, false);
#line 51 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.CommDue);

#line default
#line hidden
            EndContext();
            BeginContext(2313, 45, true);
            WriteLiteral("  </td>\r\n                                <td>");
            EndContext();
            BeginContext(2359, 15, false);
#line 52 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.CommPaid);

#line default
#line hidden
            EndContext();
            BeginContext(2374, 44, true);
            WriteLiteral(" </td>\r\n                                <td>");
            EndContext();
            BeginContext(2419, 16, false);
#line 53 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Effective);

#line default
#line hidden
            EndContext();
            BeginContext(2435, 44, true);
            WriteLiteral(" </td>\r\n                                <td>");
            EndContext();
            BeginContext(2480, 18, false);
#line 54 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.PaidPremium);

#line default
#line hidden
            EndContext();
            BeginContext(2498, 46, true);
            WriteLiteral("   </td>\r\n                                <td>");
            EndContext();
            BeginContext(2545, 22, false);
#line 55 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.PremPaidinMonth);

#line default
#line hidden
            EndContext();
            BeginContext(2567, 43, true);
            WriteLiteral("</td>\r\n                                <td>");
            EndContext();
            BeginContext(2611, 11, false);
#line 56 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Rate);

#line default
#line hidden
            EndContext();
            BeginContext(2622, 45, true);
            WriteLiteral("  </td>\r\n                                <td>");
            EndContext();
            BeginContext(2668, 11, false);
#line 57 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                               Write(apollo.Type);

#line default
#line hidden
            EndContext();
            BeginContext(2679, 85, true);
            WriteLiteral("         </td>\r\n                                \r\n                            </tr>\r\n");
            EndContext();
#line 60 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"

                        }

#line default
#line hidden
            BeginContext(2793, 250, true);
            WriteLiteral("                    </tbody>\r\n                </table>\r\n            </div>\r\n\r\n\r\n            <div class=\"pagination-custom mt-4\">\r\n                <nav aria-label=\"Page navigation\">\r\n                    <ul class=\"pagination justify-content-center\">\r\n");
            EndContext();
#line 70 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                         for (int i = 1; i <= Model.PageCount; i++)
                        {

#line default
#line hidden
            BeginContext(3139, 52, true);
            WriteLiteral("                            <li class=\"page-item\">\r\n");
            EndContext();
#line 73 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                 if (i != Model.CurrentPageIndex)
                                {

#line default
#line hidden
            BeginContext(3293, 56, true);
            WriteLiteral("                                    <a class=\"page-link\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3349, "\"", 3383, 3);
            WriteAttributeValue("", 3356, "javascript:PagerClick(", 3356, 22, true);
#line 75 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
WriteAttributeValue("", 3378, i, 3378, 2, false);

#line default
#line hidden
            WriteAttributeValue("", 3380, ",);", 3380, 3, true);
            EndWriteAttribute();
            BeginContext(3384, 68, true);
            WriteLiteral(">\r\n                                        <span aria-hidden=\"true\">");
            EndContext();
            BeginContext(3453, 1, false);
#line 76 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                                            Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(3454, 71, true);
            WriteLiteral("</span>\r\n                                        <span class=\"sr-only\">");
            EndContext();
            BeginContext(3526, 1, false);
#line 77 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                                         Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(3527, 51, true);
            WriteLiteral("</span>\r\n                                    </a>\r\n");
            EndContext();
#line 79 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                }
                                else
                                {

#line default
#line hidden
            BeginContext(3686, 79, true);
            WriteLiteral("                                    <span aria-hidden=\"true\" class=\"page-link\">");
            EndContext();
            BeginContext(3766, 1, false);
#line 82 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                                                          Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(3767, 9, true);
            WriteLiteral("</span>\r\n");
            EndContext();
#line 83 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                                }

#line default
#line hidden
            BeginContext(3811, 35, true);
            WriteLiteral("                            </li>\r\n");
            EndContext();
#line 85 "D:\Chandan Sharma\AmaxCarrierInsProUI\Views\Home\ApolloCommissions.cshtml"
                        }

#line default
#line hidden
            BeginContext(3873, 694, true);
            WriteLiteral(@"


                    </ul>
                </nav>
            </div>
        </div>

    </div>
</div>

<script src=""https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"" type=""text/javascript""></script>
<script type=""text/javascript"">
    function PagerClick(index, x) {
        var x = $(""#loc"").val();
        window.location.href = ""/Home/ApolloCommissions?currentPageIndex="" + index + ""&AgencyId="" + x;
    }


    function Search(currentPageIndex) {
        var x = $(""#loc"").val();
        //var currentPageIndex = 1;
        window.location.href = ""/Home/ApolloCommissions?currentPageIndex="" + currentPageIndex + ""&AgencyId="" + x;
    }
</script>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<AmaxCarrierInsProUI.Models.ApolloCommissionPagedModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
