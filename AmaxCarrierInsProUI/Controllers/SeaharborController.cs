﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.ServiceContract.DataScraping.Carrier;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AmaxCarrierInsProUI.Controllers
{
    public class SeaharborController : Controller
    {
        ICarrierLiveService _Service;
        IConfiguration _configuration;
        public SeaharborController(ICarrierLiveService Service, IConfiguration configuration)
        {
            _Service = Service;
            _configuration = configuration;
        }
        public IActionResult Index()
        {

            var data = _Service.GetSeaharborLiveData();
            ViewBag.IPdate = _Service.GetSeaharborIPData();
            return View(data);
        }
    }
}