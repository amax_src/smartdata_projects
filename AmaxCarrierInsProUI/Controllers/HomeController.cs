﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AmaxCarrierInsProUI.Models;
using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.Models.ApolloMGM;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.DataScraping.ApolloMGM;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace AmaxCarrierInsProUI.Controllers
{
    public class HomeController : Controller
    {
        IApolloMgmService _Service;
        IConfiguration _configuration;
        IAgencyService _agencyService;

        public HomeController(IApolloMgmService Service, IConfiguration configuration, IAgencyService agencyService)
        {
            _Service = Service;
            _configuration = configuration;
            _agencyService = agencyService;
        }

        public IActionResult Index([FromQuery(Name = "currentPageIndex")] int currentPageIndex, [FromQuery(Name = "AgencyId")] int AgencyId)

       {
            if (currentPageIndex == 0)
            {
                currentPageIndex = 1;
            }
            if (AgencyId == 0)
            {
                AgencyId = 112;
            }

            List<AgencyModel> locations = new List<AgencyModel>();
            locations = _agencyService.Get().Result.ToList();
            ViewBag.Locations = GetSelectListItems(locations);
            ViewBag.selectedloc = 112;
            var data = this.GetApollos(currentPageIndex, AgencyId);
            return View(data);
        }
        public IActionResult ApolloPoliciesExpiring([FromQuery(Name = "currentPageIndex")] int currentPageIndex, [FromQuery(Name = "AgencyId")] int AgencyId)
        {

            if (currentPageIndex == 0)
            {
                currentPageIndex = 1;
            }
            if (AgencyId == 0)
            {
                AgencyId = 112;
            }
            List<AgencyModel> locations = new List<AgencyModel>();
            locations = _agencyService.Get().Result.ToList();
            ViewBag.Locations = GetSelectListItems(locations);
            ViewBag.selectedloc = 112;
            var data = this.GetApolloDataExpiring(currentPageIndex, AgencyId);
            return View(data);
        }

        public IActionResult ApolloDatadueDate([FromQuery(Name = "currentPageIndex")] int currentPageIndex, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            if (currentPageIndex == 0)
            {
                currentPageIndex = 1;
            }
            if (AgencyId == 0)
            {
                AgencyId = 112;
            }
            List<AgencyModel> locations = new List<AgencyModel>();
            locations = _agencyService.Get().Result.ToList();
            ViewBag.Locations = GetSelectListItems(locations);
            ViewBag.selectedloc = 112;
            var data = this.GetApolloDatadueDate(currentPageIndex, AgencyId);
            return View(data);
        }
        public IActionResult ApolloCommissions([FromQuery(Name = "currentPageIndex")] int currentPageIndex, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            if (currentPageIndex == 0)
            {
                currentPageIndex = 1;
            }
            if (AgencyId == 0)
            {
                AgencyId = 112;
            }
            List<AgencyModel> locations = new List<AgencyModel>();
            locations = _agencyService.Get().Result.ToList();
            ViewBag.Locations = GetSelectListItems(locations);
            ViewBag.selectedloc = 112;
            var data = this.GetApolloCommissions(currentPageIndex, AgencyId);
            return View(data);
        }
        
        //Consolidated Apollo Data
        private ApollosMgmPagedModel GetApollos(int currentPage, int AgencyId)
        {

            int TotalRecords = 0;
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);
            var model = new ApollosMgmPagedModel();
            var data = _Service.GetApolloData(currentPage, out TotalRecords, AgencyId).ToList().OrderBy(m => m.DueDate);
            foreach (var x in data)
            {
                model.apollosMgmModels.Add(new ApolloMgmModel

                {
                   
                    Paydate = x.Paydate,
                    Applicant = x.Applicant,
                    Expiration = x.Expiration,
                    Effective = x.Effective,
                    Description = x.Description,
                    NamedInsured = x.NamedInsured,
                    PayAmount = x.PayAmount,
                    Category = x.Category,
                    PolicyExpDate = x.PolicyExpDate,
                    PayBalance = x.PayBalance,
                    PayFee = x.PayFee,
                    Payment = x.Payment,
                    PayMethod = x.PayMethod,
                    PayPaid = x.PayPaid,
                    PayTotal = x.PayTotal,
                    Policy = x.Policy,
                    Policyeffdate = x.Policyeffdate,
                    Policynum = x.Policynum,
                    PostedDate = x.PostedDate,
                    AmountDue = x.AmountDue,
                    Carrier=x.Carrier,
                    DueDate = Convert.ToDateTime(x.DueDate),
                    Location = x.Location,
                    Email = x.Email,
                    Phone = x.Phone

                });

            }
            model.TotalRecords = TotalRecords;
            double pageCount = (double)((decimal)model.TotalRecords / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);
            model.CurrentPageIndex = currentPage;
            return (model);
        }
        //SelectList for drop down
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<AgencyModel> agencyModels)
        {
            var selectList = new List<SelectListItem>();
            foreach (var element in agencyModels)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element.AgencyId.ToString(),
                    Text = element.AgencyName
                });
            }
            return selectList;
        }
        //Policies expiring in next month 
        private ApollosMgmPagedModel GetApolloDataExpiring(int currentPage, int AgencyId)
        {
            int TotalRecords = 0;
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);

            var model = new ApollosMgmPagedModel();
            var data = _Service.GetApolloDataExpiring(currentPage, out TotalRecords, AgencyId).ToList();
            foreach (var x in data)
            {
                model.apollosMgmModels.Add(new ApolloMgmModel
                {
                    Carrier = x.Carrier,
                    Paydate = x.Paydate,
                    Applicant = x.Applicant,
                    Expiration = x.Expiration,
                    Effective = x.Effective,
                    Description = x.Description,
                    NamedInsured = x.NamedInsured,
                    PayAmount = x.PayAmount,
                    Category = x.Category,
                    PolicyExpDate = x.PolicyExpDate,
                    PayBalance = x.PayBalance,
                    PayFee = x.PayFee,
                    Payment = x.Payment,
                    PayMethod = x.PayMethod,
                    PayPaid = x.PayPaid,
                    PayTotal = x.PayTotal,
                    Policy = x.Policy,
                    Policyeffdate = x.Policyeffdate,
                    Policynum = x.Policynum,
                    PostedDate = x.PostedDate,
                    AmountDue = x.AmountDue,
                    DueDate = Convert.ToDateTime(x.DueDate),
                    Location = x.Location,
                    Email = x.Email,
                    Phone = x.Phone

                });

            }
            model.TotalRecords = TotalRecords;
            double pageCount = (double)((decimal)model.TotalRecords / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);
            model.CurrentPageIndex = currentPage;
            return (model);
        }
        //Policieswith due date in next 20 days 
        private ApollosMgmPagedModel GetApolloDatadueDate(int currentPage, int AgencyId)
        {
            int TotalRecords = 0;
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);

            var model = new ApollosMgmPagedModel();
            var data = _Service.GetApolloDatadueDate(currentPage, out TotalRecords, AgencyId).ToList();
            foreach (var x in data)
            {
                model.apollosMgmModels.Add(new ApolloMgmModel
                {

                    Paydate = x.Paydate,
                    Applicant = x.Applicant,
                    Expiration = x.Expiration,
                    Effective = x.Effective,
                    Description = x.Description,
                    NamedInsured = x.NamedInsured,
                    PayAmount = x.PayAmount,
                    Category = x.Category,
                    PolicyExpDate = x.PolicyExpDate,
                    PayBalance = x.PayBalance,
                    PayFee = x.PayFee,
                    Payment = x.Payment,
                    PayMethod = x.PayMethod,
                    PayPaid = x.PayPaid,
                    PayTotal = x.PayTotal,
                    Policy = x.Policy,
                    Policyeffdate = x.Policyeffdate,
                    Policynum = x.Policynum,
                    PostedDate = x.PostedDate,
                    AmountDue = x.AmountDue,
                   Carrier=x.Carrier,
                    DueDate = Convert.ToDateTime(x.DueDate),
                    Location = x.Location,
                    Email = x.Email,
                    Phone = x.Phone

                });

            }
            model.TotalRecords = TotalRecords;
            double pageCount = (double)((decimal)model.TotalRecords / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);
            model.CurrentPageIndex = currentPage;
            return (model);
        }

        private ApolloCommissionPagedModel GetApolloCommissions(int currentPage, int AgencyId)
        {
            int TotalRecords = 0;
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);

            var model = new ApolloCommissionPagedModel();
            var data = _Service.GetApolloCommissions(currentPage, out TotalRecords, AgencyId).ToList();
            foreach (var x in data)
            {
                model.apolloCommissionsModels.Add(new ApolloCommissionsModel
                {
                    AgencyID=x.AgencyID,
                    CommDue=x.CommDue,
                    CommPaid=x.CommPaid,
                    Effective=x.Effective,
                    Location=x.Location,
                    Name=x.Name,
                    PaidPremium=x.PaidPremium,
                    Policy=x.Policy,
                    Premium=x.Premium,
                    PremPaidinMonth=x.PremPaidinMonth,
                    Rate=x.Rate,
                    TotalCommission=x.TotalCommission,
                    Type=x.Type 

                });

            }
            model.TotalRecords = TotalRecords;
            double pageCount = (double)((decimal)model.TotalRecords / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);
            model.CurrentPageIndex = currentPage;
            return (model);
        }

    }
}
