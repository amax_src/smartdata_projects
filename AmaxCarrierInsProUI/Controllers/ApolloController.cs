﻿using AmaxIns.ServiceContract.DataScraping.ApolloMGM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AmaxCarrierInsProUI.Controllers
{
    public class ApolloController : Controller
    {
        IApolloPaymentService _Service;
        IConfiguration _configuration;
        //
        public ApolloController(IApolloPaymentService Service, IConfiguration configuration)
        {
            _Service = Service;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            var data = _Service.GetApolloLiveData();
            ViewBag.IPdate = _Service.GetApolloIPData();
            return View(data);
        }
    }
}