﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxCarrierInsProUI.Models;
using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.DataScraping.Carrier;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace AmaxCarrierInsProUI.Controllers
{
    public class AspenController : Controller
    {
        ICarrierLiveService _Service;
        IConfiguration _configuration;
        IAgencyService _agencyService;
        public AspenController(ICarrierLiveService Service, IConfiguration configuration, IAgencyService agencyService)
        {
            _Service = Service;
            _agencyService = agencyService;
            _configuration = configuration;
        }
        public IActionResult Index()
        {

            var data = _Service.GetAspenLiveData();
            ViewBag.IPdate = _Service.GetAspenIPData();
            return View(data);
        }
    }
}