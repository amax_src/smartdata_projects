﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxCarrierInsProUI.Models
{
    public class ApolloCommissionPagedModel
    {
        public ApolloCommissionPagedModel()
        {
            apolloCommissionsModels = new List<ApolloCommissionsModel>();
        }
        public List<ApolloCommissionsModel> apolloCommissionsModels { get; set; }

        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
        public int TotalRecords { get; set; }
        public object apollosMgmModels { get; internal set; }
    }
}
