﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using System.Collections.Generic;

namespace AmaxIns.Models.ApolloMGM
{
    public class ApollosMgmPagedModel
    {
        public ApollosMgmPagedModel()
        {
            apollosMgmModels = new List<ApolloMgmModel>();
        }
        public List<ApolloMgmModel> apollosMgmModels { get; set; }
        
        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
        public int TotalRecords { get; set; }

    }
}
