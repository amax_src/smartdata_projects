﻿using AmaxIns.DataContract.DataScraping.Aspen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxCarrierInsProUI.Models
{
    public class AspenLivePagedModel
    {
        public AspenLivePagedModel()
        {
            aspenLiveModels = new List<AspenLiveModel>();
        }

     
        public List<AspenLiveModel> aspenLiveModels { get; set; }

        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
        public int TotalRecords { get; set; }
    }
}
