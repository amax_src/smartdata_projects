﻿using AmaxIns.Repository.Agency;
using AmaxIns.Repository.DataScraping.ApolloMGM;
using AmaxIns.Repository.DataScraping.Aspen;
using AmaxIns.Repository.DataScraping.Livedate;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.Aspen;
using AmaxIns.RepositoryContract.DataScraping.Livedate;
using AmaxIns.Service.Agency;
using AmaxIns.Service.DataScraping.ApolloMGM;
using AmaxIns.Service.DataScraping.Carrier;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.DataScraping.ApolloMGM;
using AmaxIns.ServiceContract.DataScraping.Carrier;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AmaxCarrierInsProUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            ConfigureDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Aspen}/{action=Index}/{id?}");
            });
        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<IApolloMgmService, ApolloMgmService>();
            services.AddScoped<IApolloMgmRepo, ApolloMgmRepo>();
            services.AddTransient<IAgencyService, AgencyService>();
            services.AddScoped<IAgencyRepository, AgencyRepository>();
            services.AddScoped<ICarrierLiveRepository, CarrierLiveRepository>();
            services.AddTransient<ICarrierLiveService, CarrierLiveService>();
            services.AddScoped<ILiveDataPaymentRepository, LiveDataPaymentRepository>();
            services.AddTransient<IApolloPaymentService, ApolloPaymentService>();
            services.AddScoped<IApolloPaymentRepository, ApolloPaymentRepository>();
            services.AddSingleton<IConfiguration>(Configuration);
        }
    }
}
