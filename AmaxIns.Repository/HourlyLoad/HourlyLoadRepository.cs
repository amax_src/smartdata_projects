﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.Quotes;
using AmaxIns.RepositoryContract.HourlyLoad;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace AmaxIns.Repository.HourlyLoad
{
    public class HourlyLoadRepository : BaseRepository, IHourlyLoadRepository
    {
        public HourlyLoadRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<IEnumerable<HourlyLoadModel>> GetData(string date, int agencyId)
        {
            IEnumerable<HourlyLoadModel> result = null;
            var query = "[spSyncDB_GetHourlyRevenueData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
            result = await GetAsync<HourlyLoadModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<string>> GetDates(int month, string year)
        {

            var query = "select distinct cast(paydate as varchar(15))paydate from[Sync_DB].[dbo].[tblConsolidatedRevenuePerHour] where month(paydate)=@month and year(paydate)=@year order by cast(paydate as varchar(15))";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<string>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<MonthModel>> GetMonth(string year)
        {
            var query = "select distinct datename(month,paydate)Name,month(paydate)Id from[Sync_DB].[dbo].[tblConsolidatedRevenuePerHour] where year(paydate)=@year order by month(paydate) desc";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", year, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<MonthModel>(query, parameter,commandType: CommandType.Text);
            return result;


        }
        //// Hourly Load For new and modifed quotes
        public async Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string date, int agencyId)
        {
            IEnumerable<HourlyNewModifiedQuotes> result = new List<HourlyNewModifiedQuotes>();
            var query = @"[spSyncDB_GetHourlyNewandModifiedQuotes_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
            result = await GetAsync<HourlyNewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<HourlyLoadModel>> GetData(string startdate,string enddate)
        {
            IEnumerable<HourlyLoadModel> result = null;
            var query = "[spSyncDB_GetHourlyRevenueDataforExcel_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@startdate", startdate, DbType.String, ParameterDirection.Input);
            parameter.Add("@enddate", enddate, DbType.String, ParameterDirection.Input);
            result = await GetAsync<HourlyLoadModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string startdate, string enddate)
        {
            IEnumerable<HourlyNewModifiedQuotes> result = new List<HourlyNewModifiedQuotes>();
            var query = @"[spSyncDB_GetHourlyNewandModifiedQuotesforExcel_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@startdate", startdate, DbType.String, ParameterDirection.Input);
            parameter.Add("@enddate", enddate, DbType.String, ParameterDirection.Input);
            result = await GetAsync<HourlyNewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<PeakHoursModel>> GetPeakHoursData(string startdate, string enddate)
        {
            IEnumerable<PeakHoursModel> result = new List<PeakHoursModel>();
            var query = @"[spSyncDB_GetPeakHours_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@sdate", startdate, DbType.String, ParameterDirection.Input);
            parameter.Add("@edate", enddate, DbType.String, ParameterDirection.Input);
            result = await GetAsync<PeakHoursModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgentCountModel>> GetAgentCountPerhour(string date, int agencyId)
        {
            IEnumerable<AgentCountModel> result = new List<AgentCountModel>();
            var query = @"[spSyncDB_GetAgency_Hours_Agents_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
            result = await GetAsync<AgentCountModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgentCountModel>> GetAgentCountPerhour(string startdate, string enddate)
        {
            IEnumerable<AgentCountModel> result = new List<AgentCountModel>();
            var query = @"[spSyncDB_GetAgency_Hours_Agents_Excel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@startdate", startdate, DbType.String, ParameterDirection.Input);
            parameter.Add("@enddate", enddate, DbType.String, ParameterDirection.Input);
            result = await GetAsync<AgentCountModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}
