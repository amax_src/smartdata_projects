﻿using AmaxIns.DataContract.Agency;
using AmaxIns.RepositoryContract.Agency;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Agency
{
    public class AgencyRepository : BaseRepository, IAgencyRepository
    {
        public AgencyRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<AgencyModel>> Get()
        {
            var query = "[spSyncDB_GetAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<AgencyModel>> GetSaleOfDirector(int SaleDirectorId)
        {
            var query = "[spSyncDB_GetAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@SDID", SaleDirectorId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<AgencyModel>> Get(int RegionalManagerId)
        {
            var query = "[spSyncDB_GetAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", RegionalManagerId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetbyZonalManager(int ZonalManager)
        {
            var query = "[spSyncDB_GetAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@ZMID", ZonalManager, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> Get(int RegionalManagerId, int ZonalManagerId)
        {
            var query = "[spSyncDB_GetAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", RegionalManagerId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@ZMID", ZonalManagerId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyLatLong>> GetAgencyAndLatLong()
        {
            //var query = "select agencyName as AgencyName from [IP_AMAX30]..[agencyInfo] where deleted=0  and AgencyName <>'Haltom City'";
            var query = "[spSyncDB_GetAllAgency_sel]";
            var result = await GetAsync<AgencyLatLong>(query, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetBSMAgency(int BsmId)
        {

            var query = "select AgencyId,AgencyName,bsm.isactive as IsActive  from[Sync_DB].[dbo].[tblBSMInfo] bs join[Sync_DB].[dbo].[tblBSMAgencyMapping] bsm on  bs.id=bsm.bsmid where bs.id=@Id and bsm.isactive=1";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", BsmId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id)
        {

            var query = "select AgencyId,AgencyName,Isactive from DirectoreyStoreManageAgencyMapping where id=@Id and isactive=1";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }
    }
}
