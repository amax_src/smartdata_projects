﻿using AmaxIns.DataContract.Referral.Language;
using AmaxIns.RepositoryContract.Referral;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Referral
{
   public class LanguageRepository : BaseRepository, ILanguageRepository
    {

        public LanguageRepository(IConfiguration configuration) : base(configuration)
        {

        }
        public async Task<IEnumerable<LanguageModel>> GetLanguage()
        {
            IEnumerable<LanguageModel> result = null;
            var query = "select Id, [LanguageName] as Language, [IsActive] from [dbo].[tblLanguages] where isactive=1";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<LanguageModel>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<ResourceValueModel>> GetLanguageResoure(int id,string section )
        {
            IEnumerable<ResourceValueModel> result = null;
            var query = "select * from tblLanguageResources where languageid=@id and pageid in (@section,@Common)";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@id", id, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@section", section, DbType.String, ParameterDirection.Input);
            parameter.Add("@Common", "common", DbType.String, ParameterDirection.Input);
            result = await GetAsync<ResourceValueModel>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }
    }
}
