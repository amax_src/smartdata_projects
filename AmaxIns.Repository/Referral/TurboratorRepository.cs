﻿using AmaxIns.DataContract.Referral.Turborator;
using AmaxIns.RepositoryContract.Referral;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AmaxIns.Repository.Referral
{
    public class TurboratorRepository : BaseRepository, ITurboratorRepository
    {
        IConfiguration _configuration;
        public TurboratorRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }

        public int SaveAddress2Data(Address2 model, int TurboratorID, int DriverId)
        {
            int Address2Id = 0;
            var query = @"spSyncDB_Referred_Turborator_Address2_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorID", TurboratorID);
            parameter.Add("@DriverId", DriverId);
            parameter.Add("@Address", model.Address1);
            parameter.Add("@City", model.City);
            parameter.Add("@State", model.State);
            parameter.Add("@ZipCode", model.ZipCode);
            parameter.Add("@Address2Id", Address2Id, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("Address2Id");
        }

        public int SaveAddressData(AddressModel model, int CustomerID, int TurboratorId)
        {
            int AddressId = 0;
            var query = @"spSyncDB_Referred_Turborator_Address_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorId);
            parameter.Add("@CustomerID", CustomerID);
            parameter.Add("@Address", model.Address1);
            parameter.Add("@City", model.City);
            parameter.Add("@State", model.State);
            parameter.Add("@ZipCode", model.ZipCode);
            parameter.Add("@AddressId", AddressId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("AddressId");
        }

        public int SaveCarData(CarModel model, int TurboratorId)
        {

            int CarId = 0;
            var query = @"spSyncDB_Referred_Turborator_Car_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorId);
            parameter.Add("@Vin", model.Vin);
            parameter.Add("@Year", model.Year);
            parameter.Add("@Maker", model.Maker);
            parameter.Add("@Model", model.Model);
            parameter.Add("@Liab", model.Liab);
            parameter.Add("@CarId", CarId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("CarId");
        }

        public int SaveCoverageData(CoverageModel model, int TurboratorId)
        {
            int CoverageId = 0;
            var query = @"spSyncDB_Referred_Turborator_Coverage_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorId);
            parameter.Add("@CoverageCd", model.CoverageCd);
            parameter.Add("@Val1", model.Val1);
            parameter.Add("@Val2", model.Val2);
            parameter.Add("@Val3", model.Val3);
            parameter.Add("@CoverageId", CoverageId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("CoverageId");

        }

        public int SaveCustomerData(CustomerModel model, int TurboratorId)
        {
            int CustomerId = 0;
            var query = @"spSyncDB_Referred_Turborator_Customer_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorId);
            parameter.Add("@Name", model.Name);
            parameter.Add("@Surname", model.Surname);
            parameter.Add("@BirthDate", model.BirthDate);
            parameter.Add("@DriverLicenseNumber", model.DriverLicenseNumber);
            parameter.Add("@Email", model.Email);
            parameter.Add("@PhoneNumber", model.PhoneNumber);
            parameter.Add("@CustomerId", CustomerId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("CustomerId");
        }

        public int SaveDriverData(DriverModel model, int TurboratorID)
        {
            int DriverId = 0;
            var query = @"spSyncDB_Referred_Turborator_Driver_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorID);
            parameter.Add("@FirstName", model.FirstName);
            parameter.Add("@LastName", model.LastName);
            parameter.Add("@Relation", model.Relation);
            parameter.Add("@DriverLicenseNumber", model.DriverLicenseNumber);
            parameter.Add("@Dob", model.Dob);
            parameter.Add("@Gender", model.Gender);
            parameter.Add("@MaritalStatus", model.MaritalStatus);
            parameter.Add("@IsDefaultInsured", model.IsDefaultInsured);
            parameter.Add("@PriorInsurance", model.PriorInsurance);
            parameter.Add("@DriverId", DriverId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("DriverId");
        }

        public int SaveGaragingAddressData(GaragingAddress model, int TurboratorID, int CarId)
        {
            int GaragingAddressId = 0;
            var query = @"spSyncDB_Referred_Turborator_GaragingAddress_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorId", TurboratorID);
            parameter.Add("@CarId", CarId);
            parameter.Add("@Address", model.Address1);
            parameter.Add("@City", model.City);
            parameter.Add("@State", model.State);
            parameter.Add("@ZipCode", model.ZipCode);

            parameter.Add("@GaragingAddressId", GaragingAddressId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("GaragingAddressId");
        }

        public int SaveTurboratorData(TurboratorModel model)
        {
            int TurboratorId = 0;
            var query = @"spSyncDB_Referred_Turborator_mod";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Agencyid", model.Agencyid);
            parameter.Add("@Term", model.Term);
            parameter.Add("@QuoteNumber", model.QuoteNumber);
            parameter.Add("@QuoteUrl", model.QuoteUrl);
            parameter.Add("@TransactionId", model.TransactionId);
            parameter.Add("@TrQuoteUrl", model.TrQuoteUrl);
            parameter.Add("@CustomerGuId", model.CustomerId);
            parameter.Add("@StoreId", model.StoreId);
            parameter.Add("@EventType", model.EventType);
            parameter.Add("@CarrierName", model.CarrierName);
            parameter.Add("@QuoteRate", model.QuoteRate);
            parameter.Add("@TurboratorId", TurboratorId, DbType.Int32, direction: ParameterDirection.Output);
            Add(query, parameter, commandType: CommandType.StoredProcedure);
            return parameter.Get<int>("TurboratorId");
        }

        public IEnumerable<TurboratorModel> TurboratorReferralReport(int month, int year, int agencyId)
        {
            IEnumerable<TurboratorModel> data;
            List<TurboratorModel> turborators = new List<TurboratorModel>();
            var query = @"[spSyncDB_Get_Referral_Turborator_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@agencyId", agencyId, DbType.String, ParameterDirection.Input);
            turborators = Get<TurboratorModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure).AsList();
            data = from turborator in turborators group turborator by turborator.TurboratorID into groups select groups.OrderByDescending(r => r.TurboraterReferencecommemtsId).First();
            return data;
        }

        public IEnumerable<CoverageModel> TurboratorCoverage(int id)
        {
            IEnumerable<CoverageModel> data;
            var query = @"select* from [Sync_DB].[dbo].[tblReferred_Turborator_Coverage] where TurboratorId =@Id";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", id, DbType.Int32, ParameterDirection.Input);
            data = Get<CoverageModel>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return data;
        }
    }
}
