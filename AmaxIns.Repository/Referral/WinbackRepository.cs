﻿using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.RepositoryContract.Referral;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Referral
{
    public class WinbackRepository : BaseRepository,
 IWinbackRepository
    {
        IConfiguration _configuration;
        public WinbackRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }

        // Change SP
        public IEnumerable<WinbackModel> WinbackReport(List<int> month, int year, string monthstr, int agencyId, string searchKeyWord, string direction = "desc", string sortkeyword = "id", int pageNumber = 1, bool sorting = false)
        {
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            var month_str = string.Join(",", month).Replace("'", "''");
            if (sorting == true)
            {
                month_str = monthstr;
            }

            IEnumerable<WinbackModel> data = null;
            var query = @"[spSyncDB_Get_Referral_Winback_sel_dev]";
            //var query = @"[spSyncDB_Get_Referral_Winback_sel_new]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", month_str, DbType.String, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@agencyId", agencyId, DbType.String, ParameterDirection.Input);
            parameter.Add("@pageNumber", pageNumber, DbType.String, ParameterDirection.Input);
            if (!string.IsNullOrWhiteSpace(searchKeyWord))
            {
                parameter.Add("@searchKeyWork", searchKeyWord, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(direction))
            {
                parameter.Add("@direction", direction, DbType.String, ParameterDirection.Input);

            }
            if (!string.IsNullOrWhiteSpace(sortkeyword))
            {
                parameter.Add("@sortkeyword", sortkeyword, DbType.String, ParameterDirection.Input);

            }
            data = Get<WinbackModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            return data;
        }

        // Change Table
        public IEnumerable<WinbackModel> WinbackReportCsv(string monthstr, int year)
        {
            IEnumerable<WinbackModel> data = null;
            var query = @"select CustomerName,Carrier,PolicyNumber,Email,Phone,Quoted,Sold,CallAnswered,DateTransaction,AgentId,
                          Updatedate,AdditionalComment,CustomerLeaveComment,LeaveUs,NoAnswerCallback,InsuranceProStatus,agencyName
                          from ReferalApp_Winback_Dev
                          where Month(DateTransaction) in (select ParameterValue from [fnUTIL_DelimitedStringToTable_sel](',',@Month))  and year(DateTransaction)=@Year order by id desc";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", monthstr, DbType.String, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            data = Get<WinbackModel>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return data;
        }

        // Change Table
        public int WinbackReportPageCount(List<int> month, int year, string monthstr, int agencyId, string search, bool sorting = false)
        {
            var month_str = string.Join(",", month).Replace("'", "''");
            if(sorting == true)
            {
                month_str = monthstr;
            }
            DynamicParameters parameter = new DynamicParameters();
            string query = @"SELECT Count(id) AS TotalRows from [ReferalApp_Winback_Dev] where Month(DateTransaction) in (select ParameterValue from [fnUTIL_DelimitedStringToTable_sel](',',@Month)) and year(DateTransaction) = @Year ";
            int totalPage = 0;
            if (!(agencyId == -1 || agencyId == 24))
            {
                query = @"SELECT Count(id) AS TotalRows from [ReferalApp_Winback_Dev] where agencyid = @agencyid and Month(DateTransaction) in (select ParameterValue from [fnUTIL_DelimitedStringToTable_sel](',',@Month)) and year(DateTransaction) = @Year ";
            }

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query + " and ( customername like '%" + search + "%' or phone like '%" + search + "%' or PolicyNumber like '%" + search + "%')";
            }

            parameter.Add("@Month", month_str, DbType.String, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@agencyId", agencyId, DbType.String, ParameterDirection.Input);
            totalPage = GetFirstOrDefault<int>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return totalPage;
        }

        // Change SP
        public void Addcomments(WinbackCommentWrapper comments)
        {
            try
            {
                //var query = "[spSyncDB_Comments_Winback_mod]";
                var query = "[spSyncDB_Comments_Winback_mod_dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@ReferenceId", comments.ReferalApp_WinbackId, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencyId", comments.AgencyId, DbType.String, ParameterDirection.Input);
                parameter.Add("@Agentid", comments.AgentId, DbType.String, ParameterDirection.Input);
                parameter.Add("@AdditionalComment", comments.AdditionalComment, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerLeaveComment", comments.CustomerLeaveComment, DbType.String, ParameterDirection.Input);
                parameter.Add("@FollowEmail", comments.FollowEmail, DbType.Boolean, ParameterDirection.Input);
                parameter.Add("@Quoted", comments.Quoted, DbType.Boolean, ParameterDirection.Input);
                parameter.Add("@Sold", comments.Sold, DbType.Boolean, ParameterDirection.Input);
                parameter.Add("@Text", comments.Text, DbType.Boolean, ParameterDirection.Input);
                parameter.Add("@CallAnswered", comments.CallAnswered, DbType.Boolean, ParameterDirection.Input);
                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Done
        public IEnumerable<WinbackCommentsModel> GetComments(int ReferenceId)
        {
            IEnumerable<WinbackCommentsModel> data;
            var query = @"sp_GetWinback_Commemts_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@ReferenceId", ReferenceId, DbType.Int32, ParameterDirection.Input);
            data = Get<WinbackCommentsModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return data;
        }

        public WinbackCommentWrapper GetCommentWrapper(int ReferenceId)
        {
            WinbackCommentWrapper data;
            var query = @"select [CustomerLeaveComment],[FollowEmail],[Quoted],[Sold],[Text],[CallAnswered] from [ReferalApp_Winback_Dev] where id = @Id ";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", ReferenceId, DbType.Int32, ParameterDirection.Input);
            data = GetFirstOrDefault<WinbackCommentWrapper>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return data;
        }


        // Change Table Name
        public int GetLatestMonthDataAvaliable(int agencyId)
        {
            string query = @"SELECT top 1 Month(DateTransaction) AS Month from [ReferalApp_Winback_Dev] where year(DateTransaction)=2021 order by DateTransaction desc";
            int month = 0;
            if (!(agencyId == -1 || agencyId == 24))
            {
                query = @"SELECT top 1 Month(DateTransaction) AS Month from [ReferalApp_Winback_Dev] where agencyid = @agencyid and year(DateTransaction) = 2021 order by DateTransaction desc ";
            }
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.String, ParameterDirection.Input);
            month = GetFirstOrDefault<int>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return month;
        }

        // Change Table Name
        public void UpdateWinBack(WinbackModel model, int agencyId)
        {
            model.LeaveUs = model.LeaveUs == null || model.LeaveUs == "Select option" ? null : model.LeaveUs;
            var query = @"update [ReferalApp_Winback_Dev] set CustomerName = @customerName, [Phone] = @Phone,[Quoted] = @Quoted, [Sold] = @Sold, [CallAnswered] = @CallAnswered, 
                        AgentId = @AgentId, LeaveUs = @LeaveUs, NoAnswerCallback = @NoAnswerCallback, EditedByAgency = @EditedByAgency, Updatedate = DATEADD(HOUR, -1, getdate()) where id = @ReferenceId ";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@customerName", model.CustomerName, DbType.String, ParameterDirection.Input);
            parameter.Add("@Phone", model.Phone, DbType.String, ParameterDirection.Input);
            parameter.Add("@Sold", model.Sold, DbType.Boolean, ParameterDirection.Input);
            parameter.Add("@CallAnswered", model.CallAnswered, DbType.Boolean, ParameterDirection.Input);
            parameter.Add("@Quoted", model.Quoted, DbType.Boolean, ParameterDirection.Input);
            parameter.Add("@AgentId", model.AgentId, DbType.String, ParameterDirection.Input);
            parameter.Add("@LeaveUs", model.LeaveUs, DbType.String, ParameterDirection.Input);
            parameter.Add("@NoAnswerCallback", model.NoAnswerCallback, DbType.Boolean, ParameterDirection.Input);
            parameter.Add("@EditedByAgency", agencyId, DbType.String, ParameterDirection.Input);
            parameter.Add("@ReferenceId", model.Id, DbType.Int32, ParameterDirection.Input);
            Add(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
        }

        public void UpdateSearchFilter(SearchFilterModel search)
        {
            try
            {
                var query = "[spSyncDB_UpdateWinback_SearchHistory]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@AgentId", search.AgentId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@SearchParameter", search.SearchParameter, DbType.String, ParameterDirection.Input);
                parameter.Add("@SortParameter", search.SortParameter, DbType.String, ParameterDirection.Input);
                parameter.Add("@SortDirection", search.SortDirection, DbType.String, ParameterDirection.Input);
                parameter.Add("@SearchMonth", search.SearchMonth, DbType.String, ParameterDirection.Input);
                parameter.Add("@SearchYear", search.SearchYear, DbType.String, ParameterDirection.Input);
                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<WinbackAPIModel> GetWinBackDataForAPI(int monthId,int year)
        {
            IEnumerable<WinbackAPIModel> data;
            var query = @"GetWinBackDataForAPI";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@monthId", monthId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            data = Get<WinbackAPIModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return data;
        }
    }
}
