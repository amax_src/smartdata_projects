﻿using AmaxIns.DataContract.Referral;
using AmaxIns.RepositoryContract.Referral;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AmaxIns.Repository.Referral
{
    public class ReferralRepository : BaseRepository, IRefferalRepository
    {
        public ReferralRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public int AddReferral(ReferralModel referralModel)
        {
            try
            {
                int assginedAgency = 0;
                var query = "[spSyncDB_ReferredBy_mod]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserName", referralModel.CustomerName, DbType.String, ParameterDirection.Input);
                parameter.Add("@UserEmail", referralModel.CustomerEmail, DbType.String, ParameterDirection.Input);
                parameter.Add("@UserPhone", referralModel.CustomerPhone, DbType.String, ParameterDirection.Input);
                parameter.Add("@ZipCode", referralModel.ZipCode, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerName", referralModel.RefName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerEmail", referralModel.RefEmail, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerPhone", referralModel.RefPhone, DbType.String, ParameterDirection.Input);
                parameter.Add("@AsginedToAgency", DbType.Int32, direction: ParameterDirection.Output);

                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                assginedAgency = parameter.Get<int>("AsginedToAgency");
                return assginedAgency;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddReferralByAgent(AgentRefModel referralModel)
        {
            try
            {
                int assginedAgency = 0;
                var query = "[spSyncDB_ReferredBy_mod]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserName", referralModel.CustomerName, DbType.String, ParameterDirection.Input);
                parameter.Add("@UserEmail", referralModel.CustomerEmail, DbType.String, ParameterDirection.Input);
                parameter.Add("@UserPhone", referralModel.CustomerPhone, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgentId", referralModel.AgentId, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerName", referralModel.RefName, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerEmail", referralModel.RefEmail, DbType.String, ParameterDirection.Input);
                parameter.Add("@CustomerPhone", referralModel.RefPhone, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencyId", referralModel.AgencyId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@AsginedToAgency", DbType.Int32, direction: ParameterDirection.Output);
                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                assginedAgency = parameter.Get<int>("AsginedToAgency");
                return assginedAgency;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<string> GetAgents()
        {
            IEnumerable<string> Agent;
            var query = @"select p.AgentId from 
                           (Select  
	                        Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int) as AgentId
                            from IP_AMAX30.[dbo].[agentInfo]
                            where agentactive = 1	and 
	                        Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int)!= 0  
	                        group by Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int)) p ";
            DynamicParameters parameter = new DynamicParameters();
            Agent = Get<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return Agent;
        }

        public string ValidateEmailAddress(string Email)
        {
            string email;
            var query = @"select CustomerEmail from [dbo].[tblReferredCustomer] where CustomerEmail=@Email";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Email", Email, DbType.String, ParameterDirection.Input);
            email = GetFirstOrDefault<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return email;
        }

        public string ValidatePhoneNumber(string PhoneNumber)
        {
            string phonenumber;
            var query = @"select CustomerPhone from [dbo].[tblReferredCustomer] where CustomerPhone=@Phone";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Phone", PhoneNumber, DbType.String, ParameterDirection.Input);
            phonenumber = GetFirstOrDefault<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return phonenumber;
        }

        public string ValidateZip(string zip)
        {
            string zipCode;
            var query = @"select zip from [dbo].[LatitudesandLogitudes] where Zip =@zip";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@zip", zip, DbType.String, ParameterDirection.Input);
            zipCode = GetFirstOrDefault<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return zipCode;
        }


        public IEnumerable<ReferralReportModel> ReferralReport(int month, int year, string type, string zip, int? agencyId)
        {
            IEnumerable<ReferralReportModel> data;
            List<ReferralReportModel> referralReportModels = new List<ReferralReportModel>();
            var query = @"[spSyncDB_Get_ReferralDetails_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            if (agencyId.HasValue)
            {
                parameter.Add("@agencyId", agencyId.Value, DbType.String, ParameterDirection.Input);
            }
            referralReportModels = Get<ReferralReportModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure).AsList();
            data = from referral in referralReportModels group referral by referral.referrelId into groups select groups.OrderByDescending(r => r.ReferencecommemtsId).First();
            return data;
        }


        public void Addcomments(CommentsModel comments)
        {
            try
            {
                var query = "[spSyncDB_Comments_mod]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@ReferenceId", comments.ReferenceId, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencyId", comments.AgencyId, DbType.String, ParameterDirection.Input);
                parameter.Add("@Comments", comments.Comments, DbType.String, ParameterDirection.Input);


                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CommentsModel> GetComments(int ReferenceId)
        {
            IEnumerable<CommentsModel> data;

            var query = @"sp_GetReferencecommemts_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@ReferenceId", ReferenceId, DbType.Int32, ParameterDirection.Input);
            data = Get<CommentsModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return data;
        }

        public IEnumerable<string> GetAgencyEmail(string agencyIds)
        {
            IEnumerable<string> data;

            var query = @"select e.email from  AgencyEmails e where e.AgencyId in(@agencyId)";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyIds, DbType.String, ParameterDirection.Input);
            data = Get<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return data;
        }

        public IEnumerable<ReferralReportModel> ReferralTouchedUnTouchedReport(int month, bool isTouched = false)
        {
            IEnumerable<ReferralReportModel> data;
            // var query = @"spSyncDB_Get_ReferralDetails_sel";
            var query = @"[spSyncDB_Get_ReferralDetails_Untouched_touched_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Istouched", isTouched, DbType.Boolean, ParameterDirection.Input);
            data = Get<ReferralReportModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return data;
        }


        public void AddTurboratorcomments(CommentsModel comments)
        {
            try
            {
                var query = "[spSyncDB_TurboraterReferencecommemts_mod]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@TurboraterReferenceId", comments.ReferenceId, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencyId", comments.AgencyId, DbType.String, ParameterDirection.Input);
                parameter.Add("@Comments", comments.Comments, DbType.String, ParameterDirection.Input);


                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CommentsModel> GetTurboratorComments( int turboraterId)
        {
            IEnumerable<CommentsModel> data;
            var query = @"sp_GetTurboRatorReferencecommemts_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@TurboratorReferenceId", turboraterId, DbType.Int32, ParameterDirection.Input);
            data = Get<CommentsModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return data;
        }

    }
}


