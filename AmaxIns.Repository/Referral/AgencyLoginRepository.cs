﻿
using AmaxIns.DataContract.Referral.AgencyLogin;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.RepositoryContract.Referral;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;

namespace AmaxIns.Repository.Referral
{
    public class AgencyLoginRepository : BaseRepository, IAgencyLoginRepository
    {
        public AgencyLoginRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public AgencyLoginModel validateAgency(AgencyLoginModel model)
        {
            var query = "Select AgencyId,AgencyLogin,Password,Zip FROM AgencyLogin where AgencyLogin=@AgencyLogin and Password=@Password";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@AgencyLogin", model.AgencyLogin, DbType.String, ParameterDirection.Input);
            parameter.Add("@Password", model.Password, DbType.String, ParameterDirection.Input);
            var result = GetFirstOrDefault<AgencyLoginModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public AgentLoginModel validateAgent(AgentLoginModel model)
        {
            var query = "select * from [IP_AMAX30]..[agentInfo] where SUBSTRING(agentName, 0, CHARINDEX(':',agentName)) = @AgentID and agentActive = 1";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@AgentID", model.LoginAgentID, DbType.String, ParameterDirection.Input);
            var result = GetFirstOrDefault<AgentLoginModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public SearchFilterModel GetSearchParameters(AgentLoginModel model)
        {
            var query = "select top 1 * from [Sync_DB]..[ReferalApp_Winback_SearchHistory] where AgentId = @AgentID";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@AgentID", model.LoginAgentID, DbType.Int32, ParameterDirection.Input);
            var result = GetFirstOrDefault<SearchFilterModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public void UpdateToken(TokenModel token)
        {
            try
            {
                var query = "[spSyncDB_UpdateToken]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@AgentId", token.AgentId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@Token", token.Token, DbType.String, ParameterDirection.Input);
                Add(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TokenModel GetToken(TokenModel model)
        {
            var query = "select top 1 * from [Sync_DB]..[ReferalApp_LoginTrack] where AgentId = @AgentID order by UpdatedDate desc";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@AgentID", model.AgentId, DbType.Int32, ParameterDirection.Input);
            var result = GetFirstOrDefault<TokenModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }

    }
}
