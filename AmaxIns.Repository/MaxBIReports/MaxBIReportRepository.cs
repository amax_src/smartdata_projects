﻿using AmaxIns.DataContract.MaxBIReports;
using AmaxIns.RepositoryContract.MaxBIReports;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Repository.MaxBIReports
{
    public class MaxBIReportRepository : BaseRepository, IMaxBIReportRepository
    {
        public MaxBIReportRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<CatalogInfo>> GetCatalogs(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@catalogFrom", request.CatalogFrom, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@catalogType", request.CatalogType, DbType.Int16, ParameterDirection.Input);
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<CatalogInfo>(DBConstants.MaxBIReport_GetCatalogs, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        public async Task<IEnumerable<HierarchyInfo>> GetHierarchyLevel(HierarchyRequest request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Level", request.Level, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@LevelId", request.LevelId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@FilterBy", request.FilterBy, DbType.Int16, ParameterDirection.Input);
            if (!string.IsNullOrEmpty(request.FilterByIds))
            {
                parameter.Add("@FilterByIds", request.FilterByIds, DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<HierarchyInfo>(DBConstants.MaxBIReport_GetHierarchyLevel, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        #region Carrier Agents
        public async Task<IEnumerable<AgentCarrierAFeePremium>> GetAgentCarrierAFeePremium(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);

            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.PayTypes.Count > 0)
            {
                parameter.Add("@payTypes", string.Join(",", request.PayTypes), DbType.String, ParameterDirection.Input);
            }
            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<AgentCarrierAFeePremium>(DBConstants.MaxBIReport_CarrierAgent, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        #endregion

        #region Agency Per Policy Reports
        public async Task<IEnumerable<AgencyPerPolicy>> GetAFeePremiumPerPolicy(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.PayTypes.Count > 0)
            {
                parameter.Add("@payTypes", string.Join(",", request.PayTypes), DbType.String, ParameterDirection.Input);
            }
            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<AgencyPerPolicy>(DBConstants.MaxBIReport_AFeePremiumPerPolicy, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        public async Task<decimal> GetAFeePremiumPerPolicySummary(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@reportType", request.ReportType.GetHashCode(), DbType.Int32, ParameterDirection.Input);

            if (request.HierarchyIds.Count > 0)
            {
                parameter.Add("@hierarchyIds", string.Join(",", request.HierarchyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.ReportBy.HasValue)
            {
                parameter.Add("@reportBy", request.ReportBy.Value, DbType.Int16, ParameterDirection.Input);
            }
            if (request.PayTypes.Count > 0)
            {
                parameter.Add("@payTypes", string.Join(",", request.PayTypes), DbType.String, ParameterDirection.Input);
            }
            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            var result = await ExecuteScalarAsync<decimal>(DBConstants.MaxBIReport_AFeePremiumPerPolicySummary, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        #endregion

        #region New / Modified Quotes
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotes(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@quoteType", request.QuoteType, DbType.String, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.ReportBy.HasValue)
            {
                parameter.Add("@reportBy", request.ReportBy.Value, DbType.Int16, ParameterDirection.Input);
            }

            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            string sp = request.IsClosingRatio ? DBConstants.MaxBIReport_NewModifiedQuotesClosingRatio : DBConstants.MaxBIReport_NewModifiedQuotes;
            var result = await GetAsync<NewModifiedQuotes>(sp, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        public async Task<decimal> GetNewModifiedQuotesSummary(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@quoteType", request.QuoteType, DbType.String, ParameterDirection.Input);

            if (request.HierarchyIds.Count > 0)
            {
                parameter.Add("@hierarchyIds", string.Join(",", request.HierarchyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.ReportBy.HasValue)
            {
                parameter.Add("@reportBy", request.ReportBy.Value, DbType.Int16, ParameterDirection.Input);
            }

            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }
            string sp = request.IsClosingRatio ? DBConstants.MaxBIReport_NewModifiedQuotesClosingRatioSummary : DBConstants.MaxBIReport_NewModifiedQuotesSummary;
            var result = await ExecuteScalarAsync<decimal>(sp, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPRGraph(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@quoteType", request.QuoteType, DbType.String, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }

            string sp = "API_MaxBIReport_NewModifiedQuotesClosingRatioGraphForEPR";
            var result = await GetAsync<NewModifiedQuotes>(sp, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        #endregion

        #region Inbound / Outbound Calls
        public async Task<IEnumerable<InboundOutboundCalls>> GetInboundOutboundCalls(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@callType", request.CallType, DbType.String, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }

            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<InboundOutboundCalls>(DBConstants.MaxBIReport_InboundOutboundCalls, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        public async Task<decimal> GetInboundOutboundCallsSummary(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@callType", request.CallType, DbType.String, ParameterDirection.Input);

            if (request.HierarchyIds.Count > 0)
            {
                parameter.Add("@hierarchyIds", string.Join(",", request.HierarchyIds), DbType.String, ParameterDirection.Input);
            }
            if (request.ReportBy.HasValue)
            {
                parameter.Add("@reportBy", request.ReportBy.Value, DbType.Int16, ParameterDirection.Input);
            }
            if (request.Dates.Count > 0)
            {
                var dates = request.Dates.Select(d => d.ToString("yyyy-MM-dd"));
                parameter.Add("@dates", string.Join(",", dates), DbType.String, ParameterDirection.Input);
            }

            var result = await ExecuteScalarAsync<decimal>(DBConstants.MaxBIReport_InboundOutboundCallsSummary, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        #endregion

        #region Payroll Dashboard
        public async Task<IEnumerable<PayrollFilterData>> GetPayrollDashboardFilterData(PayrollRequest request)
        {
            DynamicParameters parameter = new DynamicParameters();
            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<PayrollFilterData>(DBConstants.MaxBIReport_GetPayrollDashboardFilterData, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        public async Task<IEnumerable<DataContract.MaxBIReports.Payroll>> GetPayrollDashboardData(PayrollRequest request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@month", request.Month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@year", request.Year, DbType.Int32, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.PayPeriods.Count > 0)
            {
                parameter.Add("@payPeriods", string.Join("|", request.PayPeriods), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<DataContract.MaxBIReports.Payroll>(DBConstants.MaxBIReport_GetPayrollDashboardData, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
        #endregion

        public async Task<IEnumerable<EmployeeInfo>> GetmployeeSensitiveInfo(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<EmployeeInfo>(DBConstants.MaxBIReport_GetmployeeSensitiveInfo, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        public async Task<IEnumerable<HourlyProduction>> GetAgentHourlyProduction(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);

            int agentId = 0;
            if (request.AgentIds.Count > 0)
            {
                agentId = Convert.ToInt32(request.AgentIds[0]);
            }
            parameter.Add("@agentId", agentId, DbType.Int32, ParameterDirection.Input);

            var result = await GetAsync<HourlyProduction>(DBConstants.MaxBIReport_GetAgentHourlyProduction, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPR(Request request)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@fromDate", request.FromDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@toDate", request.ToDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@quoteType", request.QuoteType, DbType.String, ParameterDirection.Input);

            if (request.AgentIds.Count > 0)
            {
                parameter.Add("@agentIds", string.Join(",", request.AgentIds), DbType.String, ParameterDirection.Input);
            }
            if (request.AgencyIds.Count > 0)
            {
                parameter.Add("@agencyIds", string.Join(",", request.AgencyIds), DbType.String, ParameterDirection.Input);
            }

            string sp = request.IsClosingRatio ? DBConstants.MaxBIReport_NewModifiedQuotesClosingRatioForEPR : DBConstants.MaxBIReport_NewModifiedQuotesForEPR;
            var result = await GetAsync<NewModifiedQuotes>(sp, parameter, commandType: CommandType.StoredProcedure, commandTimeout: 0);
            return result;
        }
    }
}
