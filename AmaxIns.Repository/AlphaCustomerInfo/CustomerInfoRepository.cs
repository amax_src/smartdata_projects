﻿using AmaxIns.DataContract.AplhaCustomerInfo;
using AmaxIns.RepositoryContract.AlphaCustomerInfo;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.AlphaCustomerInfo
{
    public class CustomerInfoRepository : BaseRepository, ICustomerInfoRepository
    {
        public CustomerInfoRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<string>> GetAgencyName()
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @" SELECT distinct a.agencyname as AgencyName
                               FROM [IP_AMAX40]..clientInfo c
                               JOIN [IP_AMAX40]..PAYMENTINFO P ON c.clientID=P.clientID
                               join [IP_AMAX40]..agencyInfo a on p.agencyid= a.agencyid
                              WHERE  (c.Deleted = 0 or c.Deleted is null)  and p.void=0
                              AND YEAR(PolicyEffDate) between 2019 and year(getdate())";
            
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetclientName(string SearchText)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"SELECT distinct applicantName as ClientName
                               FROM [IP_AMAX40]..clientInfo c
                               JOIN [IP_AMAX40]..PAYMENTINFO P ON c.clientID=P.clientID
                               join [IP_AMAX40]..agencyInfo a on p.agencyid= a.agencyid
                              WHERE  (c.Deleted = 0 or c.Deleted is null)  and p.void=0
                              AND YEAR(PolicyEffDate) between 2019 and year(getdate()) and applicantName like '"+ SearchText+"%'";
            //parameter.Add("@SearchText", SearchText, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<string>> GetCity()
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"SELECT 
                             distinct addressCity as City
                               FROM [IP_AMAX40]..clientInfo c
                               JOIN [IP_AMAX40]..PAYMENTINFO P ON c.clientID=P.clientID
                               join [IP_AMAX40]..agencyInfo a on p.agencyid= a.agencyid
                              WHERE  (c.Deleted = 0 or c.Deleted is null)  and p.void=0
                              AND YEAR(PolicyEffDate) between 2019 and year(getdate()) and addressCity is not null";
           
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<string>> GetPolicyType()
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"SELECT 
                             distinct policyType AS PolicyType
                               FROM [IP_AMAX40]..clientInfo c
                               JOIN [IP_AMAX40]..PAYMENTINFO P ON c.clientID=P.clientID
                               join [IP_AMAX40]..agencyInfo a on p.agencyid= a.agencyid
                              WHERE  (c.Deleted = 0 or c.Deleted is null)  and p.void=0
                              AND YEAR(PolicyEffDate) between 2019 and year(getdate()) and addressCity is not null";

            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetPolicyNumber(string SearchText)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"SELECT distinct policyNum as PolicyNumber
                               FROM [IP_AMAX40]..clientInfo c
                               JOIN [IP_AMAX40]..PAYMENTINFO P ON c.clientID=P.clientID
                               join [IP_AMAX40]..agencyInfo a on p.agencyid= a.agencyid
                              WHERE  (c.Deleted = 0 or c.Deleted is null)  and p.void=0
                              AND YEAR(PolicyEffDate) between 2019 and year(getdate()) and policyNum like '" + SearchText + "%'";
            //parameter.Add("@SearchText", SearchText, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<CustomerInfoModel>> CustomerInfoData()
        {
            IEnumerable<CustomerInfoModel> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"spSyncDB_GetAlpaCustomerInfo";
            result = await GetAsync<CustomerInfoModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
