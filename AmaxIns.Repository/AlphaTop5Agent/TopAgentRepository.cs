﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.AlphaTop5Agent;
using AmaxIns.DataContract.WorkingDays;
using AmaxIns.RepositoryContract.AlphaTop5Agent;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.AlphaTop5Agent
{
   public  class TopAgentRepository : BaseRepository, ITopAgentRepository
    {
        public TopAgentRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<AgencyModel>> GetAgency()
        {
            var query = "select AgencyID,AgencyName from IP_AMAX40.dbo.agencyInfo  where Deleted=0";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<int>> GetYear()
        {
            var query = "select distinct year(payDate) as Year from IP_AMAX40.dbo.paymentInfo where year(payDate)>2012 order by year(payDate) desc";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<int>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetPremiumData(string agency,string year,string month)
        {
            var query = "spSyncDB_GetAlpaTopfiveAgentPremium_sel";
            DynamicParameters parameter = new DynamicParameters();
            if(!string.IsNullOrWhiteSpace(agency))
            {
                parameter.Add("@Location", agency, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(year))
            {
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(month))
            {
                parameter.Add("@Month", month, DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<TopFiveDataModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetAgencyFeeData(string agency, string year, string month)
        {
            var query = "spSyncDB_GetAlpaTopfiveAgentAgencyFee_sel";
            DynamicParameters parameter = new DynamicParameters();
            if (!string.IsNullOrWhiteSpace(agency))
            {
                parameter.Add("@Location", agency, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(year))
            {
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(month))
            {
                parameter.Add("@Month", month, DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<TopFiveDataModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetPolicyCount(string agency, string year, string month)
        {
            var query = "spSyncDB_GetAlpaTopfiveAgentPolicyCount_sel";
            DynamicParameters parameter = new DynamicParameters();
            if (!string.IsNullOrWhiteSpace(agency))
            {
                parameter.Add("@Location", agency, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(year))
            {
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(month))
            {
                parameter.Add("@Month", month, DbType.String, ParameterDirection.Input);
            }

            var result = await GetAsync<TopFiveDataModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        
    }
}
