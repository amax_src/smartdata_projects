﻿using AmaxIns.DataContract.CarrierMix;
using AmaxIns.RepositoryContract.CarrierMix;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.CarrierMix
{
    public class CarrierMixRepository : BaseRepository, ICarrierMixRepository
    {
        public CarrierMixRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<string>> GetCarrier(string Month,string year)
        {
            IEnumerable<string> result = null;
            var query = @" select distinct CASE CHARINDEX(' ', replace(Carrier,'/',' '), 1) WHEN 0 THEN Carrier ELSE SUBSTRING(replace(Carrier,'/',' '), 1, CHARINDEX(' ', replace(Carrier,'/',' '), 1) - 1) end from tblCarrierMix crp where year(crp.Date)=@Year and  datename(Month,crp.Date)=@Month";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@Month", Month, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetDates(string Month, string year)
        {
            IEnumerable<string> result = null;
            var query = @"select  convert(varchar,crp.Date,101) as date from tblCarrierMix crp where year(crp.Date)=@Year and  datename(Month,crp.Date)=@Month group by crp.Date order by crp.Date desc";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@Month", Month, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<CarrierMixModel>> GetCarrierMixData(string date, string agency, string month, string Carrier,string year)
        {
            IEnumerable<CarrierMixModel> result = null;
            var query = @"[spSyncDB_GetCarrierMixAng_sel]";
            DynamicParameters parameter = new DynamicParameters();
            if (!string.IsNullOrWhiteSpace(date))
            {
                parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(agency))
            {
                parameter.Add("@agency", agency, DbType.String, ParameterDirection.Input);
            }
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            if (!string.IsNullOrWhiteSpace(Carrier))
            {
                parameter.Add("@Carrier", Carrier, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrWhiteSpace(year))
            {
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            }
            result = await GetAsync<CarrierMixModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
