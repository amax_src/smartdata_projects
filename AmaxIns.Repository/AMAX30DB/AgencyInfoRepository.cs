﻿using AmaxIns.DataContract.AMAX30DB;
using AmaxIns.RepositoryContract.AMAX30DB;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.AMAX30DB
{
    public class AgencyInfoRepository : BaseRepository, IAgencyInfoRepository
    {
        public AgencyInfoRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<AMAX30AncyInfoModel>> GetAgencyInfo(int agencyId, string agencyName)
        {
            var query = @"Sync_SP_Amax30DBAPI_GetAgencyInfo";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@agencyName", agencyName, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AMAX30AncyInfoModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<Amax30AgentInfoModel>> GetAgentInfo(string agentName, string email)
        {
            var query = @"Sync_SP_Amax30DBAPI_GetAgentInfo";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agentName", agentName, DbType.String, ParameterDirection.Input);
            parameter.Add("@email", email, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<Amax30AgentInfoModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<AMAX30ClientInfoModel>> GetClientInfo(CountListClientInfoQueryModel countListClientInfoQueryModel)
        {
            var query = @"Sync_SP_Amax30DBAPI_GetClientInfo";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@startDate", countListClientInfoQueryModel.startDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@endDate", countListClientInfoQueryModel.endDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@policyType", countListClientInfoQueryModel.policyType, DbType.String, ParameterDirection.Input);
            parameter.Add("@addressCity", countListClientInfoQueryModel.addressCity, DbType.String, ParameterDirection.Input);
            parameter.Add("@addressZip", countListClientInfoQueryModel.addressZip, DbType.String, ParameterDirection.Input);
            parameter.Add("@email", countListClientInfoQueryModel.email, DbType.String, ParameterDirection.Input);
            parameter.Add("@homePhone", countListClientInfoQueryModel.homePhone, DbType.String, ParameterDirection.Input);
            parameter.Add("@applicantName", countListClientInfoQueryModel.applicantName, DbType.String, ParameterDirection.Input);
            parameter.Add("@policyNum", countListClientInfoQueryModel.policyNum, DbType.String, ParameterDirection.Input);
            parameter.Add("@policyStatus", countListClientInfoQueryModel.policyStatus, DbType.String, ParameterDirection.Input);
            parameter.Add("@clientId", countListClientInfoQueryModel.clientId, DbType.Int64, ParameterDirection.Input);
            var result = await GetAsync<AMAX30ClientInfoModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AMAX30PaymentInfoModel>> GetClientPaymentInfo(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel)
        {
            var query = @"Sync_SP_Amax30DBAPI_GetClientPaymentInfo";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@payStartDate", countListPaymentInfoQueryModel.payStartDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@payEndDate", countListPaymentInfoQueryModel.payEndDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@carrier", countListPaymentInfoQueryModel.carrier, DbType.String, ParameterDirection.Input);
            parameter.Add("@applicantName", countListPaymentInfoQueryModel.applicantName, DbType.String, ParameterDirection.Input);
            parameter.Add("@policyNum", countListPaymentInfoQueryModel.policyNum, DbType.String, ParameterDirection.Input);
            parameter.Add("@paymentType", countListPaymentInfoQueryModel.paymentType, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AMAX30PaymentInfoModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
