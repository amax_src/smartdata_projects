﻿using AmaxIns.DataContract.Authentication;
using AmaxIns.RepositoryContract.Authentication;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Authentication
{
    public class AuthenticationRepository : BaseRepository, IAuthenticationRepository
    {

        public AuthenticationRepository(IConfiguration configuration) : base(configuration)
        {

        }
        public async Task<LoginUser> LogInAsyncUsingOTPRequest(LoginModel login)
        {
            var query = "[spSyncDB_GetDirectoryUsersRequestOTP_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@LoginName", login.UserName, DbType.String, ParameterDirection.Input);
            parameter.Add("@LoginPassword", login.Password, DbType.String, ParameterDirection.Input);
            parameter.Add("@GroupName", login.LoginType, DbType.String, ParameterDirection.Input);
            parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<LoginUser> LogInAsync(LoginModel login)
        {
            //LoginUser loginUser = null;

           

            if (login.LoginType.ToLower() != "agent" && !login.LoginType.Contains("alpa"))
            {
                var query = "[spSyncDB_GetDirectoryUsers_sel]";

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@LoginName", login.UserName, DbType.String, ParameterDirection.Input);
                parameter.Add("@LoginPassword", login.Password, DbType.String, ParameterDirection.Input);
                parameter.Add("@GroupName", login.LoginType, DbType.String, ParameterDirection.Input);
                parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);
                var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            else if (login.LoginType.ToLower() == "agent")
            {
                var query = "[spSyncDB_GetDirectoryAgentUsers_sel]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@LoginName", login.UserName, DbType.String, ParameterDirection.Input);
                parameter.Add("@LoginPassword", login.Password, DbType.String, ParameterDirection.Input);
                parameter.Add("@GroupName", login.LoginType, DbType.String, ParameterDirection.Input);
                parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);
                var reader = await GetMultipleQueryAsync(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                var result = reader.Read<LoginUser>().FirstOrDefault();
                if (!reader.IsConsumed && result != null)
                {
                    result.UserAgencyList = reader.Read<UserAgency>().ToList();
                }
                return result;
            }
            else if (login.LoginType.Contains("alpa"))
            {
                var query = "Alpa_DB..[spSyncDB_GetDirectoryUsersALPA_sel]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@loginEmail", login.UserName, DbType.String, ParameterDirection.Input);
                parameter.Add("@loginPassword", login.Password, DbType.String, ParameterDirection.Input);
                parameter.Add("@alpaRoleName", login.LoginType.Split("-")[1], DbType.String, ParameterDirection.Input);
                parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);
                var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            else
            {
                return null;
            }
        }
        public async void LogUser(LoginUser login)
        {
            var query = @"insert into DirectoryUserLog (UserID,GroupName,UserName,LoginName) values (@UserID,@GroupName,@UserName,@LoginName ) ";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@UserID", login.UserID, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@GroupName", login.GroupName, DbType.String, ParameterDirection.Input);
            parameter.Add("@UserName", login.UserName, DbType.String, ParameterDirection.Input);
            parameter.Add("@LoginName", login.LoginName, DbType.String, ParameterDirection.Input);
            var res = await AddAsync(query, parameters: parameter, commandType: CommandType.Text);
            var res1 = res;
        }
        public async void UpdatePassword(ResetPassword login)
        {
            var query = @"Update [dbo].[DirectoryUser] set LoginPassword=@Password where LoginName=@Login ";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Login", login.loginName, DbType.String, ParameterDirection.Input);
            parameter.Add("@Password", login.newPassword, DbType.String, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.Text);
        }
        public async Task<LoginUser> ValidateUserName(string email, string LoginType)
        {
            LoginUser loginUser = null;

            var query = "[spSyncDB_GetDirectoryUsers_sel]";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@LoginName", email, DbType.String, ParameterDirection.Input);
            parameter.Add("@GroupName", LoginType, DbType.String, ParameterDirection.Input);
            parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);


            var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.StoredProcedure);

            return result;
        }
        public async Task<string> RecoverPassword(string email, string LoginType)
        {

            LoginUser loginUser = null;

            var query = @"select LoginPassword from   dbo.DirectoryUserRole join DirectoryRole on DirectoryRole.DirectoryRoleID = DirectoryUserRole.DirectoryRoleid 
                        join DirectoryUser on DirectoryUser.UserID = DirectoryUserRole.UserID
                        where LoginName = @LoginName and IsActive = @IsActive and lower(replace(GroupName,' ',''))= lower(@GroupName)";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@LoginName", email, DbType.String, ParameterDirection.Input);
            parameter.Add("@GroupName", LoginType, DbType.String, ParameterDirection.Input);
            parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);


            var result = await GetFirstOrDefaultAsync<string>(query, parameter, commandType: CommandType.Text);

            return result;



        }


        public async Task<DesktopLoginUser> LogInDesktopAsync(string userName, string password)
        {
            var query = "select Name,Id from DesktopUser where UserName=@UserName and Password=@Password and [IsActive]=@IsActive";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
            parameter.Add("@Password", password, DbType.String, ParameterDirection.Input);
            parameter.Add("@IsActive", true, DbType.Boolean, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<DesktopLoginUser>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public async Task<Tuple<List<UserFeatureDesktop>, List<UserAgencyDesktop>>> GetAgencyFeatureAsync(int Userid)
        {
            var query = "sp_GetDesktopUserFeatureAndAgency";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@userid", Userid, DbType.String, ParameterDirection.Input);
            dynamic result = await GetMultipleAsync< UserFeatureDesktop, UserAgencyDesktop >(query, parameter, commandType: CommandType.StoredProcedure);
            var Feature = result.Item1;
            var Agencies = result.Item2;
            var tResult = new Tuple<List<UserFeatureDesktop>, List<UserAgencyDesktop>>(Feature, Agencies);
            return tResult;
        }

    }
}
