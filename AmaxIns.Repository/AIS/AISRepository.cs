﻿using AmaxIns.DataContract.AIS;
using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.PayRoll;
using AmaxIns.RepositoryContract.AIS;
using AmaxIns.RepositoryContract.EPR;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.AIS
{
    public class AISRepository : BaseRepository, IAISRepository
    {
        public AISRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<IEnumerable<AISModel>> GetAISData()
        {
            IEnumerable<AISModel> result = null;
            var query = "[spSyncDB_GetAisData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<AISModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }



        public async Task<IEnumerable<AISModel>> GetAISDataCsv()
        {
            IEnumerable<AISModel> result = null;
            var query = "[spSyncDB_GetAisDataCsv_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<AISModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
