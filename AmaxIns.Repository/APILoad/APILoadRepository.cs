﻿using AmaxIns.DataContract.APILoad;
using AmaxIns.RepositoryContract.APILoad;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AmaxIns.Repository.APILoad
{
   public class APILoadRepository : BaseRepository, IAPILoadRepository
    {
        public APILoadRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async void Save(ApiLoadModel model)
        {
            var query = @"insert into LoginResponsetime (Page,UserId,Time) values (@Page,@UserId,@Time)";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Page", model.Page, DbType.String, ParameterDirection.Input);
            parameter.Add("@UserId", model.User, DbType.String, ParameterDirection.Input);
            parameter.Add("@Time", model.Time, DbType.Int32, ParameterDirection.Input);
            await AddAsync(query, parameters: parameter, commandType: CommandType.Text);
        }
    }
}
