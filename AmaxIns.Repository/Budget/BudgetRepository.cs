﻿using AmaxIns.DataContract.Budget;
using AmaxIns.DataContract.Response;
using AmaxIns.RepositoryContract.Budget;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Budget
{
    public class BudgetRepository : BaseRepository, IBudgetRepository
    {
        public IConfiguration Configuration;
        public BudgetRepository(IConfiguration configuration) : base(configuration)
        {
            Configuration = configuration;
        }

        public async Task<IEnumerable<string>> GetBudgetUser(BudgetSearch budgetSearch)
        {
            var dept = string.Empty;
            if (budgetSearch.dept == "All")
            {
                dept = string.Join(",", budgetSearch.department).Replace("'", "''");
            }
            else
            {
                dept = budgetSearch.dept;
            }
            var cat = string.Join(",", budgetSearch.catg).Replace("'", "''");
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"select distinct(UserName) from [ActualTableImport] a join [BudgetTableImport] b on a.PlanID = b.PlanID where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) and Categoryname in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Catg), ',')) and Year = 2021 order by UserName";
            parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
            parameter.Add("@Catg", cat, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetBudgetLocation(BudgetSearch budgetSearch)
        {
            var dept = string.Empty;
            if (budgetSearch.dept == "All")
            {
                dept = string.Join(",", budgetSearch.department).Replace("'", "''");
            }
            else
            {
                dept = budgetSearch.dept;
            }
            var cat = string.Join(",", budgetSearch.catg).Replace("'", "''");
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"select distinct(LocationID) from [ActualTableImport] a join [BudgetTableImport] b on a.PlanID = b.PlanID where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) and Categoryname in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Catg), ',')) and Year = 2021 order by LocationID";
            parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
            parameter.Add("@Catg", cat, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetBudgetSource(BudgetSearch budgetSearch)
        {
            var dept = string.Empty;
            if (budgetSearch.dept == "All")
            {
                dept = string.Join(",", budgetSearch.department).Replace("'", "''");
            }
            else
            {
                dept = budgetSearch.dept;
            }
            var cat = string.Join(",", budgetSearch.catg).Replace("'", "''");
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"select distinct(Source) from [ActualTableImport] a join [BudgetTableImport] b on a.PlanID = b.PlanID where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) and Categoryname in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Catg), ',')) and Year = 2021 order by Source";
            parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
            parameter.Add("@Catg", cat, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetBudgetCode(BudgetSearch budgetSearch)
        {
            var dept = string.Empty;
            if (budgetSearch.dept == "All")
            {
                dept = string.Join(",", budgetSearch.department).Replace("'", "''");
            }
            else
            {
                dept = budgetSearch.dept;
            }
            var cat = string.Join(",", budgetSearch.catg).Replace("'", "''");
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"select distinct(a.FrontBudgetCode) from [ActualTableImport] a join [BudgetTableImport] b on a.PlanID = b.PlanID where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) and Categoryname in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Catg), ',')) and Year = 2021 order by a.FrontBudgetCode";
            parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
            parameter.Add("@Catg", cat, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetBudgetDept(int userId)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@userId", userId, DbType.Int16, ParameterDirection.Input);
            var query = @"select distinct Department from [BudgetTableImport] B 
                            INNER JOIN BudgetUserDepartment BUD ON BUD.DepartmentName=B.Department
                            INNER JOIN BudgetDepartmentMappingWithUser BDMU ON BDMU.DepartmentId=BUD.DepartmentId
                            WHERE BDMU.UserId=@userId and Department != 'NAN'
                            order by Department";
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            var data = result.ToList();
            data.Insert(0, "All");
            return data.AsEnumerable<string>();
        }

        public async Task<IEnumerable<string>> GetBudgetCategories(BudgetSearch obj)
        {
            var dept = string.Empty;
            if (obj.dept == "All")
            {
                dept = string.Join(",", obj.department).Replace("'", "''");
            }
            else
            {
                dept = obj.dept;
            }
            IEnumerable<string> result = null;
            var query = @"select distinct Categoryname from [BudgetTableImport] where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) order by CategoryName";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetBudgetBudgetName(BudgetSearch obj)
        {

            IEnumerable<string> result = null;
            try
            {
                var dept = string.Empty;
                if (obj.dept == "All")
                {
                    dept = string.Join(",", obj.department).Replace("'", "''");
                }
                else
                {
                    dept = obj.dept;
                }
                var cat = string.Join(",", obj.catg);
                //var query = @"select distinct BudgetName from [Sync_DB].[dbo].[Plan] where Department=@Department and Categoryname in (select ParameterValue from [fnUTIL_DelimitedStringToTable_sel](',',@Catg))";
                var query = @"select distinct BudgetName from [BudgetTableImport] where Department in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Department), ',')) and Categoryname in (SELECT Data FROM [Sync_DB].[dbo].[fn_DelimitedStringToTable_sel]((@Catg), ','))";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
                parameter.Add("@Catg", cat, DbType.String, ParameterDirection.Input);
                result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {

            }
            return result;
        }


        public async Task<BudgetModel> GetBudgetByMonth(BudgetFormModel budget)
        {
            BudgetModel result = null;
            try
            {
                var dept = string.Empty;
                if (budget.DepartmentName == "All")
                {
                    dept = string.Join(",", budget.Department).Replace("'", "''");
                }
                else
                {
                    dept = budget.DepartmentName;
                }
                var budgetName = string.Join(",", budget.Budget).Replace("'", "''");
                var Months = string.Join(",", budget.Months).Replace("'", "''");

                var query = @"[spSyncDB_GetBudget_sel]";
                //var query = @"[spSyncDB_GetBudget_sel_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                parameter.Add("@PeriodId", budget.Month, DbType.String, ParameterDirection.Input);
                parameter.Add("@Months", Months, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetName", budgetName, DbType.String, ParameterDirection.Input);
                parameter.Add("@DeptName", dept, DbType.String, ParameterDirection.Input);
                result = await GetFirstOrDefaultAsync<BudgetModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        
        public async Task<IEnumerable<BudgetModel>> GetBudgetAnnual(BudgetFormModel budget)
        {
            IEnumerable<BudgetModel> result = null;
            try
            {
                var dept = string.Empty;
                if (budget.DepartmentName == "All")
                {
                    dept = string.Join(",", budget.Department).Replace("'", "''");
                }
                else
                {
                    dept = budget.DepartmentName;
                }
                var budgetName = string.Join(",", budget.Budget).Replace("'", "''");

                var query = @"[spSyncDB_GetBudgetAnnual_sel]";
                //var query = @"[spSyncDB_GetBudgetAnnual_sel_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetName", budgetName, DbType.String, ParameterDirection.Input);
                parameter.Add("@DeptName", dept, DbType.String, ParameterDirection.Input);
                result = await GetAsync<BudgetModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<BudgetOverspendModel>> GetBudgetSpending(BudgetFormModel budget)
        {
            List<BudgetOverspendModel> budgetOverspends = new List<BudgetOverspendModel>();
            IEnumerable<BudgetOverspendModel> result = null;
            try
            {
                var dept = string.Empty;
                if (budget.DepartmentName == "All")
                {
                    dept = string.Join(",", budget.Department).Replace("'", "''");
                }
                else
                {
                    dept = budget.DepartmentName;
                }
                var budgt = string.Join(",", budget.Budget).Replace("'", "''");
                var Months = string.Join(",", budget.Months).Replace("'", "''");

                var query = @"[spSyncDB_GetBudgetSpending_sel]";
                //var query = @"[spSyncDB_GetBudgetSpending_sel_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PeriodId", Months, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetName", budgt, DbType.String, ParameterDirection.Input);
                parameter.Add("@DeptName", dept, DbType.String, ParameterDirection.Input);
                result = await GetAsync<BudgetOverspendModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                budgetOverspends = result.ToList();
                budgetOverspends.RemoveAll(x => x.Budget == 0 && x.Actual == 0 && x.Variance == 0);
                result = budgetOverspends.AsEnumerable();
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<BudgetTrancModel>> GetBudgetTranc(BudgetFormModel budget)
        {
            IEnumerable<BudgetTrancModel> result = null;
            try
            {
                var dept = string.Empty;
                if (budget.DepartmentName == "All")
                {
                    dept = string.Join(",", budget.Department).Replace("'", "''");
                }
                else
                {
                    dept = budget.DepartmentName;
                }
                var location = string.Join(",", budget.Location).Replace("'", "''");
                var user = string.Join(",", budget.User).Replace("'", "''");
                var cat = string.Join(",", budget.CategoryName).Replace("'", "''");
                var btname = string.Join(",", budget.BudgetName).Replace("'", "''");
                var btcode = string.Join(",", budget.BudgetCode).Replace("'", "''");
                var src = string.Join(",", budget.Source).Replace("'", "''");
                var Months = string.Join(",", budget.Months).Replace("'", "''");

                var query = @"[spSyncDB_GetBudgetTransactions_sel_new]";
                //var query = @"[spSyncDB_GetBudgetTransactions_sel_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PeriodId", Months, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                parameter.Add("@Department", dept, DbType.String, ParameterDirection.Input);
                parameter.Add("@Location", location, DbType.String, ParameterDirection.Input);
                parameter.Add("@User", user, DbType.String, ParameterDirection.Input);
                parameter.Add("@Category", cat, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetName", btname, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetCode", btcode, DbType.String, ParameterDirection.Input);
                parameter.Add("@Source", src, DbType.String, ParameterDirection.Input);
                result = await GetAsync<BudgetTrancModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<IEnumerable<BudgetSummaryModel>> GetBudgetSummary(BudgetFormModel budget)
        {
            IEnumerable<BudgetSummaryModel> result = null;
            try
            {
                var location = string.Join(",", budget.Location).Replace("'", "''");
                var user = string.Join(",", budget.User).Replace("'", "''");
                var cat = string.Join(",", budget.CategoryName).Replace("'", "''");
                var btname = string.Join(",", budget.BudgetName).Replace("'", "''");
                var btcode = string.Join(",", budget.BudgetCode).Replace("'", "''");
                var src = string.Join(",", budget.Source).Replace("'", "''");
                var Months = string.Join(",", budget.Months).Replace("'", "''");

                var query = @"[spSyncDB_GetBudgetSummary_sel]";
                //var query = @"[spSyncDB_GetBudgetSummary_sel_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PeriodId", Months, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                result = await GetAsync<BudgetSummaryModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }


        public async Task<IEnumerable<BudgetTransactionModel>> GetBudgetTrx(BudgetFormModel budget)
        {
            IEnumerable<BudgetTransactionModel> result = null;
            try
            {
                var budgetName = string.Join(",", budget.Budget).Replace("'", "''");
                var query = @"[spSyncDB_GetBudgetTransactions_sel]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PeriodId", budget.Month, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", budget.year, DbType.String, ParameterDirection.Input);
                parameter.Add("@BudgetName", budgetName, DbType.String, ParameterDirection.Input);
                result = await GetAsync<BudgetTransactionModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public ResponseViewModel UploadActualExcelFile(List<ActualTable> actualTablesList)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var query = "ActualTableImport";
                var res = UploadExcelFile(query, actualTablesList);
                if (res)
                {
                    response.IsSuccess = true;
                    response.StatusCode = 200;
                    response.Message = "File Uploaded successfully";
                }
                else
                {
                    response.IsSuccess = true;
                    response.StatusCode = 400;
                    response.Message = "Something went wrong. Please check file or try again";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = true;
                response.StatusCode = 400;
                response.Message = "Something went wrong. Please check file or try again";
                return response;
            }
            
            return response;
        }

        public async Task<IEnumerable<string>> DeleteActualFileExestingData(int month, int year)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"delete from ActualTableImport where YearID = @Year and PeriodID = @Month";
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public ResponseViewModel UploadBudgetExcelFile(List<BudgetTable> budgetTablesList)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var query = "BudgetTableImport";
                var res = UploadExcelFile(query, budgetTablesList);
                if(res)
                {
                    response.IsSuccess = true;
                    response.StatusCode = 200;
                    response.Message = "File Uploaded successfully";
                    return response;
                }
                else
                {
                    response.IsSuccess = true;
                    response.StatusCode = 400;
                    response.Message = "Something went wrong. Please check file or try again";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = true;
                response.StatusCode = 400;
                response.Message = "Something went wrong. Please check file or try again";
                return response;
            }
            
        }

        public async Task<IEnumerable<string>> DeleteBudgetFileExestingData(int month, int year)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"delete from BudgetTableImport where Year = @Year and Period = @Month";
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetLatestMonthValue(int year)
        {
            IEnumerable<string> result = null;
            DynamicParameters parameter = new DynamicParameters();
            var query = @"select top 1 periodid from [ActualTableImport] where year(cast( trxdate as date) )= @year order by 1 desc ";
            parameter.Add("@year", year, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

    }
}
