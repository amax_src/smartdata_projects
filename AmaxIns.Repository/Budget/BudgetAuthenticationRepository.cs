﻿using AmaxIns.DataContract.Authentication;
using AmaxIns.RepositoryContract.Budget;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Budget
{
    public class BudgetAuthenticationRepository : BaseRepository, IBudgetAuthenticationRepository
    {
        public BudgetAuthenticationRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<LoginUser> LogInAsync(LoginModel login)
        {
            //var query = "[spSyncDB_ValidateBudgetLogin]";
            var query = "[spSyncDB_ValidateBudgetLogin_Dev]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@LoginName", login.UserName, DbType.String, ParameterDirection.Input);
            parameter.Add("@LoginPassword", login.Password, DbType.String, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<LoginUser>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
