﻿using AmaxIns.DataContract.Calendar;
using AmaxIns.RepositoryContract.Calendar;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Calendar
{
    public class CalendarRepository : BaseRepository, ICalendarRepository
    {
        public CalendarRepository(IConfiguration configuration) : base(configuration) { }


        public async Task<IEnumerable<CalendarEvents>> GetEvents(int agencyId, string month)
        {

            var query = "select EventId,EventName,EventType,EventDate,AgencyId,CreatedDate,ModifiedDate from [dbo].[CalendarEvents] where AgencyId=@agencyId and MONTH(eventdate)=ISNULL(@month,MONTH(getdate()))";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<CalendarEvents>(query, parameter, commandType: CommandType.Text);
            return result;

        }

        public async Task<IEnumerable<CalendarLedgerDetails>> GetEventLedgerDetail(int eventId)
        {
            var query = "select LedgerId,EventId,Memo,Credit,Debit,EntryDate,CreatedDate from [dbo].[CalendarLedgerDetails] where EventId=@eventId";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@eventId", eventId, DbType.Int16, ParameterDirection.Input);
            var result = await GetAsync<CalendarLedgerDetails>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public int AddEvents(CalendarEvents addEvent)
        {
            //TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            //DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
            try
            {
                int EventIdOut = 0;
                var query = "[spSyncDB_CalendarEvents_mod]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@EventId", addEvent.EventId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@EventName", addEvent.EventName, DbType.String, ParameterDirection.Input);
                parameter.Add("@EventType", addEvent.EventType, DbType.String, ParameterDirection.Input);
                parameter.Add("@EventDate", addEvent.EventDate, DbType.Date, ParameterDirection.Input);
                parameter.Add("@AgencyId", addEvent.AgencyId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@AddedBy", addEvent.AddedBy, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@EventIdOut", EventIdOut, DbType.Int32, ParameterDirection.Output);
                Add(query, parameter, commandType: CommandType.StoredProcedure);
                return parameter.Get<int>("EventIdOut");
            }catch(Exception ex)
            {
                return -1;
            }
        }
        public void AddLedgerDetails(CalendarLedgerDetails ledgerDetails, int eventId)
        {
            var sqlQuery = "[spSyncDB_CalendarLedgerDetails_mod]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@LedgerId", ledgerDetails.LedgerId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@EventId", eventId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Memo", ledgerDetails.Memo, DbType.String, ParameterDirection.Input);
            parameter.Add("@Credit", ledgerDetails.Credit, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Debit", ledgerDetails.Debit, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@EntryDate", ledgerDetails.EntryDate, DbType.Date, ParameterDirection.Input);
            parameter.Add("@AddedBy", ledgerDetails.AddedBy, DbType.Int32, ParameterDirection.Input);
            Add(sqlQuery, parameter, commandType: CommandType.StoredProcedure);
        }

    }
}
