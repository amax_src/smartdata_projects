﻿using AmaxIns.DataContract.Planner;
using AmaxIns.RepositoryContract.Planner;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Planner
{
    public class PlannerRepository : BaseRepository, IPlannerRepository
    {
        public PlannerRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<PlannerActivity>> GetAllActivity()
        {
            var query = "SELECT ActivityId,ActivityName FROM tblPlannerActivity WHERE ISNULL(IsDeleted,0)=0";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<PlannerActivity>(query, null, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<PlannerHour>> GetAllHours()
        {
            var query = "SELECT PlannerHoursId,PlannerHours FROM tblPlannerHours WHERE IsDeleted=0";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<PlannerHour>(query, null, commandType: CommandType.Text);
            return result;
        }

        public async Task<int> SaveUpdatePlanner(DataTable dataTable)
        {
            try
            {
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@tblPlanner", dataTable, DbType.Object, ParameterDirection.Input);
                var result = await GetAsync<PlannerHour>("sp_SaveUpdatePlanner", parameter, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }

            return 1;
        }

        public async Task<int> DeleteBudgetPlanner(int plannerBudgetId)
        {
            try
            {
                var query = "Update tblPlannerBudget set IsDeleted = 1 WHERE PlannerBudgetId=@plannerBudgetId";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@plannerBudgetId", plannerBudgetId, DbType.Int16, ParameterDirection.Input);
                var result = await GetAsync<PlannerHour>(query, parameter, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                throw;
            }

            return 1;
        }

        public async Task<IEnumerable<PlannerBudget>> GetAllPlanner(int agencyId, string monthName)
        {
            DynamicParameters parameter = new DynamicParameters();
            string query = @"SELECT t.PlannerBudgetId,t.AgencyId,CONVERT(VARCHAR,t.PlannerDate, 101) PlannerDate,t.Activity,t.OthersActivity,t.PlannedHours,t.PlannedDetails,t.PlannedBudgetAmount
                             FROM tblPlannerBudget t WHERE AgencyId=@agencyId AND ISNULL(IsDeleted,0)=0 AND MONTH(PlannerDate) =
                             CASE WHEN @Month='January' THEN 1 
                             WHEN @Month='February' THEN 2
                             WHEN @Month='March' THEN 3
                             WHEN @Month='April' THEN 4
                             WHEN @Month='May' THEN 5
                             WHEN @Month='June' THEN 6
                             WHEN @Month='July' THEN 7
                             WHEN @Month='August' THEN 8
                             WHEN @Month='September' THEN 9
                             WHEN @Month='October' THEN 10
                             WHEN @Month='November' THEN 11
                             WHEN @Month='December' THEN 12
                             END ORDER BY PlannerDate ASC";
            parameter.Add("@agencyId", agencyId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@Month", monthName, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<PlannerBudget>(query, parameter, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<ActualPlanner>> GetActualPlanner(int agencyId, string monthName,string year)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", agencyId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@Month", monthName, DbType.String, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<ActualPlanner>("sync_DB_GetActualPlanner", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<PlannerActualDetails> GetSingelActual(int plannerBudgetId)
        {
            PlannerActualDetails plannerActualDetails = new PlannerActualDetails();
            plannerActualDetails.plannerBudget = new PlannerBudget();
            plannerActualDetails.actualDetail = new ActualDetail();
            plannerActualDetails.listActualPolicyDetails = new List<ActualPolicyDetails>();
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@plannerBudgetId", plannerBudgetId, DbType.Int16, ParameterDirection.Input);

            string Query = @"select AgencyId,CONVERT(VARCHAR,PlannerDate, 101)  PlannerDate,Activity,OthersActivity,PlannedHours,PlannedDetails,
                             PlannedBudgetAmount,PlannerBudgetId from tblPlannerBudget WHERE PlannerBudgetId=@plannerBudgetId AND ISNULL(IsDeleted,0)=0; 

                             select PlannerBudgetId,PlannerActualId,ActualHours,IsWent,ActualDetails,ActualAmount from tblPlannerActual 
                             WHERE PlannerBudgetId=@plannerBudgetId; 

                             SELECT PolicyDetailId,PlannerBudgetId,PolicyNumber,AgencyFee,PremiumAmount FROM tblActualPolicyDetails 
                             WHERE PlannerBudgetId=@plannerBudgetId;";

            var result = await GetMultipleQueryAsync(Query, parameter, commandType: CommandType.Text);

            plannerActualDetails.plannerBudget = result.Read<PlannerBudget>().FirstOrDefault();
            plannerActualDetails.actualDetail = result.Read<ActualDetail>().FirstOrDefault();
            if (plannerActualDetails.actualDetail == null)
            {
                plannerActualDetails.actualDetail = new ActualDetail();
                plannerActualDetails.actualDetail.PlannerActualId = 0;
            }
            plannerActualDetails.listActualPolicyDetails = result.Read<ActualPolicyDetails>().ToList();

            return plannerActualDetails;
        }

        public async Task<int> SaveUpdateActual(PlannerActualDetails plannerActualDetails)
        {
            string Query = string.Empty;
             Query = @" 
                            IF EXISTS (SELECT * FROM tblPlannerActual WHERE PlannerBudgetId=@PlannerBudgetId)
                             BEGIN
	                            UPDATE tblPlannerActual SET ActualHours=@ActualHours,IsWent=@IsWent,ActualDetails=@ActualDetails
                                ,ActualAmount=@ActualAmount,UpdatedDated=GETDATE(),CreadedBy=@LoginUserId WHERE PlannerBudgetId=@PlannerBudgetId
                             END
                             ELSE IF NOT EXISTS (SELECT * FROM tblPlannerActual WHERE PlannerBudgetId=@PlannerBudgetId)
                             BEGIN
	                            INSERT INTO tblPlannerActual (PlannerBudgetId,ActualHours,IsWent,ActualDetails,ActualAmount ,UpdatedDated,CreadedBy)
	                            VALUES (@PlannerBudgetId,@ActualHours,@IsWent,@ActualDetails,@ActualAmount,GETDATE(),@LoginUserId)
                             END

                            SELECT @PlannerBudgetId";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@PlannerBudgetId", plannerActualDetails.plannerBudget.PlannerBudgetId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@LoginUserId", plannerActualDetails.actualDetail.LoginUserId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@IsWent", plannerActualDetails.actualDetail.IsWent, DbType.Boolean, ParameterDirection.Input);
            parameter.Add("@ActualHours", plannerActualDetails.actualDetail.ActualHours, DbType.Decimal, ParameterDirection.Input);
            parameter.Add("@ActualDetails", plannerActualDetails.actualDetail.ActualDetails, DbType.String, ParameterDirection.Input);
            parameter.Add("@ActualAmount", plannerActualDetails.actualDetail.ActualAmount, DbType.Decimal, ParameterDirection.Input);
            int result = await GetFirstOrDefaultAsync<int>(Query, parameter, commandType: CommandType.Text);

            if(result>0)
            {
                foreach (ActualPolicyDetails obj in plannerActualDetails.listActualPolicyDetails.Where(x=>!string.IsNullOrEmpty(x.PolicyNumber)))
                {
                    Query = @"
                            IF EXISTS (SELECT PolicyDetailId FROM tblActualPolicyDetails WHERE PolicyDetailId=@PolicyDetailId)
                            BEGIN
	                            UPDATE tblActualPolicyDetails SET PlannerBudgetId=@PlannerBudgetId
	                            ,PolicyNumber=@PolicyNumber,AgencyFee=@AgencyFee,PremiumAmount=@PremiumAmount,UpdatedDated=GETDATE(),UpdatedBy=@LoginUserId
	                            WHERE PolicyDetailId=@PolicyDetailId
                            END
                            ELSE IF NOT EXISTS (SELECT PolicyDetailId FROM tblActualPolicyDetails WHERE PolicyDetailId=@PolicyDetailId)
                            BEGIN
	                            INSERT INTO tblActualPolicyDetails (PlannerBudgetId,PolicyNumber,AgencyFee,PremiumAmount,CreatedDate,CreadedBy)
	                            VALUES (@PlannerBudgetId,@PolicyNumber,@AgencyFee,@PremiumAmount,GETDATE(),@LoginUserId)
                            END
                            ";
                    parameter = new DynamicParameters();
                    parameter.Add("@PlannerBudgetId", plannerActualDetails.plannerBudget.PlannerBudgetId, DbType.Int16, ParameterDirection.Input);
                    parameter.Add("@PolicyDetailId", obj.PolicyDetailId, DbType.Int16, ParameterDirection.Input);
                    parameter.Add("@PolicyNumber", obj.PolicyNumber, DbType.String, ParameterDirection.Input);
                    parameter.Add("@AgencyFee", obj.AgencyFee, DbType.Decimal, ParameterDirection.Input);
                    parameter.Add("@PremiumAmount", obj.PremiumAmount, DbType.Decimal, ParameterDirection.Input);
                    parameter.Add("@LoginUserId", plannerActualDetails.actualDetail.LoginUserId, DbType.Int16, ParameterDirection.Input);
                    await GetFirstOrDefaultAsync<int>(Query, parameter, commandType: CommandType.Text);
                }
            }

            return result;
        }

        public async Task<IEnumerable<PlannerDashboard>> GetDashboardReport(string monthName, string year)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", monthName, DbType.String, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<PlannerDashboard>("sync_DB_GetActualPlannerDashboard", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DailyView>> GetDailyViewReport(string monthName,int agencyId,string year)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Month", monthName, DbType.String, ParameterDirection.Input);
            parameter.Add("@agencyId", agencyId, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@yaer", year, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<DailyView>("Sync_DB_PlannerDailyView", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityTotal(string monthName, string year, string agencyId)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@month", monthName, DbType.String, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@AcencyId", agencyId, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<PlannerActivityDetails>("sp_GetPlannerActivityCountAllAgency_sel", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityAgencyWise(string monthName, string year)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@month", monthName, DbType.String, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<PlannerActivityDetails>("sp_GetPlannerActivityCountAgencyWise_sel", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DailyView>> GetDataForWeekly(QueryPlanner queryPlanner)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", queryPlanner.selectedlocation, DbType.Int16, ParameterDirection.Input);
            parameter.Add("@startDate", queryPlanner.startDate, DbType.String, ParameterDirection.Input);
            parameter.Add("@endDate", queryPlanner.endDate, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<DailyView>("Sync_DB_PlannerWeeklyCalendarView", parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
