﻿using AmaxIns.DataContract.Dashboard;
using AmaxIns.RepositoryContract.Dashboard;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Dashboard
{
    public class DashboardRepository : BaseRepository, IDashboardRepository
    {
        public DashboardRepository(IConfiguration configuration) : base(configuration)
        {
           
        }

        public async Task<IEnumerable<DashboardQuotesModel>> GetQuoteData(int agencyid, int month, int year)
        {
            //sp_GetQuotesClosingData_sel_Dev_New ==> Dev Test New logic
            //sp_GetQuotesClosingData_sel_LiveProd_New  ==> Dev Test New logic
            //sp_GetQuotesClosingData_sel ==> Live Orignal (Old)
            IEnumerable<DashboardQuotesModel> result = null;
            //var query = @"sp_GetQuotesClosingData_sel";
            //if (year == 2022 || year == 2021)
            //{
            //    // query = @"sp_GetQuotesClosingData_sel_LiveProd_New";
            //    query = @"sp_GetQuotesClosingData_sel_Prod_V1"; 
            //}
            var query = @"sp_GetQuotesClosingData_sel_Prod_V1";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardQuotesModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardRevenueModel>> GetSalesData(int agencyid, int month, int year)
        {
            IEnumerable<DashboardRevenueModel> result = null;
            var query = @"spSyncDB_GetConsolidatedRevenuePercentages_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardRevenueModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }


        public async Task<IEnumerable<DashboardEPRModel>> GetEprData(int agencyid, int month, int year)
        {
            IEnumerable<DashboardEPRModel> result = null;
            var query = @"[sp_GetEprData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardEPRModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboadSpectrumModel>> GetCallsData(int agencyid, int month, int year)
        {
            IEnumerable<DashboadSpectrumModel> result = null;
            var query = @"[spSyncDB_GetSpetrumDashboardData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboadSpectrumModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardPayrollModel>> GetPayrollActualData(int agencyid, int month, int year)
        {
            IEnumerable<DashboardPayrollModel> result = null;
            var query = @"[sp_GetPayroll_ActualData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardPayrollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardPayrollModel>> GetPayrollBudgetOvertime(int agencyid, int month, int year)
        {
            IEnumerable<DashboardPayrollModel> result = null;
            var query = @"[sp_GetPayroll_BudgetOvertime_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardPayrollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }



        public async Task<IEnumerable<DashboardPayrollModel>> GetPayrollBudgeData(int agencyid, int month, int year)
        {
            IEnumerable<DashboardPayrollModel> result = null;
            var query = @"[sp_GetPayroll_BudgetData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardPayrollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardPayrollModel>> GetPayrollOvertimeData(int agencyid, int month, int year)
        {
            IEnumerable<DashboardPayrollModel> result = null;
            var query = @"[sp_GetPayroll_OvertimeData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardPayrollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardEPRModel>> GetEprDashboardDaily(int agencyid, int month, int year)
        {
            IEnumerable<DashboardEPRModel> result = null;
            var query = @"[sp_GetEprDataDetailsDaily_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardEPRModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<DashboardEPRModel>> GetEprlDashboardWeekly(int agencyid, int month, int year)
        {
            IEnumerable<DashboardEPRModel> result = null;
            var query = @"[sp_GetEprDataDetails_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@AgencyId", agencyid, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<DashboardEPRModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
