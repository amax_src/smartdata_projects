﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using AmaxIns.DataContract.PBX;
using AmaxIns.RepositoryContract.PBX;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace AmaxIns.Repository.PBX
{
    public class PbxRepository : BaseRepository, IPbxRepository
    {
        public PbxRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public Task<IEnumerable<string>> GetDate()
        {
            return null;
        }

        public async Task<IEnumerable<string>> GetExtention()
        {
            IEnumerable<string> result = null;
            var query = @"select  distinct ltrim(rtrim(REPLACE(Extension, CHAR(13), '')))Extension from   [Sync_DB].[dbo].[pbx] 
                          where datepart(year,cast((REPLACE([Start Time], CHAR(13), '')) as date))=year(getdate())";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<string>> GetMonth()
        {
            IEnumerable<string> result = null;
            var query = @"select   datename(mm,cast(REPLACE([Start Time], CHAR(13), '')as datetime))Month from   [Sync_DB].[dbo].[pbx] where 
                          datepart(year,cast((REPLACE([Start Time], CHAR(13), '')) as date))=year(getdate())
                          group by datename(mm,cast(REPLACE([Start Time], CHAR(13), '')as datetime)), datepart(mm,cast(REPLACE([Start Time], CHAR(13), '')as datetime))
                          order by datepart(mm,cast(REPLACE([Start Time], CHAR(13), '')as datetime)) desc";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<PbxModel>> GetPBXData()
        {
            IEnumerable<PbxModel> result = null;
            var query = @"spSyncDB_GetPbx_sel";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<PbxModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<PbxModel>> GetPBXDailyData()
        {
            IEnumerable<PbxModel> result = null;
            var query = @"spSyncDB_GetPbxDaily_sel";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<PbxModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        
    }
}
