﻿using AmaxIns.DataContract.Spectrum;
using AmaxIns.RepositoryContract.Spectrum;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System;

namespace AmaxIns.Repository.Spectrum
{
    public class SpectrumRepository : BaseRepository, ISpectrumRepository
    {

        public SpectrumRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumDailySummary()
        {
            IEnumerable<SpectrumDailySummaryModel> result = null;
            var query = "[spSyncDB_GetSpectrumDailySummary_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<SpectrumDailySummaryModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumMonthlySummary()
        {
            IEnumerable<SpectrumDailySummaryModel> result = null;
            var query = "[spSyncDB_GetSpectrumMonthlySummary_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<SpectrumDailySummaryModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumDailyDetail(string date)
        {
            IEnumerable<SpectrumDailyDetailModel> result = null;
            var query = "[spSyncDB_GetSpectrumDaily_sel]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@date", date, DbType.Date, ParameterDirection.Input);
            result = await GetAsync<SpectrumDailyDetailModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumMonthlyDetail(string month, int? agencyId)
        {
            IEnumerable<SpectrumDailyDetailModel> result = null;
            try
            {

                var query = "[spSyncDB_GetConsolidatedSpectrumMonthlyData_sel]";
                DynamicParameters parameter = new DynamicParameters();
                if (!string.IsNullOrWhiteSpace(month))
                {
                    parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                }
                if (agencyId.HasValue)
                {
                    parameter.Add("@agencyId", agencyId, DbType.Int32, ParameterDirection.Input);
                }
                result = await GetAsync<SpectrumDailyDetailModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public async Task<ConsolidatedCallVolumeModel> GetSpectrumCallVolume()
        {
            ConsolidatedCallVolumeModel result = new ConsolidatedCallVolumeModel();
            try
            {
                var query = "[spSyncDB_GetSpectrumMonthlyCallVolume_ByDays_sel]";
                DynamicParameters parameter = new DynamicParameters();
                var result1 = await GetAsync<CallVolumeModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                result.ByDays = result1;
                var query2 = "[spSyncDB_GetSpectrumMonthlyCallVolume_byStatus_sel]";
                DynamicParameters parameter2 = new DynamicParameters();
                var result2 = await GetAsync<CallVolumeModel>(query2, parameter2, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                result.ByStatus = result2;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<CallCountModel>> GetCallCount(string date)
        {
            IEnumerable<CallCountModel> result = new List<CallCountModel>();
            try
            {
                var query = "[spSyncDB_GetSpectrumDailyRepeated_sel]";
                DynamicParameters parameter = new DynamicParameters();
                if (date!=null)
                {
                    parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
                }

                result = await GetAsync<CallCountModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<CallCountModel>> GetCallCountOutBound(string date)
        {
            IEnumerable<CallCountModel> result = new List<CallCountModel>();
            try
            {
                var query = "[spSyncDB_GetSpectrumDailyRepeatedOut_sel]";
                DynamicParameters parameter = new DynamicParameters();
                if (date != null)
                {
                    parameter.Add("@date", date, DbType.String, ParameterDirection.Input);
                }

                result = await GetAsync<CallCountModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<RepeatedCallersModel>> GetRepeatedCallers(string date)
        {
            IEnumerable<RepeatedCallersModel> result = new List<RepeatedCallersModel>();
            try
            {
                var query = "[spSyncDB_GetSpectrumDailyRepeatedCalls_sel]";
                DynamicParameters parameter = new DynamicParameters();
                result = await GetAsync<RepeatedCallersModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<string>> GetDates()
        {
            IEnumerable<string> result = new List<string>();
            try
            {
                var query = @"  select convert(varchar,p.date, 110)   from( 
                                select   cast(Start_Time as date) as Date  FROM  Spectrum
	                            right join  SpectrumAgencyMap on Spectrum.agencyname=SpectrumAgencyMap.summaryfilename
	                            right join  IP_AMAX30..agencyinfo on IP_AMAX30..agencyinfo.agencyid=SpectrumAgencyMap.agencyid
	                            where 
	                            Spectrum.agencyname not in( '19177PrestonRd','3302HarwoodDr','5530EBelknapStreet') 
	                            and filename like '%Daily.details%' group by cast(Start_Time as date) ) p order by p.date desc";
                DynamicParameters parameter = new DynamicParameters();
                result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

    }
}
