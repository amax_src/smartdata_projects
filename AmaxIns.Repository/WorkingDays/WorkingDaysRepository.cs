﻿using AmaxIns.DataContract.WorkingDays;
using AmaxIns.RepositoryContract.WorkingDays;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.WorkingDays
{
    public class WorkingDaysRepository : BaseRepository, IWorkingDaysRepository
    {
        public WorkingDaysRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<WorkingdayModel>> GetWorkingDaysOfMonth()
        {
            var query = "[spSyncDB_GetDaysInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<WorkingdayModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<int> GetTotalWorkingDaysOfMonth(int month,int year)
        {
            var query = "select TotalWorkingDays from tblWorkingDays where MonthNo=@Month and year=@Year";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.String, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<int>(query, parameter, commandType: CommandType.Text);
            return result;
        }
    }
}
