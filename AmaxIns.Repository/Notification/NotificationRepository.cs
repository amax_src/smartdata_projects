﻿using AmaxIns.RepositoryContract.Notification;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection.Metadata;
using System.Text;

namespace AmaxIns.Repository.Notification
{
    public class NotificationRepository : BaseRepository, INotificationRepository
    {
        //private string message = "";
        public NotificationRepository(IConfiguration configuration) : base(configuration)
        {
            //  message = configuration.GetSection("NotificationMessage").Value; 
        }

        public string GetNotification()
        {
            var query = "select top 1 message from [Sync_DB].[dbo].GobalErrorMessage order by 1  desc";
            var result = GetFirstOrDefault<string>(query, commandTimeout: 0, commandType: CommandType.Text);
            return string.IsNullOrEmpty(result) ? " " : Convert.ToString(result);
        }



    }
}
