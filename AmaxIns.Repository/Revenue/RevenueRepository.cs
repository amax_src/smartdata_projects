﻿using AmaxIns.DataContract.Revenue;
using AmaxIns.RepositoryContract.Revenue;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Revenue
{
    public class RevenueRepository : BaseRepository, IRevenueRepository
    {
        public RevenueRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<string> GetDataRefreshDate()
        {
            string result = null;
            var query = "[spSyncDB_GetDataRefreshDate_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetFirstOrDefaultAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<RevenueModel>> GetRevenueData()
        {
            IEnumerable<RevenueModel> result = null;
            var query = "spSyncDB_GetConsolidatedRevenue";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<RevenueModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }


        public async Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData()
        {
            IEnumerable<RevenueDailyModel> result = null;
            var query = "spSyncDB_GetConsolidatedRevenuewithdate";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<RevenueDailyModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<TierPercentageModel>> GetTierPercentage()
        {
            IEnumerable<TierPercentageModel> result = null;
            var query = "select * from [dbo].[tblTierCommissionPercentage]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<TierPercentageModel>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<string>> Getmonths(string year)
        {
            IEnumerable<string> result = null;
            var query = @"select  paydate from [dbo].[tblConsolidatedRevenueAndPremiumDate] where payyear=2022 group by paydate
			                    order by  
	                            CASE
                                   WHEN	 paydate = 'January' THEN 1
                                   WHEN  paydate = 'February' THEN 2
                                   WHEN  paydate = 'March' THEN 3
                                   WHEN  paydate = 'April' THEN 4
                                   WHEN  paydate = 'May' THEN 5
                                   WHEN  paydate = 'June' THEN 6
                                   WHEN  paydate = 'July' THEN 7
	                               WHEN  paydate = 'August' THEN 8
                                   WHEN  paydate = 'September' THEN 9
                                   WHEN  paydate = 'October' THEN 10
                                   WHEN  paydate = 'November' THEN 11
                                   WHEN  paydate = 'December ' THEN 12
                              END ASC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<RevenuTodayModel>> GetRevenuToday()
        {
            IEnumerable<RevenuTodayModel> result = null;
            var query = @" select agencyName as AgencyName ,ROUND(dailybuscount,0) as PolicyCount,ROUND(dailyAgencyFee,0) as AgencyFee,ROUND(dailyPremium,0) as Premium from [dbo].[DailyProjectionRevenue]
                            where date = cast((DATEADD(HOUR, -1, getdate())) as date)";
            result = await GetAsync<RevenuTodayModel>(query, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<AmPmDepositModel>> GetAMPMDeposit()
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
            IEnumerable<AmPmDepositModel> result = null;
            var query = "sp_GetAmPmDeposit_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", newDT.AddDays(-1), DbType.String, ParameterDirection.Input);
            result = await GetAsync<AmPmDepositModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}