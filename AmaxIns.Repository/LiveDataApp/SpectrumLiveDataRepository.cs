﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.LiveDataApp
{
    public class SpectrumLiveDataRepository: BaseRepository, ISpectrumLiveDataRepository
    {
        public SpectrumLiveDataRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveData()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_SpectrumLiveData_sel]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        //public async Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataV1()
        //{
        //    IEnumerable<SpectrumLiveData> result = null;
        //    var query = "[spSyncDB_SpectrumLiveData_selV1]";
        //    result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
        //    return result;
        //}

        //public async Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWise()
        //{
        //    IEnumerable<SpectrumLiveData> result = null;
        //    var query = "[spSyncDB_StatsLiveDataAgentWise_sel]";
        //    result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
        //    return result;
        //}

        //public async Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWise(int weeklyId)
        //{
        //    IEnumerable<SpectrumLiveData> result = null;
        //    var query = "[spSyncDB_StatsWeeklyDataAgentWise_sel]";
        //    DynamicParameters parameter = new DynamicParameters();
        //    parameter.Add("@WeekId", weeklyId, DbType.String, ParameterDirection.Input);
        //    result = await GetAsync<SpectrumLiveData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
        //    return result;
        //}


        #region UpdatedSpectrumV1

        public async Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataV2()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_SpectrumLiveData_selV2]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseV2()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_StatsLiveDataAgentWise_selV2]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<SpectrumLiveHourlyModel>> GetStatsLiveDataAgentWiseHourlyV2()
        {
            IEnumerable<SpectrumLiveHourlyModel> result = null;
            var query = "[spSyncDB_StatsLiveDataAgentWiseHourly_selV2]";
            result = await GetAsync<SpectrumLiveHourlyModel>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWiseV2(SpectrumParam data)
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_StatsWeeklyDataAgentWise_selV2]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@WeekId", weeklyId, DbType.String, ParameterDirection.Input);
            parameter.Add("@startDate", data.date[0], DbType.Date, ParameterDirection.Input);
            parameter.Add("@endDate", data.date[1], DbType.Date, ParameterDirection.Input);
            result = await GetAsync<SpectrumLiveData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<SpectrumAgentCall>> GetSpectrumAgentCallDetails(string extension)
        {
            IEnumerable<SpectrumAgentCall> result = null;
            var query = @"select type,time_start,time_answer,time_release,duration_human,number,name,duration 
                            from tblStratusSepctrumInboundOutBoundCallsLive where extension = @extension ORDER BY time_start DESC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@extension", extension, DbType.String, ParameterDirection.Input);
            result = await GetAsync<SpectrumAgentCall>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<AgencyAgentLogLive>> GetSpectrumLiveAgentLogV2()
        {
            IEnumerable<AgencyAgentLogLive> result = null;
            var query = "[GetSpectrumLiveAgentLog]";
            result = await GetAsync<AgencyAgentLogLive>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumAgentExtensionLog>> GetSpectrumLiveAgentExtensionLogV2(string extension)
        {
            IEnumerable<SpectrumAgentExtensionLog> result = null;
            var query = @"select timestamp,action,loggedin_sec,available_sec,unavailable_sec,break_sec,lunch_sec
                            ,meeting_sec,other_sec,acw_sec,web_sec from SpectrumAgentLogLive
                            WHERE extension = @extension ORDER BY timestamp DESC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@extension", extension, DbType.String, ParameterDirection.Input);
            result = await GetAsync<SpectrumAgentExtensionLog>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<AgentCallTransfer>> GetCallTransferDetails(string extension, int AgencyId)
        {
            IEnumerable<AgentCallTransfer> result = null;

            var query = "[Sync_DB_SP_GetCallTransferDetails]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", AgencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@extenasion", extension, DbType.String, ParameterDirection.Input);
            result = await GetAsync<AgentCallTransfer>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<SpectrumAgentCall>> GetSpectrumExtensionWiseCallDetails(StratusExtensionParam stratusExtension)
        {
            IEnumerable<SpectrumAgentCall> result = null;
            var query = @"SELECT AWE.AgencyId agencyid,AWE.AgencyName Location,t.extension,type,time_start,time_answer,time_release,duration_human,number,name,duration,Date
                         FROM tblAgencyWithExtension AWE INNER JOIN (
                         select extension,      
                        type,time_start,time_answer,time_release,duration_human,number,name,duration 
                        ,callDate AS Date      
                         from tblStratusSepctrumInboundOutBoundCallsWithDetails 
                        WHERE  CAST(callDate AS DATE) =CAST(@date AS DATE) AND extension=@extension) t ON AWE.Extension=t.extension ORDER BY time_start DESC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@extension", stratusExtension.extension, DbType.String, ParameterDirection.Input);
            parameter.Add("@date", stratusExtension.date, DbType.String, ParameterDirection.Input);
            result = await GetAsync<SpectrumAgentCall>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }
        public async Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseVSTeam_selV2()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_StatsLiveDataAgentWiseVSTeam_selV2]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion



        #region CA_Spectrum_Live
        public async Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataCAV2()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_SpectrumDataCA_Live_Sel]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgentCallTransfer>> GetCallTransferDetailsCA(string extension, int AgencyId)
        {
            IEnumerable<AgentCallTransfer> result = null;

            var query = "[Sync_DB_SP_GetCallTransferDetailsCA]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@agencyId", AgencyId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@extenasion", extension, DbType.String, ParameterDirection.Input);
            result = await GetAsync<AgentCallTransfer>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseCAV2()
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_StatsLiveDataCAAgentWise_selV2]";
            result = await GetAsync<SpectrumLiveData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyAgentLogLive>> GetSpectrumLiveAgentLogCAV2()
        {
            IEnumerable<AgencyAgentLogLive> result = null;
            var query = "[GetSpectrumLiveAgentLogForCA]";
            result = await GetAsync<AgencyAgentLogLive>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWiseCAV2(SpectrumParam data)
        {
            IEnumerable<SpectrumLiveData> result = null;
            var query = "[spSyncDB_StatsWeeklyDataAgentWise_selCAV2]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@WeekId", weeklyId, DbType.String, ParameterDirection.Input);
            parameter.Add("@startDate", data.date[0], DbType.Date, ParameterDirection.Input);
            parameter.Add("@endDate", data.date[1], DbType.Date, ParameterDirection.Input);
            result = await GetAsync<SpectrumLiveData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<SpectrumAgentCall>> GetSpectrumExtensionWiseCallDetailsCA(StratusExtensionParam stratusExtension)
        {
            IEnumerable<SpectrumAgentCall> result = null;
            var query = @"SELECT AWE.AgencyId agencyid,AWE.AgencyName Location,t.extension,type,time_start,time_answer,time_release,duration_human,number,name,duration,Date
                         FROM tbl_spectrumCAAgency AWE INNER JOIN (
                         select extension,      
                        type,time_start,time_answer,time_release,duration_human,number,name,duration 
                        ,callDate AS Date      
                         from tblStratusSepctrumInboundOutBoundCallsWithDetails 
                        WHERE  CAST(callDate AS DATE) =CAST(@date AS DATE) AND extension=@extension) t ON AWE.Extension=t.extension ORDER BY time_start DESC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@extension", stratusExtension.extension, DbType.String, ParameterDirection.Input);
            parameter.Add("@date", stratusExtension.date, DbType.String, ParameterDirection.Input);
            result = await GetAsync<SpectrumAgentCall>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }
        #endregion
    }

}
