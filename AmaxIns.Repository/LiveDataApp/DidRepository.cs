﻿using AmaxIns.DataContract.LiveData.DidConsolatedData;
using AmaxIns.RepositoryContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace AmaxIns.Repository.LiveDataApp
{
    public class DidRepository : BaseRepository, IDidRepository
    {
        private const string gplexLoginUrl = "https://amax.gplex.com/index.php";
        private const string glexUser = "3003";
        private const string glexPassword = "Amax2025";
        private const string consolidatedDidUrl = "https://amax.gplex.com/index.php?task=get-report-data&act=didinboundlog2";
        public DidRepository(IConfiguration configuration) : base(configuration)
        {

        }
        private CookieContainer LoginGlexSite()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(gplexLoginUrl);
            request.Method = "Post";
            request.CookieContainer = new CookieContainer();
            byte[] data = Encoding.ASCII.GetBytes("user=" + glexUser + "&" + "pass=" + glexPassword);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
            string pageContent = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();
            return request.CookieContainer;
        }
        public List<DidNumberData> GetSummaryDIDData(string sday, string smonth, string syear, string eday, string emonth, string eyear)
        {
            CookieContainer cookies = new CookieContainer();
            cookies = LoginGlexSite();
            List<DidNumberData> didNumberData = new List<DidNumberData>();
            string start = syear + "-" + smonth + "-" + sday;
            string end = eyear + "-" + emonth + "-" + eday;
            var url = "https://amax.gplex.com/index.php?task=get-report-data&act=didinboundlog2&download_csv=true&isMultiSearch=true";
            url = url + "&ms=ms[start_time][from]=" + start + " 00:00%26ms[start_time][to]=" + end + " 23:59%26ms[did]=1800";
            url = url + "&cols={'did':'DID','num_calls':'Calls offered','num_ans':'Calls answered','perc_ans':' % of calls answered','call_duration':'Duration(H: M:S)'}";
            string FinalUrl = url.Replace("'", "\"");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FinalUrl);
            request.Method = "GET";
            request.CookieContainer = cookies;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
            string pageContent = myStreamReader.ReadToEnd();
            if(!string.IsNullOrWhiteSpace(pageContent))
            {
                List<string> _data = pageContent.Split('\n').ToList();
                for(int i=0;i<_data.Count;i++)
                {

                    if(i>0)
                    {
                        string[] csvArray = _data[i].Split(',');
                        if(csvArray.Length>1)
                        {
                            DidNumberData _csvdata = new DidNumberData();
                            for (int z = 0; z < csvArray.Length; z++)
                            {
                                switch (z)
                                {
                                    case 0:
                                        _csvdata.DID = csvArray[z];
                                        break;
                                    case 1:
                                        _csvdata.Callsoffered = csvArray[z];
                                        break;
                                    case 2:
                                        _csvdata.Callsanswered = csvArray[z];
                                        break;
                                    case 3:
                                        _csvdata.PercentageAns = csvArray[z];
                                        break;
                                    case 4:
                                        _csvdata.Duration = csvArray[z];
                                        break;
                                }
                               
                            }
                            didNumberData.Add(_csvdata);
                        }
                    }
                }
            }
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();
            return didNumberData;
        }

  
    }
}
