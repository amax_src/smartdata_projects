﻿using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.LiveDataApp
{
    public class AlpaBolQuotesRepository : BaseRepository, IAlpaBolQuotesRepository
    {
        ILogger _logger;
        public AlpaBolQuotesRepository(IConfiguration configuration, ILogger logger) : base(configuration)
        {
            _logger = logger;
        }

        public async Task<IEnumerable<BOLQuoteData>> GetAlpaDataBolData_Quotes(List<string> date, string state)
        {
            IEnumerable<BOLQuoteData> result = null;
            var query = @"GetAlpaBindOnlineQuotesData_Quotes";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", date[0], DbType.Date, ParameterDirection.Input);
            parameter.Add("@date2", date[1], DbType.Date, ParameterDirection.Input);
            parameter.Add("@state", state, DbType.String, ParameterDirection.Input);
            result = await GetAsync<BOLQuoteData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
