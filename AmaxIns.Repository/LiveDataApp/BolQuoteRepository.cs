﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.LiveDataApp
{

    public class BolQuoteRepository : BaseRepository, IBolQuoteRepository
    {
        ILogger _logger;
        public BolQuoteRepository(IConfiguration configuration, ILogger logger) : base(configuration)
        {
            _logger = logger;
        }

        public async Task<IEnumerable<BOLQuoteData>> GetDataBolData(List<string> date,string state)
        {
            IEnumerable<BOLQuoteData> result = null;
            //var query = @"select  distinct Date,QuoteUrl,CarrierName,CallCenterSent,BuyNowOption,BuyNowClick,
            //                PolicyNumber,AgencyFee,Time,Name,Surname,Email,PhoneNumber,a.state, q.isdelayed from [BindOnline_Quote] q join BindOnline_Customer c on q.id=c.BindOnline_QuoteId  join [dbo].[BindOnline_Address] a on c.id=a.[BindOnline_CustomerId] where [date] between @date and @date2";
            var query = @"GetBindOnlineQuotesData";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", date[0], DbType.Date, ParameterDirection.Input);
            parameter.Add("@date2", date[1], DbType.Date, ParameterDirection.Input);
            parameter.Add("@state", state, DbType.String, ParameterDirection.Input);
            result = await GetAsync<BOLQuoteData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<BOLQuoteData>> GetDataBolData_Quotes(List<string> date, string state)
        {
            IEnumerable<BOLQuoteData> result = null;
            //var query = @"select  distinct Date,QuoteUrl,CarrierName,CallCenterSent,BuyNowOption,BuyNowClick,
            //                PolicyNumber,AgencyFee,Time,Name,Surname,Email,PhoneNumber,a.state, q.isdelayed from [BindOnline_Quote] q join BindOnline_Customer c on q.id=c.BindOnline_QuoteId  join [dbo].[BindOnline_Address] a on c.id=a.[BindOnline_CustomerId] where [date] between @date and @date2";
            var query = @"GetBindOnlineQuotesData_Quotes";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", date[0], DbType.Date, ParameterDirection.Input);
            parameter.Add("@date2", date[1], DbType.Date, ParameterDirection.Input);
            parameter.Add("@state", state, DbType.String, ParameterDirection.Input);
            result = await GetAsync<BOLQuoteData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<BOLQuoteData> GetSingleQuote(string quoteNumber)
        {
            BOLQuoteData result = null; 
            var query = @"spSyncDB_GetSingleQuote";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@quoteNumber", quoteNumber, DbType.String, ParameterDirection.Input);
            result = await GetFirstOrDefaultAsync<BOLQuoteData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<EODLive>> GetEODLiveReport(List<string> date)
        {
            try
            {
                IEnumerable<EODLive> result = null;
                var query = @"sp_GetEODDataSet_sel";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@StartDate", date[0], DbType.Date, ParameterDirection.Input);
                parameter.Add("@EndDate", date[1], DbType.Date, ParameterDirection.Input);

                result = await GetAsync<EODLive>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (System.Exception ex)
            {

                return null;
            }

        }

        public async Task<IEnumerable<CustomerJourneyInfo>> CustomerJourneyInfoReport(List<string> date)
        {
            try
            {
                IEnumerable<CustomerJourneyInfo> result = null;
                var query = @"Sync_SP_BOLCustomerJourneyInfo";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@date", date[0], DbType.Date, ParameterDirection.Input);
                parameter.Add("@date2", date[1], DbType.Date, ParameterDirection.Input);

                result = await GetAsync<CustomerJourneyInfo>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (System.Exception ex)
            {

                return null;
            }

        }

        public async Task<IEnumerable<RetailCustomerJourneyInfo>> GetRetailCustomerJourneyInfoReport(List<string> date)
        {
            try
            {
                IEnumerable<RetailCustomerJourneyInfo> result = null;
                var query = @"Sync_SP_RetailCustomerJourneyInfo";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@date", date[0], DbType.Date, ParameterDirection.Input);
                parameter.Add("@date2", date[1], DbType.Date, ParameterDirection.Input);

                result = await GetAsync<RetailCustomerJourneyInfo>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (System.Exception ex)
            {

                return null;
            }

        }
    }
}
