﻿using AmaxIns.Common.Interface;
using AmaxIns.DataContract.LiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.LiveDataApp
{
    public class LivedataRepository : BaseRepository, ILivedataRepository
    {
        string key = string.Empty;

        public string ApiUrl
        {
            get
            {
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                DateTime NextDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.AddDays(1), targetZone);
                string dateString = newDT.Year + "-" + (newDT.Month < 10 ? "0" + newDT.Month.ToString() : newDT.Month.ToString()) + "-" + (newDT.Day < 10 ? "0" + (newDT.Day).ToString() : (newDT.Day).ToString());
                string NextdateString = newDT.Year + "-" + (NextDT.Month < 10 ? "0" + NextDT.Month.ToString() : NextDT.Month.ToString()) + "-" + (NextDT.Day < 10 ? "0" + (NextDT.Day).ToString() : (NextDT.Day).ToString());
                return "https://ipservices.getitc.com/iPwn/api/Reportpayment?$filter=payDate ge " + dateString + "T12:00:00Z and payDate le " + NextdateString + "T02:00:00Z";

                //string dateString = DateTime.Now.AddDays(-1).Year + "-" + (DateTime.Now.AddDays(-1).Month < 10 ? "0" + DateTime.Now.AddDays(-1).Month.ToString() : DateTime.Now.AddDays(-1).Month.ToString()) + "-" + (DateTime.Now.AddDays(-1).Day < 10 ? "0" + (DateTime.Now.AddDays(-1).Day).ToString() : (DateTime.Now.AddDays(-1).Day).ToString());
                //string NextdateString = DateTime.Now.AddDays(0).Year + "-" + (DateTime.Now.AddDays(0).Month < 10 ? "0" + DateTime.Now.AddDays(0).Month.ToString() : DateTime.Now.AddDays(0).Month.ToString()) + "-" + (DateTime.Now.AddDays(0).Day < 10 ? "0" + (DateTime.Now.AddDays(0).Day).ToString() : (DateTime.Now.AddDays(0).Day).ToString());
                //return "https://ipservices.getitc.com/iPwn/api/Reportpayment?$filter=payDate ge " + dateString + "T12:00:00Z and payDate le " + NextdateString + "T02:00:00Z";
            }
        }
        public LivedataRepository(IConfiguration configuration, IEmailService emailService) : base(configuration)
        {
            key = configuration.GetSection("LiveDataAppKey").Value;


        }
        public LiveDataRoot GetData(string url)
        {
            LiveDataRoot ldata = new LiveDataRoot();
            try
            {
                System.Net.WebRequest webRequest;
                if (string.IsNullOrWhiteSpace(url))
                {
                    webRequest = System.Net.WebRequest.Create(ApiUrl);
                }
                else
                {
                    webRequest = System.Net.WebRequest.Create(url);
                }

                if (webRequest != null)
                {

                    webRequest.Method = "GET";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", key);

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            ldata = JsonConvert.DeserializeObject<LiveDataRoot>(jsonResponse);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return ldata;
        }

        public async Task<IEnumerable<QuotesData>> GetQuotes()
        {
            IEnumerable<QuotesData> result = null;
            var query = "[sp_GetLastModifiedQuotes_sel]";
            result = await GetAsync<QuotesData>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyDIDData>> GetConsolidatedDIDData(string day,string month, string year, string endday, string endmonth, string endyear, string Did)
        {
            IEnumerable<AgencyDIDData> result = null;
            var query = "[sp_GetDiDCallMapping_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@did", Did, DbType.String, ParameterDirection.Input);
            parameter.Add("@day", day, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@year",year, DbType.String, ParameterDirection.Input);

            parameter.Add("@endday", endday, DbType.String, ParameterDirection.Input);
            parameter.Add("@endmonth", endmonth, DbType.String, ParameterDirection.Input);
            parameter.Add("@endyear", endyear, DbType.String, ParameterDirection.Input);
            result = await GetAsync<AgencyDIDData>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public List<ConsolidatedAgentCount> GetConSolidatedAgentloginData()
        {
            List<ConsolidatedAgentCount> ldata = new List<ConsolidatedAgentCount>();
            try
            {
                System.Net.WebRequest webRequest;
               
                    webRequest = System.Net.WebRequest.Create("http://18.222.69.131:8080/api/agentsPerHour");
               

                if (webRequest != null)
                {

                    webRequest.Method = "GET";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
             
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            ldata = JsonConvert.DeserializeObject<List<ConsolidatedAgentCount>>(jsonResponse);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return ldata;
        }

        public List<ConsolidatedAgentCount> GetAgencywiseAgentloginData()
        {
            List<ConsolidatedAgentCount> ldata = new List<ConsolidatedAgentCount>();
            try
            {
                System.Net.WebRequest webRequest;

                webRequest = System.Net.WebRequest.Create("http://18.222.69.131:8080/api/agentsPerLocation");


                if (webRequest != null)
                {

                    webRequest.Method = "GET";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            ldata = JsonConvert.DeserializeObject<List<ConsolidatedAgentCount>>(jsonResponse);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return ldata;
        }


       
    }
    
}
