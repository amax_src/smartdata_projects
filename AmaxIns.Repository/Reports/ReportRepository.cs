﻿using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Reports
{
    public class ReportRepository : BaseRepository, IReportRepository
    {
        public ReportRepository(IConfiguration configuration) : base(configuration) { }


        #region Avg-Agency-Fee
        public async Task<IEnumerable<AvgAgencyFee>> GetAvgAgencyFees(string Year, string month, string location)
        {
            var query = @"GetAvgAgencyFees";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AvgAgencyFee>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AvgAgencyFeeII>> GetAvgAgencyFeesII(string Year, string month, string location)
        {
            var query = @"GetAvgAgencyFeesII_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AvgAgencyFeeII>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<AvgAgencyFeeIII>> GetAvgAgencyFeesIII(string Year, string month, string location)
        {
            var query = @"GetAvgAgencyFeesIII_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AvgAgencyFeeIII>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion

        #region Agency-Break-Down
        // Agency Breakdown Report Methods -- sp_AgencyBreakdownI_sel
        public async Task<IEnumerable<AgencyBreakdownI>> GetAgencyBreakdownI(string Year, string month, string location)
        {
            var query = @"sp_AgencyBreakdownI_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AgencyBreakdownI>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyBreakdownII>> GetAgencyBreakdownII(string Year, string month, string location)
        {
            var query = @"sp_AgencyBreakdownII_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AgencyBreakdownII>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyBreakdownIII>> GetAgencyBreakdownIII(string Year, string month, string location)
        {
            var query = @"sp_AgencyBreakdownIII_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@months", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AgencyBreakdownIII>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion


       

        #region OverTimeReport
        public async Task<IEnumerable<OvertimeI>> GetOvertimeI(string Year, string month, string location)
        {
            try
            {
                var query = @"[spSyncDB_Get_OvertimeI]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
                var result = await GetAsync<OvertimeI>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IEnumerable<OvertimeII>> GetOvertimeII(string Year, string month, string location)
        {
            try
            {
                var query = @"[spSyncDB_Get_OvertimeII]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
                var result = await GetAsync<OvertimeII>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        public async Task<IEnumerable<OvertimeIII>> GetOvertimeIII(string Year, string month, string location)
        {
            var query = @"[spSyncDB_Get_OvertimeIII]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<OvertimeIII>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion

        

        // Tracker Report Methods
        public async Task<IEnumerable<TrackerRender>> GetRenterTracker(string Year, string month, string location)
        {
            var query = @"[sync_db_Tracker_Renter]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<TrackerRender>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<TrackerProduction>> GetProductionTracker(string Year, string month, string location)
        {
            try
            {
                var query = @"[sync_db_Tracker_Production]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
                var result = await GetAsync<TrackerProduction>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception ex)
            {
                return null;
                
            }
            
        }

        public async Task<IEnumerable<TrackerActiveCustomer>> GetActiveCustomerTracker(string Year, string month, string location)
        {
            var query = @"[sync_db_Tracker_Active_Customer]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<TrackerActiveCustomer>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        // Agent Performance

        public async Task<IEnumerable<AgentPerformance>> GetAgentPerformaceI(string Year, string month, string location)
        {
            var query = @"[spSyncDB_Get_AgentPerformanceI]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AgentPerformance>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgentPerformance>> GetAgentPerformaceII(string Year, string month, string location)
        {
            var query = @"[spSyncDB_Get_AgentPerformanceII]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<AgentPerformance>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}
