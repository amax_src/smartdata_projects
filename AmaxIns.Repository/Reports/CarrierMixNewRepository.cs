﻿using AmaxIns.RepositoryContract.Reports;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Reports
{
    public class CarrierMixNewRepository : BaseRepository, ICarrierMixNewRepository
    {
        public CarrierMixNewRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixI(int Year, string month)
        {
            try
            {
                var query = @"[sync_db_sp_Carrier_Mix_I]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                var result = await Query(query, parameter, commandType: CommandType.StoredProcedure);
                return result.Cast<IDictionary<string, object>>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixPremium(int Year, string month)
        {
            var query = @"[sync_db_Carrier_Mix_Premium]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            var result = await Query(query, parameter, commandType: CommandType.StoredProcedure);
            return result.Cast<IDictionary<string, object>>();
        }
        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixAgencyFee(int Year, string month)
        {
            var query = @"[sync_db_sp_Carrier_Mix_AgencyFee]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            var result = await Query(query, parameter, commandType: CommandType.StoredProcedure);
            return result.Cast<IDictionary<string, object>>();
        }
    }
}
