﻿using System;
using System.Collections.Generic;
using System.Text;
using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Threading.Tasks;


namespace AmaxIns.Repository.Reports
{
    public class RentersReportRepository : BaseRepository, IRentersReportRepository
    {
        public RentersReportRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<RentersData>> GetRenterData(string Year, string month, string location)
        {
            var query = @"GetRenterData";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<RentersData>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<ActiveCustomerData>> GetActiveCustomerData(string Year, string month, string location)
       {
            var query = @"GetActiveCustomer";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<ActiveCustomerData>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}





