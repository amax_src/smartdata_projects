﻿using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Reports
{
    public class UniqueCreatedQuotesRepository : BaseRepository, IUniqueCreatedQuotesRepository
    {
        public UniqueCreatedQuotesRepository(IConfiguration configuration) : base(configuration) { }
        public async Task<IEnumerable<UniqueCreatedQuotes>> GetUniqueCreatedQuotes(int Year, string month, string location)
        {
            // spSyncDB_Get_Unique_Created_Quotes LIVE(old)
            // spSyncDB_Get_Unique_Created_Quotes_Dev DEV
            //spSyncDB_Get_Unique_Created_Quotes_LiveProd (live)New
            //var query = @"[spSyncDB_Get_Unique_Created_Quotes_LiveProd]";
            var query = @"[spSyncDB_Get_Unique_Created_Quotes_Prod_V1]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", Year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            var result = await GetAsync<UniqueCreatedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<Market>> GetAllMarket()
        {
            var query = @"select DISTINCT Market AS MarketName from WebApplication.dbo.marketMapping ORDER BY Market ASC";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<Market>(query, null, commandType: CommandType.Text);
            return result;
        }
    }
}
