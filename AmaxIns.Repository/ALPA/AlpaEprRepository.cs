﻿using AmaxIns.DataContract.EPR;
using AmaxIns.RepositoryContract.ALPA;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.ALPA
{
    public class AlpaEprRepository : BaseRepository, IAlpaEprRepository
    {
        private string year = "2021";
        public AlpaEprRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<EPRModel>> GetEPRData()
        {
            IEnumerable<EPRModel> result = null;
            var query = "Alpa_DB..[spALPADB_GetEPRPowerBI_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            result = await GetAsync<EPRModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<EPRModel>> GetEPRTotalData()
        {
            IEnumerable<EPRModel> result = null;
            var query = "Alpa_DB..[spALPADB_GetEPRPowerBITotal_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            result = await GetAsync<EPRModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }


        public async Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string _year, string location, string _selectedagent)
        {
            IEnumerable<EPRGraphModel> result = null;

            var query = "Alpa_DB..[ALPADB_sp_GetEPRGraph]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", _year, DbType.String, ParameterDirection.Input);
            parameter.Add("@location", location, DbType.String, ParameterDirection.Input);
            parameter.Add("@selectedagent", _selectedagent, DbType.String, ParameterDirection.Input);
            result = await GetAsync<EPRGraphModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<string>> GetEPRmonths()
        {
            IEnumerable<string> result = null;
            var query = @"select   DATENAME(MONTH,paydate) from  ip_amax40.dbo.companyinfo 
			                    right join (ip_amax40.dbo.clientinfo
			                    right join (ip_amax40.dbo.agentinfo
			                    right join ip_amax40.dbo.paymentinfo 
			                    on agentinfo.agentid = paymentinfo.agentid) 
			                    on clientinfo.clientid = paymentinfo.clientid) 
			                    on companyinfo.companyid = clientinfo.companyid 
			                    where   (companyinfo.deleted = 0 or companyinfo.deleted is null) 
			                    and paymentinfo.bdeleted = 0
			                    and paynotes not like '%iou%' 
			                    and year(paydate) =@Year
			                    and void = 0  group by  DATENAME(MONTH,paydate) 
			                    order by  
	                            CASE
                                   WHEN DATENAME(MONTH,paydate) = 'January' THEN 1
                                   WHEN  DATENAME(MONTH,paydate) = 'February' THEN 2
                                   WHEN  DATENAME(MONTH,paydate) = 'March' THEN 3
                                   WHEN  DATENAME(MONTH,paydate) = 'April' THEN 4
                                   WHEN  DATENAME(MONTH,paydate) = 'May' THEN 5
                                   WHEN  DATENAME(MONTH,paydate) = 'June' THEN 6
                                   WHEN  DATENAME(MONTH,paydate) = 'July' THEN 7
	                               WHEN  DATENAME(MONTH,paydate) = 'August' THEN 8
                                   WHEN  DATENAME(MONTH,paydate) = 'September' THEN 9
                                   WHEN  DATENAME(MONTH,paydate) = 'October' THEN 10
                                   WHEN  DATENAME(MONTH,paydate) = 'November' THEN 11
                                   WHEN  DATENAME(MONTH,paydate) = 'December ' THEN 12
                              END ASC";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }
    }
}
