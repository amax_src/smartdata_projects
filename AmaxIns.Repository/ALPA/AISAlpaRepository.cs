﻿using AmaxIns.DataContract.AIS;
using AmaxIns.RepositoryContract.ALPA;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.ALPA
{
    public class AISAlpaRepository : BaseRepository, IAISAlpaRepository
    {
        public AISAlpaRepository(IConfiguration configuration) : base(configuration)
        {
        }
        public async Task<IEnumerable<AISModel>> GetAISData()
        {
            IEnumerable<AISModel> result = null;
            var query = "Alpa_DB..[spAlpaDB_GetAisData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<AISModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
