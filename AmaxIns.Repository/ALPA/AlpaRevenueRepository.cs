﻿using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using AmaxIns.RepositoryContract.ALPA;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.ALPA
{
    public class AlpaRevenueRepository : BaseRepository, IAlpaRevenueRepository
    {
        public AlpaRevenueRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<RevenueModel>> GetAlpaRevenueData()
        {
            IEnumerable<RevenueModel> result = null;
            var query = "Alpa_DB..[spAlpADB_GetConsolidatedRevenue]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<RevenueModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData()
        {
            IEnumerable<RevenueDailyModel> result = null;
            var query = "Alpa_DB..[spALPADB_GetConsolidatedRevenuewithdate]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<RevenueDailyModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<RevenuTodayModel>> GetRevenuToday()
        {
            IEnumerable<RevenuTodayModel> result = null;
            var query = @" select agencyName as AgencyName ,ROUND(dailybuscount,0) as PolicyCount,ROUND(dailyAgencyFee,0) as AgencyFee,ROUND(dailyPremium,0) as Premium from [dbo].[DailyProjectionRevenue]
                            where date = cast((DATEADD(HOUR, -1, getdate())) as date)";
            result = await GetAsync<RevenuTodayModel>(query, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }


        public async Task<IEnumerable<DailyTransactionPayments>> DailyTransactionPayments(QuotesParam quotesParam, string month = null, string year = null)
        {
            IEnumerable<DailyTransactionPayments> result = new List<DailyTransactionPayments>();
            try
            {
                string list = string.Join(",", quotesParam.Agencyids).Replace("'", "''");
                string datelist = string.Join(",", quotesParam.dates).Replace("'", "''");

                var query = @"Alpa_DB..[ALPA_SP_DailyTransactionPayments]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@pdates", datelist, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
                result = await GetAsync<DailyTransactionPayments>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
    }
}
