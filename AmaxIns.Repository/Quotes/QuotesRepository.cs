﻿using AmaxIns.Common.Interface;
using AmaxIns.DataContract.Quotes;
using AmaxIns.RepositoryContract.Quotes;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Repository.Quotes
{
    public class QuotesRepository : BaseRepository, IQuotesRepository
    {
        IEmailService _emailService;

        public QuotesRepository(IConfiguration configuration, IEmailService emailService) : base(configuration)
        {
            _emailService = emailService;
        }

        public async Task<IEnumerable<string>> GetPaydate(string year, string month)
        {
            IEnumerable<string> result = null;
            var query = @"select distinct CONVERT(varchar(10), paydate, 110) as paydate from IP_AMAX30.dbo.paymentInfo where Cast(paydate  as date)< getdate() 
                            and DATEPART(year,paydate)=@Year and DateName(month,paydate)=@Month order by 1 desc ";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@Month", month, DbType.String, ParameterDirection.Input);
            result = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<QuotesModel>> GetBoundQuotes(string paydate)
        {
            IEnumerable<QuotesModel> result = null;
            var query = @"spSyncDB_GetboundQuotesData_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", paydate, DbType.Date, ParameterDirection.Input);
            result = await GetAsync<QuotesModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }


        public async Task<IEnumerable<QuotesModel>> GetBoundNoTurboratorQuotes(string paydate)
        {
            IEnumerable<QuotesModel> result = null;
            var query = @"[spSyncDB_GetboundQuotesNoTurboratorData_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", paydate, DbType.Date, ParameterDirection.Input);
            result = await GetAsync<QuotesModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<QoutesHistory>> GetQuotesHistory()
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
            IEnumerable<QoutesHistory> result = null;
            var query = @"[sp_GetQuoteHistory_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@date", newDT, DbType.Date, ParameterDirection.Input);
            try
            {

                result = await GetAsync<QoutesHistory>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<IEnumerable<ModifiesQuotesReport>> GetModifiedQuotesReport()
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
            IEnumerable<ModifiesQuotesReport> result = null;
            var query = @"[sp_GetLastModifiedQuotesReport_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Date", newDT, DbType.Date, ParameterDirection.Input);

            try
            {
                result = await GetAsync<ModifiesQuotesReport>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _emailService.SendEmail("Expection in Linked Server", ex.Message + " " + ex.StackTrace.ToString(), "hammadahmed642@gmail.com", "azam@amaxinsurance.com", "ravikumar318@gmail.com");
            }

            return result;
        }

        public async Task<IEnumerable<QuotesSale>> GetQuotesSale()
        {
            IEnumerable<QuotesSale> result = new List<QuotesSale>();
            var query = @"[sp_SyncDB_IPQuotes_and_Sale]";
            DynamicParameters parameter = new DynamicParameters();
            result = await GetAsync<QuotesSale>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotesNew(List<string> Agencyids, string month = null, string year = null)
        {
            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            try
            {
                string list = string.Join(",", Agencyids).Replace("'", "''");

                if (string.IsNullOrEmpty(month))
                {
                    month = "1";
                }
                if (string.IsNullOrEmpty(year))
                {
                    year = "2022";
                }

                var query = @"[Sync_DB_SP_NewAndModifyQuotes_New_Dev]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@agencyid", list, DbType.String, ParameterDirection.Input);
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);

                result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetDistinctDateQuotes(string month = null, string year = null)
        {
            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            try
            {
                if (string.IsNullOrEmpty(month))
                {
                    month = "1";
                }
                if (string.IsNullOrEmpty(year))
                {
                    year = "2022";
                }

                var query = @"IF(@year=2021 OR @year=2020)
                                BEGIN
                                 --SELECT DISTINCT cast(q.CreateDate as date) as CreateDate  from  NewModifieQuoteData q  WHERE month(createdate)=@month AND YEAR(createdate)=@year 
                                       -- Declare @year int = 2021, @month int = 2;
                                        WITH numbers
                                        as
                                        (
                                            Select 1 as value
                                            UNion ALL
                                            Select value + 1 from numbers
                                            where value + 1 <= Day(EOMONTH(datefromparts(@year,@month,1)))
                                        )
                                        SELECT CAST(datefromparts(@year,@month,numbers.value) AS DATE) CreateDate FROM numbers
                                END
                                ELSE
                                BEGIN
	                               -- select DISTINCT CreateDate from NewAndModifiedQuotesNew_Dev WHERE MONTH(CreateDate)=@month AND YEAR(CreateDate)=@year ORDER BY CreateDate

                                SELECT DISTINCT CreateDate FROM (
		                                SELECT CAST(LastModifyQuoteDate AS DATE) CreateDate FROM Sync_DB..tblNewAndModifyQuotesNew_Prod_V1 WHERE MONTH(LastModifyQuoteDate)=@month AND YEAR(LastModifyQuoteDate)=@year 
		                                UNION ALL
		                                SELECT CAST(LastModifyQuoteDate AS DATE) CreateDate FROM Sync_DB..tblNewAndModifyQuotesModified_Prod_V1  WHERE MONTH(LastModifyQuoteDate)=@month AND YEAR(LastModifyQuoteDate)=@year 
		                                ) t   ORDER BY CreateDate


                                END";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);

                result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotesDev2021(List<string> Agencyids, string month = null, string year = null)
        {
            string list = string.Join(",", Agencyids).Replace("'", "''");

            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            var query = @"[Sync_SP_GetNewModiFiedQuotes_Dev2021]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@agencyId", list, DbType.String, ParameterDirection.Input);

            result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotes(List<string> Agencyids, string month = null, string year = null)
        {
            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            if (year == "2022")
            {
                return GetNewModiFiedQuotesNew(Agencyids, month, year).Result;
            }
            else if (year == "2020" || year == "2021")
            {
                return GetNewModiFiedQuotesOld(Agencyids, month, year).Result;
            }
            else
            {
                return result;
            }
        }

        public async Task<int> ModifiedQuotesSoldTiles(QuotesParam quotesParam, string month = null, string year = null)
        {
            string list = string.Join(",", quotesParam.Agencyids).Replace("'", "''");
            string datelist = string.Join(",", quotesParam.dates).Replace("'", "''");

            if (string.IsNullOrEmpty(month))
            {
                month = "1";
            }
            if (string.IsNullOrEmpty(year))
            {
                year = "2023";
            }
            int result = 0;

            var query = @"[Sp_GetNewAndModifiedQuotesForUI_Tiles_DEV]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@pdates", datelist, DbType.String, ParameterDirection.Input);
            parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
            parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            result = await GetFirstOrDefaultAsync<int>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewAndModifiedData_New(QuotesParam quotesParam, string month = null, string year = null)
        {
            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            try
            {
                if (year == "2020")
                {
                    //return GetNewModiFiedQuotesOld(quotesParam.Agencyids, month, year).Result;
                    return GetNewModiFiedQuotesDev2021(quotesParam.Agencyids, month, year).Result;
                }
                else if(year == "2021" || year == "2022" || year == "2023" || year == "2024")
                {
                    string list = string.Join(",", quotesParam.Agencyids).Replace("'", "''");
                    string datelist = string.Join(",", quotesParam.dates).Replace("'", "''");

                    if (string.IsNullOrEmpty(month))
                    {
                        month = "1";
                    }
                    if (string.IsNullOrEmpty(year))
                    {
                        year = "2023";
                    }
                    //var query = @"[Sp_GetNewAndModifiedQuotesForOneDate_LiveProd_New]";
                    //var query = @"[Sp_GetNewAndModifiedQuotesForUI_Prod]";
                    var query = @"[Sp_GetNewAndModifiedQuotesForUI_Prod_V1]";
                    DynamicParameters parameter = new DynamicParameters();
                    parameter.Add("@pdates", datelist, DbType.String, ParameterDirection.Input);
                    parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
                    parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                    parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
                    result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);

                    //if (string.IsNullOrEmpty(datelist))
                    //{
                    //    var query = @"[Sp_GetNewAndModifiedQuotesForMonth_Dev]";
                    //    DynamicParameters parameter = new DynamicParameters();
                    //    parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
                    //    result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                    //}
                    //else if (quotesParam.dates.Count > 1)
                    //{
                    //    var query = @"[Sp_GetNewAndModifiedQuotesForMoreThanOneDay_Dev]";
                    //    DynamicParameters parameter = new DynamicParameters();
                    //    parameter.Add("@dates", datelist, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
                    //    result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                    //}
                    //else if (quotesParam.dates.Count == 1)
                    //{
                    //    var query = @"[Sp_GetNewAndModifiedQuotesForOneDate_Dev]";
                    //    DynamicParameters parameter = new DynamicParameters();
                    //    parameter.Add("@dates", datelist.TrimEnd(','), DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@AgencyId", list, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                    //    parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
                    //    result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                    //}
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotesOld(List<string> Agencyids, string month = null, string year = null)
        {
            string list = string.Format("'{0}'", string.Join("','", Agencyids.Select(i => i.Replace("'", "''"))));

            IEnumerable<NewModifiedQuotes> result = new List<NewModifiedQuotes>();
            var query = @"select distinct
	                        q.FirstName,
	                        q.Lastname,
	                        q.AgencyName,
	                        ai.Agencyid,
	                        q.AgencyAddress,
	                        q.QuoteType,
	                        q.Zip,
	                        q.Quoteid,
	                        q.Phone,
	                        q.PolicyNumber,
                            cast(q.CreateDate as date) as CreateDate,
                            cast(q.TCreateDate as date)as TCreateDate,
                            cast(q.TModifiedDate as date) as TModifiedDate
                         from  NewModifieQuoteData q join [IP_AMAX30].[dbo].[agencyInfo] 
                        ai on q.AgencyName=ai.AgencyName and ai.deleted=0 
                        where ai.agencyid in (" + list + ")" + (string.IsNullOrEmpty(month) ? " " : "and month(createdate) = @month") + " "
                        + (string.IsNullOrEmpty(year) ? " " : "and Year(createdate) = @year") +
                        " order by q.CreateDate asc";

            DynamicParameters parameter = new DynamicParameters();
            if (!string.IsNullOrEmpty(month))
            {
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
            }
            if (!string.IsNullOrEmpty(year))
            {
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);
            }
            result = await GetAsync<NewModifiedQuotes>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<QuotesProjection>> GetQuotesProjection()
        {
            IEnumerable<QuotesProjection> result = new List<QuotesProjection>();
            var query = @"select 
                                AgencyName
                                ,AgencyId
                                ,ProjectionYear
                                ,ProjectionMonthId
                                ,ProjectionMonth
                                ,ROUND(CreatedQuotesProjection,0) CreatedQuotesProjection
                                ,(CreatedQuotesCR*100) CreatedQuotesCR
                                ,ROUND(ModifiedQuotesProjection,0) ModifiedQuotesProjection
                                ,(ModifiedQuotesCR *100) ModifiedQuotesCR
                                from AgencyLocationByQuotesProjection Order BY AgencyName  ASC";

            DynamicParameters parameter = new DynamicParameters();
          
            result = await GetAsync<QuotesProjection>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return result;
        }

        public async Task<IEnumerable<NewAndModifiedQuotesProj>> GetNewModiFiedQuotesProjection(string month = null, string year = null)
        {
            IEnumerable<NewAndModifiedQuotesProj> result = new List<NewAndModifiedQuotesProj>();
            try
            {
                
                if (string.IsNullOrEmpty(month))
                {
                    month = "1";
                }
                if (string.IsNullOrEmpty(year))
                {
                    year = "2022";
                }

                //var query = @"[sp_NewAndModifiedQuotesProjection_LiveProd]";
                var query = @"[sp_NewAndModifiedQuotesProjection_Prod_V1]";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@month", month, DbType.String, ParameterDirection.Input);
                parameter.Add("@year", year, DbType.String, ParameterDirection.Input);

                result = await GetAsync<NewAndModifiedQuotesProj>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

    }
}
