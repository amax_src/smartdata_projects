﻿using AmaxIns.RepositoryContract.Agents;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.Agents
{
    public class AgentRepository : BaseRepository, IAgentRepository
    {
        public AgentRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<string>> GetAgents()
        {
            var query = @"select p.AgentId from 
                           (Select  
	                        Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int) as AgentId
                            from IP_AMAX30.[dbo].[agentInfo]
                            where agentactive = 1	and 
	                        Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int)!= 0  
	                        group by Cast(Replace( left (agentName,CHARINDEX(':',agentName)) ,':','') as int)) p ";
            DynamicParameters parameter = new DynamicParameters();
            var Agent = await GetAsync<string>(query, parameter, commandTimeout: 0, commandType: CommandType.Text);
            return Agent;
        }
    }
}
