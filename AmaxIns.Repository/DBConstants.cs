﻿namespace AmaxIns.Repository
{
    public class DBConstants
    {
        #region MaxBI Reports
        public const string MaxBIReport_CarrierAgent = "API_MaxBIReport_CarrierAgent";
        public const string MaxBIReport_AFeePremiumPerPolicy = "API_MaxBIReport_AFeePremiumPerPolicy";
        public const string MaxBIReport_AFeePremiumPerPolicySummary = "API_MaxBIReport_AFeePremiumPerPolicy_Summary";
        public const string MaxBIReport_NewModifiedQuotes = "API_MaxBIReport_NewModifiedQuotes";
        public const string MaxBIReport_NewModifiedQuotesClosingRatio = "API_MaxBIReport_NewModifiedQuotesClosingRatio";
        public const string MaxBIReport_NewModifiedQuotesSummary = "API_MaxBIReport_NewModifiedQuotes_Summary";
        public const string MaxBIReport_NewModifiedQuotesClosingRatioSummary = "API_MaxBIReport_NewModifiedQuotesClosingRatio_Summary";
        public const string MaxBIReport_InboundOutboundCalls = "API_MaxBIReport_InboundOutboundCalls";
        public const string MaxBIReport_InboundOutboundCallsSummary = "API_MaxBIReport_InboundOutboundCalls_Summary";
        public const string MaxBIReport_GetCatalogs = "API_MaxBIReport_GetCatalogs";
        public const string MaxBIReport_GetHierarchyLevel = "API_MaxBIReport_GetHierarchyLevel";

        public const string MaxBIReport_GetPayrollDashboardFilterData = "API_MaxBIReport_GetPayrollDashboardFilterData";
        public const string MaxBIReport_GetPayrollDashboardData = "API_MaxBIReport_GetPayrollDashboardData";

        public const string MaxBIReport_GetmployeeSensitiveInfo = "API_MaxBIReport_GetmployeeSensitiveInfo";

        public const string MaxBIReport_GetAgentHourlyProduction = "API_MaxBIReport_GetAgentHourlyProduction";


        public const string MaxBIReport_NewModifiedQuotesForEPR = "API_MaxBIReport_NewModifiedQuotesForEPR";
        public const string MaxBIReport_NewModifiedQuotesClosingRatioForEPR = "API_MaxBIReport_NewModifiedQuotesClosingRatioForEPR";
        #endregion
    }
}
