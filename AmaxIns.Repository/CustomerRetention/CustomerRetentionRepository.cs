﻿using AmaxIns.DataContract.CustomerRetention;
using AmaxIns.RepositoryContract.CustomerRetention;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.CustomerRetention
{
    public class CustomerRetentionRepository : BaseRepository, ICustomerRetentionRepository
    {
        public CustomerRetentionRepository(IConfiguration configuration) : base(configuration)
        {
            
        }

        public async Task<IEnumerable<CustomerRetentions>> GetStoreManagersReports(string startDate,string endDate,int storeManagerId,int AgencyId)
        {

            IEnumerable<CustomerRetentions> result = null;
            try
            {
                var query = "sp_GetStoreManagersReports";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@startDate", startDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@endDate", endDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@storeManagerId", storeManagerId, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@agencyId", AgencyId, DbType.Int32, ParameterDirection.Input);
                result = await GetAsync<CustomerRetentions>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }
           
            return result;
        }

        public async Task<IEnumerable<CustomerRetentionCountForZone>> StoreManagerCustomerRetentionCountForZone(string startDate, string endDate, int storeManagerId)
        {

            IEnumerable<CustomerRetentionCountForZone> result = null;
            try
            {
                var query = "sp_StoreManagerCustomerRetentionCountForZone";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@startDate", startDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@endDate", endDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@storeManagerId", storeManagerId, DbType.Int32, ParameterDirection.Input);
                result = await GetAsync<CustomerRetentionCountForZone>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }

            return result;
        }

        public async Task<IEnumerable<CustomerRetentionCountForZone>> ZonalManagerCustomerRetentionCountForZone(string startDate, string endDate, int ZMID, string AgencyId)
        {

            IEnumerable<CustomerRetentionCountForZone> result = null;
            try
            {
                var query = "sp_ZonalManagerCustomerRetentionCountForZone";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@startDate", startDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@endDate", endDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@ZMID", ZMID, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@AgencysId", AgencyId, DbType.String, ParameterDirection.Input);
                result = await GetAsync<CustomerRetentionCountForZone>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }

            return result;
        }
        public async Task<IEnumerable<CustomerRetentionCountForRegion>> RegionalManagerCustomerRetentionCount(string startDate, string endDate, int RMID, string AgencyId)
        {

            IEnumerable<CustomerRetentionCountForRegion> result = null;
            try
            {
                var query = "sp_RegionalManagerCustomerRetentionCount";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@startDate", startDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@endDate", endDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@RMID", RMID, DbType.Int32, ParameterDirection.Input);
                parameter.Add("@AgencysId", AgencyId, DbType.String, ParameterDirection.Input);
                result = await GetAsync<CustomerRetentionCountForRegion>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }

            return result;
        }

        public async Task<IEnumerable<CustomerRetentionCountForHod>> HODCustomerRetentionCount(string startDate, string endDate,string AgencyId)
        {

            IEnumerable<CustomerRetentionCountForHod> result = null;
            try
            {
                var query = "sp_HODCustomerRetentionCount";
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@startDate", startDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@endDate", endDate, DbType.String, ParameterDirection.Input);
                parameter.Add("@AgencysId", AgencyId, DbType.String, ParameterDirection.Input);
                result = await GetAsync<CustomerRetentionCountForHod>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {

                throw;
            }

            return result;
        }
    }
}
