﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Repository.User
{
    public class AlpaHierarchyRepository : BaseRepository, IAlpaHierarchyRepository
    {
        public AlpaHierarchyRepository(IConfiguration configuration) : base(configuration)
        {

        }

        #region SaleDirector
        public async Task<IEnumerable<SaleDirectorModel>> GetAllAlpaSaleDirector()
        {
            var query = "Alpa_DB..[spAlpaDB_GetSaleDirectors_sel]";
            DynamicParameters parameter = new DynamicParameters();
            var result = await GetAsync<SaleDirectorModel>(query, null, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion

        #region RegionalManager
        public async Task<IEnumerable<RegionalManagerModel>> RegionalManagerALPA()
        {
            var query = "Alpa_DB..[spAlpaDB_GetRegionalManagers_sel]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@Calling", "I", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<RegionalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion

        #region ZonalManager
        public async Task<IEnumerable<ZonalManagerModel>> ZonalManagerALPA()
        {
            var query = "Alpa_DB..[spAlpaDB_GetZonalManagers_sel]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@Status", "Active", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<ZonalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
        #endregion

        public async Task<IEnumerable<AgencyModel>> GetAllAgency()
        {
            try
            {
                var query = "Alpa_DB..[spAlpa_GetAlpaAgencyInfo_sel]";
                DynamicParameters parameter = new DynamicParameters();
                var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception ex)
            {

                return null;
            }
           
        }

        public async Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id)
        {
            var query = "Alpa_DB..[spAlpaDB_GetRegionalManagersBySD_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@SDId", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<RegionalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id)
        {
            var query = "Alpa_DB..[spAlpaDB_GetZonalManagersByRM_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<ZonalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(int RegionalManagerId, int ZonalManagerId)
        {
            var query = "Alpa_DB..[spAlpa_GetAlpaAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", RegionalManagerId, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@ZMID", ZonalManagerId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetbyZonalManager(int ZonalManager)
        {
            var query = "Alpa_DB..[spAlpa_GetAlpaAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@ZMID", ZonalManager, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetAllLocationsRegional(int RegionalManagerId)
        {
            var query = "Alpa_DB..[spAlpa_GetAlpaAgencyInfo_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", RegionalManagerId, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id)
        {

            var query = "select AgencyId,AgencyName,Isactive from DirectoreyStoreManageAgencyMapping where id=@Id and isactive=1";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Id", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetAsync<AgencyModel>(query, parameter, commandType: CommandType.Text);
            return result;
        }
    }
}
