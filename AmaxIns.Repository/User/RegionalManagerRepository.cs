﻿using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.User
{
    public class RegionalManagerRepository : BaseRepository, IRegionalManagerRepository
    {
        public RegionalManagerRepository(IConfiguration configuration) : base(configuration)
        {
         
        }

        public async Task<IEnumerable<RegionalManagerModel>> GetAll()
        {
            var query = "[spSyncDB_GetRegionalManagers_sel1]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@Calling", "I", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<RegionalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<RegionalManagerModel> GetById(int Id)
        {
            var query = "[spSyncDB_GetRegionalManagers_sel1]";
            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@Calling", "I", DbType.String, ParameterDirection.Input);
            parameter.Add("@RMID", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<RegionalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id)
        {
            var query = "[spSyncDB_GetRegionalManagers_sel1]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@sdid", Id, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Status", "Active", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<RegionalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}
