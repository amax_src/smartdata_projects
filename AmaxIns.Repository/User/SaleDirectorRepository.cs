﻿using System.Collections.Generic;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.User
{
    public class SaleDirectorRepository : BaseRepository, ISaleDirectorRepository
    {
        public SaleDirectorRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<IEnumerable<SaleDirectorModel>> GetAll()
        {
            var query = "[spSyncDB_GetSaleDirectors_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Calling", "I", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<SaleDirectorModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<SaleDirectorModel> GetById(int Id)
        {
            var query = "[spSyncDB_GetSaleDirectors_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Calling", "I", DbType.String, ParameterDirection.Input);
            parameter.Add("@RMID", Id, DbType.Int32, ParameterDirection.Input);
            var result = await GetFirstOrDefaultAsync<SaleDirectorModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
