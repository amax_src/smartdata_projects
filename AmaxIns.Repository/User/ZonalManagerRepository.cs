﻿using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AmaxIns.Repository.User
{
    public class ZonalManagerRepository : BaseRepository,IZonalManagerRepository
    {

        public ZonalManagerRepository(IConfiguration configuration) : base(configuration)
        {
          
        }
        public async Task<IEnumerable<ZonalManagerModel>> GetAll()
        {
            var query = "[spSyncDB_GetZonalManagers_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Status", "Active", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<ZonalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<ZonalManagerModel> GetById(int Id)
        {
            var query = "[spSyncDB_GetZonalManagers_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@ZMID", Id, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Status", "Active", DbType.String, ParameterDirection.Input);
            
            var result = await GetFirstOrDefaultAsync<ZonalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id)
        {
            var query = "[spSyncDB_GetZonalManagers_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@RMID", Id, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@Status", "Active", DbType.String, ParameterDirection.Input);
            var result = await GetAsync<ZonalManagerModel>(query, parameter, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
