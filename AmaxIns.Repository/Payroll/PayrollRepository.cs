﻿using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.Payroll;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace AmaxIns.Repository.Payroll
{
    public class PayrollRepository : BaseRepository, IPayrollRepository
    {
        private string year = "2019";
        public PayrollRepository(IConfiguration configuration) : base(configuration)
        {
            year = configuration.GetSection("Year").Value;//DateTime.Now.Year.ToString();
            year = "2018";
        }

        public async Task<IEnumerable<PayRollModel>> GetPayRoleBudgetedData()
        {
            IEnumerable<PayRollModel> result = null;
            var query = "[spSyncDB_GetPayrollBudgeted_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@PayrollYear", year, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<PayRollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<IEnumerable<PayRollModel>> GetPayRoleActualData()
        {
            IEnumerable<PayRollModel> result = null;
            var query = "[spSyncDB_GetPayrollActual_sel]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@PayrollYear", year, DbType.Int32, ParameterDirection.Input);
            result = await GetAsync<PayRollModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<ConsolidatedTileData> GetPayRoleTilesConsolidatedData()
        {
            ConsolidatedTileData result = new ConsolidatedTileData();

            IEnumerable<PayrollTiles> PayrollAndOverTimePerPolicy = null;
            IEnumerable<PayrollTiles> PayrollPerPolicy = null;
            IEnumerable<PayrollTiles> TransPerPolicy = null;
            var queryPayroll = "[spSyncDB_GetPayrollbyPolicyOvertime]";
            var queryHours = "[spSyncDB_GetPayrollbyPolicy]";
            var querytrans = "[spSyncDB_GetPayrollbyTransaction]";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@Year", year, DbType.Int32, ParameterDirection.Input);
            PayrollAndOverTimePerPolicy = await GetAsync<PayrollTiles>(queryPayroll, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            PayrollPerPolicy = await GetAsync<PayrollTiles>(queryHours, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            TransPerPolicy = await GetAsync<PayrollTiles>(querytrans, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            result.PayrollPerPolicy = PayrollPerPolicy.ToList();
            result.PayrollAndOverTimePerPolicy = PayrollAndOverTimePerPolicy.ToList();
            result.TransPerPolicy = TransPerPolicy.ToList();
            return result;
        }


        public async Task<IEnumerable<UserModel>> GetUserListAsync()
        {

            #region With Stubbed Data.
            List<UserModel> userList = new List<UserModel>()
            {
                new UserModel(){UserId = 101, FirstName = "Test", LastName = "User 1", MobileNumber = "9876543210"},
                new UserModel(){UserId = 102, FirstName = "Test", LastName = "User 2", MobileNumber = "9876543210"},
                new UserModel(){UserId = 103, FirstName = "Test", LastName = "User 3", MobileNumber = "9876543210"},
                new UserModel(){UserId = 104, FirstName = "Test", LastName = "User 4", MobileNumber = "9876543210"},
                new UserModel(){UserId = 105, FirstName = "Test", LastName = "User 5", MobileNumber = "9876543210"},
            };




            return userList;
            #endregion

            #region With Stored procedure
            var query = "sp_GetAgents_sel";

            DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@ProjectId", 101, DbType.Int32, ParameterDirection.Input);

            var result = await GetAsync<UserModel>(query, parameter, commandType: CommandType.StoredProcedure);
            #endregion
        }
    }
}
