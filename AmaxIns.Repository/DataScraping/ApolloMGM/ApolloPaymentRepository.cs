﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.Aspen;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AmaxIns.Repository.DataScraping.ApolloMGM
{
    public class ApolloPaymentRepository : BaseRepository, IApolloPaymentRepository
    {
        IConfiguration _configuration;
        public ApolloPaymentRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<ApolloLiveModel> GetApolloPaymentCarrierData()
        {
            IEnumerable<ApolloLiveModel> result = null;
            var query = @"sp_SyncDb_GetLivePaymentApolloData";
            result = Get<ApolloLiveModel>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }
    }
}
