﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.ApolloMGM;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Repository.DataScraping.ApolloMGM
{
    public class ApolloMgmRepo : BaseRepository, IApolloMgmRepo
    {

        IConfiguration _configuration;
        public ApolloMgmRepo(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }
    
        public IEnumerable<ApolloMgmModel> GetApolloData(int currentPage , out int TotalRecords,int AgencyId)
        {
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);
            IEnumerable<ApolloMgmModel> result = null;
            var query = @"spSyncDB_GetApolloPaymentsvsAmaxPay_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@OffsetValue", (currentPage - 1) * maxRows, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PagingSize", maxRows);
            parameter.Add("@agencyid", AgencyId);
            parameter.Add("@TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            result =  Get<ApolloMgmModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            TotalRecords = parameter.Get<int>("TotalRecords");
            return result;
            
        }

        public IEnumerable<ApolloMgmModel> GetApolloDatadueDate(int currentPage, out int TotalRecords, int AgencyId)
        {
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);
            IEnumerable<ApolloMgmModel> result = null;
            var query = @"spSyncDB_GetApolloPaymentsvsAmaxPay_Policies15DaysDue_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@OffsetValue", (currentPage - 1) * maxRows, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PagingSize", maxRows);
            parameter.Add("@agencyid", AgencyId);
            parameter.Add("@TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            result = Get<ApolloMgmModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            TotalRecords = parameter.Get<int>("TotalRecords");
            return result;
        }

        public IEnumerable<ApolloMgmModel> GetApolloDataExpiring(int currentPage, out int TotalRecords, int AgencyId)
        {
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);
            IEnumerable<ApolloMgmModel> result = null;
            var query = @"spSyncDB_GetApolloPaymentsvsAmaxPay_PoliciesExpiring_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@OffsetValue", (currentPage - 1) * maxRows, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PagingSize", maxRows);
            parameter.Add("@agencyid", AgencyId);
            parameter.Add("@TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            result = Get<ApolloMgmModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            TotalRecords = parameter.Get<int>("TotalRecords");
            return result;

        }


        public IEnumerable<ApolloCommissionsModel> GetApolloCommissions(int currentPage, out int TotalRecords, int AgencyId)
        {
            int maxRows = Convert.ToInt32(_configuration.GetSection("maxRows").Value);
            IEnumerable<ApolloCommissionsModel> result = null;
            var query = @"spSyncDB_GetApolloCommissions_sel";
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@OffsetValue", (currentPage - 1) * maxRows, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PagingSize", maxRows);
            parameter.Add("@agencyid", AgencyId);
            parameter.Add("@TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            result = Get<ApolloCommissionsModel>(query, parameter, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            TotalRecords = parameter.Get<int>("TotalRecords");
            return result;

        }







    }
}
