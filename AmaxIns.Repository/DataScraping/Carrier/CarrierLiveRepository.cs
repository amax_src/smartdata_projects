﻿using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.DataContract.DataScraping.Seaharbor;
using AmaxIns.RepositoryContract.DataScraping.Aspen;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AmaxIns.Repository.DataScraping.Aspen
{
    public class CarrierLiveRepository : BaseRepository, ICarrierLiveRepository
    {
        IConfiguration _configuration;

        public CarrierLiveRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;

        }

        public IEnumerable<AspenLiveModel> GetAspenLiveData()
        {

            IEnumerable<AspenLiveModel> result = null;
            var query = @"sp_SyncDb_GetLiveAspenData";

            result = Get<AspenLiveModel>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            return result;
        }

        public IEnumerable<SeaharborLiveModel> GetSeaharborLiveData()
        {

            IEnumerable<SeaharborLiveModel> result = null;
            var query = @"sp_SyncDb_GetLiveSeahaborData";

            result = Get<SeaharborLiveModel>(query, commandTimeout: 0, commandType: CommandType.StoredProcedure);

            return result;
        }
    }
}


