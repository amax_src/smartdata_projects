﻿using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using AmaxIns.RepositoryContract.DataScraping.Livedate;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AmaxIns.Repository.DataScraping.Livedate
{
    public class LiveDataPaymentRepository:BaseRepository, ILiveDataPaymentRepository
    {
        IConfiguration _configuration;
        public LiveDataPaymentRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;

        }

        public IEnumerable<IPModel> GetIPData(string Carrier)
        {
            IEnumerable<IPModel> result = null;
            var query = @"sp_SyncDb_GetLiveAspenIPData";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Carrier", Carrier, DbType.String, ParameterDirection.Input);
            result = Get<IPModel>(query, parameters, commandTimeout: 0, commandType: CommandType.StoredProcedure);
            return result;
        }

    }
}
