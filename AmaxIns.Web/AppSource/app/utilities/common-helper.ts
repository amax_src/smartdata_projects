import { keyValue } from './../model/keyValue';
import * as moment from "moment";
//import { MaxPLDashboardCount } from '../model/MaxPLReport';

export class CommonHelper {

    static sortOrderComparer(prop: string, asc: boolean = true) {
        return function (a, b) {
            if (asc) {
                if (a[prop] > b[prop]) {
                    return 1;
                } else if (a[prop] < b[prop]) {
                    return -1;
                }
            }
            else {
                if (a[prop] < b[prop]) {
                    return 1;
                } else if (a[prop] > b[prop]) {
                    return -1;
                }
            }
            return 0;
        }
    }

    static getFirstLastDates(date: Date) {
        const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        return {
            FromDate: firstDay,
            ToDate: lastDay
        };
    }

    static uniqueList(list, props, plainArray = false) {
        let lst = [];
        props = props.split(",");

        list.forEach((r) => {
            let obj = {};
            for (var i = 0; i < props.length; i++) {
                obj[props[i]] = r[props[i]];
            }
            if (plainArray) {
                lst.push(obj[Object.keys(obj)[0]]);
            }
            else {
                lst.push(obj);
            }
        });

        const unique = new Set(lst.map(m => JSON.stringify(m)));
        return Array.from(unique).map(m => JSON.parse(m));
    }

    static getDateTimeDifference(fromDate, toDate) {
        let duration = moment.duration(moment(toDate).diff(fromDate));
        return {
            months: duration.asMonths(),
            days: duration.asDays(),
            hours: duration.asHours(),
            minutes: duration.asMinutes(),
            seconds: duration.asSeconds()
        };
    }

    static getTenureByDays(days: number) {
        let start = moment().add((-1 * days), 'days');
        let end = moment();

        let duration = moment.duration(moment(end).diff(start));
        let y = duration.years();
        let m = duration.months();
        let d = duration.days();

        let tenure = '';
        if (y > 0) {
            tenure += (y > 1 ? y + " years" : y + " year");
        }
        if (m > 0) {
            tenure += (y > 0 ? ", " : "");
            tenure += (m > 1 ? m + " months" : m + " month");
        }
        if (d > 0) {
            tenure += (y > 0 || m > 0 ? ", " : "");
            tenure += (d > 1 ? d + " days" : d + " day");
        }
        return tenure;
    }

    static dateInBetween(date, fromDate, toDate) {
        return moment(date).isBetween(moment(fromDate), moment(toDate), 'days', '[]');
    }

    static getMonthNumber(monthName) {
        return Number(moment().month(monthName).format("M"));
    }

    static getMonthName(monthNumber) {
        return moment('1900-' + monthNumber + '-01').format("MMMM");
    }

    static isNumeric(str) {
        if (typeof str != "string") return false // we only process strings!  
        return !isNaN(parseInt(str)) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
            !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
    }

    static getYearMonths(month: number, year: number): any[] {
        let months = [];
        for (let m = 1; m <= month; m++) {
            let date = year + '-' + m + '-1';
            months.push({
                year: year,
                month: m,
                monthName: moment(date).format("MMM, YYYY")
            });
        }
        return months;
    }

    static getMonthDates(month: number, year: number): Date[] {
        let dates = [];
        let firstLast = this.getFirstLastDates(new Date(year + '-' + month + '-1'));
        while (firstLast.FromDate <= firstLast.ToDate) {
            dates.push({
                year: year,
                month: month,
                monthName: moment(firstLast.FromDate).format("YYYY-MM-DD")
            });
            firstLast.FromDate = moment(firstLast.FromDate).add(1, 'days').toDate();
        }
        return dates;
    }

    static getFormattedDate(date: Date, format: string): string {
        return moment(date).format(format);
    }

    static with2Decimals(num: number) {
        return num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    }

    static getYears(): any[] {
        let lst = [];
        let startYear = 2020;
        let currentYear = moment().utc().year();
        for (let index = startYear; index <= currentYear; index++) {
            lst.push({
                value: index,
                label: index
            });
        }
        return lst;
    }

    static getMonths(): any[] {
        let lst = [];
        for (let m = 1; m <= 12; m++) {
            let date = '2000-' + m + '-1';
            lst.push({
                value: m,
                label: moment(date).format("MMMM")
            });
        }
        return lst;
    }

    static nFormatter(num, digits) {
        const lookup = [
            { value: 1, symbol: "" },
            { value: 1e3, symbol: "k" },
            { value: 1e6, symbol: "M" },
            { value: 1e9, symbol: "G" },
            { value: 1e12, symbol: "T" },
            { value: 1e15, symbol: "P" },
            { value: 1e18, symbol: "E" }
        ];
        const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var item = lookup.slice().reverse().find(function (item) {
            return num >= item.value;
        });
        //num = new Intl.NumberFormat('en-US', { maximumFractionDigits: digits }).format(num / item.value)
        let ret = "0";
        try {
            ret = item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
        } catch (error) {
            console.error(error);
        }
        console.warn(ret);
        return ret;
    }

    static clone(source, cloneObject = {}) {
        if (typeof source !== 'object') return source;

        // Source is an array
        if (Array.isArray(source)) {
            cloneObject = [];

            source.forEach((item, index) => {
                if (typeof item === 'object') {
                    cloneObject[index] = this.clone(item);
                } else {
                    cloneObject[index] = item;
                }
            });
        }
        // Source is another type of object
        else {
            Object.keys(source).forEach(key => {
                if (typeof source[key] === 'object') {
                    cloneObject[key] = this.clone(source[key]);
                } else {
                    cloneObject[key] = source[key];
                }
            });
        }

        return cloneObject;
    }

}

export namespace Enumerations {
    export enum E_ReportFor {
        REGION = 0,
        ZONE = 1,
        OFFICE = 2,
        COMPANY = 3,
        AGENT = 4
    }
}
