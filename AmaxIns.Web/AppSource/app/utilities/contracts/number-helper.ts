export class NumberHelper {
  static isAvailable(value: number): boolean {
    if (typeof value !== "number") {
      return false;
    }

    const isNaN = this.isNaN(value);
    const isInfinity = this.isInfinity(value);
    return !isNaN && !isInfinity;
  }

  static isInfinity(value: number): boolean {
    const isInifinity = value === Infinity;
    const isPositiveInfinity = value === +Infinity;
    const isNegativeInfinity = value === -Infinity;
    return isInifinity || isPositiveInfinity || isNegativeInfinity;
  }

  static isNaN(value: number): boolean {
    return value !== value;
  }

  static isGreaterThan(value: number, minValue: number, inclusion = false): boolean {
    if (!this.isAvailable(value)) {
      return false;
    }

    const result = inclusion ? value >= minValue : value > minValue;
    return result;
  }
}
