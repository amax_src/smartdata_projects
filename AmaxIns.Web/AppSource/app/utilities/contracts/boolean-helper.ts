export class BooleanHelper {
  static isAvailable(value: boolean): boolean {
    return typeof value === "boolean";
  }

  static tryGet(value: boolean, defaultValue: boolean): boolean {
    return this.isAvailable(value) ? value : defaultValue;
  }
}
