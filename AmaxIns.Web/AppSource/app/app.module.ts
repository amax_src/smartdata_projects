import { BrowserModule } from '@angular/platform-browser'; 
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { AppRoutingModule } from './app-routing.module';
import { RevenueModule } from './Modules/Revenue/revenue.module';
import { PayrollModule } from './Modules/Payroll/payroll.module';
import { DashBoardModule } from './Modules/Dashboard/DashBoard.module';
import { EPRModule } from './Modules/EPR/epr.module';
import { AISModule } from './Modules/AIS/ais.module';
import { SpectrumModule } from './Modules/Spectrum/spectrum.module';
import { TierGoalModule } from './Modules/TierGoal/tier-goal.module';
import { AplhaPBXModule } from './Modules/AlphaPBX/alpha.pbx.module';
import { AplhaCustomerInfoModule } from './Modules/AlphaCustomerInfo/alpha.customerInfo.module';
import { AlphaTopAgentsModule } from './Modules/AlphaTopAgents/alpha.AlphaTopAgents.module';
import { YearOnYearRevenueModule } from './Modules/YearOnYearRevenue/yearOnYearRevenue.module';
import { CarrierMixModule } from './Modules/CarrierMix/CarrierMix.module';
import { RevenueDailyModule } from './Modules/RevenueDaily/RevenueDaily.module';
import { SpectrumMonthlyModule } from './Modules/SpectrumMonthly/spectrumMonthly.module';
import { SpectrumRepeatedCallModule } from './Modules/SpectrumRepeatedCall/spectrumRepeatedCall.module';
import { BoundQuotesModule } from './Modules/BoundQuotes/BoundQuotes.module';
import { BoundQuotesNoTurboratorModule } from './Modules/BoundQuotesNoTurborator/BoundQuotesNoTurborator.module';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { ZmDashBoardModule } from './Modules/ZMDashBoard/ZmDashBoard.module';
import { NewOldQuotesModule } from './Modules/NewOldQuotes/newOldQuotes.module';
import { HourlyLoadModule } from './Modules/HourlyLoad/hourlyLoad.module';
import { CalendarModule } from './Modules/Planner/Calendar.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BudgetModule } from './Modules/Budget/Budget.module';
import { ReportsModule } from './Modules/reports/reports.module';
import { QuotesProjectionModule } from './Modules/quotes-projection/quotes-projection.module';

import { CustomerRetentionListModule } from './Modules/CustomerRetentionList/CustomerRetentionList.module';
import { CustomerRetentionCountListForZoneModule } from './Modules/CustomerRetentionCountListForZone/CustomerRetentionCountListForZone.module';
import { CustomerRetentionCountListForRegionModule } from './Modules/CustomerRetentionCountListForRegion/CustomerRetentionCountListForRegion.module';
import { CustomerRetentionCountListForHodModule } from './Modules/CustomerRetentionCountListForHod/CustomerRetentionCountListForHod.module';
import { CustomerRetentionCountListForStoreModule } from './Modules/CustomerRetentionCountListForStore/CustomerRetentionCountListForStore.module';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { MaxBIAgentModule } from './Modules/MaxBIReports/maxbi-agent.module';
import { AlpaModule } from './Modules/alpa/Alpa.module';

import { PowerbifinancialModule } from './Modules/PowerBIFinancials/Powerbifinancial.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    LoginModule,
    RevenueModule,
    RevenueDailyModule,
    PayrollModule,
    SpectrumModule,
    BrowserAnimationsModule,
    EPRModule,
    AISModule,
    AplhaPBXModule,
    AplhaCustomerInfoModule,
    AlphaTopAgentsModule,
    YearOnYearRevenueModule,
    CarrierMixModule,
    TierGoalModule,
    SpectrumMonthlyModule,
    SpectrumRepeatedCallModule,
    BoundQuotesNoTurboratorModule,
    ZmDashBoardModule,
    HourlyLoadModule,
    BoundQuotesModule,
    BudgetModule,
    DashBoardModule,
    NewOldQuotesModule,
    CalendarModule,
    ReportsModule,
    AppRoutingModule,
    CustomerRetentionListModule,
    CustomerRetentionCountListForZoneModule,
    CustomerRetentionCountListForRegionModule,
    CustomerRetentionCountListForHodModule,
    CustomerRetentionCountListForStoreModule,
      QuotesProjectionModule,
      CoreModule,
      SharedModule,
      MaxBIAgentModule, AlpaModule, PowerbifinancialModule,
    DeviceDetectorModule.forRoot()
  ],
  providers: [DatePipe],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
