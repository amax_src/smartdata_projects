import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'Revenue', children: [
      { path: "Revenue", loadChildren: "app/Modules/Revenue/revenue.module#RevenueModule" }
    ]
  },
  {
    path: 'RevenueDaily', children: [
      { path: "RevenueDaily", loadChildren: "app/Modules/RevenueDaily/RevenueDaily.module#RevenueDailyModule" }
    ]
  },
  {
    path: 'YearOnYearRevenue', children: [
      { path: "YearOnYearRevenue", loadChildren: "app/Modules/YearOnYearRevenue/yearOnYearRevenue.module#YearOnYearRevenueModule" }
    ]
  },
  {
    path: 'Payroll', children: [
      { path: "Payroll", loadChildren: "app/Modules/Payroll/payroll.module#PayrollModule" }
    ]
  },
  {
    path: 'Spectrum', children: [
      { path: "Spectrum", loadChildren: "app/Modules/Spectrum/spectrum.module#SpectrumModule" }, 
    ]
  },
  {
    path: 'SpectrumMonthly', children: [
      { path: "SpectrumMonthly", loadChildren: "app/Modules/SpectrumMonthly/spectrumMonthly.module#SpectrumMonthlyModule" }
    ]
  },
  {
    path: 'SpectrumRepeatedCall', children: [
      { path: "SpectrumRepeatedCall", loadChildren: "app/Modules/SpectrumRepeatedCall/spectrumRepeatedCall.module#SpectrumRepeatedCallModule" }
    ]
  },
  {
    path: 'EPR', children: [
      { path: "EPR", loadChildren: "app/Modules/EPR/epr.module#EPRModule" }
    ]
  }
  ,
  {
    path: 'AIS', children: [
      { path: "AIS", loadChildren: "app/Modules/AIS/ais.module#AISModule" }
    ]
  },
  {
    path: 'Tier', children: [
      { path: "Tier", loadChildren: "app/Modules/TierGoal/tier-goal.module#TierGoalModule" }
    ]
  }
  ,
  {
    path: 'PBX', children: [
      { path: "PBX", loadChildren: "app/Modules/AlphaPBX/alpha.pbx.module#AplhaPBXModule" }
    ]
  }
  ,
  {
    path: 'CustomerInfo', children: [
      { path: "CustomerInfo", loadChildren: "app/Modules/AlphaCustomerInfo/alpha.customerInfo.module#AplhaCustomerInfoModule" }
    ]
  },
   {
     path: 'TopAgent', children: [
       {
         path: "TopAgent", loadChildren: "app/Modules/AlphaTopAgents/alpha.AlphaTopAgents.module#AlphaTopAgentsModule"
       }
    ]
  },
  {
    path: 'CarrierMix', children: [
      {
        path: "CarrierMix", loadChildren: "app/Modules/CarrierMix/CarrierMix.module#CarrierMixModule"
      }
    ]
  },
  {
    path: 'BoundQuotes', children: [
      {
        path: "BoundQuotes", loadChildren: "app/Modules/BoundQuotes/BoundQuotes.module#BoundQuotesModule"
      }
    ]
  },
  {
    path: 'Budget', children: [
      {
        path: "Budget", loadChildren: "app/Modules/Budget/Budget.module#BudgetModule"
      }
    ]
  },
  {
    path: 'BoundQuotesNoTurborator', children: [
      {
        path: "BoundQuotesNoTurborator", loadChildren: "app/Modules/BoundQuotesNoTurborator/BoundQuotesNoTurborator.module#BoundQuotesNoTurboratorModule"
      }
    ]
  }
  ,
  {
    path: 'ZmDashboard', children: [
      {
        path: "ZmDashboard", loadChildren: "app/Modules/ZMDashBoard/ZmDashBoard.module#ZmDashBoardModule"
      }
    ]
  },
  { path: 'NewModifiedQuotes', loadChildren: "app/Modules/NewOldQuotes/newOldQuotes.module#NewOldQuotesModule"},
  { path: 'Hourlyload', loadChildren: "app/Modules/HourlyLoad/hourlyLoad.module#HourlyLoadModule"}

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
