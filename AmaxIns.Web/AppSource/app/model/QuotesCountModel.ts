import { QuotesModel } from './QuotesModel';

export class QuotesCountModel {
  agencyName: string = '';
  count: number = 0;
  quotesModels: Array<QuotesModel> = new Array<QuotesModel>();
}
