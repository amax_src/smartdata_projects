export class spectrumDailySummaryModel {
  public agencyid: number=0;
  public agencyName: string='';
  public extension: string = '';
  public inboundCalls: number = 0;
  public outboundCalls: number = 0;
  public internalCalls: number = 0;
  public totalCalls: number = 0;
  public talktimeMinutes: number = 0;
  public averageTalkTime: number = 0;
  public date: Date;
  public month: string = "";
  public SortMonthNo: number = 0;
}
