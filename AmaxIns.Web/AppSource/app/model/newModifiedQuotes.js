"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NewModifiedQuotes = /** @class */ (function () {
    function NewModifiedQuotes() {
        this.firstName = '';
        this.lastname = '';
        this.agencyName = '';
        this.iProAgency = '';
        this.quoteType = '';
        this.phone = '';
        this.policyNumber = '';
        this.createDate = '';
        this.tCreateDate = '';
        this.tModifiedDate = '';
    }
    return NewModifiedQuotes;
}());
exports.NewModifiedQuotes = NewModifiedQuotes;
//# sourceMappingURL=newModifiedQuotes.js.map