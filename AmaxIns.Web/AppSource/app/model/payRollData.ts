export class PayRollData {
  rmId: number;
  regionalManager: string;
  zmId: number;
  zonalManager: string;
  agencyid: number;
  location: string;
  months: string;
  categoryDescription: string;
  debitAmount: number;
  debitHours: number;
  year: number;
  payPeriod: null
}
