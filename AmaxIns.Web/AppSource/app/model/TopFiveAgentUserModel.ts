export class TopFiveAgentUserModel {
  agencyId: Array<number>;
  year: Array<number>;
  month: Array<string>;
}
