export class HourlyNewModifiedQuotes {
  agencyId: number = 0;
  location: string = '';
  modifiedQuotes: number = 0;
  newQuotes: number = 0;
  date: string = '';
  hourIntervel: string = '';
  dayofweek: string = '';
}
