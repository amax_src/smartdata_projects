export class DashboardPayrollModel {
  public agencyID: number = 0;
  public location: string = '';
  public year: number = 0;
  public month: string = '';
  public payrollBudget: number = 0;
  public payrollActual: number = 0;
  public otActual: number = 0;
  public otBudget: number = 0;
  public otPerPolicy: number = 0;
  public otHoursPerPolicy: number = 0;
  public payrollPerPolicy: number = 0;
  public payrollPerTransaction: number = 0;
  public sortMonthNum: number = 0;
  public groupName: string = '';
}
