export class user {
    userID: number;
    groupName: string;
    userName: string;
    token: string;
    loginName: string;
    agentID: number;
    agentInfoID: number;
    agencyIds: string;
    userAgencyList: []

    constructor() {
        this.userID = 0;
        this.groupName = "";
        this.userName = "";
        this.loginName = "";
        this.agencyIds = "";
        this.token = "";
        this.userAgencyList = [];
    }
}
