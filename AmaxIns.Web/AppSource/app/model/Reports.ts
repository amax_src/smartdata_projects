export class AvgAgencyFee {
  rom: string = "";
  zm: string = "";
  agencyName: number = 0;
  transactions: number = 0;
  policy: number = 0;
  feePerPolicy: number = 0;
  endorsment: number = 0;
  feePerEndorstment: number = 0;
  year: number = 0;
  month: string = "";
  rmId: number = 0;
  zmId: number = 0;
  agencyId: number = 0;
  mName: string = '';
}

export class AgencyBreakdown {
  ROM: string = "";
  ZM: string = "";
  AgencyName: string = "";
  Avg_fee: number = 0;
  Fee_Per_Policy: number = 0;
  AfeeLessThan1: number = 0;
  PercentageAfeeLessThan1: number = 0;
  AfeeLessThan1_10: number = 0;
  PercentageAfeeLessThan1_10: number = 0;
  AfeeLessThan10_25: number = 0;
  PercentageAfeeLessThan10_25: number = 0;
  AfeeLessThan25_50: number = 0;
  PercentageAfeeLessThan25_50: number = 0;
  AfeeLessThan50_100: number = 0;
  PercentageAfeeLessThan50_100: number = 0;
  AfeeLessThan100_150: number = 0;
  PercentageAfeeLessThan100_150: number = 0;
  AfeeLessThan150_200: number = 0;
  PercentageAfeeLessThan150_200: number = 0;
  AfeeLessThan200_500: number = 0;
  PercentageAfeeLessThan200_500: number = 0;
  AfeeGreaterThan500: number = 0;
  PercentageAfeeGreaterThan500: number = 0;
  SumOfAllBuckets: number = 0;
  year: number = 0;
  month: string = "";
  rmId: number = 0;
  zmId: number = 0;
  agencyID: number = 0;
}

export class UniqueQuotes {
  market: string = '';
  agencyID: number = 0;
  agencyName: string = "";
  createdQuotes0: number = 0;
  createdQuotes1: number = 0;
  yoY: number = 0;
  rmid: number = 0;
  zmid: number = 0;
}

export class Overtime {
  userName: string = "";
  regionalManagersId: string = "";
  regionalManagers: string = "";
  zonalManagersId: string = "";
  zonalManagers: string = "";
  agencyNameID: string = "";
  agencyName: string = "";
  agentName: string = "";
  total: number = 0;
  regular: number = 0;
  oT: number = 0;
  policies: number = 0;
  transactions: number = 0;
  oTPerPolicies: number = 0;
  oTPerTransaction: number = 0;
  policiesInOT: string = "";
  transactionsInOT: number = 0;
  monthsTillToday: number = 0;
  month: string = "";
  totalhrsperPolicy: number = 0;
  totalhrsperTransaction: number = 0;
}

export class TrackerReport {
  agencyName: string = "";
  agencyNameID: number = 0;
  eoM_AgencyFeeVSGOAL_AgencyFee: string = "";
  eoM_AgencyFeeVSGOAL_AgencyFeeNO: string = "";
  eoM_Policies: string = "";
  eoM_PoliciesVSGOAL_Policies: string = "";
  eoM_PoliciesVSGOAL_PoliciesNO: string = "";
  eoM_Premium: string = "";
  eoM_PremiumVSGOAL_Premium: string = "";
  eoM_PremiumVSGOAL_PremiumNO: string = "";
  eoM_agencyfee: string = "";
  goal_AgencyFee: string = "";
  goal_Policies: string = "";
  goal_Premium: string = "";
  mtD_Policies: string = "";
  mtD_Premium: string = "";
  mtD_agencyfee: string = "";
  regionalManager: string = "";
  regionalManagerId: string = "";
  zonalManager: string = "";
  zonalManagerId: string = "";

  eomPacingPolicies: string = "";
  eomPacingVSGoalNoPolicies: string = "";
  eomPacingVSGoalPolicies: string = "";
  goals: string = "";
  monthEndActiveCustomersPOLICIES: string = "";
}

export class AgentPerformace {
  afeeScore: any = 0;
  agencyID: number = 0;
  agencyName: string = "";
  agencyfee: any = 0;
  agentID: string = "";
  agentName: string = "";
  polScore: any = 0;
  policies: any = 0;
  premium: any = 0;
  regionalManagers: string = "";
  regionalManagersId: number = 0;
  totalScore100: any = 0;
  transaction: any = 0;
  transactionScore: any = 0;
  zonalManagers: string = "";
  zonalManagersId: number = 0;
}

export class Months {
  monthId: string;
  monthName: string;
}

export class RenterReport {
    agencyID: string = "";
    agencyName: string = "";
    agencyfee: string = "";
    goal_AgencyFee: string = "";
    goal_Policies: string = "";
    goal_Premium: string = "";
    policies: string = "";
    premium: string = "";
    priorYear_Policies: string = "";
    priorYear_Premium: string = "";
    priorYear_agencyfee: string = "";
    rm: string = "";
    rmid: string = "";
    total_Projecte_Policies: string = "";
    zm: string = "";
    zmid: string = "";
    policy_Goal_vs_Actual: string = "";
    afee_Goal_vs_Actual: string = "";
    premium_Goal_vs_Actual: string = "";
    policy_YoY: string = "";
    afee_YoY: string = "";
    premium_YoY: string = "";
}

export class ActiveCustomers {
    activeCustomer: string = "";
    active_Customers_Goals: string = "";
    market: string = "";
    priorYearActiveCustomer: string = "";
    agencyID: string = "";
    agencyName: string = "";
    rm: string = "";
    rmid: string = "";
    zm: string = "";
    zmid: string = "";
    activeCustomersVsGoals: number = 0;
    activeCustomersvsPriorYear: number = 0;
}
