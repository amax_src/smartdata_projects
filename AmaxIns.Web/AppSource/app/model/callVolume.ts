export class callVolume {
  public agencyId: number = 0;
  public agencyName: string = '';
  public month: string = '';
  public sortMonthNo: string = '';
  public countOfCalls: number = 0;
  public dayofWeek: string = '';
  public asTalkTime: number =0;
  public call_Status: string = '';
}
