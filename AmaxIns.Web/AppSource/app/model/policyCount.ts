export class PolicyCount {
  goal: number=0;
  actual: number=0;
  difference: number=0;
  averagepolicy: number=0;
  pacing: number=0;
  goalpacing: number = 0;
  averagepremium: number = 0;
  averageAgencyFee: number = 0;
  month: string = "";
  agencyName: string = "";
  agencyId: string = "";
}


