export class DashboardQuotesModel {
  public agencyID: number = 0;
  public location: string = '';
  public year: string = '';
  public month: string = '';
  public newQuotes: number = 0;
  public closingRatioNewQuotes: number = 0;
  public closingRatioModQuotes: number = 0;
  public groupName: string = '';
}
