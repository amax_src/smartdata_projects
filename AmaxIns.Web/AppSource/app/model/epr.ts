export class Epr {
  policies: number=0;
  agencyfee: number = 0;
  premium: number = 0;
  location: string ='';
  agencyID: number = 0;
  csr: string = '';
  transactions: number = 0;
  date: Date ;
  year: string = '';
  month: string = '';
  avgagencyfeebypolicy: number = 0;
  policiesbyagent: number = 0;
  transactionsbyagent: number = 0;
  agencyfeebyagent: number = 0;
  premiumbyagent: number = 0;
  avgagencyfeebypolicybyagent: number = 0;
  regionalManagers: string = '';
  regionalManagerID: number = 0;
  zonalManagers: string = '';
    zonalManagerID: number = 0
    agentID: number = 0;
}
export class EPRGraphModel {
    policies: number = 0;
    agencyFee: number = 0;
    month: string = ''
    payType: string = ''
    eprValue: number = 0;
}
