export class SpectrumRepeatedCall {
  public agencyId: number = 0;
  public agencyName: string = '';
  public cnumber: string = '';
  public date: Date;
  public countofcalls: number = 0;
  public callerid_Internal: string = '';
  public callerid_External: string = '';
  public callType: string = '';
}
