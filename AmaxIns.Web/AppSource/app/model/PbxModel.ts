export class PbxModel {

  public callId: string = '';

  public date: Date

  public direction: string = '';

  public extension: string = '';

  public from: string = '';

  public month: string = '';

  public monthNo: 4

  public startTime: Date

  public talkTimeMinutes: number = 0

  public to: string = '';

}
