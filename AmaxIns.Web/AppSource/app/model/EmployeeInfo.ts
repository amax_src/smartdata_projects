export class EmployeeInfo {
  employeeCode: string;
  tenureInDays: number;
  licenseType1: string;
  licenseNumber1: string;
  licenseType2: string;
  licenseNumber2: string;
}