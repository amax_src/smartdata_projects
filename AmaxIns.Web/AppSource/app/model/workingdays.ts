    export class Workingdays
    {
      year: string;
      month: string;
      totalWorkingdays: number;
      workingDaysPassed: number;
      remainingDays: number;
    }
