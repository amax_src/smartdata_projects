export class HourlyProduction {
  policies: string;
  agencyfee: number;
  premium: number;
  transactions: number;
  paydate: Date;
  firsthour: number;
  dayofweek: string;
}