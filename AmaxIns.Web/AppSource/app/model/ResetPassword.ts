export class ResetPasswordModel {
  public password:string=''
  public newPassword: string = ''
  public repeatPassword: string = ''
}
