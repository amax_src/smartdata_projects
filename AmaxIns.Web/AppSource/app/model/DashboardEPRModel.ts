export class DashboardEPRModel {
  public agencyID: number = 0;
  public location: string = '';
  public year: string = '';
  public month: string = '';
  public weekStart: string = '';
  public weekEnd: string = '';
  public sortMonthNum: number = 0;
  public groupName: string = '';
  public days: number = 0;
  public date: string = '';
  public averagepolicies: number = 0;
  public averagetransactions: number = 0;
  public averageagencyfee: number = 0;
  public averagepremium: number = 0;
  public csr: string = '';
}
