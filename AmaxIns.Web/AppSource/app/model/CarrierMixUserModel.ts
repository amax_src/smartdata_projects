export class CarrierMixUserModel
{
  public date: Array<string> = new Array<string>();
  public agency: Array<number> = new Array<number>();
  public month: string;
  public carrier: Array<string> = new Array<string>();
  public year: string;
}

