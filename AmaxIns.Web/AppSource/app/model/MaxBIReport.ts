import * as moment from "moment";

export enum E_MaxBIReport {
  CarrierAgent = 0,
  AgencyFeePerPolicy = 1,
  PremiumPerPolicy = 2
}

export enum E_MaxBICatalogFrom {
  PaymentInfo = 1,
  Quotes = 2,
  Calls = 3
}

export enum E_MaxBICatalogType {
  Agent = 1,
  Company = 2,
  Agency = 3
}

export class MaxBISummary {
  Region: string;
  Zone: string;
  Office: string;
  Company: string;
}
export class MaxBISummaryIndicator {
  Region: boolean;
  Zone: boolean;
  Office: boolean;
  Company: boolean;
}

export class MaxBIRequest {
  HierarchyIds: Array<string>;
  AgencyIds: Array<number>;
  AgentIds: Array<string>;
  CompanyIds: Array<number>;
  PayTypes: Array<string>;
  FromDate?: Date;
  ToDate?: Date;
  Dates: Array<Date>;
  Month: number;
  Year: number;
  ReportType: E_MaxBIReport;
  QuoteType: string;
  CallType: string;
  ReportBy: number;
  IsClosingRatio: boolean = false;
  CatalogFrom: E_MaxBICatalogFrom;
  CatalogType: E_MaxBICatalogType;

  constructor() {
    this.Month = moment().utc().month();
    this.Year = moment().utc().year();
    this.ReportBy = 3;

    this.HierarchyIds = [];
    this.AgencyIds = [];
    this.AgentIds = [];
    this.CompanyIds = [];
    this.PayTypes = [];
    this.Dates = [];
  }
}

export class MaxBIHierarchyRequest {
  Level: number;
  LevelId: number;
  FilterBy: number;
  FilterByIds: string;
}

export class InboundOutboundCalls {
  recordId: number;
  parentId: number;
  monthName: string;
  month: number;
  year: number;
  date?: Date;
  agencyId: number;
  agencyName: string;
  agentId: number;
  agentName: string;
  rmId: number;
  rmName: string;
  zmId: number;
  zmName: string;
  outBoundCalls: number;
  inBoundCalls: number;
  outBoundCallsAverage: string;
  inBoundCallsAverage: string;
  noOfAgents: number;
  agents: Array<any>;
}

export class NewModifiedQuotes {
  recordId: number;
  parentId: number;
  monthName: string;
  month: number;
  year: number;
  date?: Date;
  agentId: string;
  agentName: string;
  agencyId: number;
  agencyName: string;
  carrierId: number;
  carrierName: string;
  rmId: number;
  rmName: string;
  zmId: number;
  zmName: string;
  quotes: number;
  sales: number;
  closingRatio: number;
  quotesAverage: string;
  noOfAgents: number;
  agents: Array<any>;
}

export class AgencyPerPolicy {
  recordId: number;
  parentId: number;
  monthName: string;
  month: number;
  year: number;
  date?: Date;
  policyType: string;
  agencyId: number;
  agencyName: string;
  carrierId: number;
  carrierName: string;
  agentId: number;
  agentName: string;
  rmId: number;
  rmName: string;
  zmId: number;
  zmName: string;
  agencyFee: number;
  premium: number;
  policies: number;
  perPolicy: number;
  agents: Array<any>;
}

export class CarrierAgent {
  dateTransaction?: Date;
  monthName: string;
  month: number;
  year: number;
  agentID: number;
  agentName: string;
  policyType: string;
  carrier: string;
  agencyFee: number;
  premium: number;
  policies: number;
}

export class HierarchyInfo {
  rmId: number;
  rmTitle: string;
  zmId: number;
  zmTitle: string;
  agencyId: number;
  agencyTitle: string;
}

export class HierarchyId {
  rmId: number;
  zmId: number;
  agencyId: number;
  carrierId: string;
}

export class MaxBIDashboardRequest {
  Month: number;
  Year: number;
  AgentIds: Array<number>;
  PayPeriods: Array<string>;

  constructor() {
    this.AgentIds = [];
    this.PayPeriods = [];
  }
}

export class MaxBIPayroll {
  agentId: number;
  employeeNumber: number;
  payPeriod: string;
  categoryDescription: string;
  debitAmount: number;
  debitHours: number;
}

export class MaxBIPayrollFilter {
  payPeriod: string;
  payPeriodStart: Date;
  payPeriodEnd: Date;
  payPeriodMonth: string;
  month: number;
  year: number;
}