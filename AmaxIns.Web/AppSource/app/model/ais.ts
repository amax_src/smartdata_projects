export class Ais {
  rmId: number=0;
  zmId: number=0;
  policies: number=0;
  agencyFee: number=0;
  premium: number=0;
  location: string='';
  month: string='';
  title: string='';
  agentName: string='';
  sortMonthNum: number=0;
  agencyId: number = 0;
  year: string = '';
}
