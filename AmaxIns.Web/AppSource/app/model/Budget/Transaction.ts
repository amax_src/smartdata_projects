export class Transaction {
  actual: number = 0
  budgetNumber: string = ""
  description: string = ""
  locationName: string = ""
  monthName: string = ""
  period: number = 0
  source: string = ""
  trxDate: string = ""
  user: string = ""
  userPosted: string = ""
  vendorName: string = ""
  categoryName: string = ""
}
