export class BudgetOverspendModel {
  public year: string="";
  public categoryName: string = "";
  public budgetName: string = "";
  public budget: number = 0;
  public actual: number = 0;
  public remaningBudget: number = 0;
  public variance: number = 0;
  public varianceRate: number=0;
  public spending: string = "";
}
