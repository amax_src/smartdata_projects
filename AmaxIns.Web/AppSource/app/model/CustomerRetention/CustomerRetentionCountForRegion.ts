export class CustomerRetentionCountForRegion {
    dueDate: string;

    zoneManagerName: string;
    zoneId: number;

    paymentDueCount: number;
    paymentReceivedCount: number;
    difference: number;
    contactCallCount: number;
}