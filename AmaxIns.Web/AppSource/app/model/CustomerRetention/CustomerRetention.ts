export class CustomerRetention {
    dueDate: string;
    clientName: string;
    policyNumber: string;
    policyStatusId: number;
    policyStatusName: string;
    companyName: string;
    policyTypeId: number;
    policyTypeName: string;
    effDate: string;
    expDate: string;
    amountDue: number;
    clientCellPhone: string;
    clientHomePhone: string;
    clientWorkPhone: string;
    calledStatus: boolean;
    calledStatusName: string;
    callsMadeCount: number;
    calledDuration: string;
}