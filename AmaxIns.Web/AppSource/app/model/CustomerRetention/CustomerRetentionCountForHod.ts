export class CustomerRetentionCountForHod {
    dueDate: string;

    regionManagerName: string;
    regionId: number;

    paymentDueCount: number;
    paymentReceivedCount: number;
    difference: number;
    contactCallCount: number;
}