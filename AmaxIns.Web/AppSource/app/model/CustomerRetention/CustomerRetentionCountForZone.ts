export class CustomerRetentionCountForZone {
    dueDate: string;

    locationName: string;
    locationId: number;
    
    paymentDueCount: number;
    paymentReceivedCount: number;
    difference: number;
    contactCallCount: number;
}