export enum DueDateFilterTypeEnum
{
    DueToday = 1,
    DueNext7Days = 2,
    PastDue = 3,
    Canceled = 4
}