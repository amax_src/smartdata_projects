"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var calendarFormModel = /** @class */ (function () {
    function calendarFormModel() {
        this.ledgerId = 0;
        this.memo = '';
        this.credit = 0;
        this.debit = 0;
        this.eventId = 0;
        this.entryDate = '';
        this.createdDate = '';
        this.addRow = false;
        this.removeRow = false;
        this.disableField = false;
        this.addedBy = 0;
    }
    return calendarFormModel;
}());
exports.calendarFormModel = calendarFormModel;
//# sourceMappingURL=calendarFormModel.js.map