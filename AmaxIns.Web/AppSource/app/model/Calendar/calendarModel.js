"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var calendarModel = /** @class */ (function () {
    function calendarModel() {
        this.eventId = 0;
        this.eventName = "";
        this.eventDate = "";
        this.eventType = "";
        this.agencyId = 0;
        this.createdDate = "";
        this.modifiedDate = "";
        this.addedBy = 0;
        this.calendarModels = new Array();
    }
    return calendarModel;
}());
exports.calendarModel = calendarModel;
//# sourceMappingURL=calendarModel.js.map