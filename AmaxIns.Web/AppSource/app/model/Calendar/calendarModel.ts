import { calendarFormModel } from './calendarFormModel';

export class calendarModel {
  public eventId: number = 0;
  public eventName: string = "";
  public eventDate: string = "";
  public eventType: string = "";
  public agencyId: number = 0;
  public createdDate: string = "";
  public modifiedDate: string = "";
  public addedBy: number = 0;
  public calendarModels:Array<calendarFormModel> = new Array<calendarFormModel>();
}


