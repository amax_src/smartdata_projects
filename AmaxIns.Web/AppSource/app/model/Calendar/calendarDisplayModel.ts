export class calendarDisplayModel {
  public id: number = 0;
  public title: string = "";
  public date: string = "";
  public start: string = "";
  public end: string = "";
  public backgroundColor: string = "";
}
