export class calendarFormModel {
  public ledgerId: number = 0;
  public memo: string = '';
  public credit: number = 0;
  public debit: number = 0;
  public eventId: number = 0;
  public entryDate: string = '';
  public createdDate: string = '';
  public addRow: boolean = false;
  public removeRow: boolean = false;
  public disableField: boolean = false;
  public addedBy: number = 0;
}
