export class TierPercentage {
  public commission: number=0;
  public id:  number = 0;
  public tierName: string = '';
  public tierPercentage: number = 0;
}
