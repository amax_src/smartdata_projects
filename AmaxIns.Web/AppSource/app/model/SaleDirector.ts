export class SaleDirector{
  saleDirectorID: number;
  saleDirector: string;
  employeeId: string;
}