

export class PlannerActivity {
  activityId: number = 0;
  activityName:string=''
}

export class PlannerHours {
  plannerHoursId: number = 0;
  plannerHours: number = 0;
}
export class PlannerDate {
  plnDate: string = '';
}
export class PlannerBudget {
  plannerBudgetId: number = 0;
  agencyId: number = 0;
  plannerDate: string = ''
  activity: string = ''
  otherActivity: string = ''
  plannedHours: number = 0;
  plannedDetails: string = ''
  plannedBudgetAmount: number = 0;
  loginUserId: number = 0;
}
export class PlannerActual {
  plannerBudgetId: number = 0
  agencyId: number = 0
  plannerDate: string = '';
  activity: string = '';
  othersActivity: string = '';
  plannedHours: number = 0
  plannedDetails: string = '';
  plannedBudgetAmount: number = 0
  plannerActualId: number = 0
  actualHours: number = 0
  isWent: boolean = false;
  actualDetails: string = '';
  actualAmount: string = '';
  totalPolicy: number = 0
  totalAgencyFee: number = 0
  totalPremiumAmount: number = 0
  isLocked: boolean = false;
  isCurrentDay: boolean = false;
}


export class ActualDetail {
  plannerActualId: number = 0;
  plannerBudgetId: number = 0;
  actualHours: number = 0;
  isWent: boolean = false;
  actualDetails: string = '';
  actualAmount: number = 0
  loginUserId: number = 0
}

export class ActualPolicyDetails {
  policyDetailId: number = 0
  plannerBudgetId: number = 0
  policyNumber: string = '';
  agencyFee: number = 0
  premiumAmount: number = 0
  isVerifyed: boolean = false;
  loginUserId: number = 0
}

export class PlannerActualDetails {
  plannerBudget: PlannerBudget = new PlannerBudget();
  actualDetail: ActualDetail = new ActualDetail();
  listActualPolicyDetails: Array<ActualPolicyDetails> = new Array<ActualPolicyDetails>();
}

export class PlannerDashboard {
  agencyId: number = 0
  plannerDate: string = '';
  totalPlannedBudgetAmount: number = 0
  totalActualAmount: number = 0
  agencyName: string = '';
}
export class DailyView {
  agencyId: number = 0
  plannerDate: string = '';
  activity: string = '';
  plannedDetails: string = '';
  plannedHours: number = 0
  actualDetails: string = '';
  actualHours: number = 0
  totalPlannedBudgetAmount: number = 0
  totalActualAmount: number = 0
  plannerActualId: number = 0
  isWent: boolean = false;
  monthlyPlannerDate: string = '';
  plannerBudgetId: number = 0;
  isLocked: boolean = false;
  isCurrentDay: boolean = false;
}

export class PalnnerActivityDetails {
  activityName: string = '';
  agencyId: number = 0;
  totalCompleted: number = 0;
  totalPlanned: number = 0;
  variance: number = 0;
  agencyName: string = '';
}
