export class revenueDaily {
  public actagencyname: string='';
  public actmonth: string='';
  public actnewbusinesscount: number=0;
  public actagencyfee: number;
  public actpremium: number;
  public actYear: string='';
  public actDate: Date;
  public regionalmanagers: string='';
  public zonalmanagers: string='';
  public rmID: number=0;
  public zmID: number = 0;
  public agencyID: number = 0;
  public sortMonthNum: number = 0;
  public projectionPolicies: number = 0;
  public projectionagencyfee: number = 0;
  public projectionpremium: number = 0;
}


export class DailyTransactonPayment {
    datePayDate: Date
    paymentId: string = '';
    bankAccount: string = '';
    policyNumber: string = '';
    applicantName: string = '';
    email: string = '';
    payType: string = '';
    payMethod: string = '';
    coName: string = '';
    moneyPayAmount: number = 0;
    agentName: string = '';
    agencyName: string = '';
    agencyId: number = 0;
    location: string = '';
    feeType: string = '';
    sentToCompany: string = '';
    moneyPayFee: number = 0;
    moneyPayBalance: number = 0;
    policyType: string = '';
    referralSource: string = '';
    payNotes: string = '';
    policyStatus: string = '';
    moneypayTotal: number = 0;
}


