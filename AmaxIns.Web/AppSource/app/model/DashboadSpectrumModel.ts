export class DashboadSpectrumModel {
  public agencyID: number = 0;
  public location: string = '';
  public year: string = '';
  public month: string = '';
  public outboundCalls: number = 0;
  public inboundCalls: number = 0;
  public sortMonthNum: number = 0;
  public groupName: string = '';
}
