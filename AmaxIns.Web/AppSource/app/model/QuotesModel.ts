export class QuotesModel {
  identifier: string ='';
  i_quoteuid: string ='';
  i_c_name: string ='';
  i_agencyName: string ='';
  i_paydate: string = '';
  i_c_dob: string ='';
  i_c_Zip: string ='';
  t_agencyName: string ='';
  t_quote_uid: string ='';
  t_agency_address: string ='';
  t_c_zip: string ='';
  t_c_dob: string ='';
  t_date_created: string ='';
  t_c_firstname: string ='';
  t_c_lastname: string ='';
  t_date: string ='';
  t_c_lead_source: string ='';
  t_c_contact_source: string ='';
}
