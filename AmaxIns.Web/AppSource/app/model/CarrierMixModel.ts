export class CarrierMixModel {
  public agencyfee: number = 0;
  public avgAgencyFeeByPolicy: number = 0;
  public carrier: string = "";
  public policies: number = 0;
  public premium: number = 0;
  public transactions: number = 0;
}

