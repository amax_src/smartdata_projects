
export class NewModifiedQuotes {
  firstName: string = '';
  lastname: string = '';
  agencyName: string = '';
  iProAgency: string = '';
  quoteType: string = '';
  phone: string = '';
  policyNumber: string = '';
  createDate: string = '';
  tCreateDate: string = '';
  tModifiedDate: string = '';
  cat: number = 0;
}

export class QuotesProjection {
  agencyName: string = '';
  agencyId: number = 0;
  projectionYear: number = 0;
  projectionMonthId: number = 0;
  projectionMonth: string = '';
  createdQuotesProjection: number = 0;
  createdQuotesCR: number = 0;
  modifiedQuotesProjection: number = 0;
  modifiedQuotesCR: number = 0;
}

export class NewAndModifiedQuotesProj {
  agencyName: string = '';
  agencyId: number = 0;
  createdQuotesCR: number = 0;
  modifiedQuotesCR: number = 0;
  newActualCR: number = 0;
  modifiedAtualCR: number = 0;
  differenceNewQuotes: number = 0;
  differenceModifiedQuotes: number = 0;
  totalNewQuotes: number = 0;
  soldNewQuotes: number = 0;
  closingRatioNewQuotes: number = 0;
  totalModifiyQuotes: number = 0;
  soldModifiedQuotes: number = 0;
  closingRatioModQuotes: number = 0;
  createdPolicies: number = 0;
  modifiedPolicies: number = 0;
  differenceNewPolicies: number = 0;
  differenceModifiedPolicies: number = 0;
}
export class QuotesParam {
  agencyids: Array<string>;
  dates: Array<string>;
}
