export class AisChartData {
  Labels: Array<string> = new Array<string>();
  Data: Array<number> = new Array<number>();
}
