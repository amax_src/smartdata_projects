import { DashboardEPRModel } from './DashboardEPRModel';

export class DashboardEPRDetails {
  public weekly: Array<DashboardEPRModel> = new Array<DashboardEPRModel>();
  public daily: Array<DashboardEPRModel> = new Array<DashboardEPRModel>();
}
