export class DashboardRevenueModel {
  public agencyID: number = 0;
  public location: string = '';
  public year: string = '';
  public month: string = '';
  public perDiffPolicies: number = 0;
  public perDiffAFee: number = 0;
  public perDiffPremium: number = 0;
  public sortMonthNum: number = 0;
  public groupName: string = '';
}
