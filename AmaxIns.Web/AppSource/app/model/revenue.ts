export class revenue {
  actagencyname: string = '';
  actmonth: string = '';
  actYear: string = '';
  actnewbusinesscount: number;
  actagencyfee: number;
  actpremium: number;
  year: string = '';
  month: string = '';
  new_business_count: number;
  agency_fee: number;
  premium: number;
  salesPerDay: number;
  countofAgents: number;
  policiesPerAgent: number;
  sortmonthnum: number;
  regionalmanagers: string = '';
  rmid: number;
  zonalmanagers: string = '';
  zmid: number;
  percentage: number;
  agencyid: number;
  pc_goal: number;
  pc_actual_newbusinesscount: number;
  pc_averagepolicy: number;
  pc_pacing: number;
  pc_goalpacing: number

  pre_goal: number;
  pre_actual_premium: number;
  pre_averagepremium: number;
  pre_pacing: number;
  pre_goalpacing: number

  agency_goal: number;
  agency_actual_fee: number;
  agency_averagFee: number;
  agency_pacing: number;
  agency_goalpacing: number
  tier1Premium: number = 0;
  tier2Premium: number = 0;
  tier3Premium: number = 0;
  tier4Premium: number = 0;
  tier5Premium: number = 0;

  goalPremium: number = 0;
  policiespercentage: number = 0;
  agencyfeepercentage: number = 0;
}



