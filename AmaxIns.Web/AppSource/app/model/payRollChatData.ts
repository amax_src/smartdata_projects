export class PayRollChatData {
  constructor() {

    this.Data = { Actual: 0, Difference: 0, Projection:0 }
  }
  Key: string = "";
  Data: {
    Projection: number,
    Actual: number,
    Difference: number
  }
}
