export class CalendarModel {
  FromDate?: any;
  ToDate?: any;
  Month: number;
  Year: number;
  Dates: Date[] = [];
}
