import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { authenticationService } from './authenticationService.service';

@Injectable({ providedIn: "root", })

export class AlphaLoginGuard implements CanActivate {

  constructor(private service: authenticationService, private router: Router) {
  }


  canActivate() {
    // in order to check the User Roll remove the spaces and make it to small case
    let isLoggedIn: boolean = this.service.isAuthenticated;
    if (!isLoggedIn) {
      this.router.navigateByUrl('/');
    }
    
    if (this.service.userRole === "alpa") {
      isLoggedIn = true;
    }
    else {
      isLoggedIn = false;
    }

    return isLoggedIn;
  }
}
