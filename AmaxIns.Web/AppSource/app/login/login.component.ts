import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { authenticationService } from './authenticationService.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Chatdata: any;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string = '';
  loginSectionDisplay: string = "none";
    homeSectionDisplay: string = "none"
    finSectionDisplay: string = "none";
  logintype: string = '';
  showLoader: boolean = false;
  ForgetPasswordModel: boolean = false;
  ForgotPassword: FormGroup;
  isValidEmail: boolean = true;
  message: string = '';
  NotificationMessage: string = "";
    showNotication: boolean;
    roleSelectItem: SelectItem[] = [];
    welcomescreen: string = "block"
    appType: string = '';
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: authenticationService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
    this.ShowNotification();
  }

    ngOnInit() {
        //this.bindAlpaRole();
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        roletype: ['']
    });

    this.ForgotPassword = new FormGroup({
      Email: new FormControl("", [
        Validators.required,
        Validators.email
      ])

    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

    //bindAlpaRole() {
    //    this.roleSelectItem = [];
    //    this.roleSelectItem.push(
    //        {
    //            label: '---Select Role---', value: ''
    //        },
    //        {
    //            label: 'Head of Depratment', value: 'HeadofDepratment'
    //        },
    //        {
    //            label: 'Director of Sales', value: 'SaleDirector'
    //        }
    //        ,
    //        {
    //            label: 'Regional Sales Manager', value: 'RegionalSalesManager'
    //        }
    //        ,
    //        {
    //            label: 'District Sales Manager', value: 'DistrictSalesManager'
    //        }
    //        ,
    //        {
    //            label: 'StoreManager', value: 'StoreManager'
    //        }
    //    )
    //}

  //ValidateUsername() {
  //  let email = this.ForgotPassword.value["Email"];
  //  if (email !== '' && !this.ForgotPassword.controls["Email"].invalid) {
  //    this.authenticationService.validateUserName(email, this.logintype).toPromise().then(m => {
  //      this.isValidEmail = m
  //    }, e => {
  //      this.isValidEmail = false
  //    })
  //  }

  //}


  ForgetPassword() {
    let email = this.ForgotPassword.value["Email"];
    this.authenticationService.OnForgotPasswordSubmit(email, this.logintype).toPromise().then(x => {
      if (x) {
        this.message = "An email has been send to your registed email address, Please check your email.";
        alert(this.message);
      }
      else {
        this.message = "Unable to recover you password, Please contact administrator.";
        alert(this.message);
      }
      this.ForgotPassword.value["Email"] = '';
      this.ForgetPasswordModel = false;
    })
  }


  onFogotPasswordSubmit() {
    let email = this.ForgotPassword.value["Email"];
    if (email !== '' && !this.ForgotPassword.controls["Email"].invalid) {
      this.authenticationService.validateUserName(email, this.logintype).toPromise().then(m => {
        this.isValidEmail = m
        if (m) {
          this.ForgetPassword()
        }
      }, e => {
        this.isValidEmail = false
      })
    }
  }

  get f() { return this.loginForm.controls; }

    onSubmit() {
        //var _loginType = this.logintype;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        //if (this.f.roletype.value == '' && this.logintype == 'alpa') {
        //    this.error = 'Please Select Role.';
        //    return;
        //}
        //else if ((this.f.roletype.value == 'HeadofDepratment' ||
        //    this.f.roletype.value == 'SaleDirector' ||
        //    this.f.roletype.value == 'RegionalSalesManager' ||
        //    this.f.roletype.value == 'DistrictSalesManager' ||
        //    this.f.roletype.value == 'StoreManager')
        //    && this.logintype == 'alpa') {
        //    _loginType = this.logintype + '-' + this.f.roletype.value;
        //}

        this.showLoader = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value, this.logintype)
            .pipe(first())
            .subscribe(
                data => {
                    this.showLoader = false;
                    if (this.authenticationService.userRole === "zonalmanager" || this.authenticationService.userRole === "saledirector" || this.authenticationService.userRole === "regionalmanager" || this.authenticationService.userRole === "headofdepratment" || this.authenticationService.userRole === "bsm" || this.authenticationService.userRole === "storemanager") {
                        this.router.navigateByUrl("Dashboard");
                    }
                    else if (this.authenticationService.userRole === "agent") {
                        this.router.navigateByUrl("Agent-Dashboard");
                    }
                    else if (this.authenticationService.userRole.includes("-alpa")) {
                        this.router.navigateByUrl("alpa-revenue");
                    }
                    else {
                        this.router.navigateByUrl("/");
                    }
                },
                error => {
                    try {
                        this.error = error.error.message;
                    }
                    catch {

                    }
                    this.showLoader = false;
                });
    }

    loadFinDashboard() {
        this.showLoader = true;
        this.authenticationService.loginPowerBI("sss", "test123", this.logintype)
            .pipe(first())
            .subscribe(
                data => {
                    this.showLoader = false;
                    this.router.navigateByUrl("/powerBi-financial/financial-dashboard");
                },
                error => {
                    try {
                        this.error = error.error.message;
                    }
                    catch {

                    }
                    this.showLoader = false;
                });

    }

    welcomeFinScreenfn(value) {
        this.welcomescreen = "none";
        this.loginSectionDisplay = "none";
        this.homeSectionDisplay = "none";
        //this.finSectionDisplay = "block";
        this.appType = value;
        this.loadFinDashboard();
    }
    finDashboarClose() {
        this.welcomescreen = "block";
        this.loginSectionDisplay = "none";
        this.homeSectionDisplay = "none";
        this.finSectionDisplay = "none";
        this.logintype = '';
        this.appType = '';
    }

    welcomeScreenfn(value) {
        this.welcomescreen = "none";
        this.loginSectionDisplay = "none";
        this.homeSectionDisplay = "block";
        this.appType = value;
    }
    homeSectionDisplayClose() {
        this.welcomescreen = "block";
        this.loginSectionDisplay = "none";
        this.homeSectionDisplay = "none";
        this.logintype = '';
        this.appType = '';
    }
    showLoginSection(roleforamax,roleforapla) {
        this.loginSectionDisplay = "block";
        this.homeSectionDisplay = "none";
        if (this.appType == 'amax') {
            this.logintype = roleforamax;
        }
        else if (this.appType == 'alpa') {
            this.logintype = this.appType + '-' + roleforapla;
        }
    }

    showHomeSection() {
        this.loginSectionDisplay = "none";
        this.homeSectionDisplay = "block"
        this.logintype = '';
        this.submitted = false;
    }

  ShowNotification() {
    this.message = '';
    this.authenticationService.GetNotification().subscribe(m => {
      this.NotificationMessage = m;

      if (this.NotificationMessage.length > 20) {
        this.showNotication = true;
      }
    })
  }

}
