import * as MaxBIReport from '../../model/MaxBIReport';
import { MaxBIReportService } from '../../Modules/Services/MaxBIReportService';
import { CommonHelper } from '../../utilities/common-helper';
import { CalendarModel } from '../../model/CalanderModel';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RegionalManager } from '../../model/RegionalManager';
import { SelectItem } from 'primeng/api';
import { ZonalManager } from '../../model/ZonalManager';
import { Location } from '../../model/Location'
import { authenticationService } from '../../login/authenticationService.service';
import * as moment from "moment";
import { AgencyService } from '../../Modules/Services/agencyService';

@Component({
  selector: 'app-location-filter',
  templateUrl: './location-filter.component.html',
  styleUrls: ['./location-filter.component.css']
})
export class LocationFilterComponent implements OnInit {

  @Output() RmChangeEmitter = new EventEmitter();
  @Output() ZmChangeEmitter = new EventEmitter();
  @Output() LocationChangeEmitter = new EventEmitter();
  @Output() AllLocationEmitter = new EventEmitter();
  @Output() singleLocationEmitter = new EventEmitter();
  @Output() PayTypeChangeEmitter = new EventEmitter();
  @Output() ShowLoaderEmitter = new EventEmitter();
  @Output() AgentChangeEmitter = new EventEmitter();
  @Output() AllAgentEmitter = new EventEmitter();

  //firstTimeLoad: boolean = true;
  firstTimeLoad: boolean = false;
  agencyName: string = "";
  showloader: boolean = false;

  regionalManager: Array<MaxBIReport.HierarchyInfo>;
  regionalManagerSelectItem: SelectItem[] = [];
  selectedRegionalManager: Array<number> = [];

  zonalManager: Array<MaxBIReport.HierarchyInfo>;
  zonalManagerSelectItem: SelectItem[] = [];
  selectedzonalManager: Array<number> = [];

  selectedBSM: Array<number> = [];

  location: Array<MaxBIReport.HierarchyInfo>;
  locationSelectItem: SelectItem[] = [];

  payTypeSelectItem: SelectItem[] = [];

  agentName: string = "";
  extensions: string = "";
  agentsSelectItem: SelectItem[] = [];

  @Input() catalogInfo: any;

  @Input() SinglelocationSelected: number;
  @Input() selectedPayType: Array<string> = [];
  @Input() selectedlocation: Array<number> = [];
  @Input() ShowLocationDropDown: boolean = false;
  @Input() ShowLocationMultiMaxFiveSelect: boolean = false;
  @Input() showPayType: boolean = false;

  @Input() showDateRange: boolean = false;
  @Input() showMonthDates: boolean = false;
  @Input() InputRangeDates: Date[] = [];
  @Output() OutputRangeDates = new EventEmitter<CalendarModel>();

  // @Input() InputMultiDates: Date[] = [];
  multiDatesSelected: Date[] = [];
  multiDatesSelectItem: SelectItem[] = [];

  currentDate: Date = moment().utc().toDate();
  @Input() showMonth: boolean = false;
  @Input() InputMonth: Date;
  @Output() OutputMonth = new EventEmitter<CalendarModel>();

  @Input() AgentLabel: string = "Agent";
  @Input() ManualExtension: boolean = false;

  selectedMonth: CalendarModel = new CalendarModel();

  // minDate: Date;
  // maxDate: Date;

  constructor(
    private maxBIService: MaxBIReportService,
    private agencyService: AgencyService,
    private authenticationService: authenticationService
  ) {

    this.regionalManager = new Array<any>();
    this.zonalManager = new Array<any>();
    this.location = new Array<any>();
    this.selectedRegionalManager = new Array<number>();

    this.resetDateRange();
    this.InputMonth = this.currentDate;

    this.payTypeSelectItem = [
      { label: "Down Payment", value: "Down Payment" },
      { label: "Endorsement", value: "Endorsement" },
      { label: "Monthly Payment", value: "Monthly Payment" },
      { label: "Reinstatement", value: "Reinstatement" },
      { label: "Renewal", value: "Renewal" }
    ];

  }

  ngOnInit() {
    this.onDateRangeSelected();
    this.onMonthSelected();

    if (this.authenticationService.userRole === "agent") {
      console.log(this.authenticationService.user);
      this.agentName = this.authenticationService.user.userName;
      let agentId = '-999';
      let aId = -999;
      if (this.catalogInfo.From == MaxBIReport.E_MaxBICatalogFrom.PaymentInfo) {
        agentId = this.authenticationService.user.agentInfoID.toString();
        aId = this.authenticationService.user.agentInfoID;
      }
      else if (this.catalogInfo.From == MaxBIReport.E_MaxBICatalogFrom.Quotes) {
        agentId = this.authenticationService.user.agentID.toString();
        aId = this.authenticationService.user.agentID;
      }

      if (this.catalogInfo.From != MaxBIReport.E_MaxBICatalogFrom.Calls) {
        this.getLocationsByAgent(agentId);
      }

      if (!this.ManualExtension) {
        this.AgentChangeEmitter.emit([aId]);
      }
    }
    else if (this.authenticationService.userRole === "storemanager") {
      let lst = [];
      let that = this;
      that.getStoreManagerAgency(that.authenticationService.userId, function (location: MaxBIReport.HierarchyInfo) {
        that.selectedlocation.push(location.agencyId);
        lst.push({
          agencyId: location.agencyId
        });
        that.locationSelectItem.push(
          {
            label: location.agencyTitle, value: location.agencyId
          })
        that.getAgents();
        that.AllLocationEmitter.emit(lst);
      });
    }
    else {
      this.loadCompleteHierarchy();
    }

    /*****************************/

    if (this.authenticationService.user.userAgencyList && this.authenticationService.user.userAgencyList.length > 0 && this.ManualExtension) { // only for calls related reports
      let lst = [];
      this.authenticationService.user.userAgencyList.forEach((ag: any) => {
        this.selectedlocation.push(ag.agencyId);
        lst.push({
          agencyId: ag.agencyId
        });
        this.locationSelectItem.push(
          {
            label: ag.agencyName, value: ag.agencyId
          });
      });
      this.getAgents();
      this.AllLocationEmitter.emit(lst);
    }
  }

  getStoreManagerAgency(Id: number, callback: Function = null) {
    this.locationSelectItem = [];
    this.agencyService.getStoreManagerAgency(Id).subscribe(m => {
      m.forEach(i => {
        this.location.push({
          agencyId: i.agencyId,
          agencyTitle: i.agencyName,
          rmId: 0,
          rmTitle: '',
          zmId: 0,
          zmTitle: ''
        });
      })
      this.agencyName = this.location[0].agencyTitle;
      if (callback) {
        callback(this.location[0]);
      }
      this.ShowLoaderEmitter.emit(false);
    });
  }

  resetDateRange() {
    let range = CommonHelper.getFirstLastDates(this.currentDate);
    if (this.InputRangeDates.length == 0) {
      this.InputRangeDates.push(range.FromDate, range.ToDate);
    }
    else {
      this.InputRangeDates[0] = range.FromDate;
      this.InputRangeDates[1] = range.ToDate;
    }
    console.log(this.InputRangeDates.join(' - '));
  }

  onRegionalManagerchange(e) {
    let filtered = this.fillHierarchy(2, 0, this.selectedRegionalManager);
    this.fillHierarchy(3, 0, filtered);
    this.RmChangeEmitter.emit(this.selectedRegionalManager);

    this.SinglelocationSelected = 0;
    this.selectedlocation = [];
    this.onLocationDropDownchange();
    this.onLocationchange();
  }

  onZonalManagerchange(e) {
    this.fillHierarchy(3, 0, this.selectedzonalManager);
    this.ZmChangeEmitter.emit(this.selectedzonalManager);

    this.SinglelocationSelected = 0;
    this.selectedlocation = [];
    this.onLocationDropDownchange();
    this.onLocationchange();
  }

  onLocationDropDownchange() {
    let data: any = {
      locations: [{
        agencyId: this.SinglelocationSelected
      }],
      firstTimeLoad: this.firstTimeLoad
    };
    this.selectedlocation = [];
    if (this.SinglelocationSelected > 0) {
      this.selectedlocation.push(this.SinglelocationSelected);
    }
    if (this.authenticationService.userRole !== "agent") {
      this.getAgents();
    }
    this.singleLocationEmitter.emit(data);
  }

  onLocationchange() {
    let data: any = {
      locations: this.selectedlocation,
      firstTimeLoad: this.firstTimeLoad
    };
    if (this.authenticationService.userRole !== "agent") {
      this.getAgents();
    }
    this.LocationChangeEmitter.emit(data);
  }

  onPayTypeChange(e) {
    this.PayTypeChangeEmitter.emit(e.value);
  }

  onAgentChange(e) {
    let agents = [];
    /*if (e.type == 'blur') {
      if (this.extensions.length > 0) {
        this.extensions.split(',').forEach(ext => {
          ext = ext.trim();
          if (ext.length > 0) {
            if (CommonHelper.isNumeric(ext)) {
              agents.push(ext);
            }
          }
        });
      }
    }
    else {*/
    agents = e.value;
    //}
    this.AgentChangeEmitter.emit(agents);
  }

  onCheckDates(event) {
    /*let fromDate = this.InputRangeDates[0];
    let toDate = this.InputRangeDates[1];

    if (fromDate && toDate) {
      let diff = CommonHelper.getDateTimeDifference(fromDate, toDate);
      if (diff.days > 31) {
        //this.notifier.showNotification("Maximum 31 days selection is allowed.", "OK", "error");
        this.resetDateRange();
      }
    }*/
  }

  onDateRangeSelected() {
    let range: CalendarModel = new CalendarModel();
    range.FromDate = this.InputRangeDates[0];
    range.ToDate = this.InputRangeDates[1];

    if (!range.ToDate) {
      range.ToDate = range.FromDate;
    }

    range.FromDate = moment(range.FromDate).format("YYYY-MM-DD");
    range.ToDate = moment(range.ToDate).format("YYYY-MM-DD");

    this.OutputRangeDates.emit(range);
  }

  onMonthSelected() {
    this.selectedMonth = new CalendarModel();
    this.selectedMonth.Month = this.InputMonth.getMonth() + 1;
    this.selectedMonth.Year = this.InputMonth.getFullYear();

    this.selectedMonth.FromDate = moment(this.selectedMonth.Year + '-1-1').format("YYYY-MM-DD");
    this.selectedMonth.ToDate = moment(CommonHelper.getFirstLastDates(this.InputMonth).ToDate).format("YYYY-MM-DD");

    this.multiDatesSelected = [];
    this.setMonthMinMaxDates();

    this.OutputMonth.emit(this.selectedMonth);
  }

  onMultiDatesSelected(e) {
    this.selectedMonth.Dates = this.multiDatesSelected;
    this.OutputMonth.emit(this.selectedMonth);
  }

  setMonthMinMaxDates() {
    let range = CommonHelper.getFirstLastDates(this.InputMonth);

    this.multiDatesSelectItem = [];
    while (range.FromDate <= range.ToDate) {
      this.multiDatesSelectItem.push({
        label: moment(range.FromDate).format("MMMM DD"),
        value: moment(range.FromDate).format("YYYY-MM-DD")
      });
      range.FromDate.setDate(range.FromDate.getDate() + 1);
    }
  }

  setFirstLocation() {
    this.selectedlocation = [];
    if (this.location.length > 0) {
      this.selectedlocation.push(this.location[0].agencyId);
      this.SinglelocationSelected = this.location[0].agencyId;
    }
    if (this.ShowLocationDropDown) {
      this.onLocationDropDownchange();
    }
    else {
      this.onLocationchange();
    }
  }

  getLocationsByAgent(agentId: string) {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
    this.selectedlocation = [];

    let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest;
    request.CatalogFrom = this.catalogInfo.From;
    request.CatalogType = MaxBIReport.E_MaxBICatalogType.Agency;
    request.AgentIds.push(agentId);

    let lst = [];
    this.maxBIService.GetCatalogs(request).subscribe(m => {
      m.forEach(i => {
        this.locationSelectItem.push(
          {
            label: i.title, value: i.id
          });
        this.selectedlocation.push(i.id);
        lst.push({
          agencyId: (i.id as unknown as number)
        });
      });
      this.ShowLoaderEmitter.emit(false);
      this.AllLocationEmitter.emit(lst);
    });
  }
  getAgents() {
    this.ShowLoaderEmitter.emit(true);
    this.agentsSelectItem = [];
    let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest;
    request.CatalogFrom = this.catalogInfo.From;
    request.CatalogType = this.catalogInfo.Type;
    request.AgencyIds = this.selectedlocation;

    let lst = [];
    this.maxBIService.GetCatalogs(request).subscribe(m => {
      m.forEach(i => {
        this.agentsSelectItem.push(
          {
            label: i.title, value: i.id
          });
        lst.push(i.id);
      });
      this.AllAgentEmitter.emit(lst);
      this.ShowLoaderEmitter.emit(false);
    });

  }

  //#region User Hierarchy
  loadCompleteHierarchy() {
    let request = new MaxBIReport.MaxBIHierarchyRequest();
    request.Level = 3;
    if (this.authenticationService.userRole === "regionalmanager"
      || this.authenticationService.userRole === "zonalmanager"
      || this.authenticationService.userRole === "storemanager") {

      request.FilterByIds = this.authenticationService.userId.toString();
      if (this.authenticationService.userRole === "regionalmanager") {
        request.FilterBy = 1;
      }
      else if (this.authenticationService.userRole === "zonalmanager") {
        request.FilterBy = 2;
      }
    }

    this.ShowLoaderEmitter.emit(true);
    this.maxBIService.GetHierarchyLevel(request).subscribe(m => {
      this.location = m;
      this.zonalManager = CommonHelper.uniqueList(m, "rmId,rmTitle,zmId,zmTitle").sort(CommonHelper.sortOrderComparer("zmTitle"));
      this.regionalManager = CommonHelper.uniqueList(m, "rmId,rmTitle").sort(CommonHelper.sortOrderComparer("rmTitle"));

      this.fillHierarchy(3);
      this.fillHierarchy(2);
      this.fillHierarchy(1);

      this.firstTimeLoad = false;
      this.ShowLoaderEmitter.emit(false);
    });
  }

  fillHierarchy(level: number, levelId: number = 0, prevLevelIds: number[] = []): number[] {
    let filtered: number[] = [];
    let lst = new Array<MaxBIReport.HierarchyInfo>();
    switch (level) {
      case 1:
        this.regionalManagerSelectItem = [];
        this.selectedRegionalManager = [];
        lst = this.regionalManager.filter(r => r.rmId == (levelId > 0 ? levelId : r.rmId));
        lst.forEach(i => {
          filtered.push(i.rmId);
          this.regionalManagerSelectItem.push(
            {
              label: i.rmTitle, value: i.rmId
            })
        })
        break;
      case 2:
        this.zonalManagerSelectItem = [];
        this.selectedzonalManager = [];
        lst = this.zonalManager.filter(r => r.zmId == (levelId > 0 ? levelId : r.zmId));
        if (prevLevelIds.length > 0) {
          lst = lst.filter(r => prevLevelIds.includes(r.rmId));
        }
        lst.forEach(i => {
          filtered.push(i.zmId);
          this.zonalManagerSelectItem.push(
            {
              label: i.zmTitle, value: i.zmId
            })
        })
        break;
      case 3:
        this.locationSelectItem = [];
        this.selectedlocation = [];
        lst = this.location;
        if (prevLevelIds.length > 0) {
          lst = this.location.filter(r => prevLevelIds.includes(r.zmId));
        }
        lst.forEach(i => {
          filtered.push(i.rmId);
          this.locationSelectItem.push(
            {
              label: i.agencyTitle, value: i.agencyId
            })
        });
        this.AllLocationEmitter.emit(lst);
        break;
    }
    this.agentsSelectItem = [];
    return filtered;
  }
  //#endregion

  filterReset() {
    //this.firstTimeLoad = true;
    this.selectedRegionalManager = [];
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.agentsSelectItem = [];

    this.fillHierarchy(3);
    this.fillHierarchy(2);
    this.fillHierarchy(1);

    this.resetDateRange();
    this.InputMonth = this.currentDate;
  }

}

