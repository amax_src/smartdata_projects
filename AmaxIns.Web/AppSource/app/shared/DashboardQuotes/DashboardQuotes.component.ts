import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { DashBoardService } from '../../Modules/Services/dashBoardService';
import { DashboardQuotesModel } from '../../model/DashboardQuotesModel';


@Component({
  selector: 'app-DashboardQuotes',
  templateUrl: './DashboardQuotes.component.html',
  styleUrls: ['./DashboardQuotes.component.scss']
})
export class DashboardQuotesComponent implements OnInit {
  dashboardQuotesData: DashboardQuotesModel = new DashboardQuotesModel();
  _month: number;
  _year: number;
  @Input() location: number;
  @Input() set month(_month: number) {
    this._month = _month
    if (this._month && this._year) {
      this.getdata();
    }
  }

  @Input() set year(year: number) {
    this._year = year;
    if (this._month && this._year) {
      this.getdata();
    }
  }
  showLoader: boolean = false;
  constructor(public cmnSrv: CommonService,
    private router: Router, private _dashboardService: DashBoardService) {

  }

  ngOnInit() {
    //this.getdata();
  }

  getdata() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardQuotesData = new DashboardQuotesModel();
    this.showLoader = true;
    this._dashboardService.getQuotesDashboard(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardQuotesData = m[0]
      if (!this.dashboardQuotesData) {
        this.dashboardQuotesData = new DashboardQuotesModel();
        this.dashboardQuotesData.closingRatioModQuotes = 0;
        this.dashboardQuotesData.closingRatioNewQuotes = 0;
        this.dashboardQuotesData.newQuotes = 0;
      }
      this.showLoader = false;
    })
  }


}

