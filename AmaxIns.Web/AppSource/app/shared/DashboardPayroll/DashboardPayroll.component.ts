import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { DashBoardService } from '../../Modules/Services/dashBoardService';
import { DashboardPayrollModel } from '../../model/DashboardPayrollModel';


@Component({
  selector: 'app-DashboardPayroll',
  templateUrl: './DashboardPayroll.component.html',
  styleUrls: ['./DashboardPayroll.component.scss']
})
export class DashboardPayrollComponent implements OnInit {
  dashboardPayrollData: DashboardPayrollModel = new DashboardPayrollModel();
  _month: number;
  _year: number;
  @Input() location: number;
  @Input() set month(_month: number) {
    this._month = _month
    if (this._month && this._year) {
      this.getdata();
    }
  }

  @Input() set year(year: number) {
    this._year = year;
    if (this._month && this._year) {
      this.getdata();
    }
  }

  constructor(public cmnSrv: CommonService,
    private router: Router,
    private _dashboardService: DashBoardService) {

  }
 showLoader: boolean = false;
  ngOnInit() {
    //this.getdata();
  }

  getdata() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardPayrollData = new DashboardPayrollModel();
    this.showLoader = true;
    this._dashboardService.getPayrollDashboard(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardPayrollData = m[0]
      if (!this.dashboardPayrollData) {
        this.dashboardPayrollData = new DashboardPayrollModel();
        this.dashboardPayrollData.otBudget = 0;
        this.dashboardPayrollData.otActual = 0;
        this.dashboardPayrollData.otPerPolicy = 0;
        this.dashboardPayrollData.otHoursPerPolicy = 0;
        this.dashboardPayrollData.payrollActual = 0;
        this.dashboardPayrollData.payrollBudget = 0;
        this.dashboardPayrollData.payrollPerPolicy = 0;
        this.dashboardPayrollData.payrollPerTransaction = 0;
      } this.showLoader = false;
    })
  }


}

