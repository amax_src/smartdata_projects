import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navmenuAlpha',
  templateUrl: './navmenu-alpha.component.html',
  styleUrls: ['./navmenu-alpha.component.scss']
})
export class NavmenuAlphaComponent implements OnInit {

  PBXclass = 'active';
  CustomerInfoclass = '';
  TopAgentclass = '';

  constructor(public cmnSrv: CommonService, private router: Router) { }

  ngOnInit() {
    this.setSelectedMenu(this.router.url.replace('/', ''));
  }

  Navigate(e) {
    this.PBXclass = '';
    this.CustomerInfoclass = '';
    this.TopAgentclass = '';
    if (e === "PBX") {
      this.router.navigateByUrl('/PBX')
      this.PBXclass = 'active';
    }
    else if (e === "CustomerInfo") {
      this.router.navigateByUrl('/CustomerInfo')
      this.CustomerInfoclass = 'active'
    }
    else if (e === "TopAgent") {
      this.router.navigateByUrl('/TopAgent')
      this.TopAgentclass = 'active';
    }

  }

  setSelectedMenu(e) {
    this.PBXclass = '';
    this.CustomerInfoclass = '';

    if (e === "PBX") {
      this.PBXclass = 'active';
    }
    else if (e === "CustomerInfo") {
      this.CustomerInfoclass = 'active'
    }
    else if (e === "TopAgent") {
      this.TopAgentclass = 'active'
    }

  }
}
