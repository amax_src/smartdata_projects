import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { DashBoardService } from '../../Modules/Services/dashBoardService';
import { DashboardEPRModel } from '../../model/DashboardEPRModel';
import { DashboardEPRDetails } from '../../model/DashboardEPRDetails';


@Component({
  selector: 'app-DashboardEPR',
  templateUrl: './DashboardEPR.component.html',
  styleUrls: ['./DashboardEPR.component.scss']
})

export class DashboardEPRComponent implements OnInit {
  dashboardEPRData: DashboardEPRModel = new DashboardEPRModel();
  dashboardEPRWeeklyDaily: Array<DashboardEPRDetails> = new Array<DashboardEPRDetails>();
  display: boolean = false;
  _month: number;
  _year: number;
  @Input() location: number;
  @Input() set month(_month: number) {
    this._month = _month
    if (this._month && this._year) {
      this.getdata();
    }
  }
  @Input() set year(year: number) {
    this._year = year;
    if (this._month && this._year) {
      this.getdata();
    }
  }

  constructor(public cmnSrv: CommonService,
    private router: Router, private _dashboardService: DashBoardService) {

  }

  ngOnInit() {
    //this.getdata();
  }
  showLoader: boolean = false;
  PopUpLoader: boolean = true;
  getdata() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardEPRData = new DashboardEPRModel();
    this.showLoader = true;
    this._dashboardService.getEPRDashboard(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardEPRData = m[0]
      if (!this.dashboardEPRData) {
        this.dashboardEPRData = new DashboardEPRModel();
        this.dashboardEPRData.averagepolicies = 0;
        this.dashboardEPRData.averageagencyfee = 0;
        this.dashboardEPRData.averagepremium = 0;
        this.dashboardEPRData.averagetransactions = 0;
      }
      this.showLoader = false;
    })
  }

  showDialog() {
    this.PopUpLoader = true;
    this.getDataWeeklyandDaily();
    this.display = true;
  }

  getDataWeeklyandDaily() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardEPRWeeklyDaily = new Array<DashboardEPRDetails>();
    this._dashboardService.getEPRDetails(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardEPRWeeklyDaily = m;//["result"];
      this.PopUpLoader = false;
    });
  }
}

