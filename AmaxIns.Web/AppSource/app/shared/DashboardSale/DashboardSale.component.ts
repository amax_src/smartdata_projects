import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { DashBoardService } from '../../Modules/Services/dashBoardService';
import { DashboardRevenueModel } from '../../model/DashboardRevenueModel';


@Component({
  selector: 'app-DashboardSale',
  templateUrl: './DashboardSale.component.html',
  styleUrls: ['./DashboardSale.component.scss']
})
export class DashboardSaleComponent implements OnInit {
  dashboardSaleData: DashboardRevenueModel = new DashboardRevenueModel();
  _month: number;
  _year: number;
  @Input() location: number;
  @Input() set month(_month: number) {
    this._month = _month
    if (this._month && this._year) {
      this.getdata();
    }
  }

  @Input() set year(year: number) {
    this._year = year;
    if (this._month && this._year) {
      this.getdata();
    }
  }
  showLoader: boolean = false;
  constructor(public cmnSrv: CommonService,
    private router: Router, private _dashboardService: DashBoardService) {

  }

  ngOnInit() {
    //this.getdata();
  }

  getdata() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardSaleData = new DashboardRevenueModel();
    this.showLoader = true;
    console.log("_month", this._month + " " + this._year);
    this._dashboardService.GetRevenueDashboard(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardSaleData = m[0]
      if (!this.dashboardSaleData) {
        this.dashboardSaleData = new DashboardRevenueModel();
        this.dashboardSaleData.perDiffPolicies = 0;
        this.dashboardSaleData.perDiffAFee = 0;
        this.dashboardSaleData.perDiffPremium = 0;
      }
      this.showLoader = false;
    })
  }


}

