import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { DashBoardService } from '../../Modules/Services/dashBoardService';
import { DashboadSpectrumModel } from '../../model/DashboadSpectrumModel';


@Component({
  selector: 'app-DashboardCalls',
  templateUrl: './DashboardCalls.component.html',
  styleUrls: ['./DashboardCalls.component.scss']
})
export class DashboardCallsComponent implements OnInit {
  dashboardSpectrumData: DashboadSpectrumModel = new DashboadSpectrumModel();
  _month: number;
  _year: number;
  @Input() location: number;
  @Input() set month(_month: number) {
    this._month = _month
    if (this._month && this._year) {
      this.getdata();
    }
  }

  @Input() set year(year: number) {
    this._year = year;
    if (this._month && this._year) {
      this.getdata();
    }
  }

  constructor(public cmnSrv: CommonService,
    private router: Router,
    private _dashboardService: DashBoardService) {

  }
 showLoader: boolean = false;
  ngOnInit() {
    //this.getdata();
  }

  getdata() {
    let agencyId: Array<number> = [];
    agencyId.push(this.location);
    this.dashboardSpectrumData = new DashboadSpectrumModel();
    this.showLoader = true;
    this._dashboardService.getCallsDashboard(this._month, this._year, agencyId).subscribe(m => {
      this.dashboardSpectrumData = m[0]
      if (!this.dashboardSpectrumData) {
        this.dashboardSpectrumData = new DashboadSpectrumModel();
        this.dashboardSpectrumData.outboundCalls = 0;
        this.dashboardSpectrumData.inboundCalls = 0;
      } this.showLoader = false;
    })
  }


}

