import { Component, OnInit, Input } from '@angular/core';
import { authenticationService } from '../../login/authenticationService.service';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { PasswordService } from '../../Modules/Services/passwordService';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { SessionService } from '../../core/storage/sessionservice';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input() PageName: string;
    userName: string = "";
    display: boolean = false;
    ResetPasswordForm: FormGroup;
    isValidPassword = true;
    constructor(private authservice: authenticationService, public cmnSrv: CommonService,
        private messageService: MessageService, private router: Router, private passwordService: PasswordService) {
    }

    ngOnInit() {
        this.ResetPasswordForm = new FormGroup({
            password: new FormControl("", [
                Validators.required,
            ]),
            newPassword: new FormControl("", [
                Validators.required,
                Validators.minLength(8)
            ]),
            repeatPassword: new FormControl("", [
                Validators.required,
                Validators.minLength(8)
            ])
        });
        //setInterval(this.autoLogout, 20000);
        setInterval(this.autoLogout, 7200000);//One hour
        //setInterval(this.autoLogout, 7200000);//two hour
    }

    autoLogout() {
        let sessionService: SessionService = new SessionService()
        sessionService.delete('currentUser');
        window.location.href = "/";

    }

    onSubmit() {
        if (this.ResetPasswordForm.valid && this.isValidPassword && (this.ResetPasswordForm.value["newPassword"] === this.ResetPasswordForm.value["repeatPassword"])) {
            this.passwordService.UpdatePassword(this.authservice.loginName, this.ResetPasswordForm.value["password"], this.ResetPasswordForm.value["newPassword"], this.ResetPasswordForm.value["repeatPassword"], this.authservice.loginName).toPromise().then(m => {
                this.display = false;
                if (m === "Success") {
                    alert('Password changed successfully');
                    this.router.navigateByUrl("/");
                }
                else {
                    alert('Error: Unable to change password');
                }
            })
            this.ResetPasswordForm.reset();
        }
    }

    Logout() {
        this.authservice.logout();
        this.router.navigateByUrl("/");
    }

    ResetPassword() {
        this.display = true;
    }

    ValidateCurrentPassword() {
        let password = this.ResetPasswordForm.value["password"];
        let username = this.authservice.loginName;
        let role = this.authservice.userRole;
        if (password !== '') {
            this.authservice.login(username, password, role).subscribe(m => {
                this.isValidPassword = true
            }, e => {
                this.isValidPassword = false
            })
        }
    }

    goToHome(path) {
        this.router.navigateByUrl(path);
    }

}

