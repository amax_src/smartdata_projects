import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../core/common.service';
import { Router } from '@angular/router';
import { authenticationService } from '../../login/authenticationService.service';
//import base64 from 'base-64';
//import utf8 from 'utf8';
import { encryption } from '../../Modules/Services/Encryption';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements OnInit {
  Dashboardclass = 'active'
  revenueclass = '';
  payrollclass = '';
  spectrumclass = ''
  eprclass = ''
  aisclass = ''
  Tierclass = ''
  YearOnYearRevenueClass = ''
  CarrierMixClass = ''
  RevenueDaily: string = '';
  spectrumMonthlyclass: string = '';
  spectrumRepeatedCallclass: string = '';
  BoundQuotesClass: string = '';
  ZmDashboardClass: string = '';
  PlannerClass: string = '';
  CustomerRetentionClass: string = '';
    idHOD: string = '';
    currentMenu: string = '';
    applicationType: string = '';
    powerBifinancial: string = '';
    bolretention: string = '';
    cfdretention: string = '';
    productiontracker: string = '';
    revenueprediction: string = '';
    topbottom: string = '';
    dailytransactionpayments: string = '';

  constructor(public cmnSrv: CommonService, private router: Router,
      private authenticationService: authenticationService, private encrypt: encryption) { }

    ngOnInit() {
        this.setSelectedMenu(this.router.url.replace('/', ''));
        this.idHOD = this.authenticationService.userRole;
        this.applicationType = this.authenticationService.applicationType;
    }

  Navigate(e) {
    this.Dashboardclass = ''
    this.revenueclass = '';
    this.payrollclass = '';
    this.spectrumclass = '';
    this.eprclass = '';
    this.aisclass = '';
    this.Tierclass = '';
    this.CarrierMixClass = "";
    this.YearOnYearRevenueClass = ''
    this.RevenueDaily = ''
    this.spectrumMonthlyclass = '';
    this.spectrumRepeatedCallclass = '';
    this.BoundQuotesClass = '';
    this.ZmDashboardClass = '';
    this.PlannerClass = '';
      this.CustomerRetentionClass = '';
      this.powerBifinancial = '';
      this.bolretention = '';
      this.cfdretention = '';
      this.productiontracker = '';
      this.revenueprediction = '';
      this.topbottom = '';
    if (e === "Dashboard") {
      this.Dashboardclass = "active";
      this.router.navigateByUrl('/Dashboard')
    }
    if (e === "Revenue") {
      this.revenueclass = "active";
      this.router.navigateByUrl('/Revenue')
    }
    if (e === "RevenueDaily") {
      this.RevenueDaily = "active";
      this.router.navigateByUrl('/RevenueDaily')
    }
    else if (e === "YearOnYearRevenue") {
      this.YearOnYearRevenueClass = "active";
      this.router.navigateByUrl('/YearOnYearRevenue')
    }
    else if (e === "Tier") {
      this.Tierclass = "active";
      this.router.navigateByUrl('/Tier')
    }
    else if (e === "Payroll") {
      this.payrollclass = "active";
      this.router.navigateByUrl('/Payroll')
    }

    else if (e === "Spectrum") {
      this.spectrumclass = "active";
      this.router.navigateByUrl('/Spectrum')
    }
    else if (e === "SpectrumMonthly") {
      this.spectrumMonthlyclass = "active";
      this.router.navigateByUrl('/SpectrumMonthly')
    }
    else if (e === "SpectrumRepeatedCall") {
      this.spectrumRepeatedCallclass = "active";
      this.router.navigateByUrl('/SpectrumRepeatedCall')
    }

    else if (e === "EPR") {
      this.eprclass = "active";
      this.router.navigateByUrl('/EPR')
    }
    else if (e === "AIS") {
      this.aisclass = "active";
      this.router.navigateByUrl('/AIS')
    }
    else if (e === "CarrierMix") {
      this.CarrierMixClass = "active";
      this.router.navigateByUrl('/CarrierMix')
    }
    else if (e === "NewModifiedQuotes") {
      this.BoundQuotesClass = "active";
      this.router.navigateByUrl('/NewModifiedQuotes')
    }
    else if (e === "ZmDashboard") {
      this.ZmDashboardClass = "active";
      this.router.navigateByUrl('/ZmDashboard')
    }
    else if (e == "LiveDataApp") {
      //window.open("http://18.216.112.189:8087/Home?userId=" + this.encrytptvalue(this.authenticationService.userId, true) + "&UserRole=" + this.encrytptvalue(this.authenticationService.userRole, true));
        window.open("http://18.216.112.189:8087/Home?userId="
            + this.encrypt.encrytptvalue(this.authenticationService.userId, true) + "&UserRole="
            + this.encrypt.encrytptvalue(this.authenticationService.userRole, true));
    }
    else if (e == "Planner") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/cal')
    }
    else if (e == "avgagencyfee") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/avg-agency-fee')
    }
    else if (e == "agentperformance") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/agent-performace')
    }
    else if (e == "uniquecreatedquotes") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/unique-created-quotes')
    } else if (e == "overtime") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/overtime')
    }
    else if (e == "carrier-mix") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/carrier-mix-i')
    }
    else if (e == "tracker") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/tracker')
    }
    else if (e == "agencybreakdown") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/agency-breakdown')
    }
    else if (e == "rentersreport") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/renters-report')
    }
    else if (e == "activecustomers") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/active-customers')
    }
    else if (e == "CustomerRetentionList") {
      this.CustomerRetentionClass = "active";

      if (this.authenticationService.userRole === "storemanager") {
        this.router.navigateByUrl('/CustomerRetentionList');
      }
      else if (this.authenticationService.userRole === "bsm") {

      }
      else if (this.authenticationService.userRole === "zonalmanager") {
        this.router.navigateByUrl('/CustomerRetentionCountListForZone');
      }
      else if (this.authenticationService.userRole === "regionalmanager") {
        this.router.navigateByUrl('/CustomerRetentionCountListForRegion');
      }
      else if (this.authenticationService.userRole === "headofdepratment") {
        this.router.navigateByUrl('/CustomerRetentionCountListForHod');
      }
    } else if (e == "Calendar") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/monthly-view')
    }
    else if (e == "activity-dashboard") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/activity-dashboard')
    }
    else if (e == "PlannerRequest") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/plannerRequest')
    }
    else if (e == "ActualEntry") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/actualEntry')
    }
    else if (e == "plannerdashboard") {
      console.log("dd");
      this.PlannerClass = "active";
      this.router.navigateByUrl('/plannerdashboard')
    }
    else if (e == "QuotesProjection") {
      this.PlannerClass = "active";
      this.router.navigateByUrl('/QuotesProjection')
      }

    else if (e == "powerBi-financial") {
        this.powerBifinancial = "active";
        this.router.navigateByUrl('/powerBi-financial/financial-dashboard')
    }
    else if (e == "bol-retention") {
        this.bolretention = "active";
        this.router.navigateByUrl('/powerBi-financial/bol-retention')
    }
    else if (e == "cfd-retention") {
        this.cfdretention = "active";
        this.router.navigateByUrl('/powerBi-financial/cfd-retention')
    }
    else if (e == "production-tracker") {
        this.productiontracker = "active";
        this.router.navigateByUrl('/powerBi-financial/production-tracker')
    }
    else if (e == "revenue-prediction") {
        this.revenueprediction = "active";
        this.router.navigateByUrl('/powerBi-financial/revenue-prediction')
    }
    else if (e == "top-bottom") {
        this.topbottom = "active";
        this.router.navigateByUrl('/powerBi-financial/top-bottom')
    }
  }
    //encrytptvalue(value: any, isEncrypt: boolean = true) {
    //    let response: any;
    //    if (value != null && value != '') {

    //        let bytes: any;
    //        if (isEncrypt) {
    //            bytes = utf8.encode(value.toString());
    //            response = base64.encode(bytes);
    //        }
    //        else {
    //            bytes = base64.decode(value.toString());
    //            response = utf8.decode(bytes);

    //        }
    //    }
    //    return response;
    //}

    NavigateAgent(menu: string, param?: any) {
        this.currentMenu = menu;

        let p = [];
        let m = '/' + menu;
        if (param) {
            for (const key in param) {
                if (Object.prototype.hasOwnProperty.call(param, key)) {
                    this.currentMenu = menu + '_' + param[key];
                    const value = this.encrypt.encrytptvalue(param[key]);
                    p.push(key + '=' + value);
                }
            }
        }

        if (p.length > 0) {
            m += '?' + p.join('&');
        }

        console.log(m);
        this.router.navigateByUrl(m, {
            queryParamsHandling: 'preserve',
            preserveFragment: false
        });
    }

    AlpaNavigate(e) {
        this.YearOnYearRevenueClass = ''; this.revenueclass = ''; this.eprclass = ''; this.RevenueDaily = ''; this.Tierclass = '';
        this.dailytransactionpayments = '';
        if (e == "AlpaRevenue") {
            this.revenueclass = "active";
            this.router.navigateByUrl('/alpa-revenue')
        }
        else if (e == "AlpaEPR") {
            this.eprclass = "active";
            this.router.navigateByUrl('/alpa-epr')
        }
        else if (e == "AlpaAIS") {
            this.YearOnYearRevenueClass = "active";
            this.router.navigateByUrl('/alpa-ais')
        }
        else if (e == "AlpaRevenueDaily") {
            this.RevenueDaily = "active";
            this.router.navigateByUrl('/alpa-daily-revenue')
        }
        else if (e == "Tier") {
            this.Tierclass = "active";
            this.router.navigateByUrl('/alpa-tier-goal')
        }
        else if (e == "daily-transaction-payments") {
            this.dailytransactionpayments = "active";
            this.router.navigateByUrl('/daily-transaction-payments')
        }
    }

  setSelectedMenu(e) {
    this.Dashboardclass = '';
    this.revenueclass = '';
    this.payrollclass = '';
    this.spectrumclass = '';
    this.eprclass = '';
    this.aisclass = '';
    this.Tierclass = ''
    this.YearOnYearRevenueClass = ''
    this.CarrierMixClass = '';
    this.RevenueDaily = ''
    this.spectrumMonthlyclass = '';
    this.spectrumRepeatedCallclass = '';
    this.BoundQuotesClass = '';
    this.ZmDashboardClass = '';
    this.PlannerClass = '';
    if (e === "Dashboard") {
      this.Dashboardclass = 'active';
    }
    if (e === "Revenue") {
      this.revenueclass = 'active';
    }
    if (e === "RevenueDaily") {
      this.RevenueDaily = "active";
    }
    else if (e === "YearOnYearRevenue") {
      this.YearOnYearRevenueClass = "active";
    }
    else if (e === "Tier") {
      this.Tierclass = 'active';
    }
    else if (e === "Payroll") {
      this.payrollclass = "active";
    }

    else if (e === "Spectrum") {
      this.spectrumclass = "active";
    }
    else if (e === "SpectrumMonthly") {
      this.spectrumMonthlyclass = "active";
    }
    else if (e === "SpectrumRepeatedCall") {
      this.spectrumRepeatedCallclass = "active";
    }

    else if (e === "EPR") {
      this.eprclass = "active";
    }
    else if (e === "AIS") {
      this.aisclass = "active";
    }
    else if (e === "CarrierMix") {
      this.CarrierMixClass = "active";
    }
    else if (e === "NewModifiedQuotes") {
      this.BoundQuotesClass = "active";
    }
    else if (e === "ZmDashboard") {
      this.ZmDashboardClass = 'active';
    }
    else if (e === "Planner") {
      this.PlannerClass = "active";
    }
  }
}
