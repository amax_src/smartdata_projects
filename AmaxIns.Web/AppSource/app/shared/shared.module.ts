import { CurrencyPipe } from '@angular/common';
import { ErrorDialogComponent } from './errors/error-dialog/error-dialog.component';
import { ErrorDialogService } from './errors/error-dialog.service';

import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { NavmenuComponent } from './navmenu/navmenu.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonService } from '../core/common.service';
import { RmZmLocationDropdownComponent } from './rm-zm-location-dropdown/rm-zm-location-dropdown.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { NavmenuAlphaComponent } from './navmenu-Alpha/navmenu-alpha.component';
import { DialogModule } from 'primeng/dialog'
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast'
import { MessageService } from 'primeng/api';
import { DashboardSaleComponent } from './DashboardSale/DashboardSale.component';
import { DashboardQuotesComponent } from './DashboardQuotes/DashboardQuotes.component';
import { DashboardEPRComponent } from './DashboardEPR/DashboardEPR.component';
import { DashboardCallsComponent } from './DashboardCalls/DashboardCalls.component';
import { DashboardPayrollComponent } from './DashboardPayroll/DashboardPayroll.component';

import { LocationFilterComponent } from './location-filter/location-filter.component';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ChartModule } from 'primeng/chart';
import { CalendarModule as pCalendarModule } from 'primeng/calendar';
import { MatProgressBarModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { MaxbiSummaryComponent } from './../Modules/MaxBIReports/maxbi-summary/maxbi-summary.component';
import {RmZmLocationDropdownAlpaComponent } from './rm-zm-location-dropdown-alpa/rm-zm-location-dropdown-alpa.component';

const sharedComponents = [HeaderComponent, NavmenuComponent, RmZmLocationDropdownComponent, NavmenuAlphaComponent, DashboardSaleComponent, DashboardQuotesComponent, DashboardEPRComponent, DashboardCallsComponent, DashboardPayrollComponent,
    LocationFilterComponent, MaxbiSummaryComponent, ErrorDialogComponent,RmZmLocationDropdownAlpaComponent];


@NgModule({
    declarations: [sharedComponents],
    exports: [sharedComponents],
  imports: [
    BrowserModule,
      BrowserAnimationsModule,
      MatProgressBarModule,
      MatSnackBarModule,
      MatProgressSpinnerModule,
      MultiSelectModule,
      MatDialogModule,
    FormsModule,
    DialogModule,
    DropdownModule,
    InputTextModule,
    ReactiveFormsModule,
      ToastModule,
      TableModule,
      TabViewModule,
      ChartModule,
      pCalendarModule,
  ],
  providers: [CommonService, MessageService, CurrencyPipe, ErrorDialogService],
    entryComponents: [ErrorDialogComponent]
})
export class SharedModule { }
