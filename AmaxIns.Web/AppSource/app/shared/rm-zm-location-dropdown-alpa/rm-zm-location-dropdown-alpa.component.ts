import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RegionalManager } from '../..//model/RegionalManager';
import { SelectItem } from 'primeng/api';
import { ZonalManager } from '../../model/ZonalManager';
import { Location } from '../../model/Location'
import { UserService } from '../../Modules/Services/UserService';
import { AgencyService } from '../../Modules/Services/agencyService';
import { authenticationService } from '../../login/authenticationService.service';
import { SaleDirector } from '../..//model/SaleDirector';
import { AlpaUserService} from '../../Modules/Services/alpaUserService';
@Component({
  selector: 'app-rm-zm-location-dropdown-alpa',
    templateUrl: './rm-zm-location-dropdown-alpa.component.html',
    styleUrls: ['./rm-zm-location-dropdown-alpa.component.css']
})
export class RmZmLocationDropdownAlpaComponent implements OnInit {

  @Output() SdChangeEmitter = new EventEmitter();
  @Output() RmChangeEmitter = new EventEmitter();
  @Output() ZmChangeEmitter = new EventEmitter();
  @Output() LocationChangeEmitter = new EventEmitter();
  @Output() AllLocationEmitter = new EventEmitter();
  @Output() singleLocationEmitter = new EventEmitter();
  @Output() ShowLoaderEmitter = new EventEmitter();
  @Input() SinglelocationSelected: number;
    @Input() selectedlocation: Array<number> = [];
  @Input() ShowLocationDropDown: boolean = false;
  @Input() ShowLocationMultiMaxFiveSelect: boolean = false;

  agencyName: string = "";
  showloader: boolean = false;

  regionalManager: Array<RegionalManager>;
  regionalManagerSelectItem: SelectItem[] = [];
  selectedRegionalManager: Array<number> = [];
  selectedRegionalManagertemp: Array<number> = [];

  saleDirector: Array<SaleDirector>;
  saleDirectorSelectItem: SelectItem[] = [];
  selectedSaleDirector: Array<number> = [];
  selectedSaleDirectortemp: Array<number> = [];


  zonalManager: Array<ZonalManager>;
  zonalManagerSelectItem: SelectItem[] = [];
  selectedZonalManager: Array<number> = [];
  selectedZonalManagertemp: Array<number> = [];

  selectedBSM: Array<number> = [];
  location: Array<Location>;
  locationSelectItem: SelectItem[] = [];

    constructor(private userService: UserService, private agencyService: AgencyService, private authenticationService: authenticationService, private alpaUserService: AlpaUserService) {
    this.regionalManager = new Array<RegionalManager>();
    this.zonalManager = new Array<ZonalManager>();
    this.saleDirector = new Array<SaleDirector>();
    this.location = new Array<Location>();
    this.selectedRegionalManager = new Array<number>();
    this.selectedSaleDirector = new Array<number>();
    this.selectedZonalManager = new Array<number>();
    this.selectedRegionalManagertemp = new Array<number>();
    this.selectedSaleDirectortemp = new Array<number>();
    this.selectedZonalManagertemp = new Array<number>();
        console.log("authenticationService.userRole", authenticationService.userRole)
        if (authenticationService.userRole === "headofdepratment-alpa") {
      this.getAllSaleDirectors();
      this.getAllRegionalManagers();
      this.getAllZonalManagers();
      this.getAllLocations();
    }

        if (authenticationService.userRole === "directorofsales-alpa") {
      this.selectedSaleDirector = new Array<number>();
      this.selectedSaleDirector.push(this.authenticationService.userId);
      this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
    }

        if (authenticationService.userRole === "regionalsalesmanager-alpa") {
      this.selectedRegionalManager = new Array<number>();
      this.selectedRegionalManager.push(this.authenticationService.userId);
      this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
    }

        if (authenticationService.userRole === "districtsalesmanager-alpa") {
      this.selectedZonalManager = new Array<number>();
      this.selectedZonalManager.push(this.authenticationService.userId);
      this.getAllLocationsRegionalZonalManager(null, this.selectedZonalManager);
    }
    if (authenticationService.userRole === "bsm") {
      this.selectedBSM = new Array<number>();
      this.selectedBSM.push(this.authenticationService.userId);
      this.getAgencyForBSM(this.selectedBSM);
    }

        if (authenticationService.userRole === 'storemanager-alpa') {
      let storemanagerId: number = this.authenticationService.userId;
      this.getStoreManagerAgency(storemanagerId);
    }
    //this.getAllLocations();
  }

  ngOnInit() {
  }

  onLocationchange(e) {
      this.LocationChangeEmitter.emit(this.selectedlocation);
  }

  onZonalManagerchange(e) {
    this.location = new Array<Location>();
      this.selectedlocation = new Array<number>();
    this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedZonalManager);
    this.ZmChangeEmitter.emit(this.selectedZonalManager);
  }

  onSaleDirectorchange(e) {
    this.selectedZonalManager = new Array<number>();
    this.selectedRegionalManager = new Array<number>();
    this.location = new Array<Location>();
    if (this.selectedSaleDirector.length > 0) {
      this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
    } else if (this.selectedSaleDirectortemp.length > 0) {
      this.getRegionalManagersBySaleDirector(this.selectedSaleDirectortemp);
    }
    else {
      this.getAllRegionalManagers();
      this.getAllZonalManagers();
    }
    this.SdChangeEmitter.emit(this.selectedSaleDirector);
  }

  onRegionalManagerchange(e) {
    this.selectedZonalManager = new Array<number>();
    this.location = new Array<Location>();
    if (this.selectedRegionalManager.length > 0) {
      this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
    }
    else if (this.selectedRegionalManagertemp.length > 0) {
      this.getZonalManagerByRegionalManagers(this.selectedRegionalManagertemp);
    }
    else {
      this.getAllZonalManagers();
    }
    this.RmChangeEmitter.emit(this.selectedRegionalManager);
  }

  onLocationDropDownchange() {
    this.singleLocationEmitter.emit(this.SinglelocationSelected);
  }

    getAllSaleDirectors() {
        this.saleDirectorSelectItem = [];
        this.alpaUserService.getAllSaleDirectors().subscribe(m => {
            this.saleDirector = m;
            this.saleDirector.forEach(i => {
                this.saleDirectorSelectItem.push(
                    {
                        label: i.saleDirector, value: i.saleDirectorID
                    })
                this.selectedSaleDirectortemp.push(i.saleDirectorID);
            })
        });
    }

    getAllRegionalManagers() {
        this.regionalManagerSelectItem = [];
        this.alpaUserService.getAllRegionalManagers().subscribe(m => {
            this.regionalManager = m;
            this.regionalManager.forEach(i => {
                this.regionalManagerSelectItem.push(
                    {
                        label: i.regionalManager, value: i.regionalManagerId
                    })
                this.selectedRegionalManagertemp.push(i.regionalManagerId);
            })
        });
    }

  getAllZonalManagers() {
    this.ShowLoaderEmitter.emit(true);
    this.zonalManagerSelectItem = [];
      this.alpaUserService.getAllZonalManagers().subscribe(m => {
      this.zonalManager = m;
      this.zonalManager.forEach(i => {
        this.zonalManagerSelectItem.push(
          {
            label: i.zonalManager, value: i.zonalManagerId
          })
        this.selectedZonalManagertemp.push(i.zonalManagerId);
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocations() {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
      this.alpaUserService.getAllLocations().subscribe(m => {
      this.location = m;
      this.AllLocationEmitter.emit(m);
      this.location.forEach(i => {
        this.locationSelectItem.push(
          {
            label: i.agencyName, value: i.agencyId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocationsRegionalManager(ids: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
    this.alpaUserService.getAllLocationsRegionalManager(ids).subscribe(m => {
      this.location = m;
      this.AllLocationEmitter.emit(m);
      this.location.forEach(i => {
        this.locationSelectItem.push(
          {
            label: i.agencyName, value: i.agencyId
          })
      })
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getAllLocationsRegionalZonalManager(Rids: Array<number>, Zids: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.locationSelectItem = [];
      this.selectedlocation = [];
    if ((Zids && Zids.length > 0) && (Rids && Rids.length > 0)) {
      this.alpaUserService.getAllLocationsRegionalZonalManager(Rids, Zids).subscribe(m => {
        this.location = m;
        this.AllLocationEmitter.emit(m);
        this.location.forEach(i => {
          this.locationSelectItem.push(
            {
              label: i.agencyName, value: i.agencyId
            })
        })
        this.ShowLoaderEmitter.emit(false);
      });
    }
    else if ((Rids && Rids.length > 0)) {
      this.getAllLocationsRegionalManager(Rids)
    }
    else if ((Zids && Zids.length > 0)) {
        this.alpaUserService.getAllLocationsRegionalZonalManager(Rids, Zids).subscribe(m => {
        this.location = m;
        this.AllLocationEmitter.emit(m);
        this.location.forEach(i => {
          this.locationSelectItem.push(
            {
              label: i.agencyName, value: i.agencyId
            })
        })
        this.ShowLoaderEmitter.emit(false);
      });
    }
    else {
      this.getAllLocations();
    }
  }


  getZonalManagerByRegionalManagers(selectedManager: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.zonalManagerSelectItem = [];
    this.selectedZonalManagertemp = [];
    this.alpaUserService.getZonalManagersByRegionalManager(selectedManager).subscribe(m => {
      this.zonalManager = m;
      this.zonalManager.forEach(i => {
        this.zonalManagerSelectItem.push(
          {
            label: i.zonalManager, value: i.zonalManagerId
          })
        this.selectedZonalManagertemp.push(i.zonalManagerId)
      })
      if (this.selectedZonalManager.length > 0) {
        this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedZonalManager);
      }
      else {
        this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedZonalManagertemp);
      }
      this.ShowLoaderEmitter.emit(false);
    });
  }

  getRegionalManagersBySaleDirector(selectedManager: Array<number>) {
    this.ShowLoaderEmitter.emit(true);
    this.regionalManagerSelectItem = [];
    this.selectedRegionalManagertemp = [];
      this.alpaUserService.getRegionalManagerBySaleDirector(selectedManager).subscribe(m => {
      this.regionalManager = m;
      this.regionalManager.forEach(i => {
        this.regionalManagerSelectItem.push(
          {
            label: i.regionalManager, value: i.regionalManagerId
          })
        this.selectedRegionalManagertemp.push(i.regionalManagerId)
      })
      if (this.selectedRegionalManager.length > 0) {
        this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
      }
      else {
        this.getZonalManagerByRegionalManagers(this.selectedRegionalManagertemp);
      }
      this.ShowLoaderEmitter.emit(false);
    });
  }


    getAgencyForBSM(BsmId: Array<number>) {
        //this.locationSelectItem = [];
        //this.agencyService.getByBsmId(BsmId).subscribe(m => {
        //    this.location = m;
        //    this.AllLocationEmitter.emit(m);
        //    this.location.forEach(i => {
        //        this.locationSelectItem.push(
        //            {
        //                label: i.agencyName, value: i.agencyId
        //            })
        //    })
        //    this.ShowLoaderEmitter.emit(false);
        //});
    }

    getStoreManagerAgency(Id: number) {
        this.locationSelectItem = [];
        this.alpaUserService.getStoreManagerAgency(Id).subscribe(m => {
            this.location = m;
            this.AllLocationEmitter.emit(m);
            this.location.forEach(i => {
                this.locationSelectItem.push(
                    {
                        label: i.agencyName, value: i.agencyId
                    })
            })
            this.agencyName = this.location[0].agencyName
            this.ShowLoaderEmitter.emit(false);
        });
    }

  filterReset() {
    this.selectedRegionalManager = new Array<number>();
    this.selectedZonalManager = new Array<number>();
    this.selectedSaleDirector = new Array<number>();
      this.selectedlocation = [];
      this.saleDirectorSelectItem = [];
    this.regionalManagerSelectItem = [];

    this.regionalManager = new Array<RegionalManager>();
    this.zonalManager = new Array<ZonalManager>();
    this.location = new Array<Location>();
    this.selectedRegionalManager = new Array<number>();
      if (this.authenticationService.userRole === "headofdepratment-alpa") {
      this.getAllSaleDirectors();
      this.getAllRegionalManagers();
      this.getAllZonalManagers();
      this.getAllLocations();
    }

      if (this.authenticationService.userRole === "directorofsales-alpa") {
      this.selectedSaleDirector = new Array<number>();
      this.selectedSaleDirector.push(this.authenticationService.userId);
      this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
    }

      if (this.authenticationService.userRole === "regionalsalesmanager-alpa") {
      this.selectedRegionalManager = new Array<number>();
      this.selectedRegionalManager.push(this.authenticationService.userId);
      this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
    }

      if (this.authenticationService.userRole === "districtsalesmanager-alpa") {
      this.selectedZonalManager = new Array<number>();
      this.selectedZonalManager.push(this.authenticationService.userId);
      this.getAllLocationsRegionalZonalManager(null, this.selectedZonalManager);
    }
    if (this.authenticationService.userRole === "bsm") {
      this.selectedBSM = new Array<number>();
      this.selectedBSM.push(this.authenticationService.userId);
      this.getAgencyForBSM(this.selectedBSM);
    }

      if (this.authenticationService.userRole === 'storemanager-alpa') {
      let storemanagerId: number = this.authenticationService.userId;
      this.getStoreManagerAgency(storemanagerId);
    }
  }

}

