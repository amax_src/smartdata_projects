import { MaxBIReportService } from './../../Services/MaxBIReportService';
import * as MaxBIReport from '../../../model/MaxBIReport';
import { CalendarModel } from '../../../model/CalanderModel';
import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { Location } from '../../../model/Location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from './../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { authenticationService } from '../../../login/authenticationService.service';

import { LocationFilterComponent } from '../../../shared/location-filter/location-filter.component';
import { CommonHelper, Enumerations } from '../../../utilities/common-helper';
import { MatTabGroup } from '@angular/material';
import { encryption } from '../../Services/Encryption';

@Component({
  selector: 'app-new-modified-quotes-closing-ratios',
  templateUrl: './new-modified-quotes-closing-ratios.component.html',
  styleUrls: [
    './new-modified-quotes-closing-ratios.component.css',
    '../maxbi-agent.scss'
  ]
})

export class NewModifiedQuotesClosingRatiosComponent implements OnInit {

  location: Array<Location> = new Array<Location>();
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  allLocations: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedAgents: Array<string> = new Array<string>();

  Records: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  regionRecords: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  zoneRecords: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  officeRecords: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  companyRecords: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();

  agentRecordsMain: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  agentRecordsDetail: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
  agentRecords: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();

  selectedTab: number = 4;
  quoteType: string = "new";
  catalogInfo = {
    From: MaxBIReport.E_MaxBICatalogFrom.Quotes,
    Type: MaxBIReport.E_MaxBICatalogType.Agent
  };

  pageHeader: string;
  pageSize: number = 10;
  pager: any = {};
  pagedItems: any[];

  calendarModel: CalendarModel = new CalendarModel();
  chartMainView: boolean = true;

  constructor(private router: Router, private renderer: Renderer2,
    public cmnSrv: CommonService, private Service: MaxBIReportService,
    private route: ActivatedRoute, private cipher: encryption,
    private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
    this.resetPager();
  }
  @ViewChild(LocationFilterComponent) child;
  @ViewChild('tabs') tabGroup: MatTabGroup;

  public routerreuse: any;
  ngOnInit() {
    this.cmnSrv.pageHeader = `${this.pageHeader} Reports`;
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
        //window.location.reload();
      }
    });
    this.route.queryParams.subscribe((data) => {
      this.quoteType = data['qt'];
      this.quoteType = this.cipher.encrytptvalue(this.quoteType, false);
      console.log('this.quoteType: ' + this.quoteType);
      if (this.quoteType == "new") {
        this.pageHeader = "Created Closing Ratios"
      }
      else if (this.quoteType == "modified") {
        this.pageHeader = "Modified Closing Ratios"
      }
      this.cmnSrv.pageHeader = `${this.pageHeader}`;
    });

    this.routerreuse = this.router.routeReuseStrategy.shouldReuseRoute;
    this.router.routeReuseStrategy.shouldReuseRoute = (future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot) => {
      return (curr != this.route.snapshot)
    };
  }
  ngOnDestroy(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = this.routerreuse;
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    if (e.firstTimeLoad) {
      this.selectedlocation.push(e.locations[0]);
      this.SearchReport();
    }
    else {
      this.selectedlocation = e.locations;
    }
  }

  RmChanged(e) { }
  ZmChanged(e) { }

  GetAllLocations(e) {
    this.allLocations = e.map(x => x.agencyId);
  }
  onAgentChanged(e) {
    this.selectedAgents = e;
  }

  OnDateRangeSelected(e) {
    this.calendarModel = e;
  }

  OnMultiDatesSelected(e) {
    this.calendarModel = e;
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.cmnSrv.showLoader = e;
    }
  }

  SearchReport() {
    var that = this;
    this.chartMainView = true;
    let reportBy = 3;
    if (this.selectedTab == 3) {
      reportBy = 4;
    }
    this.GetData(reportBy, function () {
      that.filterData();
      that.buildChartMainData();
    });
  }

  filterReset() {
    this.child.filterReset();
  }

  GetData(reportBy: number = 3, callback: Function = null) {
    this.emptyListing();
    this.summaryLoad = false;
    let startTime: number = new Date().getTime();
    this.Records = new Array<MaxBIReport.NewModifiedQuotes>();
    if (this.selectedlocation.length == 0) {
      this.selectedlocation = this.allLocations;
    }
    /*if (this.selectedlocation.length > 100) {
      this.notifier.showNotification("Please narrow down locations filter to avoid report timeout.", "OK", "error");
    }
    else {*/
    if (this.calendarModel.FromDate
      && this.calendarModel.ToDate) {
      this.cmnSrv.showLoader = true;

      let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest();
      request.AgentIds = this.selectedAgents;
      request.ReportBy = reportBy;
      request.QuoteType = this.quoteType;
      request.AgencyIds = this.selectedlocation;
      request.FromDate = this.calendarModel.FromDate;
      request.ToDate = this.calendarModel.ToDate;
      request.Dates = this.calendarModel.Dates;
      this.Service.GetNewModifiedQuotesClosingRatios(request).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = `MaxBIReports: ${this.pageHeader} Report`
        model.time = ResponseTime;
        this.Records = m;
        this.ischartDataLoaded = true;
        this.ApiLoad.SaveApiTime(model).subscribe();
        if (callback) {
          callback();
        }
        this.cmnSrv.showLoader = false;
        this.GetSummary();
      });
    }
    else {
      this.ischartDataLoaded = true;
    }
    //}
  }

  summaryLoad: boolean = false;
  summaryRequest: MaxBIReport.MaxBIRequest;
  summaryHierarchy: Array<MaxBIReport.HierarchyId>;
  GetSummary() {
    if (this.Records.length > 0) {
      this.summaryRequest = new MaxBIReport.MaxBIRequest();
      this.summaryRequest.CatalogFrom = MaxBIReport.E_MaxBICatalogFrom.Quotes;
      this.summaryRequest.QuoteType = this.quoteType;
      this.summaryRequest.IsClosingRatio = true;
      this.summaryRequest.FromDate = this.calendarModel.FromDate;
      this.summaryRequest.ToDate = this.calendarModel.ToDate;
      this.summaryRequest.Dates = this.calendarModel.Dates;

      //this.summaryHierarchy = CommonHelper.uniqueList(this.Records, "rmId,zmId,agencyId,carrierId");
      this.summaryHierarchy = this.allLocations.map((l) => { return { rmId: 0, zmId: 0, agencyId: l, carrierId: '' } });
      this.summaryLoad = true;
    }
  }

  tabChange(e) {
    var that = this;
    let _fn = function () {
      that.selectedTab = e;
      that.filterData();
      that.buildChartMainData();
    };

    if (e != this.selectedTab) {
      if (e == 3) {
        this.GetData(4, function () {
          _fn();
        });
      }
      else if (this.selectedTab == 3) {
        this.GetData(3, function () {
          _fn();
        });
      }
      else {
        _fn();
      }
    }
    else if (e == 0 && this.selectedTab == 0) {
      _fn();
    }
  }

  onPageChange(e) {
    this.pager.first = e.first;
    this.pager.page = e.page;
    this.pager.pageCount = e.pageCount;
    this.pager.rows = e.rows;
    this.setPaging();
  }

  filterDataMain() {
    let lst: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();

    let _fn = function (x: MaxBIReport.NewModifiedQuotes, rec: MaxBIReport.NewModifiedQuotes[]) {
      x.quotes = Math.round(rec.map(o => o.quotes).reduce((prev, curr) => prev + curr));
      x.sales = Math.round(rec.map(o => o.sales).reduce((prev, curr) => prev + curr));
      x.closingRatio = parseInt((x.quotes == 0 ? 0 : (Math.round((x.sales / x.quotes) * 100) / 100) * 100).toFixed());
    };

    this.agentRecordsMain = [];
    let temp = [];
    let dates = [];
    for (let d = 0; d < this.calendarModel.Dates.length; d++) {
      dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
    }
    if (dates.length > 0) {
      temp = this.Records.filter(x => dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
    }
    else {
      temp = this.Records;
    }

    if (this.calendarModel.Dates.length > 0) {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,date");
    }
    else {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year");
    }
    lst.forEach((x, i) => {
      let rec = temp.filter(y => {
        let ret = false;
        if (this.calendarModel.Dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }
        return ret;
      });
      _fn(x, rec);
      x.agents = temp.filter(y => {
        let ret = false;
        if (this.calendarModel.Dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }
        return ret;
      });
      x.agents.forEach(r => {
        r.parentId = i;
        r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
      });
      x.recordId = i;
      this.agentRecordsMain.push(x);
    });
  }
  filterDataDetail(agentName) {
    let lst: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();
    let agentId = '-1';
    if (agentName.length > 0) {
      agentId = this.Records.filter(x => x.agentName == agentName)[0].agentId;
    }

    let _fn = function (x: MaxBIReport.NewModifiedQuotes, rec: MaxBIReport.NewModifiedQuotes[]) {
      x.quotes = Math.round(rec.map(o => o.quotes).reduce((prev, curr) => prev + curr));
      x.sales = Math.round(rec.map(o => o.sales).reduce((prev, curr) => prev + curr));
      x.closingRatio = parseInt((x.quotes == 0 ? 0 : (Math.round((x.sales / x.quotes) * 100) / 100) * 100).toFixed());
    };

    this.agentRecordsDetail = [];
    let temp = [];
    let dates = [];
    for (let d = 0; d < this.calendarModel.Dates.length; d++) {
      dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
    }
    if (dates.length > 0) {
      temp = this.Records.filter(x => x.agentId == (agentId == '-1' ? x.agentId : agentId) && dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
    }
    else {
      temp = this.Records.filter(x => x.agentId == (agentId == '-1' ? x.agentId : agentId));
    }

    if (this.calendarModel.Dates.length > 0) {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,date");
    }
    else {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year");
    }
    lst.forEach((x, i) => {
      let rec = temp.filter(y => {
        let ret = false;
        if (this.calendarModel.Dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }
        return ret;
      });
      _fn(x, rec);
      x.agents = temp.filter(y => {
        let ret = false;
        if (this.calendarModel.Dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }
        return ret;
      });
      x.agents.forEach(r => {
        r.parentId = i;
        r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
      });
      x.recordId = i;
      this.agentRecordsDetail.push(x);
    });
  }

  filterData() {
    if (this.authenticationService.userRole === 'agent') {
      this.filterDataDetail('');
    }
    else {
      this.filterDataMain();
    }
    let lst: Array<MaxBIReport.NewModifiedQuotes> = new Array<MaxBIReport.NewModifiedQuotes>();

    let _fn = function (x: MaxBIReport.NewModifiedQuotes, rec: MaxBIReport.NewModifiedQuotes[]) {
      x.quotes = Math.round(rec.map(o => o.quotes).reduce((prev, curr) => prev + curr));
      x.sales = Math.round(rec.map(o => o.sales).reduce((prev, curr) => prev + curr));
      x.closingRatio = parseInt((x.quotes == 0 ? 0 : (Math.round((x.sales / x.quotes) * 100) / 100) * 100).toFixed());
    };

    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.regionRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "rmId,rmName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.rmId == x.rmId
              && y.monthName == x.monthName
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.rmId == x.rmId
              && a.monthName == x.monthName
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
          });
          x.recordId = i;
          this.regionRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.zoneRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "zmId,zmName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.zmId == x.zmId
              && y.monthName == x.monthName
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.zmId == x.zmId
              && a.monthName == x.monthName
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
          });
          x.recordId = i;
          this.zoneRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.officeRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "agencyId,agencyName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.agencyId == x.agencyId
              && y.monthName == x.monthName
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.agencyId == x.agencyId
              && a.monthName == x.monthName
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
          });
          x.recordId = i;
          this.officeRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.companyRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "carrierId,carrierName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.carrierId == x.carrierId
              && y.monthName == x.monthName
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.carrierId == x.carrierId
              && a.monthName == x.monthName
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
          });
          x.recordId = i;
          this.companyRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.AGENT:
        this.agentRecords = [];
        let temp = [];
        let dates = [];
        for (let d = 0; d < this.calendarModel.Dates.length; d++) {
          dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
        }
        if (dates.length > 0) {
          temp = this.Records.filter(x => dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
        }
        else {
          temp = this.Records;
        }

        if (this.calendarModel.Dates.length > 0) {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,date");
        }
        else {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year");
        }
        lst.forEach((x, i) => {
          let rec = temp.filter(y => {
            let ret = false;
            if (this.calendarModel.Dates.length > 0) {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.date == x.date);
            }
            else {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName);
            }
            return ret;
          });
          _fn(x, rec);
          x.agents = temp.filter(y => {
            let ret = false;
            if (this.calendarModel.Dates.length > 0) {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.date == x.date);
            }
            else {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName);
            }
            return ret;
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.closingRatio = parseInt((r.quotes == 0 ? 0 : (Math.round((r.sales / r.quotes) * 100) / 100) * 100).toFixed());
          });
          x.recordId = i;
          this.agentRecords.push(x);
        });
        break;
    }

    this.setPaging();
  }
  setPaging() {
    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.pager.totalRecords = this.regionRecords.length;
        this.pagedItems = this.regionRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.pager.totalRecords = this.zoneRecords.length;
        this.pagedItems = this.zoneRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.pager.totalRecords = this.officeRecords.length;
        this.pagedItems = this.officeRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.pager.totalRecords = this.companyRecords.length;
        this.pagedItems = this.companyRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.AGENT:
        this.pager.totalRecords = this.agentRecords.length;
        this.pagedItems = this.agentRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
    }
  }

  resetPager() {
    this.pager = {
      first: 0,
      rows: 10,
      page: 0,
      pageCount: 0,
      totalRecords: 0
    };
  }

  emptyListing() {
    this.resetPager();
    this.pagedItems = [];
    this.regionRecords = [];
    this.zoneRecords = [];
    this.officeRecords = [];
    this.companyRecords = [];
    this.agentRecords = [];
    this.agentRecordsMain = [];
    this.agentRecordsDetail = [];
    this.chartData.labels = [];
    this.chartData.datasets = [];
  }

  //#region Chart
  resetChart() {
    this.chartMainView = true;
  }

  charDataSetTemplate = {
    label: 'DataSet Title',
    backgroundColor: 'transparent',
    //strokeColor: '#fff',
    borderColor: '#F2BB21',
    //fill: false,
    tension: .4,
    //pointStyle: 'circle',
    //pointRadius: 6,
    //pointHoverRadius: 8,
    data: []
  };
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    animation: {
      duration: 0
    },
    responsive: true,
    title: {
      display: true,
      text: 'Agent Name',
      color: 'navy',
      position: 'top',
      align: 'center',
      font: {
        weight: 'bold',
        size: '18px'
      },
      padding: 8,
      fullSize: true,
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
            style: 'percent'
          }).format(value / 100);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      },
      beforeInit: function(chart, options) {
        try {
          chart.legend.afterFit = function() {
            this.height = this.height + 50;
          };
        } catch { }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += Intl.NumberFormat('en-US', {
            style: 'percent'
          }).format(tooltipItem.yLabel / 100);
          return label;
        }
      }
    },
    layout: {
      padding: {
        top: 15
      }
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: '#A3A0FB'
        },
        scaleLabel: {
          display: true,
          fontColor: "#A3A0FB"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true
        }
      }]
    }
  };
  lstColor: Array<string> = [
    '#4bc0c0',
    '#42A5F5',
    '#9CCC65',
    '#FF8373',
    '#A3A0FB',
    '#FFDA83',
    '#55D8FE',
    '#44B980',
    '#BA59D6',
    '#657EB8'
  ];

  chartDataMain = {
    labels: [],
    datasets: []
  };
  chartOptionsMain = {
    animation: {
      duration: 0
    },
    responsive: true,
    title: {
      display: false,
      text: 'Agent View',
      color: 'navy',
      position: 'top',
      align: 'center',
      font: {
        weight: 'bold',
        size: '18px'
      },
      padding: 8,
      fullSize: true,
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
            style: 'percent'
          }).format(value / 100);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      },
      beforeInit: function(chart, options) {
        try {
          chart.legend.afterFit = function() {
            this.height = this.height + 50;
          };
        } catch { }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += Intl.NumberFormat('en-US', {
            style: 'percent'
          }).format(tooltipItem.yLabel / 100);
          return label;
        }
      }
    },
    layout: {
      padding: {
        top: 15
      }
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: '#A3A0FB'
        },
        scaleLabel: {
          display: true,
          fontColor: "#A3A0FB"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true
        }
      }]
    },
    onHover: (event, item) => {
      /*event.target.style.cursor = 'default';
      if (item[0]) {
        event.target.style.cursor = 'pointer';
      }*/
    },
    onClick: (event, item) => {
      /*if (item.length > 0) {
        const chart = item[0]._chart;
        const line = chart.getElementsAtEventForMode(
          event,
          'nearest',
          { intersect: true },
          true);

        if (line.length > 0) {
          debugger
          const agentName = chart.config.data.datasets[line[0]._datasetIndex].label;
          this.setDetailedChartTitle(agentName, chart.config.data.datasets[line[0]._datasetIndex].borderColor);
          this.filterDataDetail(agentName);
          this.buildChartData();
          this.chartMainView = false;
        }
      }*/
    }
  };

  setDetailedChartTitle(title, color) {
    this.chartOptions.title.text = title;
    this.chartOptions.title.color = color;
  }

  buildChartMainData() {
    if (this.authenticationService.userRole === 'agent') {
      this.chartOptions.title.display = false;
      this.buildChartData();
      return;
    }
    this.chartDataMain.labels = [];
    this.chartDataMain.datasets = [];
    let clr = this.lstColor;

    let _fn = (records: Array<MaxBIReport.NewModifiedQuotes>, datasetName: string) => {

      let lables = [];
      if (this.calendarModel.Dates.length > 0) {
        lables = CommonHelper.getMonthDates(this.calendarModel.Month, this.calendarModel.Year);
      }
      else {
        lables = CommonHelper.getYearMonths(this.calendarModel.Month, this.calendarModel.Year);
      }
      let lst = [];
      lables.forEach(l => {
        if (this.calendarModel.Dates.length > 0) {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: CommonHelper.getFormattedDate(l.monthName, "MMM DD"),
            labelDate: l.monthName
          });
        }
        else {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: l.monthName,
            labelDate: l.monthName
          });
        }
      });
      lst.sort(CommonHelper.sortOrderComparer("sortLabel"));

      let datasets = CommonHelper
        .uniqueList(records, datasetName)
        .map((m, i) => {
          let idx = i % clr.length;
          return {
            title: m[datasetName],
            color: clr[idx]
          };
        });

      lst.forEach(l => {
        this.chartDataMain.labels.push(l.label);

        datasets.forEach(ds => {
          let q = 0,
            s = 0;

          if (this.calendarModel.Dates.length > 0) {
            records
              .filter(r => r[datasetName] == ds.title
                && CommonHelper.getFormattedDate(r.date, "YYYY-MM-DD") == l.labelDate)
              .forEach(r => {
                q += r.quotes;
                s += r.sales;
              });
          }
          else {
            records
              .filter(r => r[datasetName] == ds.title && r.monthName == l.label)
              .forEach(r => {
                q += r.quotes;
                s += r.sales;
              });
          }
          let d = parseInt((q == 0 ? 0 : (Math.round((s / q) * 100) / 100) * 100).toFixed());

          let dSet = null;
          let exists = this.chartDataMain.datasets.filter(cds => cds.label === ds.title);
          if (exists.length > 0) {
            exists[0].data.push(d);
          }
          else {
            dSet = JSON.parse(JSON.stringify(this.charDataSetTemplate));
            dSet.label = ds.title;
            dSet.borderColor = ds.color;
            dSet.data = [];
            dSet.data.push(d);
            this.chartDataMain.datasets.push(dSet);
          }
        });
      });
    };

    _fn(this.agentRecordsMain, "agentName");
    console.log(this.chartDataMain);
  }

  buildChartData() {
    this.chartData.labels = [];
    this.chartData.datasets = [];
    let clr = this.lstColor;

    let _fn = (records: Array<MaxBIReport.NewModifiedQuotes>, datasetName: string) => {

      let lables = [];
      if (this.calendarModel.Dates.length > 0) {
        lables = CommonHelper.getMonthDates(this.calendarModel.Month, this.calendarModel.Year);
      }
      else {
        lables = CommonHelper.getYearMonths(this.calendarModel.Month, this.calendarModel.Year);
      }
      let lst = [];
      lables.forEach(l => {
        if (this.calendarModel.Dates.length > 0) {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: CommonHelper.getFormattedDate(l.monthName, "MMM DD"),
            labelDate: l.monthName
          });
        }
        else {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: l.monthName,
            labelDate: l.monthName
          });
        }
      });
      lst.sort(CommonHelper.sortOrderComparer("sortLabel"));

      let datasets = CommonHelper
        .uniqueList(records, datasetName)
        .map((m, i) => {
          let idx = i % clr.length;
          return {
            title: m[datasetName],
            color: clr[idx]
          };
        });

      lst.forEach(l => {
        this.chartData.labels.push(l.label);

        datasets.forEach(ds => {
          let q = 0,
            s = 0;

          if (this.calendarModel.Dates.length > 0) {
            records
              .filter(r => r[datasetName] == ds.title
                && CommonHelper.getFormattedDate(r.date, "YYYY-MM-DD") == l.labelDate)
              .forEach(r => {
                q += r.quotes;
                s += r.sales;
              });
          }
          else {
            records
              .filter(r => r[datasetName] == ds.title && r.monthName == l.label)
              .forEach(r => {
                q += r.quotes;
                s += r.sales;
              });
          }
          let d = parseInt((q == 0 ? 0 : (Math.round((s / q) * 100) / 100) * 100).toFixed());

          let dSet = null;
          let exists = this.chartData.datasets.filter(cds => cds.label === ds.title);
          if (exists.length > 0) {
            exists[0].data.push(d);
          }
          else {
            dSet = JSON.parse(JSON.stringify(this.charDataSetTemplate));
            dSet.label = ds.title;
            dSet.borderColor = ds.color;
            dSet.data = [];
            dSet.data.push(d);
            this.chartData.datasets.push(dSet);
          }
        });
      });
    };

    _fn(this.agentRecordsDetail, "agencyName");
    console.log(this.chartData);
  }
  //#endregion

}
