import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import * as moment from "moment";
import { CommonHelper } from '../../../utilities/common-helper';

@Component({
  selector: 'app-test-multiselect',
  templateUrl: './test-multiselect.component.html',
  styleUrls: ['./test-multiselect.component.css']
})
export class TestMultiselectComponent implements OnInit {

  multiDatesSelected: Date[] = [];
  multiDatesSelectItem: SelectItem[] = [];

  payTypeSelectItem: SelectItem[] = [];

  constructor() {
    // this.payTypeSelectItem = [
    //   { label: "Down Payment", value: "Down Payment" },
    //   { label: "Endorsement", value: "Endorsement" },
    //   { label: "Monthly Payment", value: "Monthly Payment" },
    //   { label: "Reinstatement", value: "Reinstatement" },
    //   { label: "Renewal", value: "Renewal" }
    // ];
  }

  ngOnInit() {
    this.setMonthMinMaxDates();
    this.payTypeSelectItem.push({ label: "Down Payment", value: "Down Payment" });
    this.payTypeSelectItem.push({ label: "Endorsement", value: "Endorsement" });
    this.payTypeSelectItem.push({ label: "Monthly Payment", value: "Monthly Payment" });
    this.payTypeSelectItem.push({ label: "Reinstatement", value: "Reinstatement" });
    this.payTypeSelectItem.push({ label: "Renewal", value: "Renewal" });
  }

  setMonthMinMaxDates() {
    let range = CommonHelper.getFirstLastDates(new Date());

    this.multiDatesSelectItem = [];
    while (range.FromDate <= range.ToDate) {
      this.multiDatesSelectItem.push({
        label: moment(range.FromDate).format("MMMM DD"),
        value: moment(range.FromDate).toDate()
      });
      range.FromDate.setDate(range.FromDate.getDate() + 1);
      console.log(this.multiDatesSelectItem[this.multiDatesSelectItem.length - 1].value);
    }
  }

  onMultiDatesSelected() {
    console.log(this.multiDatesSelected);
  }

  onPayTypeChange(e) {
    console.log(e.value);
  }

}
