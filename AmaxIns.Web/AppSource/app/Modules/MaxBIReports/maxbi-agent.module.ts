import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../../shared/shared.module';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { ChartModule } from 'primeng/chart';
import { CalendarModule } from 'primeng/calendar';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AgencyFeePolicyComponent } from './agency-fee-policy/agency-fee-policy.component';
import { MaxBIAgentRoutingModule } from './maxbi-agent-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarrierAgentComponent } from './carrier-agent/carrier-agent.component';
import { InboundOutboundCallsComponent } from './inbound-outbound-calls/inbound-outbound-calls.component';
import { NewModifiedQuotesClosingRatiosComponent } from './new-modified-quotes-closing-ratios/new-modified-quotes-closing-ratios.component';
import { NewModifiedQuotesComponent } from './new-modified-quotes/new-modified-quotes.component';
import { PremiumPolicyComponent } from './premium-policy/premium-policy.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HourlyProductionComponent } from './hourly-production/hourly-production.component';
import { PayrollComponent } from './payroll/payroll.component';
import { TestMultiselectComponent } from './test-multiselect/test-multiselect.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    PaginatorModule,
    SharedModule,
    FormsModule,
    TableModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    CalendarModule,
    MaxBIAgentRoutingModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [
    DashboardComponent,
    CarrierAgentComponent,
    AgencyFeePolicyComponent,
    PremiumPolicyComponent,
    NewModifiedQuotesComponent,
    NewModifiedQuotesClosingRatiosComponent,
    InboundOutboundCallsComponent,
    HourlyProductionComponent,
    PayrollComponent,
    TestMultiselectComponent
  ],
  exports: []
 })
export class MaxBIAgentModule { }
