import { MaxBIReportService } from './../../Services/MaxBIReportService';
import * as MaxBIReport from '../../../model/MaxBIReport';
import { CalendarModel } from '../../../model/CalanderModel';
import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild, AfterViewInit } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { Location } from '../../../model/Location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { PageService } from '../../Services/pageService';
import { DateService } from '../../Services/dateService';

import { DownloadexcelService } from '../../Services/downloadexcel.service';
import { LocationFilterComponent } from './../../../shared/location-filter/location-filter.component';
import { CommonHelper } from '../../../utilities/common-helper';
import { MatTabGroup } from '@angular/material';

@Component({
  selector: 'app-carrier-agent',
  templateUrl: './carrier-agent.component.html',
  styleUrls: [
    './carrier-agent.component.css',
    '../maxbi-agent.scss'
  ]
})

export class CarrierAgentComponent implements OnInit, AfterViewChecked {

  location: Array<Location> = new Array<Location>();
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  allLocations: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedPayTypes: Array<string> = new Array<string>();
  selectedAgents: Array<string> = new Array<string>();

  Records: Array<MaxBIReport.CarrierAgent> = new Array<MaxBIReport.CarrierAgent>();

  selectedTab: number = 0;
  catalogInfo = {
    From: MaxBIReport.E_MaxBICatalogFrom.PaymentInfo,
    Type: MaxBIReport.E_MaxBICatalogType.Agent
  };

  pager: any = {};
  pagedItems: any[];

  calendarModel: CalendarModel = new CalendarModel();

  constructor(private router: Router, private renderer: Renderer2,
    public cmnSrv: CommonService, private Service: MaxBIReportService,
    private ApiLoad: ApiLoadTimeService, private _pageservice: PageService) {

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  @ViewChild(LocationFilterComponent) child;
  @ViewChild('tabs') tabGroup: MatTabGroup;

  ngOnInit() {
    this.cmnSrv.pageHeader = "Carrier Agent";
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    if (e.firstTimeLoad) {
      this.selectedlocation.push(e.locations[0]);
      this.SearchReport();
    }
    else {
      this.selectedlocation = e.locations;
    }
  }

  RmChanged(e) { }
  ZmChanged(e) { }

  GetAllLocations(e) {
    this.allLocations = e.map(x => x.agencyId);
  }
  onPayTypeChanged(e) {
    this.selectedPayTypes = e;
  }
  onAgentChanged(e) {
    this.selectedAgents = e;
  }

  OnDateRangeSelected(e) {
    this.calendarModel = e;
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.cmnSrv.showLoader = e;
    }
  }

  SearchReport() {
    this.GetData();
  }

  filterReset() {
    this.child.filterReset();
  }

  GetData() {
    this.emptyListing();
    let startTime: number = new Date().getTime();
    if (this.selectedlocation.length == 0) {
      this.selectedlocation = this.allLocations;
    }
    if (this.calendarModel.FromDate
      && this.calendarModel.ToDate) {
      this.cmnSrv.showLoader = true;

      let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest();
      request.AgentIds = this.selectedAgents;
      request.PayTypes = this.selectedPayTypes;
      request.ReportType = MaxBIReport.E_MaxBIReport.CarrierAgent;
      request.AgencyIds = this.selectedlocation;
      request.FromDate = this.calendarModel.FromDate;
      request.ToDate = this.calendarModel.ToDate;
      this.Service.GetAgentCarrierAFeePremium(request).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "MaxBIReports: Carrier Agent Report"
        model.time = ResponseTime;
        this.Records = m;
        this.setPaging(1);
        this.ischartDataLoaded = true;
        this.ApiLoad.SaveApiTime(model).subscribe();
        this.tabGroup.selectedIndex = 0;
        this.cmnSrv.showLoader = false;
      });
    }
    else {
      this.ischartDataLoaded = true;
    }
  }

  tabChange(e) {
    this.selectedTab = e;
    switch (e) {
      case 1: // Afee Chart
        this.buildChartData("agencyFee");
        break;
      case 2: // Premium Chart
        this.buildChartData("premium");
        break;
      case 3: // Policies Chart
        this.buildChartData("policies");
        break;
    }
  }

  setPaging(page: number) {
    let _fn = () => {
      this.pager = this._pageservice.getPager(this.Records.length, page, 50);
      this.pagedItems = this.Records.slice(this.pager.startIndex, this.pager.endIndex + 1);
    };

    if (page < 1 || page > this.pager.totalPages) {
      if (this.pager.totalPages == 0 && this.Records.length > 0) {
        _fn();
      }
      else {
        return;
      }
    }
    _fn();
  }

  emptyListing() {
    this.Records = new Array<MaxBIReport.CarrierAgent>();
    this.setPaging(1);
    this.chartData.labels = [];
    if (this.chartData.datasets.length > 0) {
      this.chartData.datasets[0].data = [];
    }
  }

  //#region Chart
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    //indexAxis: 'y',
    layout: {
      padding: {
        top: 15
      }
    },
    plugins: {
      legend: {
        labels: {
          color: '#495057'
        }
      },
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          if (label == "policies: ") {
            return label + Intl.NumberFormat('en-US', {
            }).format(tooltipItem.yLabel);
          }
          else {
            label += Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              maximumFractionDigits: 2
            }).format(tooltipItem.yLabel);
            return label;
          }
        }
      }
    },
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 75,
          minRotation: 75,
          fontColor: '#A3A0FB'
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true,
          userCallback: function (value, index, values) {
            return new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
            }).format(value);
          }
        }
      }]
    }
  };
  chartOptionsPolicies = {
    indexAxis: 'y',
    layout: {
      padding: {
        top: 15
      }
    },
    plugins: {
      legend: {
        labels: {
          color: '#495057'
        }
      },
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
          }).format(value);
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          if (label == "policies: ") {
            return label + Intl.NumberFormat('en-US', {
            }).format(tooltipItem.yLabel);
          }
          else {
            label += Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD'
            }).format(tooltipItem.yLabel);
            return label;
          }
        }
      }
    },
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 75,
          minRotation: 75,
          fontColor: '#A3A0FB'
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true
        }
      }]
    }
  };

  buildChartData(dataProp) {
    this.chartData.labels = [];
    this.chartData.datasets = [{
      label: dataProp,
      backgroundColor: '#4bc0c0',
      data: []
    }];
    let customData = [];

    this.Records.forEach(x => {
      let label = x.carrier + ' - ' + x.monthName;
      let exist = customData.filter(c => c.label == label);
      if (exist.length > 0) {
        exist[0].data += parseInt(x[dataProp]);
      }
      else {
        customData.push({
          sortLabel: x.year.toString() + ('00' + x.month).slice(-2),
          label: label,
          data: parseInt(x[dataProp])
        });
      }
    });

    customData = customData.filter(r => r.data > 0);
    customData.sort(CommonHelper.sortOrderComparer("sortLabel"));
    customData.sort(CommonHelper.sortOrderComparer("label"));

    customData.forEach((r, i) => {
      this.chartData.labels.push(r.label);
      this.chartData.datasets[0].data.push(r.data);
    });

    console.log(this.chartData);

  }
  //#endregion

}
