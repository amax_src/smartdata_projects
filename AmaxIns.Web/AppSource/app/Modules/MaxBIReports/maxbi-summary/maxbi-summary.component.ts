import { MaxBISummary, MaxBISummaryIndicator, MaxBIRequest, HierarchyId, E_MaxBICatalogFrom } from './../../../model/MaxBIReport';
import { Component, Input, OnInit } from '@angular/core';
import { MaxBIReportService } from '../../Services/MaxBIReportService';
import { CommonHelper, Enumerations } from '../../../utilities/common-helper';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-maxbi-summary',
  templateUrl: './maxbi-summary.component.html',
  styleUrls: ['./maxbi-summary.component.css']
})
export class MaxbiSummaryComponent implements OnInit {
  indicator = new MaxBISummaryIndicator();
  record = new MaxBISummary();

  hasCompany: boolean = true;
  _request: MaxBIRequest;
  _hierarchy: Array<HierarchyId>;

  isClosingRatio: boolean = false;
  summaryFor: E_MaxBICatalogFrom;
  @Input("hierarchy") set hierarchy(_hierarchy: Array<HierarchyId>) {
    this._hierarchy = _hierarchy;
    if (this._request && this._hierarchy) {
      this.GetData();
    }
  }

  @Input("request") set request(_request: MaxBIRequest) {
    this._request = _request;
    this.GetData();
  }

  constructor(private Service: MaxBIReportService,
    private currencyPipe: CurrencyPipe) { }

  ngOnInit() {
  }

  GetData() {
    this.hasCompany = !(this._request.CatalogFrom == E_MaxBICatalogFrom.Calls);
    this.summaryFor = this._request.CatalogFrom;
    this.isClosingRatio = this._request.IsClosingRatio;

    let that = this;
    that.record.Region = '0';
    that.indicator.Region = true;
    //let lst = CommonHelper.uniqueList(this._hierarchy, "rmId");
    let lst = CommonHelper.uniqueList(this._hierarchy, "agencyId");
    this.GetSummary(Enumerations.E_ReportFor.REGION, lst, function (result: string) {
      that.record.Region = result;
      that.indicator.Region = false;
    });

    that.record.Zone = '0';
    that.indicator.Zone = true;
    //lst = CommonHelper.uniqueList(this._hierarchy, "zmId");
    lst = CommonHelper.uniqueList(this._hierarchy, "agencyId");
    this.GetSummary(Enumerations.E_ReportFor.ZONE, lst, function (result: string) {
      that.record.Zone = result;
      that.indicator.Zone = false;
    });

    that.record.Office = '0';
    that.indicator.Office = true;
    lst = CommonHelper.uniqueList(this._hierarchy, "agencyId");
    this.GetSummary(Enumerations.E_ReportFor.OFFICE, lst, function (result: string) {
      that.record.Office = result;
      that.indicator.Office = false;
    });

    if (this.hasCompany) {
      that.record.Company = '0';
      that.indicator.Company = true;
      //lst = CommonHelper.uniqueList(this._hierarchy, "carrierId");
      lst = CommonHelper.uniqueList(this._hierarchy, "agencyId");
      this.GetSummary(Enumerations.E_ReportFor.COMPANY, lst, function (result: string) {
        that.record.Company = result;
        that.indicator.Company = false;
      });
    }
  }

  GetSummary(reportBy: number, hierarchyIds: Array<string>, callback: Function = null) {
    let that = this;
    that._request.ReportBy = reportBy + 1;
    that._request.HierarchyIds = [];
    hierarchyIds.forEach((obj) => {
      that._request.HierarchyIds.push(obj[Object.keys(obj)[0]]);
    });

    switch (this.summaryFor) {
      case E_MaxBICatalogFrom.PaymentInfo:
        this.Service.GetAgencyPerPolicyReportSummary(that._request).subscribe(m => {
          if (callback) {
            callback(
              Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD'
              }).format(m)
            );
            //callback(that.currencyPipe.transform(m, 'USD'));
          }
        });
        break;
      case E_MaxBICatalogFrom.Calls:
        this.Service.GetInboundOutboundCallsSummary(that._request).subscribe(m => {
          if (callback) {
            //callback(m.toString());
            callback(
              Intl.NumberFormat('en-US').format(m)
            );
          }
        });
        break;
      case E_MaxBICatalogFrom.Quotes:
        if (this.isClosingRatio) {
          this.Service.GetNewModifiedQuotesClosingRatiosSummary(that._request).subscribe(m => {
            if (callback) {
              //callback(m.toString());
              callback(
                Intl.NumberFormat('en-US').format(m)
              );
            }
          });
        }
        else {
          this.Service.GetNewModifiedQuotesSummary(that._request).subscribe(m => {
            if (callback) {
              //callback(m.toString());
              callback(
                Intl.NumberFormat('en-US').format(m)
              );
            }
          });
        }
        break;
    }
  }

}
