import { encryption } from './../../Services/Encryption';
import { MaxBIReportService } from './../../Services/MaxBIReportService';
import * as MaxBIReport from './../../../model/MaxBIReport';
import { CalendarModel } from './../../../model/CalanderModel';
import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { Location } from '../../../model/Location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from './../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { PageService } from './../../Services/pageService';
import { DateService } from './../../Services/dateService';

import { DownloadexcelService } from './../../Services/downloadexcel.service';
import { LocationFilterComponent } from '../../../shared/location-filter/location-filter.component';
import { CommonHelper, Enumerations } from '../../../utilities/common-helper';
import { MatTabGroup } from '@angular/material';
import { noConflict } from 'underscore';

@Component({
  selector: 'app-inbound-outbound-calls',
  templateUrl: './inbound-outbound-calls.component.html',
  styleUrls: [
    './inbound-outbound-calls.component.css',
    '../maxbi-agent.scss'
  ]
})
export class InboundOutboundCallsComponent implements OnInit {

  location: Array<Location> = new Array<Location>();
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  allLocations: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedAgents: Array<string> = new Array<string>();
  allAgents: Array<string> = new Array<string>();

  Records: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();
  regionRecords: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();
  zoneRecords: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();
  officeRecords: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();
  companyRecords: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();

  selectedTab: number = 3;
  callType: string = "new";
  catalogInfo = {
    From: MaxBIReport.E_MaxBICatalogFrom.Calls,
    Type: MaxBIReport.E_MaxBICatalogType.Agent
  };

  pageHeader: string = "";
  pager: any = {};
  pagedItems: any[];

  calendarModel: CalendarModel = new CalendarModel();

  constructor(private router: Router, private renderer: Renderer2,
    public cmnSrv: CommonService, private Service: MaxBIReportService,
    private ApiLoad: ApiLoadTimeService, private _pageservice: PageService,
    private route: ActivatedRoute, private cipher: encryption,
    private _DateService: DateService, private _downloadExcel: DownloadexcelService) {

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
    this.resetPager();
  }
  @ViewChild(LocationFilterComponent) child;
  @ViewChild('tabs') tabGroup: MatTabGroup;

  public routerreuse: any;

  ngOnInit() {
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
        //window.location.reload();
      }
    });
    this.route.queryParams.subscribe((data) => {
      this.callType = data['ct'];
      this.callType = this.cipher.encrytptvalue(this.callType, false);

      if (this.callType == "IN") {
        this.pageHeader = "Inbound Calls"
      }
      else if (this.callType == "OUT") {
        this.pageHeader = "Outbound Calls"
      }
      this.cmnSrv.pageHeader = `${this.pageHeader}`;
    });

    this.routerreuse = this.router.routeReuseStrategy.shouldReuseRoute;
    this.router.routeReuseStrategy.shouldReuseRoute = (future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot) => {
      return (curr != this.route.snapshot)
    };
  }
  ngOnDestroy(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = this.routerreuse;
  }


  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    if (e.firstTimeLoad) {
      this.selectedlocation.push(e.locations[0]);
      this.SearchReport();
    }
    else {
      this.selectedlocation = e.locations;
    }
  }

  onAgentChanged(e) {
    this.selectedAgents = e;
  }

  RmChanged(e) { }
  ZmChanged(e) { }

  GetAllLocations(e) {
    this.allLocations = e.map(x => x.agencyId);
  }
  GetAllAgents(e) {
    this.allAgents = e;
  }

  OnDateRangeSelected(e) {
    this.calendarModel = e;
  }

  OnMultiDatesSelected(e) {
    this.calendarModel = e;
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.cmnSrv.showLoader = e;
    }
  }

  SearchReport() {
    this.GetData();
  }

  filterReset() {
    this.child.filterReset();
  }

  GetData() {
    this.emptyListing();
    this.summaryLoad = false;
    let startTime: number = new Date().getTime();
    this.Records = new Array<MaxBIReport.InboundOutboundCalls>();
    if (this.selectedlocation.length == 0) {
      this.selectedlocation = this.allLocations;
    }
    if (this.selectedAgents.length == 0) {
      this.selectedAgents = this.allAgents;
    }

    if (this.calendarModel.FromDate
      && this.calendarModel.ToDate) {
      this.cmnSrv.showLoader = true;

      let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest();
      request.AgentIds = this.selectedAgents;
      request.CallType = this.callType;
      request.AgencyIds = this.selectedlocation;
      request.FromDate = this.calendarModel.FromDate;
      request.ToDate = this.calendarModel.ToDate;
      request.Dates = this.calendarModel.Dates;
      this.Service.GetInboundOutboundCalls(request).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = `MaxBIReports: ${this.pageHeader} Report`
        model.time = ResponseTime;
        this.Records = m;
        this.ischartDataLoaded = true;
        this.ApiLoad.SaveApiTime(model).subscribe();
        this.tabChange(this.selectedTab);
        this.GetSummary();
        this.cmnSrv.showLoader = false;
      });
    }
    else {
      this.ischartDataLoaded = true;
    }
  }

  summaryLoad: boolean = false;
  summaryRequest: MaxBIReport.MaxBIRequest;
  summaryHierarchy: Array<MaxBIReport.HierarchyId>;
  GetSummary() {
    if (this.Records.length > 0) {
      this.summaryRequest = new MaxBIReport.MaxBIRequest();
      this.summaryRequest.CatalogFrom = MaxBIReport.E_MaxBICatalogFrom.Calls;
      this.summaryRequest.CallType = this.callType;
      this.summaryRequest.FromDate = this.calendarModel.FromDate;
      this.summaryRequest.ToDate = this.calendarModel.ToDate;
      this.summaryRequest.Dates = this.calendarModel.Dates;

      //this.summaryHierarchy = CommonHelper.uniqueList(this.Records, "rmId,zmId,agencyId");
      this.summaryHierarchy = this.allLocations.map((l) => { return { rmId: 0, zmId: 0, agencyId: l, carrierId: '' } });
      this.summaryLoad = true;
    }
  }

  tabChange(e) {
    this.selectedTab = e;
    this.emptyListing();
    this.filterData();
    this.buildChartData();
  }

  onPageChange(e) {
    this.pager.first = e.first;
    this.pager.page = e.page;
    this.pager.pageCount = e.pageCount;
    this.pager.rows = e.rows;
    this.setPaging();
  }

  filterData() {
    let lst: Array<MaxBIReport.InboundOutboundCalls> = new Array<MaxBIReport.InboundOutboundCalls>();

    let _fn = function (x: MaxBIReport.InboundOutboundCalls, rec: MaxBIReport.InboundOutboundCalls[]) {
      x.inBoundCalls = Math.round(rec.map(o => o.inBoundCalls).reduce((prev, curr) => prev + curr));
      x.outBoundCalls = Math.round(rec.map(o => o.outBoundCalls).reduce((prev, curr) => prev + curr));

      x.inBoundCallsAverage = x.noOfAgents == 0 ? '0' : (x.inBoundCalls / x.noOfAgents).toFixed(2);
      x.outBoundCallsAverage = x.noOfAgents == 0 ? '0' : (x.outBoundCalls / x.noOfAgents).toFixed(2);
    };

    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.regionRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "rmId,rmName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.rmId == x.rmId
              && y.monthName == x.monthName
          });
          x.noOfAgents = CommonHelper.uniqueList(rec, "agentId").length;
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            let ret = (a.rmId == x.rmId && a.monthName == x.monthName);
            if (ret) {
              a.parentId = i;
            }
            return ret;
          });
          x.recordId = i;
          this.regionRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.zoneRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "zmId,zmName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.zmId == x.zmId
              && y.monthName == x.monthName
          });
          x.noOfAgents = CommonHelper.uniqueList(rec, "agentId").length;
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            let ret = (a.zmId == x.zmId && a.monthName == x.monthName);
            if (ret) {
              a.parentId = i;
            }
            return ret;
          });
          x.recordId = i;
          this.zoneRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.officeRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "agencyId,agencyName,monthName,month,year");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.agencyId == x.agencyId
              && y.monthName == x.monthName
          });
          x.noOfAgents = CommonHelper.uniqueList(rec, "agentId").length;
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            let ret = (a.agencyId == x.agencyId && a.monthName == x.monthName);
            if (ret) {
              a.parentId = i;
            }
            return ret;
          });
          x.recordId = i;
          this.officeRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.companyRecords = [];
        let temp = [];
        let dates = [];
        for (let d = 0; d < this.calendarModel.Dates.length; d++) {
          dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
        }
        if (dates.length > 0) {
          temp = this.Records.filter(x => dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
        }
        else {
          temp = this.Records;
        }

        if (this.calendarModel.Dates.length > 0) {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,date");
        }
        else {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year");
        }
        lst.forEach((x, i) => {
          let rec = temp.filter(y => {
            let ret = false;
            if (this.calendarModel.Dates.length > 0) {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.date == x.date);
            }
            else {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName);
            }
            return ret;
          });
          x.noOfAgents = CommonHelper.uniqueList(rec, "agentId").length;
          _fn(x, rec);
          x.recordId = i;
          this.companyRecords.push(x);
        });
        break;
    }

    this.setPaging();
  }
  setPaging() {
    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.pager.totalRecords = this.regionRecords.length;
        this.pagedItems = this.regionRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.pager.totalRecords = this.zoneRecords.length;
        this.pagedItems = this.zoneRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.pager.totalRecords = this.officeRecords.length;
        this.pagedItems = this.officeRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.pager.totalRecords = this.companyRecords.length;
        this.pagedItems = this.companyRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
    }
  }

  resetPager() {
    this.pager = {
      first: 0,
      rows: 10,
      page: 0,
      pageCount: 0,
      totalRecords: 0
    };
  }

  emptyListing() {
    this.resetPager();
    this.pagedItems = [];
    this.regionRecords = [];
    this.zoneRecords = [];
    this.officeRecords = [];
    this.companyRecords = [];
    this.chartData.labels = [];
    this.chartData.datasets = [];
  }

  //#region Chart
  charDataSetTemplate = {
    label: 'DataSet Title',
    //backgroundColor: '#fff',
    //strokeColor: '#fff',
    borderColor: '#F2BB21',
    //fill: false,
    tension: .4,
    //pointStyle: 'circle',
    //pointRadius: 6,
    //pointHoverRadius: 8,
    data: []
  };
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    animation: {
      duration: 0
    },
    responsive: false,
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
          }).format(value);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      },
      beforeInit: function (chart, options) {
        try {
          chart.legend.afterFit = function () {
            this.height = this.height + 50;
          };
        } catch { }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ' + Intl.NumberFormat('en-US', {
            }).format(tooltipItem.yLabel);
          }
          return label;
        }
      }
    },
    layout: {
      padding: {
        top: 15
      }
    },
    legend: {
      display: true,
      position: 'top',
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
              return label;
          },
          fontColor: '#A3A0FB'
        },
        scaleLabel: {
            display: true,
            fontColor: "#A3A0FB"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true
        }
      }]
    }
  };
  lstColor: Array<string> = [
    '#4bc0c0',
    '#42A5F5',
    '#9CCC65',
    '#FF8373',
    '#A3A0FB',
    '#FFDA83',
    '#55D8FE',
    '#44B980',
    '#BA59D6',
    '#657EB8'
  ];

  buildChartData() {
    this.chartData.labels = [];
    this.chartData.datasets = [];
    let clr = this.lstColor;

    let _fn = (records: Array<MaxBIReport.InboundOutboundCalls>, datasetName: string) => {
      let prop = this.callType == "IN" ? "inBoundCallsAverage" : "outBoundCallsAverage";

      let lables = [];
      if (this.calendarModel.Dates.length > 0) {
        lables = CommonHelper.getMonthDates(this.calendarModel.Month, this.calendarModel.Year);
      }
      else {
        lables = CommonHelper.getYearMonths(this.calendarModel.Month, this.calendarModel.Year);
      }
      let lst = [];
      lables.forEach(l => {
        if (this.calendarModel.Dates.length > 0) {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: CommonHelper.getFormattedDate(l.monthName, "MMM DD"),
            labelDate: l.monthName
          });
        }
        else {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: l.monthName
          });
        }
      });
      lst.sort(CommonHelper.sortOrderComparer("sortLabel"));

      let datasets = CommonHelper
        .uniqueList(records, datasetName)
        .map((m, i) => {
          let idx = i % clr.length;
          return {
            title: m[datasetName],
            color: clr[idx]
          };
        });

      lst.forEach(l => {
        this.chartData.labels.push(l.label);

        datasets.forEach(ds => {
          let d = 0;
          if (this.calendarModel.Dates.length > 0) {
            records
              .filter(r =>
                r[datasetName] == ds.title
                && CommonHelper.getFormattedDate(r.date, "YYYY-MM-DD") == l.labelDate)
              .forEach(r => d += parseFloat(r[prop]));
          }
          else {
            records
              .filter(r => r[datasetName] == ds.title && r.monthName == l.label)
              .forEach(r => d += parseFloat(r[prop]));
          }

          let dSet = null;
          let exists = this.chartData.datasets.filter(cds => cds.label === ds.title);
          if (exists.length > 0) {
            exists[0].data.push(d);
          }
          else {
            dSet = JSON.parse(JSON.stringify(this.charDataSetTemplate));
            dSet.label = ds.title;
            dSet.borderColor = ds.color;
            dSet.data = [];
            dSet.data.push(d);
            this.chartData.datasets.push(dSet);
          }
        });
      });
    };

    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        _fn(this.regionRecords, "rmName");
        break;
      case Enumerations.E_ReportFor.ZONE:
        _fn(this.zoneRecords, "zmName");
        break;
      case Enumerations.E_ReportFor.OFFICE:
        _fn(this.officeRecords, "agencyName");
        break;
      case Enumerations.E_ReportFor.COMPANY:
        _fn(this.companyRecords, "agentName");
        break;
    }
    console.log(this.chartData);

  }
  //#endregion

}
