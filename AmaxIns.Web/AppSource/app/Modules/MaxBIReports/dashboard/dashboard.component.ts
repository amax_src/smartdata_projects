import { Component, OnInit, Renderer2 } from '@angular/core';
import * as NProgress from 'nprogress';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { encryption } from '../../Services/Encryption';
import { CommonService } from './../../../core/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.scss',
    '../maxbi-agent.scss'
  ]
})
export class DashboardComponent implements OnInit {

  quoteTypeN: string;
  quoteTypeM: string;
  callTypeIN: string;
  callTypeOUT: string;

  constructor(public cmnSrv: CommonService,
    private renderer: Renderer2,
    private router: Router,
    private encrypt: encryption
  ) {
    this.quoteTypeN = this.encrypt.encrytptvalue('new');
    this.quoteTypeM = this.encrypt.encrytptvalue('modified');

    this.callTypeIN = this.encrypt.encrytptvalue('IN');
    this.callTypeOUT = this.encrypt.encrytptvalue('OUT');

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit(): void {
    this.cmnSrv.showLoader = false;
    this.cmnSrv.pageHeader = "Dashboard";

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

}
