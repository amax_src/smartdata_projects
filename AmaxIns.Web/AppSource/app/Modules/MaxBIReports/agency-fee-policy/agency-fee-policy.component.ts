import { MaxBIReportService } from '../../Services/MaxBIReportService';
import * as MaxBIReport from '../../../model/MaxBIReport';
import { CalendarModel } from '../../../model/CalanderModel';
import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { Location } from '../../../model/Location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { authenticationService } from '../../../login/authenticationService.service';
import { LocationFilterComponent } from '../../../shared/location-filter/location-filter.component';
import { CommonHelper, Enumerations } from '../../../utilities/common-helper';
import { MatTabGroup } from '@angular/material';

@Component({
  selector: 'app-agency-fee-policy',
  templateUrl: './agency-fee-policy.component.html',
  styleUrls: [
    './agency-fee-policy.component.css',
    '../maxbi-agent.scss'
  ]
})
export class AgencyFeePolicyComponent implements OnInit {

  location: Array<Location> = new Array<Location>();
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  allLocations: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedPayTypes: Array<string> = new Array<string>();
  selectedAgents: Array<string> = new Array<string>();

  Records: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  regionRecords: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  zoneRecords: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  officeRecords: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  companyRecords: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();

  agentRecordsMain: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  agentRecordsDetail: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
  agentRecords: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();

  selectedTab: number = 4;
  catalogInfo = {
    From: MaxBIReport.E_MaxBICatalogFrom.PaymentInfo,
    Type: MaxBIReport.E_MaxBICatalogType.Agent
  };

  pager: any = {};
  pagedItems: any[];

  calendarModel: CalendarModel = new CalendarModel();
  chartMainView: boolean = true;

  constructor(private router: Router, private renderer: Renderer2,
    public cmnSrv: CommonService, private Service: MaxBIReportService,
    private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
    this.resetPager();
  }

  @ViewChild(LocationFilterComponent) child;
  @ViewChild('tabs') tabGroup: MatTabGroup;

  ngOnInit() {
    this.cmnSrv.pageHeader = "Agency Fee / Policy";
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    if (e.firstTimeLoad) {
      this.selectedlocation.push(e.locations[0]);
      this.SearchReport();
    }
    else {
      this.selectedlocation = e.locations;
    }
  }

  RmChanged(e) { }
  ZmChanged(e) { }

  GetAllLocations(e) {
    this.allLocations = e.map(x => x.agencyId);
  }
  onPayTypeChanged(e) {
    this.selectedPayTypes = e;
  }
  onAgentChanged(e) {
    this.selectedAgents = e;
  }

  OnDateRangeSelected(e) {
    this.calendarModel = e;
  }

  OnMultiDatesSelected(e) {
    this.calendarModel = e;
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.cmnSrv.showLoader = e;
    }
  }

  SearchReport() {
    let that = this;
    this.chartMainView = true;
    this.GetData(function () {
      that.filterData();
      that.buildChartMainData();
    });
  }

  filterReset() {
    this.child.filterReset();
  }

  GetData(callback: Function = null) {
    this.emptyListing();
    this.summaryLoad = false;
    let reportBy = this.selectedTab + 1;
    let startTime: number = new Date().getTime();
    this.Records = new Array<MaxBIReport.AgencyPerPolicy>();
    if (this.selectedlocation.length == 0) {
      this.selectedlocation = this.allLocations;
    }
    /*if (this.selectedlocation.length > 100) {
      this.notifier.showNotification("Please narrow down locations filter to avoid report timeout.", "OK", "error");
    }
    else {*/
    if (this.calendarModel.FromDate
      && this.calendarModel.ToDate) {
      this.cmnSrv.showLoader = true;

      let request: MaxBIReport.MaxBIRequest = new MaxBIReport.MaxBIRequest();
      request.AgentIds = this.selectedAgents;
      request.PayTypes = this.selectedPayTypes;
      request.ReportBy = reportBy;
      request.ReportType = MaxBIReport.E_MaxBIReport.AgencyFeePerPolicy;
      request.AgencyIds = this.selectedlocation;
      request.FromDate = this.calendarModel.FromDate;
      request.ToDate = this.calendarModel.ToDate;
      request.Dates = this.calendarModel.Dates;
      this.Service.GetAgencyPerPolicyReport(request).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "MaxBIReports: Agency Fee / Policy Report"
        model.time = ResponseTime;
        this.Records = m;
        this.ischartDataLoaded = true;
        this.ApiLoad.SaveApiTime(model).subscribe();
        if (callback) {
          callback();
        }
        this.GetSummary();
        this.cmnSrv.showLoader = false;
      });
    }
    else {
      this.ischartDataLoaded = true;
    }
    //}
  }

  summaryLoad: boolean = false;
  summaryRequest: MaxBIReport.MaxBIRequest;
  summaryHierarchy: Array<MaxBIReport.HierarchyId>;
  GetSummary() {
    if (this.Records.length > 0) {
      this.summaryRequest = new MaxBIReport.MaxBIRequest();
      this.summaryRequest.CatalogFrom = MaxBIReport.E_MaxBICatalogFrom.PaymentInfo;
      this.summaryRequest.PayTypes = this.selectedPayTypes;
      this.summaryRequest.ReportType = MaxBIReport.E_MaxBIReport.AgencyFeePerPolicy;
      this.summaryRequest.FromDate = this.calendarModel.FromDate;
      this.summaryRequest.ToDate = this.calendarModel.ToDate;
      this.summaryRequest.Dates = this.calendarModel.Dates;

      //this.summaryHierarchy = CommonHelper.uniqueList(this.Records, "rmId,zmId,agencyId,carrierId");
      this.summaryHierarchy = this.allLocations.map((l) => { return { rmId: 0, zmId: 0, agencyId: l, carrierId: '' } });
      this.summaryLoad = true;
    }
  }

  tabChange(e) {
    let that = this;
    that.selectedTab = e;
    this.GetData(function () {
      that.tabGroup.selectedIndex = that.selectedTab;
      that.filterData();
      that.buildChartMainData();
    });
  }

  onPageChange(e) {
    this.pager.first = e.first;
    this.pager.page = e.page;
    this.pager.pageCount = e.pageCount;
    this.pager.rows = e.rows;
    this.setPaging();
  }

  filterDataMain() {
    let lst: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();

    let _fn = function (x, rec) {
      x.agencyFee = CommonHelper.with2Decimals(rec.map(o => o.agencyFee).reduce((prev, curr) => prev + curr));
      x.policies = rec.map(o => o.policies).reduce((prev, curr) => prev + curr);
      x.perPolicy = parseFloat(CommonHelper.with2Decimals((x.policies <= 0 ? 0 : ((x.agencyFee / x.policies) * 100) / 100)));
    };

    this.agentRecordsMain = [];
    let temp = [];
    let dates = [];
    for (let d = 0; d < this.calendarModel.Dates.length; d++) {
      dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
    }
    if (dates.length > 0) {
      temp = this.Records.filter(x => dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
    }
    else {
      temp = this.Records;
    }

    if (this.calendarModel.Dates.length > 0) {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,date");
    }
    else {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year");
    }
    lst.forEach((x, i) => {
      let rec = temp.filter(y => {
        let ret = false;
        if (dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }

        return ret;
      });
      _fn(x, rec);
      x.agents = temp.filter(y => {
        let ret = false;
        if (dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName);
        }

        return ret;
      });
      x.agents.forEach(r => {
        r.parentId = i;
        r.perPolicy = CommonHelper.with2Decimals((r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100));
      });
      x.recordId = i;
      this.agentRecordsMain.push(x);
    });
  }
  filterDataDetail(agentName) {
    let lst: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();
    let agentId = -1;
    if (agentName.length > 0) {
      agentId = this.Records.filter(x => x.agentName == agentName)[0].agentId;
    }

    let _fn = function (x, rec) {
      x.agencyFee = CommonHelper.with2Decimals(rec.map(o => o.agencyFee).reduce((prev, curr) => prev + curr));
      x.policies = rec.map(o => o.policies).reduce((prev, curr) => prev + curr);
      x.perPolicy = parseFloat(CommonHelper.with2Decimals((x.policies <= 0 ? 0 : ((x.agencyFee / x.policies) * 100) / 100)));
    };

    this.agentRecordsDetail = [];
    let temp = [];
    let dates = [];
    for (let d = 0; d < this.calendarModel.Dates.length; d++) {
      dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
    }
    if (dates.length > 0) {
      temp = this.Records.filter(x => x.agentId == (agentId == -1 ? x.agentId : agentId) && dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
    }
    else {
      temp = this.Records.filter(x => x.agentId == (agentId == -1 ? x.agentId : agentId));
    }

    if (this.calendarModel.Dates.length > 0) {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,policyType,date");
    }
    else {
      lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,policyType");
    }
    lst.forEach((x, i) => {
      let rec = temp.filter(y => {
        let ret = false;
        if (dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.policyType == x.policyType
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.policyType == x.policyType);
        }

        return ret;
      });
      _fn(x, rec);
      x.agents = temp.filter(y => {
        let ret = false;
        if (dates.length > 0) {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.policyType == x.policyType
            && y.date == x.date);
        }
        else {
          ret = (y.agentId == x.agentId
            && y.agencyId == x.agencyId
            && y.monthName == x.monthName
            && y.policyType == x.policyType);
        }

        return ret;
      });
      x.agents.forEach(r => {
        r.parentId = i;
        r.perPolicy = CommonHelper.with2Decimals((r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100));
      });
      x.recordId = i;
      this.agentRecordsDetail.push(x);
    });

  }

  filterData() {
    if (this.authenticationService.userRole === 'agent') {
      this.filterDataDetail('');
    }
    else {
      this.filterDataMain();
    }
    let lst: Array<MaxBIReport.AgencyPerPolicy> = new Array<MaxBIReport.AgencyPerPolicy>();

    let _fn = function (x, rec) {
      x.agencyFee = CommonHelper.with2Decimals(rec.map(o => o.agencyFee).reduce((prev, curr) => prev + curr));
      x.policies = rec.map(o => o.policies).reduce((prev, curr) => prev + curr);
      x.perPolicy = CommonHelper.with2Decimals((x.policies <= 0 ? 0 : ((x.agencyFee / x.policies) * 100) / 100));
    };

    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.regionRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "rmId,rmName,monthName,month,year,policyType");
        lst.forEach((x, i) => {
          let recs = this.Records.filter(y => {
            return y.rmId == x.rmId
              && y.monthName == x.monthName
              && y.policyType == x.policyType
          });
          _fn(x, recs);
          x.agents = this.Records.filter(a => {
            return a.rmId == x.rmId
              && a.monthName == x.monthName
              && a.policyType == x.policyType
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.perPolicy = r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100;
          });
          x.recordId = i;
          this.regionRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.zoneRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "zmId,zmName,monthName,month,year,policyType");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.zmId == x.zmId
              && y.monthName == x.monthName
              && y.policyType == x.policyType
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.zmId == x.zmId
              && a.monthName == x.monthName
              && a.policyType == x.policyType
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.perPolicy = r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100;
          });
          x.recordId = i;
          this.zoneRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.officeRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "agencyId,agencyName,monthName,month,year,policyType");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.agencyId == x.agencyId
              && y.monthName == x.monthName
              && y.policyType == x.policyType
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.agencyId == x.agencyId
              && a.monthName == x.monthName
              && a.policyType == x.policyType
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.perPolicy = r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100;
          });
          x.recordId = i;
          this.officeRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.companyRecords = [];
        lst = CommonHelper.uniqueList(this.Records, "carrierId,carrierName,monthName,month,year,policyType");
        lst.forEach((x, i) => {
          let rec = this.Records.filter(y => {
            return y.carrierId == x.carrierId
              && y.monthName == x.monthName
              && y.policyType == x.policyType
          });
          _fn(x, rec);
          x.agents = this.Records.filter(a => {
            return a.carrierId == x.carrierId
              && a.monthName == x.monthName
              && a.policyType == x.policyType
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.perPolicy = r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100;
          });
          x.recordId = i;
          this.companyRecords.push(x);
        });
        break;
      case Enumerations.E_ReportFor.AGENT:
        this.agentRecords = [];
        let temp = [];
        let dates = [];
        for (let d = 0; d < this.calendarModel.Dates.length; d++) {
          dates.push(CommonHelper.getFormattedDate(this.calendarModel.Dates[d], "YYYY-MM-DD"));
        }
        if (dates.length > 0) {
          temp = this.Records.filter(x => dates.includes(CommonHelper.getFormattedDate(x.date, "YYYY-MM-DD")));
        }
        else {
          temp = this.Records;
        }

        if (this.calendarModel.Dates.length > 0) {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,policyType,date");
        }
        else {
          lst = CommonHelper.uniqueList(temp, "agentId,agentName,agencyId,agencyName,monthName,month,year,policyType");
        }
        lst.forEach((x, i) => {
          let rec = temp.filter(y => {
            let ret = false;
            if (dates.length > 0) {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.policyType == x.policyType
                && y.date == x.date);
            }
            else {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.policyType == x.policyType);
            }

            return ret;
          });
          _fn(x, rec);
          x.agents = temp.filter(y => {
            let ret = false;
            if (dates.length > 0) {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.policyType == x.policyType
                && y.date == x.date);
            }
            else {
              ret = (y.agentId == x.agentId
                && y.agencyId == x.agencyId
                && y.monthName == x.monthName
                && y.policyType == x.policyType);
            }

            return ret;
          });
          x.agents.forEach(r => {
            r.parentId = i;
            r.perPolicy = CommonHelper.with2Decimals((r.policies <= 0 ? 0 : ((r.agencyFee / r.policies) * 100) / 100));
          });
          x.recordId = i;
          this.agentRecords.push(x);
        });
        break;
    }

    this.setPaging();
  }
  setPaging() {
    switch (this.selectedTab) {
      case Enumerations.E_ReportFor.REGION:
        this.pager.totalRecords = this.regionRecords.length;
        this.pagedItems = this.regionRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.ZONE:
        this.pager.totalRecords = this.zoneRecords.length;
        this.pagedItems = this.zoneRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.OFFICE:
        this.pager.totalRecords = this.officeRecords.length;
        this.pagedItems = this.officeRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.COMPANY:
        this.pager.totalRecords = this.companyRecords.length;
        this.pagedItems = this.companyRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
      case Enumerations.E_ReportFor.AGENT:
        this.pager.totalRecords = this.agentRecords.length;
        this.pagedItems = this.agentRecords.slice(this.pager.first, this.pager.first + this.pager.rows);
        break;
    }
  }

  resetPager() {
    this.pager = {
      first: 0,
      rows: 10,
      page: 0,
      pageCount: 0,
      totalRecords: 0
    };
  }

  emptyListing() {
    this.resetPager();
    this.pagedItems = [];
    this.regionRecords = [];
    this.zoneRecords = [];
    this.officeRecords = [];
    this.companyRecords = [];
    this.agentRecords = [];
    this.agentRecordsMain = [];
    this.agentRecordsDetail = [];
    this.chartData.labels = [];
    this.chartData.datasets = [];
  }

  //#region Chart
  resetChart() {
    this.chartMainView = true;
  }

  charDataSetTemplate = {
    backgroundColor: 'transparent',
    label: 'DataSet Title',
    //strokeColor: '#fff',
    borderColor: '#F2BB21',
    //fill: false,
    tension: .4,
    //pointStyle: 'circle',
    //pointRadius: 6,
    //pointHoverRadius: 8,
    data: []
  };
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    locale: 'en-US',
    animation: {
      duration: 0
    },
    responsive: true,
    title: {
      display: true,
      text: 'Agent Name',
      color: 'navy',
      position: 'top',
      align: 'center',
      font: {
        weight: 'bold',
        size: '18px'
      },
      padding: 8,
      fullSize: true,
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
          }).format(value);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        },
      },
      beforeInit: function(chart, options) {
        try {
          chart.legend.afterFit = function() {
            this.height = this.height + 50;
          };
        } catch { }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            maximumFractionDigits: 2
          }).format(tooltipItem.yLabel);
          return label;
        }
      }
    },
    layout: {
      padding: {
        top: 15
      }
    },
    legend: {
      display: true,
      position: "top",
      afterFit: function () {
        this.height = this.height + 50;
      },
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        //stacked: true,
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: '#A3A0FB'
        },
        scaleLabel: {
          display: true,
          fontColor: "#A3A0FB"
        },
        gridLines: {
          zeroLineColor: '#A3A0FB'
        }
      }],
      yAxes: [{
        //stacked: true,
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true,
          userCallback: function (value, index, values) {
            return new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              maximumFractionDigits: 0
            }).format(value);
          }
        },
        gridLines: {
          zeroLineColor: '#A3A0FB'
        }
      }]
    }
  };
  lstColor: Array<string> = [
    '#4bc0c0',
    '#42A5F5',
    '#9CCC65',
    '#FF8373',
    '#A3A0FB',
    '#FFDA83',
    '#55D8FE',
    '#44B980',
    '#BA59D6',
    '#657EB8'
  ];

  chartDataMain = {
    labels: [],
    datasets: []
  };
  chartOptionsMain = {
    locale: 'en-US',
    animation: {
      duration: 0
    },
    responsive: true,
    title: {
      display: true,
      text: 'Agent View',
      color: 'navy',
      position: 'top',
      align: 'center',
      font: {
        weight: 'bold',
        size: '18px'
      },
      padding: 8,
      fullSize: true,
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
          }).format(value);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      },
      beforeInit: function(chart, options) {
        try {
          chart.legend.afterFit = function() {
            this.height = this.height + 50;
          };
        } catch { }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            maximumFractionDigits: 2
          }).format(tooltipItem.yLabel);
          return label;
        }
      }
    },
    layout: {
      padding: {
        top: 15
      }
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        //stacked: true,
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: '#A3A0FB'
        },
        scaleLabel: {
          display: true,
          fontColor: "#A3A0FB"
        },
        gridLines: {
          zeroLineColor: '#A3A0FB'
        }
      }],
      yAxes: [{
        //stacked: true,
        ticks: {
          fontColor: '#A3A0FB',
          beginAtZero: true,
          userCallback: function (value, index, values) {
            return new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              maximumFractionDigits: 0
            }).format(value);
          }
        },
        gridLines: {
          zeroLineColor: '#A3A0FB'
        }
      }]
    },
    onHover: (event, item) => {
      if (this.authenticationService.userRole !== 'agent') {
        event.target.style.cursor = 'default';
        if (item[0]) {
          event.target.style.cursor = 'pointer';
        }
      }
    },
    onClick: (event, item) => {
      if (this.authenticationService.userRole !== 'agent') {
        if (item.length > 0) {
          const chart = item[0]._chart;
          const line = chart.getElementsAtEventForMode(
            event,
            'nearest',
            { intersect: true },
            true);

          if (line.length > 0) {
            debugger
            const agentName = chart.config.data.datasets[line[0]._datasetIndex].label;
            this.setDetailedChartTitle(agentName, chart.config.data.datasets[line[0]._datasetIndex].borderColor);
            this.filterDataDetail(agentName);
            this.buildChartData();
            this.chartMainView = false;
          }
        }
      }
    }
  };

  setDetailedChartTitle(title, color) {
    this.chartOptions.title.text = title;
    this.chartOptions.title.color = color;
  }

  buildChartMainData() {
    if (this.authenticationService.userRole === 'agent') {
      this.chartOptions.title.display = false;
      this.buildChartData();
      return;
    }
    this.chartDataMain.labels = [];
    this.chartDataMain.datasets = [];
    let clr = this.lstColor;

    let _fn = (records: Array<MaxBIReport.AgencyPerPolicy>, datasetName: string) => {

      let lables = [];
      if (this.calendarModel.Dates.length > 0) {
        lables = CommonHelper.getMonthDates(this.calendarModel.Month, this.calendarModel.Year);
      }
      else {
        lables = CommonHelper.getYearMonths(this.calendarModel.Month, this.calendarModel.Year);
      }
      let lst = [];
      lables.forEach(l => {
        if (this.calendarModel.Dates.length > 0) {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: CommonHelper.getFormattedDate(l.monthName, "MMM DD"),
            labelDate: l.monthName
          });
        }
        else {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: l.monthName,
            labelDate: l.monthName
          });
        }
      });
      lst.sort(CommonHelper.sortOrderComparer("sortLabel"));

      let datasets = CommonHelper
        .uniqueList(records, datasetName)
        .map((m, i) => {
          let idx = i % clr.length;
          return {
            title: m[datasetName],
            color: clr[idx]
          };
        });

      lst.forEach(l => {
        this.chartDataMain.labels.push(l.label);

        datasets.forEach(ds => {
          let d: number = 0;
          if (this.calendarModel.Dates.length > 0) {
            records
              .filter(r => r[datasetName] == ds.title
                && CommonHelper.getFormattedDate(r.date, "YYYY-MM-DD") == l.labelDate)
              .forEach(r => d += parseFloat(r.perPolicy.toString()));
          }
          else {
            records
              .filter(r => r[datasetName] == ds.title && r.monthName == l.label)
              .forEach(r => d += parseFloat(r.perPolicy.toString()));
          }

          let dSet = null;
          let exists = this.chartDataMain.datasets.filter(cds => cds.label === ds.title);
          if (exists.length > 0) {
            exists[0].data.push(d);
          }
          else {
            dSet = JSON.parse(JSON.stringify(this.charDataSetTemplate));
            dSet.label = ds.title;
            dSet.borderColor = ds.color;
            dSet.data = [];
            dSet.data.push(d);
            this.chartDataMain.datasets.push(dSet);
          }
        });
      });
      console.log(this.chartDataMain);
    };

    _fn(this.agentRecordsMain, "agentName");
  }
  buildChartData() {
    this.chartData.labels = [];
    this.chartData.datasets = [];
    let clr = this.lstColor;

    let _fn = (records: Array<MaxBIReport.AgencyPerPolicy>, datasetName: string) => {

      //let lables = CommonHelper.uniqueList(records, "year,month,monthName");
      let lables = [];
      if (this.calendarModel.Dates.length > 0) {
        lables = CommonHelper.getMonthDates(this.calendarModel.Month, this.calendarModel.Year);
      }
      else {
        lables = CommonHelper.getYearMonths(this.calendarModel.Month, this.calendarModel.Year);
      }
      let lst = [];
      lables.forEach(l => {
        if (this.calendarModel.Dates.length > 0) {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: CommonHelper.getFormattedDate(l.monthName, "MMM DD"),
            labelDate: l.monthName
          });
        }
        else {
          lst.push({
            sortLabel: l.year.toString() + ('00' + l.month).slice(-2),
            label: l.monthName,
            labelDate: l.monthName
          });
        }
      });
      lst.sort(CommonHelper.sortOrderComparer("sortLabel"));

      let datasets = CommonHelper
        .uniqueList(records, datasetName)
        .map((m, i) => {
          let idx = i % clr.length;
          return {
            title: m[datasetName],
            color: clr[idx]
          };
        });

      lst.forEach(l => {
        this.chartData.labels.push(l.label);

        datasets.forEach(ds => {
          let d = 0;
          if (this.calendarModel.Dates.length > 0) {
            records
              .filter(r => r[datasetName] == ds.title
                && CommonHelper.getFormattedDate(r.date, "YYYY-MM-DD") == l.labelDate)
              .forEach(r => d += parseFloat(r.perPolicy.toString()));
          }
          else {
            records
              .filter(r => r[datasetName] == ds.title && r.monthName == l.label)
              .forEach(r => d += parseFloat(r.perPolicy.toString()));
          }

          let dSet = null;
          let exists = this.chartData.datasets.filter(cds => cds.label === ds.title);
          if (exists.length > 0) {
            exists[0].data.push(d);
          }
          else {
            dSet = JSON.parse(JSON.stringify(this.charDataSetTemplate));
            dSet.label = ds.title;
            dSet.borderColor = ds.color;
            dSet.data = [];
            dSet.data.push(d);
            this.chartData.datasets.push(dSet);
          }
        });
      });

      console.log(this.chartData);
    };

    _fn(this.agentRecordsDetail, "policyType");
  }
  //#endregion

}
