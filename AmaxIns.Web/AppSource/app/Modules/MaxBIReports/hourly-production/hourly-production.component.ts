import { HourlyProduction } from './../../../model/HourlyProduction';
import { Component, OnInit, Renderer2 } from '@angular/core';
import { MaxBIReportService } from './../../Services/MaxBIReportService';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { authenticationService } from '../../../login/authenticationService.service';
import { CommonHelper } from '../../../utilities/common-helper';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { MaxBIRequest } from './../../../model/MaxBIReport';
import * as moment from 'moment';
import * as Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-hourly-production',
  templateUrl: './hourly-production.component.html',
  styleUrls: [
    './hourly-production.component.css',
    '../maxbi-agent.scss'
  ]
})
export class HourlyProductionComponent implements OnInit {
  //chartJs = Chart;
  //chartLabelPlugin = ChartDataLabels;

  reportData: Array<HourlyProduction> = [];
  selectedDate: Date;

  constructor(public cmnSrv: CommonService,
    private Service: MaxBIReportService,
    private authenticationService: authenticationService,
    private renderer: Renderer2,
    private router: Router
  ) {

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit(): void {
    //this.chartJs.plugins.unregister(this.chartLabelPlugin);

    this.cmnSrv.showLoader = false;
    this.cmnSrv.pageHeader = "Hourly Production";

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
    this.selectedDate = moment.utc(moment().format("YYYY-MM-DD")).toDate();
    this.GetData();
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  GetData() {
    this.reportData = [];
    let request = new MaxBIRequest();
    request.AgentIds = [];
    request.AgentIds.push(this.authenticationService.user.agentInfoID.toString());
    request.FromDate = this.selectedDate;
    request.ToDate = this.selectedDate;

    this.cmnSrv.showLoader = true;
    this.Service.GetAgentHourlyProduction(request).subscribe(m => {
      this.reportData = m;
      this.cmnSrv.showLoader = false;

      if (this.reportData.length > 0) {
        this.buildChartData();
      }
    });
  }

  onDateSeleted(e) {
    this.selectedDate = moment.utc(moment(e).format("YYYY-MM-DD")).toDate();
    this.GetData();
  }

  //#region Chart
  charDataSetTemplate = {
    label: 'DataSet Title',
    backgroundColor: '#fff',
    strokeColor: '#fff',
    borderColor: '#F2BB21',
    fill: false,
    tension: 0,
    pointStyle: 'circle',
    pointRadius: 6,
    pointHoverRadius: 8,
    data: []
  };
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    locale: 'en-US',
    animation: {
      duration: 0
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
          }).format(value);
        }
      },
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          if (label == "Policies: " || label == "Transactions: ") {
            return label + Intl.NumberFormat('en-US', {
            }).format(tooltipItem.yLabel);
          }
          else {
            label += Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD'
            }).format(tooltipItem.yLabel);
            return label;
          }
        }
      }
    },
    layout: {
      padding: {
        top: 25
      }
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        fontColor: "#fff",
        fontSize: 16
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          fontColor: '#FFF'
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: '#fff',
          beginAtZero: true,
          // userCallback: function (value, index, values) {
          //   return new Intl.NumberFormat('en-US', {
          //     style: 'currency',
          //     currency: 'USD',
          //     maximumFractionDigits: 0
          //   }).format(value);
          // }
        },
        scaleLabel: {
          display: true
        }
      }]
    }
  };
  lstColor: Array<string> = [
    '#4bc0c0',
    '#42A5F5',
    '#9CCC65',
    '#FF8373',
    '#A3A0FB',
    '#FFDA83',
    '#55D8FE',
    '#44B980',
    '#BA59D6',
    '#657EB8'
  ];

  buildChartData() {
    this.chartData.labels = [];
    this.chartData.datasets = [];

    console.log(this.reportData);
    let policies = [],
      agencyfee = [],
      premium = [],
      transactions = [];
    for (let i = 8; i < 22; i++) {
      this.chartData.labels.push(i + '-' + (i + 1));
      let v = this.reportData.filter(r => {
        return moment(r.paydate).format("YYYY-MM-DD") == moment(this.selectedDate).format("YYYY-MM-DD")
          && r.firsthour == i
      });
      if (v.length > 0) {
        policies.push(v[0].policies);
        agencyfee.push(v[0].agencyfee);
        premium.push(v[0].premium);
        transactions.push(v[0].transactions);
      }
      else {
        policies.push(0);
        agencyfee.push(0);
        premium.push(0);
        transactions.push(0);
      }
    }

    this.chartData.datasets.push({
      label: 'Policies',
      data: policies,
      borderColor: this.lstColor[0],
      tension: 0.4
    });

    this.chartData.datasets.push({
      label: 'Agency Fee',
      data: agencyfee,
      borderColor: this.lstColor[1],
      tension: 0.4
    });

    this.chartData.datasets.push({
      label: 'Premium',
      data: premium,
      borderColor: this.lstColor[2],
      tension: 0.4
    });

    this.chartData.datasets.push({
      label: 'Transactions',
      data: transactions,
      borderColor: this.lstColor[3],
      tension: 0.4
    });

  }
  //#endregion

}
