import { PayrollComponent } from './payroll/payroll.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { AgencyFeePolicyComponent  } from './agency-fee-policy/agency-fee-policy.component';
import { CarrierAgentComponent } from "./carrier-agent/carrier-agent.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { InboundOutboundCallsComponent } from "./inbound-outbound-calls/inbound-outbound-calls.component";
import { NewModifiedQuotesClosingRatiosComponent } from "./new-modified-quotes-closing-ratios/new-modified-quotes-closing-ratios.component";
import { NewModifiedQuotesComponent } from "./new-modified-quotes/new-modified-quotes.component";
import { PremiumPolicyComponent } from "./premium-policy/premium-policy.component";
import { HourlyProductionComponent } from './hourly-production/hourly-production.component';
import { TestMultiselectComponent } from './test-multiselect/test-multiselect.component';

const routes: Routes = [
  { path: 'TestMultiselect', component: TestMultiselectComponent}, 
  { path: 'HourlyProduction', component: HourlyProductionComponent, canActivate: [LoginGuard]}, 
  { path: 'AgentPayroll', component: PayrollComponent, canActivate: [LoginGuard]}, 
  { path: 'Agent-Dashboard', component: DashboardComponent, canActivate: [LoginGuard]}, 
  { path: 'CarrierAgent', component: CarrierAgentComponent, canActivate: [LoginGuard]},
  { path: 'AgencyFeePolicy', component: AgencyFeePolicyComponent, canActivate: [LoginGuard]},
  { path: 'PremiumPolicy', component: PremiumPolicyComponent, canActivate: [LoginGuard]},
  { path: 'NewModifiedQuotesAg', component: NewModifiedQuotesComponent, canActivate: [LoginGuard]},
  { path: 'NewModifiedQuotesClosingRatiosAg', component: NewModifiedQuotesClosingRatiosComponent, canActivate: [LoginGuard]},
  { path: 'InboundOutboundCalls', component: InboundOutboundCallsComponent, canActivate: [LoginGuard]}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MaxBIAgentRoutingModule { }
