import { SelectItem } from 'primeng/api';
import { MaxBIDashboardRequest, MaxBIPayroll, MaxBIPayrollFilter } from './../../../model/MaxBIReport';
import { MaxBIReportService } from './../../Services/MaxBIReportService';
import { Component, OnInit, Renderer2 } from '@angular/core';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { authenticationService } from '../../../login/authenticationService.service';
import { CommonHelper } from '../../../utilities/common-helper';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: [
    './payroll.component.scss',
    '../maxbi-agent.scss'
  ]
})
export class PayrollComponent implements OnInit {

  filterData: Array<MaxBIPayrollFilter>;
  records: Array<MaxBIPayroll>;
  reportData: Array<MaxBIPayroll> = [];
  monthSelectItem: SelectItem[] = [];
  yearSelectItem: SelectItem[] = [];
  payPeriodSelectItem: SelectItem[] = [];
  SelectedMonth: string;
  SelectedYear: number;
  SelectedPayPeriod: Array<string>;

  loadLocally: boolean = false;

  payPeriods: string = "-";
  tenure: string = "-";
  licenseTypes: string[] = [];
  effectiveRate: string = "0";
  regularRate: string = "0";
  totalPayroll: string = "0";

  constructor(public cmnSrv: CommonService,
    private Service: MaxBIReportService,
    private authenticationService: authenticationService,
    private renderer: Renderer2,
    private router: Router
  ) {

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit(): void {
    this.cmnSrv.showLoader = false;
    this.cmnSrv.pageHeader = "Payroll";

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
    let that = this;
    this.GetEmployeeInfo();
    this.GetFilterData(function () {
      that.SetCurrentWeek();
      that.GetPayrollData();
    });
  }

  GetEmployeeInfo() {
    let request = new MaxBIDashboardRequest();
    request.AgentIds = [];
    request.AgentIds.push(this.authenticationService.user.agentID);
    this.Service.GetEmployeeSensitiveInfo(request).subscribe(m => {
      if (m.length == 0) {
        this.tenure = 'N/A';
        this.licenseTypes.push('N/A');
      }
      else {
        this.tenure = CommonHelper.getTenureByDays(m[0].tenureInDays);

        let lType = m[0].licenseType1;
        let lNum = m[0].licenseNumber1;

        if (lType) {
          lType = lType.trim();
          if (lType) {
            if (lNum) {
              lNum = lNum.trim();
              if (lNum) {
                lType += " (" + lNum + ")";
              }
            }
            this.licenseTypes.push(lType);
          }
        }

        lType = m[0].licenseType2;
        lNum = m[0].licenseNumber2;

        if (lType) {
          lType = lType.trim();
          if (lType) {
            if (lNum) {
              lNum = lNum.trim();
              if (lNum) {
                lType += " (" + lNum + ")";
              }
            }
            this.licenseTypes.push(lType);
          }
        }
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  GetFilterData(callback: Function = null) {
    let request = new MaxBIDashboardRequest();
    request.AgentIds = [];
    request.AgentIds.push(this.authenticationService.user.agentID);
    this.monthSelectItem = [];
    this.yearSelectItem = [];
    this.payPeriodSelectItem = [];
    this.cmnSrv.showLoader = true;
    this.Service.GetPayrollDashboardFilterData(request).subscribe(m => {
      this.filterData = m;
      if (m.length > 0) {
        let lst = CommonHelper.uniqueList(m, "payPeriodMonth,month,year");

        lst.sort(CommonHelper.sortOrderComparer("year", false));
        this.PopulateDropDownList(this.yearSelectItem, CommonHelper.uniqueList(lst, "year", true));

        lst = CommonHelper.uniqueList(lst, "month");
        lst.sort(CommonHelper.sortOrderComparer("month"));
        lst.forEach(mn => {
          mn["payPeriodMonth"] = CommonHelper.getMonthName(mn.month)
        });
        this.PopulateDropDownList(this.monthSelectItem, CommonHelper.uniqueList(lst, "payPeriodMonth", true));
      }
      this.cmnSrv.showLoader = false;
      if (callback) {
        callback();
      }
    });
  }

  PopulateDropDownList(target: SelectItem[], lst: Array<any>) {
    lst.forEach(r => {
      target.push({
        label: r,
        value: r
      });
    })
  }

  GetPayrollData() {
    this.reportData = [];
    let request = new MaxBIDashboardRequest();
    request.AgentIds = [];
    request.AgentIds.push(this.authenticationService.user.agentID);
    request.PayPeriods = this.SelectedPayPeriod;
    request.Month = CommonHelper.getMonthNumber(this.SelectedMonth);
    request.Year = this.SelectedYear;
    this.cmnSrv.showLoader = true;
    this.Service.GetPayrollDashboardData(request).subscribe(m => {
      this.records = m;
      this.reportData = m;
      this.LoadReportLocally();
      this.cmnSrv.showLoader = false;
    });
  }

  LoadReport(e) {
    /*if (this.loadLocally) {
      this.LoadReportLocally();
    }
    else {*/
    this.GetPayrollData();
    //}
  }

  SetCurrentWeek() {
    let currentDate = new Date();
    let filter = this.filterData.filter(f => {
      return CommonHelper.dateInBetween(currentDate, f.payPeriodStart, f.payPeriodEnd);
    });

    let f: MaxBIPayrollFilter;
    if (filter.length > 0) {
      f = filter[0];
    }
    else {
      f = this.filterData[0];
    }

    this.SelectedMonth = f.payPeriodMonth;
    this.SelectedYear = f.year;

    this.filterPayPeriod();
    this.SelectedPayPeriod = [];
    this.SelectedPayPeriod.push(f.payPeriod);
  }

  LoadReportLocally() {
    // this.reportData = [];
    // if (this.SelectedPayPeriod.length > 0) {
    //   this.reportData = this.records.filter(r => {
    //     return this.SelectedPayPeriod.includes(r.payPeriod);
    //   });
    // }
    // else {
    //   let pp = this.filterData.filter(r => {
    //     return r.month == CommonHelper.getMonthNumber(this.SelectedMonth)
    //       && r.year == this.SelectedYear
    //   }).map(p => p.payPeriod);
    //   console.log(pp);

    //   this.reportData = this.records.filter(r => {
    //     return pp.includes(r.payPeriod);
    //   });
    // }

    this.payPeriods = this.SelectedPayPeriod.join(', ');
    this.payPeriods = this.payPeriods.length == 0 ? "-" : this.payPeriods;

    if (this.reportData.length > 0) {
      this.buildChartData();
    }
  }

  filterPayPeriod() {
    this.SelectedPayPeriod = [];
    this.payPeriodSelectItem = [];
    let data = this.filterData.filter(r => {
      return r.payPeriodMonth == this.SelectedMonth
        && r.year == this.SelectedYear
    });
    data.forEach(d => {
      this.payPeriodSelectItem.push({
        label: d.payPeriod,
        value: d.payPeriod
      });
    });
  }

  onMonthChange(e) {
    this.filterPayPeriod();
  }

  onYearChange(e) {
    this.filterPayPeriod();
  }

  onPayPeriodChange() { }


  //#region Chart
  charDataSetTemplate = {
    label: 'DataSet Title',
    backgroundColor: '#fff',
    strokeColor: '#fff',
    borderColor: '#F2BB21',
    fill: false,
    tension: 0,
    pointStyle: 'circle',
    pointRadius: 6,
    pointHoverRadius: 8,
    data: []
  };
  chartData = {
    labels: [],
    datasets: []
  };
  chartOptions = {
    layout: {
      padding: {
        top: 25,
        bottom: 15
      }
    },
    animation: {
      duration: 0
    },
    plugins: {
      legend: {
        labels: {
          fontColor: '#FFF'
        }
      },
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
          }).format(value);
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var label = data.datasets[0].data[tooltipItem.index] || '';

          if (label) {
            label = Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD'
            }).format(label);
          }
          return label;
        }
      }
    },
    legend: {
      display: true,
      position: "right",
      labels: {
        fontColor: "#fff",
        fontSize: 14
      }
    }
  };
  lstColor: Array<string> = [
    '#4bc0c0',
    '#42A5F5',
    '#9CCC65',
    '#FF8373',
    '#A3A0FB',
    '#FFDA83',
    '#55D8FE',
    '#44B980',
    '#BA59D6',
    '#657EB8'
  ];

  buildChartData() {
    this.chartData.labels = [];
    this.chartData.datasets = [];

    let tp = this.reportData.map(o => o.debitAmount).reduce((prev, curr) => prev + curr);
    this.totalPayroll = CommonHelper.with2Decimals(tp);
    let workingHours = this.reportData.map(o => o.debitHours).reduce((prev, curr) => prev + curr);
    this.effectiveRate = CommonHelper.with2Decimals((workingHours <= 0 ? 0 : (tp / workingHours)));

    let regular = this.reportData.filter(o => o.categoryDescription.toLowerCase() == 'regular');
    let regularAmount = 0,
      regularHours = 0;
    if (regular.length > 0) {
      regularAmount = regular.map(o => o.debitAmount).reduce((prev, curr) => prev + curr);
      regularHours = regular.map(o => o.debitHours).reduce((prev, curr) => prev + curr);
    }
    this.regularRate = CommonHelper.with2Decimals((regularHours <= 0 ? 0 : (regularAmount / regularHours)));

    let lables = CommonHelper.uniqueList(this.reportData, "categoryDescription");
    let data = [];
    let bgColors = [];
    let hbgColors = [];
    lables.forEach((l, i) => {
      this.chartData.labels.push(l.categoryDescription);

      let d = this.reportData.filter(r => {
        return r.categoryDescription == l.categoryDescription
      });
      if (d.length > 0) {
        data.push(CommonHelper.with2Decimals(d.map(o => o.debitAmount).reduce((prev, curr) => prev + curr)));
      }

      bgColors.push(this.lstColor[i]);
      hbgColors.push(this.lstColor[i]);
    });

    this.chartData.datasets.push({
      data: data,
      backgroundColor: bgColors,
      hoverBackgroundColor: hbgColors
    });

    console.log(this.chartData);
  }
  //#endregion

}
