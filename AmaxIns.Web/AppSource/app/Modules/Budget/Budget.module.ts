import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { AccordionModule } from 'primeng/accordion';
import { BudgetRoutingModule } from './Budget-routing.module';
import { BudgetComponent } from './Budget.component';
import { DonutChartComponent } from './DonutChart/DonutChart.component';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    MatTabsModule,
    BudgetRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
  ],
  declarations: [BudgetComponent, DonutChartComponent],
  exports: []
 })
export class BudgetModule { }
