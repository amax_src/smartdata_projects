import { Component, OnInit, Renderer2, Input, Output, EventEmitter } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { DateService } from '../../Services/dateService';
import { PageService } from '../../Services/pageService';
import { bugetService } from '../../Services/bugetService';
import { BudgetActualVsPlanDonut } from '../../../model/Budget/BudgetSearch';
import { BudgetDonutsChartData } from '../../../model/Budget/BudgetDonutsChartData';


@Component({
  selector: 'DonutChart-component',
  templateUrl: './DonutChart.component.html',
  styleUrls: ['./DonutChart.component.scss']
})
export class DonutChartComponent implements OnInit {
  DoNutchartOptions: any = {
    //elements: {
    //  center: {
    //    text: 'Red is 2/3 of the total numbers',
    //    color: '#FF6384', // Default is #000000
    //    fontStyle: 'Arial', // Default is Arial
    //    sidePadding: 20, // Default is 20 (as a percentage)
    //    minFontSize: 25, // Default is 20 (in px), set to false and text will not wrap.
    //    lineHeight: 25 // Default is 25 (in px), used for when text wraps
    //  }
    //},
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {

      padding: {
        right: 25,
        left: 25,
        top: 25,
        bottom: 25
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      display: false,
    }
  }

  constructor(private dateservice: DateService, private _pageservice: PageService, private router: Router,
    private renderer: Renderer2, public cmnSrv: CommonService, private _bugetService: bugetService) {
  }

  _search: BudgetActualVsPlanDonut
  _ChartName: string;
  showLoader: boolean = true
  @Input() set ChartName(_ChartName: string) {
    this._ChartName = _ChartName
  }

  @Input() set search(_search: BudgetActualVsPlanDonut) {
    this._search = _search
    if (_search) {
      this.createChart();
    }
  }
  @Output() SelectMonthBudgetOut = new EventEmitter();

  Chartdata: any;


  BudgetData: BudgetDonutsChartData = new BudgetDonutsChartData();

  ngOnInit() {
    //   this.createChart();
  }

  createChart()
  {
    this.showLoader = true;
    this.Chartdata = [];
    if (this._search.year) {
      this._bugetService.getBudgetDonutData(this._search).subscribe(m => {
        this.BudgetData = m;
        if (this.BudgetData) {
          if (this._search.requestType == "Selected") {
            this.SelectMonthBudgetOut.emit(this.BudgetData)
          }
          this.Chartdata = {
            labels: ['Actual', 'Remaining Budget'],
            datasets: [
              {
                data: [this.BudgetData.actual, this.BudgetData.remaningBudget],
                backgroundColor: [
                  "#FF6384",
                  "#36A2EB"
                ],
                hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB"
                ]
              }]
          };
        }
        this.showLoader = false;

      }, error => {
        this.showLoader = false;
      })
    }
  }
}




