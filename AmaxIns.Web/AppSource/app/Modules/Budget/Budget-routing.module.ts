import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { BudgetComponent } from './Budget.component';

const routes: Routes = [
  { path: 'Budget', component: BudgetComponent}//, canActivate: [LoginGuard], 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BudgetRoutingModule { }
