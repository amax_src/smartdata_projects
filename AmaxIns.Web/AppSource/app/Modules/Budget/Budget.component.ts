import { Component, OnInit, Renderer2 } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../core/common.service';
import { DateService } from '../Services/dateService';
import * as NProgress from 'nprogress';
import { PageService } from '../Services/pageService';
import { bugetService } from '../Services/bugetService';
import { BudgetDonutsChartData } from '../../model/Budget/BudgetDonutsChartData';
import { BudgetActualVsPlanDonut } from '../../model/Budget/BudgetSearch';
import { MonthModel } from '../../model/MonthModel';
import { Transaction } from '../../model/Budget/Transaction';
import { BudgetOverspendModel } from '../../model/Budget/BudgetOverspendModel';

@Component({
  selector: 'Budget-component',
  templateUrl: './Budget.component.html',
  styleUrls: ['./Budget.component.scss']
})
export class BudgetComponent implements OnInit {
  PageName: string = "Budget";
  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";
  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  showLoader: boolean = false;
  selectmonthName: string;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  cat: Array<string> = new Array<string>();
  department: Array<string> = new Array<string>();
  buget: Array<string> = new Array<string>();
  catSelectItem: SelectItem[] = [];
  deptSelectItem: SelectItem[] = [];

  selectedDepartment: string;
  selectedCat: Array<string> = new Array<string>();
  Budget: Array<string> = new Array<string>();
  selectedBudget: Array<string> = new Array<string>();
  BudgetSelectItem: SelectItem[] = [];

  BudgetSeletedMonth: BudgetDonutsChartData = new BudgetDonutsChartData();
  BudgetAnual: Array<BudgetDonutsChartData> = new Array<BudgetDonutsChartData>();

  LatestMonthChartdata: any;
  SelectedMonthChartdata: any;
  YtdChartdata: any;

  YTMsearch: BudgetActualVsPlanDonut;
  LatestMonthsearch: BudgetActualVsPlanDonut;
  SelectedMonthsearch: BudgetActualVsPlanDonut;
  TransSearch: BudgetActualVsPlanDonut;
  AnualLinedata: any;
  tabSelectedIndex: number = 0;

  LinechartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      custom: function (tooltip) {
        if (!tooltip) return;
        // disable displaying the color box;
        tooltip.displayColors = false;
      },
      callbacks: {
        label: function (tooltipItem, data) {
          var ToolTip = []//data.datasets[0].label + " : $" + data.datasets[0].data[tooltipItem.index] + "</br>" + data.datasets[1].label + " : $" + data.datasets[1].data[tooltipItem.index]
          ToolTip.push(data.datasets[0].label + " : $" + data.datasets[0].data[tooltipItem.index])
          ToolTip.push(data.datasets[1].label + " : $" + data.datasets[1].data[tooltipItem.index])
          ToolTip.push("Varience : $" + (data.datasets[1].data[tooltipItem.index] - data.datasets[0].data[tooltipItem.index]))

          return ToolTip;
          //return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }
  }
  bTrax: Array<Transaction>;
  pager: any = {};
  pagedItems: any[];
  SpendingModel: Array<BudgetOverspendModel> = new Array<BudgetOverspendModel>();
  SpendingSearch: BudgetActualVsPlanDonut;
  Spendingpager: any = {};
  SpendingpagedItems: any = [];
  constructor(private dateservice: DateService, private _pageservice: PageService, private router: Router,
    private renderer: Renderer2, public cmnSrv: CommonService, private _bugetService: bugetService) {
  }

  ngOnInit() {
    this.getYears();
    this.getDept();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  onYearchange(e) {
    this.getMonth();
    this.getYtdBudgetData();
  }
  onMonthchange(e) {
    this.selectmonthName = this.monthNames[parseInt(this.selectedmonth) - 1]
    if (this.tabSelectedIndex == 0) {
      this.getSelectedMonthBudgetData()
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetBudgetTrx();
    }
    else {
      this.GetBudgetSpend();
    }
  }


  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
      if (this.selectedBudget.length > 0) {
        this.getAnualData();
      }

    });
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice.GetMonthskeyvalue(this.selectedyear).toPromise().then(m => {
      months = m;
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })

      this.selectedmonth = this.monthSelectItem[this.monthSelectItem.length - 1].value;
      this.selectmonthName = this.monthSelectItem[this.monthSelectItem.length - 1].label;
    })
  }

  getDept() {
    this._bugetService.getDepartMent().subscribe(x => {
      this.department = x;
      this.department.forEach(m => {
        this.deptSelectItem.push({ label: m, value: m })
      })
      if (this.department.length > 0) {
        this.selectedDepartment = this.department[0];
        this.getCat();
      }
    })
  }

  getCat() {
    this.catSelectItem = [];
    this.selectedCat = [];
    this._bugetService.getCategories(this.selectedDepartment).subscribe(x => {
      this.cat = x;
      this.cat.forEach(m => {
        this.catSelectItem.push({ label: m, value: m })
        this.selectedCat.push(m)
      })
      if (this.cat.length > 0) {
        this.getBudgetName();
      }
    })
  }

  getBudgetName() {
    this.BudgetSelectItem = [];
    this.selectedBudget = [];
    this._bugetService.getBudgetName(this.selectedDepartment, this.selectedCat).subscribe(x => {
      this.Budget = x;
      this.Budget.forEach(m => {
        this.BudgetSelectItem.push({ label: m, value: m })
        this.selectedBudget.push(m)
      })
      this.LoadAllChart();
    })
  }

  LoadAllChart() {
    if (this.tabSelectedIndex == 0) {
      this.getLatestMonthBudgetData();
      this.getYtdBudgetData();
      this.getSelectedMonthBudgetData();
      this.getAnualData();
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetBudgetTrx();
    }
    else {
      this.GetBudgetSpend();
    }
  }

  onDeptchange(e) {
    this.getCat();
  }

  onCatChange(e) {
    this.getBudgetName();
  }


  onBudgetChange() {
    this.LoadAllChart();
  }

  getLatestMonthBudgetData() {
    this.LatestMonthsearch = new BudgetActualVsPlanDonut();
    this.LatestMonthsearch.year = this.selectedyear;
    this.LatestMonthsearch.Budget = this.selectedBudget;
    this.LatestMonthsearch.month = null;
    this.LatestMonthsearch.requestType = "Latest";
  }

  getSelectedMonthBudgetData() {

    this.SelectedMonthsearch = new BudgetActualVsPlanDonut()
    this.SelectedMonthsearch.year = this.selectedyear;
    this.SelectedMonthsearch.month = this.selectedmonth;
    this.SelectedMonthsearch.Budget = this.selectedBudget
    this.SelectedMonthsearch.requestType = "Selected";

  }

  getYtdBudgetData() {
    this.YTMsearch = new BudgetActualVsPlanDonut()
    this.YTMsearch.year = this.selectedyear;
    this.YTMsearch.month = "0";
    this.YTMsearch.Budget = this.selectedBudget
    this.YTMsearch.requestType = "YTM";

  }

  getAnualData() {
    let search: BudgetActualVsPlanDonut = new BudgetActualVsPlanDonut();
    search.year = this.selectedyear;
    search.Budget = this.selectedBudget
    search.month = null;
    this._bugetService.getBudgetAnuallData(search).subscribe(m => {
      this.BudgetAnual = m;
      let Actual: Array<number> = []
      let Budget: Array<number> = []
      let MonthName: Array<string> = []

      this.BudgetAnual.forEach(e => {
        Actual.push(e.actual)
        Budget.push(e.budget)
        MonthName.push(e.monthName)
      })
      this.AnualLinedata = {
        labels: MonthName, //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
        datasets: [
          {
            label: 'Budget Amount',
            data: Budget, //[65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderColor: '#ffd800'
          },
          {
            label: 'Actual',
            data: Actual,//[28, 48, 40, 19, 86, 27, 90],
            fill: false,
            borderColor: '#FF6384'
          }
        ]
      }
    });
    //BudgetAnual
  }


  selectAnualData(e) {
  }

  SelectMonthBudgetEmitter(e) {
    this.BudgetSeletedMonth = e;
  }
  GetBudgetTrx() {
    this.showLoader = true;
    this.pager = {};
    this.pagedItems = [];
    this.bTrax = new Array<Transaction>();
    this.TransSearch = new BudgetActualVsPlanDonut();
    this.TransSearch.year = this.selectedyear;
    this.TransSearch.month = this.selectedmonth;
    this.TransSearch.Budget = this.selectedBudget
    this._bugetService.getBudgetTrx(this.TransSearch).subscribe(m => {
      this.bTrax = m;
      this.showLoader = false;
      this.setPage(1);
    }, z => {
      this.showLoader = false;
    })

  }
  
  GetBudgetSpend() {
    this.showLoader = true;
    this.Spendingpager = {};
    this.SpendingpagedItems = [];
    this.SpendingModel = new Array<BudgetOverspendModel>();
    this.SpendingSearch = new BudgetActualVsPlanDonut();
    this.SpendingSearch.year = this.selectedyear;
    this.SpendingSearch.month = this.selectedmonth;
    this.SpendingSearch.Budget = this.selectedBudget
    this._bugetService.GetBudgetSpend(this.SpendingSearch).subscribe(m => {
      this.SpendingModel = m;
      this.showLoader = false;
      this.setSpendingPage(1);
    }, z => {
      this.showLoader = false;
    })
  }

  setSpendingPage(page: number) {
    if (page < 1 || page > this.Spendingpager.totalPages) {
      return;
    }
    this.Spendingpager = this._pageservice.getPager(this.SpendingModel.length, page, 100);
    this.SpendingpagedItems = this.SpendingModel.slice(this.Spendingpager.startIndex, this.Spendingpager.endIndex + 1);
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this._pageservice.getPager(this.bTrax.length, page, 100);
    this.pagedItems = this.bTrax.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  tabChanged(e) {
    this.tabSelectedIndex = e;
    if (e == 0) {
      this.LoadAllChart();
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetBudgetTrx();
    }
    else {
      this.GetBudgetSpend();
    }
  }

}

