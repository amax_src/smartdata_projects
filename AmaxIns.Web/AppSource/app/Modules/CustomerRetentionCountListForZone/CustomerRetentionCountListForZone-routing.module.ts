import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CustomerRetentionCountListForZoneComponent } from './CustomerRetentionCountListForZone.component';


const routes: Routes = [
  { path: 'CustomerRetentionCountListForZone', component: CustomerRetentionCountListForZoneComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomerRetentionCountListForZoneRoutingModule { }
