import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../core/common.service';
import { RevenueService } from '../Services/revenueService';
import { SessionService } from '../../core/storage/sessionservice';
import { revenue } from '../../model/revenue';
import { DateService } from '../Services/dateService';
import * as NProgress from 'nprogress';
import { TierPercentage } from '../../model/tierPercentage';
import { Workingdays } from '../../model/workingdays';
import { WorkingDaysService } from '../Services/workingdaysService';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { PageService } from '../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-tier-goal-component',
  templateUrl: './tier-goal-component.component.html',
  styleUrls: ['./tier-goal-component.component.scss']
})
export class TierGoalComponent implements OnInit {

  PageName: string = "TierGoal";
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  showLoader: boolean = false;
  revenueData: Array<revenue> = new Array<revenue>();
  filterRevenueData: Array<revenue> = new Array<revenue>();
  selectmonthindex: number = 0;
  workingdays: Workingdays;
  workingdaysList: Array<Workingdays> = new Array<Workingdays>();
  tierPercentage: Array<TierPercentage> = new Array<TierPercentage>();
  policyCountactualTileData: number = 0;
  policyPacingTile: number = 0;
  IsFirstTimePageLoad: boolean = true;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  tierChartdata: Array<{ _data: any, agenyName: string }>;

  tierChart: any;
  tierPolicyChart: any;
  tierAgencyFeeChart: any;

  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }

  PolicyCountchartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50,
        top: 25,
        bottom: 5
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' +value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;/// 1000 + 'k'
          },
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC",
         
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }]
    }

  }

  selectedIndex: number = 0;;

  constructor(private dateservice: DateService, private router: Router, private workingDaysService: WorkingDaysService, private renderer: Renderer2, public cmnSrv: CommonService, private revenueService: RevenueService, private sessionService: SessionService, private ApiLoad: ApiLoadTimeService, private _pageservice: PageService) {
    this.workingdays = new Workingdays();

  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.getWorkingDays();
    this.getTierData();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    //this.filterData()
  }

  onYearchange(e) {
    if (this.IsFirstTimePageLoad) {
      this.getMonth();
    }
    else {
     
      this.SearchgetMonth();
    }
    //this.filterData();
  }

  onMonthchange(e) {
    //this.filterData();
  }

  SearchReport() {
    this.showLoader = true;
    setTimeout(() => {
      this.filterData();
      this.showLoader = false;
    }, 200);
    
  }

  filterReset() {
    this.child.filterReset();
      this.selectedyear = (new Date()).getFullYear().toString();
    setTimeout(() => {
      this.getMonth();
    }, 700);
   
  }

  getWorkingDays() {
    this.workingDaysService.getWorkingdays().toPromise().then(m => {
      this.getconsolidatedRevenueData();
      this.workingdaysList = m;
    });
  }

  setWorkingDayForMonth() {
    let workingdays: Array<Workingdays>;
    workingdays = this.workingdaysList.filter(m => m.month === this.selectedmonth);
    workingdays = workingdays.filter(m => m.year.toString() === this.selectedyear)
    workingdays.forEach(i => {
      this.workingdays.month = i.month
      this.workingdays.remainingDays = i.remainingDays
      this.workingdays.totalWorkingdays = i.totalWorkingdays
      this.workingdays.workingDaysPassed = i.workingDaysPassed
      this.workingdays.year = i.year
    })
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  SearchgetMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.filterRevenueData = this.revenueData.filter(m => m.actYear === this.selectedyear);
    this.filterRevenueData = this.filterRevenueData.sort(this.sortMonth)
    months = Array.from(new Set(this.filterRevenueData.map(item => item.actmonth)));
    months.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i, value: i
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];
    //this.setWorkingDayForMonth();
    //this.filterData();
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.filterRevenueData = this.revenueData.filter(m => m.actYear === this.selectedyear);
    this.filterRevenueData = this.filterRevenueData.sort(this.sortMonth)
    months = Array.from(new Set(this.filterRevenueData.map(item => item.actmonth)));
    months.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i, value: i
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];
    this.setWorkingDayForMonth();
    this.filterData();
  }

  getconsolidatedRevenueData() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.revenueService.getRevenueData().subscribe(m => {
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Tier Goal Data"
      model.time = ResponseTime
      this.revenueData = m;
      this.getYears();
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    });
  }

  getTierData() {
    this.showLoader = true;
    this.revenueService.getTierData().subscribe(m => {
      this.tierPercentage = m;
      this.showLoader = false;
    })
  }

  filterData() {
    this.showLoader = true;
    this.IsFirstTimePageLoad = false;
    this.filterRevenueData = new Array<revenue>();
    this.filterRevenueData = this.revenueData;
    this.filterRevenueData = this.revenueData.filter(m => m.actYear === this.selectedyear);
    this.filterRevenueData = this.filterRevenueData.filter(m => m.actmonth === this.selectedmonth);


    let Locationfilterdata: Array<revenue> = new Array<revenue>();

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = this.filterRevenueData.filter(m => m.agencyid === i);
        filterdata.forEach(m => {
          Locationfilterdata.push(m);
        })
      })
    }
    else if (this.location && this.location.length > 0) {
      this.location.forEach(i => {
        let filterdata = this.filterRevenueData.filter(m => m.agencyid === i);
        filterdata.forEach(m => {
          Locationfilterdata.push(m);
        })
      })
    }
    this.filterRevenueData = Locationfilterdata;
    
    this.CalculateRawData();
    this.CalculateRowDataTier();
  }

  CalculateRowDataTier() {
    let premiumPacing: number = 0;
    let premiumActual: number = 0;

    let _tierData: Array<{ tierone: number, tiertwo: number, tierthree: number, tierfour: number, tierfive: number, premiumgoel: number, premiumPacing: number, premiumActual: number, agenyName: string }>;
    _tierData=new  Array<{ tierone: number, tiertwo: number, tierthree: number, tierfour: number, tierfive: number, premiumgoel: number, premiumPacing: number, premiumActual: number, agenyName: string }>();

    this.filterRevenueData.forEach(i => {
      i.pc_actual_newbusinesscount = 0;
      i.pre_goal = 0;
      i.pre_actual_premium = 0
    });

    this.filterRevenueData.forEach(i => {
      i.pc_actual_newbusinesscount = i.actnewbusinesscount;
      i.pre_actual_premium = i.actpremium;
    })

    this.filterRevenueData.forEach(i => {
      premiumActual = Math.round((!isNaN(i.pre_actual_premium) ? i.pre_actual_premium : 0));
      
      if (this.selectedmonth === this.monthNames[this.selectmonthindex] && this.selectedyear === (new Date()).getFullYear().toString()) {
        premiumPacing = Math.round(((premiumActual / this.workingdays.workingDaysPassed) * this.workingdays.remainingDays) + premiumActual);
      }
      else {
        premiumPacing = 0;
      }

      _tierData.push({
        tierone: Math.round((!isNaN(i.tier1Premium) ? i.tier1Premium : 0)),
        tiertwo: Math.round((!isNaN(i.tier2Premium) ? i.tier2Premium : 0)),
        tierthree: Math.round((!isNaN(i.tier3Premium) ? i.tier3Premium : 0)),
        tierfour: Math.round((!isNaN(i.tier4Premium) ? i.tier4Premium : 0)),
        tierfive: Math.round((!isNaN(i.tier5Premium) ? i.tier5Premium : 0)),
        premiumgoel: Math.round((!isNaN(i.goalPremium) ? i.goalPremium : 0)),
        premiumActual: Math.round((!isNaN(i.pre_actual_premium) ? i.pre_actual_premium : 0)),
        premiumPacing: premiumPacing,
        agenyName: i.actagencyname
      });
    });
    this.showLoader = false;
    this.renderChartTier(_tierData);
  }

  CalculateRawData() {
    let tierone: number = 0
    let tiertwo: number = 0
    let tierthree: number = 0
    let tierfour: number = 0
    let tierfive: number = 0
    let premiumgoel: number = 0;
    let premiumPacing: number = 0;
    let premiumActual: number = 0;
    let policyCountactual: number = 0;
    let policyCountgoalpacing: number = 0;
    let agencyFeeactual: number = 0;
    this.filterRevenueData.forEach(i => {
      i.pc_actual_newbusinesscount = 0;
      i.pre_goal = 0;
      i.pre_actual_premium = 0
    });
    this.filterRevenueData.forEach(i => {
      i.pc_actual_newbusinesscount = i.actnewbusinesscount;
      i.pre_actual_premium = i.actpremium;
    })

    this.filterRevenueData.forEach(i => {
      policyCountactual = Math.round(policyCountactual + (!isNaN(i.pc_actual_newbusinesscount) ? i.pc_actual_newbusinesscount : 0));
      premiumActual = Math.round(premiumActual + (!isNaN(i.pre_actual_premium) ? i.pre_actual_premium : 0));
      premiumgoel = Math.round(premiumgoel + (!isNaN(i.goalPremium) ? i.goalPremium : 0));
      tierone = tierone + (!isNaN(i.tier1Premium) ? i.tier1Premium : 0);
      tiertwo = tiertwo + (!isNaN(i.tier2Premium) ? i.tier2Premium : 0);
      tierthree = tierthree + (!isNaN(i.tier3Premium) ? i.tier3Premium : 0);
      tierfour = tierfour + (!isNaN(i.tier4Premium) ? i.tier4Premium : 0);
      tierfive = tierfive + (!isNaN(i.tier5Premium) ? i.tier5Premium : 0);
    });

    policyCountgoalpacing = Math.round(((policyCountactual / this.workingdays.workingDaysPassed) * this.workingdays.remainingDays) + policyCountactual);
    this.policyCountactualTileData = !isNaN(policyCountactual) ? policyCountactual : 0;
    if (this.selectedmonth === this.monthNames[this.selectmonthindex] && this.selectedyear === (new Date()).getFullYear().toString()) {
      this.policyPacingTile = !isNaN(policyCountgoalpacing) ? policyCountgoalpacing : 0;
      premiumPacing = Math.round(((premiumActual / this.workingdays.workingDaysPassed) * this.workingdays.remainingDays) + premiumActual);
    }
    else {
      this.policyPacingTile = 0;
      premiumPacing = 0;

    }
      this.renderChat(tierone, tiertwo, tierthree, tierfour, tierfive, premiumgoel, premiumPacing, premiumActual)
  }

  renderChat(one: number, two: number, three: number, four: number, five: number, premiumgoel: number, Premiumpacing: number, Premiumactual: number) {
    this.tierChart = {
      labels: ['Tier5', 'Tier4', 'Tier3', 'Tier2', 'Tier1', 'Goal Premium', 'Pacing Premium', 'Actual Premium'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
          borderColor: '#1E88E5',
          data: [five, four, three, two, one, premiumgoel, Premiumpacing, Premiumactual]
        }
      ]
    }

   
  }

  renderChartTier(_tierData: any) {
    this.tierChartdata = new Array<{ _data: any, agenyName: string }>();
    let _tierChartDetails: any;
   
    _tierData.forEach(i => {
      _tierChartDetails = {};
      _tierChartDetails = {
        labels: ['Tier5', 'Tier4', 'Tier3', 'Tier2', 'Tier1', 'Goal Premium', 'Pacing Premium', 'Actual Premium'],
        datasets: [
          {
            data: [i.tierfive, i.tierfour, i.tierthree, i.tiertwo, i.tierone, i.premiumgoel, i.premiumPacing, i.premiumActual],
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            hoverBackgroundColor: [
              "#FF6384",
              "#36A2EB",
              "#FFCE56",
              "#FF8373",
              "#55D8FE",
              "#FFDA83",
              "#42A5F5",
              "#BA59D6",
            ]
          }

        ]
      }
      this.tierChartdata.push({ _data: _tierChartDetails, agenyName: i.agenyName });
    });

    if (this.tierChartdata.length > 0) {
      this.setTierPaging(1);
    }
  }
 
  onTabChanged(e) {
    this.selectedIndex = e;
    //this.filterData();
  }
  pagerTier: any = {};
  pagedTier: any[];

  setTierPaging(page: number) {

    if (page < 1 || page > this.pagerTier.totalPages) {
      return;
    }
    // get pager object from service
    this.pagerTier = this._pageservice.getPayrollPager(this.tierChartdata.length, page, 10);

    // get current page of items
    this.pagedTier = this.tierChartdata.slice(this.pagerTier.startIndex, this.pagerTier.endIndex + 1);
  }
}
