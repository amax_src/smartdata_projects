import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TierGoalComponent } from './tier-goal-component.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { TierGoalRoutingModule } from './tier-goal-routing.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'

@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    TierGoalRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [TierGoalComponent],
  exports: []
 })
export class TierGoalModule { }
