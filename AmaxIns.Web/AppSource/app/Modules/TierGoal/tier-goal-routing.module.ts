import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TierGoalComponent } from './tier-goal-component.component';
import { LoginGuard } from '../../login/login-guard.service';

const routes: Routes = [
  { path: 'Tier', component: TierGoalComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TierGoalRoutingModule { }
