import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuotesProjectionComponent } from './quotes-projection.component';
import { LoginGuard } from '../../login/login-guard.service';

const routes: Routes = [
  { path: 'QuotesProjection', component: QuotesProjectionComponent, canActivate: [LoginGuard] }//, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotesProjectionRoutingModule { }
