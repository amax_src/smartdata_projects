import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../core/common.service';
import { UserService } from '../Services/UserService';
import { AgencyService } from '../Services/agencyService';
import { DateService } from '../Services/dateService';
import { SessionService } from '../../core/storage/sessionservice';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { QuotesService } from '../Services/quotesService';
import { PageService } from '../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';
import { MonthModel } from '../../model/MonthModel';
import { NewAndModifiedQuotesProj } from '../../model/NewModifiedQuotes';

@Component({
  selector: 'app-quotes-projection',
  templateUrl: './quotes-projection.component.html',
  styleUrls: ['./quotes-projection.component.scss']
})
export class QuotesProjectionComponent implements OnInit {
  showLoader: boolean = true;
  PageName: string = "Quotes Projection";
  selectedzonalManager: [];
  selectedRegionalManager: [];
  selectedlocation: Array<number> = []
  location: Array<number> = []
    selectedyear: string = (new Date()).getFullYear().toString();
  yearsSelectItem: SelectItem[] = [];
  monthSelectItem: SelectItem[] = [];
  selectmonthindex: number = 0;
  selectedmonth: string = "";

  NewQuotesProjCountChardata: Array<{ _data: any, activityName: string }>;
  ModifiedQuoteProjCountChardata: Array<{ _data: any, activityName: string }>;

  CreatedNewQuotesCountChardata: Array<{ _data: any, activityName: string }>;
  ModifiedQuoteCountChardata: Array<{ _data: any, activityName: string }>;

  PoliciesNewQuotesCountChardata: Array<{ _data: any, activityName: string }>;
  PoliciesModifiedQuoteCountChardata: Array<{ _data: any, activityName: string }>;

  newQuotes: Array<NewAndModifiedQuotesProj> = new Array<NewAndModifiedQuotesProj>();
  modifiedQuotes: Array<NewAndModifiedQuotesProj> = new Array<NewAndModifiedQuotesProj>();

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"

  ];


  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25,
        bottom: 5
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%'}`;
        }
      }
    },

    scales: {
      xAxes: [{
        ticks: {
          display: false
        }//this will remove all the x-axis grid lines
      }],
      yAxes: [{
        ticks: {
          display: false
        } //this will remove all the x-axis grid lines
      }]
    }
  }


  chartQuotesOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25,
        bottom: 5
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          display: false
        }//this will remove all the x-axis grid lines
      }],
      yAxes: [{
        ticks: {
          display: false
        } //this will remove all the x-axis grid lines
      }]
    }
  }

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private userService: UserService, private agencyService: AgencyService, private dateservice: DateService,
     private sessionService: SessionService,   private quotes: QuotesService, private _pageservice: PageService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.fillYear();
    this.getMonth();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.TabChange();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    //this.TabChange();
  }
  onYearchange() {
      this.getMonth();
  }
  loaderStatusChanged(e) {
    //
  }
  fillYear() {
    this.yearsSelectItem = [];
     this.yearsSelectItem.push(
      {
        label: '2022', value: '2022'
      })
      this.yearsSelectItem.push(
          {
              label: '2023', value: '2023'
          })
  }
  
  GetQuotesProjection() {
    this.showLoader = true;
    this.newQuotes = new Array<NewAndModifiedQuotesProj>();
    console.log("this.selectedmonth", this.selectedmonth)
    this.quotes.GetNewModiFiedQuotesProjection(this.selectedmonth, this.selectedyear).subscribe(x => {
      this.newQuotes = x;
      this.showLoader = false;
      console.log(this.newQuotes);
      this.FilterLocation();
    })
  }

  FilterLocation() {
    this.NewQuotesProjCountChardata = new Array<{ _data: any, activityName: string }>();
    this.ModifiedQuoteProjCountChardata = new Array<{ _data: any, activityName: string }>();

    this.CreatedNewQuotesCountChardata=new  Array<{ _data: any, activityName: string }>();
    this.ModifiedQuoteCountChardata = new Array<{ _data: any, activityName: string }>();

    this.PoliciesNewQuotesCountChardata = new Array<{ _data: any, activityName: string }>();
    this.PoliciesModifiedQuoteCountChardata = new Array<{ _data: any, activityName: string }>();

    let NewQuotesProj: any;
    let ModifiedQuotesProj: any;
    let CreatedQuotes: any;
    let ModifiedQuotes: any;
    if (this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        this.newQuotes.filter(x => x.agencyId == m).forEach(i => {
          var diffNew = i.differenceNewQuotes;
          if (i.differenceNewQuotes < 0) {
            diffNew = (i.differenceNewQuotes * -1);
          }
          NewQuotesProj = {};
          NewQuotesProj = {
            labels: ['CR Goal', 'CR Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.createdQuotesCR, i.newActualCR, diffNew]
              }
            ]
          }
          this.NewQuotesProjCountChardata.push({ _data: NewQuotesProj, activityName: i.agencyName });

          var diffmod = i.differenceModifiedQuotes;
          if (i.differenceModifiedQuotes < 0) {
            diffmod = (i.differenceModifiedQuotes * -1);
          }
          ModifiedQuotesProj = {};
          ModifiedQuotesProj = {
            labels: ['CR Goal', 'CR Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.modifiedQuotesCR, i.modifiedAtualCR, diffmod]
              }
            ]
          }
          this.ModifiedQuoteProjCountChardata.push({ _data: ModifiedQuotesProj, activityName: i.agencyName });

           //*************************************Created Quotes Chart*************************
          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Created Quotes', 'Sold Policy Quotes', 'Closing Ratio(%)'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.totalNewQuotes, i.soldNewQuotes, i.closingRatioNewQuotes]
              }
            ]
          }
          this.CreatedNewQuotesCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

          //*************************************Modified Quotes Chart*************************

          ModifiedQuotes = {};
          ModifiedQuotes = {
            labels: ['Modified Quotes', 'Sold Policy Quotes', 'Closing Ratio(%)'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.totalModifiyQuotes, i.soldModifiedQuotes, i.closingRatioModQuotes]
              }
            ]
          }
          this.ModifiedQuoteCountChardata.push({ _data: ModifiedQuotes, activityName: i.agencyName });

          //***************************************Actual vs Goal policies *************************************

          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.createdPolicies, i.soldNewQuotes, i.differenceNewPolicies]
              }
            ]
          }
          this.PoliciesNewQuotesCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.modifiedPolicies, i.soldModifiedQuotes, i.differenceModifiedPolicies]
              }
            ]
          }
          this.PoliciesModifiedQuoteCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

        })
      })
    }
    else {
      this.location.forEach(m => {
        this.newQuotes.filter(x => x.agencyId == m).forEach(i => {
          var diffNew = i.differenceNewQuotes;
          if (i.differenceNewQuotes < 0) {
            diffNew = (i.differenceNewQuotes * -1);
          }
          NewQuotesProj = {};
          NewQuotesProj = {
            labels: ['CR Goal', 'CR Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.createdQuotesCR, i.newActualCR, diffNew]
              }
            ]
          }
          this.NewQuotesProjCountChardata.push({ _data: NewQuotesProj, activityName: i.agencyName });

          
          var diffmod = i.differenceModifiedQuotes;
          if (i.differenceModifiedQuotes < 0) {
            diffmod = (i.differenceModifiedQuotes * -1);
          }
          ModifiedQuotesProj = {};
          ModifiedQuotesProj = {
            labels: ['CR Goal', 'CR Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.modifiedQuotesCR, i.modifiedAtualCR, diffmod]
              }
            ]
          }
          this.ModifiedQuoteProjCountChardata.push({ _data: ModifiedQuotesProj, activityName: i.agencyName });

          //*************************************Created Quotes Chart*************************
          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Created Quotes', 'Sold Policy Quotes', 'Closing Ratio(%)'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.totalNewQuotes, i.soldNewQuotes, i.closingRatioNewQuotes]
              }
            ]
          }
          this.CreatedNewQuotesCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

          //*************************************Modified Quotes Chart*************************

          ModifiedQuotes = {};
          ModifiedQuotes = {
            labels: ['Modified Quotes', 'Sold Policy Quotes', 'Closing Ratio(%)'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.totalModifiyQuotes, i.soldModifiedQuotes, i.closingRatioModQuotes]
              }
            ]
          }
          this.ModifiedQuoteCountChardata.push({ _data: ModifiedQuotes, activityName: i.agencyName });


          //***************************************Actual vs Goal policies *************************************

          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.createdPolicies, i.soldNewQuotes, i.differenceNewPolicies]
              }
            ]
          }
          this.PoliciesNewQuotesCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

          CreatedQuotes = {};
          CreatedQuotes = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
              {
                backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                borderColor: '#1E88E5',
                data: [i.modifiedPolicies, i.soldModifiedQuotes, i.differenceModifiedPolicies]
              }
            ]
          }
          this.PoliciesModifiedQuoteCountChardata.push({ _data: CreatedQuotes, activityName: i.agencyName });

        })
      })
    }

    this.setPolicyCountPaging(1);
    this.setAfeePaging(1);
    this.setCreatedPaging(1);
    this.setModifiedPaging(1);
    this.setPolicyGoalCountPaging(1);
    this.setModifiedSoldPoliciesPaging(1);
  }


  pagerNewQuotesPoliciesProjCount: any = {};
  pagedNewQuotesPoliciesProjCount: any[];

  setPolicyGoalCountPaging(page: number) {
    if ((page < 1 || page > this.pagerNewQuotesPoliciesProjCount.totalPages) && this.PoliciesNewQuotesCountChardata.length == 0) {
      return;
    }
    // get pager object from service
    this.pagerNewQuotesPoliciesProjCount = this._pageservice.getPayrollPager(this.PoliciesNewQuotesCountChardata.length, page, 10);
    // get current page of items
    this.pagedNewQuotesPoliciesProjCount = this.PoliciesNewQuotesCountChardata.slice(this.pagerNewQuotesPoliciesProjCount.startIndex, this.pagerNewQuotesPoliciesProjCount.endIndex + 1);
  }

  pagerModifiedQuotesPoliciesProj: any = {};
  pagedModifiedQuotesPoliciesProj: any[];
  setModifiedSoldPoliciesPaging(page: number) {
    if ((page < 1 || page > this.pagerModifiedQuotesPoliciesProj.totalPages) && this.PoliciesModifiedQuoteCountChardata.length == 0) {
      return;
    }
    // get pager object from service
    this.pagerModifiedQuotesPoliciesProj = this._pageservice.getPayrollPager(this.PoliciesModifiedQuoteCountChardata.length, page, 10);
    // get current page of items
    this.pagedModifiedQuotesPoliciesProj = this.PoliciesModifiedQuoteCountChardata.slice(this.pagerModifiedQuotesPoliciesProj.startIndex, this.pagerModifiedQuotesPoliciesProj.endIndex + 1);
  }


  pagerNewQuotesProjCount: any = {};
  pagedNewQuotesProjCount: any[];

  setPolicyCountPaging(page: number) {
    if ((page < 1 || page > this.pagerNewQuotesProjCount.totalPages) && this.NewQuotesProjCountChardata.length==0) {
      return;
    }
    // get pager object from service
    this.pagerNewQuotesProjCount = this._pageservice.getPayrollPager(this.NewQuotesProjCountChardata.length, page, 10);
    // get current page of items
    this.pagedNewQuotesProjCount = this.NewQuotesProjCountChardata.slice(this.pagerNewQuotesProjCount.startIndex, this.pagerNewQuotesProjCount.endIndex + 1);
    console.log("this.pagerNewQuotesProjCount", this.pagerNewQuotesProjCount)
  }

  pagerModifiedQuotesProj: any = {};
  pagedModifiedQuotesProj: any[];
  setAfeePaging(page: number) {
    if ((page < 1 || page > this.pagerModifiedQuotesProj.totalPages) && this.ModifiedQuoteProjCountChardata.length==0) {
      return;
    }
    // get pager object from service
    this.pagerModifiedQuotesProj = this._pageservice.getPayrollPager(this.ModifiedQuoteProjCountChardata.length, page, 10);
    // get current page of items
    this.pagedModifiedQuotesProj = this.ModifiedQuoteProjCountChardata.slice(this.pagerModifiedQuotesProj.startIndex, this.pagerModifiedQuotesProj.endIndex + 1);
  }

  pagerCreatedQuotes: any = {};
  pagedCreatedQuotes: any[];
  setCreatedPaging(page: number) {
    if ((page < 1 || page > this.pagerCreatedQuotes.totalPages) && this.CreatedNewQuotesCountChardata.length==0) {
      return;
    }
    // get pager object from service
    this.pagerCreatedQuotes = this._pageservice.getPayrollPager(this.CreatedNewQuotesCountChardata.length, page, 10);
    // get current page of items
    this.pagedCreatedQuotes = this.CreatedNewQuotesCountChardata.slice(this.pagerCreatedQuotes.startIndex, this.pagerCreatedQuotes.endIndex + 1);
  }

  pagerModifiedQuotes: any = {};
  pagedModifiedQuotes: any[];
  setModifiedPaging(page: number) {
    if ((page < 1 || page > this.pagerModifiedQuotes.totalPages) && this.ModifiedQuoteCountChardata.length==0) {
      return;
    }
    // get pager object from service
    this.pagerModifiedQuotes = this._pageservice.getPayrollPager(this.ModifiedQuoteCountChardata.length, page, 10);
    // get current page of items
    this.pagedModifiedQuotes = this.ModifiedQuoteCountChardata.slice(this.pagerModifiedQuotes.startIndex, this.pagerModifiedQuotes.endIndex + 1);
  }

  onTabChanged(index) {
    if (index == 1) {

    }
  }

  getMonth() {
    this.dateservice.GetMonthskeyvalue(this.selectedyear).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      let tempmonths: Array<MonthModel> = new Array<MonthModel>();
      let _Date = new Date();
      months = m;

      if (months.length > 2 && parseInt(this.selectedyear) == _Date.getFullYear()) {
          tempmonths = months;//months.slice(months.length - 2, months.length);
      }
      else {
        tempmonths = months;
      }
      months = tempmonths.reverse();
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })

      this.selectedmonth = months[0].id

      setTimeout(() => {
        this.GetQuotesProjection();
      }, 500);
    })
    //this.filterData();
  }

  SearchReport() {
    this.GetQuotesProjection();
   
  }

  onMonthchange() {

  }
  filterReset() {
      this.selectedyear = (new Date()).getFullYear().toString();
    this.showLoader = true;
    this.child.filterReset();
    this.fillYear();
    this.getMonth();
    setTimeout(() => {
      this.showLoader = false;
    }, 400);

    // this.getconsolidatedRevenueData();
  }

}
