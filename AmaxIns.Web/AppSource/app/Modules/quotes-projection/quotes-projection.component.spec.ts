import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotesProjectionComponent } from './quotes-projection.component';

describe('QuotesProjectionComponent', () => {
  let component: QuotesProjectionComponent;
  let fixture: ComponentFixture<QuotesProjectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotesProjectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesProjectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
