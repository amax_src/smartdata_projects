
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'

import { QuotesProjectionRoutingModule } from './quotes-projection-routing.module';
import { QuotesProjectionComponent } from './quotes-projection.component';

@NgModule({
  declarations: [QuotesProjectionComponent],
  imports: [
    SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
    QuotesProjectionRoutingModule
  ]
})
export class QuotesProjectionModule { }
