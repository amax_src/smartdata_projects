import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown'

import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { PanelModule } from "primeng/components/panel/panel";
import { ButtonModule } from "primeng/components/button/button";
import { DialogModule } from "primeng/components/dialog/dialog";
import { ToastModule } from "primeng/components/toast/toast";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService } from "primeng/api";
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from "primeng/calendar";

import { CustomerRetentionCountListForStoreComponent } from './CustomerRetentionCountListForStore.component';
import { CustomerRetentionCountListForStoreRoutingModule } from './CustomerRetentionCountListForStore-routing.module';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,    
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
    TableModule,
    PanelModule, ButtonModule, DialogModule, ToastModule, ConfirmDialogModule, InputTextareaModule, CalendarModule,
    CustomerRetentionCountListForStoreRoutingModule
  ],
  declarations: [CustomerRetentionCountListForStoreComponent],
  providers: [ConfirmationService],
  exports: []
 })
export class CustomerRetentionCountListForStoreModule { }
