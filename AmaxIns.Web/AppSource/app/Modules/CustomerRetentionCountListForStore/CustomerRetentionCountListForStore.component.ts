import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, ActivatedRoute } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { Location } from '../../model/location';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { DateService } from '../Services/dateService';

import { LazyLoadEvent, MessageService, ConfirmationService, SelectItem } from 'primeng/api';
import { Table } from "primeng/components/table/table";

import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { authenticationService } from '../../login/authenticationService.service';

//import { ExportFileService } from '../Services/ExportFileService';
import { DownloadexcelService } from "./../Services/downloadexcel.service";
import { DatePipe } from '@angular/common';

import { AgencyService } from '../Services/agencyService';
import { Agency } from '../../model/Agency/Agency';

import { CustomerRetentionService } from '../Services/CustomerRetentionService';
import { CustomerRetentionCountForZone } from '../../model/CustomerRetention/CustomerRetentionCountForZone';
import { DueDateFilterTypeEnum } from '../../model/CustomerRetention/DueDateFilterTypeEnum';


@Component({
  selector: 'app-CustomerRetentionCountListForStore',
  templateUrl: './CustomerRetentionCountListForStore.component.html',
  styleUrls: ['./CustomerRetentionCountListForStore.component.scss']
})
export class CustomerRetentionCountListForStoreComponent implements OnInit, AfterViewChecked {

  PageName: string = "Customer Retention";
  
  showLoader: boolean = false;

  //@ViewChild(Table) dt: Table;
  @ViewChild("dt") dt: Table;

  ColumnList: any[];  

  //CustomerRetentionList: Array<CustomerRetention> = new Array<CustomerRetention>();
  CustomerRetentionCountList: CustomerRetentionCountForZone[];
  totalRecordCount: number = 0;
  loadingTable: boolean = false;

  displayDialog: boolean = false;

  DueDateFilterTypeSelectItemList: SelectItem[] = [];
  
  CustomerRetentionCountList2: CustomerRetentionCountForZone[];
  totalRecordCount2: number = 0;  
  loadingTable2: boolean = false;

  CustomerRetentionCountList3: CustomerRetentionCountForZone[];
  totalRecordCount3: number = 0;  
  loadingTable3: boolean = false;

  CustomerRetentionCountList4: CustomerRetentionCountForZone[];
  totalRecordCount4: number = 0;  
  loadingTable4: boolean = false;

  selectedDueDate: Date;

  years: Array<any> = Array<any>();
  yearSelectItemList: SelectItem[] = [];
  selectedYear: string = "";

  months: Array<any>;
  monthSelectItemList: SelectItem[] = [];
  selectedMonth: string = "";
  selectedMonthIndex: number = 0;

  date: Array<any>;
  dateSelectItemList: SelectItem[] = [];
  //selectedDate: Array<Date> = new Array<Date>();
  selectedDate: Date;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  isMonthChanged: boolean = true;


  selectedRegionalManager: [];
  selectedzonalManager: []
  selectedlocation: Array<number> = []
  location: Array<number> = []

  currentZoneManagerUserId: number = 0;
  currentZoneManagerUserName: string = '';

  showSwitchButtonToStoreManagerView: boolean = true;

  agencyName: string = "";
  currentStoreManagerUserId: number = 0;

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private _pageservice: PageService, private dateService: DateService,
    private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService,
    private readonly _downloadExcel: DownloadexcelService, public datepipe: DatePipe,
    private messageService: MessageService, private confirmationService: ConfirmationService,
    private route: ActivatedRoute,
    private agencyService: AgencyService,
    private customerRetentionService: CustomerRetentionService
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {

    //this.loadDueDateFilterType();

    this.getYears();

    if (this.authenticationService.userRole === 'storemanager') {
      this.selectedDueDate = this.getYesterday();
      this.selectedDate = this.getYesterday();
      this.currentStoreManagerUserId = this.authenticationService.userId;
      this.getStoreManagerAgency(this.currentStoreManagerUserId);
    }
    else if (this.authenticationService.userRole === 'zonalmanager') {
      this.selectedDueDate = this.getYesterday();
      this.selectedDate = this.getYesterday();
      let queryParamAgencyId = this.route.snapshot.queryParamMap.get('agencyId');
      if(queryParamAgencyId != null && queryParamAgencyId != undefined && queryParamAgencyId != ''){
        //this.currentAgencyId = Number(queryParamAgencyId);
        
        //this.showSwitchButtonToStoreManagerView = true;
      }
    }

    //this.showLoader = true;

    this.loadingTable = true;
    this.loadingTable2 = true;
    this.loadingTable3 = true;
    this.loadingTable4 = true;
    
    this.ColumnList = [
      { field: 'dueDate', header: 'Due Date', isDate:true, format: 'MM/dd/yyyy', width:'110px' },
      { field: 'locationName', header: 'Location Name', width:'110px' },      
      { field: 'paymentDueCount', header: '# Payments Due', width:'80px' },
      { field: 'paymentReceivedCount', header: '# Payments Received', width:'80px' },
      { field: 'difference', header: 'Difference', width:'80px' }      
    ];

    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });

  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;

    if(this.selectedlocation.length == 1){
      this.showSwitchButtonToStoreManagerView = true;
    }
    else {
      this.showSwitchButtonToStoreManagerView = false;
    }

    //this.TabChange();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    //this.TabChange();
  }

  loaderStatusChanged(e) {
    // if (this.ischartDataLoaded) {
    //   // this.showLoader = e;
    // }
  }

  switchToStoreManagerView(){
    this.router.navigateByUrl("/CustomerRetentionList");
  }


  onYearChange(e) {
    this.getMonth(this.selectedYear);
    
  }

  onMonthChange(e) {    
    this.isMonthChanged = true;

    //set this.selectmonthindex
    for (let index = 0; index < this.monthNames.length; index++) {
      if(this.monthNames[index] == this.selectedMonth){
        this.selectedMonthIndex = index;
        break;
      };      
    }

    this.getDate();
  }

  getYears() {
    this.dateService.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        if (i > 2019) {
          this.yearSelectItemList.push(
            {
              label: i, value: i
            })
        }
      })

      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedYear = _selectedYear;
      this.selectedMonth = "";
      this.getMonth(this.selectedYear);
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth(year: string) {
    this.monthSelectItemList = [];
    let months: Array<string> = new Array<string>();
    this.selectedMonthIndex = 0;
    
    months = this.monthNames;
    months.forEach(i => {
      this.monthSelectItemList.push(
        {
          label: i, value: i
        })
    });

    let monthcount = (new Date()).getMonth();
    this.selectedMonthIndex = monthcount - 1;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectedMonthIndex = x;
        break;
      }
    }

    let _currentyear: string = (new Date()).getFullYear().toString()
    if (year === _currentyear) {
      this.selectedMonth = this.monthNames[this.selectedMonthIndex];
    }
    else {
      this.selectedMonth = this.monthNames[11];
    }

    this.getDate();

  }

  getDate() {
    console.log('Date Method');

    if (this.authenticationService.userRole === 'storemanager') {
      this.selectedDueDate = this.getYesterday();
      this.selectedDate = this.getYesterday();
    }
    else if (this.authenticationService.userRole === 'zonalmanager') {
      this.selectedDueDate = this.getYesterday();
      this.selectedDate = this.getYesterday();
    }


    this.dateSelectItemList = [];
    let dt: Array<Date> = new Array<Date>();

    let lastDayNumber = this.getLastDayOfMonth(Number(this.selectedYear), this.selectedMonthIndex);
    for (let i = 1; i <= lastDayNumber; i++) {
      let dateValue = new Date(Number(this.selectedYear), this.selectedMonthIndex, i);
      this.dateSelectItemList.push(
          {
            label: this.getDateString(this.datepipe.transform(dateValue,'MM/dd/yyyy')), value: dateValue
          });
    }

    // select default date on page load
    this.selectedDate = (new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));
    this.selectedDueDate = (new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    //this.TabChange();
    this.selectedDueDate = this.selectedDate; //new Date();
  }
  
  getLastDayOfMonth(year: number, month:number){
    return new Date(year, month + 1, 0).getDate();
  }

  getYesterday() {
    let d = new Date();
    d.setDate(d.getUTCDate() - 1);
    //return d.toISOString().split('T')[0]; // yesterday(); // 2018-10-17 (if current date is 2018-10-18)
    return d;
  };


  getToday() {
    let d = new Date();
    d.setDate(d.getUTCDate());
    //return d.toISOString().split('T')[0]; // yesterday(); // 2018-10-17 (if current date is 2018-10-18)
    return d;
  }

  loadDueDateFilterType() {
    this.DueDateFilterTypeSelectItemList = [];
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Today', value: '1'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Next 7 Days', value: '2'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Past Due', value: '3'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Canceled', value: '4'
      });
  }

  getStoreManagerAgency(Id: number) {
    this.agencyService.getStoreManagerAgency(Id).subscribe(m => {
      this.agencyName = m[0].agencyName;
    });
  }
  filterReset() {
    this.getMonth('2021');
    this.getDate();
  }
  loadCustomerRetentionCountList(event: LazyLoadEvent) {
    this.loadingTable = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        ZoneManagerUserId: this.currentZoneManagerUserId,
        StoreManagerUserId: this.currentStoreManagerUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueToday,
        DueDate: this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy')
      };
      this.CustomerRetentionCountList = new Array<CustomerRetentionCountForZone>();
      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList = res.data;
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });

    }, 500);

  }
 
  loadCustomerRetentionCountList2(event: LazyLoadEvent) {
    this.loadingTable2 = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        ZoneManagerUserId: this.currentZoneManagerUserId,
        StoreManagerUserId: this.currentStoreManagerUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueNext7Days,
        DueDate: this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy')
      };
      this.CustomerRetentionCountList2 = new Array<CustomerRetentionCountForZone>();
      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });
    }, 500);

  }

  loadCustomerRetentionCountList3(event: LazyLoadEvent) {
    this.loadingTable3 = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        ZoneManagerUserId: this.currentZoneManagerUserId,
        StoreManagerUserId: this.currentStoreManagerUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.PastDue,
        DueDate: this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy')
      };

      this.CustomerRetentionCountList3 = new Array<CustomerRetentionCountForZone>();

      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });

    }, 500);

  }

  loadCustomerRetentionCountList4(event: LazyLoadEvent) {
    this.loadingTable4 = true;

    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    // https://www.primefaces.org/primeng/v7.2.6-lts/#/table/lazy
    // https://github.com/primefaces/primeng/blob/master/src/app/showcase/components/table/tablelazydemo.ts

    setTimeout(() => {

      //this.CustomerRetentionService.getAll({ lazyEvent: JSON.stringify(event) }).then(res => {
      //  this.CustomerRetentionList = res.data;
      //  this.totalRecordCount = res.totalRecordCount;
      //  this.loadingTable = false;
      //});

      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        ZoneManagerUserId: this.currentZoneManagerUserId,
        StoreManagerUserId: this.currentStoreManagerUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.Canceled
      };

      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList4 = res.data;
        this.totalRecordCount4 = res.totalRecordCount;
        this.loadingTable4 = false;
      });

    }, 500);

  }

  Search() {
    console.log("this.selectedDate", this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy'))
    this.loadingTable3 = true;
    var queryModel = {
      RowOffset: 0,
      PageSize: 10,
      SortBy: "dueDate",
      SortDir: 'ASC',
      ZoneManagerUserId: this.currentZoneManagerUserId,
      StoreManagerUserId: this.currentStoreManagerUserId,
      DueDateFilterTypeId: DueDateFilterTypeEnum.DueToday,
      DueDate: this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy')
    };


    this.loadCustomerRetentionCountListSearch(queryModel);
    this.loadCustomerRetentionCountList2Search(queryModel);
    this.loadCustomerRetentionCountList3Search(queryModel);
  }

  loadCustomerRetentionCountListSearch(queryModel: any) {
    this.loadingTable = true;
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;

      console.log("queryModel", queryModel);
      this.CustomerRetentionCountList = new Array<CustomerRetentionCountForZone>();
      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList = res.data;
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });
    }, 500);
  }

  loadCustomerRetentionCountList2Search(queryModel: any) {
    this.loadingTable2 = true;
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;
      this.CustomerRetentionCountList2 = new Array<CustomerRetentionCountForZone>();
      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });

    }, 500);

  }

  loadCustomerRetentionCountList3Search(queryModel: any) {
    this.loadingTable3 = true;
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;
      this.CustomerRetentionCountList3 = new Array<CustomerRetentionCountForZone>();
      this.customerRetentionService.getAllCountForZonePaging(queryModel).then(res => {
        this.CustomerRetentionCountList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });
    }, 500);
  }

  public exportToExcel(excelType) {
    this.showLoader = true;
    var queryModel = {
      RowOffset: 0,
      PageSize: 1000000,
      SortBy: "dueDate",
      SortDir: 'ASC',
      ZoneManagerUserId: this.currentZoneManagerUserId,
      StoreManagerUserId: this.currentStoreManagerUserId,
      DueDateFilterTypeId: 0,
      DueDate: this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy')
    };

    if(excelType == 1){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;
    }
    else if(excelType == 2){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;
    }
    else if(excelType == 3){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;
    }


    this.customerRetentionService.getAllCountForZonePagingExport(queryModel).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      this.showLoader = false;
    })

  }

  public exportDataToExcel(excelType, dataList) {
    
    // let reportHeader = { 
    //   dueDate:"Due Date", locationName:"Location Name", 
    //   paymentDueCount:"# Payments Due", paymentReceivedCount:"# Payments Received", 
    //   difference:"Difference"
    // };

    // let agentReport = this.filteredData.map(agent =>({agentId:agent.agentId,firstName: agent.firstName,lastName:agent.lastName,
    //                                                   email:agent.email, userType: UserTypes[agent.userType],
    //                                                   status:agent.status === 1 ? 'ACTIVE' : 'INACTIVE', 
    //                                                   description:agent.description, userId:agent.userId === null ? 'No' : 'Yes',
    //                                                   createUser:agent.createUser, createDate:this.datepipe.transform(agent.createDate,'MM/dd/yyyy'),
    //                                                   updateUser:agent.updateUser, updateDate:this.datepipe.transform(agent.updateDate, 'MM/dd/yyyy')})
    //                                                 );

    let reportDataList = [];

    let sourceReportDataList = dataList;    

    let mappedDataList = sourceReportDataList.map( item => (
      { 
        dueDate: this.datepipe.transform(item.dueDate,'MM/dd/yyyy'), locationName: item.locationName, 
        paymentDueCount: item.paymentDueCount.toString(), paymentReceivedCount: item.paymentReceivedCount.toString(), 
        difference: item.difference.toString()
      }
    ));

    mappedDataList.map(item => {
      reportDataList.push(item);
    });

    //reportDataList.unshift(reportHeader);

    // var wscols = [
    //   {wch: 10}, 
    //   {wch: 20}, 
    //   {wch: 20}, 
    //   {wch: 20}, 
    //   {wch: 20}
    // ];

    var wscols = [
      { header: 'Due Date', key: 'dueDate', width: 15 },
      { header: 'Location Name', key: 'locationName', width: 30 },
      { header: '# Payments Due', key: 'paymentDueCount', width: 15 },
      { header: '# Payments Received', key: 'paymentReceivedCount', width: 15 },
      { header: 'Difference', key: 'difference', width: 15 }
    ];

    if (reportDataList.length > 0) {
        //this.exportService.exportExcel(reportDataList,'Customer Retention For Zone', wscols);
       // this.exportService.exportExcelWithColor(reportDataList,'Customer Retention For Zone', wscols);
    }

  }


}
