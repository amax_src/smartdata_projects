import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CustomerRetentionCountListForStoreComponent } from './CustomerRetentionCountListForStore.component';


const routes: Routes = [
  { path: 'CustomerRetentionCountListForStore', component: CustomerRetentionCountListForStoreComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomerRetentionCountListForStoreRoutingModule { }
