import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { RevenueDailyComponent } from './RevenueDaily.component';

const routes: Routes = [
  { path: 'RevenueDaily', component: RevenueDailyComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RevenueDailyRoutingModule { }
