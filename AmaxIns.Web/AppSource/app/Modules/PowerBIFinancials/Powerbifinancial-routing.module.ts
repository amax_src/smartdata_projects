import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import {PowerbiFinDashboadComponent } from '../PowerBIFinancials/PowerBi-Fin-Dashboard/powerbi-fin-dashboad.component';
const routes: Routes = [

    { path: 'powerBi-financial/:pageName', component: PowerbiFinDashboadComponent }
   
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PowerbifinancialRoutingModule { }
