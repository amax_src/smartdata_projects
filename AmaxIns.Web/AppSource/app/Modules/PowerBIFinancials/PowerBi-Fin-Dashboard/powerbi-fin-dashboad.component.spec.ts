import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerbiFinDashboadComponent } from './powerbi-fin-dashboad.component';

describe('AlpaAisComponent', () => {
    let component: PowerbiFinDashboadComponent;
    let fixture: ComponentFixture<PowerbiFinDashboadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [PowerbiFinDashboadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(PowerbiFinDashboadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
