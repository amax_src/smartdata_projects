import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, ActivatedRoute } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { AISService } from '../../Services/aisService';
import { DateService } from '../../Services/dateService';

@Component({
    selector: 'app-powerbi-fin-dashboad',
    templateUrl: './powerbi-fin-dashboad.component.html',
    styleUrls: ['./powerbi-fin-dashboad.component.scss']
})
export class PowerbiFinDashboadComponent implements OnInit {
    innerHeight: number = 0;
    innercontainer: string = "400px";
    url: string = "https://app.powerbi.com/reportEmbed?reportId=a0c2af29-91dd-4933-947d-f95f8e311359&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
    pageName: string = "powerBi-financial";
    constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
        , private aisService: AISService, private dateservice: DateService,private activateRouter: ActivatedRoute) {
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }

    ngOnInit() {
       

        this.router.events.subscribe((obj: any) => {
            if (obj instanceof RouteConfigLoadStart) {
                NProgress.start();
                NProgress.set(0.4);
            } else if (obj instanceof RouteConfigLoadEnd) {
                NProgress.set(0.9);
                setTimeout(() => {
                    NProgress.done();
                    NProgress.remove();
                }, 500);
            } else if (obj instanceof NavigationEnd) {
                this.cmnSrv.dashboardState.navbarToggle = false;
                this.cmnSrv.dashboardState.sidebarToggle = true;
                window.scrollTo(0, 0);
            }
        });
        this.innerHeight = (window.innerHeight - 250);
        this.innercontainer = this.innerHeight.toString() + "px";

        this.activateRouter.params.subscribe(params => {
            this.pageName = params['pageName'];
            //alert(this.pageName)
            //this.LoadPage(this.pageName);
        });
    }

    ngAfterViewChecked() {
        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }

    reload() { window.location.reload();}

    LoadPage(pagename:any) {
        if (pagename = "powerBi-financial") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=a0c2af29-91dd-4933-947d-f95f8e311359&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        }
        else if (pagename = "bol-retention") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=39d72b2b-a283-4dde-857c-c0c279b6623e&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        } else if (pagename = "cfd-retention") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=98e26a8f-d370-4572-b905-e69039abacb8&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        } else if (pagename = "production-tracker") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=0cc7bfaf-417d-4c7e-affe-d79194b96c37&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        } else if (pagename = "revenue-prediction") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=9443eae6-963d-428c-8961-9b227289d9c7&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        } else if (pagename = "top-bottom") {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=e704bc90-e059-4ce3-8e4d-73cec28eb77c&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        }
        else {
            this.url = "https://app.powerbi.com/reportEmbed?reportId=a0c2af29-91dd-4933-947d-f95f8e311359&autoAuth=true&ctid=d7401adc-cd72-4c8b-9911-89cc1844162b";
        }
    }
   

   

   

   
    

}
