import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { MultiSelectModule } from 'primeng/multiselect';
import { MatTabsModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import { FullCalendarModule } from '@fullcalendar/angular';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';

import { PowerbifinancialRoutingModule } from './Powerbifinancial-routing.module';
import {PowerbiFinDashboadComponent } from '../PowerBIFinancials/PowerBi-Fin-Dashboard/powerbi-fin-dashboad.component';

@NgModule({
  imports: [SharedModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    FullCalendarModule,
    DialogModule,
        PowerbifinancialRoutingModule,
    FormsModule, ReactiveFormsModule,
    ToastModule,
    ButtonModule,
    MultiSelectModule,
    MatTabsModule,
    ChartModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [PowerbiFinDashboadComponent],
  exports: []
})
export class PowerbifinancialModule { }
