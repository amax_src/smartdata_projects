import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { BoundQuotesComponent } from './BoundQuotes.component';
import { BoundQuotesRoutingModule } from './BoundQuotes-routing.module';

import { AccordionModule } from 'primeng/accordion';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    BoundQuotesRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    AccordionModule,
    ChartModule,
  
  ],
  declarations: [BoundQuotesComponent],
  exports: []
 })
export class BoundQuotesModule { }
