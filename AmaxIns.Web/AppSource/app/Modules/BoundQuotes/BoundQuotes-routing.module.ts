import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { BoundQuotesComponent } from './BoundQuotes.component';

const routes: Routes = [
  { path: 'BoundQuotes', component: BoundQuotesComponent}//, canActivate: [LoginGuard], 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BoundQuotesRoutingModule { }
