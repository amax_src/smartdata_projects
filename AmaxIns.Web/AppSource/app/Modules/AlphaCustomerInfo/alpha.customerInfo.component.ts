import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { SelectItem } from 'primeng/api';
import { CustomerInfoService } from '../Services/customerInfoService';
import { custmoreInfoUserModel } from '../../model/custmoreInfoUserModel';
import { CustomerInfo } from '../../model/custmoreInfo';


@Component({
  selector: 'app-alpha-customerInfo',
  templateUrl: './alpha.customerInfo.component.html',
  styleUrls: ['./alpha.customerInfo.component.scss']
})

export class AphaCustomerInfoComponent implements OnInit, AfterViewChecked {
  PageName: string = "CustomerInfo";

  pager: any = {};
  pagedItems: any[] = [];

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  Customer_Info: Array<CustomerInfo> = new Array<CustomerInfo>();

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = new Date().getFullYear().toString();
  showLoader: boolean = true;
  Agencies: Array<string>;
  AgencySelectItem: SelectItem[] = [];
  selectedAgency: string = '';

  Policies: Array<string>;
  policySelectItem: SelectItem[] = [];
  selectedPolicy: string = '';


  cities: Array<string>;
  selectedCity: string = '';
  CitySelectItem: SelectItem[] = [];

  ClientNameResults: Array<string> = new Array<string>();
  ClientName: string = '';

  PolicyNumberResults: Array<string> = new Array<string>();
  PolicyNumber: string = '';

  userModel: custmoreInfoUserModel = new custmoreInfoUserModel();

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private _pageservice: PageService, private _service: CustomerInfoService) {
    this.yearsSelectItem.push({
      label: new Date().getFullYear().toString(), value: new Date().getFullYear().toString()
    })
    this.yearsSelectItem.push({
      label: (new Date().getFullYear() - 1).toString(), value: (new Date().getFullYear() - 1).toString()
    })

    this.GetAgencyName();
   // this.GetPolicyType();
   // this.GetCity();

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  
  onYearchange() {
    this.GetCustomerInfoData();
  }

  onAgencyChange() {
    this.GetCustomerInfoData();
  }

  onPolicyChange() {
    this.GetCustomerInfoData();
  }

  ClientNameSelected() {
    this.GetCustomerInfoData();
  }

  PolicyNumberSelected() {
    this.GetCustomerInfoData();
  }

  onCityChange() {
    this.GetCustomerInfoData();
  }

  GetAgencyName() {
    this._service.getAgencyName().subscribe(m => {
      this.Agencies = m;
      let firstRecord: boolean = true;
      this.Agencies.forEach(z => {
        if (firstRecord) {
          this.selectedAgency = z;
          firstRecord = false;
        }
        this.AgencySelectItem.push({
          label: z, value: z
        })
      })

      this.GetPolicyType();
    })
  }

  GetPolicyType() {
    this._service.getPolicyType().subscribe(m => {
      this.Policies = m;
      let firstRecord: boolean = true;
      this.Policies.forEach(z => {
        if (z !== "" && z !== null) {
          if (firstRecord) {
            this.selectedPolicy = z;
            firstRecord = false;
          }

          this.policySelectItem.push({
            label: z, value: z
          })
        }
      })
      this.GetCity();
    })
  }

  GetCity() {
    this._service.getCity().subscribe(m => {
      this.cities = m;

      let firstRecord: boolean = true;
      this.cities.forEach(z => {
        if (z !== "" && z !== null) {
          if (firstRecord) {
            this.selectedCity = z;
            firstRecord = false;
          }

          this.CitySelectItem.push({
            label: z, value: z
          })
        }
      })

      this.GetCustomerInfoData();
    })


  }

  searchClient(e) {
    this._service.getClients(e.query).then(data => {
      this.ClientNameResults = data;
    });
    
  }


  searchPolicyNumber(e) {
    this._service.GetPolicyNumber(e.query).then(data => {
      this.PolicyNumberResults = data;
    });

  }


  GetCustomerInfoData() {
    this.pager = {};
    this.pagedItems= [];
    this.showLoader = true;
    this.userModel.agency = this.selectedAgency;
    this.userModel.city = this.selectedCity;
    this.userModel.clientName = this.ClientName;
    this.userModel.policyNumber = this.PolicyNumber;
    this.userModel.policyType = this.selectedPolicy
    this._service.GetCustomerInfoData(this.selectedyear, this.userModel).subscribe(m => {
      this.Customer_Info = m;
      this.showLoader = false;
      this.setPage(1);
    })
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.pager = this._pageservice.getPager(this.Customer_Info.length, page);

    // get current page of items
    this.pagedItems = this.Customer_Info.slice(this.pager.startIndex, this.pager.endIndex + 1);

  }
}
