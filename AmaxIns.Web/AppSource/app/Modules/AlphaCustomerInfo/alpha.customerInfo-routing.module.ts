import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AlphaLoginGuard } from '../../login/alpha-login-guard.service';
import { AphaCustomerInfoComponent } from './alpha.customerInfo.component';


const routes: Routes = [
  { path: 'CustomerInfo', component: AphaCustomerInfoComponent, canActivate: [AlphaLoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AlphaCustomerInfoRoutingModule { }
