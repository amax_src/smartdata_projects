import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AphaCustomerInfoComponent } from './alpha.customerInfo.component';
import { AlphaCustomerInfoRoutingModule } from './alpha.customerInfo-routing.module';




@NgModule({
  imports: [SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    AlphaCustomerInfoRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
    AutoCompleteModule,
  ],
  declarations: [AphaCustomerInfoComponent],
  exports: []
 })
export class AplhaCustomerInfoModule { }
