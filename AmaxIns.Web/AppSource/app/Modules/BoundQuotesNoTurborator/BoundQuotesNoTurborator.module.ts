import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { AccordionModule } from 'primeng/accordion';
import { BoundQuotesNoTurboratorComponent } from './BoundQuotesNoTurborator.component';
import { BoundQuotesNoTurboratorRoutingModule } from './BoundQuotesNoTurborator-routing.module';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    BoundQuotesNoTurboratorRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    AccordionModule,
    ChartModule,
  
  ],
  declarations: [BoundQuotesNoTurboratorComponent],
  exports: []
 })
export class BoundQuotesNoTurboratorModule { }
