import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BoundQuotesNoTurboratorComponent } from './BoundQuotesNoTurborator.component';

const routes: Routes = [
  { path: 'BoundQuotesNoTurborator', component: BoundQuotesNoTurboratorComponent}//, canActivate: [LoginGuard], 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BoundQuotesNoTurboratorRoutingModule { }
