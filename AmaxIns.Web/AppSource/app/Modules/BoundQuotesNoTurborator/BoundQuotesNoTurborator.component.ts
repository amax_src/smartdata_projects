import { Component, OnInit, Renderer2 } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../core/common.service';

import { DateService } from '../Services/dateService';
import * as NProgress from 'nprogress';

import { QuotesService } from '../Services/quotesService';
import { QuotesCountModel } from '../../model/QuotesCountModel';
import { PageService } from '../Services/pageService';

import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';


@Component({
  selector: 'BoundQuotesNoTurborator-component',
  templateUrl: './BoundQuotesNoTurborator.component.html',
  styleUrls: ['./BoundQuotesNoTurborator.component.scss']
})
export class BoundQuotesNoTurboratorComponent implements OnInit {

  PageName: string = "Bound Quotes No Turborator";
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  date: Array<string>;
  dateSelectItem: SelectItem[] = [];

  showLoader: boolean = true;

  selectmonthindex: number = 0;
  selectedDate: string;
  //Quotes: Array<QuotesModel> = new Array<QuotesModel>()
  QuotesCount: Array<QuotesCountModel> = new Array<QuotesCountModel>()
  pager: any = {};
  pagedItems: any[];
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
   
  constructor(private dateservice: DateService, private _pageservice: PageService, private router: Router, private quotesService: QuotesService, private renderer: Renderer2, public cmnSrv: CommonService, private ApiLoad: ApiLoadTimeService) {
  }

  ngOnInit() {
    this.getYears();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    this.filterData()
  }

  onYearchange(e) {
    this.getMonth();
    this.filterData();
  }

  onMonthchange(e) {
    this.showLoader = true;
    this.getDate();
    this.filterData();
  }


  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }


  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.dateservice.getMonth(this.selectedyear).toPromise().then(m => {
      months = m;
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i, value: i
          })
      })

      this.selectedmonth = this.monthSelectItem[this.monthSelectItem.length - 1].value;
      this.getDate()
      this.filterData();
    })
  }

  getDate() {
    this.dateSelectItem = []
    let dt: Array<string> = new Array<string>();
    this.quotesService.getPayDate(this.selectedyear, this.selectedmonth).toPromise().then(m => {
      dt = m;
      //let selectDate: Date;
      let check: boolean = true;
      dt.forEach(i => {
        if (check) {
          this.selectedDate = i;
          check = false;
        }
        this.dateSelectItem.push(
          {
            label: this.getDateString(i), value: i
          })
      })
      this.getBoundQuotes();
    })
  }

  

  getBoundQuotes() {
    this.pager= {};
    this.pagedItems = [];
    let startTime: number = new Date().getTime();
    this.quotesService.GetBoundQuotesNoturborator(this.selectedDate).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Bound Quotes Not In Turborator"
      model.time = ResponseTime

      this.QuotesCount = m;
      this.showLoader = false;
      this.setPage(1);
      this.ApiLoad.SaveApiTime(model).subscribe();
    }, catchError => {
      this.showLoader = false;
      alert("Please choose previous month.");
    })
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  filterData() {

  }

  onDatechange() {
    this.showLoader = true;
    this.getBoundQuotes();
  }


  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this._pageservice.getPager(this.QuotesCount.length, page);

    // get current page of items
    this.pagedItems = this.QuotesCount.slice(this.pager.startIndex, this.pager.endIndex + 1);

  }

}
