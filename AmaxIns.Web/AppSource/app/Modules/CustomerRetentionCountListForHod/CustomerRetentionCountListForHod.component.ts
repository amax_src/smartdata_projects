import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { Location } from '../../model/location';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { DateService } from '../Services/dateService';

import { LazyLoadEvent, MessageService, ConfirmationService, SelectItem } from 'primeng/api';
import { Table } from "primeng/components/table/table";

import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { authenticationService } from '../../login/authenticationService.service';

//import { ExportFileService } from '../Services/ExportFileService';
import { DownloadexcelService } from "./../Services/downloadexcel.service";
import { DatePipe } from '@angular/common';

import { AgencyService } from '../Services/agencyService';
import { Agency } from '../../model/Agency/Agency';

import { CustomerRetentionService } from '../Services/CustomerRetentionService';
import { CustomerRetentionCountForHod } from '../../model/CustomerRetention/CustomerRetentionCountForHod';
import { DueDateFilterTypeEnum } from '../../model/CustomerRetention/DueDateFilterTypeEnum';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';


@Component({
  selector: 'app-CustomerRetentionCountListForHod',
  templateUrl: './CustomerRetentionCountListForHod.component.html',
  styleUrls: ['./CustomerRetentionCountListForHod.component.scss']
})
export class CustomerRetentionCountListForHodComponent implements OnInit, AfterViewChecked {

  PageName: string = "Customer Retention";
  
  showLoader: boolean = false;

  //@ViewChild(Table) dt: Table;
  @ViewChild("dt") dt: Table;

  ColumnList: any[];  

  //CustomerRetentionList: Array<CustomerRetention> = new Array<CustomerRetention>();
  CustomerRetentionCountList: CustomerRetentionCountForHod[];
  totalRecordCount: number = 0;
  loadingTable: boolean = false;

  displayDialog: boolean = false;

  DueDateFilterTypeSelectItemList: SelectItem[] = [];
  
  CustomerRetentionCountList2: CustomerRetentionCountForHod[];
  totalRecordCount2: number = 0;  
  loadingTable2: boolean = false;

  CustomerRetentionCountList3: CustomerRetentionCountForHod[];
  totalRecordCount3: number = 0;  
  loadingTable3: boolean = false;

  CustomerRetentionCountList4: CustomerRetentionCountForHod[];
  totalRecordCount4: number = 0;  
  loadingTable4: boolean = false;

  selectedDueDate: Date;

  years: Array<any> = Array<any>();
  yearSelectItemList: SelectItem[] = [];
  selectedYear: string = "";

  months: Array<any>;
  monthSelectItemList: SelectItem[] = [];
  selectedMonth: string = "";
  selectedMonthIndex: number = 0;
  currentMonth: string = "";
  currentDate: Date;

  date: Array<any>;
  dateSelectItemList: SelectItem[] = [];
  //selectedDate: Array<Date> = new Array<Date>();
  selectedDate: Date;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  isMonthChanged: boolean = true;

  selectedRegionalManager: [];
  selectedzonalManager: []
  selectedlocation: Array<number> = []
  location: Array<number> = []

  currentHodUserId: number = 0;
  currentHodUserName: string = '';

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private _pageservice: PageService, private dateService: DateService,
    private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService,
    private readonly _downloadExcel: DownloadexcelService,  public datepipe: DatePipe,
    private messageService: MessageService, private confirmationService: ConfirmationService,
    private agencyService: AgencyService,
    private customerRetentionService: CustomerRetentionService
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {

    //this.loadDueDateFilterType();

    this.getYears();

    this.selectedDueDate = this.getYesterday(); //new Date();
    this.selectedDate = this.getYesterday();

    this.currentHodUserId = this.authenticationService.userId;
    this.currentHodUserName = this.authenticationService.username;

    //this.showLoader = true;

    this.loadingTable = true;
    this.loadingTable2 = true;
    this.loadingTable3 = true;
    this.loadingTable4 = true;
    
    this.ColumnList = [
      {
        field: 'dueDate', header: 'Due Date', isDate: true, format: 'MM/dd/yyyy', width:'110px' },
      { field: 'regionManagerName', header: 'ROM Name', width:'110px' },
      { field: 'paymentDueCount', header: '# Payments Due', width:'80px' },
      { field: 'paymentReceivedCount', header: '# Payments Received', width:'80px' },
      { field: 'difference', header: 'Difference', width:'80px' }      
    ];

    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });

  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  getToday() {
    let d = new Date();
    d.setDate(d.getUTCDate());
    console.log("getTodacccccy", d);
    console.log("getToday", this.datepipe.transform(d.getDate(), 'MM/dd/yyyy', 'UTC'));
    //return d.toISOString().split('T')[0]; // yesterday(); // 2018-10-17 (if current date is 2018-10-18)
    return d;
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    //this.TabChange();
  }

  loaderStatusChanged(e) {
    // if (this.ischartDataLoaded) {
    //   // this.showLoader = e;
    // }
  }


  onYearChange(e) {
    this.getMonth(this.selectedYear);
    
  }

  onMonthChange(e) {    
    this.isMonthChanged = true;

    //set this.selectmonthindex
    for (let index = 0; index < this.monthNames.length; index++) {
      if(this.monthNames[index] == this.selectedMonth){
        this.selectedMonthIndex = index;
        break;
      };      
    }

    this.getDate();
    
  }

  getYears() {
    this.dateService.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        if (i > 2020) {
          this.yearSelectItemList.push(
            {
              label: i, value: i
            })
        }
      })

      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedYear = _selectedYear;
      this.selectedMonth = "";
      this.getMonth(this.selectedYear);
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth(year: string) {
    this.monthSelectItemList = [];
    let months: Array<string> = new Array<string>();
    this.selectedMonthIndex = 0;
    
    months = this.monthNames;
    months.forEach(i => {
      this.monthSelectItemList.push(
        {
          label: i, value: i
        })
    });

    let monthcount = (new Date()).getMonth();
    this.selectedMonthIndex = monthcount - 1;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectedMonthIndex = x;
        break;
      }
    }

    let _currentyear: string = (new Date()).getFullYear().toString()
    if (year === _currentyear) {
      this.selectedMonth = this.monthNames[this.selectedMonthIndex];
      this.currentMonth = this.selectedMonth;
    }
    else {
      this.selectedMonth = this.monthNames[11];
    }

    this.getDate();

  }

  getDate() {
    this.selectedDate = this.getYesterday();
    this.selectedDueDate = this.getYesterday();
    this.dateSelectItemList = [];
    let dt: Array<Date> = new Array<Date>();

    let lastDayNumber = this.getLastDayOfMonth(Number(this.selectedYear), this.selectedMonthIndex);
    for (let i = 1; i <= lastDayNumber; i++) {
      let dateValue = new Date(Number(this.selectedYear), this.selectedMonthIndex, i);
      this.dateSelectItemList.push(
          {
            label: this.getDateString(this.datepipe.transform(dateValue,'MM/dd/yyyy')), value: dateValue
          });
    }

    // select default date on page load
    //this.selectedDate = this.selectedDate.concat(new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));
    this.selectedDate = (new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));    
    this.currentDate = (new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));
    this.selectedDueDate = (new Date(Number(this.selectedYear), this.selectedMonthIndex, this.selectedDueDate.getDate()));
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    //this.TabChange();
    this.selectedDueDate = this.selectedDate; //new Date();
    
  }
  
  getLastDayOfMonth(year: number, month:number){
    return new Date(year, month + 1, 0).getDate();
  }

  getYesterday() {
    let d = new Date();
    d.setDate(d.getDate() - 1);
    //return d.toISOString().split('T')[0]; // yesterday(); // 2018-10-17 (if current date is 2018-10-18)
    return d;
  };

  loadDueDateFilterType() {
    this.DueDateFilterTypeSelectItemList = [];
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Today', value: '1'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Next 7 Days', value: '2'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Past Due', value: '3'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Canceled', value: '4'
      });
  }

  filterReset() {
    this.child.filterReset();
    this.getMonth('2021');
    this.getDate();

    setTimeout(() => {
      this.Search();
    }, 100);
    
  }


  Search() {

    let selectedAgency;
    selectedAgency = this.location;
    if (this.selectedlocation.length > 0) {
      selectedAgency = this.selectedlocation;
    }

    var _dueDate: any = null;
    var queryModel: any;

      _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');
      queryModel = {
        RowOffset: 0,
        PageSize: 20,
        SortBy: "dueDate",
        SortDir: 'ASC',
        ZoneManagerUserId: this.currentHodUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueToday,
        AgencyIdList: selectedAgency,
        DueDate: _dueDate
      };

      this.loadingTable = true;
      this.loadingTable2 = true;
      this.loadingTable3 = true;
      this.loadCustomerRetentionCountListSearch(queryModel);
      this.loadCustomerRetentionCountListSearch2(queryModel);
      this.loadCustomerRetentionCountListSearch3(queryModel);
  }

  loadCustomerRetentionCountListSearch(queryModel:any) {
    this.loadingTable = true;
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;

      console.log("queryModel", queryModel);
      this.CustomerRetentionCountList = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList = res.data;
        console.log("this.CustomerRetentionCountList", this.CustomerRetentionCountList);
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });

    }, 500);
  }
  loadCustomerRetentionCountListSearch2(queryModel: any) {
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;

      console.log("queryModel", queryModel);
      this.CustomerRetentionCountList2 = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });

    }, 500);
  }
  loadCustomerRetentionCountListSearch3(queryModel: any) {
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;

      console.log("queryModel", queryModel);
      this.CustomerRetentionCountList3 = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });

    }, 500);
  }

  loadCustomerRetentionCountList(event: LazyLoadEvent) {
    this.loadingTable = true;
    setTimeout(() => {

      let selectedAgency;
      selectedAgency = this.location;
      if (this.selectedlocation.length > 0) {
        selectedAgency = this.selectedlocation;
      }
      var _dueDate: any = null;
      _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');
      console.log("location", this.location);
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        HodUserId: this.currentHodUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueToday,
        AgencyIdList: selectedAgency,
        DueDate: _dueDate
      };

      console.log("queryModel", queryModel);
      this.CustomerRetentionCountList = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList = res.data;
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });

    }, 2000);

  }
 
  loadCustomerRetentionCountList2(event: LazyLoadEvent) {
    this.loadingTable2 = true;

    setTimeout(() => {
      let selectedAgency;
      selectedAgency = this.location;
      if (this.selectedlocation.length > 0) {
        selectedAgency = this.selectedlocation;
      }
      var _dueDate: any = null;
      _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');

      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        HodUserId: this.currentHodUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueNext7Days,
        AgencyIdList: selectedAgency,
        DueDate: _dueDate
      };
      this.CustomerRetentionCountList2 = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });

    }, 2000);

  }

  loadCustomerRetentionCountList3(event: LazyLoadEvent) {
    this.loadingTable3 = true;

    setTimeout(() => {

      let selectedAgency;
      selectedAgency = this.location;
      if (this.selectedlocation.length > 0) {
        selectedAgency = this.selectedlocation;
      }
      var _dueDate: any = null;
      _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');

      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        HodUserId: this.currentHodUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.PastDue,
        AgencyIdList: selectedAgency,
        DueDate: _dueDate
      };
      this.CustomerRetentionCountList3 = new Array<CustomerRetentionCountForHod>();
      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });

    }, 2000);

  }

  loadCustomerRetentionCountList4(event: LazyLoadEvent) {
    this.loadingTable4 = true;

    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    // https://www.primefaces.org/primeng/v7.2.6-lts/#/table/lazy
    // https://github.com/primefaces/primeng/blob/master/src/app/showcase/components/table/tablelazydemo.ts

    setTimeout(() => {

      //this.CustomerRetentionService.getAll({ lazyEvent: JSON.stringify(event) }).then(res => {
      //  this.CustomerRetentionList = res.data;
      //  this.totalRecordCount = res.totalRecordCount;
      //  this.loadingTable = false;
      //});
      var _dueDate: any = null;
      _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');

      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "dueDate" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        HodUserId: this.currentHodUserId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.Canceled,
        AgencyIdList: this.location,
        DueDate: _dueDate
      };

      this.customerRetentionService.getAllCountForHodPaging(queryModel).then(res => {
        this.CustomerRetentionCountList4 = res.data;
        this.totalRecordCount4 = res.totalRecordCount;
        this.loadingTable4 = false;
      });

    }, 500);

  }

  public exportToExcel(excelType) {
    let selectedAgency;
    selectedAgency = this.location;
    if (this.selectedlocation.length > 0) {
      selectedAgency = this.selectedlocation;
    }
    var _dueDate: any = null;
    _dueDate = this.datepipe.transform(this.selectedDate, 'MM-dd-yyyy');

    var queryModel = {
      RowOffset: 0,
      PageSize: 1000000,
      SortBy: "dueDate",
      SortDir: 'ASC',
      HodUserId: this.currentHodUserId,
      DueDateFilterTypeId: 0,
      AgencyIdList: selectedAgency,
      DueDate: _dueDate
    };

    if(excelType == 1){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;
    }
    else if(excelType == 2){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;
    }
    else if(excelType == 3){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;
    }

    this.customerRetentionService.getAllCountForHodPagingExport(queryModel).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      this.showLoader = false;
    })

  }

  public exportDataToExcel(excelType, dataList) {
    
    // let reportHeader = { 
    //   dueDate:"Due Date", regionManagerName:"ROM Name", 
    //   paymentDueCount:"# Payments Due", paymentReceivedCount:"# Payments Received", 
    //   difference:"Difference"
    // };

    // let agentReport = this.filteredData.map(agent =>({agentId:agent.agentId,firstName: agent.firstName,lastName:agent.lastName,
    //                                                   email:agent.email, userType: UserTypes[agent.userType],
    //                                                   status:agent.status === 1 ? 'ACTIVE' : 'INACTIVE', 
    //                                                   description:agent.description, userId:agent.userId === null ? 'No' : 'Yes',
    //                                                   createUser:agent.createUser, createDate:this.datepipe.transform(agent.createDate,'MM/dd/yyyy'),
    //                                                   updateUser:agent.updateUser, updateDate:this.datepipe.transform(agent.updateDate, 'MM/dd/yyyy')})
    //                                                 );

    let reportDataList = [];

    let sourceReportDataList = dataList;    

    let mappedDataList = sourceReportDataList.map( item => (
      { 
        dueDate: this.datepipe.transform(item.dueDate,'MM/dd/yyyy'), regionManagerName: item.regionManagerName, 
        paymentDueCount: item.paymentDueCount.toString(), paymentReceivedCount: item.paymentReceivedCount.toString(), 
        difference: item.difference.toString()
      }
    ));

    mappedDataList.map(item => {
      reportDataList.push(item);
    });

    // reportDataList.unshift(reportHeader);

    // var wscols = [
    //   {wch: 10}, 
    //   {wch: 20}, 
    //   {wch: 20}, 
    //   {wch: 20}, 
    //   {wch: 20}
    // ];

    var wscols = [
      { header: 'Due Date', key: 'dueDate', width: 15 },
      { header: 'ROM Name', key: 'regionManagerName', width: 30 },
      { header: '# Payments Due', key: 'paymentDueCount', width: 15 },
      { header: '# Payments Received', key: 'paymentReceivedCount', width: 15 },
      { header: 'Difference', key: 'difference', width: 15 }
    ];

    if (reportDataList.length > 0) {
        //this.exportService.exportExcel(reportDataList,'Customer Retention For Hod', wscols);
        //this.exportService.exportExcelWithColor(reportDataList,'Customer Retention For Hod', wscols);
    }

  }


}
