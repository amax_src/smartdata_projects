import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CustomerRetentionCountListForHodComponent } from './CustomerRetentionCountListForHod.component';


const routes: Routes = [
  { path: 'CustomerRetentionCountListForHod', component: CustomerRetentionCountListForHodComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomerRetentionCountListForHodRoutingModule { }
