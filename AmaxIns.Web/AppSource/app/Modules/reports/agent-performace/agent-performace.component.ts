import { Component, OnInit, Renderer2, HostListener } from "@angular/core";
import { SelectItem } from "primeng/api";
import { DateService } from "../../Services/dateService";
import {
  Router,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
  NavigationEnd,
} from "@angular/router";
import { CommonService } from "../../../core/common.service";
import * as NProgress from "nprogress";
import { ReportService } from "../../Services/ReportService";
import { AgentPerformace, Months } from "../../../model/Reports";
import { DownloadexcelService } from '../../Services/downloadexcel.service';
@Component({
  selector: "app-agent-performace",
  templateUrl: "./agent-performace.component.html",
  styleUrls: ["./agent-performace.component.scss"],
})
export class AgentPerformaceComponent implements OnInit {
  AgentPerformaceI: Array<AgentPerformace>;
  AgentPerformaceII: Array<AgentPerformace>;

  filteredAgentPerformaceI: Array<AgentPerformace>;
  filteredAgentPerformaceII: Array<AgentPerformace>;

  selectedRegionalManager: [];
  selectedzonalManager: [];
  showLoader: boolean = true;
  selectedlocation: Array<number> = [];
  location: Array<number> = [];

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<string> = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = [];

  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;

  previousYearLength: boolean = false;
  previousMonthLength: boolean = false;


  monthNames: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  innerHeight: number = 0;
  innercontainer: string = "400px";

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
  { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
  { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
    { monthId: '10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  constructor(
    private dateservice: DateService,
    private router: Router,
    private renderer: Renderer2,
    public cmnSrv: CommonService,
    public reportService: ReportService,
    private _downloadExcel: DownloadexcelService
  ) {}

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabSelectedIndex = 0;
    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 320);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetAgentPerformaceI();
    } else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.GetAgentPerformaceII();
    }
  }

  GetAgentPerformaceI() {
    this.showLoader = true;
    this.reportService
        .GetAgentPerformaceI(this.selectedyear, this.selectedmonth, this.location)
      .subscribe((x) => {
        this.AgentPerformaceI = x;
        console.log(this.AgentPerformaceI, "Renter");
        this.filterAgentPerformaceI();
        this.showLoader = false;
      });
  }

  GetAgentPerformaceII() {
    this.showLoader = true;
    this.reportService
        .GetAgentPerformaceII(this.selectedyear, this.selectedmonth, this.location)
      .subscribe((x) => {
        this.AgentPerformaceII = x;
        console.log(this.AgentPerformaceII, "Production");
        this.filterAgentPerformaceII();
        this.showLoader = false;
      });
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedzonalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAgentPerformaceI();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterAgentPerformaceII();
    //}
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedRegionalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAgentPerformaceI();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterAgentPerformaceII();
    //}
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAgentPerformaceI();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterAgentPerformaceII();
    //}
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach((i) => {
      this.location.push(i.agencyId);
    });
      this.getYears();
  }
  onYearchange(e) {
    this.previousYearLength = true;
    this.getMonth();
  }
  loaderStatusChanged(e) {}

  onMonthchange(e) {
    this.previousMonthLength = true;
    //if (this.tabSelectedIndex == 0) {
    //  this.GetAgentPerformaceI();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.GetAgentPerformaceII();
    //}
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear.push(_selectedYear);
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.selectedmonth = new Array<string>();

    this.listOfMonths.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i.monthName, value: i.monthId
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = this.listOfMonths.filter(m => m.monthName === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.currentmonth = this.listOfMonths[this.selectmonthindex].monthId;
    this.selectedmonth.push(this.listOfMonths[this.selectmonthindex].monthId)

      if (this.tabSelectedIndex == 0) {
        this.GetAgentPerformaceI();
      } else if (this.tabSelectedIndex == 1) {
        this.GetAgentPerformaceII();
      }
  }

  filterAgentPerformaceI() {
    this.filteredAgentPerformaceI = new Array<AgentPerformace>();
    let tempFilterData = new Array<AgentPerformace>();
    let RegionalManager = new Array<AgentPerformace>();
    let zonalManager = new Array<AgentPerformace>();
    let agencyName = new Array<AgentPerformace>();


    tempFilterData = this.AgentPerformaceI;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.AgentPerformaceI.filter(m => m.regionalManagersId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zonalManagersId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    console.log("zonalManager", zonalManager);
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyID === i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    }
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }

    this.filteredAgentPerformaceI = tempFilterData;
  }

  filterAgentPerformaceII() {
    console.log("filterAgentPerformaceII");
    this.filteredAgentPerformaceII = new Array<AgentPerformace>();
    let tempFilterData = new Array<AgentPerformace>();
    let RegionalManager = new Array<AgentPerformace>();
    let zonalManager = new Array<AgentPerformace>();
    let agencyName = new Array<AgentPerformace>();

    tempFilterData = this.AgentPerformaceII;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.AgentPerformaceII.filter(m => m.regionalManagersId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zonalManagersId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    console.log("zonalManager", zonalManager);

    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyID === i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    }
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<AgentPerformace>();
    }

    this.filteredAgentPerformaceII = tempFilterData;
  }

  SearchReport() {
    console.log("this.tabSelectedIndex", this.tabSelectedIndex);

    if (this.previousYearLength == true || this.previousMonthLength == true) {

      this.previousYearLength = false;
      this.previousMonthLength = false;

      if (this.tabSelectedIndex == 0) {
        this.GetAgentPerformaceI();
      } else if (this.tabSelectedIndex == 1) {
        this.GetAgentPerformaceII();
      }
     
    }
    else {
      if (this.tabSelectedIndex == 0) {
        console.log("filterAgentPerformaceIdd");
        this.filterAgentPerformaceI();
      }
      else if (this.tabSelectedIndex == 1) {
        console.log("filterAgentPerformaceIIdd");
        this.filterAgentPerformaceII();
      }
     
    }
  }

  exportExcel() {
    this.showLoader = true;

    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportAgentPerformaceI(this.filteredAgentPerformaceI).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExporttAgentPerformaceII(this.filteredAgentPerformaceII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
  }
}
