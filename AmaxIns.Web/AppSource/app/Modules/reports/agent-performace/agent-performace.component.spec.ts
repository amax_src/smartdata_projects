import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentPerformaceComponent } from './agent-performace.component';

describe('AgentPerformaceComponent', () => {
  let component: AgentPerformaceComponent;
  let fixture: ComponentFixture<AgentPerformaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentPerformaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentPerformaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
