
import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { SelectItem } from 'primeng/api'
import { DateService } from '../../Services/dateService';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { ReportService } from '../../Services/ReportService';
import { AvgAgencyFee, Overtime, Months } from '../../../model/Reports';
import { DownloadexcelService } from '../../Services/downloadexcel.service';

@Component({
  selector: 'app-overtime',
  templateUrl: './overtime.component.html',
  styleUrls: ['./overtime.component.scss']
})
export class OvertimeComponent implements OnInit {
  OvertimeI: Array<Overtime>;
  OvertimeII: Array<Overtime>;
  OvertimeIII: Array<Overtime>;

  filteredOvertimeI: Array<Overtime>;
  filteredOvertimeII: Array<Overtime>;
  filteredOvertimeIII: Array<Overtime>;


  filterOvertime: Array<Overtime>;
  selectedRegionalManager: [];
  selectedzonalManager: []
  showLoader: boolean = true;
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<string> = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = [];
  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;
  rowcounter: number = 0;
  bgcolor: string = '';
  previousZMId: number = 0;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  innerHeight: number = 0;
  innercontainer: string = "400px";

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
  { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
  { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
  { monthId: '10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  previousYearLength: boolean = false;
  previousMonthLength: boolean = false;
  constructor(private dateservice: DateService, private router: Router, private renderer: Renderer2
    , public cmnSrv: CommonService, public reportService: ReportService, private _downloadExcel: DownloadexcelService) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabSelectedIndex = 0;
    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetOvertimesI();
    }
    else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.GetOvertimesII();
    }
    else if (_index == 2) {
      this.tabSelectedIndex = _index;
      this.GetOvertimesIII();
    }
  }

  GetOvertimesI() {
    this.showLoader = true;
      this.reportService.GetOvertimeI(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.OvertimeI = x;
      this.filterOvertimesDataI();
      this.showLoader = false;
    })
  }

  GetOvertimesII() {
    this.showLoader = true;
      this.reportService.GetOvertimeII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.OvertimeII = x;
      this.filterOvertimesDataII();
      this.showLoader = false;
    })
  }

  GetOvertimesIII() {
    this.showLoader = true;
      this.reportService.GetOvertimeIII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.OvertimeIII = x;
      this.filterOvertimesDataIII();
      this.showLoader = false;
    })
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterOvertimesDataI();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterOvertimesDataII();
    //}
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterOvertimesDataI();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterOvertimesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterOvertimesDataIII();
    //}
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterOvertimesDataI();
    //}
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
      this.getYears();
  }
  onYearchange(e) {
    //this.getMonth();
    this.previousYearLength = true;
  }
  loaderStatusChanged(e) {

  }

  onMonthchange(e) {
    this.previousMonthLength = true;
    //if (this.tabSelectedIndex == 0) {
    //  this.GetOvertimesI();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.GetOvertimesII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.GetOvertimesIII();
    //}
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear.push(_selectedYear);
      this.previousYearLength = false;
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.selectedmonth = new Array<string>();

    this.listOfMonths.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i.monthName, value: i.monthId
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = this.listOfMonths.filter(m => m.monthName === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.currentmonth = this.listOfMonths[this.selectmonthindex].monthId;
    this.selectedmonth.push(this.listOfMonths[this.selectmonthindex].monthId)
    //this.selectedmonth.push('7')
    this.previousMonthLength = false;
      if (this.tabSelectedIndex == 0) {
        this.GetOvertimesI();
      }
      else if (this.tabSelectedIndex == 1) {
        this.GetOvertimesII();
      }
      else if (this.tabSelectedIndex == 2) {
        this.GetOvertimesIII();
      }
  }

  filterOvertimesDataI() {
    this.filteredOvertimeI = new Array<Overtime>();
    let tempFilterData = new Array<Overtime>();
    let RegionalManager = new Array<Overtime>();
    let zonalManager = new Array<Overtime>();
    let agencyName = new Array<Overtime>();
    
    tempFilterData = this.OvertimeI;
    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.OvertimeI.filter(m => parseInt(m.regionalManagersId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } 
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.zonalManagersId) === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    } 
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.agencyNameID) === i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    } 
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<Overtime>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<Overtime>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<Overtime>();
    }
    this.filteredOvertimeI = tempFilterData;
  }

  filterOvertimesDataII() {
    this.filteredOvertimeII = new Array<Overtime>();
    let tempFilterData = new Array<Overtime>();
    let RegionalManager = new Array<Overtime>();
    let zonalManager = new Array<Overtime>();
    tempFilterData = this.OvertimeII;
    

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.OvertimeII.filter(m => parseInt(m.regionalManagersId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.zonalManagersId) === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    } 
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<Overtime>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<Overtime>();
    }

    this.filteredOvertimeII = tempFilterData;
  }

  filterOvertimesDataIII() {
    this.filteredOvertimeIII = new Array<Overtime>();
    let tempFilterData = new Array<Overtime>();
    let RegionalManager = new Array<Overtime>();
    tempFilterData = this.OvertimeIII;
   
    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.OvertimeIII.filter(m => parseInt(m.regionalManagersId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } 
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<Overtime>();
    }

    this.filteredOvertimeIII = tempFilterData;
  }

  SearchReport() {
    if (this.previousYearLength ==true || this.previousMonthLength ==true) {

      this.previousYearLength = false;
      this.previousMonthLength = false;

      if (this.tabSelectedIndex == 0) {
        this.GetOvertimesI();
      }
      else if (this.tabSelectedIndex == 1) {
        this.GetOvertimesII();
      }
      else if (this.tabSelectedIndex == 2) {
        this.GetOvertimesIII();
      }
    }
    else {
      if (this.tabSelectedIndex == 0) {
        this.filterOvertimesDataI();
      }
      else if (this.tabSelectedIndex == 1) {
        this.filterOvertimesDataII();
      }
      else if (this.tabSelectedIndex == 2) {
        this.filterOvertimesDataIII();
      }
    }
  }

  exportExcel() {
    this.showLoader = true;

    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportExcelOverTimeByOfficeI(this.filteredOvertimeI).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExportExcelOverTimeZM(this.filteredOvertimeII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 2) {
      this.reportService.ExportExcelOverTimeROM(this.filteredOvertimeIII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
  }

  rowbgColor(zmId) {

    this.bgcolor = '';
    if (this.previousZMId != parseInt(zmId)) {
      this.previousZMId = parseInt(zmId);
      this.rowcounter++;
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }
    else if (this.previousZMId == parseInt(zmId)) {
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }

  }
}
