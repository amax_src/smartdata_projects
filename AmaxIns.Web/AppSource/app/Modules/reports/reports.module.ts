import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'

import { ReportsRoutingModule } from './reports-routing.module';
import { AvgAgencyFeeComponent } from './avg-agency-fee/avg-agency-fee.component';
import { AgentPerformaceComponent } from './agent-performace/agent-performace.component';
import { UniqueCreatedQuotesComponent } from './unique-created-quotes/unique-created-quotes.component';
import { OvertimeComponent } from './overtime/overtime.component';
import { CarrierMixReportComponent } from './carrier-mix/carrier-mix-report.component';
import { TrackerComponent } from './tracker/tracker.component';
import { AgencyBreakdownComponent } from './agency-breakdown/agency-breakdown.component';
import { RentersReportComponent } from './renters-report/renters-report.component';
import { ActiveCustomersComponent } from './active-customers/active-customers.component';
import { CarrierPremiumComponent } from './carrier-premium/carrier-premium.component';

@NgModule({
  declarations: [AvgAgencyFeeComponent, AgentPerformaceComponent, UniqueCreatedQuotesComponent, OvertimeComponent, CarrierMixReportComponent, TrackerComponent, AgencyBreakdownComponent, RentersReportComponent, ActiveCustomersComponent, CarrierPremiumComponent],
  imports: [
    SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
