import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvgAgencyFeeComponent } from './avg-agency-fee.component';

describe('AvgAgencyFeeComponent', () => {
  let component: AvgAgencyFeeComponent;
  let fixture: ComponentFixture<AvgAgencyFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvgAgencyFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvgAgencyFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
