import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { SelectItem } from 'primeng/api'
import { DateService } from '../../Services/dateService';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { ReportService} from '../../Services/ReportService';
import { AvgAgencyFee, Months } from '../../../model/Reports';
import { DownloadexcelService } from '../../Services/downloadexcel.service';
@Component({
  selector: 'app-avg-agency-fee',
  templateUrl: './avg-agency-fee.component.html',
  styleUrls: ['./avg-agency-fee.component.scss']
})
export class AvgAgencyFeeComponent implements OnInit {
  avgAgencyFee: Array<AvgAgencyFee>;
  avgAgencyFeeII: Array<AvgAgencyFee>;
  avgAgencyFeeIII: Array<AvgAgencyFee>;

  filteredAvgAgencyFeeII: Array<AvgAgencyFee>;
  filteredAvgAgencyFeeIII: Array<AvgAgencyFee>;


  filteravgAgencyFee: Array<AvgAgencyFee>;
  filteredAvgAgencyFee: Array<AvgAgencyFee>;
  selectedRegionalManager: [];
  selectedzonalManager: []
  showLoader: boolean = true;
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<string> = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = [];
  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;
  listreport: any;
  previousZMId: number = 0;
  rowcounter: number =0;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
  { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
  { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
    { monthId: '10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  innerHeight: number = 0;
  innercontainer: string = "400px";
  previousYear: string = '';

  constructor(private dateservice: DateService, private router: Router, private renderer: Renderer2
    , public cmnSrv: CommonService, public reportService: ReportService, private _downloadExcel: DownloadexcelService) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabSelectedIndex = 0;
   
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetAvgAgencyFees();
    }
    else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.GetAvgAgencyFeesII();
    }
    else if (_index == 2) {
      this.tabSelectedIndex = _index;
      this.GetAvgAgencyFeesIII();
    }
  }

  GetAvgAgencyFees() {
    this.showLoader = true;
      this.reportService.GetAvgAgencyFees(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.avgAgencyFee = x;
      this.filterAvgAgencyFeesData();
      
    })
  }

  GetAvgAgencyFeesII() {
    this.showLoader = true;
      this.reportService.GetAvgAgencyFeesII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.avgAgencyFeeII = x;
      this.filterAvgAgencyFeesDataII();
      this.showLoader = false;
    })
  }

  GetAvgAgencyFeesIII() {
    this.showLoader = true;
      this.reportService.GetAvgAgencyFeesIII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.avgAgencyFeeIII = x;
      this.filterAvgAgencyFeesDataIII();
      this.showLoader = false;
    })
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
    

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAvgAgencyFeesData();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterAvgAgencyFeesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterAvgAgencyFeesDataIII();
    //}

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAvgAgencyFeesData();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterAvgAgencyFeesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterAvgAgencyFeesDataIII();
    //}
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
      this.getYears();
  }
  onYearchange(e) {
    this.selectedmonth = [];
    //if (this.tabSelectedIndex == 0) {
    //  this.GetAvgAgencyFees();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.GetAvgAgencyFeesII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.GetAvgAgencyFeesIII();
    //}
    
  }
  loaderStatusChanged(e) {
    
  }

  onMonthchange(e) {
    //if (this.selectedmonth.length == 0) {
    //  this.selectedmonth.push(this.currentmonth);
    //}
    //if (this.tabSelectedIndex == 0) {
    //  this.filterAvgAgencyFeesData();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterAvgAgencyFeesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterAvgAgencyFeesDataIII();
    //}
    
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear.push(_selectedYear);
      this.previousYear = _selectedYear;
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.selectedmonth = new Array<string>();

    this.listOfMonths.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i.monthName, value: i.monthId
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = this.listOfMonths.filter(m => m.monthName === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.currentmonth = this.listOfMonths[this.selectmonthindex].monthId;
    this.selectedmonth.push(this.listOfMonths[this.selectmonthindex].monthId)

    if (this.tabSelectedIndex == 0) {
      this.GetAvgAgencyFees();
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetAvgAgencyFeesII();
    }
    else if (this.tabSelectedIndex == 2) {
      this.GetAvgAgencyFeesIII();
    }
  }

  filterAvgAgencyFeesData() {
   
    this.filteredAvgAgencyFee = new Array<AvgAgencyFee>();
    let tempFilterData = new Array<AvgAgencyFee>();

    let monthdatafilter = new Array<AvgAgencyFee>();
    let RegionalManager = new Array<AvgAgencyFee>();
    let zonalManager = new Array<AvgAgencyFee>();
    let locations = new Array<AvgAgencyFee>();
    tempFilterData = this.avgAgencyFee;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zmId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    } 
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyId === i);
        filterdata.forEach(m => {
          locations.push(m);
        })
      })
    }
    if (locations && locations.length > 0) {
      tempFilterData = locations;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && locations && locations.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }
    this.filteredAvgAgencyFee = tempFilterData;
    
    setTimeout(() => {                           // <<<---using ()=> syntax
      this.showLoader = false;
    }, 100);
  }


  filterAvgAgencyFeesDataII() {
    this.showLoader = true;
    this.filteredAvgAgencyFeeII = new Array<AvgAgencyFee>();
    let tempFilterData = new Array<AvgAgencyFee>();

    let monthdatafilter = new Array<AvgAgencyFee>();
    let RegionalManager = new Array<AvgAgencyFee>();
    let zonalManager = new Array<AvgAgencyFee>();
    tempFilterData = this.avgAgencyFeeII;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } 
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zmId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

     if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }

    this.filteredAvgAgencyFeeII = tempFilterData;
    setTimeout(() => {                           // <<<---using ()=> syntax
      this.showLoader = false;
    }, 100);
  }

  filterAvgAgencyFeesDataIII() {
    this.showLoader = true;
    this.filteredAvgAgencyFeeIII = new Array<AvgAgencyFee>();
    let tempFilterData = new Array<AvgAgencyFee>();

    let monthdatafilter = new Array<AvgAgencyFee>();
    let RegionalManager = new Array<AvgAgencyFee>();
    tempFilterData = this.avgAgencyFeeIII;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } 
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

     if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AvgAgencyFee>();
    }

    this.filteredAvgAgencyFeeIII = tempFilterData;
    setTimeout(() => {                           // <<<---using ()=> syntax
      this.showLoader = false;
    }, 100);
  }

  SearchReport() {
    if (this.tabSelectedIndex == 0) {
      this.GetAvgAgencyFees();
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetAvgAgencyFeesII();
    }
    else if (this.tabSelectedIndex == 2) {
      this.GetAvgAgencyFeesIII();
    }
  }

  exportExcel() {
    this.showLoader = true;
    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportAgencyFeeI(this.filteredAvgAgencyFee).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExportAgencyFeeII(this.filteredAvgAgencyFeeII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 2) {
      this.reportService.ExportAgencyFeeIII(this.filteredAvgAgencyFeeIII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    
  }

  rowbgColor(zmId) {
  
    let bgcolor = '';
    if (this.previousZMId != zmId) {
      this.previousZMId = zmId;
      this.rowcounter++;
      if (this.rowcounter % 2 == 0) {
        bgcolor = '#4d5668'
      }
      else {
        bgcolor = '#5e7385';
      }
      return bgcolor
    }
    else if (this.previousZMId == zmId ) {
      if (this.rowcounter % 2 == 0) {
        bgcolor = '#4d5668'
      }
      else {
        bgcolor = '#5e7385';
      }
      return bgcolor
    }
   
  }

}
