import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierPremiumComponent } from './carrier-premium.component';

describe('CarrierPremiumComponent', () => {
  let component: CarrierPremiumComponent;
  let fixture: ComponentFixture<CarrierPremiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrierPremiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierPremiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
