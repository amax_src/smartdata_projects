import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyBreakdownComponent } from './agency-breakdown.component';

describe('AgencyBreakdownComponent', () => {
  let component: AgencyBreakdownComponent;
  let fixture: ComponentFixture<AgencyBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
