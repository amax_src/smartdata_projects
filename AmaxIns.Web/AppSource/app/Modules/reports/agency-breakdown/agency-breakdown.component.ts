import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { SelectItem } from 'primeng/api'
import { DateService } from '../../Services/dateService';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { ReportService } from '../../Services/ReportService';
import { DownloadexcelService } from '../../Services/downloadexcel.service';
import { AvgAgencyFee, AgencyBreakdown, Months } from '../../../model/Reports';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-agency-breakdown',
  templateUrl: './agency-breakdown.component.html',
  styleUrls: ['./agency-breakdown.component.scss']
})
export class AgencyBreakdownComponent implements OnInit {
  AgentPerformaceI: Array<AgencyBreakdown>;
  AgentPerformaceII: Array<AgencyBreakdown>;
  AgentPerformaceIII: Array<AgencyBreakdown>;

  filteredAgentPerformaceI: Array<AgencyBreakdown>;
  filteredAgentPerformaceII: Array<AgencyBreakdown>;
  filteredAgentPerformaceIII: Array<AgencyBreakdown>;


  filterAgentPerformace: Array<AgencyBreakdown>;
  selectedRegionalManager: [];
  selectedzonalManager: []
  showLoader: boolean = true;
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<string> = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = [];

  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;

  previousYearLength: number=0;
  previousMonthLength: number = 0;
  previousZMId: number = 0;
  rowcounter: number = 0;
  bgcolor: string = '';

  PreviousZM: string = '';
  zmBGColor: string = '';
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  innerHeight: number = 0;
  innercontainer: string = "400px";

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
    { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
    { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
    { monthId:'10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  

  constructor(private dateservice: DateService, private router: Router, private renderer: Renderer2
    , public cmnSrv: CommonService, public reportService: ReportService, private _downloadExcel: DownloadexcelService) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabSelectedIndex = 0;
    
   
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetAgentPerformacesI();
    }
    else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.GetAgentPerformacesII();
    }
    else if (_index == 2) {
      this.tabSelectedIndex = _index;
      this.GetAgentPerformacesIII();
    }
  }

  GetAgentPerformacesI() {
    this.showLoader = true;
      this.reportService.GetAgencyBreakdownI(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.AgentPerformaceI = x;
      this.filterAgentPerformacesDataI();
      this.showLoader = false;

    })
  }


  GetAgentPerformacesII() {
    this.showLoader = true;
      this.reportService.GetAgencyBreakdownII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.AgentPerformaceII = x;
      this.filterAgentPerformacesDataII();
      this.showLoader = false;
    })
  }

  GetAgentPerformacesIII() {
    this.showLoader = true;
      this.reportService.GetAgencyBreakdownIII(this.selectedyear, this.selectedmonth, this.location).subscribe(x => {
      this.AgentPerformaceIII = x;
      this.filterAgentPerformacesDataIII();
      this.showLoader = false;
    })
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAgentPerformacesDataI();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterAgentPerformacesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterAgentPerformacesDataIII();
    //}

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterAgentPerformacesDataI();
    //}
    //else if (this.tabSelectedIndex == 1) {
    //  this.filterAgentPerformacesDataII();
    //}
    //else if (this.tabSelectedIndex == 2) {
    //  this.filterAgentPerformacesDataIII();
    //}
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;

  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
      this.getYears();
  }
  onYearchange(e) {
    this.selectedmonth = [];
  }
  loaderStatusChanged(e) {

  }

  onMonthchange(e) {
    if (this.selectedmonth.length == 0) {
      this.selectedmonth.push(this.currentmonth);
    }
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear.push(_selectedYear);
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.selectedmonth = new Array<string>();
    
    this.listOfMonths.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i.monthName, value: i.monthId
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = this.listOfMonths.filter(m => m.monthName === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.currentmonth = this.listOfMonths[this.selectmonthindex].monthId;
    this.selectedmonth.push(this.listOfMonths[this.selectmonthindex].monthId)

    if (this.tabSelectedIndex == 0) {
      this.GetAgentPerformacesI();
    }
    else if (this.tabSelectedIndex == 1) {
      this.GetAgentPerformacesII();
    }
    else if (this.tabSelectedIndex == 2) {
      this.GetAgentPerformacesIII();
    }
  }

  filterAgentPerformacesDataI() {
    this.filteredAgentPerformaceI = new Array<AgencyBreakdown>();
    let tempFilterData = new Array<AgencyBreakdown>();

    let monthdatafilter = new Array<AgencyBreakdown>();
    let RegionalManager = new Array<AgencyBreakdown>();
    let zonalManager = new Array<AgencyBreakdown>();
    let locations = new Array<AgencyBreakdown>();
    monthdatafilter = this.AgentPerformaceI;
    tempFilterData = this.AgentPerformaceI;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.AgentPerformaceI.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zmId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    } 
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyID === i);
        filterdata.forEach(m => {
          locations.push(m);
        })
      })
    } 
    if (locations && locations.length > 0) {
      tempFilterData = locations;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && locations && locations.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    }

    this.filteredAgentPerformaceI = tempFilterData;
  }

  SearchReport() {

    if (this.previousYearLength != this.selectedyear.length || this.previousMonthLength != this.selectedmonth.length) {

      this.previousYearLength = this.selectedyear.length;
      this.previousMonthLength = this.selectedmonth.length;

      if (this.tabSelectedIndex == 0) {
        this.GetAgentPerformacesI();
      }
      else if (this.tabSelectedIndex == 1) {
        this.GetAgentPerformacesII();
      }
      else if (this.tabSelectedIndex == 2) {
        this.GetAgentPerformacesIII();
      }
    }
    else {
      if (this.tabSelectedIndex == 0) {
        this.filterAgentPerformacesDataI();
      }
      else if (this.tabSelectedIndex == 1) {
        this.filterAgentPerformacesDataII();
      }
      else if (this.tabSelectedIndex == 2) {
        this.filterAgentPerformacesDataIII();
      }
    }
    
  }
  filterAgentPerformacesDataII() {
    this.filteredAgentPerformaceII = new Array<AgencyBreakdown>();
    let tempFilterData = new Array<AgencyBreakdown>();

    
    let RegionalManager = new Array<AgencyBreakdown>();
    let zonalManager = new Array<AgencyBreakdown>();
    tempFilterData = this.AgentPerformaceII;
    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.AgentPerformaceII.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } 
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zmId === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    } 
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    }

    this.filteredAgentPerformaceII = tempFilterData;
  }

  filterAgentPerformacesDataIII() {
    this.filteredAgentPerformaceIII = new Array<AgencyBreakdown>();
    let tempFilterData = new Array<AgencyBreakdown>();
    let RegionalManager = new Array<AgencyBreakdown>();

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.AgentPerformaceIII.filter(m => m.rmId === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    } else if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager.length == 0) {
      tempFilterData = new Array<AgencyBreakdown>();
    } else {
      tempFilterData = this.AgentPerformaceIII;
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    this.filteredAgentPerformaceIII = tempFilterData;
  }


  exportExcel() {
    this.showLoader = true;

    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportExcelAgencyBreakDownByOfficeI(this.filteredAgentPerformaceI).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExportExcelAgencyBreakDownZM(this.filteredAgentPerformaceII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 2) {
      this.reportService.ExportExcelAgencyBreakDownROM(this.filteredAgentPerformaceIII).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
  }


  rowbgColor(zmId) {
    this.bgcolor = '';
    if (this.previousZMId != zmId) {
      this.previousZMId = zmId;
      this.rowcounter++;
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }
    else if (this.previousZMId == zmId) {
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }
  }
  //PreviousZM: string = '';
  //zmBGColor: string = '';
  rowbgColorForZM(zmId) {
    this.zmBGColor = '';
    if (this.PreviousZM != zmId) {
      this.PreviousZM = zmId;
      this.zmBGColor = Math.floor(Math.random() * 16777215).toString(16);
      return '#' + this.zmBGColor
    }
    else if (this.PreviousZM == zmId) {
        this.zmBGColor = this.zmBGColor
      return this.zmBGColor
    }
  }

}
