import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { SelectItem } from 'primeng/api'
import { DateService } from '../../Services/dateService';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { ReportService } from '../../Services/ReportService';
import { DownloadexcelService } from '../../Services/downloadexcel.service';
@Component({
  selector: 'app-carrier-mix-report',
  templateUrl: './carrier-mix-report.component.html',
  styleUrls: ['./carrier-mix-report.component.scss']
})
export class CarrierMixReportComponent implements OnInit {
  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  showLoader: boolean = true;

  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;
  selectedmonth: string = "";

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  innerHeight: number = 0;
  innercontainer: string = "400px";
  bgcolor: string = '';

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  carrierMixExcelI: any;
  carrierMixI: any;
  carrierMixITotal: any;
  carrierMixPremium: any;
  carrierMixPremiumTotal: any;
  carrierMixPremiumExcel: any;

  carrierMixAgencyFee: any;
  carrierMixAgencyFeeTotal: any;
  carrierMixAgencyFeeExcel: any;

  constructor(private dateservice: DateService, private router: Router, private renderer: Renderer2
    , public cmnSrv: CommonService, public reportService: ReportService, private _downloadExcel: DownloadexcelService) { }

  ngOnInit() {
    this.carrierMixExcelI = [];
    this.carrierMixI = [];
    this.carrierMixITotal = [];
    this.carrierMixPremium = [];
    this.carrierMixAgencyFee = [];

    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.getYears();

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.CarrierMixI();
    }
    else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.CarrierMixPremium();
    }
    else if (_index == 2) {
      this.tabSelectedIndex = _index;
      this.CarrierMixAgencyFee();
    }
  }

  onYearchange(e) {
    
  }
    onMonthchange(e) {
      //if (this.tabSelectedIndex == 0) {
      //  this.CarrierMixI();
      //}
      //else if (this.tabSelectedIndex == 1) {
      //  this.CarrierMixPremium();
      //}
    }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
     
    });
  }

  getMonth() {
    this.selectedmonth = this.monthNames[new Date().getMonth()]
    this.dateservice.getMonth(this.selectedyear).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<string> = new Array<string>();
      months = m;
      console.log("months", months)
      months = months.reverse();
      if (months.length > 0) {
        this.selectedmonth = months[0]
      }
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i, value: i
          })
      })
      this.CarrierMixI();
    })
  }

  Object = Object;
  CarrierMixI() {
    this.showLoader = true;
    this.carrierMixI = [];
    this.reportService.CarrierMixI(parseInt(this.selectedyear), this.selectedmonth).subscribe(x => {
      let _carrierMix = x;
      this.carrierMixExcelI = x;
      this.carrierMixITotal = _carrierMix.filter(x => x.Company == 'Total');
      this.carrierMixI =  _carrierMix.filter(x => x.Company != 'Total');
      console.log("this.carrierMixI", this.carrierMixI);
      this.showLoader = false;
    })
    
  }

  CarrierMixPremium() {
    this.showLoader = true;
    this.carrierMixPremium = [];
    this.reportService.CarrierMixPremium(parseInt(this.selectedyear), this.selectedmonth).subscribe(x => {
      let _carrierMix = x;
      this.carrierMixPremiumExcel = x;
      this.carrierMixPremiumTotal = _carrierMix.filter(x => x.Company == 'Total');
      this.carrierMixPremium = _carrierMix.filter(x => x.Company != 'Total');
      console.log("this.carrierMixI", this.carrierMixI);
      this.showLoader = false;

    })
  }

  CarrierMixAgencyFee() {
    this.showLoader = true;
    this.carrierMixAgencyFee = [];
    this.reportService.CarrierMixAgencyFee(parseInt(this.selectedyear), this.selectedmonth).subscribe(x => {
      let _carrierMix = x;
      this.carrierMixAgencyFeeExcel = x;
      this.carrierMixAgencyFeeTotal = _carrierMix.filter(x => x.Company == 'Total');
      this.carrierMixAgencyFee = _carrierMix.filter(x => x.Company != 'Total');
      console.log("this.carrierMixI", this.carrierMixI);
      this.showLoader = false;

    })
  }

  getcolumnName(val: string) {
    if (val.indexOf("%") !== -1) {
      return '%';
    }
    else {
      return val;
    }
  }

  GetPercentageSign(val: string) {
    if (val.indexOf("%") !== -1) {
      return '%';
    }
    else {
      return '';
    }
  }

  SearchReport() {
    if (this.tabSelectedIndex == 0) {
      this.CarrierMixI();
    }
    else if (this.tabSelectedIndex == 1) {
      this.CarrierMixPremium();
    }
    else if (this.tabSelectedIndex == 2) {
      this.CarrierMixAgencyFee();
    }
  }

  exportExcel() {
    this.showLoader = true;
    console.log("this.tabSelectedIndex", this.tabSelectedIndex);
    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportExcelCarrierMixI(this.carrierMixExcelI).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExportExcelCarrierMixPremium(this.carrierMixPremiumExcel).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    } else if (this.tabSelectedIndex == 2) {
      this.reportService.ExportExcelCarrierMixAgencyFee(this.carrierMixAgencyFeeExcel).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }

  }


  rowbgColor(zmId) {

    this.bgcolor = '';

    if (zmId % 2 == 0) {
      this.bgcolor = '#4d5668'
    }
    else {
      this.bgcolor = '#5e7385';
    }
    return this.bgcolor



  }
}
