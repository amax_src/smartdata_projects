import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from '../../login/login-guard.service';
import { AvgAgencyFeeComponent } from './avg-agency-fee/avg-agency-fee.component';
import { AgentPerformaceComponent } from './agent-performace/agent-performace.component';
import { UniqueCreatedQuotesComponent } from './unique-created-quotes/unique-created-quotes.component';
import { OvertimeComponent } from './overtime/overtime.component';
import { CarrierMixReportComponent } from './carrier-mix/carrier-mix-report.component';
import { TrackerComponent } from './tracker/tracker.component';
import { AgencyBreakdownComponent } from './agency-breakdown/agency-breakdown.component';
import { RentersReportComponent } from './renters-report/renters-report.component';
import { ActiveCustomersComponent } from './active-customers/active-customers.component'



const routes: Routes = [
  { path: 'avg-agency-fee', component: AvgAgencyFeeComponent, canActivate: [LoginGuard] },
  { path: 'agent-performace', component: AgentPerformaceComponent, canActivate: [LoginGuard] },
  { path: 'unique-created-quotes', component: UniqueCreatedQuotesComponent, canActivate: [LoginGuard] },
  { path: 'overtime', component: OvertimeComponent, canActivate: [LoginGuard] },
  { path: 'carrier-mix-i', component: CarrierMixReportComponent, canActivate: [LoginGuard] } ,
  { path: 'tracker', component: TrackerComponent, canActivate: [LoginGuard] } ,
  { path: 'agency-breakdown', component: AgencyBreakdownComponent, canActivate: [LoginGuard] } ,
  { path: 'renters-report', component: RentersReportComponent, canActivate: [LoginGuard] } ,
  { path: 'active-customers', component: ActiveCustomersComponent, canActivate: [LoginGuard] } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
