import { Component, HostListener, OnInit, Renderer2 } from "@angular/core";
import { SelectItem } from "primeng/api";
import { DateService } from "../../Services/dateService";
import {
  Router,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
  NavigationEnd,
} from "@angular/router";
import { CommonService } from "../../../core/common.service";
import * as NProgress from "nprogress";
import { ReportService } from "../../Services/ReportService";
import { RenterReport } from "../../../model/Reports";
import { DownloadexcelService } from "../../Services/downloadexcel.service";
import { ThirdPartyDraggable } from "@fullcalendar/interaction";


@Component({
  selector: 'app-renters-report',
  templateUrl: './renters-report.component.html',
  styleUrls: ['./renters-report.component.scss']
})
export class RentersReportComponent implements OnInit {
  RenterTracker: Array<RenterReport>;
  filteredRenterTracker: Array<RenterReport>;
  previousZMId: number = 0;
  rowcounter: number = 0;
  bgcolor: string = '';
  selectedRegionalManager: [];
  selectedzonalManager: [];
  showLoader: boolean = true;
  selectedlocation: Array<number> = [];
  location: Array<number> = [];

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;

  selectedPreviousYear: string = "";
  selectedPreviousMonth: string = "";
  monthNames: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  innerHeight: number = 0;
  innercontainer: string = "400px";

  constructor(
    private dateservice: DateService,
    private router: Router,
    private renderer: Renderer2,
    public cmnSrv: CommonService,
    public reportService: ReportService,
    private _downloadExcel: DownloadexcelService
  ) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 250);
    this.innercontainer = this.innerHeight.toString() + "px";
    this.tabSelectedIndex = 0;
    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }


  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetRentersReport();
    } 
  }

  GetRentersReport() {
    this.showLoader = true;
    this.reportService
        .GetRentersReport(parseInt(this.selectedyear), this.selectedmonth, this.location)
      .subscribe((x) => {
        this.RenterTracker = x;
        this.filterRenterTrackerData();
        this.showLoader = false;
      });
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedzonalManager = e;
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedRegionalManager = e;
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach((i) => {
      this.location.push(i.agencyId);
    });
      this.getYears();
  }
  onYearchange(e) {
    this.getMonth();
  }
  loaderStatusChanged(e) { }

  onMonthchange(e) {
    if (this.tabSelectedIndex == 0) {
      // this.GetRentersReport();
    } 
  }

  getYears() {
    this.dateservice.getYear().subscribe((m) => {
      this.years = m;
      this.years.forEach((i) => {
        this.yearsSelectItem.push({
          label: i,
          value: i,
        });
      });
      let _selectedYear = new Date().getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.selectedPreviousYear = this.selectedyear;
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.dateservice.GetMonthskeyvalue(this.selectedyear).subscribe((m) => {
      this.month = m;
      this.month.forEach((i) => {
        this.monthSelectItem.push({
          label: i.name,
          value: i.id,
        });
      });
      let _selectedMonth = 12;
      if (this.selectedyear == new Date().getFullYear().toString()) {
        _selectedMonth = new Date().getMonth() + 1;
      } else {
      }
      this.selectedmonth = _selectedMonth.toString();

      if (this.tabSelectedIndex == 0 && this.selectedPreviousMonth == "") {
        this.selectedPreviousMonth = this.selectedmonth;
        this.GetRentersReport();
      } 
    });
  }

  filterRenterTrackerData() {
    this.filteredRenterTracker = new Array<RenterReport>();
    let tempFilterData = new Array<RenterReport>();
    let monthdatafilter = new Array<RenterReport>();
    let RegionalManager = new Array<RenterReport>();
    let zonalManager = new Array<RenterReport>();
    let agencyName = new Array<RenterReport>();

    this.RenterTracker.forEach((m) => {
      monthdatafilter.push(m);
    });

    if (
      this.selectedRegionalManager &&
      this.selectedRegionalManager.length > 0
    ) {
      this.selectedRegionalManager.forEach((i) => {
        let filterdata = monthdatafilter.filter(
          (m) => parseInt(m.rmid) === i
        );
        filterdata.forEach((m) => {
          RegionalManager.push(m);
        });
      });
    } else {
      tempFilterData = monthdatafilter;
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach((i) => {
        let filterdata = tempFilterData.filter(
          (m) => parseInt(m.zmid) === i
        );
        filterdata.forEach((m) => {
          zonalManager.push(m);
        });
      });
    } else {
      if (RegionalManager && RegionalManager.length == 0) {
        tempFilterData = monthdatafilter;
      }
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach((i) => {
        let filterdata = tempFilterData.filter(
          (m) => parseInt(m.agencyID) === i
        );
        filterdata.forEach((m) => {
          agencyName.push(m);
        });
      });
      tempFilterData = agencyName;
    this.filteredRenterTracker = tempFilterData;
    } else {
      if (
        RegionalManager &&
        RegionalManager.length == 0 &&
        zonalManager &&
        zonalManager.length == 0
      ) {
        tempFilterData = monthdatafilter;
      }
      this.filteredRenterTracker = tempFilterData;
    }
   // if (agencyName && agencyName.length > 0) {
     // tempFilterData = agencyName;
    //}

    //this.filteredRenterTracker = tempFilterData;
  }

  exportExcel() {
    this.showLoader = true;
      this.reportService.ExportRentersReport(this.filteredRenterTracker).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        this.showLoader = false;
      })
  }

  SearchReport() {
    if (this.selectedPreviousYear != this.selectedyear || this.selectedPreviousMonth != this.selectedmonth) {
      this.selectedPreviousYear = this.selectedyear;
      this.selectedPreviousMonth = this.selectedmonth;
      if (this.tabSelectedIndex == 0) {
        this.GetRentersReport();
      }
    }
    else {
      if (this.tabSelectedIndex == 0) {
        this.filterRenterTrackerData();
      }
    }

  }


  rowbgColor(zmId) {
    this.bgcolor = '';
    if (this.previousZMId != zmId) {
        this.previousZMId = zmId;
        this.rowcounter++;
        if (this.rowcounter % 2 == 0) {
            this.bgcolor = '#4d5668'
        }
        else {
            this.bgcolor = '#5e7385';
        }
        return this.bgcolor
    }
    else if (this.previousZMId == zmId) {
        if (this.rowcounter % 2 == 0) {
            this.bgcolor = '#4d5668'
        }
        else {
            this.bgcolor = '#5e7385';
        }
        return this.bgcolor
    }
}
}

