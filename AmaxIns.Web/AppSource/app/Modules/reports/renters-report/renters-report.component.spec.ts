import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentersReportComponent } from './renters-report.component';

describe('RentersReportComponent', () => {
  let component: RentersReportComponent;
  let fixture: ComponentFixture<RentersReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentersReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentersReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
