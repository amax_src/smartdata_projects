import { Component, OnInit, Renderer2, HostListener } from "@angular/core";
import { SelectItem } from "primeng/api";
import { DateService } from "../../Services/dateService";
import {  Router,  RouteConfigLoadStart,  RouteConfigLoadEnd,  NavigationEnd,} from "@angular/router";
import { CommonService } from "../../../core/common.service";
import * as NProgress from "nprogress";
import { ReportService } from "../../Services/ReportService";
import { TrackerReport, Months } from "../../../model/Reports";
import { DownloadexcelService } from '../../Services/downloadexcel.service';
@Component({
  selector: "app-tracker",
  templateUrl: "./tracker.component.html",
  styleUrls: ["./tracker.component.scss"],
})
export class TrackerComponent implements OnInit {
  RenterTracker: Array<TrackerReport>;
  ProductionTracker: Array<TrackerReport>;
  ActiveCustomerTracker: Array<TrackerReport>;

  filteredRenterTracker: Array<TrackerReport>;
  filteredProductionTracker: Array<TrackerReport>;
  filteredActiveCustomerTracker: Array<TrackerReport>;

  selectedRegionalManager: [];
  selectedzonalManager: [];
  showLoader: boolean = true;
  selectedlocation: Array<number> = [];
  location: Array<number> = [];

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<string> = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = [];

  currentmonth: string = "";
  selectmonthindex: number = 0;
  tabSelectedIndex: number = 0;

  previousYearLength: boolean = false;
  previousMonthLength: boolean = false;

  previousZMId: number = 0;
  rowcounter: number = 0;
  bgcolor: string = '';

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  innerHeight: number = 0;
  innercontainer: string = "400px";

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
  { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
  { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
  { monthId: '10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  constructor(
    private dateservice: DateService,
    private router: Router,
    private renderer: Renderer2,
    public cmnSrv: CommonService,
    public reportService: ReportService,
    private _downloadExcel: DownloadexcelService
  ) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabSelectedIndex = 0;
   
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  setHeader(_index) {
    if (_index == 0) {
      this.tabSelectedIndex = 0;
      this.GetRenterTracker();
    } else if (_index == 1) {
      this.tabSelectedIndex = _index;
      this.GetProductionTracker();
    } else if (_index == 2) {
      this.tabSelectedIndex = _index;
      this.GetActiveCustomerTracker();
    }
  }

  GetRenterTracker() {
    this.showLoader = true;
    this.reportService
        .GetRenterTracker(this.selectedyear, this.selectedmonth, this.location)
      .subscribe((x) => {
        this.RenterTracker = x;
        console.log(this.RenterTracker, "Renter");
        this.filterRenterTrackerData();
        this.showLoader = false;
      });
  }

  GetProductionTracker() {
    this.showLoader = true;
    this.reportService
        .GetProductionTracker(this.selectedyear, this.selectedmonth, this.location)
      .subscribe((x) => {
        this.ProductionTracker = x;
        console.log(this.ProductionTracker, "Production");
        this.filterProductionTrackerData();
        this.showLoader = false;
      });
  }

  GetActiveCustomerTracker() {
    this.showLoader = true;
    this.reportService
        .GetActiveCustomerTracker(this.selectedyear, this.selectedmonth, this.location)
      .subscribe((x) => {
        this.ActiveCustomerTracker = x;
        console.log(this.ActiveCustomerTracker, "Active Customer");
        this.filterActiveCustomerTrackerData();
        this.showLoader = false;
      });
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedzonalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterRenterTrackerData();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterProductionTrackerData();
    //} else if (this.tabSelectedIndex == 2) {
    //  this.filterActiveCustomerTrackerData();
    //}
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedRegionalManager = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterRenterTrackerData();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterProductionTrackerData();
    //} else if (this.tabSelectedIndex == 2) {
    //  this.filterActiveCustomerTrackerData();
    //}
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;

    //if (this.tabSelectedIndex == 0) {
    //  this.filterRenterTrackerData();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.filterProductionTrackerData();
    //} else if (this.tabSelectedIndex == 2) {
    //  this.filterActiveCustomerTrackerData();
    //}
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach((i) => {
      this.location.push(i.agencyId);
    });
      this.getYears();
  }
  onYearchange(e) {
    this.previousMonthLength = true;
    this.getMonth();
  }
  loaderStatusChanged(e) { }

  onMonthchange(e) {

    this.previousYearLength = true;
    //if (this.tabSelectedIndex == 0) {
    //  this.GetRenterTracker();
    //} else if (this.tabSelectedIndex == 1) {
    //  this.GetProductionTracker();
    //} else if (this.tabSelectedIndex == 2) {
    //  this.GetActiveCustomerTracker();
    //}
  }

  getYears() {
    this.dateservice.getYear().subscribe((m) => {
      this.years = m;
      this.years.forEach((i) => {
        this.yearsSelectItem.push({
          label: i,
          value: i,
        });
      });
      let _selectedYear = new Date().getFullYear().toString();
      this.selectedyear.push(_selectedYear);
      this.getMonth();
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    this.selectedmonth = new Array<string>();

    this.listOfMonths.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i.monthName, value: i.monthId
        })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth() - 1;;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = this.listOfMonths.filter(m => m.monthName === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.currentmonth = this.listOfMonths[this.selectmonthindex].monthId;
    this.selectedmonth.push(this.listOfMonths[this.selectmonthindex].monthId);

      if (this.tabSelectedIndex == 0) {
        this.GetRenterTracker();
      } else if (this.tabSelectedIndex == 1) {
        this.GetProductionTracker();
      } else if (this.tabSelectedIndex == 2) {
        this.GetActiveCustomerTracker();
      }
  }

  filterRenterTrackerData() {
    this.filteredRenterTracker = new Array<TrackerReport>();
    let tempFilterData = new Array<TrackerReport>();
    let RegionalManager = new Array<TrackerReport>();
    let zonalManager = new Array<TrackerReport>();
    let agencyName = new Array<TrackerReport>();

    tempFilterData = this.RenterTracker;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.RenterTracker.filter(m => parseInt(m.regionalManagerId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.zonalManagerId) === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyNameID == i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    }
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    this.filteredRenterTracker = tempFilterData;
  }

  filterProductionTrackerData() {
    this.filteredProductionTracker = new Array<TrackerReport>();
    let tempFilterData = new Array<TrackerReport>();
    let RegionalManager = new Array<TrackerReport>();
    let zonalManager = new Array<TrackerReport>();
    let agencyName = new Array<TrackerReport>();


    tempFilterData = this.ProductionTracker;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.ProductionTracker.filter(m => parseInt(m.regionalManagerId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.zonalManagerId) === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyNameID == i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    }
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    this.filteredProductionTracker = tempFilterData;
  }

  filterActiveCustomerTrackerData() {
    this.filteredActiveCustomerTracker = new Array<TrackerReport>();
    let tempFilterData = new Array<TrackerReport>();
    let RegionalManager = new Array<TrackerReport>();
    let zonalManager = new Array<TrackerReport>();
    let agencyName = new Array<TrackerReport>();

    tempFilterData = this.ActiveCustomerTracker;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = this.ActiveCustomerTracker.filter(m => parseInt(m.regionalManagerId) === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }
    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => parseInt(m.zonalManagerId) === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyNameID == i);
        filterdata.forEach(m => {
          agencyName.push(m);
        })
      })
    }
    if (agencyName && agencyName.length > 0) {
      tempFilterData = agencyName;
    }

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && agencyName && agencyName.length == 0) {
      tempFilterData = new Array<TrackerReport>();
    }

    this.filteredActiveCustomerTracker = tempFilterData;
  }



  SearchReport() {
    if (this.previousYearLength ==true || this.previousMonthLength ==true) {

      this.previousYearLength = false;
      this.previousMonthLength = false;

      if (this.tabSelectedIndex == 0) {
        this.GetRenterTracker();
      } else if (this.tabSelectedIndex == 1) {
        this.GetProductionTracker();
      } else if (this.tabSelectedIndex == 2) {
        this.GetActiveCustomerTracker();
      }
    }
    else {
      if (this.tabSelectedIndex == 0) {
        this.filterRenterTrackerData();
      }
      else if (this.tabSelectedIndex == 1) {
        this.filterProductionTrackerData();
      }
      else if (this.tabSelectedIndex == 2) {
        this.filterActiveCustomerTrackerData();
      }
    }
  }

  exportExcel() {
    this.showLoader = true;

    if (this.tabSelectedIndex == 0) {
      this.reportService.ExportRenterTracker(this.filteredRenterTracker).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 1) {
      this.reportService.ExportProductionTracker(this.filteredProductionTracker).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.tabSelectedIndex == 2) {
      this.reportService.ExportActiveCustomerTracker(this.filteredActiveCustomerTracker).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
  }

  rowbgColor(zmId) {

    this.bgcolor = '';
    if (this.previousZMId != zmId) {
      this.previousZMId = zmId;
      this.rowcounter++;
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }
    else if (this.previousZMId == zmId) {
      if (this.rowcounter % 2 == 0) {
        this.bgcolor = '#4d5668'
      }
      else {
        this.bgcolor = '#5e7385';
      }
      return this.bgcolor
    }

  }

}
