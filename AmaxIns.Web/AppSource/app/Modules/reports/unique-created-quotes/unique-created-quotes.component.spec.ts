import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniqueCreatedQuotesComponent } from './unique-created-quotes.component';

describe('UniqueCreatedQuotesComponent', () => {
  let component: UniqueCreatedQuotesComponent;
  let fixture: ComponentFixture<UniqueCreatedQuotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniqueCreatedQuotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueCreatedQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
