import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { SelectItem } from 'primeng/api'
import { DateService } from '../../Services/dateService';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import * as NProgress from 'nprogress';
import { ReportService } from '../../Services/ReportService';
import { UniqueQuotes } from '../../../model/Reports';
import { debug } from 'util';
import { DownloadexcelService } from '../../Services/downloadexcel.service';
import { retry } from 'rxjs/operators';
@Component({
  selector: 'app-unique-created-quotes',
  templateUrl: './unique-created-quotes.component.html',
  styleUrls: ['./unique-created-quotes.component.scss']
})
export class UniqueCreatedQuotesComponent implements OnInit {

  UniqueQuotes: Array<UniqueQuotes>;
  filteredUniqueQuotes: Array<UniqueQuotes>;
  filterUniqueQuotes: Array<UniqueQuotes>;

  marketSelectItem: SelectItem[] = [];
  selectedMarket: Array<string> = [];

  selectedRegionalManager: [];
  selectedzonalManager: []
  showLoader: boolean = true;
  selectedlocation: Array<number> = []
  location: Array<number> = []

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  innerHeight: number = 0;
  innercontainer: string = "400px";

  previousMarketName: string = '';
  totalMarketCount: number = 0;
  colspan: number = 0;
  totalCreatedQuotes0: number = 0;
  totalCreatedQuotes1: number = 0;
  yoyCreatedQuotes0: number = 0;
  isShowMarket: boolean = false;
  bgColor: string = '';

  gtotalCreatedQuotes0: number = 0;
  gtotalCreatedQuotes1: number = 0;
  gyoyCreatedQuotes0: number = 0;
  isShowTotalRow: boolean = false;
    colorIndex: number = 0;
    onPageLoad: boolean = true;
  colorlist = ['#483D8B', '#6A5ACD', '#B22222', '#FF7F50', '#FF6347', '#BDB76B', '#98FB98', '#3CB371'
    , '#2E8B57', '#228B22', '#6B8E23', '#556B2F', '#8FBC8F', '#008B8B', '#5F9EA0', '#B0C4DE', '#CD853F', '#A0522D', '#696969','#2F4F4F']
  constructor(private dateservice: DateService, private router: Router, private renderer: Renderer2
    , public cmnSrv: CommonService, public reportService: ReportService, private _downloadExcel: DownloadexcelService) { }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.GetAllMarket();
   
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  loaderStatusChanged(e) {
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
  }

  GetAllMarket() {
    this.reportService.GetAllMarket().subscribe(x => {
      console.log(x);
      let marketList = x;
      marketList.forEach(i => {
        this.marketSelectItem.push(
          {
            label: i.marketName, value: i.marketName
          })
      })
    })
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
      this.getYears();
  }
  onYearchange(e) {
    this.getMonth();
  }

  onMonthchange(e) {
    //this.GetUniqueQuotes();
  }
  onMarketchange() {

  }
  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
    });
  }

  getMonth() {
    this.dateservice.GetMonthskeyvalue(this.selectedyear).subscribe(m => {
      this.month = m;
      this.month.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })
      let _selectedMonth = 12;
      if (this.selectedyear == (new Date()).getFullYear().toString()) {
        _selectedMonth = (new Date()).getMonth() + 1;
      } else {

      }
      this.selectedmonth = _selectedMonth.toString();
        if (this.onPageLoad == true) {
            this.onPageLoad = false;
            this.GetUniqueQuotes();
        }
      //

    });
  }

  GetUniqueQuotes() {
    this.showLoader = true;
      this.reportService.GetUniqueQuotes(parseInt(this.selectedyear), this.selectedmonth, this.location).subscribe(x => {
      this.UniqueQuotes = x;
      console.log(x);
      //this.filteredUniqueQuotes = this.UniqueQuotes;
      this.filterUniqueQuotessData()
      this.showLoader = false;
    })
  }

  filterUniqueQuotessData() {

    this.filteredUniqueQuotes = new Array<UniqueQuotes>();
    let tempFilterData = new Array<UniqueQuotes>();

    let marketFilter = new Array<UniqueQuotes>();
    let RegionalManager = new Array<UniqueQuotes>();
    let zonalManager = new Array<UniqueQuotes>();
    let locations = new Array<UniqueQuotes>();
    tempFilterData = this.UniqueQuotes;

    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0) {
      this.selectedRegionalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.rmid === i);
        filterdata.forEach(m => {
          RegionalManager.push(m);
        })
      })
    }
    if (RegionalManager && RegionalManager.length > 0) {
      tempFilterData = RegionalManager;
    }

    if (this.selectedzonalManager && this.selectedzonalManager.length > 0) {
      this.selectedzonalManager.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.zmid === i);
        filterdata.forEach(m => {
          zonalManager.push(m);
        })
      })
    }
    if (zonalManager && zonalManager.length > 0) {
      tempFilterData = zonalManager;
    }

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.agencyID === i);
        filterdata.forEach(m => {
          locations.push(m);
        })
      })
    }
    if (locations && locations.length > 0) {
      tempFilterData = locations;
    }

    if (this.selectedMarket && this.selectedMarket.length > 0) {
      this.selectedMarket.forEach(i => {
        let filterdata = tempFilterData.filter(m => m.market === i);
        filterdata.forEach(m => {
          marketFilter.push(m);
        })
      })
    }
    if (marketFilter && marketFilter.length > 0) {
      tempFilterData = marketFilter;
    }


    if (this.selectedRegionalManager && this.selectedRegionalManager.length > 0 && RegionalManager && RegionalManager.length == 0) {
      tempFilterData = new Array<UniqueQuotes>();
    }
    else if (this.selectedzonalManager && this.selectedzonalManager.length > 0 && zonalManager && zonalManager.length == 0) {
      tempFilterData = new Array<UniqueQuotes>();
    }
    else if (this.selectedlocation && this.selectedlocation.length > 0 && locations && locations.length == 0) {
      tempFilterData = new Array<UniqueQuotes>();
    }
    else if (this.selectedMarket && this.selectedMarket.length > 0 && marketFilter && marketFilter.length == 0) {
      tempFilterData = new Array<UniqueQuotes>();
    }
    this.filteredUniqueQuotes = tempFilterData;

      this.gtotalCreatedQuotes0 = 0;
      this.gtotalCreatedQuotes1 = 0;
      this.filteredUniqueQuotes.forEach(x => {
          this.gtotalCreatedQuotes1 = this.gtotalCreatedQuotes1 + x.createdQuotes1;
          this.gtotalCreatedQuotes0 = this.gtotalCreatedQuotes0 + x.createdQuotes0;
      })
    
    //this.gtotalCreatedQuotes0 = this.filteredUniqueQuotes.reduce((CreatedQuotes0, current) => CreatedQuotes0 + current.createdQuotes0, 0);
    //this.gtotalCreatedQuotes1 = this.filteredUniqueQuotes.reduce((CreatedQuotes1, current) => CreatedQuotes1 + current.createdQuotes1, 0);
    if (this.gtotalCreatedQuotes0 > 0) {
      this.gyoyCreatedQuotes0 = Math.round(((this.gtotalCreatedQuotes1 / this.gtotalCreatedQuotes0) * 100 - 100));
    }
    else {
      this.gyoyCreatedQuotes0 = 0;
    }
    this.colorIndex = 0;
    setTimeout(() => {                           // <<<---using ()=> syntax
      this.showLoader = false;
    }, 100);
  }

  SearchReport() {
    this.GetUniqueQuotes();
  }

  exportExcel() {
    this.reportService.ExportExcelUniqueCretedQuotes(this.filteredUniqueQuotes).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      console.log("FileName", x);
      this.showLoader = false;
    })
  }


  rowbgColor(zmId, item) {
    //debugger
    this.isShowTotalRow = false;
    if (this.filteredUniqueQuotes.length - 1 != zmId) {
      if (this.filteredUniqueQuotes[zmId + 1].market != item.market) {
        this.isShowTotalRow = true;
      }
    }
    else if (this.filteredUniqueQuotes.length - 1 == zmId) {
      this.isShowTotalRow = true;
    }

    if (this.previousMarketName != item.market) {
      this.previousMarketName = item.market;
      var lenght = this.filteredUniqueQuotes.filter(x => x.market == item.market).length;
      this.colspan = lenght + 1;
        this.isShowMarket = true;
        if (this.selectedlocation && this.selectedlocation.length > 0) {
            this.totalCreatedQuotes0 = this.filteredUniqueQuotes.filter(x => x.market == item.market && x.agencyID == item.agencyID).reduce((CreatedQuotes0, current) => CreatedQuotes0 + current.createdQuotes0, 0);
            this.totalCreatedQuotes1 = this.filteredUniqueQuotes.filter(x => x.market == item.market && x.agencyID == item.agencyID).reduce((CreatedQuotes1, current) => CreatedQuotes1 + current.createdQuotes1, 0);
        }
        else {
            this.totalCreatedQuotes0 = this.filteredUniqueQuotes.filter(x => x.market == item.market).reduce((CreatedQuotes0, current) => CreatedQuotes0 + current.createdQuotes0, 0);
            this.totalCreatedQuotes1 = this.filteredUniqueQuotes.filter(x => x.market == item.market).reduce((CreatedQuotes1, current) => CreatedQuotes1 + current.createdQuotes1, 0);
        }
     
      if (this.totalCreatedQuotes0 > 0) {
        this.yoyCreatedQuotes0 = Math.round(((this.totalCreatedQuotes1 / this.totalCreatedQuotes0) * 100 - 100));

        //this.colorIndex = Math.floor(Math.random() * 20) + 1;
          if (this.bgColor == '') {
              this.bgColor = '#4d5668';
          }
          else if (this.bgColor == '#4d5668') {
              this.bgColor = '#5e7385';
          }
          else {
              this.bgColor = '#4d5668';
          }
        //this.bgColor = this.colorlist[this.colorIndex];
      
        return this.bgColor;
      }
      else {
        this.yoyCreatedQuotes0 = 0;
      }
    }
    else if (this.previousMarketName == item.market) {
      this.isShowMarket = false;
      return this.bgColor;
    }

   

  }

}

