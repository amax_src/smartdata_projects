import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { SpectrumMonthlyComponent } from './spectrumMonthly.component';


const routes: Routes = [
  { path: 'SpectrumMonthly', component: SpectrumMonthlyComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SpectrumMonthlyRoutingModule { }
