import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import { spectrumService } from '../Services/spectrumService';
import { spectrumDailySummaryModel } from '../../model/spectrumDailySummaryModel';
import { spectrumDailyDetailModel } from '../../model/spectrumDailyDetailModel';
import { PageService } from '../Services/pageService';
import { consolidatedCallVolume } from '../../model/consolidatedCallVolume';
import { SpectrumRepeatedCall } from '../../model/SpectrumRepeatedCall';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';

@Component({
  selector: 'app-SpectrumMonthly',
  templateUrl: './spectrumMonthly.component.html',
  styleUrls: ['./spectrumMonthly.component.scss']
})

export class SpectrumMonthlyComponent implements OnInit, AfterViewChecked {
  MonthlyAvarageTalkTime: number = 0;
  CallTypeSelectItem: SelectItem[] = [];
  selectedCallType: string = "";
  sumAverageTalkTime: number = 0;
  CallNumberSelectItem: SelectItem[] = [];
  SelectedCallNumber: Array<string> = new Array<string>()
  outboundcalls: number = 0;
  dateRange: string = '';
  PageName: string = "Spectrum > Monthly";
  location: Array<Location> = new Array<Location>();
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<number> = new Array<number>();
  singleLocation: number = 112;
  selectedRegionalManager: [] = [];


  spectrumDailyCallVolumeData: consolidatedCallVolume = new consolidatedCallVolume();
  FilterspectrumDailyCallVolumeData: consolidatedCallVolume = new consolidatedCallVolume();

  spectrumMonthlySummaryData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
  FilterspectrumMonthlySummaryData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
  ChartModelMonthlySummaryData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();

  spectrumMonthlyDetailData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();
  FilterspectrumMonthlyDetailData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();


  defaultselectedDate: Date;

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  selectedmonthList: Array<string> = new Array<string>();
  selectedmonthListForMothlyHeading: Array<string> = new Array<string>();
  singleLocationSelected: boolean = false;

  chartheight: number = 300;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  currentMonth: string = "Feburay";
  MonthlydetailMonth: string = '';
  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();

  extentionSelectItem: SelectItem[] = [];
  selectedextention: Array<string> = new Array<string>();
  selectedTabIndex: number = 0;


  SpectrumMonthlySummary: Array<any>;
  SpectrumMonthlyCallDetailSummary: Array<any>;

  totalInbound: number = 0;
  totalOutbound: number = 0;
  totaltalktime: number = 0;

  DetailtotalInbound: number = 0;
  DetailtotalOutbound: number = 0;
  Detailtotaltalktime: number = 0;
  DetailOutBoundMints: number = 0;

  MonthlytotalInbound: number = 0;
  MonthlytotalOutbound: number = 0;
  Monthlytotaltalktime: number = 0;


  MonthlyDetailtotalInbound: number = 0;
  MonthlyDetailtotalOutbound: number = 0;
  MonthlyDetailtotaltalktime: number = 0;
  MonthlyDetailOutboundMints: number = 0;

  ChartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          //labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }
  firstLocation: number = 112;

  SetLocations: Array<number> = [];
  selectmonthindex: number;


  MonthlyPager: any = {};
  MonthlyPagedItems: any[];

  SpectrumMonthlyCallVolumeTalkTime: any = [];
  SpectrumMonthlyCallVolumeTotalCalls: any = [];
  SpectrumMonthlyCallVolumeStatus: any = [];

  allLocations: Array<Location> = new Array<Location>();
  isFirstTimeLocationLoaded: boolean = true;
  RepeatedCallsIncomming: any = [];

  RepeatedCallsOutbound: any = [];

  RepeatedCallPager: any = {};
  RepeatedCallPagedItems: any[];

  SpectrumRepeatedCall: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();
  FilterSpectrumRepeatedCall: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, public spectrumService: spectrumService, private _pageservice: PageService, private ApiLoad: ApiLoadTimeService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.getSpectrumMonthlySummary();

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }



  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();

  }


  SetSingleLocation(e) {
    this.singleLocation = e;
    this.filterData();
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();

  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    if (this.isFirstTimeLocationLoaded) {
      this.allLocations = e;
      this.isFirstTimeLocationLoaded = false;
    }
    this.location = e;
    this.singleLocation = e[0].agencyId;
    this.filterData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
    }
  }

  getspectrumMonthlySummaryextention() {
    this.extentionSelectItem = []
    let dt: Array<string> = new Array<string>();
    dt = Array.from(new Set(this.spectrumMonthlySummaryData.map(item => item.extension)));
    dt.forEach(i => {
      this.extentionSelectItem.push(
        {
          label: i, value: i
        })
    })

  }



  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    this.filterData();
  }

  onDropDownDateChange() {


    this.filterData();
  }

  onExtentionChange() {
    this.filterData();
  }

  filterData() {
   
    if (this.selectedTabIndex === 0) {
      this.singleLocationSelected = false;
      this.getSpectrumMonthlySummary()
    }
    else if (this.selectedTabIndex === 1) {
      this.singleLocationSelected = true;
      this.getSpectrumMonthlyDetail();
    }
    else if (this.selectedTabIndex === 2) {
      this.singleLocationSelected = false;
      this.getSpectrumCallVolume();
    }


  }

  getSpectrumCallVolume() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.spectrumService.GetSpectrumCallVolume().subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Monthly Spectrum Calls"
      model.time = ResponseTime

      this.spectrumDailyCallVolumeData = m;
      this.FilterMonthlyCallVolumeData();
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    })

  }

  getSpectrumMonthlySummary(hideloader: boolean = true) {
    let startTime: number = new Date().getTime();
    if (this.spectrumMonthlySummaryData.length > 0) {
      this.getMonth(this.spectrumMonthlySummaryData);
      this.filterMonthlySummary();
      this.getspectrumMonthlySummaryextention();
      this.ischartDataLoaded = true;
    }
    else {
      this.showLoader = true;
      this.spectrumService.GetSpectrumMonthlySummary().toPromise().then(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "Monthly Spectrum Summary"
        model.time = ResponseTime

        this.spectrumMonthlySummaryData = m
        this.getMonth(this.spectrumMonthlySummaryData);

        this.filterMonthlySummary();
        this.getspectrumMonthlySummaryextention();
        this.ischartDataLoaded = true;
        if (hideloader) {
          this.showLoader = false;
        }
        this.ApiLoad.SaveApiTime(model).subscribe();
      })
    }
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth(e: Array<any>) {

    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    let tempdata: Array<any> = new Array<any>()
    tempdata = e.sort(this.sortMonth)

    months = Array.from(new Set(tempdata.map(item => item.month)));

    if (this.selectedmonthListForMothlyHeading.length === 0) {
      this.selectedmonthListForMothlyHeading = new Array<string>();
      months.forEach(i => {
        this.selectedmonthListForMothlyHeading.push(i);
      })
    }

    months.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i, value: i
        })
    })

    months = months.reverse();
    this.selectedmonth = months[months.length - 1];
    this.currentMonth = this.selectedmonth;
    if (this.MonthlydetailMonth === "") {
      this.MonthlydetailMonth = this.currentMonth;
    }
  }

  filterMonthlySummary() {

    this.FilterspectrumMonthlySummaryData = this.spectrumMonthlySummaryData;
    let LocationfilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    let DatefilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    let ExtionfilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    this.ChartModelMonthlySummaryData = new Array<spectrumDailySummaryModel>();
    this.MonthlytotalOutbound = 0;
    let sumTotalTalkTime = 0;
    let sumAverageTalkTime = 0;
    this.MonthlytotalInbound = 0;
    this.Monthlytotaltalktime = 0;
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        let tempLocationfilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        tempLocationfilter = this.FilterspectrumMonthlySummaryData.filter(x => x.agencyid === m)
        tempLocationfilter.forEach(z => {
          LocationfilterData.push(z);
        })
      })
    }
    else {
      this.location.forEach(m => {
        let tempLocationfilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        tempLocationfilter = this.FilterspectrumMonthlySummaryData.filter(x => x.agencyid === m.agencyId)
        tempLocationfilter.forEach(z => {
          LocationfilterData.push(z);
        })
      })
    }

    this.FilterspectrumMonthlySummaryData = LocationfilterData;

    if (this.selectedextention && this.selectedextention.length > 0) {
      this.selectedextention.forEach(m => {
        let selectedextentionFilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        selectedextentionFilter = this.FilterspectrumMonthlySummaryData.filter(x => x.extension === m)
        selectedextentionFilter.forEach(z => {
          ExtionfilterData.push(z);
        })
      })
    }


    if (ExtionfilterData && ExtionfilterData.length > 0) {
      this.FilterspectrumMonthlySummaryData = ExtionfilterData;
    }

    this.MonthlyAvarageTalkTime = 0;
    if (this.selectedmonthListForMothlyHeading && this.selectedmonthListForMothlyHeading.length > 0) {
      this.selectedmonthListForMothlyHeading.forEach(m => {
        let selectedMonthFilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        selectedMonthFilter = this.FilterspectrumMonthlySummaryData.filter(x => x.month === m)
        let monthlysumTotalTalkTime: number = 0;
        let monthlysumAverageTalkTime: number = 0;
        let monthlyOutBound: number = 0;
        let monthlyInBound: number = 0;
        let tempMonthlyDetail: spectrumDailySummaryModel = new spectrumDailySummaryModel();

        selectedMonthFilter.forEach(z => {
          monthlysumTotalTalkTime = monthlysumTotalTalkTime + z.talktimeMinutes;
          monthlysumAverageTalkTime = monthlysumAverageTalkTime + z.averageTalkTime;
          monthlyOutBound = monthlyOutBound + z.outboundCalls;
          monthlyInBound = monthlyInBound + z.inboundCalls;
        })

        tempMonthlyDetail.talktimeMinutes = Math.round(monthlysumTotalTalkTime);
        tempMonthlyDetail.averageTalkTime = Math.round(monthlysumAverageTalkTime / selectedMonthFilter.length);
        tempMonthlyDetail.outboundCalls = Math.round(monthlyOutBound);
        tempMonthlyDetail.inboundCalls = Math.round(monthlyInBound);
        tempMonthlyDetail.month = m;
        this.ChartModelMonthlySummaryData.push(tempMonthlyDetail);

      })
    }

    this.CreateSpectrumMonthlySummaryChartData(this.ChartModelMonthlySummaryData);

    //Below code is for Tiles Calculation on thrid (MonthlySummary ) tab
    this.FilterspectrumMonthlySummaryData.forEach(m => {
      sumTotalTalkTime = sumTotalTalkTime + m.talktimeMinutes;
      this.MonthlyAvarageTalkTime = this.MonthlyAvarageTalkTime + m.averageTalkTime;
      this.MonthlytotalOutbound = this.MonthlytotalOutbound + m.outboundCalls;
      this.MonthlytotalInbound = this.MonthlytotalInbound + m.inboundCalls;
    })

    this.Monthlytotaltalktime = Math.round(sumTotalTalkTime);

    this.MonthlyAvarageTalkTime = this.MonthlyAvarageTalkTime / this.FilterspectrumMonthlySummaryData.length;
    this.MonthlyAvarageTalkTime = Math.round(this.MonthlyAvarageTalkTime);
  }


  CreateSpectrumMonthlySummaryChartData(MonthlyDetailData: Array<spectrumDailySummaryModel>) {
    this.SpectrumMonthlySummary = new Array<any>();
    this.SpectrumMonthlyCallDetailSummary = new Array<any>();

    MonthlyDetailData.forEach(m => {
      let chatdata: any = [];
      let chartCallDetaildata: any = [];
      chatdata = {
        labels: ['Total Talktime ', 'Average Talktime'],
        month: m.month,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: [m.talktimeMinutes, m.averageTalkTime]
          }
        ]
      }

      chartCallDetaildata = {
        labels: ['Inbound calls', 'Outbound calls'],
        month: m.month,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: [m.inboundCalls, m.outboundCalls]
          }
        ]
      }
      this.SpectrumMonthlyCallDetailSummary.push(chartCallDetaildata)
      this.SpectrumMonthlySummary.push(chatdata)
    })
  }

 

  OntabSelectionChange(e) {

    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    
    this.spectrumDailyCallVolumeData = new consolidatedCallVolume();
   

    this.selectedTabIndex = e;
    this.filterData();
  }


  onMonthSummryChange() {
    this.selectedmonthListForMothlyHeading = this.selectedmonthList;
    this.filterData();
  }

  onMonthDetailChange() {
    this.filterData();
  }



  getSpectrumMonthlyDetail() {
   
    this.MonthlyPager = {};
    this.MonthlyPagedItems = [];
    this.showLoader = true;
    this.spectrumMonthlyDetailData = new Array<spectrumDailyDetailModel>();
    this.FilterspectrumMonthlyDetailData = new Array<spectrumDailyDetailModel>();
    this.getSpectrumMonthlySummary(false);

   // if (this.MonthlydetailMonth === '') {
    //  this.MonthlydetailMonth = this.currentMonth;

   // }
    //this.MonthlydetailMonth = this.currentMonth;
    this.MonthlyDetailtotalInbound = 0;
    this.MonthlyDetailtotalOutbound = 0;
    this.MonthlyDetailtotaltalktime = 0;
    this.MonthlyDetailOutboundMints = 0;
    let startTime: number = new Date().getTime();
    this.spectrumService.GetSpectrumMonthlyDetail(this.MonthlydetailMonth, this.singleLocation).toPromise().then(m => {
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Monthly Spectrum Details"
      model.time = ResponseTime

      this.spectrumMonthlyDetailData = m
      this.FilterspectrumMonthlyDetailData = this.spectrumMonthlyDetailData;

      this.FilterspectrumMonthlyDetailData.forEach(x => {
        if (!isNaN(x.talktime)) {
          this.MonthlyDetailtotaltalktime = this.MonthlyDetailtotaltalktime + (x.talktime / 60);
        }
      })

      this.MonthlyDetailtotaltalktime = Math.round(this.MonthlyDetailtotaltalktime);
      let outboundMintData = this.FilterspectrumMonthlyDetailData.filter(m => m.uniqueid.toLowerCase().includes("out"))
      if (outboundMintData && outboundMintData.length > 0) {
        this.MonthlyDetailtotalOutbound = outboundMintData.length;
      }

      outboundMintData.forEach(m => {
        if (!isNaN(m.talktime)) {
          this.MonthlyDetailOutboundMints = this.MonthlyDetailOutboundMints + (m.talktime / 60)
        }
      })

      this.MonthlyDetailOutboundMints = Math.round(this.MonthlyDetailOutboundMints);

      let inboundMintData = this.FilterspectrumMonthlyDetailData.filter(m => m.uniqueid.toLowerCase().includes("in"))
      if (inboundMintData && inboundMintData.length > 0) {
        this.MonthlyDetailtotalInbound = inboundMintData.length;
      }

      this.ischartDataLoaded = true;

      this.setMonthlyPage(1)
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    })

  }

  setMonthlyPage(page: number) {
    if (page < 1 || page > this.MonthlyPager.totalPages) {
      return;
    }

    // get pager object from service
    this.MonthlyPager = this._pageservice.getPager(this.FilterspectrumMonthlyDetailData.length, page);

    // get current page of items
    this.MonthlyPagedItems = this.FilterspectrumMonthlyDetailData.slice(this.MonthlyPager.startIndex, this.MonthlyPager.endIndex + 1);


  }


  FilterMonthlyCallVolumeData() {
    let CallVolumebyLocation: Array<any> = new Array<any>();
    let CallVolumebyStatusLocation: Array<any> = new Array<any>();

    this.FilterspectrumDailyCallVolumeData = this.spectrumDailyCallVolumeData;

    this.FilterspectrumDailyCallVolumeData.byDays = this.spectrumDailyCallVolumeData.byDays.filter(m => m.month === this.MonthlydetailMonth);
    this.FilterspectrumDailyCallVolumeData.byStatus = this.spectrumDailyCallVolumeData.byStatus.filter(m => m.month === this.MonthlydetailMonth);

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {

        let tempLocationfilter: Array<any> = new Array<any>();
        tempLocationfilter = this.FilterspectrumDailyCallVolumeData.byDays.filter(x => x.agencyId === m)
        tempLocationfilter.forEach(z => {
          CallVolumebyLocation.push(z);
        })


        let tempLocationStatusfilter: Array<any> = new Array<any>();
        tempLocationStatusfilter = this.FilterspectrumDailyCallVolumeData.byStatus.filter(x => x.agencyId === m)
        tempLocationStatusfilter.forEach(z => {
          CallVolumebyStatusLocation.push(z);
        })

      })
    }
    else {
      this.location.forEach(m => {

        let tempLocationfilter: Array<any> = new Array<any>();
        tempLocationfilter = this.FilterspectrumDailyCallVolumeData.byDays.filter(x => x.agencyId === m.agencyId)
        tempLocationfilter.forEach(z => {
          CallVolumebyLocation.push(z);
        })

        let tempLocationStatusfilter: Array<any> = new Array<any>();
        tempLocationStatusfilter = this.FilterspectrumDailyCallVolumeData.byStatus.filter(x => x.agencyId === m.agencyId)
        tempLocationStatusfilter.forEach(z => {
          CallVolumebyStatusLocation.push(z);
        })

      })
    }

    this.FilterspectrumDailyCallVolumeData.byDays = CallVolumebyLocation;

    this.FilterspectrumDailyCallVolumeData.byStatus = CallVolumebyStatusLocation;

    this.CreateChartDataForMonthlyCallVolume(this.FilterspectrumDailyCallVolumeData);

  }

  CreateChartDataForMonthlyCallVolume(e: consolidatedCallVolume) {
    this.SpectrumMonthlyCallVolumeTalkTime = [];
    this.SpectrumMonthlyCallVolumeTotalCalls = [];
    this.SpectrumMonthlyCallVolumeStatus = []
    let SuntalkTime: number = 0;
    let MontalkTime: number = 0;
    let TusetalkTime: number = 0;
    let WedtalkTime: number = 0;
    let ThrustalkTime: number = 0;
    let FritalkTime: number = 0;
    let SattalkTime: number = 0;

    let Suncount: number = 0;
    let Moncount: number = 0;
    let Tusecount: number = 0;
    let Wedcount: number = 0;
    let Thruscount: number = 0;
    let Fricount: number = 0;
    let Satcount: number = 0;


    let busy: number = 0;
    let answer: number = 0;
    let unknow: number = 0;
    let error: number = 0;
    let noanswer: number = 0;

    e.byDays.forEach(m => {
      switch (m.dayofWeek) {
        case 'Monday':
          MontalkTime = MontalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Moncount = Moncount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Tuesday':
          TusetalkTime = TusetalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Tusecount = Tusecount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Wednesday':
          WedtalkTime = WedtalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Wedcount = Wedcount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Thursday':
          ThrustalkTime = ThrustalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Thruscount = Thruscount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Friday':
          FritalkTime = FritalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Fricount = Fricount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Saturday':
          SattalkTime = SattalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Satcount = Satcount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'Sunday':
          SuntalkTime = SuntalkTime + (!isNaN(m.asTalkTime) ? m.asTalkTime : 0);
          Suncount = Suncount + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
      }
    })

    e.byStatus.forEach(m => {
      switch (m.call_Status) {
        case 'busy':
          busy = busy + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'unknown':
          unknow = unknow + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'error':
          error = error + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'noanswer':
          noanswer = noanswer + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
        case 'answer':
          answer = answer + (!isNaN(m.countOfCalls) ? m.countOfCalls : 0);
          break;
      }

    })

    this.SpectrumMonthlyCallVolumeStatus = {
      labels: ['Answer', 'Noanswer', 'Busy', 'Error', 'Unknown'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
          borderColor: '#1E88E5',
          data: [answer, noanswer, busy, error, unknow]
        }
      ]
    }


    this.SpectrumMonthlyCallVolumeTalkTime = {
      labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
          borderColor: '#1E88E5',
          data: [MontalkTime, TusetalkTime, WedtalkTime, ThrustalkTime, FritalkTime, SattalkTime, SuntalkTime]
        }
      ]

    }


    this.SpectrumMonthlyCallVolumeTotalCalls = {
      labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
          borderColor: '#1E88E5',
          data: [Moncount, Tusecount, Wedcount, Thruscount, Fricount, Satcount, Suncount]
        }
      ]

    }

  }

}
