import { Component, OnInit, AfterViewChecked, Renderer2, ViewChild } from '@angular/core';
import * as NProgress from 'nprogress';
import * as _ from 'underscore';
import { FormBuilder } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { authenticationService } from '../../../login/authenticationService.service';
import { CalendarService } from '../../Services/calendarService';
import { DateService } from '../../Services/dateService';
import { Location } from '../../../model/location';
import { PlannerDashboard } from '../../../model/plannerModel';
import { PageService } from '../../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-plannerdashboard',
  templateUrl: './plannerdashboard.component.html',
  styleUrls: ['./plannerdashboard.component.scss']
})
export class PlannerdashboardComponent implements OnInit, AfterViewChecked {

  PageName: string = "Planner > Variance Dashboard";

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }
  showLoader: boolean = true;
    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<number> = []
  PolicyCountChardata: Array<{ _data: any, agenyName: string }>;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  dashboardPlannerlist: Array<PlannerDashboard> = new Array<PlannerDashboard>();
  filterdashboardPlanner = new Array<PlannerDashboard>();
  PlannerData: Array<{ budget: number, actual: number, differnce: number, agenyName: string }> = new Array<{ actual: number, budget: number, differnce: number, agenyName: string }>();
  selectedDate: Array<string> = new Array<string>();
  dateSelectItem: SelectItem[] = [];

  monthSelectItem: SelectItem[] = [];
  selectmonthindex: number = 0;
  selectedmonth: string = "";
  pagerPolicyCount: any = {};
  pagedPolicyCount: any[];
  PolicyCountchartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25,
        bottom: 5
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' +value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${('$' +data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },

    scales: {
      xAxes: [{
        ticks: {
          display: false
        }//this will remove all the x-axis grid lines
      }],
      yAxes: [{
        ticks: {
          display: false
        } //this will remove all the x-axis grid lines
      }]
    }


  }

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private _pageservice: PageService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.fillYear();
    this.getMonth();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });
    //this.filterDashboard();
  }

  loaderStatusChanged(e) {
    //
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.filterDashboard();
  }

  getMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];

    setTimeout(() => {
      if (this.selectedmonth) {
        this.getDate();
        this.GetDashboard();
      }
    }, 500);
  }

  SearchGetDate() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];
    if (this.selectedmonth) {
      this.getDate();
      //this.GetDashboard();
    }
  }
  onYearchange() {
    this.SearchGetDate();
  }

  fillYear() {
    this.YearSelectItem = [];
    this.YearSelectItem.push(
      {
        label: '2021', value: '2021'
      })
    this.YearSelectItem.push(
      {
        label: '2022', value: '2022'
        })
      this.YearSelectItem.push(
          {
              label: '2023', value: '2023'
          })
  }

  getDate() {
    this.selectedDate = [];
    this.dateSelectItem = []
    this._DateService.GetDatesByMonth(this.selectedmonth, this.year).subscribe(x => {
      console.log("Date",x)
      x.forEach(i => {
        this.dateSelectItem.push(
          {
            label: i, value: i
          })
      })
    })
  }

  onDateChange() {
    //this.filterDashboard();
  }

  onMonthchange(e) {
    this.getDate();
    //if (this.selectedmonth) {
    //  this.GetDashboard();
    //}
  }

  SearchReport() {
    this.GetDashboard();
  }

  filterReset() {
    this.child.filterReset();
    this.fillYear();
      this.year = (new Date()).getFullYear().toString();
    this.getDate();
    setTimeout(() => {
      this.GetDashboard();
    }, 500);
   
  }

  GetDashboard() {
    this.showLoader = true;
    this.dashboardPlannerlist = new Array<PlannerDashboard>();
    this.calService.GetDashboardReport(this.selectedmonth,this.year).toPromise().then(x => {
      this.dashboardPlannerlist = x;
      console.log(this.dashboardPlannerlist);
      this.filterDashboard();
      this.showLoader = false;
    })
  }

  filterDashboard() {
    this.filterdashboardPlanner = new Array<PlannerDashboard>();
    let LocationDailyfilterdata: Array<PlannerDashboard> = new Array<PlannerDashboard>();
    let DateDailyfilterdata: Array<PlannerDashboard> = new Array<PlannerDashboard>();
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(i => {
        let filterDilydata = this.dashboardPlannerlist.filter(m => m.agencyId === i);
        filterDilydata.forEach(m => {
          LocationDailyfilterdata.push(m);
        })
      })
    }
    else if (this.location && this.location.length > 0) {
      this.location.forEach(i => {
        let filterDilydata = this.dashboardPlannerlist.filter(m => m.agencyId === i);
        filterDilydata.forEach(m => {
          LocationDailyfilterdata.push(m);
        })
      })
    }

    this.filterdashboardPlanner = LocationDailyfilterdata;

    if (this.selectedDate && this.selectedDate.length > 0) {
      this.selectedDate.forEach(i => {
        let filterDilydata = this.filterdashboardPlanner.filter(m => m.plannerDate === i);
        filterDilydata.forEach(m => {
          DateDailyfilterdata.push(m);
        })
      })
    }
    if (this.selectedDate && this.selectedDate.length > 0) {
      this.filterdashboardPlanner = DateDailyfilterdata;
    }
    this.CalculateRawPlanner();
  }

  CalculateRawPlanner() {
    this.PlannerData = [];
    let totalBudhet: number = 0;
    let totalActual: number = 0;
    let agency: string = '';
    let DailyDataViaAgency: Array<PlannerDashboard>

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        totalBudhet = 0;
        totalActual = 0;
      
        agency = '';
        DailyDataViaAgency = new Array<PlannerDashboard>();
        DailyDataViaAgency = this.filterdashboardPlanner.filter(z => z.agencyId === m);
        DailyDataViaAgency.forEach(i => {
          totalBudhet = totalBudhet + (!isNaN(i.totalPlannedBudgetAmount) ? i.totalPlannedBudgetAmount : 0);
          totalActual = totalActual + (!isNaN(i.totalActualAmount) ? i.totalActualAmount : 0);
          agency = i.agencyName;
        })
        if (agency != '') {
          this.PlannerData.push({ budget: totalBudhet, actual: totalActual, differnce: (totalBudhet - totalActual), agenyName: agency })
        }
      })
    }
    else {
      this.location.forEach(m => {
        totalBudhet = 0;
        totalActual = 0;
        agency = '';
        DailyDataViaAgency = new Array<PlannerDashboard>();
        DailyDataViaAgency = this.filterdashboardPlanner.filter(z => z.agencyId === m);
        DailyDataViaAgency.forEach(i => {
          totalBudhet = totalBudhet + (!isNaN(i.totalPlannedBudgetAmount) ? i.totalPlannedBudgetAmount : 0);
          totalActual = totalActual + (!isNaN(i.totalActualAmount) ? i.totalActualAmount : 0);
          agency = i.agencyName;
        })
        if (agency != '') {
          this.PlannerData.push({ budget: totalBudhet, actual: totalActual, differnce: (totalBudhet - totalActual), agenyName: agency })
        }
      })
    }
    if (this.PlannerData.length > 0) {
      this.CreateCartDataForDailyPolicyCount();
    }
    else {
      this.pagerPolicyCount = {};
      this.pagedPolicyCount = [];
      this.PlannerData = []; 
      
    }
  }

  

  CreateCartDataForDailyPolicyCount() {
    this.PolicyCountChardata = new Array<{ _data: any, agenyName: string }>();
    let DailyPolicyCountChardata: any;
    this.PlannerData.forEach(m => {
      DailyPolicyCountChardata = {};
      DailyPolicyCountChardata = {
        labels: ['Budget', 'Actual', 'Variance'],
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            borderColor: '#1E88E5',
            data: [Math.round(m.budget), Math.round(m.actual), Math.round((m.budget - m.actual))]
          }
        ]
      }
      this.PolicyCountChardata.push({ _data: DailyPolicyCountChardata, agenyName: m.agenyName });
    })

      this.setPolicyCountPaging(1);
  }



  setPolicyCountPaging(page: number) {
    if (page < 1 || page > this.pagerPolicyCount.totalPages) {
      return;
    }
    // get pager object from service
    this.pagerPolicyCount = this._pageservice.getPayrollPager(this.PolicyCountChardata.length, page, 10);
    // get current page of items
    this.pagedPolicyCount = this.PolicyCountChardata.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);
  }

  Navigate(e) {
    if (e === "DailyView") {
      this.router.navigateByUrl('/daily-view')
    }
    else if (e === "MonthlyView") {
      this.router.navigateByUrl('/monthly-view')
    }
    else if (e === "Dashboard") {
      this.router.navigateByUrl('/plannerdashboard')
    }
  }
}
