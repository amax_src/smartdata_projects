import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerdashboardComponent } from './plannerdashboard.component';

describe('PlannerdashboardComponent', () => {
  let component: PlannerdashboardComponent;
  let fixture: ComponentFixture<PlannerdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlannerdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
