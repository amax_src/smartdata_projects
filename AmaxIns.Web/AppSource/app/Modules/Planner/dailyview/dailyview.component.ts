import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as NProgress from 'nprogress';
import { CalendarService } from '../../Services/calendarService';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { MessageService, SelectItem } from 'primeng/api';
import { authenticationService } from '../../../login/authenticationService.service';
import { DateService } from '../../Services/dateService';
import { PlannerActivity, PlannerHours, PlannerDate, PlannerBudget, PlannerActual, PlannerActualDetails, ActualPolicyDetails, DailyView } from '../../../model/plannerModel';
import { Location } from '../../../model/location';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-dailyview',
  templateUrl: './dailyview.component.html',
  styleUrls: ['./dailyview.component.scss'],
  providers: [DatePipe]
})
export class DailyviewComponent implements OnInit, AfterViewChecked {

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }
  showLoader: boolean = true;
  selectmonthindex: number = 0;

  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<Location> = new Array<Location>();
  listDailyView: Array<DailyView> = new Array<DailyView>();
  filterDailyView: Array<DailyView>= new Array<DailyView>();

  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<string> = new Array<string>();
  CurrentMonth: string = "";
  myDate = new Date();
  totalBudget: number = 0;
  totalActual: number = 0;

  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private datePipe: DatePipe
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];



  ngOnInit() {
    let dateObj = new Date();
    let myDate = (dateObj.getUTCFullYear()) + "/" + (dateObj.getMonth() + 1) + "/" + (dateObj.getUTCDate());
    this.CurrentMonth = this.datePipe.transform(myDate, 'MMMM');

    this.listDailyView = [];
    this.showLoader = true;
    this.getMonth();
  }

  GetAllLocations(e) {
    this.setLocation = [];
    this.selectedlocation = [];
    this.location = e;
    if (this.location.length === 1 && this.authenticationService.userRole === "storemanager") {
      this.selectedlocation.push(this.location[0].agencyId);
    }
    this.selectedlocation.push(this.location[0].agencyId);
    this.setLocation.push(this.location[0].agencyId);

    if (this.selectedlocation && this.selectedmonth) {
      this.GetAllDailyView();
    }
  }

  loaderStatusChanged(e) {
    //
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    if (this.selectedlocation.length > 1) {
      this.selectedlocation = [];
      this.setLocation = [];
    }
    else if (this.selectedlocation.length == 1) {
      this.GetAllDailyView();
    }
  }

  getMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue('2021').subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];

    if (this.selectedmonth && this.selectedlocation) {
      this.getDate();
    }
  }

  onMonthchange(e) {
    this.getDate();
  }

  getDate() {
    this.selectedDate = [];
    this.dateSelectItem = []
    this._DateService.GetDatesByMonth(this.selectedmonth,'2022').subscribe(x => {
      x.forEach(i => {
        this.dateSelectItem.push(
          {
            label: i, value: i
          })
      })

      if (this.CurrentMonth == this.selectedmonth ) {
        this.selectedDate.push(this.datePipe.transform(this.myDate, 'MM/dd/yyyy').toString());
        this.GetAllDailyView();
      }
      else if (this.dateSelectItem.length > 0) {
        this.selectedDate.push(this.dateSelectItem[0].value);
        this.GetAllDailyView();
      }

    })
  }

  onDateChange() {
    if (this.selectedDate.length > 1) {
      this.selectedDate = [];
    }
    else if (this.selectedDate.length == 1) {
      //this.filterDashboard();
    }
    this.filterDashboard();
  }

  GetAllDailyView() {
    this.showLoader = true;
    this.calService.GetDailyViewReport(this.selectedlocation[0], this.selectedmonth,"2021").subscribe(x => {
      this.listDailyView = x;
      console.log("this.listDailyView", this.listDailyView);
      this.filterDashboard();
      this.showLoader = false;
    })
  }

  filterDashboard() {
    this.totalBudget = 0;
    this.totalActual = 0;

    this.filterDailyView = new Array<DailyView>();
    let DatefilterDailyView: Array<DailyView> = new Array<DailyView>();
    if (this.selectedDate && this.selectedDate.length > 0) {
      this.selectedDate.forEach(i => {
        let filterDilydata = this.listDailyView.filter(m => m.plannerDate === i);
        filterDilydata.forEach(m => {
          DatefilterDailyView.push(m);
        })
      })
    }
    if (this.selectedDate && this.selectedDate.length > 0) {
      this.filterDailyView = DatefilterDailyView;
    }


    this.totalBudget = this.filterDailyView.reduce((acc, cur) => acc + cur.totalPlannedBudgetAmount, 0);
    this.totalActual = this.filterDailyView.reduce((acc, cur) => acc + cur.totalActualAmount, 0);
  }


  Navigate(e) {
    if (e === "DailyView") {
      this.router.navigateByUrl('/daily-view')
    }
    else if (e === "MonthlyView") {
      this.router.navigateByUrl('/monthly-view')
    }
    else if (e === "Dashboard") {
      this.router.navigateByUrl('/plannerdashboard')
    }
  }

}
