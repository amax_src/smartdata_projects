import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { MultiSelectModule } from 'primeng/multiselect';
import { MatTabsModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { CalendarComponent } from '../Planner/PlannerRequest/calendar.component';
import { CalendarRoutingModule } from './calendar-routing.module';
import { FullCalendarModule } from '@fullcalendar/angular';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { ActualEntryComponent } from './actual-entry/actual-entry.component';
import { PlannerdashboardComponent } from './plannerdashboard/plannerdashboard.component';
import { ChartModule } from 'primeng/chart';
import { DailyviewComponent } from './dailyview/dailyview.component';
import { MonthlyViewComponent } from './monthly-view/monthly-view.component';
import { ActivityDashboardComponent } from './activity-dashboard/activity-dashboard.component'

@NgModule({
  imports: [SharedModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    FullCalendarModule,
    DialogModule,
    CalendarRoutingModule,
    FormsModule, ReactiveFormsModule,
    ToastModule,
    ButtonModule,
    MultiSelectModule,
    MatTabsModule,
    ChartModule
  ],
  declarations: [CalendarComponent, ActualEntryComponent, PlannerdashboardComponent, DailyviewComponent, MonthlyViewComponent, ActivityDashboardComponent],
  exports: []
})
export class CalendarModule { }
