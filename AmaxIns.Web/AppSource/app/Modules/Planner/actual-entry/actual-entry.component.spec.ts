import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualEntryComponent } from './actual-entry.component';

describe('ActualEntryComponent', () => {
  let component: ActualEntryComponent;
  let fixture: ComponentFixture<ActualEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
