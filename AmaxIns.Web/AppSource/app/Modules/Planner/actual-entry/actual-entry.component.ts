import { FullCalendarComponent } from '@fullcalendar/angular';
import { Component, ViewChild, Renderer2, AfterViewChecked, ElementRef, HostListener } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import * as NProgress from 'nprogress';
import interactionPlugin from '@fullcalendar/interaction';
import { OnInit } from '@angular/core';
import { Location } from '../../../model/location';
import * as _ from 'underscore';
import { FormBuilder } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { authenticationService } from '../../../login/authenticationService.service';
import { DateService } from '../../Services/dateService';
import { PlannerActivity, PlannerHours, PlannerDate, PlannerBudget, PlannerActual, PlannerActualDetails, ActualPolicyDetails } from '../../../model/plannerModel';
import { CalendarService } from '../../Services/calendarService';
import { PageService } from '../../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-actual-entry',
  templateUrl: './actual-entry.component.html',
  styleUrls: ['./actual-entry.component.scss']
})
export class ActualEntryComponent implements OnInit, AfterViewChecked {



  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  singleLocationSelected: boolean = true;
  PageName: string;
  showLoader: boolean = true;
  createPlan: boolean = false;
  plannerBudgetId: number = 0;
  isLocked: boolean = false;
  selectmonthindex: number = 0;
  innerHeight: number = 0;
  innercontainer: string = "400px";
  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<Location> = new Array<Location>();
  listEventHours: Array<PlannerHours> = new Array<PlannerHours>();
  listOfActualPlanner: Array<PlannerActual> = new Array<PlannerActual>();

  plannerActualDetails: PlannerActualDetails = new PlannerActualDetails();

  actualPolicyDetails: ActualPolicyDetails = new ActualPolicyDetails();

  //selectedlocation: Array<number> = new Array<number>();
  selectedlocation: number = 0;
  setLocation: number = 0;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";



  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private _pageservice: PageService
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  @ViewChild(RmZmLocationDropdownComponent) child;

  ngOnInit() {
    this.fillYear();
    this.PageName = "Actual Entry";
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 256);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.listOfActualPlanner = new Array<PlannerActual>();
    this.showLoader = true;
    this.getMonth();
    this.BindEventHours();

  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 256);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  GetAllLocations(e) {
    this.setLocation =0;
    this.selectedlocation = 0;
    this.location = e;
    if (this.location.length === 1 && this.authenticationService.userRole === "storemanager") {
      this.selectedlocation=this.location[0].agencyId;
    }
    this.selectedlocation=this.location[0].agencyId;
    this.setLocation=this.location[0].agencyId;
    if (this.selectedlocation>0) {
      this.GetAllActualPlanner();
    }
  }
  loaderStatusChanged(e) {
    //
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  //LocationChanged(e) {
  //  console.log("Location ", e);
  //  this.selectedlocation = [];
  //  this.selectedlocation = e;

    
  //  if (this.selectedlocation.length > 1) {
  //    var singleAgencyId = this.selectedlocation[this.selectedlocation.length - 1];
  //    this.selectedlocation = [];
  //    this.setLocation = [];

  //    this.selectedlocation.push(singleAgencyId);
  //    this.setLocation.push(singleAgencyId);
  //  }

  //   if (this.selectedlocation.length == 1) {
  //    this.GetAllActualPlanner();
  //  }
  //}

  LocationChanged(e) {
    console.log("Location ", e);
    this.selectedlocation = 0;
    this.selectedlocation = e;
    this.setLocation = this.selectedlocation;
 
    //if (this.selectedlocation>0) {
    //  this.GetAllActualPlanner();
    //}
  }

  SetSingleLocation(e) {
    this.setLocation = e;
    this.selectedlocation = e;
    if (this.selectedlocation > 0) {
      //this.GetAllActualPlanner();
    }
  }

    fillYear() {
        this.YearSelectItem = [];
        this.YearSelectItem.push(
            {
                label: '2021', value: '2021'
            })
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
        this.YearSelectItem.push(
            {
                label: '2023', value: '2023'
            })
    }

  SearchReport() {
    this.GetAllActualPlanner();
  }

  filterReset() {
    this.child.filterReset();
      this.year = (new Date()).getFullYear().toString();
    this.getMonth();
    this.GetAllActualPlanner();
  }

  getMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];
  }

  onMonthchange(e) {
    //this.GetAllActualPlanner();
  }

  onYearchange() {
    this.getMonth();
  }

  GetAllActualPlanner() {
    this.showLoader = true;
    this.listOfActualPlanner = new Array<PlannerActual>();
    this.calService.GetAllPlannerActual(this.selectedlocation, this.selectedmonth,this.year).subscribe(x => {
      this.listOfActualPlanner = x;
      this.showLoader = false;
      this.setPolicyCountPaging(1);
    })
  }

  UpdateActual(plannerBudgetId, isLocked) {
    this.plannerActualDetails = new PlannerActualDetails();
    this.showLoader = true;
    this.calService.GetSingelActual(plannerBudgetId).subscribe(x => {
      this.plannerActualDetails.actualDetail = x.actualDetail;
      this.plannerActualDetails.plannerBudget = x.plannerBudget;
      this.plannerActualDetails.listActualPolicyDetails = x.listActualPolicyDetails;
      console.log(" this.plannerActualDetails", this.plannerActualDetails);
      this.createPlan = true;
      this.plannerBudgetId = plannerBudgetId;
      this.isLocked = isLocked;
      if (this.plannerActualDetails.listActualPolicyDetails.length == 0) {
        this.AddRow();
      }
      this.showLoader = false;
    })
  }

  AddRow() {
    this.actualPolicyDetails = new ActualPolicyDetails();
    this.actualPolicyDetails.plannerBudgetId = this.plannerBudgetId;
    this.actualPolicyDetails.policyDetailId = 0;
    this.actualPolicyDetails.policyNumber = '';
    this.actualPolicyDetails.agencyFee = 0;
    this.actualPolicyDetails.premiumAmount = 0;
    this.actualPolicyDetails.loginUserId = 1;
    this.plannerActualDetails.listActualPolicyDetails.push(this.actualPolicyDetails);
  }

  Delete(obj) {
    console.log(obj);
    if (obj.policyDetailId == 0) {
      const index: number = this.plannerActualDetails.listActualPolicyDetails.indexOf(obj);
      if (index != -1) {
        this.plannerActualDetails.listActualPolicyDetails.splice(index, 1);
      }
    } else {
    }
  }

  DeletePlanner(plannerBudgetId, isLocked) {
    this.showLoader = true;
    if (confirm("Are you sure to delete this activity")) {
      this.calService.DeleteBudgetPlanner(plannerBudgetId).subscribe(x => {
        if (x > 0) {
          this.messageService.add({ severity: 'success', summary: 'Done !', detail: 'Planner Deleted Successfully !' });
          this.GetAllActualPlanner();
        }
        this.showLoader = false;
      })
    }
    this.showLoader = false;
  }

  BindEventHours() {
    this.calService.GetAllHours().subscribe(x => {
      this.listEventHours = x;
    })
  }
  CancelActual() {
    this.createPlan = false;
    this.plannerActualDetails = new PlannerActualDetails();
  }

  SaveUpdateActual() {
    this.showLoader = true;
    this.calService.SaveUpdateActual(this.plannerActualDetails).subscribe(x => {
      if (x > 0) {
        this.GetAllActualPlanner();
        this.createPlan = false;
        this.showLoader = false;

      }
    })
  }

  numberOnly(event, plannedBudgetAmount): boolean {
   
    if (plannedBudgetAmount.toString().length > 9) {
      this.messageService.add({ severity: 'error', detail: 'Please enter maximum 10 digit number' });
      return false;
    }

    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    return true;

  }


  pagerPolicyCount: any = {};
  pagedPolicyCount: any[];

  setPolicyCountPaging(page: number) {
    // if (page < 1 || page > this.pagerPolicyCount.totalPages) {
    //   return;
    // }

    // get pager object from service
    this.pagerPolicyCount = this._pageservice.getPayrollPager(this.listOfActualPlanner.length, page, 100);

    // get current page of items
    this.pagedPolicyCount = this.listOfActualPlanner.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);
  }
}
