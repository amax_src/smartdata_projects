import { FullCalendarComponent } from '@fullcalendar/angular';
import { Component, ViewChild, Renderer2, AfterViewChecked, HostListener } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import * as NProgress from 'nprogress';
import interactionPlugin from '@fullcalendar/interaction';
import { OnInit } from '@angular/core';
import { Location } from '../../../model/location';
import * as _ from 'underscore';
import { FormBuilder } from '@angular/forms';
import { CalendarService } from '../../Services/calendarService';
import { calendarDisplayModel } from '../../../model/Calendar/calendarDisplayModel';
import { calendarModel } from '../../../model/Calendar/calendarModel';
import { MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { calendarFormModel } from '../../../model/Calendar/calendarFormModel';
import { authenticationService } from '../../../login/authenticationService.service';

import { DateService } from '../../Services/dateService';
import { PlannerActivity, PlannerHours, PlannerDate, PlannerBudget } from '../../../model/plannerModel';
import { MatTabGroup } from '@angular/material';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-Calendar',
  templateUrl: './Calendar.component.html',
  styleUrls: ['./Calendar.component.scss'],
  providers: [DatePipe]
})

export class CalendarComponent implements OnInit, AfterViewChecked {
  innerHeight: number = 0;
  innercontainer: string = "400px";
  showLoader: boolean = true;
  eventId: number = 0;
  eventDate: string = '';
  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<Location> = new Array<Location>();
 
  selectedlocation: number = 0;
  setLocation: number = 0;
  singleLocationSelected: boolean = true;
//-----------------------------------------------------------------------------
  CreatelistPlan: Array<PlannerBudget> = new Array<PlannerBudget>();
  filterListPlan: Array<PlannerBudget> = new Array<PlannerBudget>();
  viewLastPlanner: Array<PlannerBudget> = new Array<PlannerBudget>();

  monthSelectItem: SelectItem[] = [];
  selectmonthindex: number = 0;
  selectedmonth: string = "";
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  listDate: Array<PlannerDate> = new Array<PlannerDate>();
  listActivity: Array<PlannerActivity> = new Array<PlannerActivity>();
  listEventHours: Array<PlannerHours> = new Array<PlannerHours>();

  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();
  updatePlanObject: PlannerBudget;
  createPlan: boolean;
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  isEditorAdd: boolean = false;
  isROMZM: boolean = false;
  currentDate: number = 0;
  PageName: string = '';
  warningPopup: boolean = false;

  ngOnInit() {
this.PageName="Planner Request";
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.tabGroup.selectedIndex = 0;
    this.updatePlanObject = new PlannerBudget();

    this.BinDateandMonths();
    this.BindActivity();
    this.BindEventHours();
  }
  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private datePipe: DatePipe
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');

    let myDate = new Date();
    this.currentDate =parseInt(this.datePipe.transform(myDate, 'dd'));
    this.isEditorAdd = false; this.isROMZM = false;

    if (authenticationService.userRole === "headofdepratment" ) {
      this.isEditorAdd = false;
    }
    if (authenticationService.userRole === "regionalmanager") {
      this.isEditorAdd = true; this.isROMZM = true;
    }
    if (authenticationService.userRole === "zonalmanager" && this.currentDate<=28) {
      this.isEditorAdd = true; this.isROMZM = true;
    }
    if (authenticationService.userRole === 'storemanager' && this.currentDate<=25) {
      this.isEditorAdd = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  loaderStatusChanged(e) {
    //
  }

  BinDateandMonths() {
    this._DateService.GetDates().subscribe(_date => {
      _date.forEach(i => {
        this.listDate.push(
          {
            plnDate:i
          })
      })
    });
  }

  BindActivity() {
    this.calService.GetAllActivity().subscribe(x => {
      let _temActivity = x;
      if (this.authenticationService.userRole === "regionalmanager" || this.authenticationService.userRole === "zonalmanager") {
        this.listActivity = x.filter(x => x.activityName == 'Sales Training');
        
      }
      else {
        this.listActivity = x;
      }
      
    })
  }

  BindEventHours() {
    this.calService.GetAllHours().subscribe(x => {
      this.listEventHours = x;
    })
  }

  GetAllLocations(e) {
    this.setLocation =0;
    this.selectedlocation = 0;
    this.location = e;
   
    this.selectedlocation = this.location[0].agencyId;
   
    if (this.location) {
      this.selectedlocation=this.location[0].agencyId;
      this.setLocation=this.location[0].agencyId;
      this.getMonth();
    }
    this.AddNewRow();
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  LocationChanged(e) {
    this.CreatelistPlan = new Array<PlannerBudget>();
    this.selectedlocation = 0;
    this.selectedlocation = e;
    if (this.selectedlocation > 0) {

      this.selectedlocation = 0;
      this.setLocation = 0;

      this.selectedlocation = e;
      this.setLocation = e;

    }

    if (this.selectedlocation > 0) {
      this.filterPlan();
    }
    if (this.selectedmonth) {
      this.GetAllPlanner();
    }
  }

  SetSingleLocation(e) {
    this.CreatelistPlan = new Array<PlannerBudget>();
    this.setLocation = e;
    this.selectedlocation = e;
    if (this.selectedlocation > 0) {
      this.filterPlan();
    }
    if (this.selectedmonth) {
      this.GetAllPlanner();
    }
  }

  //this.messageService.add({ severity: 'success', summary: 'Done !', detail: 'Event Added Successfully !' });
//this.messageService.add({ severity: 'error', detail: 'Something Went Wrong !' });

  //-----------------------------------------------------------

  getMonth() {
    this._DateService.GetMonthPlanner().subscribe(m => {
      this.monthSelectItem = [];
      this.monthSelectItem.push(
        {
          label: m[0].name, value: m[0].name
        })
      this.selectedmonth = m[0].name;
      this.GetAllPlanner();
    })
  }
  

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  filterPlan() {
    this.filterListPlan = this.CreatelistPlan.filter(x => x.agencyId === this.selectedlocation);
    if (this.filterListPlan.length == 0 && this.selectedlocation>0) {
      this.AddNewRow();
    }
  }

  AddNewRow() {
    //this.CreatelistPlan = new Array<PlanObject>();
    if (this.selectedlocation > 0) {
      var _data = new PlannerBudget();
      var leta = this.CreatelistPlan.length;
      _data.plannerBudgetId = 0;
      _data.plannerDate = '';
      _data.activity = '';
      _data.plannedHours = 0;
      _data.plannedDetails = '';
      _data.plannedBudgetAmount = 0;
      _data.agencyId = this.selectedlocation;
      _data.otherActivity = '';
      if (this.CreatelistPlan.length > 0) {
        var _indedata = this.CreatelistPlan[this.CreatelistPlan.length - 1];
        _data.plannerDate = _indedata.plannerDate;
        _data.activity = _indedata.activity;
      }
    
      if ((this.CreatelistPlan.filter(x => x.plannerDate == '' || x.activity == ''
        || x.plannedHours <= 0).length > 0) && this.CreatelistPlan.length > 0) {
        this.messageService.add({ severity: 'error', detail: 'Please enter the value' });
      }
      else {
        this.CreatelistPlan.push(_data);
        this.filterPlan();
      }
    }
    else {
      this.messageService.add({ severity: 'error', detail: 'Please select a location first.' });
    }
   
  }

  DeleteRow(index:number) {
    this.CreatelistPlan.splice(index, 1);
    this.filterPlan();
  }

  numberOnly(event, plannedBudgetAmount): boolean {
    console.log("plannedBudgetAmount", plannedBudgetAmount.toString().length);
    if (plannedBudgetAmount.toString().length > 9) {
      this.messageService.add({ severity: 'error', detail: 'Please enter maximum 10 digit number' });
      return false;
    }

    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
   
    return true;

  }

  SavePlan() {
    
    if (this.CreatelistPlan.filter(x => x.plannerDate == '' || x.activity == ''  || x.plannedHours <= 0).length > 0) {
      this.messageService.add({ severity: 'error', detail: 'Please enter the value' });
    }
    else {
      this.calService.SaveUpdatePlanner(this.CreatelistPlan).subscribe(x => {
        if (x > 0) {
          this.CreatelistPlan = new Array<PlannerBudget>();
          this.messageService.add({ severity: 'success', summary: 'Done !', detail: 'Planner Added Successfully !' });
          this.tabGroup.selectedIndex = 0;
          this.GetAllPlanner();
        }
      })
    }
  }

  GetAllPlanner() {
    this.showLoader = true;
    this.viewLastPlanner = new Array<PlannerBudget>();
    this.calService.GetAllPlanner(this.selectedlocation, this.selectedmonth).subscribe(x => {
      this.viewLastPlanner = x;
      this.showLoader =false;
    })
    if (this.selectedlocation == 0) {
      this.showLoader = false;
    }
  }

  EditUpdate(index) {
    this.updatePlanObject = new PlannerBudget();
    this.updatePlanObject.plannerBudgetId = this.viewLastPlanner[index].plannerBudgetId;
    this.updatePlanObject.activity = this.viewLastPlanner[index].activity;
    this.updatePlanObject.plannedBudgetAmount = this.viewLastPlanner[index].plannedBudgetAmount;
    this.updatePlanObject.plannerDate = this.viewLastPlanner[index].plannerDate;
    this.updatePlanObject.plannedDetails = this.viewLastPlanner[index].plannedDetails;
    this.updatePlanObject.plannedHours = this.viewLastPlanner[index].plannedHours;
    this.updatePlanObject.otherActivity = this.viewLastPlanner[index].otherActivity;
    this.createPlan = true;
  }

  UpdatePlanner() {
    let _CreatelistPlan = new Array<PlannerBudget>();
    _CreatelistPlan.push(this.updatePlanObject);
    this.calService.SaveUpdatePlanner(_CreatelistPlan).subscribe(x => {
      if (x > 0) {
        this.updatePlanObject = new PlannerBudget();
        this.messageService.add({ severity: 'success', summary: 'Done !', detail: 'Planner Updated Successfully !' });
        this.GetAllPlanner();
      }
    })
    this.createPlan = false;
  }

  DeletePlanner(plannerBudgetId) {
    if (confirm("Are you sure to delete event activity")) {
      this.calService.DeleteBudgetPlanner(plannerBudgetId).subscribe(x => {
        if (x > 0) {
          this.updatePlanObject = new PlannerBudget();
          this.messageService.add({ severity: 'success', summary: 'Done !', detail: 'Planner Deleted Successfully !' });
          this.GetAllPlanner();
        }
      })
    }
  }

  CancelPlanner() {
    this.updatePlanObject = new PlannerBudget();
    this.createPlan = false;
  }

  onTabChanged(index) {
    if (index == 1) {
      this.CreatelistPlan = new Array<PlannerBudget>();
      this.AddNewRow();
    }
  }

  OpenWarningPopup() {
    this.warningPopup = true;
  }
  ClosewarningPopup() {
    this.warningPopup = false;
  }
}

