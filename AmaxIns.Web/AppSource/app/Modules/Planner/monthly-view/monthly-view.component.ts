import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as NProgress from 'nprogress';
import { CalendarService } from '../../Services/calendarService';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { MessageService, SelectItem } from 'primeng/api';
import { authenticationService } from '../../../login/authenticationService.service';
import { DateService } from '../../Services/dateService';
import { PlannerActivity, PlannerHours, PlannerDate, PlannerBudget, PlannerActual, PlannerActualDetails, ActualPolicyDetails, DailyView } from '../../../model/plannerModel';
import { Location } from '../../../model/location';
import { DatePipe } from '@angular/common';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { calendarDisplayModel } from '../../../model/Calendar/calendarDisplayModel';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { Months } from '../../../model/Reports';
import { formatDate } from '@fullcalendar/core'
import { RmZmLocationDropdownComponent } from '../../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-monthly-view',
  templateUrl: './monthly-view.component.html',
  styleUrls: ['./monthly-view.component.scss'],
  providers: [DatePipe]
})
export class MonthlyViewComponent implements OnInit {
  singleLocationSelected: boolean = true;
  showLoader: boolean = true;
  selectmonthindex: number = 0;
  tabIndex: number = 0;

    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<Location> = new Array<Location>();
  listDailyView: Array<DailyView> = new Array<DailyView>();
  filterDailyView: Array<DailyView> = new Array<DailyView>();
  popupfilterDailyView: Array<DailyView> = new Array<DailyView>();

  selectedlocation: number = 0;
  setLocation: number = 0;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  createPlan: boolean = false;
  totalBudget: number = 0;
  totalActual: number = 0;
  PageName: string = '';
  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private datePipe: DatePipe
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  listOfMonths: Array<Months> = [{ monthId: '1', monthName: 'January' }, { monthId: '2', monthName: 'February' }, { monthId: '3', monthName: 'March' },
  { monthId: '4', monthName: 'April' }, { monthId: '5', monthName: 'May' }, { monthId: '6', monthName: 'June' },
  { monthId: '7', monthName: 'July' }, { monthId: '8', monthName: 'August' }, { monthId: '9', monthName: 'September' },
  { monthId: '10', monthName: 'October' }, { monthId: '11', monthName: 'November' }, { monthId: '12', monthName: 'December' }];

  calendarEvents: Array<any>;
  calendarWeeklyEvents: Array<any>;
  eventArray: Array<calendarDisplayModel>;
  calendarDailyEvents: Array<any>;
  calendarAPI: any;

  @ViewChild('fullcalendar') calendarComponent: FullCalendarComponent;
  @ViewChild('fullcalendarWeekly') fullcalendarWeekly: FullCalendarComponent;
  @ViewChild('fullcalendarDaily') fullcalendarDaily: FullCalendarComponent;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeeklyPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarDailyPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  //calendarWeekends = true;
  dayOptions: any;
  weeklyOptions: any;
  monthlyOptions: any;
  @ViewChild(RmZmLocationDropdownComponent) child;

  ngOnInit() {
    this.tabIndex = 0;
    this.PageName = "Calendar";
    this.calendarEvents = new Array<any>();
    this.calendarWeeklyEvents = new Array<any>();
    this.calendarDailyEvents = new Array<any>();
    let tempArray = new Array<any>();
    this.eventArray = new Array<calendarDisplayModel>();

    //this.calendarComponent.selectable = false;
    //this.calendarComponent.contentHeight = 766;

    //this.fullcalendarWeekly.selectable = false;
    //this.fullcalendarWeekly.contentHeight = 766;

    this.showLoader = false;
    this.fillYear();
    this.getMonth();


    this.dayOptions = {
      editable: true,
      customButtons: {
        next: {
          click: () => this.nextPage()
        },
        prev: {
          click: () => this.prevPage()
        },
        today: {
          text: "Today",
          click:()=>this.todayPage()
        }
      },
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'timeGridDay,timeGridWeek,dayGridMonth'
      },
      plugins: [dayGridPlugin, interactionPlugin, timeGrigPlugin]
    };

  }
  today() {
    alert("Today");
  }



  GetAllLocations(e) {
    this.setLocation = 0;
    this.selectedlocation = 0;
    this.location = e;
    if (this.location.length === 1 && this.authenticationService.userRole === "storemanager") {
      this.selectedlocation = this.location[0].agencyId;
    }
    this.selectedlocation = this.location[0].agencyId;
    this.setLocation = this.location[0].agencyId;

    if (this.selectedlocation > 0 && this.selectedmonth) {
      this.GetAllMonthlyView();
    }
  }

  loaderStatusChanged(e) {
    //
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  LocationChanged(e) {
    this.selectedlocation = 0;
    this.selectedlocation = e;

    this.setLocation = e;

    if (this.selectedlocation > 0) {
      this.GetAllMonthlyView();
    }
  }

  SetSingleLocation(e) {
    this.setLocation = e;
    this.selectedlocation = e;
    if (this.selectedlocation > 0) {
      this.GetAllMonthlyView();
    }
  }

  fillYear() {
    this.YearSelectItem = [];
  
    this.YearSelectItem.push(
      {
        label: '2021', value: '2021'
      })
    this.YearSelectItem.push(
      {
        label: '2022', value: '2022'
        })
      this.YearSelectItem.push(
          {
              label: '2023', value: '2023'
          })
  }

  onYearchange() {
    this.getMonth();
  }

  filterReset() {
    this.child.filterReset();
    this.fillYear();
      this.year = (new Date()).getFullYear().toString();
    this.getMonth();
  }

  getMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue('2021').subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];
    this.GetAllMonthlyView();
  }

  onMonthchange(e) {
    //this.GetAllMonthlyView();
  }

  SearchReport() {
    this.GetAllMonthlyView();
  }



  GetAllMonthlyView() {
    this.listDailyView = [];
    this.showLoader = true;
    this.calService.GetDailyViewReport(this.selectedlocation, this.selectedmonth, this.year).subscribe(x => {
      this.listDailyView = x;

      this.showLoader = false;
      this.filterDashboard();
    })
  }

  filterDashboard() {
    let d = new Date();
    d.setDate(d.getUTCDate());
    console.log("getTodacccccy", d);

    var dd = new Date();

    var n = dd.getUTCDate();

    console.log("cccccccc", n);

    let date = new Date();
    date.setDate(date.getUTCDate());
    let current_date = this.datePipe.transform(date, 'dd');
    let index = this.monthNames.indexOf(this.selectedmonth)+1;
  
    if (index > 9) {
      this.calendarComponent.getApi().gotoDate(this.year+"-" + index + "-"+current_date.toString());
      this.fullcalendarWeekly.getApi().gotoDate(this.year +"-" + index + "-" + current_date.toString());
      this.fullcalendarDaily.getApi().gotoDate(this.year+"-" + index + "-" + current_date.toString());
    }
    else {
      this.calendarComponent.getApi().gotoDate(this.year +"-0" + index + "-" + current_date.toString());
      this.fullcalendarWeekly.getApi().gotoDate(this.year +"-0" + index + "-" + current_date.toString());
      this.fullcalendarDaily.getApi().gotoDate(this.year +"-0" + index + "-" + current_date.toString());
    }

    this.calendarEvents = [];
    this.calendarWeeklyEvents = [];
    this.calendarDailyEvents = [];
    this.eventArray = [];
   
    this.listDailyView.forEach(i => {
        if (i.isLocked) {
          this.eventArray.push(
            {
              id: i.plannerBudgetId,
              title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
              date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              backgroundColor: '#004a80 !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
            })
        }
        else if (!i.isCurrentDay) {
          this.eventArray.push(
            {
              id: i.plannerBudgetId,
              title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
              date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              backgroundColor: '#3ed23e !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
            })
        }
        else {
          this.eventArray.push(
            {
              id: i.plannerBudgetId,
              title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
              date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
              backgroundColor: '#c8be2b !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
            })
        }
    })
    this.calendarEvents = this.eventArray;
    this.calendarWeeklyEvents = this.eventArray;
    this.calendarDailyEvents = this.eventArray;
    //this.calendarAPI = this.fullcalendarDaily.getApi();
   
  }

  onEventClick(d) {
   
    this.popupfilterDailyView = [];

    let varObject = this.listDailyView.filter(x => x.plannerBudgetId == d.event.id)[0];
    console.log(varObject)
    console.log(this.datePipe.transform(varObject.monthlyPlannerDate, 'MM-dd-yyyy'))  
    this.popupfilterDailyView = this.listDailyView.filter(x => x.monthlyPlannerDate == varObject.monthlyPlannerDate);

    this.totalBudget = this.popupfilterDailyView.reduce((acc, cur) => acc + cur.totalPlannedBudgetAmount, 0);
    this.totalActual = this.popupfilterDailyView.reduce((acc, cur) => acc + cur.totalActualAmount, 0);
    this.createPlan = true;

  }

 
  CancelActual() {
    this.createPlan =false;
  }

  onTabChanged(index) {
    this.tabIndex = 0;
    this.GetAllMonthlyView();
    this.tabIndex = parseInt(index);
  }

  filterDashboardReCall(datetime: any) {
    let date = new Date();
    let current_date = this.datePipe.transform(datetime, 'dd');
    let index = this.monthNames.indexOf(this.selectedmonth) + 1;
    console.log("current_date", current_date);
    if (index > 9) {
      this.calendarComponent.getApi().gotoDate(this.year + "-" + index + "-" + current_date.toString());
      this.fullcalendarWeekly.getApi().gotoDate(this.year + "-" + index + "-" + current_date.toString());
      this.fullcalendarDaily.getApi().gotoDate(this.year + "-" + index + "-" + current_date.toString());
    }
    else {
      this.calendarComponent.getApi().gotoDate(this.year + "-0" + index + "-" + current_date.toString());
      this.fullcalendarWeekly.getApi().gotoDate(this.year + "-0" + index + "-" + current_date.toString());
      this.fullcalendarDaily.getApi().gotoDate(this.year + "-0" + index + "-" + current_date.toString());
    }

    this.calendarEvents = [];
    this.calendarWeeklyEvents = [];
    this.calendarDailyEvents = [];
    this.eventArray = [];

    this.listDailyView.forEach(i => {
      if (i.isLocked) {
        this.eventArray.push(
          {
            id: i.plannerBudgetId,
            title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
            date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            backgroundColor: '#004a80 !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
          })
      }
      else if (!i.isCurrentDay) {
        this.eventArray.push(
          {
            id: i.plannerBudgetId,
            title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
            date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            backgroundColor: '#3ed23e !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
          })
      }
      else {
        this.eventArray.push(
          {
            id: i.plannerBudgetId,
            title: i.activity + ' \n' + 'Hours: ' + i.plannedHours + ' \nBudget: $' + i.totalPlannedBudgetAmount + ' \nActual: $' + i.totalActualAmount,
            date: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            start: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            end: i.monthlyPlannerDate.replace('/', '-').replace('/', '-'),
            backgroundColor: '#c8be2b !important;height: auto;overflow-wrap: break-word;min-height: 30px;cursor: pointer;'
          })
      }
    })
    this.calendarEvents = this.eventArray;
    this.calendarWeeklyEvents = this.eventArray;
    this.calendarDailyEvents = this.eventArray;
    //this.calendarAPI = this.fullcalendarDaily.getApi();

  }

  LoadCalanderEvents(selectedlocation: any, monthName: any, datetime: any,year:string) {
    this.showLoader = true;
    this.selectedmonth = monthName;
   
    if (this.year != year) {
      this.year = year;
    }
    this.calService.GetDailyViewReport(selectedlocation, monthName, this.year).subscribe(x => {
      this.listDailyView = x;
      this.showLoader = false;
      this.filterDashboardReCall(datetime);
    })
  }

  //GetDataForWeekly(selectedlocation, startDate, endDate) {
  //  var param = {
  //    selectedlocation: selectedlocation,
  //    startDate: startDate,
  //    endDate: endDate
  //  }

  //  this.listDailyView = [];
  //  this.showLoader = true;
  //  this.calService.GetDataForWeekly(param).subscribe(x => {
  //    this.listDailyView = x;

  //    this.showLoader = false;
  //    this.filterDashboard();
  //  })
  //}

  LoadCalanderWeeklyEvents(selectedlocation: any, startDate: any, endDate: any, year: string, monthName:any,datetime:any) {
    this.showLoader = true;
    this.selectedmonth = monthName;

    if (this.year != year) {
      this.year = year;
    }

    var param = {
      selectedlocation: selectedlocation,
      startDate: startDate,
      endDate: endDate
    }

    this.calService.GetDataForWeekly(param).subscribe(x => {
      this.listDailyView = x;
      this.showLoader = false;
      this.filterDashboardReCall(datetime);
    })
  }

  todayPage() {
    if (this.tabIndex == 0) {
      this.fullcalendarDaily.getApi().today();
    }
    else if (this.tabIndex == 1) {
      this.fullcalendarWeekly.getApi().today();
    }
    else if (this.tabIndex == 2) {
      this.calendarComponent.getApi().today();
    }

    let date = new Date();
    date.setDate(date.getUTCDate());
    var _year = this.datePipe.transform(date, 'yyyy');
    var _monthIndexWeekly = this.datePipe.transform(date, 'MM');
    this.year = _year;
    var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthIndexWeekly))[0].monthName;
    this.selectedmonth = monthName;
    this.GetAllMonthlyView();
  }

  nextPage() {
    if (this.tabIndex == 0) {
      this.fullcalendarDaily.getApi().next();
      let calendarApi = this.fullcalendarDaily.getApi();

      console.log("Weekly_API", calendarApi);
      var _datetime = calendarApi.state.dateProfile.currentRange.end;
      var startDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var endDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var _year = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'yyyy');

      var _monthIndexWeekly = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM');
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthIndexWeekly))[0].monthName;
      var selectedlocation = this.selectedlocation;
      this.LoadCalanderWeeklyEvents(selectedlocation, startDate, endDate, _year.toString(), monthName, _datetime);
    }
    else if (this.tabIndex == 1) {
      this.fullcalendarWeekly.getApi().next();
      let calendarApi = this.fullcalendarWeekly.getApi();
      console.log("Weekly_API", calendarApi);
      var _datetime = calendarApi.state.dateProfile.currentRange.start;

      console.log("_datetime", _datetime);
      var startDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.start, 'MM-dd-yyyy');
      var endDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var _year = this.datePipe.transform(_datetime, 'yyyy');

      var _monthIndexPrevWeekly = this.datePipe.transform(_datetime.setDate(_datetime.getDate() + 1), 'MM');
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthIndexPrevWeekly))[0].monthName;
      var selectedlocation = this.selectedlocation;
      this.LoadCalanderWeeklyEvents(selectedlocation, startDate, endDate, _year.toString(), monthName, _datetime.setDate(_datetime.getDate()));
    }
    else if (this.tabIndex == 2) {
      this.calendarComponent.getApi().next();
      let calendarApi = this.calendarComponent.getApi();
      console.log("Monthly", calendarApi);

      var _monthEndDatetime = calendarApi.state.dateProfile.currentRange.end;
      console.log("_monthEndDatetime", _monthEndDatetime);
      var MonthYear = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'yyyy');
      var _monthName = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM');

      console.log("_monthName", _monthName); 
      console.log("Monthly_API", calendarApi);
      var selectedlocation = this.selectedlocation;
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthName))[0].monthName;
      this.LoadCalanderEvents(selectedlocation, monthName, _monthEndDatetime, MonthYear.toString());

    }
  }
  prevPage() {
    if (this.tabIndex == 0) {
      this.fullcalendarDaily.getApi().prev();
      let calendarApi = this.fullcalendarDaily.getApi();

      var _datetime = calendarApi.state.dateProfile.currentRange.end;
      var startDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var endDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var _year = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'yyyy');

      var _monthIndexWeekly = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM');
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthIndexWeekly))[0].monthName;
      var selectedlocation = this.selectedlocation;
      this.LoadCalanderWeeklyEvents(selectedlocation, startDate, endDate, _year.toString(), monthName, _datetime);

    }
    else if (this.tabIndex == 1) {
      this.fullcalendarWeekly.getApi().prev();
      let calendarApi = this.fullcalendarWeekly.getApi();

      var _datetime = calendarApi.state.dateProfile.currentRange.start;
      
      console.log("_datetime", _datetime);
      var startDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.start, 'MM-dd-yyyy');
      var endDate = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM-dd-yyyy');
      var _year = this.datePipe.transform(_datetime, 'yyyy');

      var _monthIndexPrevWeekly = this.datePipe.transform(_datetime.setDate(_datetime.getDate() + 1), 'MM');
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthIndexPrevWeekly))[0].monthName;
      var selectedlocation = this.selectedlocation;
      this.LoadCalanderWeeklyEvents(selectedlocation, startDate, endDate, _year.toString(), monthName, _datetime.setDate(_datetime.getDate()));

    }
    else if (this.tabIndex == 2) {
      this.calendarComponent.getApi().prev();
      let calendarApi = this.calendarComponent.getApi();
      //var datetime = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.start, 'dd');
      //let year = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.start, 'yyyy');

      //var _monthIndex = 0;
      //_monthIndex = calendarApi.state.currentDate.getMonth() + 1;

      //var selectedlocation = this.selectedlocation;
      //var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == _monthIndex)[0].monthName;
      //this.LoadCalanderEvents(selectedlocation, monthName, datetime, year);
      console.log("Monthly", calendarApi);
      var _monthEndDatetime = calendarApi.state.dateProfile.currentRange.end;
      console.log("_monthEndDatetime", _monthEndDatetime);
      var MonthYear = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'yyyy');
      var _monthName = this.datePipe.transform(calendarApi.state.dateProfile.currentRange.end, 'MM');

      console.log("_monthName", _monthName);
      console.log("Monthly_API", calendarApi);
      var _monthIndex = 0;
      _monthIndex = calendarApi.state.currentDate.getMonth() + 1;
      var selectedlocation = this.selectedlocation;
      var monthName = this.listOfMonths.filter(x => parseInt(x.monthId) == parseInt(_monthName))[0].monthName;
      this.LoadCalanderEvents(selectedlocation, monthName, _monthEndDatetime, MonthYear.toString());

    }
  }


}
