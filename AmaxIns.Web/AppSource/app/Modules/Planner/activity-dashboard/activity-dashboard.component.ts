import { Component, OnInit, AfterViewChecked, Renderer2, ViewChild } from '@angular/core';
import * as NProgress from 'nprogress';
import * as _ from 'underscore';
import { FormBuilder } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { authenticationService } from '../../../login/authenticationService.service';
import { CalendarService } from '../../Services/calendarService';
import { DateService } from '../../Services/dateService';
import { Location } from '../../../model/location';
import { PlannerDashboard, PalnnerActivityDetails, PlannerActivity } from '../../../model/plannerModel';
import { PageService } from '../../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-activity-dashboard',
  templateUrl: './activity-dashboard.component.html',
  styleUrls: ['./activity-dashboard.component.scss']
})
export class ActivityDashboardComponent implements OnInit, AfterViewChecked {
  PageName: string = "Planner > Activity Dashboard";

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"

  ];

    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];
  showLoader: boolean = true;
  selectedRegionalManager: [] = [];
  selectedzonalManager: [] = [];
  location: Array<number> = []
  plannerActivityCountChardata: Array<{ _data: any, activityName: string }>;
  plannerActivityTotalData: Array<{ _data: any, activityName: string }>;
  plannerGrandTotalData: Array<{ _data: any, activityName: string }>;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();

  PlannerData: Array<{ totalPlanned: number, totalCompleted: number, variance: number, activityName: string }> = new Array<{ totalPlanned: number, totalCompleted: number, variance: number, activityName: string }>();

  monthSelectItem: SelectItem[] = [];
  selectmonthindex: number = 0;
  selectedmonth: string = "";

  totalPlannerActivity: Array<PalnnerActivityDetails> = new Array<PalnnerActivityDetails>();
  agencyPlannerActivity: Array<PalnnerActivityDetails> = new Array<PalnnerActivityDetails>();
  agencyPlannerActivityTotalAgencyWise: Array<PalnnerActivityDetails> = new Array<PalnnerActivityDetails>();

  listActivity: Array<PlannerActivity> = new Array<PlannerActivity>();


  ActivityCountchartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25,
        bottom: 5
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },

    scales: {
      xAxes: [{
        ticks: {
          display: false
        }//this will remove all the x-axis grid lines
      }],
      yAxes: [{
        ticks: {
          display: false
        } //this will remove all the x-axis grid lines
      }]
    }


  }

  constructor(private formBuilder: FormBuilder,
    private calService: CalendarService,
    private messageService: MessageService,
    private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
    , private authenticationService: authenticationService, private _DateService: DateService, private _pageservice: PageService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.fillYear();
    this.BindActivity();
    this.totalPlannerActivity = new Array<PalnnerActivityDetails>();
    this.agencyPlannerActivity = new Array<PalnnerActivityDetails>();
    this.getMonth();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = [];
    e.forEach(i => {
      this.location.push(i.agencyId)
    });

    if (this.location && this.selectedmonth) {
      this.GetTotalActivity();
    }
  }

    fillYear() {
        this.YearSelectItem = [];
        this.YearSelectItem.push(
            {
                label: '2021', value: '2021'
            })
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
        this.YearSelectItem.push(
            {
                label: '2023', value: '2023'
            })
    }


  BindActivity() {
    this.calService.GetAllActivity().subscribe(x => {
      this.listActivity = x;
    })
  }
  GetTotalActivity() {
    this.showLoader = true;
    this.totalPlannerActivity = new Array<PalnnerActivityDetails>();
    this.calService.GetPlannerActivityTotal(this.selectedmonth,this.year, this.location).subscribe(x => {
      this.totalPlannerActivity = x;
      console.log(this.totalPlannerActivity);
      this.CreateCartDataForDailyPolicyCount();
      this.showLoader = false;
    })
  }

  GetAllActivity() {
    this.showLoader = true;
    this.agencyPlannerActivity = new Array<PalnnerActivityDetails>();
    this.calService.GetPlannerActivityAgencyWise(this.selectedmonth,this.year).subscribe(x => {
      this.agencyPlannerActivity = x;
      this.showLoader = false;
    })
  }

  SearchGetAllActivity() {
    this.showLoader = true;
    this.agencyPlannerActivity = new Array<PalnnerActivityDetails>();
    this.calService.GetPlannerActivityAgencyWise(this.selectedmonth, this.year).subscribe(x => {
      this.agencyPlannerActivity = x;
      this.filterAllActivityAgencyWise();
      this.showLoader = false;
    })
  }

  loaderStatusChanged(e) {
    //
  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.filterAllActivityTotal();
  }


  SearchReport() {
    if (this.selectedmonth) {
      //this.GetAllActivity();
      //this.GetTotalActivity();
      if (this.selectedlocation.length > 0) {
        this.SearchGetAllActivity();
      }
      else {
        this.GetTotalActivity();
      }
      //this.filterAllActivityTotal();
    }
  }

  filterReset() {
    this.child.filterReset();
    this.fillYear();
      this.year = (new Date()).getFullYear().toString();
    this.SearchgetMonth();
    if (this.selectedmonth) {
      if (this.selectedlocation.length > 0) {
        this.SearchGetAllActivity();
      }
      else {
        this.GetTotalActivity();
      }
    }
  }

  SearchgetMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue('2021').subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];

    if (this.selectedmonth) {
      //this.GetAllActivity();
    }
  }

  getMonth() {
    let months: Array<string> = new Array<string>();
    this._DateService.GetExtraMonthskeyvalue('2021').subscribe(m => {
      this.monthSelectItem = [];
      m.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.name
          })
      })
    })
    let monthcount = (new Date()).getMonth();
    this.selectmonthindex = (new Date()).getMonth();
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectmonthindex = x;
        break;
      }
    }
    this.selectedmonth = this.monthNames[this.selectmonthindex];

    if (this.selectedmonth) {
      this.GetAllActivity();
    }
  }


  onMonthchange(e) {
    //if (this.selectedmonth) {
    //  this.GetAllActivity();
    //  this.GetTotalActivity();
    //  this.filterAllActivityTotal();
    //}
  }

  CreateCartDataForDailyPolicyCount() {
    this.plannerActivityCountChardata = new Array<{ _data: any, activityName: string }>();
    this.plannerActivityTotalData = new Array<{ _data: any, activityName: string }>();
    this.plannerGrandTotalData = new Array<{ _data: any, activityName: string }>();

    let plannerActivityTotalData: any;
    let plannerActivityCountChardata: any;
    this.totalPlannerActivity.forEach(m => {
      plannerActivityCountChardata = {};
      plannerActivityCountChardata = {
        labels: ['Planned', 'Complete', 'Variance'],
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            borderColor: '#1E88E5',
            data: [m.totalPlanned, m.totalCompleted, m.variance]
          }
        ]
      }
      this.plannerActivityCountChardata.push({ _data: plannerActivityCountChardata, activityName: m.activityName });
    })
    
    let totalActivity = 0;
    let totalCompleted = 0;
    let totalVariance = 0;

    this.totalPlannerActivity.forEach(m => {
      if (m.activityName != 'Sales Training') {
        totalActivity = totalActivity + m.totalPlanned;
        totalCompleted = totalCompleted + m.totalCompleted;
        totalVariance = totalVariance + m.variance;
      }
    })

    plannerActivityTotalData = {};
    plannerActivityTotalData = {
      labels: ['Planned', 'Complete', 'Variance'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [totalActivity, totalCompleted, totalVariance]
        }
      ]
    }
    this.plannerActivityTotalData.push({ _data: plannerActivityTotalData, activityName: 'Activity Total' });

    // Grand Total

     totalActivity = 0;
     totalCompleted = 0;
     totalVariance = 0;

    this.totalPlannerActivity.forEach(m => {
        totalActivity = totalActivity + m.totalPlanned;
        totalCompleted = totalCompleted + m.totalCompleted;
        totalVariance = totalVariance + m.variance;
    })

    plannerActivityTotalData = {};
    plannerActivityTotalData = {
      labels: ['Planned', 'Complete', 'Variance'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [totalActivity, totalCompleted, totalVariance]
        }
      ]
    }
    this.plannerGrandTotalData.push({ _data: plannerActivityTotalData, activityName: 'Total' });

  }

  filterAllActivityTotal() {
    if (this.selectedlocation.length > 0) {
      this.filterAllActivityAgencyWise();
    }
    else {
      this.GetTotalActivity();
    }
  }

  filterAllActivityAgencyWise() {
    this.agencyPlannerActivityTotalAgencyWise = new Array<PalnnerActivityDetails>();
    let activity = new Array<PalnnerActivityDetails>();
    let objactivity = new PalnnerActivityDetails();
    if (this.agencyPlannerActivity.length > 0) {
      this.listActivity.forEach(j => {
        objactivity = new PalnnerActivityDetails();
        this.selectedlocation.forEach(i => {
          objactivity.totalPlanned = objactivity.totalPlanned + this.agencyPlannerActivity.filter(x => x.activityName == j.activityName && x.agencyId==i).reduce((acc, cur) => acc + cur.totalPlanned, 0);
          objactivity.totalCompleted = objactivity.totalCompleted + this.agencyPlannerActivity.filter(x => x.activityName == j.activityName && x.agencyId == i).reduce((acc, cur) => acc + cur.totalCompleted, 0);
          objactivity.variance = objactivity.variance + this.agencyPlannerActivity.filter(x => x.activityName == j.activityName && x.agencyId == i).reduce((acc, cur) => acc + cur.variance, 0);
          objactivity.activityName = j.activityName
        })
        activity.push(objactivity);
      })
      this.agencyPlannerActivityTotalAgencyWise = activity;
      this.bindfilterchart();
    }
  }

  bindfilterchart() {
    this.plannerActivityCountChardata = new Array<{ _data: any, activityName: string }>();
    this.plannerActivityTotalData = new Array<{ _data: any, activityName: string }>();
    this.plannerGrandTotalData = new Array<{ _data: any, activityName: string }>();

    let plannerActivityTotalData: any;
    let plannerActivityCountChardata: any;
    this.agencyPlannerActivityTotalAgencyWise.forEach(m => {
      plannerActivityCountChardata = {};
      plannerActivityCountChardata = {
        labels: ['Planned', 'Complete', 'Variance'],
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            borderColor: '#1E88E5',
            data: [m.totalPlanned, m.totalCompleted, m.variance]
          }
        ]
      }
      this.plannerActivityCountChardata.push({ _data: plannerActivityCountChardata, activityName: m.activityName });
    })

    let totalActivity = 0;
    let totalCompleted = 0;
    let totalVariance = 0;

    this.agencyPlannerActivityTotalAgencyWise.forEach(m => {
      if (m.activityName != 'Sales Training') {
        totalActivity = totalActivity + m.totalPlanned;
        totalCompleted = totalCompleted + m.totalCompleted;
        totalVariance = totalVariance + m.variance;
      }
    })

    plannerActivityTotalData = {};
    plannerActivityTotalData = {
      labels: ['Planned', 'Complete', 'Variance'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [totalActivity, totalCompleted, totalVariance]
        }
      ]
    }
    this.plannerActivityTotalData.push({ _data: plannerActivityTotalData, activityName: 'Activity Total' });

    // Grand Total***************************************
     totalActivity = 0;
     totalCompleted = 0;
     totalVariance = 0;

    this.agencyPlannerActivityTotalAgencyWise.forEach(m => {
     
        totalActivity = totalActivity + m.totalPlanned;
        totalCompleted = totalCompleted + m.totalCompleted;
        totalVariance = totalVariance + m.variance;
      
    })

    plannerActivityTotalData = {};
    plannerActivityTotalData = {
      labels: ['Planned', 'Complete', 'Variance'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [totalActivity, totalCompleted, totalVariance]
        }
      ]
    }
    this.plannerGrandTotalData.push({ _data: plannerActivityTotalData, activityName: 'Total' });
  }


  onTabChanged(index) {
    if (index == 1) {
     
    }
  }

}
