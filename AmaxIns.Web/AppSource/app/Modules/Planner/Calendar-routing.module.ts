import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CalendarComponent } from '../Planner/PlannerRequest/calendar.component';
import { ActualEntryComponent } from './actual-entry/actual-entry.component';
import { PlannerdashboardComponent } from './plannerdashboard/plannerdashboard.component';
import { DailyviewComponent } from './dailyview/dailyview.component';
import { MonthlyViewComponent } from './monthly-view/monthly-view.component';
import { ActivityDashboardComponent } from './activity-dashboard/activity-dashboard.component';


const routes: Routes = [
  { path: 'plannerRequest', component: CalendarComponent, canActivate: [LoginGuard] },
  { path: 'actualEntry', component: ActualEntryComponent, canActivate: [LoginGuard] },
  { path: 'plannerdashboard', component: PlannerdashboardComponent, canActivate: [LoginGuard] },
  { path: 'daily-view', component: DailyviewComponent, canActivate: [LoginGuard] },
  { path: 'monthly-view', component: MonthlyViewComponent, canActivate: [LoginGuard] },
  { path: 'activity-dashboard', component: ActivityDashboardComponent, canActivate: [LoginGuard] },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }
