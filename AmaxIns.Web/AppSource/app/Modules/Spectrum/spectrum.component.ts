import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import { spectrumService } from '../Services/spectrumService';
import { spectrumDailySummaryModel } from '../../model/spectrumDailySummaryModel';
import { spectrumDailyDetailModel } from '../../model/spectrumDailyDetailModel';
import { PageService } from '../Services/pageService';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';

@Component({
  selector: 'app-Spectrum',
  templateUrl: './spectrum.component.html',
  styleUrls: ['./spectrum.component.scss']
})

export class SpectrumComponent implements OnInit, AfterViewChecked {

  spectrumDailyDetailData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();
  FilterspectrumDailyDetailData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();

  sumAverageTalkTime: number = 0;
  CallNumberSelectItem: SelectItem[] = [];
  SelectedCallNumber: Array<string> = new Array<string>()
  outboundcalls: number = 0;
  dateRange: string = '';
  PageName: string = "Spectrum> Daily";
  location: Array<Location> = new Array<Location>();
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<number> = new Array<number>();
  singleLocation: number = 112;
  selectedRegionalManager: [] = [];
  spectrumDailySummaryData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
  FilterspectrumDailySummaryData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();

  defaultselectedDate: Date;

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  selectedmonthList: Array<string> = new Array<string>();
  selectedmonthListForMothlyHeading: Array<string> = new Array<string>();
  singleLocationSelected: boolean = false;

  chartheight: number = 300;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  currentMonth: string = "Feburay";
  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();

  extentionSelectItem: SelectItem[] = [];
  selectedextention: Array<string> = new Array<string>();
  selectedTabIndex: number = 0;

  SpectrumDailySummary: any;
  totalInbound: number = 0;
  totalOutbound: number = 0;
  totaltalktime: number = 0;

  DetailtotalInbound: number = 0;
  DetailtotalOutbound: number = 0;
  Detailtotaltalktime: number = 0;
  DetailOutBoundMints: number = 0;

  ChartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          //labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }
  firstLocation: number = 112;

  SetLocations: Array<number> = [];
  selectmonthindex: number;

  dailyPager: any = {};
  dailyPagedItems: any[];

  allLocations: Array<Location> = new Array<Location>();
  isFirstTimeLocationLoaded: boolean = true;
  
  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, public spectrumService: spectrumService, private _pageservice: PageService, private ApiLoad: ApiLoadTimeService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.getspectrumDailySummaryDate()
    this.getSpectrumDailySummary();
    //this.getSpectrumMonthlySummary();

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }



  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    // this.filterData();
  }


  SetSingleLocation(e) {
    this.singleLocation = e;
    this.filterData();
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    //this.filterData();
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    if (this.isFirstTimeLocationLoaded) {
      this.allLocations = e;
      this.isFirstTimeLocationLoaded = false;
    }
    this.location = e;
    this.singleLocation = e[0].agencyId;
    this.filterData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  getspectrumDailySummaryextention() {
    this.extentionSelectItem = []
    let dt: Array<string> = new Array<string>();
    dt = Array.from(new Set(this.FilterspectrumDailySummaryData.map(item => item.extension)));
    dt.forEach(i => {
      this.extentionSelectItem.push(
        {
          label: i, value: i
        })
    })

  }

  getspectrumDailySummaryDate() {
    let firstRecord: boolean = true;
    this.dateSelectItem = []
    //spectrumDailySummaryData: Array < spectrumDailySummaryModel >

    this.spectrumService.GetDates().toPromise().then(m => {
      let dt: Array<Date> = new Array<Date>();
      dt = m;
      dt.forEach(i => {
        if (firstRecord) {
          if (this.selectedDate.length === 0) {
            this.defaultselectedDate = i;
          }
          firstRecord = false;
        }

        this.dateSelectItem.push(
          {
            label: this.getDateString(i), value: i
          })

      })
      let date: string = dt[dt.length - 1].toString();
      let parts = date.split('-');
      this.currentMonth = this.monthNames[(parseInt(parts[0]) - 1)];
    });
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    this.filterData();
  }

  onDropDownDateChange() {
    if (this.selectedTabIndex === 1) {
      this.spectrumDailyDetailData = new Array<spectrumDailyDetailModel>();
      this.FilterspectrumDailyDetailData = new Array<spectrumDailyDetailModel>();
    }

    this.filterData();
  }

  onExtentionChange() {
    this.filterData();
  }

  filterData() {
    if (this.selectedTabIndex === 0) {
      this.singleLocationSelected = false;
      this.getSpectrumDailySummary();
    }
    else if (this.selectedTabIndex === 1) {
      this.singleLocationSelected = true;
      this.getSpectrumDailyDetail();
    }
   
  }

  getSpectrumDailySummary() {
    let startTime: number = new Date().getTime();
    if (this.spectrumDailySummaryData.length > 0) {
      //this.getspectrumDailySummaryDate(this.spectrumDailySummaryData);
      this.filterSpectrumDailySummary();
      this.ischartDataLoaded = true;
    }
    else {
      this.showLoader = true;
      this.spectrumService.GetSpectrumDailySummary().subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "Daily Spectrum Summary"
        model.time = ResponseTime
        this.spectrumDailySummaryData = m
        //this.getspectrumDailySummaryDate(this.spectrumDailySummaryData);
        this.filterSpectrumDailySummary();
        this.ischartDataLoaded = true;
        this.showLoader = false;
        this.ApiLoad.SaveApiTime(model).subscribe();
      })
    }
  }


  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth(e: Array<any>) {

    this.monthSelectItem = [];
    let months: Array<string> = new Array<string>();
    let tempdata: Array<any> = new Array<any>()
    tempdata = e.sort(this.sortMonth)

    months = Array.from(new Set(tempdata.map(item => item.month)));

    if (this.selectedmonthListForMothlyHeading.length === 0) {
      this.selectedmonthListForMothlyHeading = new Array<string>();
      months.forEach(i => {
        this.selectedmonthListForMothlyHeading.push(i);
      })
    }

    months.forEach(i => {
      this.monthSelectItem.push(
        {
          label: i, value: i
        })
    })

    months = months.reverse();
    this.selectedmonth = months[months.length - 1];
    this.currentMonth = this.selectedmonth;
    //this.MonthlydetailMonth = this.currentMonth;



  }

  getSpectrumDailyDetail() {
    this.dailyPager = {};
    this.dailyPagedItems = [];
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.spectrumService.GetSpectrumDailyDetail(this.defaultselectedDate, this.singleLocation).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Monthly RevenueData"
      model.time = ResponseTime

      this.spectrumDailyDetailData = m
      this.filterSpectrumDailyDetail();
      this.ischartDataLoaded = true;
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    })
  }


  filterSpectrumDailySummary() {
    this.GetCountOutBoundLastSevenDay();
    this.FilterspectrumDailySummaryData = this.spectrumDailySummaryData;
    let LocationfilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    let DatefilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    let ExtionfilterData: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
    this.totalOutbound = 0;
    let sumTotalTalkTime = 0;
    this.sumAverageTalkTime = 0;
    this.totalInbound = 0;
    this.totaltalktime = 0;
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        let tempLocationfilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        tempLocationfilter = this.FilterspectrumDailySummaryData.filter(x => x.agencyid === m)
        tempLocationfilter.forEach(z => {
          LocationfilterData.push(z);
        })
      })
    }
    else {
      let firstRecord: boolean = true;
      this.location.forEach(m => {

        if (firstRecord) {
          this.firstLocation = m.agencyId;
          firstRecord = false;
        }

        let tempLocationfilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        tempLocationfilter = this.FilterspectrumDailySummaryData.filter(x => x.agencyid === m.agencyId)
        tempLocationfilter.forEach(z => {
          LocationfilterData.push(z);
        })
      })
    }

    this.FilterspectrumDailySummaryData = LocationfilterData;
    this.getspectrumDailySummaryextention();
    if (this.selectedDate && this.selectedDate.length > 0) {
      this.selectedDate.forEach(m => {
        let selectedDateFilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        selectedDateFilter = this.FilterspectrumDailySummaryData.filter(x => x.date === m)
        selectedDateFilter.forEach(z => {
          DatefilterData.push(z);
        })
      })
    }

    if (DatefilterData && DatefilterData.length > 0) {
      this.FilterspectrumDailySummaryData = DatefilterData;
    }
    if (this.selectedextention && this.selectedextention.length > 0) {
      this.selectedextention.forEach(m => {
        let selectedextentionFilter: Array<spectrumDailySummaryModel> = new Array<spectrumDailySummaryModel>();
        selectedextentionFilter = this.FilterspectrumDailySummaryData.filter(x => x.extension === m)
        selectedextentionFilter.forEach(z => {
          ExtionfilterData.push(z);
        })
      })
    }


    if (ExtionfilterData && ExtionfilterData.length > 0) {
      this.FilterspectrumDailySummaryData = ExtionfilterData;
    }

    this.FilterspectrumDailySummaryData.forEach(m => {
      sumTotalTalkTime = sumTotalTalkTime + m.talktimeMinutes;
      this.sumAverageTalkTime = this.sumAverageTalkTime + m.averageTalkTime;
      this.totalOutbound = this.totalOutbound + m.outboundCalls;
      this.totalInbound = this.totalInbound + m.inboundCalls;
    })

    this.sumAverageTalkTime = Math.round(this.sumAverageTalkTime / this.FilterspectrumDailySummaryData.length);

    this.totaltalktime = Math.round(sumTotalTalkTime);
    this.CreateSpectrumDailySummaryChartData(Math.round(sumTotalTalkTime), Math.round(this.sumAverageTalkTime));

  }


  

  filterSpectrumDailyDetail() {
    this.GetCountOutBoundLastSevenDay();
    this.FilterspectrumDailyDetailData = this.spectrumDailyDetailData;
    let LocationfilterData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();
    let DatefilterData: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();

   

    this.FilterspectrumDailyDetailData = this.spectrumDailyDetailData;
    this.getspectrumDailySummaryextention();
    if (this.selectedDate && this.selectedDate.length > 0) {
      this.selectedDate.forEach(m => {
        let selectedDateFilter: Array<spectrumDailyDetailModel> = new Array<spectrumDailyDetailModel>();
        selectedDateFilter = this.FilterspectrumDailyDetailData.filter(x => x.date === m)
        selectedDateFilter.forEach(z => {
          DatefilterData.push(z);
        })
      })
    }

    if (DatefilterData && DatefilterData.length > 0) {
      this.FilterspectrumDailyDetailData = DatefilterData;
    }
    let sumTotalTalkTime: number = 0;

    this.FilterspectrumDailyDetailData.forEach(m => {
      sumTotalTalkTime = sumTotalTalkTime + (m.talktime / 60);
      //this.totalOutbound = this.totalOutbound + ;
      //this.totalInbound = this.totalInbound + m.inboundCalls;
    })

    this.DetailtotalOutbound = 0
    this.DetailtotalInbound = 0
    this.Detailtotaltalktime = 0
    this.DetailOutBoundMints = 0;
    let outboundMintData = this.FilterspectrumDailyDetailData.filter(m => m.uniqueid.toLowerCase().includes("out"))
    if (outboundMintData && outboundMintData.length > 0) {
      this.DetailtotalOutbound = outboundMintData.length;
    }

    outboundMintData.forEach(m => {
      this.DetailOutBoundMints = this.DetailOutBoundMints + (m.talktime / 60);
    })

    this.DetailOutBoundMints = Math.round(this.DetailOutBoundMints);

    let inboundMintData = this.FilterspectrumDailyDetailData.filter(m => m.uniqueid.toLowerCase().includes("in"))
    if (inboundMintData && inboundMintData.length > 0) {
      this.DetailtotalInbound = inboundMintData.length;
    }


    this.DetailtotalOutbound = Math.round(this.DetailtotalOutbound);
    this.DetailtotalInbound = Math.round(this.DetailtotalInbound);
    this.Detailtotaltalktime = Math.round(sumTotalTalkTime);

    this.setDailyPage(1)

  }



  CreateSpectrumDailySummaryChartData(sumTotalTalkTime: number, sumAverageTalkTime: number) {
    this.SpectrumDailySummary = [];

    this.SpectrumDailySummary = {
      labels: ['Total Talktime ', 'Average Talktime'],

      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
          borderColor: '#1E88E5',
          data: [sumTotalTalkTime, sumAverageTalkTime]
        }
      ]

    }
  }

  OntabSelectionChange(e) {

    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    //this.spectrumMonthlyDetailData = new Array<spectrumDailyDetailModel>();
    //this.FilterspectrumMonthlyDetailData = new Array<spectrumDailyDetailModel>();
    this.spectrumDailyDetailData = new Array<spectrumDailyDetailModel>();
    this.FilterspectrumDailyDetailData = new Array<spectrumDailyDetailModel>();
    //this.spectrumDailyCallVolumeData = new consolidatedCallVolume();
    //if (e === 1) {
    //  this.SetLocations.push(this.firstLocation);
    //  this.selectedlocation.push(this.firstLocation);
    //}

    this.selectedTabIndex = e;
    this.filterData();
  }


  onMonthSummryChange() {
    this.selectedmonthListForMothlyHeading = this.selectedmonthList;
    this.filterData();
  }

  onMonthDetailChange() {
    this.filterData();
    //      this.getSpectrumMonthlyDetail();
  }



  



  setDailyPage(page: number) {
    if (page < 1 || page > this.dailyPager.totalPages) {
      return;
    }

    // get pager object from service
    this.dailyPager = this._pageservice.getPager(this.FilterspectrumDailyDetailData.length, page);

    // get current page of items
    this.dailyPagedItems = this.FilterspectrumDailyDetailData.slice(this.dailyPager.startIndex, this.dailyPager.endIndex + 1);

  }


 

  GetCountOutBoundLastSevenDay() {

    let agencyId: Array<number> = new Array<number>();
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      agencyId = this.selectedlocation;
    }
    else {
      this.location.forEach(m => {
        agencyId.push(m.agencyId);
      })

    }
    this.spectrumService.GetTalkTimeOutBoundLastSevenDay(agencyId).subscribe(m => {
      for (var key in m) {
        this.outboundcalls = parseInt(key);
        this.dateRange = m[key];
      }
    });
  }

}
