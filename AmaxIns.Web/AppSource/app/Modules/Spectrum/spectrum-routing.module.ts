import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { SpectrumComponent } from './spectrum.component';


const routes: Routes = [
  { path: 'Spectrum', component: SpectrumComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SpectrumRoutingModule { }
