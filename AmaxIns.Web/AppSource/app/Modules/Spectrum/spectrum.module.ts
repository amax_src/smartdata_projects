import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { SpectrumRoutingModule } from './spectrum-routing.module';
import { SpectrumComponent } from './spectrum.component';


@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    SpectrumRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [SpectrumComponent],
  exports: []
 })
export class SpectrumModule { }
