import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaAisComponent } from './alpa-ais.component';

describe('AlpaAisComponent', () => {
  let component: AlpaAisComponent;
  let fixture: ComponentFixture<AlpaAisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaAisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaAisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
