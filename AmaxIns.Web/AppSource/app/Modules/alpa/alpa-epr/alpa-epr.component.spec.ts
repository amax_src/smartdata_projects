import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaEprComponent } from './alpa-epr.component';

describe('AlpaEprComponent', () => {
  let component: AlpaEprComponent;
  let fixture: ComponentFixture<AlpaEprComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaEprComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaEprComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
