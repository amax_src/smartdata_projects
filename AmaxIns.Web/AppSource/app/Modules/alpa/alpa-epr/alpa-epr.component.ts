import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../../core/common.service';
import { SelectItem } from 'primeng/api';
import { EprService } from '../../Services/eprService';
import { Epr, EPRGraphModel } from '../../../model/epr';
import { Location } from '../../../model/location';
import { PageService } from '../../Services/pageService';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { DateService } from '../../Services/dateService';
import { Observable } from 'rxjs';
import { RmZmLocationDropdownAlpaComponent } from '../../../shared/rm-zm-location-dropdown-alpa/rm-zm-location-dropdown-alpa.component';
@Component({
  selector: 'app-alpa-epr',
  templateUrl: './alpa-epr.component.html',
  styleUrls: ['./alpa-epr.component.scss']
})
export class AlpaEprComponent implements OnInit {

    selectedIndex: number = 0;
    location: Array<Location> = new Array<Location>();
    PageName: string = "EPR";
    selectedzonalManager: [] = [];
    ischartDataLoaded: boolean = false;
    showLoader: boolean = true;
    selectedlocation: Array<number> = new Array<number>();
    setLocation: Array<number> = new Array<number>();
    selectedRegionalManager: [] = [];
    eprDataFilter: Array<Epr> = new Array<Epr>();
    EprData: Array<Epr> = new Array<Epr>();

    EPRGraphModel: Array<EPRGraphModel> = new Array<EPRGraphModel>();

    EprTotalData: Array<Epr> = new Array<Epr>();
    eprTotalDataFilter: Array<Epr> = new Array<Epr>();

    month: Array<any>;
    monthSelectItem: SelectItem[] = [];
    selectedmonth: string = "";
    agentSelectItem: SelectItem[] = [];
    selectedagent: [] = [];
    year: string = '2022';
    YearSelectItem: SelectItem[] = [];
    date: Array<any>;
    dateSelectItem: SelectItem[] = [];
    selectedDate: Array<Date> = new Array<Date>();
    sortDirection: string = "desc";
    sortBy: string = "";

    pager: any = {};
    pagedItems: any[];

    pagerTotal: any = {};
    pagedTotalItems: any[];

    PolicyPerAgent: number = 0;
    AgenyFeePerAgent: number = 0
    PremiumPerAgent: number = 0;
    TransPerAgent: number = 0
    selectmonthindex: number = 0;
    monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    ConsolatedChartData: any;
    options: any;

    chartOptions: any = {
        animation: {
            duration: 0
        },
        responsive: false,
        maintainAspectRatio: false,
        layout: {
            padding: {
                right: 50
            }
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, context) {
                    if (context && (context.dataset.label === "Monthly Payment" || context.dataset.label === "Endorsement" || context.dataset.label === "Down Payment")) {
                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                    else {
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                }
            }
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    if (data && data.datasets && data.datasets.length > 0 && (data.datasets[tooltipItem.datasetIndex].label === "Monthly Payment" || data.datasets[tooltipItem.datasetIndex].label === "Endorsement" || data.datasets[tooltipItem.datasetIndex].label === "Down Payment")) {
                        return data.datasets[tooltipItem.datasetIndex].label + `- $${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
                    }
                    else {
                        return data.datasets[tooltipItem.datasetIndex].label + `- ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

                    }
                }
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    callback: function (label) {
                        return label;
                    },
                    fontColor: "#A3A0FB"
                },
                scaleLabel: {
                    display: true,
                    fontColor: "#A3A0FB"
                }
            }],
            yAxes: [{
                ticks: {
                    fontColor: "#A3A0FB"
                }
            }]
        }

    }
    @ViewChild(RmZmLocationDropdownAlpaComponent) child;
    constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private eprService: EprService,
        private _pageservice: PageService, private ApiLoad: ApiLoadTimeService, private _DateService: DateService) {
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }

    ngOnInit() {
        this.year = (new Date()).getFullYear().toString();//"2022";
        console.log("this.year", this.year);
        this.fillYear()
        this.getMonth();
        this.showLoader = false;
        this.router.events.subscribe((obj: any) => {
            if (obj instanceof RouteConfigLoadStart) {
                NProgress.start();
                NProgress.set(0.4);
            } else if (obj instanceof RouteConfigLoadEnd) {
                NProgress.set(0.9);
                setTimeout(() => {
                    NProgress.done();
                    NProgress.remove();
                }, 500);
            } else if (obj instanceof NavigationEnd) {
                this.cmnSrv.dashboardState.navbarToggle = false;
                this.cmnSrv.dashboardState.sidebarToggle = true;
                window.scrollTo(0, 0);
            }
        });
    }

    ngAfterViewChecked() {

        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }




    ZmChanged(e) {
        this.selectedzonalManager = [];
        this.selectedzonalManager = e;

    }
    RmChanged(e) {
        this.selectedRegionalManager = [];
        this.selectedRegionalManager = e;
    }
    LocationChanged(e) {
        this.selectedlocation = [];
        this.selectedlocation = e;
        this.selectedagent = [];
        //this.GetEprData();
    }

    GetAllLocations(e) {
        this.setLocation = [];
        this.selectedlocation = [];
        this.location = e;
        this.selectedagent = [];
        if (this.ischartDataLoaded) {
            //this.filterData();
            //this.filterEprTotal();
        }

        this.selectedlocation.push(this.location[0].agencyId);
        this.setLocation.push(this.location[0].agencyId);
        //this.GetEprData();
    }

    loaderStatusChanged(e) {
        if (this.ischartDataLoaded) {
            //this.showLoader = e;
        }
    }

    SearchReport() {
        this.GetEprData();
    }

    filterReset() {
        this.child.filterReset();
        this.year = "2022";
        this.getMonth();
        this.GetEprData();
    }

    GetEprData() {
        this.showLoader = true;
        let startTime: number = new Date().getTime();
        this.eprService.getAlpaEPRData(this.selectedmonth, this.selectedlocation, this.year).subscribe(m => {
            let model: ApiLoadModel = new ApiLoadModel();
            let EndTime: number = new Date().getTime();
            let ResponseTime: number = EndTime - startTime;
            model.page = "EPR Data"
            model.time = ResponseTime
            this.EprData = m;
            //this.eprDataFilter = this.EprData;
            this.ischartDataLoaded = true;
            //this.getMonth();
            //this.showLoader = false;
            this.getDate();
            this.filterData();
            this.setPage(1);
            this.ApiLoad.SaveApiTime(model).subscribe();

            this.showLoader = true;
            let _location = [];
            let _year = [];
            _year.push(this.year);
            if (this.selectedlocation) {
                _location = this.selectedlocation;
            }
            else {
                _location = this.location;
            }

            this.eprService.GetAlpaEPRGraph(_year, _location, this.selectedagent).subscribe(m => {
                this.EPRGraphModel = m;
                this.bindEPRGraphdata(this.EPRGraphModel);

                setTimeout(() => {
                    this.showLoader = false;
                }, 3000);

            })

            //this.GetEprTotalData();
        });
    }

    GetEprTotalData() {
        this.eprService.getAlpaEPRTotalData(this.selectedmonth, this.selectedlocation, this.year).subscribe(m => {
            this.EprTotalData = m;
            this.filterEprTotal();
            this.setTotalPage(1);
        })
    }

    //GetEPRGraph() {
    //    this.showLoader = true;
    //    let _location = [];
    //    let _year = [];
    //    _year.push(this.year);
    //    if (this.selectedlocation) {
    //        _location = this.selectedlocation;
    //    }
    //    else {
    //        _location = this.location;
    //    }
    //    this.eprService.GetEPRGraph(_year, this.selectedlocation,).subscribe(m => {
    //        this.EPRGraphModel = m;
    //        this.bindEPRGraphdata(this.EPRGraphModel);
    //        this.showLoader = false;
    //    })
    //}

    bindEPRGraphdata(val: Array<EPRGraphModel>) {
        let _MonthlyPaymen = [];
        let _DownPayment = [];
        let _Endorsement = [];

        this.monthNames.forEach(i => {
            var mp = val.filter(x => x.month == i && x.payType == 'Monthly Payment');
            var dp = val.filter(x => x.month == i && x.payType == 'Down Payment');
            var end = val.filter(x => x.month == i && x.payType == 'Endorsement');

            if (mp.length > 0) {
                _MonthlyPaymen.push(mp[0].eprValue);
            }
            else { _MonthlyPaymen.push(0); }

            if (dp.length > 0) {
                _DownPayment.push(dp[0].eprValue);
            }
            else { _DownPayment.push(0); }

            if (end.length > 0) {
                _Endorsement.push(end[0].eprValue);
            }
            else { _Endorsement.push(0); }
        })

        this.ConsolatedChartData = {
            labels: [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December',
            ],
            datasets: [
                {
                    label: 'Monthly Payment',
                    data: _MonthlyPaymen,
                    fill: false,
                    borderColor: '#4bc0c0',
                },
                {
                    label: 'Down Payment',
                    data: _DownPayment,
                    fill: false,
                    borderColor: '#42A5F5',
                },
                {
                    label: 'Endorsement',
                    data: _Endorsement,
                    fill: false,
                    borderColor: '#9CCC65',
                }
            ],
        };


    }

    sortMonth(a, b) {
        return a["sortmonthnum"] - b["sortmonthnum"];
    }

    getMonth() {
        this._DateService.getMonth(this.year).subscribe(m => {
            this.monthSelectItem = [];
            let months: Array<string> = new Array<string>();
            months = m;
            months.forEach(i => {
                this.monthSelectItem.push(
                    {
                        label: i, value: i
                    })
            })
            let monthcount = (new Date()).getMonth();
            this.selectmonthindex = (new Date()).getMonth() - 1;;
            for (var x = monthcount; x >= 0; x--) {
                let checkmonth = months.filter(m => m === this.monthNames[x])
                if (checkmonth && checkmonth.length > 0) {
                    this.selectmonthindex = x;
                    break;
                }
            }
            this.selectedmonth = this.monthNames[this.selectmonthindex];
            //this.GetEprData();
        })
        //this.filterData();
    }

    onMonthchange(e) {
        //this.getDate();
        //this.filterData();
        //this.GetEprData();
    }



    filterData() {
        let AgenctCount: number = 0;
        this.PolicyPerAgent = 0;
        this.AgenyFeePerAgent = 0;
        this.PremiumPerAgent = 0;
        this.TransPerAgent = 0;
        this.pager = {};
        this.pagedItems = [];
        this.eprDataFilter = this.EprData.filter(m => m.month === this.selectedmonth);
        let tempFilter: Array<Epr> = new Array<Epr>();
        let filterDataByAgent: Array<Epr> = new Array<Epr>();
        let filterDataByDate: Array<Epr> = new Array<Epr>();

        if (this.selectedlocation && this.selectedlocation.length > 0) {
            this.selectedlocation.forEach(m => {
                let data: Array<Epr> = new Array<Epr>();
                data = this.eprDataFilter.filter(x => x.agencyID === m);
                data.forEach(z => {
                    tempFilter.push(z);
                })
            })
        }
        else {
            this.location.forEach(m => {
                let data: Array<Epr> = new Array<Epr>();
                data = this.eprDataFilter.filter(x => x.agencyID === m.agencyId);
                data.forEach(z => {
                    tempFilter.push(z);
                })
            })
        }

        this.getAgents(tempFilter);

        if (this.selectedagent && this.selectedagent.length > 0) {
            this.selectedagent.forEach(m => {
                let data: Array<Epr> = new Array<Epr>();
                data = tempFilter.filter(x => x.csr === m);
                data.forEach(z => {
                    filterDataByAgent.push(z);
                })
            })

            tempFilter = filterDataByAgent;
        }

        if (this.selectedDate && this.selectedDate.length > 0) {
            this.selectedDate.forEach(m => {
                let data: Array<Epr> = new Array<Epr>();
                data = tempFilter.filter(x => x.date === m);
                data.forEach(z => {
                    filterDataByDate.push(z);
                })
            })

            tempFilter = filterDataByDate;
        }

        if (this.selectedagent && this.selectedagent.length > 0) {
            AgenctCount = this.selectedagent.length
        }
        else {
            AgenctCount = this.agentSelectItem.length;
        }

        this.eprDataFilter = tempFilter;
        this.filterEprTotal();

        this.eprDataFilter.forEach(m => {

            this.PolicyPerAgent = this.PolicyPerAgent + (!isNaN(m.policies) ? m.policies : 0)
            this.AgenyFeePerAgent = this.AgenyFeePerAgent + (!isNaN(m.agencyfee) ? m.agencyfee : 0)
            this.PremiumPerAgent = this.PremiumPerAgent + (!isNaN(m.premium) ? m.premium : 0)
            this.TransPerAgent = this.TransPerAgent + (!isNaN(m.transactions) ? m.transactions : 0)

        })

        this.PolicyPerAgent = this.PolicyPerAgent / AgenctCount
        this.AgenyFeePerAgent = this.AgenyFeePerAgent / AgenctCount
        this.PremiumPerAgent = this.PremiumPerAgent / AgenctCount
        this.TransPerAgent = this.TransPerAgent / AgenctCount

        this.TransPerAgent = Math.round(this.TransPerAgent);
        this.PolicyPerAgent = Math.round(this.PolicyPerAgent);
        this.AgenyFeePerAgent = Math.round(this.AgenyFeePerAgent);
        this.PremiumPerAgent = Math.round(this.PremiumPerAgent);

        this.setPage(1);

    }

    onTabChanged(e) {
        this.selectedIndex = e;
        if (e == 1) {
            //this.filterEprTotal();
        }
        else if (e == 2) {

        }
    }


    //filterTest() {
    //    var promise = new Promise((resolve, reject) => {

    //      resolve();

    //    });
    //    return promise;
    //}
    filterEprTotal() {
        this.showLoader = true;
        this.pagerTotal = {};
        this.pagedTotalItems = [];
        this.eprTotalDataFilter = [];

        let dataEpr: Epr = new Epr();
        let listEprData: Array<Epr> = new Array<Epr>();

        let tempDistinctLocation = this.eprDataFilter.map(item => item.location)
            .filter((value, index, self) => self.indexOf(value) === index);

        let tempDistinctcsr = this.eprDataFilter.map(item => item.csr)
            .filter((value, index, self) => self.indexOf(value) === index);
        let _agencyFee: number = 0;
        let _policies: number = 0;
        let _avgagencyfeebypolicy: number = 0;

        tempDistinctLocation.forEach(l => {

            tempDistinctcsr.forEach(cr => {
                let _tempEPR = this.eprDataFilter.filter(x => x.csr === cr && x.location === l);
                dataEpr = new Epr();
                if (_tempEPR.length > 0 && _tempEPR[0].location != "") {
                    _agencyFee = _tempEPR.reduce((prev, next) => prev + Math.round(next.agencyfee), 0);
                    _policies = _tempEPR.reduce((prev, next) => prev + Math.round(next.policies), 0);
                    _avgagencyfeebypolicy = (_agencyFee > 0 && _policies > 0 ? Math.round((_agencyFee / _policies)) : 0);

                    dataEpr.agencyfee = _agencyFee;
                    //dataEpr.agencyfeebyagent = _tempEPR.reduce((prev, next) => prev + Math.round(next.agencyfeebyagent), 0);
                    dataEpr.avgagencyfeebypolicy = _avgagencyfeebypolicy;//_tempEPR.reduce((prev, next) => prev + Math.round(next.avgagencyfeebypolicy), 0);
                    //dataEpr.avgagencyfeebypolicybyagent = _tempEPR.reduce((prev, next) => prev + Math.round(next.avgagencyfeebypolicybyagent), 0);
                    dataEpr.policies = _policies;
                    dataEpr.premium = _tempEPR.reduce((prev, next) => prev + Math.round(next.premium), 0);
                    //dataEpr.premiumbyagent = _tempEPR.reduce((prev, next) => prev + Math.round(next.premiumbyagent), 0);
                    dataEpr.transactions = _tempEPR.reduce((prev, next) => prev + Math.round(next.transactions), 0);
                    dataEpr.csr = _tempEPR[0].csr;
                    dataEpr.location = _tempEPR[0].location;
                    dataEpr.agencyID = _tempEPR[0].agencyID;
                }
                if (dataEpr.location != "") {
                    this.eprTotalDataFilter.push(dataEpr);
                }
            })
        })

        if (this.eprTotalDataFilter.length > 0) {
            this.setTotalPage(1);
            this.showLoader = false;
        }
        else if (this.eprDataFilter.length == 0) {
            this.showLoader = false;
        }
    }


    getAgents(data: Array<Epr>) {
        this.agentSelectItem = []
        let agents: Array<string> = new Array<string>();
        agents = Array.from(new Set(data.map(item => item.csr)));
        agents.forEach(i => {
            this.agentSelectItem.push(
                {
                    label: i, value: i
                })
        })
    }


    getDate() {
        this.selectedDate = [];
        let data: Array<Epr> = new Array<Epr>();
        data = this.EprData.filter(m => m.month === this.selectedmonth);
        this.dateSelectItem = []
        let dt: Array<Date> = new Array<Date>();
        dt = Array.from(new Set(data.map(item => item.date)));
        let selectDate: Date;
        let check: boolean = true;
        dt.forEach(i => {
            if (check) {
                selectDate = i;
                check = false;
            }
            this.dateSelectItem.push(
                {
                    label: this.getDateString(i), value: i
                })
        })

    }

    onAgentChange() {
        //this.filterData();

        //this.filterEprTotal();
    }

    getDateString(i): string {
        let date: string = '';
        let parts = i.split('/');
        let month = this.monthNames[(parts[0] - 1)];
        date = month + " " + (parts[1]);
        return date;
    }

    onDateChange() {
        this.filterData();
    }

    sortByPolicies(a, b) {
        return a["policies"] - b["policies"];
    }

    sortByAgencyFee(a, b) {
        return a["agencyfee"] - b["agencyfee"];
    }

    sortByPremium(a, b) {
        return a["premium"] - b["premium"];
    }

    sortByTransactions(a, b) {
        return a["transactions"] - b["transactions"];
    }

    sortByAgencyFeePerPolicy(a, b) {
        return a["avgagencyfeebypolicy"] - b["avgagencyfeebypolicy"];
    }

    sortByLocation(a, b) {
        return a["agencyID"] - b["agencyID"];
    }



    sortdescByPolicies(a, b) {
        return b["policies"] - a["policies"];
    }

    sortdescByAgencyFee(a, b) {
        return b["agencyfee"] - a["agencyfee"];
    }

    sortdescByPremium(a, b) {
        return b["premium"] - a["premium"];
    }

    sortdescByTransactions(a, b) {
        return b["transactions"] - a["transactions"];
    }

    sortdescByAgencyFeePerPolicy(a, b) {
        return b["avgagencyfeebypolicy"] - a["avgagencyfeebypolicy"];
    }

    sortdescByLocation(a, b) {
        return b["agencyID"] - a["agencyID"];
    }




    sortData(e) {
        this.pager = {};
        this.pagedItems = [];
        if (this.ischartDataLoaded) {
            if (this.sortBy === e) {
                if (this.sortDirection === "desc") {
                    this.sortDirection = "asc";
                }
                else {
                    this.sortDirection = "desc";
                }
            }

            this.sortBy = e;

            let Data: Array<Epr> = new Array<Epr>();
            switch (e) {
                case "Policies":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByPolicies);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByPolicies);
                        }
                    }
                    break
                case "AgencyFee":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByAgencyFee);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByAgencyFee);
                        }
                    }
                    break
                case "Premium":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByPremium);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByPremium);
                        }
                    }
                    break
                case "Transactions":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByTransactions);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByTransactions);
                        }
                    }
                    break
                case "AgencyFeePerPolicy":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByAgencyFeePerPolicy);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByAgencyFeePerPolicy);
                        }
                    }
                    break
                case "Locations":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprDataFilter.sort(this.sortdescByLocation);
                        }
                        else {
                            Data = this.eprDataFilter.sort(this.sortByLocation);
                        }
                    }
                    break

            }

            this.eprDataFilter = Data;
            this.setPage(1);
        }
    }

    sortDataTotal(e) {
        this.pagerTotal = {};
        this.pagedTotalItems = [];
        if (this.ischartDataLoaded) {
            if (this.sortBy === e) {
                if (this.sortDirection === "desc") {
                    this.sortDirection = "asc";
                }
                else {
                    this.sortDirection = "desc";
                }
            }

            this.sortBy = e;

            let Data: Array<Epr> = new Array<Epr>();
            switch (e) {
                case "Policies":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByPolicies);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByPolicies);
                        }
                    }
                    break
                case "AgencyFee":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByAgencyFee);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByAgencyFee);
                        }
                    }
                    break
                case "Premium":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByPremium);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByPremium);
                        }
                    }
                    break
                case "Transactions":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByTransactions);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByTransactions);
                        }
                    }
                    break
                case "AgencyFeePerPolicy":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByAgencyFeePerPolicy);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByAgencyFeePerPolicy);
                        }
                    }
                    break
                case "Locations":
                    {
                        if (this.sortDirection === "desc") {
                            Data = this.eprTotalDataFilter.sort(this.sortdescByLocation);
                        }
                        else {
                            Data = this.eprTotalDataFilter.sort(this.sortByLocation);
                        }
                    }
                    break

            }

            this.eprTotalDataFilter = Data;
            this.setTotalPage(1);
        }
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this._pageservice.getPager(this.eprDataFilter.length, page);
        // get current page of items
        this.pagedItems = this.eprDataFilter.slice(this.pager.startIndex, this.pager.endIndex + 1);

    }

    setTotalPage(page: number) {
        if (page < 1 || page > this.pagerTotal.totalPages) {
            return;
        }
        // get pager object from service
        this.pagerTotal = this._pageservice.getPager(this.eprTotalDataFilter.length, page);
        // get current page of items
        this.pagedTotalItems = this.eprTotalDataFilter.slice(this.pagerTotal.startIndex, this.pagerTotal.endIndex + 1);
    }


    fillYear() {
        this.YearSelectItem = [];
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
        this.YearSelectItem.push(
            {
                label: '2023', value: '2023'
            })
    }

    onYearchange() {
        this.getMonth();
    }

}
