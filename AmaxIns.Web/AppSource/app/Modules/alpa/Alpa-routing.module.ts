import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';

import {AlpaDashboardComponent } from './alpa-dashboard/alpa-dashboard.component';
import {AlpaRevenueComponent } from './alpa-revenue/alpa-revenue.component';
import { AlpaEprComponent} from './alpa-epr/alpa-epr.component';
import { AlpaAisComponent } from './alpa-ais/alpa-ais.component';
import { AlpaDailyrevenueComponent } from './alpa-dailyrevenue/alpa-dailyrevenue.component';
import { TiergoalComponent } from './tiergoal/tiergoal.component';
import { DailyTransactionPaymentsComponent } from './daily-transaction-payments/daily-transaction-payments.component';
const routes: Routes = [

    { path: 'alpa-dashboard', component: AlpaDashboardComponent, canActivate: [LoginGuard] },
    { path: 'alpa-revenue', component: AlpaRevenueComponent, canActivate: [LoginGuard] },
    { path: 'alpa-epr', component: AlpaEprComponent, canActivate: [LoginGuard] },
    { path: 'alpa-ais', component: AlpaAisComponent, canActivate: [LoginGuard] },
    { path: 'alpa-daily-revenue', component: AlpaDailyrevenueComponent, canActivate: [LoginGuard] },
    { path: 'alpa-tier-goal', component: TiergoalComponent, canActivate: [LoginGuard] },
    { path: 'daily-transaction-payments', component: DailyTransactionPaymentsComponent, canActivate: [LoginGuard] }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AlpaRoutingModule { }
