import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaDailyrevenueComponent } from './alpa-dailyrevenue.component';

describe('AlpaDailyrevenueComponent', () => {
  let component: AlpaDailyrevenueComponent;
  let fixture: ComponentFixture<AlpaDailyrevenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaDailyrevenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaDailyrevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
