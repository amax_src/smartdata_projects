import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../../core/common.service';
import { UserService } from '../../Services/UserService';
import { AgencyService } from '../../Services/agencyService';
import { DateService } from '../../Services/dateService';
import { RevenueService } from '../../Services/revenueService';
import { SessionService } from '../../../core/storage/sessionservice';
import { Workingdays } from '../../../model/workingdays';
import { WorkingDaysService } from '../../Services/workingdaysService';
import { revenueDaily } from '../../../model/revenueDaily';
import { spectrumService } from '../../Services/spectrumService';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { projection } from '@angular/core/src/render3';
import { PageService } from '../../Services/pageService';
import { RmZmLocationDropdownAlpaComponent } from '../../../shared/rm-zm-location-dropdown-alpa/rm-zm-location-dropdown-alpa.component';
@Component({
  selector: 'app-alpa-dailyrevenue',
  templateUrl: './alpa-dailyrevenue.component.html',
  styleUrls: ['./alpa-dailyrevenue.component.scss']
})
export class AlpaDailyrevenueComponent implements OnInit {
    outboundcalls: number = 0;
    dateRange: string = '';
    revenueDailyData: Array<revenueDaily>;
    filterrevenueDailyData: Array<revenueDaily>;
    PageName: string = "Revenue > Daily";
    selectedRegionalManager: [];
    selectedzonalManager: []
    selectedlocation: Array<number> = []
    location: Array<number> = []
    years: Array<any>;
    yearsSelectItem: SelectItem[] = [];
    selectedyear: string = "";
    month: Array<any>;
    monthSelectItem: SelectItem[] = [];
    selectedmonth: string = "";
    workingdaysList: Array<Workingdays>;
    workingdays: Workingdays;
    showLoader: boolean = true;
    actualValue: number = 0;
    actualLable: string = "Actual Daily Policies";
    selectmonthindex: number = 0;
    selectedTabIndex: number = 0;
    ischartDataLoaded: boolean = true;

    PolicyTotalCountChart: any;
    AgencyFeeTotalCountChart: any;
    PremiumTotalCountChart: any;
    IsPageLoadFirstTime: boolean = true;

    PolicyCountChardata: Array<{ _data: any, agenyName: string }>;
    AgencyFeeChardata: Array<{ _data: any, agenyName: string }>;
    PremiumChardata: Array<{ _data: any, agenyName: string }>;

    PolicyData: Array<{ projection: number, actual: number, differnce: number, agenyName: string }> = new Array<{ actual: number, projection: number, differnce: number, agenyName: string }>();
    AFeeData: Array<{ projection: number, actual: number, differnce: number, agenyName: string }> = new Array<{ actual: number, projection: number, differnce: number, agenyName: string }>();
    PreData: Array<{ projection: number, actual: number, differnce: number, agenyName: string }> = new Array<{ actual: number, projection: number, differnce: number, agenyName: string }>();


    PolicyCountchartOptions: any = {
        animation: {
            duration: 0
        },
        responsive: false,
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 25,
                bottom: 5
            }
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, context) {
                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        },
        legend: { display: false },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
                }
            }
        },

        scales: {
            xAxes: [{
                ticks: {
                    display: false
                }//this will remove all the x-axis grid lines
            }],
            yAxes: [{
                ticks: {
                    display: false
                } //this will remove all the x-axis grid lines
            }]
        }


    }
    chartOptions: any = {
        animation: {
            duration: 0
        },
        responsive: false,
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 25,
                bottom: 5
            }
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, context) {
                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        },
        legend: { display: false },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
                }
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    display: false
                }//this will remove all the x-axis grid lines
            }],
            yAxes: [{
                ticks: {
                    display: false
                } //this will remove all the x-axis grid lines
            }]
        }

    }

    date: Array<any>;
    dateSelectItem: SelectItem[] = [];
    selectedDate: Array<Date> = new Array<Date>();

    monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    IsMonthChanged: boolean = true;

    constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
        private userService: UserService, private agencyService: AgencyService, private dateservice: DateService,
        private revenueService: RevenueService, private sessionService: SessionService, private workingDaysService: WorkingDaysService, private spectservice: spectrumService, private ApiLoad: ApiLoadTimeService, private _pageservice: PageService) {
        this.years = Array<any>();
        this.revenueDailyData = new Array<revenueDaily>();
        this.filterrevenueDailyData = new Array<revenueDaily>();
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }

    @ViewChild(RmZmLocationDropdownAlpaComponent) child;
    ngOnInit() {
        this.getYears();
        //this.getconsolidatedRevenueDailyData();
        this.router.events.subscribe((obj: any) => {
            if (obj instanceof RouteConfigLoadStart) {
                NProgress.start();
                NProgress.set(0.4);
            } else if (obj instanceof RouteConfigLoadEnd) {
                NProgress.set(0.9);
                setTimeout(() => {
                    NProgress.done();
                    NProgress.remove();
                }, 500);
            } else if (obj instanceof NavigationEnd) {
                this.cmnSrv.dashboardState.navbarToggle = false;
                this.cmnSrv.dashboardState.sidebarToggle = true;
                window.scrollTo(0, 0);
            }
        });
    }

    ngAfterViewChecked() {

        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }


    ZmChanged(e) {
        this.selectedzonalManager = [];
        this.selectedzonalManager = e;
    }
    RmChanged(e) {
        this.selectedRegionalManager = [];
        this.selectedRegionalManager = e;
    }
    LocationChanged(e) {
        this.selectedlocation = [];
        this.selectedlocation = e;
        //this.TabChange();
    }

    GetAllLocations(e) {
        this.selectedlocation = [];
        this.location = [];
        e.forEach(i => {
            this.location.push(i.agencyId)
        });
        //this.TabChange();
    }

    loaderStatusChanged(e) {
        if (this.ischartDataLoaded) {
            // this.showLoader = e;
        }
    }

    SearchReport() {
        this.TabChange();
    }


    filterReset() {
        this.child.filterReset();
        this.selectedyear = (new Date()).getFullYear().toString();
        this.selectedDate = [];
        this.selectedmonth = "";
        this.getYears();
    }

    onYearchange(e) {
        this.IsMonthChanged = true;
        if (this.IsPageLoadFirstTime) {
            this.getMonth(this.selectedyear);
        }
        else {
            this.SearchgetMonth(this.selectedyear);
        }
        //this.TabChange();
    }

    onMonthchange(e) {
        this.revenueDailyData = new Array<revenueDaily>();
        this.IsMonthChanged = true;
        this.getDate();
        //this.TabChange();
    }


    getYears() {
        this.dateservice.getYear().toPromise().then(m => {
            this.years = m;
            this.years.forEach(i => {
                if (i > 2019) {
                    this.yearsSelectItem.push(
                        {
                            label: i, value: i
                        })
                }
            })

            let _selectedYear = (new Date()).getFullYear().toString();
            this.selectedyear = _selectedYear;
            this.selectedmonth = "";
            this.getMonth(this.selectedyear);
        });
    }

    sortMonth(a, b) {
        return a["sortmonthnum"] - b["sortmonthnum"];
    }

    SearchgetMonth(year: string) {
        this.monthSelectItem = [];
        let months: Array<string> = new Array<string>();
        this.selectmonthindex = 0;
        this.revenueService.getMonth(year).toPromise().then(m => {
            months = m;
            months.forEach(i => {
                this.monthSelectItem.push(
                    {
                        label: i, value: i
                    })
            })

            let monthcount = (new Date()).getMonth();
            this.selectmonthindex = monthcount - 1;
            for (var x = monthcount; x >= 0; x--) {
                let checkmonth = months.filter(m => m === this.monthNames[x])
                if (checkmonth && checkmonth.length > 0) {
                    this.selectmonthindex = x;
                    break;
                }
            }

            let _currentyear: string = (new Date()).getFullYear().toString()
            if (year === _currentyear) {
                this.selectedmonth = this.monthNames[this.selectmonthindex];
            }
            else {
                this.selectedmonth = this.monthNames[11];
            }
            //this.getconsolidatedRevenueDailyData();
        })

    }

    getMonth(year: string) {
        this.monthSelectItem = [];
        let months: Array<string> = new Array<string>();
        this.selectmonthindex = 0;
        this.revenueService.getMonth(year).toPromise().then(m => {
            months = m;
            months.forEach(i => {
                this.monthSelectItem.push(
                    {
                        label: i, value: i
                    })
            })

            let monthcount = (new Date()).getMonth();
            this.selectmonthindex = monthcount - 1;
            for (var x = monthcount; x >= 0; x--) {
                let checkmonth = months.filter(m => m === this.monthNames[x])
                if (checkmonth && checkmonth.length > 0) {
                    this.selectmonthindex = x;
                    break;
                }
            }

            let _currentyear: string = (new Date()).getFullYear().toString()
            if (year === _currentyear) {
                this.selectedmonth = this.monthNames[this.selectmonthindex];
            }
            else {
                this.selectedmonth = this.monthNames[11];
            }
            this.getconsolidatedRevenueDailyData();
        })

    }

    getconsolidatedRevenueDailyData() {
        this.showLoader = true;
        let startTime: number = new Date().getTime();
        this.revenueService.getAlpaRevenueDailyData(this.selectedmonth).toPromise().then(m => {
            let model: ApiLoadModel = new ApiLoadModel();
            let EndTime: number = new Date().getTime();
            let ResponseTime: number = EndTime - startTime;
            model.page = "Daily RevenueData"
            model.time = ResponseTime
            this.revenueDailyData = m;
            if (this.IsMonthChanged && this.revenueDailyData.length > 0) {
                this.getDate();
                this.IsMonthChanged = false;
            }
            this.ischartDataLoaded = true;
            this.IsPageLoadFirstTime = false;
            this.FilterDailyData();
            this.showLoader = false;
            this.ApiLoad.SaveApiTime(model).subscribe();
        });

    }

    getDate() {
        this.selectedDate = [];
        let data: Array<revenueDaily> = new Array<revenueDaily>();
        data = this.revenueDailyData.filter(m => m.actYear === this.selectedyear);
        data = data.filter(m => m.actmonth === this.selectedmonth);
        this.dateSelectItem = []
        let dt: Array<Date> = new Array<Date>();
        dt = Array.from(new Set(data.map(item => item.actDate)));
        dt.forEach(i => {
            this.dateSelectItem.push(
                {
                    label: this.getDateString(i), value: i
                })
        })
    }

    getDateString(i): string {
        let date: string = '';
        let parts = i.split('/');
        let month = this.monthNames[(parts[0] - 1)];
        date = month + " " + (parts[1]);
        return date;
    }

    onDateChange() {
        //this.TabChange();
    }

    FilterDailyData() {
        this.filterrevenueDailyData = new Array<revenueDaily>();
        let LocationDailyfilterdata: Array<revenueDaily> = new Array<revenueDaily>();
        let DateDailyfilterdata: Array<revenueDaily> = new Array<revenueDaily>();
        this.filterrevenueDailyData = this.revenueDailyData.filter(m => m.actYear === this.selectedyear);
        this.filterrevenueDailyData = this.filterrevenueDailyData.filter(m => m.actmonth === this.selectedmonth);
        if (this.selectedlocation && this.selectedlocation.length > 0) {
            this.selectedlocation.forEach(i => {
                let filterDilydata = this.filterrevenueDailyData.filter(m => m.agencyID === i);
                filterDilydata.forEach(m => {
                    LocationDailyfilterdata.push(m);
                })
            })
        }
        else if (this.location && this.location.length > 0) {
            this.location.forEach(i => {
                let filterDilydata = this.filterrevenueDailyData.filter(m => m.agencyID === i);
                filterDilydata.forEach(m => {
                    LocationDailyfilterdata.push(m);
                })
            })
        }

        this.filterrevenueDailyData = LocationDailyfilterdata;

        if (this.selectedDate && this.selectedDate.length > 0) {
            this.selectedDate.forEach(i => {
                let filterDilydata = this.filterrevenueDailyData.filter(m => m.actDate === i);
                filterDilydata.forEach(m => {
                    DateDailyfilterdata.push(m);
                })
            })
        }
        if (this.selectedDate && this.selectedDate.length > 0) {
            this.filterrevenueDailyData = DateDailyfilterdata;
        }

        this.CalculateRawDailyData();

    }

    CalculateRawDailyData() {
        this.PolicyData = []
        this.AFeeData = []
        this.PreData = []
        let dailyactPolicy: number = 0;
        let dailyactAgencyFee: number = 0;
        let dailyactPremium: number = 0;
        let dailyPolicy: number = 0;
        let dailyAgencyFee: number = 0;
        let dailyPremium: number = 0;
        let DailyDataViaAgency: Array<revenueDaily>
        let agency: string = '';
        if (this.selectedlocation && this.selectedlocation.length > 0) {
            this.selectedlocation.forEach(m => {
                dailyactPolicy = 0;
                dailyactAgencyFee = 0;
                dailyactPremium = 0;
                dailyPolicy = 0;
                dailyAgencyFee = 0;
                dailyPremium = 0;
                agency = '';
                DailyDataViaAgency = new Array<revenueDaily>();
                DailyDataViaAgency = this.filterrevenueDailyData.filter(z => z.agencyID === m);
                DailyDataViaAgency.forEach(i => {
                    dailyactPolicy = dailyactPolicy + (!isNaN(i.actnewbusinesscount) ? i.actnewbusinesscount : 0);
                    dailyactAgencyFee = dailyactAgencyFee + (!isNaN(i.actagencyfee) ? i.actagencyfee : 0);
                    dailyactPremium = dailyactPremium + (!isNaN(i.actpremium) ? i.actpremium : 0);
                    dailyPolicy = dailyPolicy + (!isNaN(i.projectionPolicies) ? i.projectionPolicies : 0);
                    dailyAgencyFee = dailyAgencyFee + (!isNaN(i.projectionagencyfee) ? i.projectionagencyfee : 0);
                    dailyPremium = dailyPremium + (!isNaN(i.projectionpremium) ? i.projectionpremium : 0);
                    agency = i.actagencyname;
                })
                if (agency != '') {
                    this.PolicyData.push({ projection: dailyPolicy, actual: dailyactPolicy, differnce: (dailyPolicy - dailyactPolicy), agenyName: agency })
                    this.AFeeData.push({ projection: dailyAgencyFee, actual: dailyactAgencyFee, differnce: (dailyAgencyFee - dailyactAgencyFee), agenyName: agency })
                    this.PreData.push({ projection: dailyPremium, actual: dailyactPremium, differnce: (dailyPremium - dailyactPremium), agenyName: agency })
                }
            })
        }
        else {
            this.location.forEach(m => {
                dailyactPolicy = 0;
                dailyactAgencyFee = 0;
                dailyactPremium = 0;
                dailyPolicy = 0;
                dailyAgencyFee = 0;
                dailyPremium = 0;
                agency = '';
                DailyDataViaAgency = new Array<revenueDaily>();
                DailyDataViaAgency = this.filterrevenueDailyData.filter(z => z.agencyID === m);
                DailyDataViaAgency.forEach(i => {
                    dailyactPolicy = dailyactPolicy + (!isNaN(i.actnewbusinesscount) ? i.actnewbusinesscount : 0);
                    dailyactAgencyFee = dailyactAgencyFee + (!isNaN(i.actagencyfee) ? i.actagencyfee : 0);
                    dailyactPremium = dailyactPremium + (!isNaN(i.actpremium) ? i.actpremium : 0);
                    dailyPolicy = dailyPolicy + (!isNaN(i.projectionPolicies) ? i.projectionPolicies : 0);
                    dailyAgencyFee = dailyAgencyFee + (!isNaN(i.projectionagencyfee) ? i.projectionagencyfee : 0);
                    dailyPremium = dailyPremium + (!isNaN(i.projectionpremium) ? i.projectionpremium : 0);
                    agency = i.actagencyname;
                })
                if (agency != '') {
                    this.PolicyData.push({ projection: dailyPolicy, actual: dailyactPolicy, differnce: (dailyPolicy - dailyactPolicy), agenyName: agency })
                    this.AFeeData.push({ projection: dailyAgencyFee, actual: dailyactAgencyFee, differnce: (dailyAgencyFee - dailyactAgencyFee), agenyName: agency })
                    this.PreData.push({ projection: dailyPremium, actual: dailyactPremium, differnce: (dailyPremium - dailyactPremium), agenyName: agency })
                }
            })
        }
        this.renderChat(this.selectedTabIndex);

    }


    setActualHeader(e) {
        this.selectedTabIndex = e;
        this.TabChange();
    }

    TabChange() {
        this.getconsolidatedRevenueDailyData();
    }


    renderChat(e) {
        this.actualValue = 0;
        if (e === 0) {
            this.PolicyData.forEach(m => {
                this.actualValue = this.actualValue + m.actual;
            })
            this.actualLable = "Actual Daily Policies";
            this.CreateCartDataForDailyPolicyCount();
        }

        else if (e === 1) {
            this.AFeeData.forEach(m => {
                this.actualValue = this.actualValue + m.actual;
            })
            this.actualLable = "Actual Daily AgencyFee";
            this.CreateCartDataForDailyAgencyFee();
        }

        else if (e === 2) {
            this.PreData.forEach(m => {
                this.actualValue = this.actualValue + m.actual;
            })
            this.actualLable = "Actual Daily Premium";
            this.CreateCartDataForDailyPremium();
        }
        else if (e === 3) {
            this.CalculateTotalDailyRevenue();
        }
    }

    CalculateTotalDailyRevenue() {
        this.PolicyTotalCountChart = {};
        this.AgencyFeeTotalCountChart = {};
        this.PremiumTotalCountChart = {};

        let _totalPolicyGoal: number = 0; let _totalPolicyActual: number = 0;
        let _totalAfeeGoal: number = 0; let _totalAfeeActual: number = 0;
        let _totalPremiumGoal: number = 0; let _totalPremiumActual: number = 0;

        // Total Policy Count
        this.PolicyData.forEach(a => {
            _totalPolicyGoal = _totalPolicyGoal + Math.round(a.projection);
            _totalPolicyActual = _totalPolicyActual + Math.round(a.actual);
        });
        this.actualValue = _totalPolicyActual;
        this.PolicyTotalCountChart = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
                {
                    data: [_totalPolicyGoal, _totalPolicyActual, (_totalPolicyGoal - _totalPolicyActual)],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }

            ]
        }


        this.AFeeData.forEach(a => {
            _totalAfeeGoal = _totalAfeeGoal + Math.round(a.projection);
            _totalAfeeActual = _totalAfeeActual + Math.round(a.actual);
        });

        this.AgencyFeeTotalCountChart = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
                {
                    data: [_totalAfeeGoal, _totalAfeeActual, (_totalAfeeGoal - _totalAfeeActual)],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }

            ]
        }

        this.PreData.forEach(a => {
            _totalPremiumGoal = _totalPremiumGoal + Math.round(a.projection);
            _totalPremiumActual = _totalPremiumActual + Math.round(a.actual);
        });

        this.PremiumTotalCountChart = {
            labels: ['Goal', 'Actual', 'Difference'],
            datasets: [
                {
                    data: [_totalPremiumGoal, _totalPremiumActual, (_totalPremiumGoal - _totalPremiumActual)],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }

            ]
        }

    }

    CreateCartDataForDailyPolicyCount() {
        this.PolicyCountChardata = new Array<{ _data: any, agenyName: string }>();
        let DailyPolicyCountChardata: any;
        this.PolicyData.forEach(m => {
            DailyPolicyCountChardata = {};
            DailyPolicyCountChardata = {
                labels: ['Projection', 'Actual', 'Difference'],
                datasets: [
                    {
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        borderColor: '#1E88E5',
                        data: [Math.round(m.projection), Math.round(m.actual), Math.round((m.projection - m.actual))]
                    }
                ]
            }
            this.PolicyCountChardata.push({ _data: DailyPolicyCountChardata, agenyName: m.agenyName });
        })
        if (this.PolicyCountChardata.length > 0) {
            this.setPolicyCountPaging(1);
        }
    }
    CreateCartDataForDailyAgencyFee() {
        this.AgencyFeeChardata = new Array<{ _data: any, agenyName: string }>();
        let DailyAgencyChardata: any;
        this.AFeeData.forEach(m => {
            DailyAgencyChardata = {};
            DailyAgencyChardata = {
                labels: ['Projection', 'Actual', 'Difference'],
                datasets: [
                    {
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        borderColor: '#1E88E5',
                        data: [Math.round(m.projection), Math.round(m.actual), Math.round((m.projection - m.actual))]
                    }
                ]
            }
            this.AgencyFeeChardata.push({ _data: DailyAgencyChardata, agenyName: m.agenyName });
        })
        console.log("this.AgencyFeeChardata", this.AgencyFeeChardata)
        this.setAfeePaging(1);
    }

    CreateCartDataForDailyPremium() {
        this.PremiumChardata = new Array<{ _data: any, agenyName: string }>();

        let DailyPremiumChardata: any;
        this.PreData.forEach(m => {
            DailyPremiumChardata = {};
            DailyPremiumChardata = {
                labels: ['Projection', 'Actual', 'Difference'],
                datasets: [
                    {
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        borderColor: '#1E88E5',
                        data: [Math.round(m.projection), Math.round(m.actual), Math.round((m.projection - m.actual))]
                    }
                ]
            }
            this.PremiumChardata.push({ _data: DailyPremiumChardata, agenyName: m.agenyName });
        })

        this.setPremiumPaging(1);
    }
    pagerPolicyCount: any = {};
    pagedPolicyCount: any[];

    setPolicyCountPaging(page: number) {
        if (page < 1 || page > this.pagerPolicyCount.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerPolicyCount = this._pageservice.getPayrollPager(this.PolicyCountChardata.length, page, 10);

        // get current page of items
        this.pagedPolicyCount = this.PolicyCountChardata.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);

    }

    pagerAfee: any = {};
    pagedAfee: any[];

    setAfeePaging(page: number) {
        if (page < 1 || page > this.pagerAfee.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerAfee = this._pageservice.getPayrollPager(this.AgencyFeeChardata.length, page, 10);

        // get current page of items
        this.pagedAfee = this.AgencyFeeChardata.slice(this.pagerAfee.startIndex, this.pagerAfee.endIndex + 1);

    }

    pagerPremium: any = {};
    pagedPremium: any[];

    setPremiumPaging(page: number) {
        if (page < 1 || page > this.pagerPremium.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerPremium = this._pageservice.getPayrollPager(this.PremiumChardata.length, page, 10);

        // get current page of items
        this.pagedPremium = this.PremiumChardata.slice(this.pagerPremium.startIndex, this.pagerPremium.endIndex + 1);

    }

}
