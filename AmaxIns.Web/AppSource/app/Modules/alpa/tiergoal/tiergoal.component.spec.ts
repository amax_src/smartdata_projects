import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiergoalComponent } from './tiergoal.component';

describe('TiergoalComponent', () => {
  let component: TiergoalComponent;
  let fixture: ComponentFixture<TiergoalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiergoalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiergoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
