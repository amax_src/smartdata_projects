import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTransactionPaymentsComponent } from './daily-transaction-payments.component';

describe('DailyTransactionPaymentsComponent', () => {
    let component: DailyTransactionPaymentsComponent;
    let fixture: ComponentFixture<DailyTransactionPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ DailyTransactionPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(DailyTransactionPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
