import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../../core/common.service';
import { UserService } from '../../Services/UserService';
import { AgencyService } from '../../Services/agencyService';
import { DateService } from '../../Services/dateService';
import { RevenueService } from '../../Services/revenueService';
import { SessionService } from '../../../core/storage/sessionservice';
import { Workingdays } from '../../../model/workingdays';
import { WorkingDaysService } from '../../Services/workingdaysService';
import { DailyTransactonPayment, revenueDaily } from '../../../model/revenueDaily';
import { spectrumService } from '../../Services/spectrumService';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { projection } from '@angular/core/src/render3';
import { PageService } from '../../Services/pageService';
import { RmZmLocationDropdownAlpaComponent } from '../../../shared/rm-zm-location-dropdown-alpa/rm-zm-location-dropdown-alpa.component';
import {  QuotesParam } from '../../../model/NewModifiedQuotes';
import { DownloadexcelService } from '../../Services/downloadexcel.service';

@Component({
    selector: 'daily-transaction-payments',
    templateUrl: './daily-transaction-payments.component.html',
    styleUrls: ['./daily-transaction-payments.component.scss']
})
export class DailyTransactionPaymentsComponent implements OnInit {
    outboundcalls: number = 0;
    dateRange: string = '';

    QuotesParam: QuotesParam;

    PageName: string = "Daily Transaction Payments";
    selectedRegionalManager: [];
    selectedzonalManager: []
    selectedlocation: Array<string> = new Array<string>();
    allLocation: Array<string> = new Array<string>();
    dataDailyTransactonPayment: Array<DailyTransactonPayment> = new Array<DailyTransactonPayment>();
    location: Array<number> = []
    years: Array<any>;
    yearsSelectItem: SelectItem[] = [];
    selectedyear: string = "";
    month: Array<any>;
    monthSelectItem: SelectItem[] = [];
    selectedmonth: string = "";
    
    showLoader: boolean = false;
    actualValue: number = 0;
    actualLable: string = "Actual Daily Policies";
    selectmonthindex: number = 0;
    selectedTabIndex: number = 0;
    ischartDataLoaded: boolean = true;

    date: Array<any>;
    dateSelectItem: SelectItem[] = [];
    selectedDate: Array<any> = new Array<any>();

    monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    IsMonthChanged: boolean = true;

    constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
        private userService: UserService, private agencyService: AgencyService, private dateservice: DateService,
        private revenueService: RevenueService, private sessionService: SessionService, private workingDaysService: WorkingDaysService
        , private spectservice: spectrumService, private ApiLoad: ApiLoadTimeService, private _pageservice: PageService, private _downloadService: DownloadexcelService) {
        this.years = Array<any>();
      
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }

    @ViewChild(RmZmLocationDropdownAlpaComponent) child;
    ngOnInit() {
        this.getYears();
        //this.getconsolidatedRevenueDailyData();
        this.router.events.subscribe((obj: any) => {
            if (obj instanceof RouteConfigLoadStart) {
                NProgress.start();
                NProgress.set(0.4);
            } else if (obj instanceof RouteConfigLoadEnd) {
                NProgress.set(0.9);
                setTimeout(() => {
                    NProgress.done();
                    NProgress.remove();
                }, 500);
            } else if (obj instanceof NavigationEnd) {
                this.cmnSrv.dashboardState.navbarToggle = false;
                this.cmnSrv.dashboardState.sidebarToggle = true;
                window.scrollTo(0, 0);
            }
        });
    }

    ngAfterViewChecked() {

        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }
    pagerdailyPaymentCount: any = {};
    pageddailyPaymentCount: any[];

    ZmChanged(e) {
        this.selectedzonalManager = [];
        this.selectedzonalManager = e;
    }
    RmChanged(e) {
        this.selectedRegionalManager = [];
        this.selectedRegionalManager = e;
    }
    LocationChanged(e) {
        this.selectedlocation = [];
        this.selectedlocation = e;
        //this.TabChange();
    }

    GetAllLocations(e) {
        this.selectedlocation = [];
        this.location = [];
        e.forEach(i => {
            this.location.push(i.agencyId)
        });
    }

    loaderStatusChanged(e) {
        if (this.ischartDataLoaded) {
        }
    }

    SearchReport() {
        this.GetDailyTransactionPayments();
    }


    filterReset() {
        this.child.filterReset();
        this.selectedyear = (new Date()).getFullYear().toString();
        this.selectedDate = [];
        this.selectedmonth = "";
        this.getYears();
    }

    onYearchange(e) {
      
    }

    onMonthchange(e) {
      
        
        this.getDate();
    }


    getYears() {
        this.dateservice.getYear().toPromise().then(m => {
            this.years = m;
            this.years.forEach(i => {
                if (i > 2019) {
                    this.yearsSelectItem.push(
                        {
                            label: i, value: i
                        })
                }
            })

            let _selectedYear = (new Date()).getFullYear().toString();
            this.selectedyear = _selectedYear;
            this.selectedmonth = "";
            this.getMonth(this.selectedyear);

           
           
        });
    }

    sortMonth(a, b) {
        return a["sortmonthnum"] - b["sortmonthnum"];
    }

    getMonth(year: string) {
        this.monthSelectItem = [];
        let months: Array<string> = new Array<string>();
        this.selectmonthindex = 0;
        this.revenueService.getMonth(year).toPromise().then(m => {
            months = m;
            months.forEach(i => {
                this.monthSelectItem.push(
                    {
                        label: i, value: i
                    })
            })

            let monthcount = (new Date()).getMonth();
            this.selectmonthindex = monthcount - 1;
            for (var x = monthcount; x >= 0; x--) {
                let checkmonth = months.filter(m => m === this.monthNames[x])
                if (checkmonth && checkmonth.length > 0) {
                    this.selectmonthindex = x;
                    break;
                }
            }

            let _currentyear: string = (new Date()).getFullYear().toString()
            if (year === _currentyear) {
                this.selectedmonth = this.monthNames[this.selectmonthindex];
                this.getDate();
            }
            else {
                this.selectedmonth = this.monthNames[11];
                this.getDate();
            }
        })
    }
    getDate() {
        this.selectedDate = [];
        this.dateSelectItem = [];
        this.revenueService.GetDatesByMonth(this.selectedmonth, this.selectedyear).toPromise().then(m => {
            var date = m;
            date.forEach(i => {
            this.dateSelectItem.push(
                {
                    label: this.getDateString(i), value: i
                })
            })
        });
    }

    getDateString(i): string {
        let date: string = '';
        let parts = i.split('/');
        let month = this.monthNames[(parts[0] - 1)];
        date = month + " " + (parts[1]);
        return date;
    }

    onDateChange() {
        //this.TabChange();
    }
    setActualHeader(e) {
        this.selectedTabIndex = e;
    }

    GetDailyTransactionPayments() {
        this.pagerdailyPaymentCount = {};
        this.showLoader = true;
        this.dataDailyTransactonPayment = new Array<DailyTransactonPayment>();
        this.QuotesParam = new QuotesParam();
        this.allLocation= new Array<string>();
      
        this.location.forEach(i => {
            this.allLocation.push(i.toString())
        })

        if (this.selectedlocation.length > 0) {
            this.QuotesParam.agencyids = this.selectedlocation;
        }
        else {
            this.QuotesParam.agencyids = this.allLocation;
        }
        if ( this.selectedmonth != "") {
            
            this.QuotesParam.dates = this.selectedDate;
            this.revenueService.DailyTransactionPaymentsService(this.QuotesParam, this.selectedmonth, this.selectedyear).subscribe(m => {
                this.dataDailyTransactonPayment = m;
                this.setdailyPaymentCountPaging(1);
                this.showLoader = false;
            });
        }
    }
   
   
   
    

    setdailyPaymentCountPaging(page: number) {
        if (page < 1 || page > this.pagerdailyPaymentCount.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerdailyPaymentCount = this._pageservice.getPayrollPager(this.dataDailyTransactonPayment.length, page, 10);

        // get current page of items
        this.pageddailyPaymentCount = this.dataDailyTransactonPayment.slice(this.pagerdailyPaymentCount.startIndex, this.pagerdailyPaymentCount.endIndex + 1);
    }

    exportExcel() {
        this.showLoader = true;
       
        this.revenueService.ExportDailyTransactonPayment(this.dataDailyTransactonPayment).subscribe(x => {
                this._downloadService.downloadFile(x);
                console.log("FileName", x);
                this.showLoader = false;
            })
      
       
    }

}
