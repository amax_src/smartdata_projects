import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaDashboardComponent } from './alpa-dashboard.component';

describe('AlpaDashboardComponent', () => {
  let component: AlpaDashboardComponent;
  let fixture: ComponentFixture<AlpaDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
