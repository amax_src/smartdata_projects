import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as NProgress from 'nprogress';
import { Router } from '@angular/router';
import { CommonService } from '../../../core/common.service';
import { MessageService } from 'primeng/api';
import { authenticationService } from '../../../login/authenticationService.service';
@Component({
    selector: 'app-alpa-dashboard',
    templateUrl: './alpa-dashboard.component.html',
    styleUrls: ['./alpa-dashboard.component.scss']
})
export class AlpaDashboardComponent implements OnInit, AfterViewChecked {

    location: Array<number> = []
    SetSelectedlocation: Array<number> = []

    selectedzonalManager: [] = [];
    ischartDataLoaded: boolean = false;
    selectedlocation: Array<number> = new Array<number>();
    setLocation: Array<number> = new Array<number>();
    selectedRegionalManager: [] = [];
    selectedSaleDirector: [] = [];
    selectedagent: [] = [];

    ngAfterViewChecked() {
        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }
    constructor(private formBuilder: FormBuilder,

        private messageService: MessageService,
        private router: Router, private renderer: Renderer2, public cmnSrv: CommonService
        , private authenticationService: authenticationService) {
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }


    ngOnInit() {
    }

    ZmChanged(e) {
        this.selectedzonalManager = [];
        this.selectedzonalManager = e;

    }
    RmChanged(e) {
        this.selectedRegionalManager = [];
        this.selectedRegionalManager = e;
    }

    SdChanged(e) {
        this.selectedSaleDirector = [];
        this.selectedSaleDirector = e;
    }

    LocationChanged(e) {
        //this.selectedlocation = [];
        //this.selectedlocation = e;
    }

    GetAllLocations(e) {
        this.setLocation = [];
        this.location = [];
        //this.selectedlocation = [];
        console.log(e);
        this.location = e;
    }

    loaderStatusChanged(e) {
        if (this.ischartDataLoaded) {
            //this.showLoader = e;
        }
    }

}
