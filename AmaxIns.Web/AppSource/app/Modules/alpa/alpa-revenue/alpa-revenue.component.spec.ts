import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaRevenueComponent } from './alpa-revenue.component';

describe('AlpaRevenueComponent', () => {
  let component: AlpaRevenueComponent;
  let fixture: ComponentFixture<AlpaRevenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaRevenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaRevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
