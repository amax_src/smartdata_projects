import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../../core/common.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { authenticationService } from '../../../login/authenticationService.service';
import { DateService } from '../../Services/dateService';
import { Workingdays } from '../../../model/workingdays';
import { WorkingDaysService } from '../../Services/workingdaysService';
import { PolicyCount } from '../../../model/policyCount';
import { PageService } from '../../Services/pageService';
import { revenue } from '../../../model/revenue';
import { RmZmLocationDropdownAlpaComponent } from '../../../shared/rm-zm-location-dropdown-alpa/rm-zm-location-dropdown-alpa.component';
import { RevenueService } from '../../Services/revenueService';
import { ApiLoadModel } from '../../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { UserService } from '../../Services/UserService';
import { AgencyService } from '../../Services/agencyService';
import { SessionService } from '../../../core/storage/sessionservice';
@Component({
  selector: 'app-alpa-revenue',
  templateUrl: './alpa-revenue.component.html',
  styleUrls: ['./alpa-revenue.component.scss']
})
export class AlpaRevenueComponent implements OnInit {
    policyCount: Array<PolicyCount>;
    premiumCount: Array<PolicyCount>;
    agencyFee: Array<PolicyCount>;

    NewQuotes: number = 0;
    ModifiedQuotes: number = 0;
    NewQuotesSold: number = 0;
    ModifiedQuotesSold: number = 0;
    outboundcalls: number = 0;
    dateRange: string = '';
    revenueData: Array<revenue>;
    filterRevenueData: Array<revenue>;
    //filterRevenueData: Array<revenue>;

    PageName: string = "Revenue > Monthly";
    selectedRegionalManager: [];

    selectedzonalManager: []

    selectedlocation: Array<number> = []
    location: Array<number> = []

    years: Array<any>;
    yearsSelectItem: SelectItem[] = [];
    selectedyear: string = "";

    month: Array<any>;
    monthSelectItem: SelectItem[] = [];
    selectedmonth: Array<string> = [];
    currentmonth: string = "";

    workingdaysList: Array<Workingdays>;
    workingdays: Workingdays;
    showLoader: boolean = true;

    actualValue: number = 0;
    actualLable: string = "Actual Policies";
    selectmonthindex: number = 0;

    PolicyCountChardata: Array<{ _data: any, agenyName: string }>;
    AgencyFeeChardata: Array<{ _data: any, agenyName: string }>;
    PremiumChardata: Array<{ _data: any, agenyName: string }>;


    selectedTabIndex: number = 0;
    ischartDataLoaded: boolean = true;
    TotalPolicyChatdata: any
    TotalAfeeChatdata: any
    TotalPremiumChatdata: any


    PolicyCountchartOptions: any = {
        animation: {
            duration: 0
        },
        responsive: false,
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 25,
                bottom: 5
            }
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, context) {
                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        },
        legend: { display: false },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
                }
            }
        },

        scales: {
            xAxes: [{
                ticks: {
                    display: false
                }//this will remove all the x-axis grid lines
            }],
            yAxes: [{
                ticks: {
                    display: false
                } //this will remove all the x-axis grid lines
            }]
        }


    }


    chartOptions: any = {
        animation: {
            duration: 0
        },
        responsive: false,
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 25,
                bottom: 5
            }
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, context) {
                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        },
        legend: { display: false },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
                }
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    display: false
                }//this will remove all the x-axis grid lines
            }],
            yAxes: [{
                ticks: {
                    display: false
                } //this will remove all the x-axis grid lines
            }]
        }

    }



    date: Array<any>;
    dateSelectItem: SelectItem[] = [];
    selectedDate: Array<Date> = new Array<Date>();

    monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    refreshDate: string;

    pagerPolicyCount: any = {};
    pagedPolicyCount: any[];

    setPolicyCountPaging(page: number) {
        if (page < 1 || page > this.pagerPolicyCount.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerPolicyCount = this._pageservice.getPayrollPager(this.PolicyCountChardata.length, page, 10);

        // get current page of items
        this.pagedPolicyCount = this.PolicyCountChardata.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);

    }

    pagerAfee: any = {};
    pagedAfee: any[];

    setAfeePaging(page: number) {
        if (page < 1 || page > this.pagerAfee.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerAfee = this._pageservice.getPayrollPager(this.AgencyFeeChardata.length, page, 10);

        // get current page of items
        this.pagedAfee = this.AgencyFeeChardata.slice(this.pagerAfee.startIndex, this.pagerAfee.endIndex + 1);

    }

    pagerPremium: any = {};
    pagedPremium: any[];

    setPremiumPaging(page: number) {
        if (page < 1 || page > this.pagerPremium.totalPages) {
            return;
        }

        // get pager object from service
        this.pagerPremium = this._pageservice.getPayrollPager(this.PremiumChardata.length, page, 10);

        // get current page of items
        this.pagedPremium = this.PremiumChardata.slice(this.pagerPremium.startIndex, this.pagerPremium.endIndex + 1);

    }


    ngAfterViewChecked() {
        setTimeout(() => {
            this.renderer.removeClass(document.body, 'preload');
        }, 300);
    }
    constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
        private userService: UserService, private agencyService: AgencyService, private dateservice: DateService,
        private revenueService: RevenueService, private sessionService: SessionService,
        private workingDaysService: WorkingDaysService,
        private ApiLoad: ApiLoadTimeService, private _pageservice: PageService) {
        this.years = Array<any>();
        this.revenueData = new Array<revenue>();
        this.filterRevenueData = new Array<revenue>();
        this.policyCount = new Array<PolicyCount>();
        this.premiumCount = new Array<PolicyCount>();
        this.agencyFee = new Array<PolicyCount>()
        this.workingdaysList = new Array<Workingdays>();
        this.workingdays = new Workingdays();
        NProgress.configure({ showSpinner: false });
        this.renderer.addClass(document.body, 'preload');
    }

    @ViewChild(RmZmLocationDropdownAlpaComponent) child;
    ngOnInit() {
        this.RestVariable();
        this.getconsolidatedRevenueData();
    }

    RestVariable() {
        this.filterRevenueData = new Array<revenue>();
        this.policyCount = new Array<PolicyCount>();
        this.premiumCount = new Array<PolicyCount>();
        this.agencyFee = new Array<PolicyCount>();
        this.PolicyCountChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        this.AgencyFeeChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        this.PremiumChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
    }



    ZmChanged(e) {
        this.selectedzonalManager = [];
        this.selectedzonalManager = e;
    }
    RmChanged(e) {
        this.selectedRegionalManager = [];
        this.selectedRegionalManager = e;
    }
    LocationChanged(e) {
        this.selectedlocation = [];
        this.selectedlocation = e;
        //this.TabChange();
    }

    GetAllLocations(e) {
        this.selectedlocation = [];
        this.location = [];
        e.forEach(i => {
            this.location.push(i.agencyId)
        });
        //this.TabChange();
    }

    loaderStatusChanged(e) {
        if (this.ischartDataLoaded) {
            // this.showLoader = e;
        }
    }
    onYearchange(e) {
        this.selectedmonth = [];
        this.RestVariable()
        this.getMonth();
        //this.TabChange();
    }



    onMonthchange(e) {
        if (this.selectedmonth.length == 0) {
            this.selectedmonth.push(this.currentmonth);
        }
        //this.setWorkingDayForMonth();
        //this.TabChange();
    }
    SearchReport() {
        this.setWorkingDayForMonth();
        this.TabChange();
    }

    filterReset() {
        this.showLoader = true;
        this.child.filterReset();
        this.selectedmonth = [];
        this.getYears();
        this.setWorkingDayForMonth();

        setTimeout(() => {
            this.TabChange();
            this.showLoader = false;
        }, 600);

        // this.getconsolidatedRevenueData();
    }

    getYears() {
        this.dateservice.getYear().subscribe(m => {
            this.years = m;
            this.years.forEach(i => {
                this.yearsSelectItem.push(
                    {
                        label: i, value: i
                    })
            })
            let _selectedYear = (new Date()).getFullYear().toString();
            this.selectedyear = _selectedYear;
            this.getMonth();
        });
    }

    sortMonth(a, b) {
        return a["sortmonthnum"] - b["sortmonthnum"];
    }

    getMonth() {
        this.monthSelectItem = [];
        let months: Array<string> = new Array<string>();
        this.filterRevenueData = this.revenueData.filter(m => m.actYear === this.selectedyear);
        this.filterRevenueData = this.filterRevenueData.sort(this.sortMonth)
        months = Array.from(new Set(this.filterRevenueData.map(item => item.actmonth)));
        months.forEach(i => {
            this.monthSelectItem.push(
                {
                    label: i, value: i
                })
        })
        let monthcount = (new Date()).getMonth();
        this.selectmonthindex = (new Date()).getMonth() - 1;;
        for (var x = monthcount; x >= 0; x--) {
            let checkmonth = months.filter(m => m === this.monthNames[x])
            if (checkmonth && checkmonth.length > 0) {
                this.selectmonthindex = x;
                break;
            }
        }
        this.currentmonth = this.monthNames[this.selectmonthindex];
        this.selectedmonth.push(this.monthNames[this.selectmonthindex])
        //this.setWorkingDayForMonth();
    }

    getWorkingDays() {
        this.workingDaysService.getWorkingdays().subscribe(m => {
            this.workingdaysList = m;
            this.setWorkingDayForMonth();
            this.TabChange();
        });
    }

    setWorkingDayForMonth() {
        let workingdays: Array<Workingdays>;
        workingdays = this.workingdaysList.filter(m => m.month === this.currentmonth);
        workingdays = workingdays.filter(m => m.year.toString() === this.selectedyear)
        workingdays.forEach(i => {
            this.workingdays.month = i.month
            this.workingdays.remainingDays = i.remainingDays
            this.workingdays.totalWorkingdays = i.totalWorkingdays
            this.workingdays.workingDaysPassed = i.workingDaysPassed
            this.workingdays.year = i.year
        })
    }

    GetWorkingDayForMonth(month: string) {
        let workingdays: Array<Workingdays>;
        workingdays = this.workingdaysList.filter(m => m.month === month);
        workingdays = workingdays.filter(m => m.year.toString() === this.selectedyear)
        return workingdays[0];
    }

    getconsolidatedRevenueData() {
        let startTime: number = new Date().getTime();
        this.showLoader = true;
        this.revenueService.getAlpaRevenueData().subscribe(m => {
            let model: ApiLoadModel = new ApiLoadModel();
            let EndTime: number = new Date().getTime();
            let ResponseTime: number = EndTime - startTime;
            model.page = "Monthly RevenueData"
            model.time = ResponseTime
            this.revenueData = m;
            console.log("this.revenueData", this.revenueData)
            //this.GetQuotesSale();
            this.getWorkingDays();
            this.getYears();
            this.ischartDataLoaded = true;
            this.getRefreshDate();
            this.showLoader = false;
            this.ApiLoad.SaveApiTime(model).subscribe();
        });
    }

    getRefreshDate() {
        this.revenueService.getRefreshDate().subscribe(m => {
            this.refreshDate = m;
            this.TabChange();
        });
    }


    filterData() {
        this.PolicyCountChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        this.AgencyFeeChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        this.PremiumChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        this.policyCount = new Array<PolicyCount>();
        this.premiumCount = new Array<PolicyCount>();
        this.agencyFee = new Array<PolicyCount>();
        //this.GetTalkTimeOutBoundLastSevenDay();
        this.filterRevenueData = new Array<revenue>();
        let Locationfilterdata: Array<revenue> = new Array<revenue>();
        this.filterRevenueData = this.revenueData;
        this.filterRevenueData = this.revenueData.filter(m => m.actYear === this.selectedyear);
        //this.filterRevenueData = this.filterRevenueData.filter(m => m.actmonth === this.selectedmonth);
        if (this.selectedlocation && this.selectedlocation.length > 0) {
            this.selectedlocation.forEach(i => {
                let filterdata = this.filterRevenueData.filter(m => m.agencyid === i);
                filterdata.forEach(m => {
                    Locationfilterdata.push(m);
                })
            })

        }
        else if (this.location && this.location.length > 0) {
            this.location.forEach(i => {
                let filterdata = this.filterRevenueData.filter(m => m.agencyid === i);
                filterdata.forEach(m => {
                    Locationfilterdata.push(m);
                })
            })
        }
        this.CalculateRawData();
    }





    CalculateRawData() {
        this.filterRevenueData.forEach(i => {
            i.pc_actual_newbusinesscount = 0;
            i.pc_averagepolicy = 0;
            i.pc_goal = 0;
            i.pc_goalpacing = 0;
            i.pc_pacing = 0;

            i.pre_actual_premium = 0;
            i.pre_averagepremium = 0;
            i.pre_goal = 0;
            i.pre_goalpacing = 0;
            i.pre_pacing = 0;

            i.agency_goal = 0;
            i.agency_actual_fee = 0;
            i.agency_averagFee = 0;
            i.agency_pacing = 0;
            i.agency_goalpacing = 0;
        })

        this.filterRevenueData.forEach(i => {
            if (i.policiespercentage && i.new_business_count > 0) {
                i.pc_goal = i.new_business_count + Math.round(((i.policiespercentage / 100) * i.new_business_count));
            }
            else {
                i.pc_goal = i.new_business_count;
            }
            i.pre_goal = Math.round(i.pre_goal + (!isNaN(i.goalPremium) ? i.goalPremium : 0));

            if (i.agencyfeepercentage && i.agency_fee > 0) {
                i.agency_goal = i.agency_fee + Math.round(((i.agencyfeepercentage / 100) * i.agency_fee));
            }
            else {
                i.agency_goal = i.agency_fee;
            }


            i.pc_actual_newbusinesscount = i.actnewbusinesscount;
            i.pre_actual_premium = i.actpremium;
            i.agency_actual_fee = i.actagencyfee;
        })

        this.selectedmonth.forEach(e => {
            this.CalculatePolicyCount(e);
        })
    }

    CalculatePolicyCount(month: string) {
        let TempRevenueData: Array<revenue> = new Array<revenue>();
        let loc: Array<number> = [];
        let workDays: Workingdays = this.GetWorkingDayForMonth(month);
        let workingDaysPassed = 0;
        let remainingDays = 0;


        if (this.selectedlocation.length > 0) {
            loc = this.selectedlocation;
        }
        else {
            loc = this.location;
        }
        loc.forEach(x => {
            TempRevenueData = this.filterRevenueData.filter(m => m.actmonth === month && m.agencyid == x);
            TempRevenueData.forEach(i => {
                let policyCount = new PolicyCount();
                let premiumCount = new PolicyCount();
                let agencyFee = new PolicyCount();

                policyCount.goal = Math.round(policyCount.goal + (!isNaN(i.pc_goal) ? i.pc_goal : 0));
                policyCount.actual = Math.round(policyCount.actual + (!isNaN(i.pc_actual_newbusinesscount) ? i.pc_actual_newbusinesscount : 0));
                policyCount.difference = Math.round(policyCount.goal - policyCount.actual);
                policyCount.agencyId = i.agencyid.toString();
                policyCount.agencyName = i.actagencyname;

                premiumCount.goal = Math.round(premiumCount.goal + (!isNaN(i.pre_goal) ? i.pre_goal : 0));
                premiumCount.actual = Math.round(premiumCount.actual + (!isNaN(i.pre_actual_premium) ? i.pre_actual_premium : 0));
                premiumCount.difference = Math.round(premiumCount.goal - premiumCount.actual);
                premiumCount.agencyId = i.agencyid.toString();
                premiumCount.agencyName = i.actagencyname;

                agencyFee.goal = Math.round(agencyFee.goal + (!isNaN(i.agency_goal) ? i.agency_goal : 0));
                agencyFee.actual = Math.round(agencyFee.actual + (!isNaN(i.agency_actual_fee) ? i.agency_actual_fee : 0));
                agencyFee.difference = Math.round(agencyFee.goal - agencyFee.actual);
                agencyFee.agencyId = i.agencyid.toString();
                agencyFee.agencyName = i.actagencyname;


                if (workDays) {
                    workingDaysPassed = workDays.workingDaysPassed;
                    remainingDays = workDays.remainingDays;
                }
                policyCount.averagepolicy = Math.round(policyCount.actual / workingDaysPassed);
                premiumCount.averagepremium = Math.round(premiumCount.actual / workingDaysPassed);
                agencyFee.averageAgencyFee = Math.round(agencyFee.actual / workingDaysPassed);
                if (month == this.monthNames[(new Date().getMonth())] && this.selectedyear == (new Date()).getFullYear().toString()) {

                    policyCount.pacing = Math.round(policyCount.difference / remainingDays);
                    policyCount.goalpacing = Math.round(((policyCount.actual / workingDaysPassed) * remainingDays) + policyCount.actual);

                    premiumCount.pacing = Math.round(premiumCount.difference / remainingDays);
                    premiumCount.goalpacing = Math.round(((premiumCount.actual / workingDaysPassed) * remainingDays) + premiumCount.actual);

                    agencyFee.pacing = Math.round(agencyFee.difference / remainingDays);
                    agencyFee.goalpacing = Math.round(((agencyFee.actual / workingDaysPassed) * remainingDays) + agencyFee.actual);
                }
                else {
                    policyCount.pacing = 0;
                    policyCount.goalpacing = 0;

                    premiumCount.pacing = 0;
                    premiumCount.goalpacing = 0;

                    agencyFee.pacing = 0;
                    agencyFee.goalpacing = 0;
                }
                policyCount.month = month;
                premiumCount.month = month;
                agencyFee.month = month;

                this.policyCount.push(policyCount);
                this.premiumCount.push(premiumCount)
                this.agencyFee.push(agencyFee)


            })


        });
        this.renderChat(this.selectedTabIndex);
    }

    setActualHeader(e) {
        this.selectedTabIndex = e;
        this.TabChange();
    }

    TabChange() {
        this.filterData();
    }

    CreateCartDataForTotal() {
        let goal: number = 0;
        let actual: number = 0;
        let pacing: number = 0;

        this.policyCount.forEach(pd => {
            goal = goal + pd.goal;
            actual = actual + pd.actual;
            pacing = pacing + pd.goalpacing;
            //this.actualValue = this.actualValue + m.actual;
        })


        this.TotalPolicyChatdata = {
            labels: ['Goal', 'Actual', 'Pacing'],
            datasets: [
                {
                    data: [goal, actual, pacing],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }

            ]
        }

        goal = 0;
        actual = 0;
        pacing = 0;

        this.agencyFee.forEach(pd => {
            goal = goal + pd.goal;
            actual = actual + pd.actual;
            pacing = pacing + pd.goalpacing;
            //this.actualValue = this.actualValue + m.actual;
        })

        this.TotalAfeeChatdata = {
            labels: ['Goal', 'Actual', 'Pacing'],
            datasets: [
                {
                    data: [goal, actual, pacing],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }

            ]
        }
        goal = 0;
        actual = 0;
        pacing = 0;


        this.premiumCount.forEach(pd => {
            goal = goal + pd.goal;
            actual = actual + pd.actual;
            pacing = pacing + pd.goalpacing;
            //this.actualValue = this.actualValue + m.actual;
        })

        this.TotalPremiumChatdata = {
            labels: ['Goal', 'Actual', 'Pacing'],
            datasets: [
                {
                    data: [goal, actual, pacing],
                    backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }
            ]
        }
    }

    renderChat(e) {

        this.actualValue = 0;
        if (e === 0) {
            //this.policyCount.forEach(m => {
            //  this.actualValue = this.actualValue + m.actual;
            //})
            this.actualLable = "Actual Policies";
            this.CreateCartDataForPolicyCount();
        }
        else if (e === 1) {
            //this.agencyFee.forEach(m => {
            //  this.actualValue = this.actualValue + m.actual;
            //})
            this.actualLable = "Actual Agency Fee";
            this.CreateCartDataForAgencyFee();
        }
        else if (e === 2) {
            //this.premiumCount.forEach(m => {
            //  this.actualValue = this.actualValue + m.actual;
            //})
            this.actualLable = "Actual Premium";
            this.CreateCartDataForPremium();
        }
        else if (e === 3) {
            this.CreateCartDataForTotal();
        }

    }

    CreateCartDataForPolicyCount() {
        this.PolicyCountChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        let loc: Array<number> = [];
        if (this.selectedlocation.length > 0) {
            loc = this.selectedlocation;
        }
        else {
            loc = this.location;
        }

        loc.forEach(m => {
            let pdata = this.policyCount.filter(x => x.agencyId == m.toString());
            debugger
            let AgencyName = "";
            if (pdata && pdata.length > 0) {
                AgencyName = pdata[0].agencyName;
            }
           
            let goal: number = 0;
            let actual: number = 0;
            let pacing: number = 0;
            pdata.forEach(pd => {
                goal = goal + pd.goal;
                actual = actual + pd.actual;
                pacing = pacing + pd.goalpacing;
            })

            let Chatdata: any;
            Chatdata = {
                labels: ['Goal', 'Actual', 'Pacing'],
                datasets: [
                    {
                        data: [goal, actual, pacing],
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }

                ]
            }
            if (AgencyName) {
                this.PolicyCountChardata.push({ _data: Chatdata, agenyName: AgencyName })
            }
        })

        this.setPolicyCountPaging(1);

    }



    CreateCartDataForAgencyFee() {
        this.AgencyFeeChardata = new Array<{ _month: string, _data: any, agenyName: string }>();
        let loc: Array<number> = [];
        if (this.selectedlocation.length > 0) {
            loc = this.selectedlocation;
        }
        else {
            loc = this.location;
        }

        loc.forEach(m => {
            let pdata = this.agencyFee.filter(x => x.agencyId == m.toString());
            let AgencyName = "";
            if (pdata && pdata.length > 0) {
                AgencyName = pdata[0].agencyName;
            }
            let goal: number = 0;
            let actual: number = 0;
            let pacing: number = 0;
            pdata.forEach(pd => {
                goal = goal + pd.goal;
                actual = actual + pd.actual;
                pacing = pacing + pd.goalpacing;
            })

            let Chatdata: any;
            Chatdata = {
                labels: ['Goal', 'Actual', 'Pacing'],
                datasets: [
                    {
                        data: [goal, actual, pacing],
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }

                ]
            }
            if (AgencyName) {
                this.AgencyFeeChardata.push({ _data: Chatdata, agenyName: AgencyName })
            }
        })
        this.setAfeePaging(1);

    }

    CreateCartDataForPremium() {
        this.PremiumChardata = new Array<{ _month: string, _data: any, agenyName: string }>();

        let loc: Array<number> = [];
        if (this.selectedlocation.length > 0) {
            loc = this.selectedlocation;
        }
        else {
            loc = this.location;
        }

        loc.forEach(m => {
            let pdata = this.premiumCount.filter(x => x.agencyId == m.toString());
            let AgencyName = "";
            if (pdata && pdata.length > 0) {
                AgencyName = pdata[0].agencyName;
            }
            let goal: number = 0;
            let actual: number = 0;
            let pacing: number = 0;
            pdata.forEach(pd => {
                goal = goal + pd.goal;
                actual = actual + pd.actual;
                pacing = pacing + pd.goalpacing;
            })

            let Chatdata: any;
            Chatdata = {
                labels: ['Goal', 'Actual', 'Pacing'],
                datasets: [
                    {
                        data: [goal, actual, pacing],
                        backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }

                ]
            }
            if (AgencyName) {
                this.PremiumChardata.push({ _data: Chatdata, agenyName: AgencyName })
            }
        });
        this.setPremiumPaging(1);
    }
}
