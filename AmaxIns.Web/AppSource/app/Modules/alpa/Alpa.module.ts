import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { MultiSelectModule } from 'primeng/multiselect';
import { MatTabsModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import { FullCalendarModule } from '@fullcalendar/angular';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';
import { AlpaDashboardComponent } from './alpa-dashboard/alpa-dashboard.component';
import { AlpaRevenueComponent } from './alpa-revenue/alpa-revenue.component';
import { AlpaEprComponent } from './alpa-epr/alpa-epr.component';
import { AlpaAisComponent } from './alpa-ais/alpa-ais.component';
import { AlpaDailyrevenueComponent } from './alpa-dailyrevenue/alpa-dailyrevenue.component';
import {TiergoalComponent } from './tiergoal/tiergoal.component';
import { AlpaRoutingModule } from './Alpa-routing.module';
import { DailyTransactionPaymentsComponent } from './daily-transaction-payments/daily-transaction-payments.component';

@NgModule({
  imports: [SharedModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    FullCalendarModule,
    DialogModule,
        AlpaRoutingModule,
    FormsModule, ReactiveFormsModule,
    ToastModule,
    ButtonModule,
    MultiSelectModule,
    MatTabsModule,
    ChartModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [AlpaDashboardComponent, AlpaRevenueComponent, AlpaEprComponent, AlpaAisComponent, AlpaDailyrevenueComponent, TiergoalComponent, DailyTransactionPaymentsComponent],
  exports: []
})
export class AlpaModule { }
