import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { DashBoardComponent } from './DashBoard.component';
import { DashBoardComponentRoutingModule } from './DashBoard-routing.module';


@NgModule({
  imports: [
    SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    DashBoardComponentRoutingModule,
    ChartModule,
  ],
  declarations: [DashBoardComponent],
  exports: []
})
export class DashBoardModule { }
