import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { DateService } from '../Services/dateService';
import { MonthModel } from '../../model/MonthModel';
import { PageService } from '../Services/pageService';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-DashBoard',
  templateUrl: './DashBoard.component.html',
  styleUrls: ['./DashBoard.component.scss']
})

export class DashBoardComponent implements OnInit, AfterViewChecked {

  showLoader: boolean = false;
  pager: any = {};
  pagedItems: any[] = [];
  location: Array<Location> = new Array<Location>();
  SetSelectedlocation: Array<Location> = new Array<Location>();
  PageName: string = "Dashboard";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedRegionalManager: [] = [];
  selectedSaleDirector: [] = [];
  selectedagent: [] = [];
  year: string = '';
    _year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";
  _selectedmonth: string = ((new Date()).getMonth() + 1).toString();
  //selectmonthindex: number = 0;
  IsLoadPageFirstTime: boolean = true;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  constructor(private router: Router,
    private _pageservice: PageService,
    private renderer: Renderer2, public cmnSrv: CommonService, private ApiLoad: ApiLoadTimeService, private _DateService: DateService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
      this.year = (new Date()).getFullYear().toString();
    this.selectedmonth = ((new Date()).getMonth() + 1).toString();
    this.fillYear()
    this.getMonth();
    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


    ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
    RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }

    SdChanged(e) {
        this.selectedSaleDirector = [];
        this.selectedSaleDirector = e;
    }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
  
    //this.setSelectedLocations();
    this.GetData();
  }

    GetAllLocations(e) {
        this.setLocation = [];
        this.location = [];
        this.selectedlocation = [];
        console.log(e);
        this.location = e;
        if (this.IsLoadPageFirstTime) {
            this.setSelectedLocations();
        }

        if (this.ischartDataLoaded) {
            // this.filterData();
        }

        //this.selectedlocation.push(this.location[0].agencyId);
        //this.setLocation.push(this.location[0].agencyId);
        this.GetData();
    }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  GetData() {
    //this.showLoader = true;
    //let startTime: number = new Date().getTime();
    //this.eprService.getEPRData(this.selectedmonth, this.selectedlocation).subscribe(m => {
    //  let model: ApiLoadModel = new ApiLoadModel();
    //  let EndTime: number = new Date().getTime();
    //  let ResponseTime: number = EndTime - startTime;
    //  model.page = "EPR Data"
    //  model.time = ResponseTime
    //  this.EprData = m;
    //  //this.eprDataFilter = this.EprData;
    //  this.ischartDataLoaded = true;
    //  //this.getMonth();
    //  this.showLoader = false;
    //  this.getDate();
    //  this.filterData();
    //  this.setPage(1);
    //  this.ApiLoad.SaveApiTime(model).subscribe();
    //});
  }

  fillYear() {
      this.YearSelectItem = [];
      this.YearSelectItem.push(
          {
              label: '2023', value: '2023'
          })
    this.YearSelectItem.push(
      {
        label: '2022', value: '2022'
      })
    this.YearSelectItem.push(
      {
        label: '2021', value: '2021'
      })
    this.YearSelectItem.push(
      {
        label: '2020', value: '2020'
      })
    
  }

  getMonth() {
    this._DateService.GetMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      months = m;
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })
      let monthcount = (new Date()).getMonth();
      this.selectedmonth = ((new Date()).getMonth()+1).toString();
      
      this.GetData();
    })
  }

  SearchgetMonth() {
    this._DateService.GetMonthskeyvalue(this._year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      months = m;
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })
      let monthcount = (new Date()).getMonth();
      this._selectedmonth = ((new Date()).getMonth() + 1).toString();

      this.GetData();
    })
  }

  onMonthchange(e) {
     this.GetData();
  }

  onYearchange() {
    this.SearchgetMonth();
  }

  SearchReport() {
    console.log("this.location", this.location);
    this.selectedmonth = this._selectedmonth;
    this.year = this._year;
    this.setSelectedLocations();
  }

  filterReset() {
    this.child.filterReset();
      this._year = (new Date()).getFullYear().toString();
    this._selectedmonth = ((new Date()).getMonth() + 1).toString();
      this.year = (new Date()).getFullYear().toString();
    this.selectedmonth = ((new Date()).getMonth() + 1).toString();

    setTimeout(() => {
      this.setSelectedLocations();
    }, 100);
    
  }

  setSelectedLocations() {
    this.IsLoadPageFirstTime = false;
    this.SetSelectedlocation = new Array<Location>();
    if (this.selectedlocation.length > 0) {
      let temp: Array<Location> = new Array<Location>();
      this.selectedlocation.forEach(m => {
        temp = new Array<Location>();
        temp = this.location.filter(x => x.agencyId === m)
        temp.forEach(a => {
          this.SetSelectedlocation.push(a);
        })
      })
    }
    else {
      this.SetSelectedlocation = this.location;
    }
    this.pager = {};
    this.pagedItems = [];
    this.setPage(1)
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this._pageservice.getPager(this.SetSelectedlocation.length, page);
    this.pagedItems = this.SetSelectedlocation.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

}
