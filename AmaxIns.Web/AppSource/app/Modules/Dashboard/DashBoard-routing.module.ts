import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { DashBoardComponent } from './DashBoard.component';


const routes: Routes = [
  { path: 'Dashboard', component: DashBoardComponent, canActivate: [LoginGuard]} 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DashBoardComponentRoutingModule { }
