import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { HourlyLoadComponent } from './hourlyLoad.component';


const routes: Routes = [
  { path: 'Hourlyload', component: HourlyLoadComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HourlyLoadRoutingModule { }
