import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { HourlyLoadComponent } from './hourlyLoad.component';
import { HourlyLoadRoutingModule } from './hourlyLoad-routing.module';


@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    HourlyLoadRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [HourlyLoadComponent],
  exports: []
 })
export class HourlyLoadModule { }
