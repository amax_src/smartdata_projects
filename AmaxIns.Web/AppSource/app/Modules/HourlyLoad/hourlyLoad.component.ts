import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';

import { Location } from '../../model/location';

import * as _ from 'underscore';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';

import { HourlyLoadService } from '../Services/hourlyLoadService';
import { MonthModel } from '../../model/MonthModel'
import { HourlyLoadModel } from '../../model/HourlyLoadModel'
import { HourlyNewModifiedQuotes } from '../../model/hourlyNewModifiedQuotes';

@Component({
  selector: 'app-hourlyLoad',
  templateUrl: './hourlyLoad.component.html',
  styleUrls: ['./hourlyLoad.component.scss']
})

export class HourlyLoadComponent implements OnInit, AfterViewChecked {

  singleLocationSelected: boolean = true;
  singleLocation:number;
  location: Array<Location> = new Array<Location>();
  hourlyData: Array<HourlyLoadModel> = new Array<HourlyLoadModel>();
  hourlyQuoteData: Array<HourlyNewModifiedQuotes> = new Array<HourlyNewModifiedQuotes>();
  PageName: string = "Hourly WorkLoad";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedRegionalManager: [] = [];
 
  month: Array<MonthModel>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  date: Array<string>;
  dateSelectItem: SelectItem[] = [];
  selecteddate: string = "";


 
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
   

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
   private ApiLoad: ApiLoadTimeService,private hourlyservice:HourlyLoadService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.getMonth();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
  }

SetSingleLocation(e) {
  this.singleLocation = e;
  this.getDataForAllTabs();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = e;
    this.singleLocation = e[0].agencyId;
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }


  getMonth()
  {
    this.hourlyservice.getMonth().subscribe(m => {
      this.month = m;
      let first: boolean = true;
      this.month.forEach(m => {
        this.monthSelectItem.push({ label: m.name, value: m.id })
        if (first) {
          this.selectedmonth = m.id;
          first = false;
        }
      })
      this.getDate();
    })
  }

  getDate() {
    this.dateSelectItem = [];
    this.hourlyservice.getDate(this.selectedmonth).subscribe(m => {
      this.date = m;
      let first: boolean = true;
      this.date.forEach(m => {
        this.dateSelectItem.push({ label: this.getDateString(m), value: m })
        if (first) {
          this.selecteddate = m;
          first = false;
        }
      })
      this.getDataForAllTabs();
    })
  }
  
  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[1] - 1)];
    date = month + " " + (parts[2]);
    return date;
  }

  
  onMonthchange(e) {
    this.getDate();
  }

  onDatechange(e) {
    this.getDataForAllTabs();
  }


  getdata(date: string, agencyId: number) {

    this.hourlyData = new Array<HourlyLoadModel>();
    this.hourlyservice.getData(date,agencyId).subscribe(m => {
      this.hourlyData = m;
    })
  }

  gethourlyQuoteData(date: string, agencyId: number) {

    this.hourlyQuoteData = new Array<HourlyNewModifiedQuotes>();
    this.hourlyservice.getQuoteHourlyData(date, agencyId).subscribe(m => {
      this.hourlyQuoteData = m;
    })
  }

  getDataForAllTabs() {
    this.getdata(this.selecteddate, this.singleLocation);
    this.gethourlyQuoteData(this.selecteddate, this.singleLocation);
  }


}
