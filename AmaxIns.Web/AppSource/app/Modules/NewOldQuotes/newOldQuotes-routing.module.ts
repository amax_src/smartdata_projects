import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { newOldQuotesComponent } from './newOldQuotes.component';


const routes: Routes = [
  { path: 'NewModifiedQuotes', component: newOldQuotesComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NewOldQuotesRoutingModule { }
