import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { Location } from '../../model/location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { QuotesService } from '../Services/quotesService';
import { NewModifiedQuotes, QuotesProjection, QuotesParam } from '../../model/NewModifiedQuotes';
import { SelectItem } from 'primeng/api';
import { PageService } from '../Services/pageService';
import { DateService } from '../Services/dateService';
import { MonthModel } from '../../model/MonthModel';
import { debug } from 'util';
import { DownloadexcelService } from '../Services/downloadexcel.service';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';
import { elementStart } from '@angular/core/src/render3';


@Component({
  selector: 'app-newOldQuotes',
  templateUrl: './newOldQuotes.component.html',
  styleUrls: ['./newOldQuotes.component.scss']
})

export class newOldQuotesComponent implements OnInit, AfterViewChecked {


  location: Array<Location> = new Array<Location>();
  PageName: string = "New and Modified Quotes";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<string> = new Array<string>();
  setLocation: Array<number> = new Array<number>();

  QuotesData: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  NewQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  ModifiedQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();

  QuotesDistinctDate: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();

  QuotesParam: QuotesParam;
  NewQuotesChatrdata: Array<{ _data: any, agenyName: string }>;
  ModifiyQuotesChatrdata: Array<{ _data: any, agenyName: string }>;

  NewQuoteCount: number = 0;
  ModifiedQuoteCount: number = 0;
  NewPolicyCount: number = 0;
  ModifiedPolicyCount: number = 0;
  PolicyCount: number = 0;
  selectedRegionalManager: [] = [];
  NewQuotesPojection: number = 0;
  ModifiedQuotesProjection: number = 0;

  IsShowBOLTiels: boolean = false;
  VisitedCarrierWebsite: number = 0;

    TotalSoldPolicy: number = 0;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<any> = new Array<any>();
  selectedTab: number = 0;
  NewQuotesfilter: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  ModifiedQuotesfilter: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
  QuotesProjectiondata: Array<QuotesProjection> = new Array<QuotesProjection>();

  pagerNewQuotes: any = {};
  pagedNewQuotesItems: any[];

  pagerModifiedQuotes: any = {};
  pagedModifiedQuotesItems: any[];

  isLoadFirstTime: boolean = true;


  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private Service: QuotesService,
    private ApiLoad: ApiLoadTimeService, private _pageservice: PageService, private _DateService: DateService, private _downloadExcel: DownloadexcelService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  @ViewChild(RmZmLocationDropdownComponent) child;

  ngOnInit() {
    this.QuotesParam = new QuotesParam();
    this.fillYear();
    this.getMonth();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.GetData();
  }

  GetAllLocations(e) {
    this.setLocation = [];
    this.selectedlocation = [];
    this.location = e;
    this.selectedlocation.push(this.location[0].agencyId.toString());
    this.setLocation.push(this.location[0].agencyId);
    //this.GetData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  SearchReport() {
    this.GetDataQuotes();
    //this.GetData();
  }

  filterReset() {
    this.child.filterReset();
    this.year = "2023";
    this.selectedDate = [];
    this.fillYear();
    this.getMonth();
  }

  fillYear() {
    this.YearSelectItem = [];
    this.YearSelectItem.push(
      {
        label: '2020', value: '2020'
      })
    this.YearSelectItem.push(
      {
        label: '2021', value: '2021'
      })
    this.YearSelectItem.push(
      {
        label: '2022', value: '2022'
        })
      this.YearSelectItem.push(
          {
              label: '2023', value: '2023'
          })
  }
  GetQuotesProjectionData() {
    this.QuotesProjectiondata = new Array<QuotesProjection>();
    this.Service.GetQuotesProjection().subscribe(m => {
      this.QuotesProjectiondata = m;
    });
  }

  ProjectionTiels() {
    this.NewQuotesChatrdata = new Array<{ _month: string, _data: any, agenyName: string }>();
    this.ModifiyQuotesChatrdata = new Array<{ _month: string, _data: any, agenyName: string }>();
    this.NewQuotesPojection = 0;
    this.ModifiedQuotesProjection = 0;
    let newQuotesPercentage = 0;
    let modififyQuotesPercentage = 0;
    let list = this.QuotesProjectiondata.filter(x => x.projectionMonthId == parseInt(this.selectedmonth) && x.projectionYear == parseInt(this.year));
    let counter = 0;
    if (list != null) {
      this.selectedlocation.forEach(m => {
        list.filter(x => x.agencyId == parseInt(m)).forEach(j => {
          this.NewQuotesPojection = this.NewQuotesPojection + j.createdQuotesProjection;
          this.ModifiedQuotesProjection = this.ModifiedQuotesProjection + j.modifiedQuotesProjection;
          newQuotesPercentage = newQuotesPercentage + j.createdQuotesCR;
          modififyQuotesPercentage = modififyQuotesPercentage + j.modifiedQuotesCR;
          counter = counter + 1;
        })
      });

      let actualQuotes = ((this.NewQuotesPojection > 0 ? (this.NewQuoteCount / (this.NewQuotesPojection)) * 100 : 0))
      let QuotesProjection = (newQuotesPercentage > 0 ? (newQuotesPercentage / counter) : 0);
      let differenceQuotes = (actualQuotes - QuotesProjection );
      let Chatdata: any;
      Chatdata = {
        labels: ['CR Goal', 'CR Actual', 'Difference'],
        datasets: [
          {
            data: [Math.round(QuotesProjection), Math.round(actualQuotes), Math.round(differenceQuotes)],
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            hoverBackgroundColor: [
              "#FF6384",
              "#36A2EB",
              "#FFCE56"
            ]
          }

        ]
      }
      this.NewQuotesChatrdata.push({ _data: Chatdata, agenyName: "Created CR Ratio" })


      actualQuotes = (this.ModifiedQuotesProjection > 0 ? (this.ModifiedQuoteCount / (this.ModifiedQuotesProjection)) * 100 : 0)
      QuotesProjection = (modififyQuotesPercentage > 0 ? (modififyQuotesPercentage / counter) : 0);
       differenceQuotes = (actualQuotes - QuotesProjection);
     let  Chatdatam: any;
      Chatdatam = {
        labels: ['CR Goal', 'CR Actual', 'Difference'],
        datasets: [
          {
            data: [Math.round(QuotesProjection), Math.round(actualQuotes), Math.round(differenceQuotes)],
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            hoverBackgroundColor: [
              "#FF6384",
              "#36A2EB",
              "#FFCE56"
            ]
          }

        ]
      }
      this.ModifiyQuotesChatrdata.push({ _data: Chatdatam, agenyName: "Modified CR Ratio" })
    }
  }
    GetSoldQuotesDataForModifiedTiles() {
        this.TotalSoldPolicy = 0;
        this.QuotesParam = new QuotesParam();
        if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
            this.QuotesParam.agencyids = this.selectedlocation;
            this.QuotesParam.dates = this.selectedDate;

            this.Service.ModifiedQuotesSoldTiles(this.QuotesParam, this.selectedmonth, this.year).subscribe(m => {
                this.TotalSoldPolicy = m;
                console.log("this.TotalSoldPolicy", this.TotalSoldPolicy)
            });
        }
    }

  GetDataQuotes() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.QuotesData = new Array<NewModifiedQuotes>();
    this.NewQuotes = new Array<NewModifiedQuotes>();
    this.ModifiedQuotes = new Array<NewModifiedQuotes>();
    //this.GetSoldQuotesDataForModifiedTiles();
      this.QuotesParam = new QuotesParam();
    if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
      this.QuotesParam.agencyids = this.selectedlocation;
      this.QuotesParam.dates = this.selectedDate;
      this.Service.GetModifiedNewQuotes_New(this.QuotesParam, this.selectedmonth, this.year).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "New and Modified Quote Data Last Seven Days"
        model.time = ResponseTime
        this.QuotesData = m;
        this.NewQuotes = this.QuotesData.filter(m => m.quoteType === 'new')
        this.ModifiedQuotes = this.QuotesData.filter(m => m.quoteType === 'modified')

        this.NewQuotesfilter = this.NewQuotes
        this.ModifiedQuotesfilter = this.ModifiedQuotes
        this.filterData();
        this.ischartDataLoaded = true;
        //this.showLoader = false;
        this.isLoadFirstTime = false;
        this.ApiLoad.SaveApiTime(model).subscribe();
      });
    }
    else {
      this.NewQuotesfilter = new Array<NewModifiedQuotes>();
      this.ModifiedQuotesfilter = new Array<NewModifiedQuotes>();
      this.filterData();
      this.ischartDataLoaded = true;
      //this.showLoader = false;
    }
  }

  GetData() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.QuotesData = new Array<NewModifiedQuotes>();
    this.NewQuotes = new Array<NewModifiedQuotes>();
    this.ModifiedQuotes = new Array<NewModifiedQuotes>();
    //this.GetSoldQuotesDataForModifiedTiles();
    if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
      this.Service.GetModifiedNewQuotes(this.selectedlocation, this.selectedmonth,this.year).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "New and Modified Quote Data Last Seven Days"
        model.time = ResponseTime
        this.QuotesData = m;
        this.NewQuotes = this.QuotesData.filter(m => m.quoteType === 'new') 
        this.ModifiedQuotes = this.QuotesData.filter(m => m.quoteType === 'modified') 
        
        this.NewQuotesfilter = this.NewQuotes
        this.ModifiedQuotesfilter = this.ModifiedQuotes
        this.filterData();
        this.ischartDataLoaded = true;
        //this.showLoader = false;
        this.isLoadFirstTime = false;
        this.ApiLoad.SaveApiTime(model).subscribe();
        this.GetDataQuotes();
      });
    }
    else {
      this.NewQuotesfilter = new Array<NewModifiedQuotes>();
      this.ModifiedQuotesfilter = new Array<NewModifiedQuotes>();
      this.filterData();
      this.ischartDataLoaded = true;
      //this.showLoader = false;
    }
  }

    // Not used
  SearchGetData() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.QuotesData = new Array<NewModifiedQuotes>();
    this.NewQuotes = new Array<NewModifiedQuotes>(); 
    this.ModifiedQuotes = new Array<NewModifiedQuotes>();
    //this.GetSoldQuotesDataForModifiedTiles();
    if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
      this.Service.GetModifiedNewQuotes(this.selectedlocation, this.selectedmonth, this.year).subscribe(m => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = "New and Modified Quote Data Last Seven Days"
        model.time = ResponseTime
        this.QuotesData = m;
        this.NewQuotes = this.QuotesData.filter(m => m.quoteType === 'new') 
        this.ModifiedQuotes = this.QuotesData.filter(m => m.quoteType === 'modified') 
        this.NewQuotesfilter = this.NewQuotes
        this.ModifiedQuotesfilter = this.ModifiedQuotes
        //this.filterData();
        this.ischartDataLoaded = true;
        this.showLoader = false;
        this.isLoadFirstTime = false;
        //this.setDates();
        this.ApiLoad.SaveApiTime(model).subscribe();
      });
    }
    else {
      this.NewQuotesfilter = new Array<NewModifiedQuotes>();
      this.ModifiedQuotesfilter = new Array<NewModifiedQuotes>();
      //this.filterData();
      this.ischartDataLoaded = true;
      this.showLoader = false;
    }
  }

  filterData() {
    this.pagerNewQuotes = {};
    this.pagedNewQuotesItems = [];
    this.pagerModifiedQuotes = {};
    this.pagedModifiedQuotesItems = [];

      if ((this.year == '2020')) {  //this.year == '2021' ||
      if (this.selectedDate.length > 0) {
       
        let tempNewQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
        let tempModifiedQuotes: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
        this.NewQuotesfilter = [];
        this.ModifiedQuotesfilter = [];

        this.selectedDate.forEach(m => {
          tempNewQuotes = this.NewQuotes.filter(x => x.createDate === m);
          tempModifiedQuotes = this.ModifiedQuotes.filter(x => x.createDate === m);

          tempNewQuotes.forEach(x => {
            this.NewQuotesfilter.push(x);
          })

          tempModifiedQuotes.forEach(x => {
            this.ModifiedQuotesfilter.push(x);
          })

        })

      }
      else {
        this.NewQuotesfilter = this.NewQuotes
        this.ModifiedQuotesfilter = this.ModifiedQuotes
      }
    }
    else {
      this.NewQuotesfilter = this.NewQuotes
      this.ModifiedQuotesfilter = this.ModifiedQuotes
    }
   

    this.NewQuotesfilter = this.NewQuotesfilter
    this.ModifiedQuotesfilter = this.ModifiedQuotesfilter
    let recordWithPolicyNum: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
    let recordWithoutPolicyNum: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
    recordWithPolicyNum = this.NewQuotesfilter.filter(m => m.policyNumber);
    recordWithoutPolicyNum = this.NewQuotesfilter.filter(m => !(m.policyNumber));

    this.NewQuotesfilter = new Array<NewModifiedQuotes>();
    recordWithPolicyNum.forEach(m => {
      this.NewQuotesfilter.push(m)
    })

    recordWithoutPolicyNum.forEach(m => {
      this.NewQuotesfilter.push(m)
    })

    recordWithPolicyNum = new Array<NewModifiedQuotes>();
    recordWithoutPolicyNum = new Array<NewModifiedQuotes>();
    recordWithPolicyNum = this.ModifiedQuotesfilter.filter(m => m.policyNumber);
    recordWithoutPolicyNum = this.ModifiedQuotesfilter.filter(m => !(m.policyNumber));
    this.ModifiedQuotesfilter = new Array<NewModifiedQuotes>();
    recordWithPolicyNum.forEach(m => {
      this.ModifiedQuotesfilter.push(m)
    })

    recordWithoutPolicyNum.forEach(m => {
      this.ModifiedQuotesfilter.push(m)
    })

    this.setNewQuotesPaging(1);
    this.setModifiedQuotesPaging(1);
    this.getTileData();
    //this.setDates();
  }


    getTileData() {
        this.NewPolicyCount = 0;
        this.ModifiedPolicyCount = 0;
        let NewQuoteswithpolicy: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
        NewQuoteswithpolicy = this.NewQuotesfilter.filter(m => m.policyNumber)
        //this.NewPolicyCount = NewQuoteswithpolicy.length;
        let ModifiedQuoteswithpolicy: Array<NewModifiedQuotes> = new Array<NewModifiedQuotes>();
        ModifiedQuoteswithpolicy = this.ModifiedQuotesfilter.filter(m => m.policyNumber)
        //this.ModifiedPolicyCount = ModifiedQuoteswithpolicy.length;
        this.NewQuoteCount = this.NewQuotesfilter.length;
        //this.NewQuoteCount = this.NewQuotesfilter.filter(m => (m.firstName != '' || m.lastname != '') && m.phone != '').length;
        this.ModifiedQuoteCount = this.ModifiedQuotesfilter.length;
        this.showLoader = true;
        this.TotalSoldPolicy = 0;
        this.QuotesParam = new QuotesParam();
        if (this.selectedlocation.length > 0 && this.selectedmonth != "") {
            this.QuotesParam.agencyids = this.selectedlocation;
            this.QuotesParam.dates = this.selectedDate;

            this.Service.ModifiedQuotesSoldTiles(this.QuotesParam, this.selectedmonth, this.year).subscribe(m => {
                this.TotalSoldPolicy = m;
                console.log("this.TotalSoldPolicy", this.TotalSoldPolicy);
                //this.ModifiedPolicyCount = ModifiedQuoteswithpolicy.length;
                this.NewPolicyCount = NewQuoteswithpolicy.length;
               
                this.ModifiedPolicyCount = (this.TotalSoldPolicy - this.NewPolicyCount);
                if (this.ModifiedPolicyCount <= 0) {
                    this.ModifiedPolicyCount = 0;
                }
                this.showLoader = false;
            });
        }

        this.IsShowBOLTiels = false;
        if (this.selectedlocation.length == 1 && this.selectedlocation[0] == '221') {
            this.IsShowBOLTiels = true;
            this.VisitedCarrierWebsite = this.NewQuotesfilter.filter(m => m.cat === 1).length;
            console.log("this.VisitedCarrierWebsite", this.VisitedCarrierWebsite);
            console.log("this.NewQuotesfilter", this.NewQuotesfilter);
        }
    }


  setDates() {
    this.QuotesDistinctDate = new Array<NewModifiedQuotes>();
    this.Service.GetDistinctDateQuotes(this.selectedmonth, this.year).subscribe(m => {
      this.QuotesDistinctDate = m;
      this.GetDate(this.QuotesDistinctDate);
      console.log("this.QuotesDistinctDate", this.QuotesDistinctDate);
    });
   
  }

  GetDate(x: Array<NewModifiedQuotes>) {
    this.dateSelectItem = []
    let dt: Array<string> = new Array<string>();
    x.forEach(i => {
      if (dt.length === 0) {
        dt.push(i.createDate);
      }
      else {
        let isUnique: boolean = true;
        dt.forEach(c => {
          if (c === i.createDate) {
            isUnique = false;
          }
        })
        if (isUnique) {
          dt.push(i.createDate);
        }
      }
    })
    dt.forEach(i => {
      this.dateSelectItem.push(
        {
          label: this.getDateString(i), value: i
          //label: i, value: i

        })
    });
    console.log("this.dateSelectItem11", this.dateSelectItem);
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  OndateChanged() {
    //this.filterData();
  }

  tabChange(e) {
    this.selectedTab = e;
    //this.setDates();
  }
  onYearchange() {
    if (this.isLoadFirstTime) {
      this.getMonth();
      this.selectedDate = new Array<any>()
    }
    else {
      this.getMonthSecondTime();
      this.selectedDate = new Array<any>()
    }
    //this.GetData();isLoadFirstTime
  }


  getMonthSecondTime() {
    this._DateService.GetMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      let tempmonths: Array<MonthModel> = new Array<MonthModel>();
      let _Date = new Date();
      months = m;
      if (months.length > 2 && parseInt(this.year) == _Date.getFullYear()) {
          tempmonths = months;// months.slice(months.length - 5, months.length);
      }
      else {
        tempmonths = months;
      }
      months = tempmonths.reverse();
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })

      this.selectedmonth = months[0].id

      setTimeout(() => {
        this.setDates();
        //this.SearchGetData();
      }, 500);
    })
  }

  getMonth() {
    this._DateService.GetMonthskeyvalue(this.year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<MonthModel> = new Array<MonthModel>();
      let tempmonths: Array<MonthModel> = new Array<MonthModel>();
      let _Date = new Date();
      months = m;
      
      if (months.length > 2 && parseInt(this.year) == _Date.getFullYear()) {
          tempmonths = months;// months.slice(months.length - 5, months.length);
      }
      else {
        tempmonths = months;
      }
      months = tempmonths.reverse();
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i.name, value: i.id
          })
      })

      this.selectedmonth = months[0].id

      setTimeout(() => {
        this.setDates();
        //this.GetData();
        this.GetDataQuotes();
      }, 500);

     
    })
    //this.filterData();
  }

  onMonthchange() {
    this.selectedDate = new Array<any>()
    this.setDates();
    //this.GetData();
  }

  setNewQuotesPaging(page: number) {
    
    if (page < 1 || page > this.pagerNewQuotes.totalPages) {
      return;
    }

    this.pagerNewQuotes = this._pageservice.getPager(this.NewQuotesfilter.length, page, 50);

    this.pagedNewQuotesItems = this.NewQuotesfilter.slice(this.pagerNewQuotes.startIndex, this.pagerNewQuotes.endIndex + 1);
  }

  setModifiedQuotesPaging(page: number) {
    if (page < 1 || page > this.pagerModifiedQuotes.totalPages) {
      return;
    }

    this.pagerModifiedQuotes = this._pageservice.getPager(this.ModifiedQuotesfilter.length, page, 50);

    this.pagedModifiedQuotesItems = this.ModifiedQuotesfilter.slice(this.pagerModifiedQuotes.startIndex, this.pagerModifiedQuotes.endIndex + 1);
  }


  exportExcel() {
    this.showLoader = true;
    if (this.selectedTab == 0) {
      this.Service.ExportNewQuotes(this.NewQuotesfilter).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else if (this.selectedTab == 1) {
      this.Service.ExportModiFiedQuotes(this.ModifiedQuotesfilter).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
  }
}
