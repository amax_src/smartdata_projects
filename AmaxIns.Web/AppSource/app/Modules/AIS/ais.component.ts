import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Ais } from '../../model/Ais';
import { Location } from '../../model/location';
import { AISService } from '../Services/aisService';
import { DateService } from '../Services/dateService';
import { AisChartData } from '../../model/aisChartData';


@Component({
  selector: 'app-AIS',
  templateUrl: './ais.component.html',
  styleUrls: ['./ais.component.scss']
})

export class AISComponent implements OnInit, AfterViewChecked {

  location: Array<Location> = new Array<Location>();
  PageName: string = "AIS Production";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: [] = [];
  selectedRegionalManager: [] = [];
  AISData: Array<Ais> = new Array<Ais>();
  FilterAISData: Array<Ais> = new Array<Ais>();

  PremiumByMonthChart: any;
  AgencyFeeByMonthChart: any;
  PolicyCountByMonthChart: any;
  LinechartMonthChart: any;

  PremiumByMonth: AisChartData = new AisChartData();
  AgencyFeeByMonth: AisChartData = new AisChartData();
  PolicyCountByMonth: AisChartData = new AisChartData();

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  agentSelectItem: SelectItem[] = [];
  selectedagent: [] = [];
  selectedLocationCount: number = 0
  selectedAgentCount: number = 0

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];




  LinechartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25
      }
    },

    legend: { display: true },
    scales: {
      yAxes: [{
        ticks: {
          callback: function (label) {
            return Math.round(label / 1000) + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }

  LinechartOptionsForPolicyCount: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 25
      }
    },
    legend: { display: false },
    scales: {
      yAxes: [{
        ticks: {
          callback: function (label) {
            return Math.round(label / 1000) + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }


  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private aisService: AISService, private dateservice: DateService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.getYears();
    this.getAISData();

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
   
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
   
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.selectedagent = [];
    this.selectedLocationCount = this.selectedlocation.length;
    this.filterData();
  }

  GetAllLocations(e) {
 this.selectedlocation = [];
    this.location = e;
    this.selectedagent = [];
    this.selectedLocationCount = this.location.length;
    this.filterData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.showLoader = e;
    }
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
    });
  }

  getAISData() {
    this.aisService.getAISData().subscribe(m => {
      this.AISData = m;
      this.FilterAISData = this.AISData;
      this.getAgents(this.FilterAISData);
      this.filterData();
      this.ischartDataLoaded = true;
      this.showLoader = false
    });
  }

  getAgents(data: Array<Ais>) {
    this.agentSelectItem = []
    let agents: Array<string> = new Array<string>();
    agents = Array.from(new Set(data.map(item => item.agentName)));
    if (this.selectedagent && this.selectedagent.length > 0) {
      this.selectedAgentCount = this.selectedagent.length;
    }
    else {
      this.selectedAgentCount = agents.length;
    }
    agents.forEach(i => {
      this.agentSelectItem.push(
        {
          label: i, value: i
        })
    })
  }

  onYearchange(e) {
    this.filterData();
  }

  onAgentChange() {
    this.filterData();
    this.selectedAgentCount = this.selectedagent.length;
  }

  filterData() {

    this.FilterAISData = this.AISData.filter(m => m.year === this.selectedyear);
    let FilterAISDatabyLocation: Array<Ais> = new Array<Ais>();
    let FilterAISDatabyAgent: Array<Ais> = new Array<Ais>();
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        let tempAISData: Array<Ais> = new Array<Ais>();
        tempAISData = this.FilterAISData.filter(x => x.agencyId === m)
        tempAISData.forEach(a => {
          FilterAISDatabyLocation.push(a);
        })
      })
    }
    else {
      this.location.forEach(m => {
        let tempAISData: Array<Ais> = new Array<Ais>();
        tempAISData = this.FilterAISData.filter(x => x.agencyId === m.agencyId)
        tempAISData.forEach(a => {
          FilterAISDatabyLocation.push(a);
        })
      })
    }

    this.getAgents(FilterAISDatabyLocation);

      this.FilterAISData = FilterAISDatabyLocation;

    if (this.selectedagent && this.selectedagent.length > 0) {
      this.selectedagent.forEach(m => {
        let tempAISData: Array<Ais> = new Array<Ais>();
        tempAISData = this.FilterAISData.filter(x => x.agentName === m)
        tempAISData.forEach(a => {
          FilterAISDatabyAgent.push(a);
        })
      })
    }

    if (FilterAISDatabyAgent && FilterAISDatabyAgent.length > 0) {
      this.FilterAISData = FilterAISDatabyAgent;
    }

    this.prepareConsolidatedData();

  }


  prepareConsolidatedData() {
    this.AgencyFeeByMonth = new AisChartData();
    this.PremiumByMonth = new AisChartData();
    this.PolicyCountByMonth = new AisChartData();

    for (var x = 0; x < this.monthNames.length; x++) {
      let tempAISData: Array<Ais> = new Array<Ais>();
      tempAISData = this.FilterAISData.filter(m => m.month === this.monthNames[x])
      let sumPremium: number = 0;
      let sumPolicy: number = 0;
      let sumAgencyFee: number = 0;
      tempAISData.forEach(m => {
        sumPremium = sumPremium + Math.round(m.premium);
        sumPolicy = sumPolicy + Math.round(m.policies);
        sumAgencyFee = sumAgencyFee + Math.round(m.agencyFee);
      })

      this.AgencyFeeByMonth.Data.push(sumAgencyFee);
      this.AgencyFeeByMonth.Labels.push(this.monthNames[x]);

      this.PremiumByMonth.Data.push(sumPremium);
      this.PremiumByMonth.Labels.push(this.monthNames[x]);

      this.PolicyCountByMonth.Data.push(sumPolicy);
      this.PolicyCountByMonth.Labels.push(this.monthNames[x]);
    }

    this.createAgencyFeeChartData();
    this.createPolicyCountChartData();
    this.createPremiumChartData();
    this.createLineChartChartData();
  }

  createAgencyFeeChartData() {
    this.AgencyFeeByMonthChart = {
      labels: this.AgencyFeeByMonth.Labels,
      datasets: [{
        //backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#DB628B','#F3B353','#F46BFE','#2AB2B7','#657EB8'],
        //borderColor: '#1E88E5',
        data: this.AgencyFeeByMonth.Data,
        borderColor: '#A3A0FB',
        borderCapStyle: 'butt',
        backgroundColor: "#BA59D6",
        //borderColor: '#1E88E5',
        fill: true,
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `Agency Fee - ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
            }
          }
        },
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }]
    }
  }

  createPremiumChartData() {
    this.PremiumByMonthChart = {
      labels: this.PremiumByMonth.Labels,
      datasets: [{
        data: this.PremiumByMonth.Data,
        borderColor: '#A3A0FB',
        borderCapStyle: 'butt',
        backgroundColor: "#BA59D6",
        fill: true,
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          },
          
        }
      }],
    }
  }

  createPolicyCountChartData() {
    this.PolicyCountByMonthChart = {
      labels: this.PolicyCountByMonth.Labels,
      datasets: [
        {
        //backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#DB628B', '#F3B353', '#F46BFE', '#2AB2B7', '#657EB8'],
        data: this.PolicyCountByMonth.Data,
        borderColor: '#A3A0FB',
        borderCapStyle: 'butt',
        backgroundColor: "#BA59D6",
        //borderColor: '#1E88E5',
        fill: true,
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `Policy count - ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
            }
          }
        },
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
        }
      ]
    }
  }



  createLineChartChartData() {
    this.LinechartMonthChart = {
      labels: this.PolicyCountByMonth.Labels,
      datasets: [
        {
        label: 'Agency Fee',
        data: this.AgencyFeeByMonth.Data,
        fill: true,
        borderColor: '#A3A0FB',
        borderCapStyle: 'butt',
        backgroundColor: "#A3A0FB",
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `Agency Fee - ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
            }
          }
        },
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }


      }, {
        label: 'Premium',
        data: this.PremiumByMonth.Data,
        fill: true,
        borderColor: '#BA59D6',
        borderCapStyle: 'butt',
        backgroundColor: "#BA59D6",
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `Premium - ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
            }
          }
        },
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }

      }, {
        label: 'Policy Count',
        data: this.PolicyCountByMonth.Data,
        fill: true,
        borderColor: '#FF8373',
        borderCapStyle: 'butt',
        backgroundColor: "#FF8373",
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `Policy Count - ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
            }
          }
        },
        datalabels: {
          align: 'end',
          anchor: 'end',
          color: 'white',
          font: {
            weight: 'bold'
          },
          formatter: function (value, context) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }


      }]
    }
  }

}
