import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { AISComponent } from './ais.component';



const routes: Routes = [
  { path: 'AIS', component: AISComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AISRoutingModule { }
