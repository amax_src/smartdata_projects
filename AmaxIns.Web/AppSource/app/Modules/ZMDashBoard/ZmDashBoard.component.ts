import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import * as _ from 'underscore';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';


@Component({
  selector: 'app-ZmDashBoard',
  templateUrl: './ZmDashBoard.component.html',
  styleUrls: ['./ZmDashBoard.component.scss']
})

export class ZmDashBoardComponent implements OnInit, AfterViewChecked {

  showLoader: boolean = false;


  location: Array<Location> = new Array<Location>();
  PageName: string = "ZmDashBoard";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  selectedlocation: Array<number> = new Array<number>();
  setLocation: Array<number> = new Array<number>();
  selectedRegionalManager: [] = [];
  selectedagent: [] = [];
 
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private ApiLoad: ApiLoadTimeService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }
  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.selectedagent = [];
    this.GetData();
  }

  GetAllLocations(e) {
    this.setLocation = [];
    this.selectedlocation = [];
    this.location = e;
    this.selectedagent = [];
    if (this.ischartDataLoaded) {
      this.filterData();
    }

    this.selectedlocation.push(this.location[0].agencyId);
    this.setLocation.push(this.location[0].agencyId);
    this.GetData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  GetData() {
    //this.showLoader = true;
    //let startTime: number = new Date().getTime();
    //this.eprService.getEPRData(this.selectedmonth, this.selectedlocation).subscribe(m => {
    //  let model: ApiLoadModel = new ApiLoadModel();
    //  let EndTime: number = new Date().getTime();
    //  let ResponseTime: number = EndTime - startTime;
    //  model.page = "EPR Data"
    //  model.time = ResponseTime
    //  this.EprData = m;
    //  //this.eprDataFilter = this.EprData;
    //  this.ischartDataLoaded = true;
    //  //this.getMonth();
    //  this.showLoader = false;
    //  this.getDate();
    //  this.filterData();
    //  this.setPage(1);
    //  this.ApiLoad.SaveApiTime(model).subscribe();
    //});
  }


  filterData() {
    
  }

}
