import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { ZmDashBoardComponent } from './ZmDashBoard.component';


const routes: Routes = [
  { path: 'ZmDashboard', component: ZmDashBoardComponent}//, canActivate: [LoginGuard], 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ZmDashBoardComponentRoutingModule { }
