import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Ais } from '../../model/Ais';
import { Location } from '../../model/location';
import { AISService } from '../Services/aisService';
import { DateService } from '../Services/dateService';
import { AisChartData } from '../../model/aisChartData';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { authenticationService } from '../../login/authenticationService.service';
import { getMatIconFailedToSanitizeLiteralError } from '@angular/material';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-yearOnYearRevenue',
  templateUrl: './yearOnYearRevenue.component.html',
  styleUrls: ['./yearOnYearRevenue.component.scss']
})

export class YearOnYearRevenueComponent implements OnInit, AfterViewChecked {
  isDisplayExportButton: boolean = false;
  location: Array<Location> = new Array<Location>();
  PageName: string = "AIS Production";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: [] = [];
  selectedRegionalManager: [] = [];
  AISData: Array<Ais> = new Array<Ais>();
  FilterAISData: Array<Ais> = new Array<Ais>();

  PremiumByMonthChart: any;
  AgencyFeeByMonthChart: any;
  PolicyCountByMonthChart: any;
  LinechartMonthChart: any;

  PremiumByYear: Array<{ Year: string, Premium: AisChartData }>;
  AgencyFeeByYear: Array<{ Year: string, AgencyFee: AisChartData }>;
  PolicyCountByYear: Array<{ Year: string, PolicyCount: AisChartData }>;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<any> = new Array<any>();

  agentSelectItem: SelectItem[] = [];
  selectedagent: [] = [];
  selectedLocationCount: number = 0
  selectedAgentCount: number = 0
  IsFitmePageLoad: boolean= true;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  BackRoundColor: Array<any> = ["#42A5F5", "#FF8373", "#FFDA83"]
  BorderColor: Array<any> = ["#42A5F5", "#FF8373", "#FFDA83"]

  //LinechartOptionsForPolicyCount: any = {
  //  responsive: false,
  //  maintainAspectRatio: false,
  //  layout: {
  //    padding: {
  //      top: 25
  //    }
  //  },
  //  legend: { display: true },
  //  scales: {
  //    yAxes: [{
  //      ticks: {
  //        callback: function (label) {
  //          return Math.round(label / 1000) + 'k';
  //        },
  //        fontColor: "#CCC",
  //      },
  //      scaleLabel: {
  //        display: true,
  //        labelString: '1k = 1000',
  //        fontColor: "#CCC",
  //      }
  //    }],
  //    xAxes: [{
  //      ticks: {
  //        fontColor: "#CCC",
  //      }
  //    }]
  //  }

  //}

  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50,
        top :20
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + Math.round((value / 1000)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'k';
        }
      }
    },
    legend: { display: true },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }

  chartOptionsNoDollar: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50,
        top: 20
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return  Math.round((value / 1000)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + 'k';
        }
      }
    },
    legend: { display: true },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }


  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private aisService: AISService, private dateservice: DateService
    , private ApiLoad: ApiLoadTimeService,private _auth: authenticationService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    if (this._auth.userRole == 'headofdepratment') {
      this.isDisplayExportButton = true;
    }
    this.getYears();
    this.getAISData();

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;

  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;

  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.selectedagent = [];
    this.selectedLocationCount = this.selectedlocation.length;
    //this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = e;
    this.selectedagent = [];
    this.selectedLocationCount = this.location.length;
    //this.filterData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      this.showLoader = e;
    }
  }

  SearchReport() {
  this.showLoader = true;
    if (this.IsFitmePageLoad) {
      this.getAISData();
    }
    else {
      this.filterData();
    }
    
    
  }

  filterReset() {
    this.child.filterReset();
    this.selectedyear = [];
    this.getYears();
    this.IsFitmePageLoad = true;
    this.getAISData();
  }

  getYears() {
    this.dateservice.getYear().subscribe(m => {
      this.years = m;
      let firstRecord: boolean = true;
      this.years.forEach(i => {
        if (firstRecord) {
          this.selectedyear.push(i);
          firstRecord = false;
        }
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })

    });
  }

  getAISData() {
   
    let startTime: number = new Date().getTime();
    this.aisService.getAISData().subscribe(m => {
      this.AISData = m;

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Ais Production"
      model.time = ResponseTime
      
      this.FilterAISData = this.AISData;
      this.getAgents(this.FilterAISData);
      this.filterData();
      this.ischartDataLoaded = true;
      this.showLoader = false
      this.IsFitmePageLoad = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    });
  }

  getAgents(data: Array<Ais>) {
    this.agentSelectItem = []
    let agents: Array<string> = new Array<string>();
    agents = Array.from(new Set(data.map(item => item.agentName)));
    if (this.selectedagent && this.selectedagent.length > 0) {
      this.selectedAgentCount = this.selectedagent.length;
    }
    else {
      this.selectedAgentCount = agents.length;
    }

    agents.forEach(i => {
      let x = this.agentSelectItem.filter(m => m.value === i)
      if (x.length === 0) {
        this.agentSelectItem.push(
          {
            label: i, value: i
          })
      }
    })
  }

  onYearchange(e) {
    //this.filterData();
  }

  onAgentChange() {
    //this.filterData();
    this.selectedAgentCount = this.selectedagent.length;
  }

  filterData() {
    this.PremiumByYear = new Array<{ Year: string, Premium: AisChartData }>();
    this.AgencyFeeByYear = new Array<{ Year: string, AgencyFee: AisChartData }>();
    this.PolicyCountByYear = new Array<{ Year: string, PolicyCount: AisChartData }>();

    this.selectedyear.forEach(z => {
      this.FilterAISData = this.AISData.filter(m => m.year === z);
      let FilterAISDatabyLocation: Array<Ais> = new Array<Ais>();
      let FilterAISDatabyAgent: Array<Ais> = new Array<Ais>();
      if (this.selectedlocation && this.selectedlocation.length > 0) {
        this.selectedlocation.forEach(m => {
          let tempAISData: Array<Ais> = new Array<Ais>();
          tempAISData = this.FilterAISData.filter(x => x.agencyId === m)
          tempAISData.forEach(a => {
            FilterAISDatabyLocation.push(a);
          })
        })
      }
      else {
        this.location.forEach(m => {
          let tempAISData: Array<Ais> = new Array<Ais>();
          tempAISData = this.FilterAISData.filter(x => x.agencyId === m.agencyId)
          tempAISData.forEach(a => {
            FilterAISDatabyLocation.push(a);
          })
        })
      }

      this.getAgents(FilterAISDatabyLocation);
      this.FilterAISData = FilterAISDatabyLocation;

      if (this.selectedagent && this.selectedagent.length > 0) {
        this.selectedagent.forEach(m => {
          let tempAISData: Array<Ais> = new Array<Ais>();
          tempAISData = this.FilterAISData.filter(x => x.agentName === m)
          tempAISData.forEach(a => {
            FilterAISDatabyAgent.push(a);
          })
        })
      }

      if (FilterAISDatabyAgent && FilterAISDatabyAgent.length > 0) {
        this.FilterAISData = FilterAISDatabyAgent;
      }

      this.prepareConsolidatedData(z);
    })

    this.CreateFinalChatData();
    this.showLoader = false;
  }


  prepareConsolidatedData(e: string) {
    let AgencyFeeByMonth: AisChartData = new AisChartData();
    let PremiumByMonth: AisChartData = new AisChartData();
    let PolicyCountByMonth: AisChartData = new AisChartData();

    for (var x = 0; x < this.monthNames.length; x++) {
      let tempAISData: Array<Ais> = new Array<Ais>();
      tempAISData = this.FilterAISData.filter(m => m.month === this.monthNames[x])
      let sumPremium: number = 0;
      let sumPolicy: number = 0;
      let sumAgencyFee: number = 0;
      tempAISData.forEach(m => {
        sumPremium = sumPremium + Math.round(m.premium);
        sumPolicy = sumPolicy + Math.round(m.policies);
        sumAgencyFee = sumAgencyFee + Math.round(m.agencyFee);
      })

      AgencyFeeByMonth.Data.push(sumAgencyFee);
      AgencyFeeByMonth.Labels.push(this.monthNames[x]);

      PremiumByMonth.Data.push(sumPremium);
      PremiumByMonth.Labels.push(this.monthNames[x]);

      PolicyCountByMonth.Data.push(sumPolicy);
      PolicyCountByMonth.Labels.push(this.monthNames[x]);
    }

    this.AgencyFeeByYear.push({ Year: e, AgencyFee: AgencyFeeByMonth });
    this.PremiumByYear.push({ Year: e, Premium: PremiumByMonth });
    this.PolicyCountByYear.push({ Year: e, PolicyCount: PolicyCountByMonth });
  }

  CreateFinalChatData() {
    this.createAgencyFeeChartData();
    this.createPolicyCountChartData();
    this.createPremiumChartData();
  }

  createAgencyFeeChartData() {
    let year: Array<any> = new Array<any>();
    let Amount: Array<any> = new Array<any>();
    let DataSet: Array<any> = new Array<any>()
    this.AgencyFeeByYear.forEach((e, i) => {
      DataSet.push(
        {
          label: e.Year,
          backgroundColor: this.BackRoundColor[i],
          borderColor: this.BorderColor[i],
          data: e.AgencyFee.Data
        })
    })

    this.AgencyFeeByMonthChart = {
      labels: this.monthNames,
      datasets: DataSet
    }
  }

  createPremiumChartData() {
    let year: Array<any> = new Array<any>();
    let Amount: Array<any> = new Array<any>();
    let DataSet: Array<any> = new Array<any>()
    this.PremiumByYear.forEach((e, i) => {
      DataSet.push(
        {
          label: e.Year,
          backgroundColor: this.BackRoundColor[i],
          borderColor: this.BorderColor[i],
          data: e.Premium.Data
        })
    })

    this.PremiumByMonthChart = {
      labels: this.monthNames,
      datasets: DataSet
    }
  }

  createPolicyCountChartData() {
    let year: Array<any> = new Array<any>();
    let Amount: Array<any> = new Array<any>();
    let DataSet: Array<any> = new Array<any>()
    this.PolicyCountByYear.forEach((e, i) => {
      DataSet.push(
        {
          label: e.Year,
          backgroundColor: this.BackRoundColor[i],
          borderColor: this.BorderColor[i],
          data: e.PolicyCount.Data
        })
    })

    this.PolicyCountByMonthChart = {
      labels: this.monthNames,
      datasets: DataSet
    }
  }

  GetAISDataCSV() {
    this.showLoader = true;
    this.aisService.GetAISDataCSV().subscribe(
      response => {
        this.downLoadFile(response, "text/csv;charset=utf-8;")
      },
      error => { this.showLoader = false;}
    );
  }
  
  downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    navigator.userAgent.indexOf('Chrome') == -1;
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", "AISReport.csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
    this.showLoader = false;
  }

}
