import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { YearOnYearRevenueRoutingModule } from './yearOnYearRevenue-routing.module';
import { YearOnYearRevenueComponent } from './yearOnYearRevenue.component';




@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    YearOnYearRevenueRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],

  declarations: [YearOnYearRevenueComponent],
  exports: []
 })
export class YearOnYearRevenueModule { }
