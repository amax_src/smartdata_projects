import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { YearOnYearRevenueComponent } from './yearOnYearRevenue.component';




const routes: Routes = [
  { path: 'YearOnYearRevenue', component: YearOnYearRevenueComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class YearOnYearRevenueRoutingModule { }
