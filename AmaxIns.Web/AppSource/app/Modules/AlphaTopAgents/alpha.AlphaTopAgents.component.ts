import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';

import * as _ from 'underscore';
import { SelectItem } from 'primeng/api';

import { CustomerInfo } from '../../model/custmoreInfo';
import { TopAgentService } from '../Services/topagentService';
import { Location } from '../../model/Location';
import { DateService } from '../Services/dateService';
import { TopFiveAgentUserModel } from '../../model/TopFiveAgentUserModel';


@Component({
  selector: 'app-alpha-AlphaTopAgents',
  templateUrl: './alpha.AlphaTopAgents.component.html',
  styleUrls: ['./alpha.AlphaTopAgents.component.scss']
})

export class AlphaTopAgentsComponent implements OnInit, AfterViewChecked {
  PageName: string = "Top 5 Agents";

  pager: any = {};
  pagedItems: any[] = [];

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  Customer_Info: Array<CustomerInfo> = new Array<CustomerInfo>();
  selectedIndex: number = 0
  years: Array<number> = new Array<number>();
  yearsSelectItem: SelectItem[] = [];
  selectedyear: Array<number> = new Array<number>();

  month: Array<string> = new Array<string>();
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<string> = new Array<string>();;

  showLoader: boolean = true;

  Agencies: Array<Location>;
  AgencySelectItem: SelectItem[] = [];
  selectedAgency: Array<number> = new Array<number>();
  UserModel: TopFiveAgentUserModel;
  PremiumChartData: any;
  AgencyChartData: any;
  PolicyCountChartData: any;
  ApiData: Array<{ agentName: string, amount: number }>;

  chartOptionsWithouDoller: any = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }

  chartOptions: any = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }
  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private _service: TopAgentService, private _DateService: DateService) {
    this.ApiData = new Array<{ agentName: string, amount: number }>();

    this.UserModel = new TopFiveAgentUserModel();
    this.UserModel.agencyId = new Array<number>();
    this.UserModel.year = new Array<number>();
    this.UserModel.month = new Array<string>();
    this.GetAgencyName();
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  onYearchange() {
    this.GetMonth();
  }

  onAgencyChange() {
    this.filterData();
  }

  onMonthchange() {
    this.filterData();
  }

  GetAgencyName() {

    this._service.GetAgency().subscribe(m => {
      this.Agencies = m;
      this.Agencies.forEach(z => {
        this.AgencySelectItem.push({
          label: z.agencyName, value: z.agencyId
        })
      })
    })
    this.GetYear();
  }

  GetYear() {
    this._service.GetYear().subscribe(m => {
      this.years = m;
      this.years.forEach(z => {
        this.yearsSelectItem.push({
          label: z.toString(), value: z
        })
      })
    })
    this.GetMonth()
  }

  GetMonth() {
    this.monthSelectItem = [];
    let year: string = (new Date().getFullYear() - 1).toString();
    if (this.selectedyear && this.selectedyear.length === 1) {
      this.selectedyear[0] === new Date().getFullYear();
      year = this.selectedyear[0].toString();
    }
    this._DateService.getMonth(year).subscribe(m => {
      this.month = m;
      this.month = this.month.reverse();
      this.month.forEach(z => {
        this.monthSelectItem.push({
          label: z.toString(), value: z
        })
      })

      this.filterData();
    })
  }

  filterData() {
    if (this.selectedIndex === 0) {
      this.getPremium();
    }
    else if (this.selectedIndex === 1) {
      this.getAgencyFee();
    }
    else if (this.selectedIndex === 2) {
      this.getPolicyCount();
    }
  }

  getPremium() {
    this.UserModel = new TopFiveAgentUserModel();
    this.UserModel.agencyId = new Array<number>();
    this.UserModel.year = new Array<number>();
    this.UserModel.month = new Array<string>();
    this.showLoader = true;
    if (this.selectedAgency && this.selectedAgency.length > 0) {
      this.UserModel.agencyId = this.selectedAgency;
    }

    if (this.selectedmonth && this.selectedmonth.length > 0) {
      this.UserModel.month = this.selectedmonth;
    }

    if (this.selectedyear && this.selectedyear.length > 0) {
      this.UserModel.year = this.selectedyear
    }

    this._service.GetPremium(this.UserModel).subscribe(m => {
      let Name: Array<string> = new Array<string>();
      let Amount: Array<number> = new Array<number>();
      this.showLoader = false;
      this.ApiData = m;
      this.ApiData.forEach(m => {
        Name.push(m.agentName);
        Amount.push(m.amount);
      })

      this.CreatePremiumChartData(Name, Amount)
    })
  }


  getAgencyFee() {
    this.UserModel = new TopFiveAgentUserModel();
    this.UserModel.agencyId = new Array<number>();
    this.UserModel.year = new Array<number>();
    this.UserModel.month = new Array<string>();
    this.showLoader = true;
    if (this.selectedAgency && this.selectedAgency.length > 0) {
      this.UserModel.agencyId = this.selectedAgency;
    }

    if (this.selectedmonth && this.selectedmonth.length > 0) {
      this.UserModel.month = this.selectedmonth;
    }

    if (this.selectedyear && this.selectedyear.length > 0) {
      this.UserModel.year = this.selectedyear
    }

    this._service.GetAgencyFee(this.UserModel).subscribe(m => {
      let Name: Array<string> = new Array<string>();
      let Amount: Array<number> = new Array<number>();
      this.showLoader = false;
      this.ApiData = m;
      this.ApiData.forEach(m => {
        Name.push(m.agentName);
        Amount.push(m.amount);
      })

      this.CreateAgencyChartData(Name, Amount)
    })
  }

  getPolicyCount() {
    this.UserModel = new TopFiveAgentUserModel();
    this.UserModel.agencyId = new Array<number>();
    this.UserModel.year = new Array<number>();
    this.UserModel.month = new Array<string>();
    this.showLoader = true;
    if (this.selectedAgency && this.selectedAgency.length > 0) {
      this.UserModel.agencyId = this.selectedAgency;
    }

    if (this.selectedmonth && this.selectedmonth.length > 0) {
      this.UserModel.month = this.selectedmonth;
    }

    if (this.selectedyear && this.selectedyear.length > 0) {
      this.UserModel.year = this.selectedyear
    }

    this._service.GetPolicyCount(this.UserModel).subscribe(m => {
      let Name: Array<string> = new Array<string>();
      let Amount: Array<number> = new Array<number>();
      this.showLoader = false;
      this.ApiData = m;
      this.ApiData.forEach(m => {
        Name.push(m.agentName);
        Amount.push(m.amount);
      })

      this.CreatePolicyCountChartData(Name, Amount)
    })
  }


  ontabchange(e) {
    this.selectedIndex = e;
    this.filterData();
  }

  CreatePremiumChartData(name: Array<string>, amount: Array<number>) {
    this.PremiumChartData = {
      labels: name,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373'],
          borderColor: '#1E88E5',
          data: amount
        }
      ]
    }
  }

  CreateAgencyChartData(name: Array<string>, amount: Array<number>) {
    this.AgencyChartData = {
      labels: name,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373'],
          borderColor: '#1E88E5',
          data: amount
        }
      ]
    }
  }

  CreatePolicyCountChartData(name: Array<string>, amount: Array<number>) {
    this.PolicyCountChartData = {
      labels: name,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373', '#FF8373'],
          borderColor: '#1E88E5',
          data: amount
        }
      ]
    }
  }

}
