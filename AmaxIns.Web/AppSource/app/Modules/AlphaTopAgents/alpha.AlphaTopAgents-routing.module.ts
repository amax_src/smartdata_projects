import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AlphaLoginGuard } from '../../login/alpha-login-guard.service';
import { AlphaTopAgentsComponent } from './alpha.AlphaTopAgents.component';


const routes: Routes = [
  { path: 'TopAgent', component: AlphaTopAgentsComponent, canActivate: [AlphaLoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AlphaTopAgentsRoutingModule { }
