import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AlphaTopAgentsComponent } from './alpha.AlphaTopAgents.component';
import { AlphaTopAgentsRoutingModule } from './alpha.AlphaTopAgents-routing.module';


@NgModule({
  imports: [SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    AlphaTopAgentsRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule,
    AutoCompleteModule,
  ],
  declarations: [AlphaTopAgentsComponent],
  exports: []
 })
export class AlphaTopAgentsModule { }
