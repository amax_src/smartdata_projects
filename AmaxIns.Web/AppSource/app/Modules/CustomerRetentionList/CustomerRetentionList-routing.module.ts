import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CustomerRetentionListComponent } from './CustomerRetentionList.component';


const routes: Routes = [
  { path: 'CustomerRetentionList', component: CustomerRetentionListComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomerRetentionListRoutingModule { }
