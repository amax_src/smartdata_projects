import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, ActivatedRoute } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { Location } from '../../model/location';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { DateService } from '../Services/dateService';

import { LazyLoadEvent, MessageService, ConfirmationService, SelectItem } from 'primeng/api';
import { Table } from "primeng/components/table/table";

import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { authenticationService } from '../../login/authenticationService.service';

//import { ExportFileService } from '../Services/ExportFileService';
import { DownloadexcelService } from "./../Services/downloadexcel.service";
import { DatePipe } from '@angular/common';

import { AgencyService } from '../Services/agencyService';
import { Agency } from '../../model/Agency/Agency';

import { CustomerRetentionService } from '../Services/CustomerRetentionService';
import { CustomerRetention } from '../../model/CustomerRetention/CustomerRetention';
import { DueDateFilterTypeEnum } from '../../model/CustomerRetention/DueDateFilterTypeEnum';


@Component({
  selector: 'app-CustomerRetentionList',
  templateUrl: './CustomerRetentionList.component.html',
  styleUrls: ['./CustomerRetentionList.component.scss']
})
export class CustomerRetentionListComponent implements OnInit, AfterViewChecked {

  PageName: string = "Customer Retention";
  
  showLoader: boolean = false;

  //@ViewChild(Table) dt: Table;
  @ViewChild("dt") dt: Table;

  ColumnList: any[];  

  //CustomerRetentionList: Array<CustomerRetention> = new Array<CustomerRetention>();
  CustomerRetentionList: CustomerRetention[];
  totalRecordCount: number = 0;
  loadingTable: boolean = false;

  displayDialog: boolean = false;

  DueDateFilterTypeSelectItemList: SelectItem[] = [];
  
  CustomerRetentionList2: CustomerRetention[];
  totalRecordCount2: number = 0;  
  loadingTable2: boolean = false;

  CustomerRetentionList3: CustomerRetention[];
  totalRecordCount3: number = 0;  
  loadingTable3: boolean = false;

  CustomerRetentionList4: CustomerRetention[];
  totalRecordCount4: number = 0;  
  loadingTable4: boolean = false;

  years: Array<any> = Array<any>();
  yearSelectItemList: SelectItem[] = [];
  selectedYear: string = "";

  month: Array<any>;
  monthSelectItemList: SelectItem[] = [];
  selectedMonth: string = "";
  selectedMonthIndex: number = 0;

  date: Array<any>;
  dateSelectItemList: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  isMonthChanged: boolean = true;

  selectedDueDate: Date;

  agencyName: string = "";

  // location: Array<Location>;
  // locationSelectItem: SelectItem[] = [];
  
  currentStoreManagerUserId: number = 0;

  showSwitchButtonToZoneManagerView: boolean = false;

  showSwitchButtonToSummarizedView: boolean = true;

  currentAgencyId: number = 0;

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private _pageservice: PageService, private dateService: DateService,
    private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService,
    private readonly _downloadExcel: DownloadexcelService, public datepipe: DatePipe,
    private messageService: MessageService, private confirmationService: ConfirmationService,
    private route: ActivatedRoute,
    private agencyService: AgencyService,
    private customerRetentionService: CustomerRetentionService
  ) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {

    //this.getYears();

    //this.loadDueDateFilterType();

    this.selectedDueDate = this.getToday();

    if (this.authenticationService.userRole === 'storemanager') {
      this.selectedDueDate = this.getToday();
      this.currentStoreManagerUserId = this.authenticationService.userId;
      this.getStoreManagerAgency(this.currentStoreManagerUserId);

    }
    else if(this.authenticationService.userRole === 'zonalmanager'){
      this.selectedDueDate = this.getToday();
      let queryParamAgencyId = this.route.snapshot.queryParamMap.get('agencyId');
      if(queryParamAgencyId != null && queryParamAgencyId != undefined && queryParamAgencyId != ''){
        this.currentAgencyId = Number(queryParamAgencyId);
        this.showSwitchButtonToZoneManagerView = true;
        this.showSwitchButtonToSummarizedView = false;

        let zoneManagerIdList = new Array<number>();
        zoneManagerIdList.push(this.authenticationService.userId);
        
        if ((zoneManagerIdList && zoneManagerIdList.length > 0)) {          
          this.agencyService.getAllLocationsRegionalZonalManager(null, zoneManagerIdList).subscribe(m => {
            m.forEach(i => {                
                if(i.agencyId == this.currentAgencyId){
                  // this.location = [];
                  // this.location.push(i);
                  // this.agencyName = this.location[0].agencyName;
                  this.agencyName = i.agencyName;
                }
            });            
          });
        }

      }
      
    }

    //this.showLoader = true;

    this.loadingTable = true;
    this.loadingTable2 = true;
    this.loadingTable3 = true;
    this.loadingTable4 = true;
    
    this.ColumnList = [
      { field: 'dueDate', header: 'Due Date', isDate:true, format: 'MM/dd/yyyy', width:'110px' },
      { field: 'clientName', header: 'Client Name', width:'280px' },
      { field: 'policyNumber', header: 'Policy #' , width:'200px' },
      { field: 'policyStatusName', header: 'Policy Status' , width:'80px' },
      { field: 'companyName', header: 'Company Name' , width:'150px' },
      { field: 'policyTypeName', header: 'Policy Type', width:'80px' },
      { field: 'effDate', header: 'Eff Date', isDate:true, format: 'MM/dd/yyyy', width:'110px' },
      { field: 'expDate', header: 'Exp Date', isDate:true, format: 'MM/dd/yyyy', width:'110px' },
      { field: 'amountDue', header: 'Amount Due', width:'80px' },
      { field: 'clientCellPhone', header: 'Client Cell Phone #', width:'110px' },
      { field: 'clientHomePhone', header: 'Client Home Phone #', width:'110px' },
      { field: 'clientWorkPhone', header: 'Client Work Phone #', width:'110px' }      
    ];

    
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });

  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  switchToZoneManagerView(){
    if(this.currentAgencyId > 0){
      this.router.navigateByUrl("/CustomerRetentionCountListForZone?agencyId=" + this.currentAgencyId);
    }
  }

  switchToSummarizedView(){
    this.router.navigateByUrl("/CustomerRetentionCountListForStore");
  }

  onYearChange(e) {
    this.getMonth(this.selectedYear);
    
  }

  onMonthChange(e) {    
    this.isMonthChanged = true;

    //set this.selectmonthindex
    for (let index = 0; index < this.monthNames.length; index++) {
      if(this.monthNames[index] == this.selectedMonth){
        this.selectedMonthIndex = index;
        break;
      };
    }

    this.getDate();
  }

  getToday() {
    let d = new Date();
    d.setDate(d.getUTCDate());
    return d;
  }

  getYesterday() {
    let d = new Date();
    d.setDate(d.getUTCDate() - 1);
    //return d.toISOString().split('T')[0]; // yesterday(); // 2018-10-17 (if current date is 2018-10-18)
    return d;
  };

  getYears() {
    this.dateService.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        if (i > 2019) {
          this.yearSelectItemList.push(
            {
              label: i, value: i
            })
        }
      })

      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedYear = _selectedYear;
      this.selectedMonth = "";
      this.getMonth(this.selectedYear);
    });
  }

  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }

  getMonth(year: string) {
    this.monthSelectItemList = [];
    let months: Array<string> = new Array<string>();
    this.selectedMonthIndex = 0;
    
    months = this.monthNames;
    months.forEach(i => {
      this.monthSelectItemList.push(
        {
          label: i, value: i
        })
    });

    let monthcount = (new Date()).getMonth();
    this.selectedMonthIndex = monthcount - 1;
    for (var x = monthcount; x >= 0; x--) {
      let checkmonth = months.filter(m => m === this.monthNames[x])
      if (checkmonth && checkmonth.length > 0) {
        this.selectedMonthIndex = x;
        break;
      }
    }

    let _currentyear: string = (new Date()).getFullYear().toString()
    if (year === _currentyear) {
      this.selectedMonth = this.monthNames[this.selectedMonthIndex];
    }
    else {
      this.selectedMonth = this.monthNames[11];
    }

    this.getDate();

  }

  getDate() {
    console.log('Date Method');
    this.selectedDate = [];
    this.dateSelectItemList = [];
    let dt: Array<Date> = new Array<Date>();
    let lastDayNumber = this.getLastDayOfMonth(Number(this.selectedYear), this.selectedMonthIndex);
    for (let i = 1; i <= lastDayNumber; i++) {
      let dateValue = new Date(Number(this.selectedYear), this.selectedMonthIndex, i);
      this.dateSelectItemList.push(
          {
            label: this.getDateString(this.datepipe.transform(dateValue,'MM/dd/yyyy')), value: dateValue
          });
    }

  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    //this.TabChange();
  }
  
  getLastDayOfMonth(year: number, month:number){
    return new Date(year, month + 1, 0).getDate();
  }

  loadDueDateFilterType() {
    this.DueDateFilterTypeSelectItemList = [];
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Today', value: '1'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Due Next 7 Days', value: '2'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Past Due', value: '3'
      });
    this.DueDateFilterTypeSelectItemList.push(
      {
        label: 'Canceled', value: '4'
      });
  }

  getStoreManagerAgency(Id: number) {
    this.agencyService.getStoreManagerAgency(Id).subscribe(m => {
      this.currentAgencyId = m[0].agencyId;
      this.agencyName = m[0].agencyName;
      
    });
  }

  Search() {
    var queryModel: any;
    queryModel = {
      RowOffset: 0,
      PageSize: 10,
      SortBy: "policyNumber",
      SortDir: 'ASC',
      StoreManagerUserId: this.currentStoreManagerUserId,
      AgencyId: this.currentAgencyId,
      DueDateFilterTypeId: 0
    };
    this.loadingTable = true;
    this.loadingTable2 = true;
    this.loadingTable3 = true;

    this.loadCustomerRetentionListSearch(queryModel);
    this.loadCustomerRetentionList2Search(queryModel);
    this.loadCustomerRetentionList3Search(queryModel);
  }

  loadCustomerRetentionListSearch(queryModel: any) {
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;
      console.log("queryModel", queryModel);
      this.CustomerRetentionList = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList = res.data;
        console.log("this.CustomerRetentionCountList", this.CustomerRetentionList);
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });
    }, 500);

  }

  loadCustomerRetentionList2Search(queryModel: any) {
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;
      this.CustomerRetentionList2 = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });
    }, 500);
  }

  loadCustomerRetentionList3Search(queryModel: any) {
    setTimeout(() => {
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;
      this.CustomerRetentionList3 = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });
    }, 1000);
  }


  loadCustomerRetentionList(event: LazyLoadEvent) {
    this.loadingTable = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "policyNumber" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        StoreManagerUserId: this.currentStoreManagerUserId,
        AgencyId: this.currentAgencyId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueToday
      };
      this.CustomerRetentionList = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList = res.data;
        this.totalRecordCount = res.totalRecordCount;
        this.loadingTable = false;
      });
    }, 1000);
  }
 
  loadCustomerRetentionList2(event: LazyLoadEvent) {
    this.loadingTable2 = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "policyNumber" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        StoreManagerUserId: this.currentStoreManagerUserId,
        AgencyId: this.currentAgencyId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.DueNext7Days
      };
      this.CustomerRetentionList2 = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList2 = res.data;
        this.totalRecordCount2 = res.totalRecordCount;
        this.loadingTable2 = false;
      });

    }, 1000);

  }

  loadCustomerRetentionList3(event: LazyLoadEvent) {
    this.loadingTable3 = true;
    setTimeout(() => {
      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "policyNumber" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        StoreManagerUserId: this.currentStoreManagerUserId,
        AgencyId: this.currentAgencyId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.PastDue
      };

      this.CustomerRetentionList3 = new Array<CustomerRetention>();
      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList3 = res.data;
        this.totalRecordCount3 = res.totalRecordCount;
        this.loadingTable3 = false;
      });
    }, 1000);
  }

  loadCustomerRetentionList4(event: LazyLoadEvent) {
    this.loadingTable4 = true;

    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    // https://www.primefaces.org/primeng/v7.2.6-lts/#/table/lazy
    // https://github.com/primefaces/primeng/blob/master/src/app/showcase/components/table/tablelazydemo.ts

    setTimeout(() => {

      //this.CustomerRetentionService.getAll({ lazyEvent: JSON.stringify(event) }).then(res => {
      //  this.CustomerRetentionList = res.data;
      //  this.totalRecordCount = res.totalRecordCount;
      //  this.loadingTable = false;
      //});

      var queryModel = {
        RowOffset: event.first,
        PageSize: event.rows,
        SortBy: event.sortField == null ? "policyNumber" : event.sortField,
        SortDir: event.sortOrder == 1 ? 'ASC' : 'DESC',
        StoreManagerUserId: this.currentStoreManagerUserId,
        AgencyId: this.currentAgencyId,
        DueDateFilterTypeId: DueDateFilterTypeEnum.Canceled
      };

      this.customerRetentionService.getAllPaging(queryModel).then(res => {
        this.CustomerRetentionList4 = res.data;
        this.totalRecordCount4 = res.totalRecordCount;
        this.loadingTable4 = false;
      });

    }, 500);

  }

  public exportToExcel(excelType) {
    this.showLoader = true;
    var queryModel = {
      RowOffset: 0,
      PageSize: 1000000,
      SortBy: "policyNumber",
      SortDir: 'ASC',
      StoreManagerUserId: this.currentStoreManagerUserId,
      AgencyId: this.currentAgencyId,
      DueDateFilterTypeId: 0
    };

    if(excelType == 1){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueToday;
    }
    else if(excelType == 2){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.DueNext7Days;
    }
    else if(excelType == 3){
      queryModel.DueDateFilterTypeId = DueDateFilterTypeEnum.PastDue;
    }

    this.customerRetentionService.getAllPagingExport(queryModel).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      this.showLoader = false;
    })

  }

  public exportDataToExcel(excelType, dataList) {
    
    // let reportHeader = { 
    //   dueDate:"Due Date", clientName:"Client Name", 
    //   policyNumber:"Policy #", policyStatusName:"Policy Status", 
    //   companyName:"Company Name", policyTypeName:"Policy Type", 
    //   effDate:"Eff Date", expDate:"Exp Date", 
    //   amountDue:"Amount Due", clientCellPhone:"Client Cell Phone #", 
    //   clientHomePhone:"Client Home Phone #", clientWorkPhone:"Client Work Phone #" 
    // };

    // let agentReport = this.filteredData.map(agent =>({agentId:agent.agentId,firstName: agent.firstName,lastName:agent.lastName,
    //                                                   email:agent.email, userType: UserTypes[agent.userType],
    //                                                   status:agent.status === 1 ? 'ACTIVE' : 'INACTIVE', 
    //                                                   description:agent.description, userId:agent.userId === null ? 'No' : 'Yes',
    //                                                   createUser:agent.createUser, createDate:this.datepipe.transform(agent.createDate,'MM/dd/yyyy'),
    //                                                   updateUser:agent.updateUser, updateDate:this.datepipe.transform(agent.updateDate, 'MM/dd/yyyy')})
    //                                                 );

    let reportDataList = [];

    let sourceReportDataList = dataList;    

    let mappedDataList = sourceReportDataList.map( item => (
      { 
        dueDate: this.datepipe.transform(item.dueDate,'MM/dd/yyyy'), clientName: item.clientName, 
        policyNumber: item.policyNumber, policyStatusName: item.policyStatusName, 
        companyName: item.companyName, policyTypeName: item.policyTypeName, 
        effDate: this.datepipe.transform(item.effDate,'MM/dd/yyyy'), expDate: this.datepipe.transform(item.expDate,'MM/dd/yyyy'), 
        amountDue: item.amountDue.toString(), clientCellPhone: item.clientCellPhone, 
        clientHomePhone: item.clientHomePhone, clientWorkPhone: item.clientWorkPhone 
      }
    ));

    mappedDataList.map(item => {
      reportDataList.push(item);
    });    

    // reportDataList.unshift(reportHeader);

    // var wscols =[
    //   {wch: 10}, 
    //   {wch: 20}, 
    //   {wch: 20}, 
    //   {wch: 50}, 
    //   {wch: 15},
    //   {wch: 10}, 
    //   {wch: 20},
    //   {wch: 20}
    // ];

    var wscols = [
      { header: 'Due Date', key: 'dueDate', width: 15 },
      { header: 'Client Name', key: 'clientName', width: 20 },
      { header: 'Policy #', key: 'policyNumber', width: 20 },
      { header: 'Policy Status', key: 'policyStatusName', width: 15 },
      { header: 'Company Name', key: 'companyName', width: 30 },
      { header: 'Policy Type', key: 'policyTypeName', width: 15 },
      { header: 'Eff Date', key: 'effDate', width: 15 },
      { header: 'Exp Date', key: 'expDate', width: 15 },
      { header: 'Amount Due', key: 'amountDue', width: 15 },
      { header: 'Client Cell Phone #', key: 'clientCellPhone', width: 20 },
      { header: 'Client Home Phone #', key: 'clientHomePhone', width: 20 },
      { header: 'Client Work Phone #', key: 'clientWorkPhone', width: 20 }
    ];

    if (reportDataList.length > 0) {
        //this.exportService.exportExcel(reportDataList,'Customer Retention For Store', wscols);
        //this.exportService.exportExcelWithColor(reportDataList,'Customer Retention For Store', wscols);
    }

  }


}
