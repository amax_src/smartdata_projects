import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { PBXService } from '../Services/pbxService';
import { SelectItem } from 'primeng/api';
import { PbxModel } from '../../model/PbxModel';

@Component({
  selector: 'app-Pbx',
  templateUrl: './alpha.pbx.component.html',
  styleUrls: ['./alpha.pbx.component.scss']
})

export class AphaPBXComponent implements OnInit, AfterViewChecked {
  PageName: string = "PBX";
  Extentions: Array<string> = new Array<string>();
  ExtentionsSelectItem: SelectItem[] = [];

  SelectedExtentions: Array<string> = new Array<string>();
  Month: Array<string> = new Array<string>();
  MonthSelectItem: SelectItem[] = [];

  SelectedDirection: Array<string> = new Array<string>();
  DirectionSelectItem: SelectItem[] = [];

  SelectedDate: Array<Date> = new Array<Date>();
  dateSelectItem: SelectItem[] = [];

  PbXDailyData: Array<PbxModel> = new Array<PbxModel>();
  PbXDailyFilterData: Array<PbxModel> = new Array<PbxModel>();

  Selectedmonth: string = "";
  showLoader: boolean = false;
  PbXData: Array<PbxModel> = new Array<PbxModel>();
  PbXFilterData: Array<PbxModel> = new Array<PbxModel>();
  SelectedTab: number = 0;
  ExtentionViseMonthlyCountOfcallChardata: Array<any> = new Array<any>();
  ExtentionViseMonthlyTalkTimecallChardata: Array<any> = new Array<any>();
  pager: any = {};
  pagedItems: any[] = [];

  Dailypager: any = {};
  DailypagedItems: any[] = [];

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  MonthytalkTimeChartData: any = [];
  MonthyCountTimeChartData: any = [];
  MonthlyInBoundCalls: number = 0;
  MonthlyOutBoundCalls: number = 0;
  MonthlyTotalTalkTime: number = 0;

  DailytalkTimeChartData: any = [];
  DailyInBoundCalls: number = 0;
  DailyOutBoundCalls: number = 0;
  DailyTotalTalkTime: number = 0;
  DailyCountTimeChartData: any = [];;

  ExtentionViseDailyCountOfcallChardata = new Array<any>();
  ExtentionViseDailyTalkTimecallChardata = new Array<any>()

  chartOptions: any = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, private service: PBXService,
    private _pageservice: PageService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.getExtention();
    this.getDailyData();
    this.DirectionSelectItem.push({ label: "Inbound", value: "Inbound" })
    this.DirectionSelectItem.push({ label: "Outbound", value: "Outbound" })
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  ontabchange(e) {
    this.SelectedTab = e;
    this.filterData();
  }

  onExtentionchange() {
    this.filterData()
  }

  onMonthchange() {
    this.getData();
  }

  onDirectionchange() {
    this.filterData()
  }

  onDatechange() {
    this.filterData();
  }

  filterData() {
    if (this.SelectedTab === 0) {
      this.FilterMonthlySummary();
    }
    else if (this.SelectedTab === 1) {
      this.FilterExtentionWiseChart();
    }
    else if (this.SelectedTab === 2) {
      this.CreateMonthlyDetailData();
    }
    else if (this.SelectedTab === 3) {
      this.FilterDailySummary()
    }
    else if (this.SelectedTab === 4) {
      this.FilterDailyExtentionWiseChart();
    }
    else if (this.SelectedTab === 5) {
      this.CreateDailyDetailData();
    }
  }

  getExtention() {
    this.service.getExtention().subscribe(m => {
      this.Extentions = m;
      this.Extentions.forEach(i => {
        this.ExtentionsSelectItem.push(
          { label: i, value: i }
        )
      })
      this.getMonth();
    });
  }

  getMonth() {
    this.service.getMonth().subscribe(m => {
      this.Month = m;
      let isfirstRecord: Boolean = true
      this.Month.forEach(i => {
        if (isfirstRecord) {
          this.Selectedmonth = i;
          isfirstRecord = false;
        }
        this.MonthSelectItem.push(
          { label: i, value: i }
        )
      })
      this.getData();
    });
  }

  getData() {
    this.showLoader = true;
    this.service.getPBXData(this.Selectedmonth).subscribe(m => {
      this.PbXData = m;
      this.showLoader = false;
      this.filterData();
    });
  }


  getDailyData() {
    this.showLoader = true;
    this.service.getPBXDailyData().subscribe(m => {
      this.PbXDailyData = m;
      this.showLoader = false;
      this.GetDate();
      this.filterData();
    });
  }

  GetDate() {
    let dt: Array<Date> = new Array<Date>();
    dt = Array.from(new Set(this.PbXDailyData.map(item => item.date)));
    let firstRecord: boolean = true;
    this.dateSelectItem = []
    dt.forEach(i => {
      this.dateSelectItem.push(
        {
          label: this.getDateString(i), value: i
        })
    })
  }


  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  FilterMonthlySummary() {
    this.PbXFilterData = this.PbXData;
    let pbxExtectionfilter: Array<PbxModel> = new Array<PbxModel>();

    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(m => {
        let tempdata: Array<PbxModel> = new Array<PbxModel>();
        tempdata = this.PbXFilterData.filter(x => x.extension === m);
        tempdata.forEach(z => {
          pbxExtectionfilter.push(z);
        })
      })
    }
    if (this.SelectedExtentions.length > 0) {
      this.PbXFilterData = pbxExtectionfilter
    }

    this.CreateMonthlySummayChartData();
  }


  FilterExtentionWiseChart() {
    this.ExtentionViseMonthlyCountOfcallChardata = new Array<any>();
    this.ExtentionViseMonthlyTalkTimecallChardata = new Array<any>()
    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(i => this.CreateExtentionWiseChart(i))
    }
    else {
      this.Extentions.forEach(i => this.CreateExtentionWiseChart(i))
    }
  }


  CreateExtentionWiseChart(m) {
    let inboundTalkTime: number = 0;
    let outboundTalkTime: number = 0;
    let inboundCount: number = 0;
    let outboundCount: number = 0;
    let FilterByExtention: Array<PbxModel> = new Array<PbxModel>();
    let InboundCalls: Array<PbxModel> = new Array<PbxModel>();
    let OutboundCalls: Array<PbxModel> = new Array<PbxModel>();
    FilterByExtention = this.PbXData.filter(x => x.extension === m)
    InboundCalls = FilterByExtention.filter(x => x.direction === 'Inbound')
    OutboundCalls = FilterByExtention.filter(x => x.direction === 'Outbound')
    outboundCount = OutboundCalls.length;
    inboundCount = InboundCalls.length;

    InboundCalls.forEach(m => {
      inboundTalkTime = inboundTalkTime + m.talkTimeMinutes;
    })

    OutboundCalls.forEach(m => {
      outboundTalkTime = outboundTalkTime + m.talkTimeMinutes;
    })
    let Data = {
      labels: ['Inbound', 'OutBound'],
      ChatName: 'Extension ' + m,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundCount), Math.round(outboundCount)]
        }
      ]
    }

    let Data2 = {
      labels: ['Inbound', 'OutBound'],
      ChatName: 'Extension ' + m,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundTalkTime), Math.round(outboundTalkTime)]
        }
      ]
    }
    this.ExtentionViseMonthlyCountOfcallChardata.push(Data);
    this.ExtentionViseMonthlyTalkTimecallChardata.push(Data2);

  }

  CreateMonthlySummayChartData() {
    this.MonthlyInBoundCalls = 0
    this.MonthlyOutBoundCalls = 0
    this.MonthlyTotalTalkTime = 0
    this.MonthytalkTimeChartData = [];
    this.MonthyCountTimeChartData = [];
    let Inbound: Array<PbxModel> = new Array<PbxModel>();
    let Outbound: Array<PbxModel> = new Array<PbxModel>();
    let inboundTalkTime: number = 0
    let outboundTalkTime: number = 0
    let inboundcount: number = 0
    let outboundcount: number = 0
    Inbound = this.PbXFilterData.filter(m => m.direction === 'Inbound')
    Outbound = this.PbXFilterData.filter(m => m.direction === 'Outbound')

    outboundcount = Outbound.length;
    inboundcount = Inbound.length;

    Inbound.forEach(m => {
      inboundTalkTime = inboundTalkTime + m.talkTimeMinutes;
    })

    Outbound.forEach(m => {
      outboundTalkTime = outboundTalkTime + m.talkTimeMinutes;
    })

    this.MonthytalkTimeChartData = {
      labels: ['InBound', 'OutBound'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundTalkTime), Math.round(outboundTalkTime)]
        }
      ]
    }

    this.MonthlyInBoundCalls = inboundcount;
    this.MonthlyOutBoundCalls = outboundcount;
    this.MonthlyTotalTalkTime = Math.round(inboundTalkTime + outboundTalkTime);
    this.MonthyCountTimeChartData = {
      labels: ['InBound', 'OutBound'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundcount), Math.round(outboundcount)]
        }
      ]
    }

  }



  CreateMonthlyDetailData() {
    this.pager = {};
    this.pagedItems = [];

    this.PbXFilterData = this.PbXData;
    let FilterByExtention: Array<PbxModel> = new Array<PbxModel>();
    let FilterByDirection: Array<PbxModel> = new Array<PbxModel>();
    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(i => {
        let temp: Array<PbxModel> = new Array<PbxModel>();
        temp = this.PbXFilterData.filter(m => m.extension === i)
        temp.forEach(a => {
          FilterByExtention.push(a);
        })
      })
    }
    else {
      this.Extentions.forEach(i => {
        let temp: Array<PbxModel> = new Array<PbxModel>();
        temp = this.PbXFilterData.filter(m => m.extension === i)
        temp.forEach(a => {
          FilterByExtention.push(a);
        })
      })
    }
    this.PbXFilterData = FilterByExtention;
    if (this.SelectedDirection && this.SelectedDirection.length > 0) {
      let temp: Array<PbxModel> = new Array<PbxModel>();
      this.SelectedDirection.forEach(i => {
        temp = this.PbXFilterData.filter(m => m.direction === i)
        temp.forEach(a => {
          FilterByDirection.push(a);
        })
      })
    }

    if (this.SelectedDirection && this.SelectedDirection.length > 0) {
      this.PbXFilterData = FilterByDirection;
    }

    this.setPage(1)
  }

  CreateDailyDetailData() {
    this.Dailypager = {};
    this.DailypagedItems = [];

    this.PbXDailyFilterData = this.PbXDailyData;
    let FilterByExtention: Array<PbxModel> = new Array<PbxModel>();
    let FilterByDirection: Array<PbxModel> = new Array<PbxModel>();
    let FilterByDate: Array<PbxModel> = new Array<PbxModel>();
    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(i => {
        let temp: Array<PbxModel> = new Array<PbxModel>();
        temp = this.PbXDailyFilterData.filter(m => m.extension === i)
        temp.forEach(a => {
          FilterByExtention.push(a);
        })
      })
    }
    else {
      this.Extentions.forEach(i => {
        let temp: Array<PbxModel> = new Array<PbxModel>();
        temp = this.PbXDailyFilterData.filter(m => m.extension === i)
        temp.forEach(a => {
          FilterByExtention.push(a);
        })
      })
    }

    this.PbXDailyFilterData = FilterByExtention;
    if (this.SelectedDirection && this.SelectedDirection.length > 0) {
      let temp: Array<PbxModel> = new Array<PbxModel>();
      this.SelectedDirection.forEach(i => {
        temp = this.PbXDailyFilterData.filter(m => m.direction === i)
        temp.forEach(a => {
          FilterByDirection.push(a);
        })
      })
    }

    if (this.SelectedDirection && this.SelectedDirection.length > 0) {
      this.PbXDailyFilterData = FilterByDirection;
    }

    if (this.SelectedDate && this.SelectedDate.length > 0) {
      this.SelectedDate.forEach(m => {
        let tempdata: Array<PbxModel> = new Array<PbxModel>();
        tempdata = this.PbXDailyFilterData.filter(x => x.date === m);
        tempdata.forEach(z => {
          FilterByDate.push(z);
        })
      })
    }
    if (this.SelectedDate && this.SelectedDate.length > 0) {
      this.PbXDailyFilterData = FilterByDate;
    }


    this.setDailyPage(1)
  }
  
  setDailyPage(page: number) {
    if (page < 1 || page > this.Dailypager.totalPages) {
      return;
    }

    // get pager object from service
    this.Dailypager = this._pageservice.getPager(this.PbXDailyFilterData.length, page);

    // get current page of items
    this.DailypagedItems = this.PbXDailyFilterData.slice(this.Dailypager.startIndex, this.Dailypager.endIndex + 1);

  }

  FilterDailySummary() {
    this.PbXDailyFilterData = this.PbXDailyData;
    let pbxExtectionfilter: Array<PbxModel> = new Array<PbxModel>();
    let pbxDatefilter: Array<PbxModel> = new Array<PbxModel>();
    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(m => {
        let tempdata: Array<PbxModel> = new Array<PbxModel>();
        tempdata = this.PbXDailyFilterData.filter(x => x.extension === m);
        tempdata.forEach(z => {
          pbxExtectionfilter.push(z);
        })
      })
    }

    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.PbXDailyFilterData = pbxExtectionfilter
    }

    if (this.SelectedDate && this.SelectedDate.length > 0) {
      this.SelectedDate.forEach(m => {
        let tempdata: Array<PbxModel> = new Array<PbxModel>();
        tempdata = this.PbXDailyFilterData.filter(x => x.date === m);
        tempdata.forEach(z => {
          pbxDatefilter.push(z);
        })
      })
    }
    if (this.SelectedDate && this.SelectedDate.length > 0) {
      this.PbXDailyFilterData = pbxDatefilter;
    }
    this.CreateDailySummayChartData();
  }

  CreateDailySummayChartData() {
    this.MonthlyInBoundCalls = 0
    this.MonthlyOutBoundCalls = 0
    this.MonthlyTotalTalkTime = 0
    this.MonthytalkTimeChartData = [];
    this.MonthyCountTimeChartData = [];
    let Inbound: Array<PbxModel> = new Array<PbxModel>();
    let Outbound: Array<PbxModel> = new Array<PbxModel>();
    let inboundTalkTime: number = 0
    let outboundTalkTime: number = 0
    let inboundcount: number = 0
    let outboundcount: number = 0
    Inbound = this.PbXDailyFilterData.filter(m => m.direction === 'Inbound')
    Outbound = this.PbXDailyFilterData.filter(m => m.direction === 'Outbound')

    outboundcount = Outbound.length;
    inboundcount = Inbound.length;

    Inbound.forEach(m => {
      inboundTalkTime = inboundTalkTime + m.talkTimeMinutes;
    })

    Outbound.forEach(m => {
      outboundTalkTime = outboundTalkTime + m.talkTimeMinutes;
    })

    this.DailytalkTimeChartData = {
      labels: ['InBound', 'OutBound'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundTalkTime), Math.round(outboundTalkTime)]
        }
      ]
    }

    this.DailyInBoundCalls = inboundcount;
    this.DailyOutBoundCalls = outboundcount;
    this.DailyTotalTalkTime = Math.round(inboundTalkTime + outboundTalkTime);
    this.DailyCountTimeChartData = {
      labels: ['InBound', 'OutBound'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundcount), Math.round(outboundcount)]
        }
      ]
    }

  }

  FilterDailyExtentionWiseChart() {
    this.ExtentionViseDailyCountOfcallChardata = new Array<any>();
    this.ExtentionViseDailyTalkTimecallChardata = new Array<any>()
    if (this.SelectedExtentions && this.SelectedExtentions.length > 0) {
      this.SelectedExtentions.forEach(i => this.CreateDailyExtentionWiseChart(i))
    }
    else {
      this.Extentions.forEach(i => this.CreateDailyExtentionWiseChart(i))
    }


  }


  CreateDailyExtentionWiseChart(m) {
    let inboundTalkTime: number = 0;
    let outboundTalkTime: number = 0;
    let inboundCount: number = 0;
    let outboundCount: number = 0;
    let FilterByDate: Array<PbxModel> = new Array<PbxModel>();
    let FilterByExtention: Array<PbxModel> = new Array<PbxModel>();
    let InboundCalls: Array<PbxModel> = new Array<PbxModel>();
    let OutboundCalls: Array<PbxModel> = new Array<PbxModel>();

    if (this.SelectedDate && this.SelectedDate.length > 0) {
      this.SelectedDate.forEach(z => {
        let temp: Array<PbxModel> = new Array<PbxModel>();
        temp = this.PbXDailyData.filter(m => m.date === z)
        temp.forEach(x => {
          FilterByDate.push(x);
        })
      })
    }
    else {
      FilterByDate = this.PbXDailyData;
    }



    FilterByExtention = FilterByDate.filter(x => x.extension === m)
    InboundCalls = FilterByExtention.filter(x => x.direction === 'Inbound')
    OutboundCalls = FilterByExtention.filter(x => x.direction === 'Outbound')
    outboundCount = OutboundCalls.length;
    inboundCount = InboundCalls.length;

    InboundCalls.forEach(m => {
      inboundTalkTime = inboundTalkTime + m.talkTimeMinutes;
    })

    OutboundCalls.forEach(m => {
      outboundTalkTime = outboundTalkTime + m.talkTimeMinutes;
    })
    let Data = {
      labels: ['Inbound', 'OutBound'],
      ChatName: 'Extension ' + m,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundCount), Math.round(outboundCount)]
        }
      ]
    }

    let Data2 = {
      labels: ['Inbound', 'OutBound'],
      ChatName: 'Extension ' + m,
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB'],
          borderColor: '#1E88E5',
          data: [Math.round(inboundTalkTime), Math.round(outboundTalkTime)]
        }
      ]
    }

    this.ExtentionViseDailyCountOfcallChardata.push(Data);
    this.ExtentionViseDailyTalkTimecallChardata.push(Data2);

  }




  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this._pageservice.getPager(this.PbXFilterData.length, page);

    // get current page of items
    this.pagedItems = this.PbXFilterData.slice(this.pager.startIndex, this.pager.endIndex + 1);

  }



}
