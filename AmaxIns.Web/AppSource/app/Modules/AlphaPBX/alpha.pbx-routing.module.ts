import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AphaPBXComponent } from './alpha.pbx.component';
import { AlphaLoginGuard } from '../../login/alpha-login-guard.service';


const routes: Routes = [
  { path: 'PBX', component: AphaPBXComponent, canActivate: [AlphaLoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AlphaPBXRoutingModule { }
