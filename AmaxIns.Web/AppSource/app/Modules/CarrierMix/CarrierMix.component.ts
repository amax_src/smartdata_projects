import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import { PageService } from '../Services/pageService';
import * as _ from 'underscore';
import { DateService } from '../Services/dateService';
import { CarrierMixService } from '../Services/carrierMixService';
import { CarrierMixUserModel } from '../../model/CarrierMixUserModel';
import { CarrierMixModel } from '../../model/CarrierMixModel';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { authenticationService } from '../../login/authenticationService.service';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-CarrierMix',
  templateUrl: './CarrierMix.component.html',
  styleUrls: ['./CarrierMix.component.scss']
})

export class CarrierMixComponent implements OnInit, AfterViewChecked {

  CarrierMix: Array<CarrierMixModel> = new Array<CarrierMixModel>();
  location: Array<Location> = new Array<Location>();
  PageName: string = "Carrier Mix";
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<any> = new Array<any>();
  selectedRegionalManager: [] = [];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];

  CarrierSelectItem: SelectItem[] = [];
  selectedCarrier: Array<string> = [];

  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();
  usermodel: CarrierMixUserModel = new CarrierMixUserModel();
  pager: any = {};
  pagedItems: any[];
  RawChartData: Array<{ Policy: any, Transactions: any, AgencyFee: any, AgencyFeeperPolicy: any, Premium: any, carrierName: string }>;
  ChartData: Array<any> = new Array<any>();
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  BackRoundColor: Array<any> = ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980']
  BorderColor: Array<any> = ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980']


  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context && (context.dataset.label === "Policy" || context.dataset.label === "Transactions")) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
          else {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          if (data && data.datasets && data.datasets.length > 0 && (data.datasets[tooltipItem.datasetIndex].label === "Policy" || data.datasets[tooltipItem.datasetIndex].label === "Transactions")) {
            return data.datasets[tooltipItem.datasetIndex].label + `- ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
          else {
            return data.datasets[tooltipItem.datasetIndex].label + `- $${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private _pageservice: PageService, private DateSrv: DateService, private cmSrv: CarrierMixService, private ApiLoad: ApiLoadTimeService, private authenticationService: authenticationService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.fillYear();
    this.getMonth();
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }


  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;

  }
  LocationChanged(e) {
   
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.getMonth();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = e;
    if (this.selectedzonalManager.length !== 0 || this.selectedRegionalManager.length !== 0) {
      this.location.forEach(m => {
        this.selectedlocation.push(m.agencyId);
      })
    }
   else if (this.authenticationService.userRole === "storemanager") {
      this.location.forEach(m => {
        this.selectedlocation.push(m.agencyId);
      })
    }
    //this.getMonth();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  SearchReport() {
    this.getCarrier();
    this.getDate();
    this.getCarrierMixData();
  }

  filterReset() {
    this.child.filterReset();
      this.year = (new Date()).getFullYear().toString();
    this.fillYear();
    this.getMonth();
  }

  onCarrierChange() {
    //this.getCarrierMixData()
  }

  getMonth() {
    this.selectedmonth = this.monthNames[new Date().getMonth()]
    this.DateSrv.getMonth(this.year).subscribe(m => {
      this.monthSelectItem = [];
      let months: Array<string> = new Array<string>();
      months = m;
      months = months.reverse();
      if (months.length > 0) {
        this.selectedmonth = months[0]
      }
      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i, value: i
          })
      })
      this.getCarrier();
      this.getDate();
      this.getCarrierMixData();
    })
  }


  getCarrier() {
    this.cmSrv.getCarriers(this.selectedmonth, this.year).subscribe(m => {
      this.CarrierSelectItem = [];
      let Carrier: Array<string> = new Array<string>();
      Carrier = m;
      Carrier.forEach(i => {
        this.CarrierSelectItem.push(
          {
            label: i, value: i
          })
      })
    })
    //Carrier;
    //CarrierSelectItem: SelectItem[] = [];
    //selectedCarrier: string = "";
  }


  onMonthchange(e) {
    this.CarrierSelectItem = [];
    //this.getDate();
    //this.getCarrier();
    //this.getCarrierMixData()
  }


  getDate() {
    this.selectedDate = [];
    this.dateSelectItem = [];
    let dt: Array<Date> = new Array<Date>();
    this.cmSrv.GetDate(this.selectedmonth, this.year).subscribe(m => {
      dt = m
      dt.forEach(i => {
        this.dateSelectItem.push(
          {
            label: this.getDateString(i), value: i
          })
      })
    })

  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('/');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  getCarrierMixData() {
    this.showLoader = true;
    this.pager = {};
    this.pagedItems = [];
    //this.RawChartData = new Array<{ Policy: any, Transactions: any, AgencyFee: any, AgencyFeeperPolicy: any, Premium: any, carrierName: string }>();

    this.usermodel.agency = new Array<number>();
    this.usermodel.carrier = new Array<string>();
    this.usermodel.date = new Array<string>();
    this.usermodel.month = this.selectedmonth;
    this.usermodel.year = this.year;
    if (this.selectedlocation && this.selectedlocation.length) {
      this.usermodel.agency = this.selectedlocation;
    }

    if (this.selectedDate && this.selectedDate.length > 0) {
      this.selectedDate.forEach(m => {
        this.usermodel.date.push(m.toString());
      })
    }

    if (this.selectedCarrier && this.selectedCarrier.length > 0) {
      this.selectedCarrier.forEach(m => {
        this.usermodel.carrier.push(m);
      })
    }

    let ChartLables: Array<any> = ["Policy", "Transactions", "Agency Fee", "Agency Fee per Policy", "Premium"];
    let startTime: number = new Date().getTime();
    this.cmSrv.GetCarrierData(this.usermodel).subscribe(m => {
      this.RawChartData = [];
      this.CarrierMix = new Array<CarrierMixModel>();
      this.CarrierMix = m;

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Carrier Mix "
      model.time = ResponseTime


      this.ChartData = [];
      this.CarrierMix.forEach((e, i) => {
        let Carrier = e.carrier;
        let Policy: any = {
          label: "Policy",
          backgroundColor: this.BackRoundColor[0],
          borderColor: this.BorderColor[0],
          data: [e.policies]
        }

        let Transactions: any = {
          label: "Transactions",
          backgroundColor: this.BackRoundColor[1],
          borderColor: this.BorderColor[1],
          data: [e.transactions]
        }

        let AgencyFee: any = {
          label: "Agency Fee",
          backgroundColor: this.BackRoundColor[2],
          borderColor: this.BorderColor[2],
          data: [e.agencyfee]
        }

        let AgencyFeeperPolicy: any = {
          label: "Agency Fee per Policy",
          backgroundColor: this.BackRoundColor[3],
          borderColor: this.BorderColor[3],
          data: [e.avgAgencyFeeByPolicy]
        }

        let Premium: any = {
          label: "Premium",
          backgroundColor: this.BackRoundColor[4],
          borderColor: this.BorderColor[4],
          data: [e.premium]
        }

        this.RawChartData.push({ Policy: Policy, Transactions: Transactions, AgencyFee: AgencyFee, AgencyFeeperPolicy: AgencyFeeperPolicy, Premium: Premium, carrierName: Carrier })

      })



      this.RawChartData.forEach(m => {
        this.ChartData.push(
          {
            ChatName: m.carrierName,
            datasets: [m.Policy, m.Transactions, m.AgencyFee, m.AgencyFeeperPolicy, m.Premium]
          }
        )

      })


      this.setPage(1);
      this.ApiLoad.SaveApiTime(model).subscribe();
      this.showLoader = false;
    })
  }


  onDateChange() {
    //this.getCarrierMixData()
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this._pageservice.getPager(this.ChartData.length, page);

    // get current page of items
    this.pagedItems = this.ChartData.slice(this.pager.startIndex, this.pager.endIndex + 1);

  }

    fillYear() {
        this.YearSelectItem = [];

        this.YearSelectItem.push(
            {
                label: '2020', value: '2020'
            })
        this.YearSelectItem.push(
            {
                label: '2021', value: '2021'
            })
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
        this.YearSelectItem.push(
            {
                label: '2023', value: '2023'
            })
    }

  onYearchange() {
    //this.getMonth();
  }


}
