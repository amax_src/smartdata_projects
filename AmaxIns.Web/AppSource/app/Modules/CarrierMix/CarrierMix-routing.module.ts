import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CarrierMixComponent } from './CarrierMix.component';


const routes: Routes = [
  { path: 'CarrierMix', component: CarrierMixComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CarrierMixRoutingModule { }
