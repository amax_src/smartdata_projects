import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { CarrierMixComponent } from './CarrierMix.component';
import { CarrierMixRoutingModule } from './CarrierMix-routing.module';



@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    CarrierMixRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [CarrierMixComponent],
  exports: []
 })
export class CarrierMixModule { }
