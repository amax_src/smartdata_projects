import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class ReportService {
  _baseUrl: string = "http://18.216.112.189:8082/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }





    GetAvgAgencyFees(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetAvgAgencyFees/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetAvgAgencyFeesII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetAvgAgencyFeesII/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetAvgAgencyFeesIII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetAvgAgencyFeesIII/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetAgentPerformaceI(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location  };
    const url = `${this.baseApiUrl}Report/GetAgentPerformaceI/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetAgentPerformaceII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location  };
    const url = `${this.baseApiUrl}Report/GetAgentPerformaceII/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  // Agent Breakdown

    GetAgencyBreakdownI(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location};
    const url = `${this.baseApiUrl}Report/GetAgencyBreakdownI/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetAgencyBreakdownII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location};
    const url = `${this.baseApiUrl}Report/GetAgencyBreakdownII/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetAgencyBreakdownIII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
    const url = `${this.baseApiUrl}Report/GetAgencyBreakdownIII/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetUniqueQuotes(year: number, month: string, location: Array<number>) {
        console.log(year);
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}UniqueCreatedQuotes/GetAllUniqueCreatedQuotes/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

  GetAllMarket() {
    const url = `${this.baseApiUrl}UniqueCreatedQuotes/GetAllMarket/`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  // Overtime Report

    GetOvertimeI(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
    const url = `${this.baseApiUrl}Report/GetOvertimeI/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetOvertimeII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
    const url = `${this.baseApiUrl}Report/GetOvertimeII/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetOvertimeIII(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
    const url = `${this.baseApiUrl}Report/GetOvertimeIII/`;
    let headers = this.header;
    return this.http.post<any>(url, listofArray, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

 CarrierMixI(year: number, month: string) {
   const url = `${this.baseApiUrl}CarrierMixNew/CarrierMixI/` + year + '/' + month;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  CarrierMixPremium(year: number, month: string) {
    const url = `${this.baseApiUrl}CarrierMixNew/CarrierMixPremium/` + year + '/' + month;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  CarrierMixAgencyFee(year: number, month: string) {
    const url = `${this.baseApiUrl}CarrierMixNew/CarrierMixAgencyFee/` + year + '/' + month;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  // Tracker

    GetRenterTracker(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetRenterTracker/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetProductionTracker(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetProductionTracker/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetActiveCustomerTracker(year: Array<string>, month: Array<string>, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}Report/GetActiveCustomerTracker/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

  // *************************Excel Export For Tracker Report*********************************
  ExportRenterTracker(RenterTrackerlist: any) {
    const url = `${this.baseApiUrl}Report/ExportRenterTracker/`;
    let headers = this.header;
    return this.http.post<any>(url, RenterTrackerlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportProductionTracker(ProductionTrackerlist: any) {
    const url = `${this.baseApiUrl}Report/ExportProductionTracker/`;
    let headers = this.header;
    return this.http.post<any>(url, ProductionTrackerlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportActiveCustomerTracker(ActiveCustomerTrackerlist: any) {
    const url = `${this.baseApiUrl}Report/ExportActiveCustomerTracker/`;
    let headers = this.header;
    return this.http.post<any>(url, ActiveCustomerTrackerlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  // *************************Excel Export For Tracker Report END*********************************

  //****************************** Agent Performace Excel Export**********************************
  ExportAgentPerformaceI(AgentPerformacelist: any) {

    const url = `${this.baseApiUrl}Report/ExportAgentPerformaceI/`;
    let headers = this.header;
    return this.http.post<any>(url, AgentPerformacelist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExporttAgentPerformaceII(AgentPerformacelist: any) {

    const url = `${this.baseApiUrl}Report/ExportAgentPerformaceII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgentPerformacelist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }
//****************************** Agent Performace Excel Export* END *********************************

  // Export Excel
  ExportAgencyFeeI(AgencyFeeIlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyFeeI/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyFeeIlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportAgencyFeeII(AgencyFeeIlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyFeeII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyFeeIlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportAgencyFeeIII(AgencyFeeIlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyFeeIII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyFeeIlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }


  ExportExcelAgencyBreakDownByOfficeI(AgencyBreakDownByOfficelist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyBreakDownByOfficeI/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownByOfficelist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelAgencyBreakDownZM(AgencyBreakDownZMlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyBreakDownByOfficeII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownZMlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelAgencyBreakDownROM(AgencyBreakDownROMlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelAgencyBreakDownByOfficeIII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownROMlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl  + x;
        return filename;
      }));
  }


  ExportExcelOverTimeByOfficeI(AgencyBreakDownByOfficelist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelGetOvertimeI/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownByOfficelist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelOverTimeZM(AgencyBreakDownZMlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelGetOvertimeII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownZMlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelOverTimeROM(AgencyBreakDownROMlist: any) {
    const url = `${this.baseApiUrl}Report/ExportExcelGetOvertimeIII/`;
    let headers = this.header;
    return this.http.post<any>(url, AgencyBreakDownROMlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelUniqueCretedQuotes(UniqueCreatedQuoteslist: any) {
    const url = `${this.baseApiUrl}UniqueCreatedQuotes/ExportExcelUniqueCretedQuotes/`;
    let headers = this.header;
    return this.http.post<any>(url, UniqueCreatedQuoteslist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelCarrierMixI(CarrierMixlist: any) {
    const url = `${this.baseApiUrl}CarrierMixNew/ExportExcelCarrierMix/`;
    let headers = this.header;
    return this.http.post<any>(url, CarrierMixlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  /// Renters Reports

    GetRentersReport(year: number, month: string, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}RentersReport/GetRentersData/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetActiveCustomerData(year: number, month: string, location: Array<number>) {
        let listofArray = { year: year, months: month, location: location };
        const url = `${this.baseApiUrl}RentersReport/GetActiveCustomerData/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

  ExportRentersReport(RentersReportList: any) {
    const url = `${this.baseApiUrl}RentersReport/ExportExcelRenterData/`;
    let headers = this.header;
    return this.http.post<any>(url, RentersReportList, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportActiveCustomerReport(ActiveCustomerList: any) {
    const url = `${this.baseApiUrl}RentersReport/ExportExcelActiveCustomer/`;
    let headers = this.header;
    return this.http.post<any>(url, ActiveCustomerList, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }


  ExportExcelCarrierMixPremium(CarrierMixlist: any) {
    const url = `${this.baseApiUrl}CarrierMixNew/ExportExcelCarrierMixPremium/`;
    let headers = this.header;
    return this.http.post<any>(url, CarrierMixlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportExcelCarrierMixAgencyFee(CarrierMixlist: any) {
    const url = `${this.baseApiUrl}CarrierMixNew/ExportExcelCarrierMixAgencyFee/`;
    let headers = this.header;
    return this.http.post<any>(url, CarrierMixlist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

}
