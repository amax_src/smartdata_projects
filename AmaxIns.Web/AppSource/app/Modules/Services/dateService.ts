import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getYear() {
    const url = `${this.baseApiUrl}Date/GetYears`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonth(year: string) {
    const url = `${this.baseApiUrl}/Date/GetMonth/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetMonthsCurrentYear() {
    const url = `${this.baseApiUrl}/Date/GetMonthsCurrentYear`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetMonthskeyvalue(year: string) {
    const url = `${this.baseApiUrl}/Date/GetMonthKeyValue/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetExtraMonthskeyvalue(year: string) {
    const url = `${this.baseApiUrl}/Date/GetExtraMonthKeyValue/${year}`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDates() {
    const url = `${this.baseApiUrl}/Date/GetDates`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetMonthPlanner() {
    const url = `${this.baseApiUrl}/Date/GetMonthPlanner`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDatesByMonth(monthName,year:string) {
    const url = `${this.baseApiUrl}/Date/GetDatesByMonth/` + monthName + '/' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  
}
