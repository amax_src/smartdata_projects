import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { debug } from 'util';
import { calendarFormModel } from '../../model/Calendar/calendarFormModel';
import { calendarModel } from '../../model/Calendar/calendarModel';
import { ActualPolicyDetails, PlannerActualDetails } from '../../model/plannerModel';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  addEvent(addEvent: calendarModel) {
    const url = `${this.baseApiUrl}Calendar/AddEvents`;
    let headers = this.header;
    return this.http.post<any>(url,addEvent, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getEvents(agencyId: Array<number>, month: string) {
    const url = `${this.baseApiUrl}Calendar/GetEvents?month=` + month;
    let headers = this.header;
    return this.http.post<any>(url,agencyId,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

  updateEvent(postData: any) {
    const url = `${this.baseApiUrl}Calendar/UpdateEvent`;
    let headers = this.header;
    return this.http.post<any>(url, postData, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  removeEvent(postData: any) {
    const url = `${this.baseApiUrl}Calendar/RemoveEvent`;
    let headers = this.header;
    return this.http.post<any>(url, postData, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  //---------------------------------------------------

  GetAllActivity() {
    const url = `${this.baseApiUrl}Planner/GetAllActivity`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAllHours() {
    const url = `${this.baseApiUrl}Planner/GetAllHours`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  SaveUpdatePlanner(postData: any) {
    const url = `${this.baseApiUrl}Planner/SaveUpdatePlanner`;
    let headers = this.header;
    return this.http.post<any>(url, postData, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  DeleteBudgetPlanner(plannerBudgetId:number) {
    const url = `${this.baseApiUrl}Planner/DeleteBudgetPlanner/` + plannerBudgetId
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAllPlanner(agencyid:number,monthName:string ) {
    const url = `${this.baseApiUrl}Planner/GetAllPlanner/` + agencyid + '/' + monthName;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAllPlannerActual(agencyid: number, monthName: string,year:string) {
    const url = `${this.baseApiUrl}Planner/GetActualPlanner/` + agencyid + '/' + monthName + '/' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetSingelActual(plannerBudgetId: number) {
    const url = `${this.baseApiUrl}Planner/GetSingelActual/` + plannerBudgetId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  SaveUpdateActual(postData: PlannerActualDetails) {
    const url = `${this.baseApiUrl}Planner/SaveUpdateActual`;
    let headers = this.header;
    return this.http.post<any>(url, postData, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDashboardReport(monthName: string,year:string) {
    const url = `${this.baseApiUrl}Planner/GetDashboardReport/` + monthName + '/' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDailyViewReport(agencyid: number, monthName: string,year:string) {
    const url = `${this.baseApiUrl}Planner/GetDailyViewReport/` + monthName + '/' + agencyid + '/' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDataForWeekly(param: any) {
    const url = `${this.baseApiUrl}Planner/GetDataForWeekly`;
    let headers = this.header;
    return this.http.post<any>(url, param, { headers })
      .pipe(map(x => {

        console.log("Data:- ", x);
        return x;
      }));
  }

  GetPlannerActivityTotal(monthName: string,year:string, agencyid:Array<number>) {
    const url = `${this.baseApiUrl}Planner/GetPlannerActivityTotal/` + monthName + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetPlannerActivityAgencyWise( monthName: string,year:string) {
    const url = `${this.baseApiUrl}Planner/GetPlannerActivityAgencyWise/` + monthName+'/'+year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
}
