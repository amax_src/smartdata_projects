import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class EprService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  getEPRData(month: string, AgencyId: Array<number>,year:string) {
    const url = `${this.baseApiUrl}EPR/GetEPRData?month=` + month + '&year=' + year;
    let headers = this.header;
    return this.http.post<any>(url, AgencyId ,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getEPRTotalData(month: string, AgencyId: Array<number>, year: string) {
    const url = `${this.baseApiUrl}EPR/GetEPRTotalData?month=` + month + '&year=' + year;
    let headers = this.header;
    return this.http.post<any>(url, AgencyId, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getEPRMonth() {
    const url = `${this.baseApiUrl}EPR/GetEPRMonth`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

    GetEPRGraph(year: Array<string>, location: Array<number>, selectedagent: Array<string>) {
        let listofArray = { year: year, months: null, location: location, selectedagent: selectedagent };
        console.log("listofArray", listofArray);
        const url = `${this.baseApiUrl}EPR/GetEPRGraph/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }


    //*********************************************************************ALPA EPR Application**************************

    getAlpaEPRData(month: string, AgencyId: Array<number>, year: string) {
        const url = `${this.baseApiUrl}ALPAEPR/GetAlpaEPRData?month=` + month + '&year=' + year;
        let headers = this.header;
        return this.http.post<any>(url, AgencyId, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAlpaEPRTotalData(month: string, AgencyId: Array<number>, year: string) {
        const url = `${this.baseApiUrl}ALPAEPR/GetAlpaEPRTotalData?month=` + month + '&year=' + year;
        let headers = this.header;
        return this.http.post<any>(url, AgencyId, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAlpaEPRMonth() {
        const url = `${this.baseApiUrl}ALPAEPR/GetAlpaEPRMonth`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetAlpaEPRGraph(year: Array<string>, location: Array<number>, selectedagent: Array<string>) {
        let listofArray = { year: year, months: null, location: location, selectedagent: selectedagent };
        console.log("listofArray", listofArray);
        const url = `${this.baseApiUrl}ALPAEPR/GetAlpaEPRGraph/`;
        let headers = this.header;
        return this.http.post<any>(url, listofArray, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
}
