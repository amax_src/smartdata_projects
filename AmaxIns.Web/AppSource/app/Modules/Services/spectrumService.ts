import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class spectrumService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

 
  GetSpectrumDailySummary() {
    const url = `${this.baseApiUrl}Spectrum/GetSpectrumDailySummary`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetSpectrumMonthlySummary() {
    const url = `${this.baseApiUrl}Spectrum/GetSpectrumMonthlySummary`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetSpectrumDailyDetail(x: Date, AgencyId: number) {
    const url = `${this.baseApiUrl}Spectrum/GetSpectrumDailyDetail?date=` + x + "&AgencyId=" + AgencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetSpectrumMonthlyDetail(month:string,agencyid:number) {
    const url = `${this.baseApiUrl}Spectrum/GetSpectrumMonthlyDetail?month=` + month + "&agencyId=" + agencyid;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetSpectrumCallVolume() {
    const url = `${this.baseApiUrl}Spectrum/GetSpectrumCallVolume`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCallCountByLocationAndDate(date: Date, Agencyid:Array<number>) {
    const url = `${this.baseApiUrl}Spectrum/GetCallCountByLocationAndDate?date=` + date;
    let headers = this.header;
    return this.http.post<any>(url, Agencyid,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCallCountAll(date: Date) {
    
    const url = `${this.baseApiUrl}Spectrum/GetCallCountAll?date=` + date;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetCallCountByLocationAndDateOutBound(date: Date, Agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Spectrum/GetCallCountByLocationAndDateOutBound?date=` + date;
    let headers = this.header;
    return this.http.post<any>(url, Agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCallCountAllOutBound(date: Date) {

    const url = `${this.baseApiUrl}Spectrum/GetCallCountAllOutBound?date=` + date;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetRepeatedCallersDetail(x: Date) {
    const url = `${this.baseApiUrl}Spectrum/RepeatedCallers?date=` + x;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDates() {
    const url = `${this.baseApiUrl}Spectrum/GetDate`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetTalkTimeOutBoundLastSevenDay(agencyId: Array<number>) {
    const url = `${this.baseApiUrl}Spectrum/GetTalkTimeOutBoundLastSevenDay`;
    let headers = this.header;
    return this.http.post<any>(url, agencyId, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCallsOutBoundLastSevenDay(agencyId: Array<number>) {
    const url = `${this.baseApiUrl}Spectrum/GetCallOutBoundLastSevenDay`;
    let headers = this.header;
    return this.http.post<any>(url, agencyId, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  
  
}
