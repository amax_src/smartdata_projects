import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { TopFiveAgentUserModel } from '../../model/TopFiveAgentUserModel';


@Injectable({
  providedIn: 'root'
})
export class TopAgentService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

 
  GetAgency() {
    const url = `${this.baseApiUrl}TopAgent/GetAgency`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetYear() {
    const url = `${this.baseApiUrl}TopAgent/GetYear`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetPremium(e:TopFiveAgentUserModel ) {
    const url = `${this.baseApiUrl}TopAgent/GetPremiumData`;
    let headers = this.header;
    return this.http.post<any>(url,e, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAgencyFee(e: TopFiveAgentUserModel) {
    const url = `${this.baseApiUrl}TopAgent/GetAgencyFee`;
    let headers = this.header;
    return this.http.post<any>(url, e, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetPolicyCount(e: TopFiveAgentUserModel) {
    const url = `${this.baseApiUrl}TopAgent/GetPolicyCount`;
    let headers = this.header;
    return this.http.post<any>(url, e, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

}
