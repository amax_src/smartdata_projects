import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class PayrollService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  GetPayRoleBudgetedActualData (month:string,year:string) {
    const url = `${this.baseApiUrl}payroll/GetPayRoleBudgetedActualData?month=` + month+'&year=' + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  
  GetPayRollTileData(agencyId: Array<number>, month: string, year: string) {
    const url = `${this.baseApiUrl}/payroll/GetPayrollTiles?month=` + month + '&year=' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyId ,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

}
