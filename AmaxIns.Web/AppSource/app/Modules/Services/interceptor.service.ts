import { CommonService } from '../../core/common.service'
import {
    HttpEvent, HttpRequest, HttpHandler,
    HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { LoaderService } from './loader.service';
import { async } from '@angular/core/testing';

@Injectable({
    providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

    constructor(public loaderService: LoaderService,
        private cmnSrv: CommonService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loaderService.isLoading.next(true);

        // console.info(`timestamp: ${new Date().toTimeString()} --> showLoader: 
        //     ${this.cmnSrv.showLoader} && isLoading: 
        //     ${this.loaderService.isLoading.toPromise().then((value) => value)}`);

        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    // refresh token
                } else {
                    return throwError(error);
                }
                this.cmnSrv.showLoader = false;
            }),
            finalize(
                () => {
                    this.loaderService.isLoading.next(false);

                    // console.info(`timestamp: ${new Date().toTimeString()} --> showLoader: 
                    //     ${this.cmnSrv.showLoader} && isLoading: 
                    //     ${this.loaderService.isLoading.toPromise().then((value) => value)}`);
                }
            )
        );
    }
}