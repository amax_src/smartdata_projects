import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';

import { CustomerRetention } from '../../model/CustomerRetention/CustomerRetention';

@Injectable({
  providedIn: 'root'
})
export class CustomerRetentionService {

  header: HttpHeaders;
  _baseUrl: string = "http://18.216.112.189:8082/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  /*
  getAll() {
    const url = `${this.baseApiUrl}ActivityType`;
    let headers = this.header;

    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));

    //return this.http.get<any>(url, { headers })
    //  .toPromise()
    //  .then(res => <ActivityType[]>res.data)
    //  .then(data => { return data; });

  }
  */

  getAllPaging(params) {
    const url = `${this.baseApiUrl}CustomerRetention`;
    let headers = this.header;
    console.log(this.baseApiUrl);
    return this.http.get<any>(url, { headers: headers, params: params }).toPromise();

    //return this.http.get<any>(url, { headers })
    //  .toPromise()
    //  .then(res => <ActivityType[]>res.data)
    //  .then(data => { return data; });

  }

  getAllPagingExport(params) {
    console.log("params", params)
    const url = `${this.baseApiUrl}CustomerRetentionExport`;
    let headers = this.header;
    return this.http.get<any>(url, { params: params,headers: headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }


  getAllCountForZonePaging(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForZone`;
    let headers = this.header;
    return this.http.post<any>(url, params, { headers: headers }).toPromise();
  }

  getAllCountForZonePagingExport(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForZoneExport`;
    let headers = this.header;
    return this.http.post<any>(url, params, { headers: headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  getAllCountForRegionPaging(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForRegion`;
    let headers = this.header;

    return this.http.post<any>(url, params, { headers: headers }).toPromise();

    //return this.http.get<any>(url, { headers })
    //  .toPromise()
    //  .then(res => <ActivityType[]>res.data)
    //  .then(data => { return data; });

  }

  getAllCountForRegionPagingExport(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForRegionExport`;
    let headers = this.header;
    return this.http.post<any>(url, params, { headers: headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  getAllCountForHodPaging(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForHod`;
    let headers = this.header;

    return this.http.post<any>(url, params, { headers: headers }).toPromise();

    //return this.http.get<any>(url, { headers })
    //  .toPromise()
    //  .then(res => <ActivityType[]>res.data)
    //  .then(data => { return data; });

  }

  getAllCountForHodPagingExport(params) {
    const url = `${this.baseApiUrl}CustomerRetentionCountForHodExport`;
    let headers = this.header;
    return this.http.post<any>(url, params, { headers: headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

}
