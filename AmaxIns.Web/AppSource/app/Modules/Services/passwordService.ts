 import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  ValidateCurrentPassword(username: string, password: string, logintype: string) {
    const url = `${this.baseApiUrl}Authentication/Validate`;
    return this.http.post<any>(url, { username, password, logintype })
      .pipe(map(user => {
          return user;
      }));
  }

  UpdatePassword(loginName: string, password: string, newPassword: string, repeatPassword: string, role: string) {
    const url = `${this.baseApiUrl}Authentication/ResetPassword`;
    let headers = this.header;
    return this.http.post<any>(url, { loginName, password, newPassword, repeatPassword, role },{ headers })
      .pipe(map(x => {
        return x;
      }));
  }
 

}
