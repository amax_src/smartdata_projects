import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { CarrierMixUserModel } from '../../model/CarrierMixUserModel';

@Injectable({
  providedIn: 'root'
})
export class CarrierMixService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getCarriers(e: string, y: string) {
    const url = `${this.baseApiUrl}CarrierMix/GetCarrier/`+e+'/'+y;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDate(e: string, y: string) {
    const url = `${this.baseApiUrl}CarrierMix/GetDate/` + e+'/'+y;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCarrierData(e: CarrierMixUserModel) {
    const url = `${this.baseApiUrl}CarrierMix/GetData/`;
    let headers = this.header;
    return this.http.post<any>(url, e, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

}
