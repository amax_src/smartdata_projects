import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class AISService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

 
  getAISData() {
    const url = `${this.baseApiUrl}AIS/GetAISData`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAISDataCSV() {
    const url = `${this.baseApiUrl}AIS/GetAISDataCSV`;
    let headers = this.header;
    return this.http.get(url, { responseType: 'blob', headers:headers })
      .pipe(map(x => {
        return x;
      }));
  }


    //****************************************************ALPA Application **************************
    GetAISAplaData() {
        const url = `${this.baseApiUrl}AISApla/GetAISAplaData`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

}
