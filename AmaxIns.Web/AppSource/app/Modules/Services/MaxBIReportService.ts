import { MaxBIHierarchyRequest, HierarchyInfo, MaxBIDashboardRequest } from '../../model/MaxBIReport';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { E_MaxBIReport, MaxBIRequest, 
  AgencyPerPolicy, NewModifiedQuotes, InboundOutboundCalls, 
  MaxBIPayroll, MaxBIPayrollFilter } from '../../model/MaxBIReport';
import * as moment from 'moment';
import { EmployeeInfo } from '../../model/EmployeeInfo';
import { HourlyProduction } from '../../model/HourlyProduction';


@Injectable({
  providedIn: 'root'
})
export class MaxBIReportService {
  _baseUrl: string = "http://18.216.112.189:8082/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  GetCatalogs(request: MaxBIRequest) {
    let url = `${this.baseApiUrl}MaxBIReport/GetCatalogs`;
    let headers = this.header;
    return this.http.post<any>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetHierarchyLevel(request: MaxBIHierarchyRequest) {
    let url = `${this.baseApiUrl}MaxBIReport/GetHierarchyLevel`;
    let headers = this.header;
    return this.http.post<Array<HierarchyInfo>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAgentCarrierAFeePremium(request: MaxBIRequest) {
    let url = '';
    if (!request.AgencyIds) {
      request.AgencyIds = [];
    }
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    if (!request.CompanyIds) {
      request.CompanyIds = [];
    }

    switch (request.ReportType) {
      case E_MaxBIReport.CarrierAgent:
        url = `${this.baseApiUrl}MaxBIReport/GetCarrierAgents`;
        break;
      case E_MaxBIReport.AgencyFeePerPolicy:
        url = `${this.baseApiUrl}MaxBIReport/GetAFeePerPolicy`;
        break;
      case E_MaxBIReport.PremiumPerPolicy:
        url = `${this.baseApiUrl}MaxBIReport/GetPremiumPerPolicy`;
        break;
    }
    let headers = this.header;
    return this.http.post<any>(url, request, { headers })
      .pipe(map((x) => {
        x.forEach(r => {
          if (r.dateTransaction) {
            r.dateTransaction = moment(r.dateTransaction).format("YYYY-MM-DD");
          }
        });
        return x;
      }));
  }

  GetAgencyPerPolicyReport(request: MaxBIRequest) {

    let url = '';
    if (!request.AgencyIds) {
      request.AgencyIds = [];
    }
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    if (!request.CompanyIds) {
      request.CompanyIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetAFeePremiumPerPolicy`;
    let headers = this.header;
    return this.http.post<Array<AgencyPerPolicy>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  GetAgencyPerPolicyReportSummary(request: MaxBIRequest) {
    let url = '';
    if (!request.HierarchyIds) {
      request.HierarchyIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetAFeePremiumPerPolicySummary`;
    let headers = this.header;
    return this.http.post<number>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetNewModifiedQuotes(request: MaxBIRequest) {
    let url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotes`;
    if (!request.AgencyIds) {
      request.AgencyIds = [];
    }
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    if (!request.CompanyIds) {
      request.CompanyIds = [];
    }

    let headers = this.header;
    return this.http.post<Array<NewModifiedQuotes>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  GetNewModifiedQuotesSummary(request: MaxBIRequest) {
    let url = '';
    if (!request.HierarchyIds) {
      request.HierarchyIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotesSummary`;
    let headers = this.header;
    return this.http.post<number>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetNewModifiedQuotesClosingRatios(request: MaxBIRequest) {
    let url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotesClosingRatios`;
    if (!request.AgencyIds) {
      request.AgencyIds = [];
    }
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    if (!request.CompanyIds) {
      request.CompanyIds = [];
    }

    request.IsClosingRatio = true;
    let headers = this.header;
    return this.http.post<Array<NewModifiedQuotes>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  GetNewModifiedQuotesClosingRatiosSummary(request: MaxBIRequest) {
    let url = '';
    if (!request.HierarchyIds) {
      request.HierarchyIds = [];
    }
    request.IsClosingRatio = true;
    
    url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotesClosingRatiosSummary`;
    let headers = this.header;
    return this.http.post<number>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetInboundOutboundCalls(request: MaxBIRequest) {
    let url = `${this.baseApiUrl}MaxBIReport/GetInboundOutboundCalls`;
    if (!request.AgencyIds) {
      request.AgencyIds = [];
    }
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    if (!request.CompanyIds) {
      request.CompanyIds = [];
    }

    let headers = this.header;
    return this.http.post<Array<InboundOutboundCalls>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  GetInboundOutboundCallsSummary(request: MaxBIRequest) {
    let url = '';
    if (!request.HierarchyIds) {
      request.HierarchyIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetInboundOutboundCallsSummary`;
    let headers = this.header;
    return this.http.post<number>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetPayrollDashboardFilterData(request: MaxBIDashboardRequest) {
    let url = '';
    if (!request.AgentIds) {
      request.AgentIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetPayrollDashboardFilterData`;
    let headers = this.header;
    return this.http.post<Array<MaxBIPayrollFilter>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetPayrollDashboardData(request: MaxBIDashboardRequest) {
    let url = '';
    if (!request.AgentIds) {
      request.AgentIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetPayrollDashboardData`;
    let headers = this.header;
    return this.http.post<Array<MaxBIPayroll>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetEmployeeSensitiveInfo(request: MaxBIDashboardRequest) {
    let url = '';
    if (!request.AgentIds) {
      request.AgentIds = [];
    }

    url = `${this.baseApiUrl}MaxBIReport/GetEmployeeSensitiveInfo`;
    let headers = this.header;
    return this.http.post<Array<EmployeeInfo>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAgentHourlyProduction(request: MaxBIRequest) {
    let url = '';
    if (!request.AgentIds) {
      request.AgentIds = [];
    }
    
    url = `${this.baseApiUrl}MaxBIReport/GetAgentHourlyProduction`;
    let headers = this.header;
    return this.http.post<Array<HourlyProduction>>(url, request, { headers })
      .pipe(map(x => {
        return x;
      }));
    }

    GetNewModifiedQuotesClosingRatiosForEPR(request: MaxBIRequest) {
        let url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotesClosingRatiosForEPR`;
        if (!request.AgencyIds) {
            request.AgencyIds = [];
        }
        if (!request.AgentIds) {
            request.AgentIds = [];
        }
        if (!request.CompanyIds) {
            request.CompanyIds = [];
        }
        request.IsClosingRatio = true;
        let headers = this.header;
        return this.http.post<Array<NewModifiedQuotes>>(url, request, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetNewModifiedQuotesClosingRatiosForEPRGraph(request: MaxBIRequest) {
        let url = `${this.baseApiUrl}MaxBIReport/GetNewModifiedQuotesClosingRatiosForEPRGraph`;
        if (!request.AgencyIds) {
            request.AgencyIds = [];
        }
        if (!request.AgentIds) {
            request.AgentIds = [];
        }
        if (!request.CompanyIds) {
            request.CompanyIds = [];
        }
        request.IsClosingRatio = true;
        let headers = this.header;
        return this.http.post<Array<NewModifiedQuotes>>(url, request, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
}
