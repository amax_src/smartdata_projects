import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';

@Injectable({
  providedIn: 'root'
})
export class AgencyService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getAllLocations() {
    const url = `${this.baseApiUrl}Agency`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAllLocationsRegionalManager(id: Array<number>) {
    const url = `${this.baseApiUrl}Agency/GetByRegionalManager`;
    let headers = this.header;
    return this.http.post<any>(url,id, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAllLocationsRegionalZonalManager(rid: Array<number>, zid: Array<number>, ) {
    const url = `${this.baseApiUrl}Agency/GetByRegionalAndZonalManager`;
    let headers = this.header;
    return this.http.post<any>(url, { "Rmid": rid, "Zmid": zid}, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getByBsmId(ids: Array<number>) {
    const url = `${this.baseApiUrl}Agency/GetByBsmId`;
    let headers = this.header;
    return this.http.post<any>(url, ids, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  getStoreManagerAgency(id: number) {
    const url = `${this.baseApiUrl}Agency/GetStoreManagerAgency`;
    let headers = this.header;
    return this.http.post<any>(url, id, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  
}
