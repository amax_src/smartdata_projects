import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';

@Injectable({
  providedIn: 'root'
})
export class DashBoardService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  getQuotesDashboard(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetQuotesDashboard/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetRevenueDashboard(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetRevenueDashboard/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getEPRDashboard(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetEPRDashboard/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getCallsDashboard(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetCallsDashboard/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getPayrollDashboard(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetPayrollDashboardData/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  getEPRDetails(month: number, year: number, agencyid: Array<number>) {
    const url = `${this.baseApiUrl}Dashboard/GetEPRDashboardDetails/` + month + '/' + year;
    let headers = this.header;
    return this.http.post<any>(url, agencyid, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
  

}
