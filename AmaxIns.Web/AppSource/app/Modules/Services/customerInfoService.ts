import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { custmoreInfoUserModel } from '../../model/custmoreInfoUserModel';


@Injectable({
  providedIn: 'root'
})
export class CustomerInfoService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }



  getAgencyName() {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/GetAgencyName`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getPolicyType() {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/GetPolicyType`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getCity() {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/GetCity`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getClients(searchText:string) {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/GetclientName?SearchText=` + searchText;
    let headers = this.header;
    return this.http.get<any>(url, { headers }).toPromise();
  }


  GetPolicyNumber(searchText: string) {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/GetPolicyNumber?SearchText=` + searchText;
    let headers = this.header;
    return this.http.get<any>(url, { headers }).toPromise();
  }


  GetCustomerInfoData(Year: string, e: custmoreInfoUserModel) {
    const url = `${this.baseApiUrl}AlphaCustomerInfo/CustomerInfoData?Year=` + Year;
    let headers = this.header;
    return this.http.post<any>(url, e, { headers }).pipe(map(x => {
      return x;
    }));
  }
 
}
