import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { BudgetSearch, BudgetActualVsPlanDonut } from '../../model/Budget/BudgetSearch';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class bugetService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  getDepartMent() {
    const url = `${this.baseApiUrl}Budget/Department`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getCategories(dept: string) {
    const url = `${this.baseApiUrl}Budget/Categories?Dept=` + dept;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetName(dept: string, catg: Array<string>) {
    const url = `${this.baseApiUrl}Budget/BudgetName`;
    let budgetSearch: BudgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch , { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetDonutData(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetByMonth`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  getBudgetAnuallData(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetAnnual`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetTrx(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetTrx`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetTrxCSV(searh: BudgetActualVsPlanDonut): Observable<Blob>{
    const url = `${this.baseApiUrl}Budget/BudgetTrxCsv`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers });
  }

  GetBudgetSpend(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetSpend`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers });
  }
}
