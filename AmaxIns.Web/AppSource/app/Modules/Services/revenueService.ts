import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { QuotesParam } from '../../model/NewModifiedQuotes';


@Injectable({
  providedIn: 'root'
})
export class RevenueService {

   //_baseUrl: string = "http://localhost:44305/Export/";
   _baseUrl: string = "http://18.216.112.189:8082/Export/";

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  getRevenueData () {
    const url = `${this.baseApiUrl}Revenue`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getRevenueDailyData(month:string) {
    const url = `${this.baseApiUrl}RevenueDaily?month=` + month;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getRefreshDate() {
    const url = `${this.baseApiUrl}Revenue/GetRefreshDate`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getTierData() {
    const url = `${this.baseApiUrl}Revenue/GetTierData`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonth(year: string) {
    const url = `${this.baseApiUrl}Revenue/GetMonth?year=` + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


    //  ***************************************************** ALPA Revenue Service ******************************************
    getAlpaRevenueData() {
        const url = `${this.baseApiUrl}AlpaRevenue/GetAlpaRevenueData`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAlpaRevenueDailyData(month: string) {
        const url = `${this.baseApiUrl}AlpaRevenue/AlpaRevenueDaily?month=` + month;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetDatesByMonth(monthName: string, year: string) {
        const url = `${this.baseApiUrl}Date/GetDatesByMonth/` + monthName + '/' + year;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    DailyTransactionPaymentsService(quoteParam: QuotesParam, month: string, year: string) {
        const url = `${this.baseApiUrl}AlpaRevenue/DailyTransactionPayments?month=` + month + `&year=` + year;
        let headers = this.header;
        return this.http.post<any>(url, quoteParam, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    ExportDailyTransactonPayment(dataDailyTransactonPayment: any) {
        const url = `${this.baseApiUrl}AlpaRevenue/ExportDailyTransactonPayment/`;
        let headers = this.header;
        return this.http.post<any>(url, dataDailyTransactonPayment, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }

}
