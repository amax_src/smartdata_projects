import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';

@Injectable({
  providedIn: 'root'
})
export class AlpaUserService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getAllRegionalManagers() {
      const url = `${this.baseApiUrl}AlpaHierarchy/RegionalManagerALPA`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAllSaleDirectors() {
      const url = `${this.baseApiUrl}AlpaHierarchy/AlpaSaleDirector`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAllZonalManagers() {
      const url = `${this.baseApiUrl}AlpaHierarchy/ZonalManagerALPA`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getZonalManagersByRegionalManager(id: Array<number>) {
      const url = `${this.baseApiUrl}AlpaHierarchy/ByMultipleRegionalManager`;
    let headers = this.header;
    return this.http.post<any>(url, id, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getRegionalManagerBySaleDirector(id: Array<number>) {
      const url = `${this.baseApiUrl}AlpaHierarchy/ByMultipleSaleDirector`;
    let headers = this.header;
    return this.http.post<any>(url, id, { headers })
      .pipe(map(x => {
        return x;
      }));
    }

    //----------------------------Agencyy---------------------------

    getAllLocations() {
        const url = `${this.baseApiUrl}AlpaHierarchy/GetAllAgency`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAllLocationsRegionalZonalManager(rid: Array<number>, zid: Array<number>,) {
        const url = `${this.baseApiUrl}AlpaHierarchy/GetByRegionalAndZonalManager`;
        let headers = this.header;
        return this.http.post<any>(url, { "Rmid": rid, "Zmid": zid }, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAllLocationsRegionalManager(id: Array<number>) {
        const url = `${this.baseApiUrl}AlpaHierarchy/GetByRegionalManager`;
        let headers = this.header;
        return this.http.post<any>(url, id, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getStoreManagerAgency(id: number) {
        const url = `${this.baseApiUrl}AlpaHierarchy/GetStoreManagerAgency`;
        let headers = this.header;
        return this.http.post<any>(url, id, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
}
