import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { QuotesParam } from '../../model/NewModifiedQuotes';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {
  _baseUrl: string = "http://18.216.112.189:8082/Export/";
  //_baseUrl: string = "http://18.216.112.189:8097/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
      private sessionService: SessionService, private _auth: authenticationService)
    {
      this.header = new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this._auth.userToken
      });
    }

  getPayDate(year: string, Month: string) {
    const url = `${this.baseApiUrl}Quotes/GetPaydate?year=` + year + `&Month=` + Month;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  GetBoundQuotes(date: string) {
    const url = `${this.baseApiUrl}Quotes/GetBoundQuotes?&date=` + date
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetBoundQuotesNoturborator(date: string) {
    const url = `${this.baseApiUrl}Quotes/GetBoundNoTurboratorQuotes?&date=` + date
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetQuotesSale() {
    const url = `${this.baseApiUrl}Quotes/GetQuotesSale`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetQuotesProjection() {
    const url = `${this.baseApiUrl}Quotes/GetQuotesProjection`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetModifiedNewQuotes(AgencyIds: Array<string>, month: string,year:string) {
    const url = `${this.baseApiUrl}Quotes/GetNewModiFiedQuotes?month=` + month + `&year=` + year;
    let headers = this.header;
    return this.http.post<any>(url, AgencyIds, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetModifiedNewQuotes_New(quoteParam: QuotesParam, month: string, year: string) {
    const url = `${this.baseApiUrl}Quotes/GetNewAndModifiedData_New?month=` + month + `&year=` + year;
    let headers = this.header;
    return this.http.post<any>(url, quoteParam, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


    ModifiedQuotesSoldTiles(quoteParam: QuotesParam, month: string, year: string) {
        const url = `${this.baseApiUrl}Quotes/ModifiedQuotesSoldTiles?month=` + month + `&year=` + year;
        let headers = this.header;
        return this.http.post<any>(url, quoteParam, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

  GetNewModiFiedQuotesProjection(month: string, year: string) {
    const url = `${this.baseApiUrl}Quotes/GetNewModiFiedQuotesProjection?month=` + month + `&year=` + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDistinctDateQuotes(month: string, year: string) {
    const url = `${this.baseApiUrl}Quotes/GetDistinctDateQuotes?month=` + month + `&year=` + year;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  ExportNewQuotes(UniqueCreatedQuoteslist: any) {
    const url = `${this.baseApiUrl}Quotes/ExportNewQuotes/`;
    let headers = this.header;
    return this.http.post<any>(url, UniqueCreatedQuoteslist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  ExportModiFiedQuotes(UniqueCreatedQuoteslist: any) {
    const url = `${this.baseApiUrl}Quotes/ExportModiFiedQuotes/`;
    let headers = this.header;
    return this.http.post<any>(url, UniqueCreatedQuoteslist, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }
}
