import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';


@Injectable({
  providedIn: 'root'
})
export class HourlyLoadService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  getDate(month: string) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetDate?month=` + month;
    let headers = this.header;
    return this.http.get<any>(url ,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getMonth() {
    const url = `${this.baseApiUrl}/HourlyLoad/GetMonth`;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getData(date: string,agencyId :number) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetData?Date=` + date + "&AgencyId=" + agencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getQuoteHourlyData(date: string, agencyId: number) {
    const url = `${this.baseApiUrl}/HourlyLoad/GetHourlyNewModifiedQuotes?Date=` + date + "&AgencyId=" + agencyId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
}
