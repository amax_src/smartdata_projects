import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from '../../core/storage/sessionservice';
import { BASE_API_URL } from '../../core/environment.tokens';
import { authenticationService } from '../../login/authenticationService.service';
import { ApiLoadModel } from '../../model/ApiLoadModel';


@Injectable({
  providedIn: 'root'
})
export class ApiLoadTimeService {

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private sessionService: SessionService, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  SaveApiTime(model: ApiLoadModel) {
    const url = `${this.baseApiUrl}APILoad/Save`;
    let headers = this.header;
    model.user = this._auth.currentUserValue.loginName
    return this.http.post<any>(url, model ,{ headers })
      .pipe(map(x => {
        return x;
      }));
  }

 

}
