import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RevenueComponent } from './revenue.component';
import { LoginGuard } from '../../login/login-guard.service';

const routes: Routes = [
  { path: 'Revenue', component: RevenueComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RevenueRoutingModule { }
