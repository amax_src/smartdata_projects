import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RevenueComponent } from './revenue.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { RevenueRoutingModule } from './revenue-routing.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'

@NgModule({
  imports: [SharedModule,
     MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    RevenueRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [RevenueComponent],
  exports: []
 })
export class RevenueModule { }
