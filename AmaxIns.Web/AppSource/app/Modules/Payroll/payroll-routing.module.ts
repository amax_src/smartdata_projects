import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { PayrollComponent } from './payroll.component';

const routes: Routes = [
  { path: 'Payroll', component: PayrollComponent, canActivate: [LoginGuard]}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PayrollRoutingModule { }
