import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { MatTabsModule } from '@angular/material';
import { PayrollRoutingModule } from './payroll-routing.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'primeng/chart'
import { PayrollComponent } from './payroll.component';

@NgModule({
  imports: [
    SharedModule,
    MultiSelectModule,
    TabViewModule,
    MatTabsModule,
    PayrollRoutingModule,
    FormsModule,
    DropdownModule,
    BrowserModule,
    ChartModule
  ],
  declarations: [PayrollComponent],
  exports: []
 })
export class PayrollModule { }
