import { Component, OnInit, Renderer2, AfterViewChecked, ViewChild, ElementRef, PACKAGE_ROOT_URL } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SessionService } from '../../core/storage/sessionservice';
import { SelectItem } from 'primeng/api';
import { PayrollService } from '../Services/payrollService';
import { PayRollData } from '../../model/PayRollData';
import { PayRollChatData } from '../../model/payRollChatData';
import { Location } from '../../model/location';
import { authenticationService } from '../../login/authenticationService.service';
import { PageService } from '../Services/pageService';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { DateService } from '../Services/dateService';
import { RmZmLocationDropdownComponent } from '../../shared/rm-zm-location-dropdown/rm-zm-location-dropdown.component';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.scss']
})

export class PayrollComponent implements OnInit, AfterViewChecked {
  selectedRegionalManager: [];
  selectedzonalManager: []
  selectedlocation: []
  location: Array<Location> = new Array<Location>();
  Actual: Array<PayRollData> = new Array<PayRollData>();
  Budgeted: Array<PayRollData> = new Array<PayRollData>();
  selectedTabIndex: number = 0;

  totalAmount: Array<PayRollChatData> = new Array<PayRollChatData>();
  totalHours: Array<PayRollChatData> = new Array<PayRollChatData>();
  PageName: string = "Payroll";
    year: string = (new Date()).getFullYear().toString();
  YearSelectItem: SelectItem[] = [];
  totalDebitAmountChardata: Array<any> = new Array<any>();
  totalDebitHourChardata: Array<any> = new Array<any>();

  summaryDebitAmountChardata: any;
  summaryDebitHourChardata: any;
  SetLocations: Array<number> = [];

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectmonthindex: number = 0;
  selectedmonth: string = "";


  cities1: SelectItem[];
  selectedCities1: [{ name: string, code: string }];

  categoryActual: Array<any>;
  categoryActualSelectItem: SelectItem[] = [];
  selectedcategoryActual: []

  categoryProjection: Array<any>;
  categoryProjectionSelectItem: SelectItem[] = [];
  selectedcategoryProjection: []

  PayPeriod: Array<any>;
  PayPeriodSelectItem: SelectItem[] = [];
  selectedPayPeriod: []

  ischartDataLoaded: boolean = false;
  showLoader: boolean;

  chartheight: number = 150;

  ChartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: true,


    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          //labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }

  chartOptionsWithDollar: any = {
    animation: {
      duration: 0
    },
    responsive: true,


    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `$${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC"
        },
        scaleLabel: {
          display: true,
          //labelString: '1k = 1000',
          fontColor: "#CCC"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC"
        }
      }]
    }

  }

  tileData = {
    overtimeperPolicy: 0,
    overtimehoursperpolicy: 0,
    payrollperPolicy: 0,
    payrollpertransaction: 0
  }

  pagerAmount: any = {};
  pagedAmountItems: any[];

  pagerHour: any = {};
  pagedHourItems: any[];

  isFirstTimeLoadPage: boolean = true;
  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService,
    private sessionService: SessionService, private payrollService: PayrollService, private authenticationService: authenticationService,
    private _pageservice: PageService, private ApiLoad: ApiLoadTimeService, private _DateService: DateService) {

    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }
  @ViewChild(RmZmLocationDropdownComponent) child;
  ngOnInit() {
    this.isFirstTimeLoadPage = true;
    //this.GetPayRoleBudgetedData();
    this.fillYear()
    this.fillMonth()
    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }

  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedlocation = [];
    this.selectedzonalManager = e;

  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedlocation = [];
    this.selectedRegionalManager = e;

  }

  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    //this.filterData()

  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    this.location = e;
    //this.filterData()

  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }

  SearchReport() {
    if (this.isFirstTimeLoadPage) {
      this.isFirstTimeLoadPage = false;
      this.GetPayRoleBudgetedData();
    }
    else {
      this.GetPayRoleBudgetedData();
    }
  }

  filterReset() {
    this.child.filterReset();
    this.PageName = "Payroll";
      this.year = (new Date()).getFullYear().toString();
    this.selectedTabIndex = 0;
    this.fillMonth();
    this.GetPayRoleBudgetedData();
  }

  GetPayRoleBudgetedData() {
    this.showLoader = true;
    let startTime: number = new Date().getTime();
    this.payrollService.GetPayRoleBudgetedActualData(this.selectedmonth, this.year).subscribe(m => {
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Payroll Data"
      model.time = ResponseTime
      this.Actual = m.actual;
      this.Budgeted = m.budgted;
      this.getcategoryActual();
      this.getcategoryProjection();
      this.getPayPeriod();
      this.filterData();
      this.ischartDataLoaded = true;
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    });
  }

  FilterDataForTotalActualVsBudgeted(agencyid) {
    let checkLocation = this.SetLocations.filter(m => m === agencyid)
    if (checkLocation.length === 0) {
      this.SetLocations.push(agencyid);
    }
    let actualAmountSum = 0;
    let projectionAmountSum = 0;
    let differenceAmount = 0;
    let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

    let actualHourSum = 0;
    let projectionHourSum = 0;
    let differenceHour = 0;
    let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
    //Filter data By month start 

    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)

    //End 

    _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
    _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      _tempActual = this.filterByActualPayPeriod(_tempActual);
    }

    if (this.selectedcategoryActual && this.selectedcategoryActual.length > 0) {
      _tempActual = this.filterByActualDiscription(_tempActual);
    }

    if (this.selectedcategoryProjection && this.selectedcategoryProjection.length > 0) {
      _tempBudgeted = this.filterByProjectionDiscription(_tempBudgeted);
    }

    let locationfiltered = this.location.filter(m => m.agencyId === agencyid);
    if (locationfiltered && locationfiltered.length > 0) {
      _temppayrollchatAmountData.Key = locationfiltered[0].agencyName;
      _temppayrollchatHourData.Key = locationfiltered[0].agencyName;
    }


    _tempActual.forEach(x => {

      actualAmountSum = actualAmountSum + x.debitAmount;
      actualHourSum = actualHourSum + x.debitHours;
    })

    _tempBudgeted.forEach(x => {

      projectionAmountSum = projectionAmountSum + x.debitAmount;
      projectionHourSum = projectionHourSum + x.debitHours;
    })

    differenceAmount = projectionAmountSum - actualAmountSum;
    differenceHour = projectionHourSum - actualHourSum;

    _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
    _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
    _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

    _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
    _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
    _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

    this.totalAmount.push(_temppayrollchatAmountData);
    this.totalHours.push(_temppayrollchatHourData);
    this.getcategoryActual();

  }

  FilterDataForRegularActualVsBudgeted(agencyid) {
    let checkLocation = this.SetLocations.filter(m => m === agencyid)
    if (checkLocation.length === 0) {
      this.SetLocations.push(agencyid);
    }
    let actualAmountSum = 0;
    let projectionAmountSum = 0;
    let differenceAmount = 0;
    let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

    let actualHourSum = 0;
    let projectionHourSum = 0;
    let differenceHour = 0;
    let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
    //Filter data By month start 

    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)

    //End 

    _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
    _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      _tempActual = this.filterByActualPayPeriod(_tempActual);
    }

    _tempActual = this.filterByRegularCategoryDiscription(_tempActual);

    _tempBudgeted = this.filterByRegularCategoryDiscription(_tempBudgeted);


    let locationfiltered = this.location.filter(m => m.agencyId === agencyid);
    if (locationfiltered && locationfiltered.length > 0) {
      _temppayrollchatAmountData.Key = locationfiltered[0].agencyName;
      _temppayrollchatHourData.Key = locationfiltered[0].agencyName;
    }


    _tempActual.forEach(x => {

      actualAmountSum = actualAmountSum + x.debitAmount;
      actualHourSum = actualHourSum + x.debitHours;
    })

    _tempBudgeted.forEach(x => {

      projectionAmountSum = projectionAmountSum + x.debitAmount;
      projectionHourSum = projectionHourSum + x.debitHours;
    })

    differenceAmount = projectionAmountSum - actualAmountSum;
    differenceHour = projectionHourSum - actualHourSum;

    _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
    _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
    _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

    _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
    _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
    _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

    this.totalAmount.push(_temppayrollchatAmountData);
    this.totalHours.push(_temppayrollchatHourData);

  }

  FilterDataForOverTimeActualVsBudgeted(agencyid) {
    let checkLocation = this.SetLocations.filter(m => m === agencyid)
    if (checkLocation.length === 0) {
      this.SetLocations.push(agencyid);
    }
    let actualAmountSum = 0;
    let projectionAmountSum = 0;
    let differenceAmount = 0;
    let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

    let actualHourSum = 0;
    let projectionHourSum = 0;
    let differenceHour = 0;
    let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
    //Filter data By month start 

    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)

    //End 

    _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
    _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      _tempActual = this.filterByActualPayPeriod(_tempActual);
    }

    _tempActual = this.filterByOverTimeCategoryDiscription(_tempActual);

    _tempBudgeted = this.filterByOverTimeCategoryDiscription(_tempBudgeted);


    let locationfiltered = this.location.filter(m => m.agencyId === agencyid);
    if (locationfiltered && locationfiltered.length > 0) {
      _temppayrollchatAmountData.Key = locationfiltered[0].agencyName;
      _temppayrollchatHourData.Key = locationfiltered[0].agencyName;
    }


    _tempActual.forEach(x => {

      actualAmountSum = actualAmountSum + x.debitAmount;
      actualHourSum = actualHourSum + x.debitHours;
    })

    _tempBudgeted.forEach(x => {

      projectionAmountSum = projectionAmountSum + x.debitAmount;
      projectionHourSum = projectionHourSum + x.debitHours;
    })

    differenceAmount = projectionAmountSum - actualAmountSum;
    differenceHour = projectionHourSum - actualHourSum;

    _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
    _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
    _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

    _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
    _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
    _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

    this.totalAmount.push(_temppayrollchatAmountData);
    this.totalHours.push(_temppayrollchatHourData);

  }

  FilterTotalSummary(agencyid) {

    let actualAmountSum = 0;
    let projectionAmountSum = 0;
    let differenceAmount = 0;
    let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

    let actualHourSum = 0;
    let projectionHourSum = 0;
    let differenceHour = 0;
    let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
    //Filter data By month start 

    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)

    //End 
    if (agencyid !== -1) {
      _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
      _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)
    }

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      _tempActual = this.filterByActualPayPeriod(_tempActual);
    }

    _tempActual.forEach(x => {

      actualAmountSum = actualAmountSum + x.debitAmount;
      actualHourSum = actualHourSum + x.debitHours;
    })

    _tempBudgeted.forEach(x => {

      projectionAmountSum = projectionAmountSum + x.debitAmount;
      projectionHourSum = projectionHourSum + x.debitHours;
    })

    differenceAmount = projectionAmountSum - actualAmountSum;
    differenceHour = projectionHourSum - actualHourSum;

    _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
    _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
    _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

    _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
    _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
    _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

    this.totalAmount.push(_temppayrollchatAmountData);
    this.totalHours.push(_temppayrollchatHourData);

  }

  FilterTotalOvertimeSummary(agencyid) {

    let actualAmountSum = 0;
    let projectionAmountSum = 0;
    let differenceAmount = 0;
    let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

    let actualHourSum = 0;
    let projectionHourSum = 0;
    let differenceHour = 0;
    let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
    //Filter data By month start 

    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)

   
    //End

    _tempActual = this.filterByOverTimeCategoryDiscription(_tempActual);

    _tempBudgeted = this.filterByOverTimeCategoryDiscription(_tempBudgeted);

    console.log("_tempBudgeted", _tempBudgeted);
    if (agencyid !== -1) {
      _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
      _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)
    }

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      _tempActual = this.filterByActualPayPeriod(_tempActual);
    }

    _tempActual.forEach(x => {

      actualAmountSum = actualAmountSum + x.debitAmount;
      actualHourSum = actualHourSum + x.debitHours;
    })

    _tempBudgeted.forEach(x => {

      projectionAmountSum = projectionAmountSum + x.debitAmount;
      projectionHourSum = projectionHourSum + x.debitHours;
    })

    differenceAmount = projectionAmountSum - actualAmountSum;
    differenceHour = projectionHourSum - actualHourSum;

    console.log("projectionAmountSum", projectionAmountSum);
    _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
    _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
    _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

    _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
    _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
    _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

    this.totalAmount.push(_temppayrollchatAmountData);
    this.totalHours.push(_temppayrollchatHourData);

  }

    FilterTotalRegularSummary(agencyid) {

        let actualAmountSum = 0;
        let projectionAmountSum = 0;
        let differenceAmount = 0;
        let _temppayrollchatAmountData: PayRollChatData = new PayRollChatData();

        let actualHourSum = 0;
        let projectionHourSum = 0;
        let differenceHour = 0;
        let _temppayrollchatHourData: PayRollChatData = new PayRollChatData();

        let _tempActual: Array<PayRollData> = new Array<PayRollData>();
        let _tempBudgeted: Array<PayRollData> = new Array<PayRollData>();
        //Filter data By month start 

        _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
        _tempBudgeted = this.Budgeted.filter(m => m.months === this.selectedmonth)


        //End

        _tempActual = this.filterByRegularCategoryDiscription(_tempActual);

        _tempBudgeted = this.filterByRegularCategoryDiscription(_tempBudgeted);

        console.log("_tempBudgeted", _tempBudgeted);
        if (agencyid !== -1) {
            _tempActual = _tempActual.filter(m => m.agencyid === agencyid)
            _tempBudgeted = _tempBudgeted.filter(m => m.agencyid === agencyid)
        }

        if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
            _tempActual = this.filterByActualPayPeriod(_tempActual);
        }

        _tempActual.forEach(x => {

            actualAmountSum = actualAmountSum + x.debitAmount;
            actualHourSum = actualHourSum + x.debitHours;
        })

        _tempBudgeted.forEach(x => {

            projectionAmountSum = projectionAmountSum + x.debitAmount;
            projectionHourSum = projectionHourSum + x.debitHours;
        })

        differenceAmount = projectionAmountSum - actualAmountSum;
        differenceHour = projectionHourSum - actualHourSum;

        console.log("projectionAmountSum", projectionAmountSum);
        _temppayrollchatAmountData.Data.Actual = Math.round(actualAmountSum);
        _temppayrollchatAmountData.Data.Projection = Math.round(projectionAmountSum);
        _temppayrollchatAmountData.Data.Difference = Math.round(differenceAmount);

        _temppayrollchatHourData.Data.Actual = Math.round(actualHourSum);
        _temppayrollchatHourData.Data.Projection = Math.round(projectionHourSum);
        _temppayrollchatHourData.Data.Difference = Math.round(differenceHour);

        this.totalAmount.push(_temppayrollchatAmountData);
        this.totalHours.push(_temppayrollchatHourData);

    }

    filterData() {
        this.showLoader = true;
        this.GetTileData();
        this.totalAmount = new Array<PayRollChatData>();
        this.totalHours = new Array<PayRollChatData>();
        if (this.selectedlocation && this.selectedlocation.length > 0) {
            if (this.selectedTabIndex === 0) {
                this.selectedlocation.forEach(i => this.FilterTotalSummary(i))
            }
            else if (this.selectedTabIndex === 1) {
                this.selectedlocation.forEach(i => this.FilterTotalRegularSummary(i))
            }
            else if (this.selectedTabIndex === 2) {
                this.selectedlocation.forEach(i => this.FilterTotalOvertimeSummary(i))
            }
            else if (this.selectedTabIndex === 3) {
                this.selectedlocation.forEach(i => this.FilterDataForTotalActualVsBudgeted(i))
            }
            else if (this.selectedTabIndex === 4) {
                this.selectedlocation.forEach(i => this.FilterDataForRegularActualVsBudgeted(i))
            }
            else if (this.selectedTabIndex === 5) {
                this.selectedlocation.forEach(i => this.FilterDataForOverTimeActualVsBudgeted(i))
            }

        }
        else {
            let setlocation: Array<Location> = new Array<Location>();
            if (this.selectedTabIndex === 0) {
                this.location.forEach(i => this.FilterTotalSummary(i.agencyId))
            }
            else if (this.selectedTabIndex === 1) {
                this.location.forEach(i => this.FilterTotalRegularSummary(i.agencyId))
            }
            else if (this.selectedTabIndex === 2) {
                this.location.forEach(i => this.FilterTotalOvertimeSummary(i.agencyId))
            }
            else if (this.selectedTabIndex === 3) {
                this.location.forEach(i => this.FilterDataForTotalActualVsBudgeted(i.agencyId))
            }
            else if (this.selectedTabIndex === 4) {
                this.location.forEach(i => this.FilterDataForRegularActualVsBudgeted(i.agencyId))
            }
            else if (this.selectedTabIndex === 5) {
                this.location.forEach(i => this.FilterDataForOverTimeActualVsBudgeted(i.agencyId))
            }

        }
        if (this.selectedTabIndex === 0 || this.selectedTabIndex === 1 || this.selectedTabIndex === 2) {
            this.CreateChartDataForsummaryDebitAmount();
            this.CreateChartDataForsummaryHourAmount();
        }
        else if (this.selectedTabIndex === 3 || this.selectedTabIndex === 4 || this.selectedTabIndex === 5) {
            this.CreateChartDataForTotalDebitAmount();
            this.CreateChartDataForTotalDebitHours();
        }

        setTimeout(() => {
            this.showLoader = false;
        }, 2000);
    }

  CreateChartDataForsummaryDebitAmount() {
    let summryAmount: PayRollChatData = new PayRollChatData();
    let ProjectionSum: number = 0;
    let ActualSum: number = 0;
    let Difference: number = 0;
    this.totalAmount.forEach(e => {
      ProjectionSum = ProjectionSum + e.Data.Projection;
      ActualSum = ActualSum + e.Data.Actual;
    });

    Difference = ProjectionSum - ActualSum;

    this.summaryDebitAmountChardata = {
      labels: ['Projection', 'Actual', 'Difference'],

      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [ProjectionSum, ActualSum, Difference]
        }
      ]
    }
  }

  CreateChartDataForsummaryHourAmount() {
    let summryAmount: PayRollChatData = new PayRollChatData();
    let ProjectionSum: number = 0;
    let ActualSum: number = 0;
    let Difference: number = 0;
    this.totalHours.forEach(e => {
      ProjectionSum = ProjectionSum + e.Data.Projection;
      ActualSum = ActualSum + e.Data.Actual;
    });

    Difference = ProjectionSum - ActualSum;

    this.summaryDebitHourChardata = {
      labels: ['Projection', 'Actual', 'Difference'],
      datasets: [
        {
          backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
          borderColor: '#1E88E5',
          data: [ProjectionSum, ActualSum, Difference]
        }
      ]
    }
  }

  CreateChartDataForTotalDebitAmount() {
    this.pagerAmount = {};
    this.pagedAmountItems = [];

    this.totalDebitAmountChardata = new Array<any>();
    let TempTotalAmount: Array<PayRollChatData> = new Array<PayRollChatData>();
    TempTotalAmount = this.totalAmount.reverse();
    TempTotalAmount.forEach(e => {
      let Data = {
        labels: ['Projection', 'Actual', 'Difference'],
        ChatName: e.Key,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            borderColor: '#1E88E5',
            data: [e.Data.Projection, e.Data.Actual, e.Data.Difference]
          }
        ]
      }
      this.totalDebitAmountChardata.push(Data);
    })
    this.setAmountPage(1);
  }

  CreateChartDataForTotalDebitHours() {
    this.pagerHour= {};
    this.pagedHourItems=[];
    this.totalDebitHourChardata = new Array<any>();
    let TempTotalHours: Array<PayRollChatData> = new Array<PayRollChatData>();
    TempTotalHours = this.totalHours.reverse();
    TempTotalHours.forEach(e => {
      let Data = {
        labels: ['Projection', 'Actual', 'Difference'],
        ChatName: e.Key,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83'],
            borderColor: '#1E88E5',
            data: [e.Data.Projection, e.Data.Actual, e.Data.Difference]
          }
        ]
      }
      this.totalDebitHourChardata.push(Data);
    })

    this.setHourPage(1);

  }

    fillYear() {
        this.YearSelectItem = [];

        this.YearSelectItem.push(
            {
                label: '2020', value: '2020'
            })
        this.YearSelectItem.push(
            {
                label: '2021', value: '2021'
            })
        this.YearSelectItem.push(
            {
                label: '2022', value: '2022'
            })
        this.YearSelectItem.push(
            {
                label: '2023', value: '2023'
            })
    }

  onYearchange() {
    this.fillMonth();
  }

  fillMonth() {
    this._DateService.getMonth(this.year).subscribe(m => {
    //this.payrollService.GetPayRoleMonth().subscribe(m => {
      this.monthSelectItem = [];

      let months: Array<string> = new Array<string>();

      months = m;//Array.from(new Set(this.Budgeted.map(item => item.months)));

      months.forEach(i => {
        this.monthSelectItem.push(
          {
            label: i, value: i
          })
      })

      let monthcount = (new Date()).getMonth();
      this.selectmonthindex = (new Date()).getMonth() - 1;;

      for (var x = monthcount; x >= 0; x--) {
        let checkmonth = months.filter(m => m === this.monthNames[x])
        if (checkmonth && checkmonth.length > 0) {
          this.selectmonthindex = x;
          break;
        }
      }

      this.selectedmonth = this.monthNames[this.selectmonthindex];
      //this.GetPayRoleBudgetedData();
    })

  }

  onMonthchange(e) {
    //this.getPayPeriod()
    //this.filterData();
    //this.GetPayRoleBudgetedData();
  }

  getcategoryActual() {
    this.categoryActualSelectItem = [];
    let categoryActual: Array<string> = new Array<string>();
    //categoryActual = Array.from(new Set(this.ActualForFiterDD.map(item => item.categoryDescription)));
    categoryActual = Array.from(new Set(this.Actual.map(item => item.categoryDescription)));
    categoryActual.forEach(i => {
      this.categoryActualSelectItem.push(
        {
          label: i, value: i
        })
    })
  }

  getcategoryProjection() {

    this.categoryProjectionSelectItem = [];
    let categoryProjection: Array<string> = new Array<string>();
    //categoryProjection = Array.from(new Set(this.BudgetedForFiterDD.map(item => item.categoryDescription)));
    categoryProjection = Array.from(new Set(this.Budgeted.map(item => item.categoryDescription)));
    categoryProjection.forEach(i => {
      this.categoryProjectionSelectItem.push(
        {
          label: i, value: i
        })
    })
  }


  getPayPeriod() {
    this.PayPeriodSelectItem = [];
    let Payperiod: Array<string> = new Array<string>();
    let _tempActual: Array<PayRollData> = new Array<PayRollData>();
    _tempActual = this.Actual.filter(m => m.months === this.selectedmonth)
    //categoryProjection = Array.from(new Set(this.ActualForFiterDD.map(item => item.payPeriod)));
    Payperiod = Array.from(new Set(_tempActual.map(item => item.payPeriod)));
    Payperiod.forEach(i => {
      this.PayPeriodSelectItem.push(
        {
          label: i, value: i
        })
    })
  }

  filterByActualPayPeriod(x: Array<PayRollData>): Array<PayRollData> {

    let Data: Array<PayRollData> = new Array<PayRollData>();

    if (this.selectedPayPeriod && this.selectedPayPeriod.length > 0) {
      this.selectedPayPeriod.forEach(z => {
        let filter: Array<PayRollData> = new Array<PayRollData>();
        filter = x.filter(m => m.payPeriod === z)
        filter.forEach(a => {
          Data.push(a);
        })
      })
    }

    return Data;
  }

  filterByActualDiscription(x: Array<PayRollData>): Array<PayRollData> {
    let Data: Array<PayRollData> = new Array<PayRollData>();
    if (this.selectedcategoryActual && this.selectedcategoryActual.length > 0) {
      this.selectedcategoryActual.forEach(z => {
        let filter: Array<PayRollData> = new Array<PayRollData>();
        filter = x.filter(m => m.categoryDescription === z)
        filter.forEach(a => {
          Data.push(a);
        })
      })
    }
    return Data;
  }


  filterByRegularCategoryDiscription(x: Array<PayRollData>): Array<PayRollData> {
    let Data: Array<PayRollData> = new Array<PayRollData>();
    let filter: Array<PayRollData> = new Array<PayRollData>();
    filter = x.filter(m => m.categoryDescription.trim() === "Regular")
    filter.forEach(a => {
      Data.push(a);
    })

    filter = x.filter(m => m.categoryDescription.trim() === "Regular Debit")
    filter.forEach(a => {
      Data.push(a);
    })


    return Data;
  }


  filterByOverTimeCategoryDiscription(x: Array<PayRollData>): Array<PayRollData> {

    let Data: Array<PayRollData> = new Array<PayRollData>();
    let filter: Array<PayRollData> = new Array<PayRollData>();
    filter = x.filter(m => m.categoryDescription.trim() === "Overtime")
    filter.forEach(a => {
      Data.push(a);
    })

    filter = x.filter(m => m.categoryDescription.trim() === "Overtime Debit")
    filter.forEach(a => {
      Data.push(a);
    })


    return Data;
    }

  filterByProjectionDiscription(x: Array<PayRollData>): Array<PayRollData> {

    let Data: Array<PayRollData> = new Array<PayRollData>();
    this.selectedcategoryProjection.forEach(z => {
      let filter: Array<PayRollData> = new Array<PayRollData>();
      filter = x.filter(m => m.categoryDescription === z)
      filter.forEach(a => {
        Data.push(a);
      })
    })

    return Data;
  }
  onPayperiodChange() { this.filterData(); }
  oncategoryProjectionChange() { this.filterData(); }
  oncategoryActualChange() { this.filterData(); }

  OntabSelectionChange(e) {
    this.totalDebitAmountChardata = new Array<any>();
    this.totalDebitHourChardata = new Array<any>();
    this.selectedTabIndex = e;
    this.filterData();
  }

  GetTileData() {

    let agencyId: Array<number> = new Array<number>();
    if (this.selectedlocation && this.selectedlocation.length > 0) {
      agencyId = this.selectedlocation;
    }
    else {
      this.location.forEach(e => {
        agencyId.push(e.agencyId);
      })
    }
    this.payrollService.GetPayRollTileData(agencyId, this.selectedmonth, this.year).subscribe(m => {
      this.tileData = m;
      this.tileData.overtimehoursperpolicy = this.tileData.overtimehoursperpolicy
      this.tileData.overtimeperPolicy = this.tileData.overtimeperPolicy;
      this.tileData.payrollperPolicy = Math.round(this.tileData.payrollperPolicy)
      this.tileData.payrollpertransaction = Math.round(this.tileData.payrollpertransaction)
    });
  }

  setAmountPage(page: number) {
    if (page < 1 || page > this.pagerAmount.totalPages) {
      return;
    }

    // get pager object from service
    this.pagerAmount = this._pageservice.getPayrollPager(this.totalDebitAmountChardata.length, page,5);

    // get current page of items
    this.pagedAmountItems = this.totalDebitAmountChardata.slice(this.pagerAmount.startIndex, this.pagerAmount.endIndex + 1);

  }


  setHourPage(page: number) {
    if (page < 1 || page > this.pagerHour.totalPages) {
      return;
    }

    // get pager object from service
    this.pagerHour = this._pageservice.getPayrollPager(this.totalDebitHourChardata.length, page,5);

    // get current page of items
    this.pagedHourItems = this.totalDebitHourChardata.slice(this.pagerHour.startIndex, this.pagerHour.endIndex + 1);

  }

}
