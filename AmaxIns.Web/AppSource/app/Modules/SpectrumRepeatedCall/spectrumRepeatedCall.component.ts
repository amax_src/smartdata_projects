import { Component, OnInit, Renderer2, AfterViewChecked } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import * as NProgress from 'nprogress';
import { CommonService } from '../../core/common.service';
import { SelectItem } from 'primeng/api';
import { Location } from '../../model/location';
import { spectrumService } from '../Services/spectrumService';
import { PageService } from '../Services/pageService';
import { repeatedCalls } from '../../model/RepeatedCalls';
import { SpectrumRepeatedCall } from '../../model/SpectrumRepeatedCall';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../Services/ApiLoadTimeService';



@Component({
  selector: 'app-SpectrumRepeatedCall',
  templateUrl: './spectrumRepeatedCall.component.html',
  styleUrls: ['./spectrumRepeatedCall.component.scss']
})

export class SpectrumRepeatedCallComponent implements OnInit, AfterViewChecked {
  RepeatedCalls: Array<repeatedCalls> = new Array<repeatedCalls>();
  MonthlyAvarageTalkTime: number = 0;
  CallTypeSelectItem: SelectItem[] = [];
  selectedCallType: string = "";
  sumAverageTalkTime: number = 0;
  CallNumberSelectItem: SelectItem[] = [];
  SelectedCallNumber: Array<string> = new Array<string>()
  outboundcalls: number = 0;
  dateRange: string = '';
  PageName: string = "Spectrum > Repeated calls";
  location: Array<Location> = new Array<Location>();
  selectedzonalManager: [] = [];
  ischartDataLoaded: boolean = false;
  showLoader: boolean = true;
  selectedlocation: Array<number> = new Array<number>();
  singleLocation: number = 112;
  selectedRegionalManager: [] = [];
 
  defaultselectedDate: Date;

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  selectedmonthList: Array<string> = new Array<string>();
  selectedmonthListForMothlyHeading: Array<string> = new Array<string>();
  singleLocationSelected: boolean = false;

  chartheight: number = 300;
  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  currentMonth: string = "Feburay";
  MonthlydetailMonth: string = '';
  date: Array<any>;
  dateSelectItem: SelectItem[] = [];
  selectedDate: Array<Date> = new Array<Date>();

  extentionSelectItem: SelectItem[] = [];
  selectedextention: Array<string> = new Array<string>();
  selectedTabIndex: number = 0;

  SpectrumDailySummary: any;
  SpectrumMonthlySummary: Array<any>;
  SpectrumMonthlyCallDetailSummary: Array<any>;

  
  ChartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'white',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    },
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return `${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label / 1000 + 'k';
          },
          fontColor: "#CCC",
        },
        scaleLabel: {
          display: true,
          //labelString: '1k = 1000',
          fontColor: "#CCC",
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#CCC",
        }
      }]
    }

  }
  firstLocation: number = 112;

  SetLocations: Array<number> = [];
  selectmonthindex: number;

  allLocations: Array<Location> = new Array<Location>();
  isFirstTimeLocationLoaded: boolean = true;
  RepeatedCallsIncomming: any = [];

  RepeatedCallsOutbound: any = [];

  RepeatedCallPager: any = {};
  RepeatedCallPagedItems: any[];

  SpectrumRepeatedCall: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();
  FilterSpectrumRepeatedCall: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();

  constructor(private router: Router, private renderer: Renderer2, public cmnSrv: CommonService, public spectrumService: spectrumService, private _pageservice: PageService, private ApiLoad: ApiLoadTimeService) {
    NProgress.configure({ showSpinner: false });
    this.renderer.addClass(document.body, 'preload');
  }

  ngOnInit() {
    this.GetDate()

    this.router.events.subscribe((obj: any) => {
      if (obj instanceof RouteConfigLoadStart) {
        NProgress.start();
        NProgress.set(0.4);
      } else if (obj instanceof RouteConfigLoadEnd) {
        NProgress.set(0.9);
        setTimeout(() => {
          NProgress.done();
          NProgress.remove();
        }, 500);
      } else if (obj instanceof NavigationEnd) {
        this.cmnSrv.dashboardState.navbarToggle = false;
        this.cmnSrv.dashboardState.sidebarToggle = true;
        window.scrollTo(0, 0);
      }
    });
  }

  ngAfterViewChecked() {

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'preload');
    }, 300);
  }



  ZmChanged(e) {
    this.selectedzonalManager = [];
    this.selectedzonalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    // this.filterData();
  }


  SetSingleLocation(e) {
    this.singleLocation = e;
    this.filterData();
  }

  RmChanged(e) {
    this.selectedRegionalManager = [];
    this.selectedRegionalManager = e;
    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();
    //this.filterData();
  }
  LocationChanged(e) {
    this.selectedlocation = [];
    this.selectedlocation = e;
    this.filterData();
  }

  GetAllLocations(e) {
    this.selectedlocation = [];
    if (this.isFirstTimeLocationLoaded) {
      this.allLocations = e;
      this.isFirstTimeLocationLoaded = false;
    }
    this.location = e;
    this.singleLocation = e[0].agencyId;
    this.filterData();
  }

  loaderStatusChanged(e) {
    if (this.ischartDataLoaded) {
      //this.showLoader = e;
    }
  }


  GetDate() {
    let firstRecord: boolean = true;
    this.dateSelectItem = []
    this.spectrumService.GetDates().toPromise().then(m => {
      let dt: Array<Date> = new Array<Date>();
      dt = m;
      dt.forEach(i => {
        if (firstRecord) {
          if (this.selectedDate.length === 0) {
            this.defaultselectedDate = i;
          }
          firstRecord = false;
        }

        this.dateSelectItem.push(
          {
            label: this.getDateString(i), value: i
          })

      })

      this.GetCallCountAll();
      this.GetCallCountAllOutBound();
    })
  }

  getDateString(i): string {
    let date: string = '';
    let parts = i.split('-');
    let month = this.monthNames[(parts[0] - 1)];
    date = month + " " + (parts[1]);
    return date;
  }

  onDateChange() {
    this.filterData();
  }

  onDropDownDateChange() {
    this.filterData();
  }


  filterData() {
    
    if (this.selectedTabIndex === 0) {
      if (this.selectedlocation.length > 0) {
        this.GetCallCountAllForLocation(this.selectedlocation);
        this.GetCallCountAllForLocationOutBound(this.selectedlocation);
      }
      else {
        if (this.allLocations.length === this.location.length) {
          this.GetCallCountAll();
          this.GetCallCountAllOutBound();
        }
        else {
          let agencyIds: Array<number> = new Array<number>();
          this.location.forEach(m => {
            agencyIds.push(m.agencyId);
          })
          this.GetCallCountAllForLocation(agencyIds);
          this.GetCallCountAllForLocationOutBound(agencyIds);
        }

      }
    }
    else if (this.selectedTabIndex === 1) {
      this.getRepetedCallDetailsVolume();
    }
  }


  sortMonth(a, b) {
    return a["sortmonthnum"] - b["sortmonthnum"];
  }


  OntabSelectionChange(e) {

    this.SetLocations = new Array<number>();
    this.selectedlocation = new Array<number>();

    this.selectedTabIndex = e;
    this.filterData();
  }

  GetCallCountAll() {
    this.RepeatedCallsIncomming = [];
    this.showLoader = true;
    let Cnumber: Array<string> = new Array<string>();
    let Count: Array<number> = new Array<number>();
    let startTime: number = new Date().getTime();
    this.spectrumService.GetCallCountAll(this.defaultselectedDate).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Spectrum Repeated call (Incoming) for all Location"
      model.time = ResponseTime

      this.RepeatedCalls = m;
      this.RepeatedCalls.forEach(x => {
        Cnumber.push(x.cnumber);
        Count.push(x.countOfIncomingCalls);
      })


      this.RepeatedCallsIncomming = {
        labels: Cnumber,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: Count
          }
        ]
      }
      this.ApiLoad.SaveApiTime(model).subscribe();
      this.showLoader = false;
    })

  }

  GetCallCountAllForLocation(Agencyid: Array<number>) {
    this.RepeatedCallsIncomming = []
    this.showLoader = true;
    let Cnumber: Array<string> = new Array<string>();
    let Count: Array<number> = new Array<number>();
    let startTime: number = new Date().getTime();
    this.spectrumService.GetCallCountByLocationAndDate(this.defaultselectedDate, Agencyid).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Spectrum Repeated call (Incoming) For Seleted Location"
      model.time = ResponseTime

      this.RepeatedCalls = m;

      this.RepeatedCalls.forEach(x => {
        Cnumber.push(x.cnumber);
        Count.push(x.countOfIncomingCalls);
      })


      this.RepeatedCallsIncomming = {
        labels: Cnumber,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: Count
          }
        ]
      }
      this.ApiLoad.SaveApiTime(model).subscribe();
      this.showLoader = false;
    })

  }


  GetCallCountAllOutBound() {
    this.RepeatedCallsOutbound = [];
    this.showLoader = true;
    let Cnumber: Array<string> = new Array<string>();
    let Count: Array<number> = new Array<number>();
    let startTime: number = new Date().getTime();
    this.spectrumService.GetCallCountAllOutBound(this.defaultselectedDate).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Spectrum Repeated call (Outbound) For All Location"
      model.time = ResponseTime

      this.RepeatedCalls = m;
      this.RepeatedCalls.forEach(x => {
        Cnumber.push(x.cnumber);
        Count.push(x.countOfIncomingCalls);
      })


      this.RepeatedCallsOutbound = {
        labels: Cnumber,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: Count
          }
        ]
      }
      this.ApiLoad.SaveApiTime(model).subscribe();
      this.showLoader = false;
    })

  }

  GetCallCountAllForLocationOutBound(Agencyid: Array<number>) {
    this.RepeatedCallsOutbound = []
    this.showLoader = true;
    let Cnumber: Array<string> = new Array<string>();
    let Count: Array<number> = new Array<number>();
    let startTime: number = new Date().getTime();

    this.spectrumService.GetCallCountByLocationAndDateOutBound(this.defaultselectedDate, Agencyid).subscribe(m => {
      this.RepeatedCalls = m;

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Spectrum Repeated call (OutBound) For Seleted Location"
      model.time = ResponseTime

      this.RepeatedCalls.forEach(x => {
        Cnumber.push(x.cnumber);
        Count.push(x.countOfIncomingCalls);
      })


      this.RepeatedCallsOutbound = {
        labels: Cnumber,
        datasets: [
          {
            backgroundColor: ['#FF8373', '#A3A0FB', '#FFDA83', '#55D8FE', '#44B980', '#42A5F5', '#BA59D6', '#657EB8'],
            borderColor: '#1E88E5',
            data: Count
          }
        ]
      }

      this.ApiLoad.SaveApiTime(model).subscribe();

      this.showLoader = false;
    })

  }


  SetCallTypeDropDown() {
    this.CallTypeSelectItem.push(
      {
        label: "In Coming", value: "In Coming"
      })

    this.CallTypeSelectItem.push(
      {
        label: "Out Going", value: "Out Going"
      })

    this.selectedCallType = "In Coming";

  }

  setCallNumberDropDown() {
    this.SelectedCallNumber = new Array<string>();
    this.CallNumberSelectItem = [];
    let CallNumber: Array<string> = new Array<string>();
    CallNumber = Array.from(new Set(this.SpectrumRepeatedCall.map(item => item.cnumber)));
    CallNumber.forEach(m => {
      this.CallNumberSelectItem.push(
        {
          label: m, value: m
        })
    })

  }

  getRepetedCallDetailsVolume() {
    //SpectrumRepeatedCall: Array
    //FilterSpectrumRepeatedCall:
    let startTime: number = new Date().getTime();
    this.showLoader = true;
    this.spectrumService.GetRepeatedCallersDetail(this.defaultselectedDate).subscribe(m => {

      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Spectrum Repeated Caller Number"
      model.time = ResponseTime

      this.SpectrumRepeatedCall = m
      this.setCallNumberDropDown();
      this.SetCallTypeDropDown();
      this.FilterCallDetailsVolume();
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    })

  }

  FilterCallDetailsVolume() {
    this.RepeatedCallPager = {};
    this.RepeatedCallPagedItems = [];

    this.FilterSpectrumRepeatedCall = this.SpectrumRepeatedCall;
    let filterByLocation: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();
    let filterByCNumber: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();

    if (this.selectedlocation && this.selectedlocation.length > 0) {
      this.selectedlocation.forEach(m => {
        let tempLocationfilter: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();
        tempLocationfilter = this.FilterSpectrumRepeatedCall.filter(x => x.agencyId === m)
        tempLocationfilter.forEach(z => {
          filterByLocation.push(z);
        })
      })
    }
    else {
      this.location.forEach(m => {
        let tempLocationfilter: Array<SpectrumRepeatedCall> = new Array<SpectrumRepeatedCall>();
        tempLocationfilter = this.FilterSpectrumRepeatedCall.filter(x => x.agencyId === m.agencyId)
        tempLocationfilter.forEach(z => {
          filterByLocation.push(z);
        })
      })
    }

    this.FilterSpectrumRepeatedCall = filterByLocation;

    this.FilterSpectrumRepeatedCall = this.FilterSpectrumRepeatedCall.filter(m => m.callType === this.selectedCallType);

    this.SelectedCallNumber.forEach(m => {
      let TempFilterByCnumber = this.FilterSpectrumRepeatedCall.filter(c => c.cnumber === m)
      TempFilterByCnumber.forEach(x => {
        filterByCNumber.push(x);
      })
    })

    if (filterByCNumber && filterByCNumber.length > 0) {
      this.FilterSpectrumRepeatedCall = filterByCNumber;
    }

    this.setRepeatedCallPage(1)
  }

  setRepeatedCallPage(page: number) {

    if (page < 1 || page > this.RepeatedCallPager.totalPages) {
      return;
    }

    // get pager object from service
    this.RepeatedCallPager = this._pageservice.getPager(this.FilterSpectrumRepeatedCall.length, page);

    // get current page of items
    this.RepeatedCallPagedItems = this.FilterSpectrumRepeatedCall.slice(this.RepeatedCallPager.startIndex, this.RepeatedCallPager.endIndex + 1);

  }

  onCallnumberChange() {
    this.FilterCallDetailsVolume();
  }

  onCallTypeChangeChange() {
    this.FilterCallDetailsVolume();
  }
}
