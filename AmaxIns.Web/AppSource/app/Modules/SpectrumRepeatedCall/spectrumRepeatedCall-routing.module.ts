import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';

import { SpectrumRepeatedCallComponent } from './spectrumRepeatedCall.component';


const routes: Routes = [
  { path: 'SpectrumRepeatedCall', component: SpectrumRepeatedCallComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SpectrumRepeatedCallRoutingModule { }
