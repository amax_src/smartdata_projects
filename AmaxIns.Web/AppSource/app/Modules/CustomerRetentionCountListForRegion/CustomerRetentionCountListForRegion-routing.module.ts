import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginGuard } from '../../login/login-guard.service';
import { CustomerRetentionCountListForRegionComponent } from './CustomerRetentionCountListForRegion.component';


const routes: Routes = [
  { path: 'CustomerRetentionCountListForRegion', component: CustomerRetentionCountListForRegionComponent, canActivate: [LoginGuard]}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomerRetentionCountListForRegionRoutingModule { }
