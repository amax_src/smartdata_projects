import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CommonService {


    dashboardState = {
        navbarToggle: false,
        sidebarToggle: true,
        sidebarMiniToggle: false
    };
    showLoader: boolean = false;
    pageHeader: string = "Dashboard";
    displayMode: number = 2;  //1=List View, 2=Chart View


    sidebarToggle(): void {
        this.dashboardState.sidebarToggle = !this.dashboardState.sidebarToggle;
    }

    sidebarMiniToggle(): void {
        this.dashboardState.sidebarMiniToggle = !this.dashboardState.sidebarMiniToggle;
    }

    navbarToggle(): void {
        this.dashboardState.navbarToggle = !this.dashboardState.navbarToggle;
    }
    navbarCollpas(): void {
        this.dashboardState.navbarToggle = true;
    }

    displayModeToggle(mode: number) {
        this.displayMode = mode;
    }
}

