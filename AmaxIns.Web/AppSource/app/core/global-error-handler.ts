import { ErrorDialogService } from './../shared/errors/error-dialog.service';
import { CommonService } from './common.service';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LoggingService } from '../Modules/Services/logging.service';
import { ErrorService } from '../Modules/Services/error.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector,
    private cmnSrv: CommonService) { }

  handleError(error: Error | HttpErrorResponse) {
    const errorService = this.injector.get(ErrorService);
    const logger = this.injector.get(LoggingService);
    const notifier = this.injector.get(ErrorDialogService);

    let message: string;
    let stackTrace: string;
    if (error instanceof HttpErrorResponse) {
      // Server error
      message = errorService.getServerErrorMessage(error);
      //stackTrace = errorService.getServerErrorStackTrace(error);
      //notifier.openDialog(message, error.status);
    } else {
      // Client Error
      message = errorService.getClientErrorMessage(error);
      //notifier.openDialog(message);
    }
    // Always log errors
    logger.logError(message, stackTrace);
    this.cmnSrv.showLoader = false;
  }
}
