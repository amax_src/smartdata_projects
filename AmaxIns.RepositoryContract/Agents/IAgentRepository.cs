﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Agents
{
    public interface IAgentRepository
    {
        Task<IEnumerable<string>> GetAgents();
    }
}
