﻿using AmaxIns.DataContract.PBX;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.PBX
{
   public interface IPbxRepository : IBaseRepository
    {
        Task<IEnumerable<string>> GetMonth();
        Task<IEnumerable<string>> GetExtention();
        Task<IEnumerable<string>> GetDate();
        Task<IEnumerable<PbxModel>> GetPBXData();
        Task<IEnumerable<PbxModel>> GetPBXDailyData();
    }
}
