﻿using AmaxIns.DataContract.Budget;
using AmaxIns.DataContract.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Budget
{
    public interface IBudgetRepository : IBaseRepository
    {
        Task<IEnumerable<string>> GetBudgetCategories(BudgetSearch obj);
        Task<IEnumerable<string>> GetBudgetDept(int userId);
        Task<IEnumerable<string>> GetBudgetBudgetName(BudgetSearch obj);
        Task<BudgetModel> GetBudgetByMonth(BudgetFormModel budget);
        Task<IEnumerable<BudgetModel>> GetBudgetAnnual(BudgetFormModel budget);
        Task<IEnumerable<BudgetTransactionModel>> GetBudgetTrx(BudgetFormModel budget);
        Task<IEnumerable<BudgetOverspendModel>> GetBudgetSpending(BudgetFormModel budget);

        Task<IEnumerable<string>> GetBudgetUser(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetLocation(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetSource(BudgetSearch budgetSearch);
        Task<IEnumerable<string>> GetBudgetCode(BudgetSearch budgetSearch);
        Task<IEnumerable<BudgetTrancModel>> GetBudgetTranc(BudgetFormModel budget);
        Task<IEnumerable<BudgetSummaryModel>> GetBudgetSummary(BudgetFormModel budget);
        ResponseViewModel UploadActualExcelFile(List<ActualTable> actualTables);
        Task<IEnumerable<string>> DeleteActualFileExestingData(int month, int year);
        ResponseViewModel UploadBudgetExcelFile(List<BudgetTable> budgetTables);
        Task<IEnumerable<string>> DeleteBudgetFileExestingData(int month, int year);
        Task<IEnumerable<string>> GetLatestMonthValue(int year);
    }

}