﻿using AmaxIns.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Budget
{
    public interface IBudgetAuthenticationRepository
    {
        Task<LoginUser> LogInAsync(LoginModel login);
    }
}
