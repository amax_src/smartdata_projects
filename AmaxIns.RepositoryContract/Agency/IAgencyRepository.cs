﻿using AmaxIns.DataContract.Agency;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Agency
{
    public interface IAgencyRepository
    {
        Task<IEnumerable<AgencyModel>> Get();
        Task<IEnumerable<AgencyModel>> GetSaleOfDirector(int SaleDirectorId);
        Task<IEnumerable<AgencyModel>> Get(int RegionalManagerId);
        Task<IEnumerable<AgencyModel>> Get(int RegionalManagerId,int ZonalManagerId);
        Task<IEnumerable<AgencyModel>> GetbyZonalManager(int ZonalManager);
        Task<IEnumerable<AgencyLatLong>> GetAgencyAndLatLong();
        Task<IEnumerable<AgencyModel>> GetBSMAgency(int BsmId);
        Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id);
    }
}
