﻿using System.Collections.Generic;
using AmaxIns.DataContract.User;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.User
{
    public interface ISaleDirectorRepository
    {
        Task<SaleDirectorModel> GetById(int Id);
        Task<IEnumerable<SaleDirectorModel>> GetAll();
    }
}

