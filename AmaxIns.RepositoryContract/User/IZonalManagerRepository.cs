﻿using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.User
{
    public interface IZonalManagerRepository
    {
        Task<ZonalManagerModel> GetById(int Id);
        Task<IEnumerable<ZonalManagerModel>> GetAll();
        Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id);
    }
}
