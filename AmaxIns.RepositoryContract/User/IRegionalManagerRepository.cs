﻿using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.User
{
    public interface IRegionalManagerRepository
    {
        Task<RegionalManagerModel> GetById(int Id);
        Task<IEnumerable<RegionalManagerModel>> GetAll();
        Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id);
    }
}
