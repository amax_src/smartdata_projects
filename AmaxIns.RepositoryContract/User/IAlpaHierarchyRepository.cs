﻿using System.Collections.Generic;
using AmaxIns.DataContract.User;
using System.Threading.Tasks;
using AmaxIns.DataContract.Agency;

namespace AmaxIns.RepositoryContract.User
{
    public interface IAlpaHierarchyRepository
    {
        Task<IEnumerable<SaleDirectorModel>> GetAllAlpaSaleDirector();
        Task<IEnumerable<RegionalManagerModel>> RegionalManagerALPA();
        Task<IEnumerable<ZonalManagerModel>> ZonalManagerALPA();
        Task<IEnumerable<AgencyModel>> GetAllAgency();
        Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id);
        Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id);
        Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(int RegionalManagerId, int ZonalManagerId);
        Task<IEnumerable<AgencyModel>> GetbyZonalManager(int ZonalManager);
        Task<IEnumerable<AgencyModel>> GetAllLocationsRegional(int RegionalManagerId);
        Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id);
    }
}
