﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.Quotes;

namespace AmaxIns.RepositoryContract.HourlyLoad
{
    public interface IHourlyLoadRepository : IBaseRepository
    {
        Task<IEnumerable<HourlyLoadModel>> GetData(string date, int agencyId);
        Task<IEnumerable<string>> GetDates(int month, string year);
        Task<IEnumerable<MonthModel>> GetMonth(string year);
        Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string date, int agencyId);
        Task<IEnumerable<HourlyLoadModel>> GetData(string startdate, string enddate);
        Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string startdate, string enddate);
        Task<IEnumerable<PeakHoursModel>> GetPeakHoursData(string startdate, string enddate);
        Task<IEnumerable<AgentCountModel>> GetAgentCountPerhour(string date, int agencyId);
        Task<IEnumerable<AgentCountModel>> GetAgentCountPerhour(string startdate, string enddate);
     }
}
