﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.AlphaTop5Agent;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.AlphaTop5Agent
{
    public interface ITopAgentRepository : IBaseRepository
    {
        Task<IEnumerable<AgencyModel>> GetAgency();
        Task<IEnumerable<int>> GetYear();
        Task<IEnumerable<TopFiveDataModel>> GetPremiumData(string agency, string year, string month);
        Task<IEnumerable<TopFiveDataModel>> GetAgencyFeeData(string agency, string year, string month);
        Task<IEnumerable<TopFiveDataModel>> GetPolicyCount(string agency, string year, string month);
    }
}
