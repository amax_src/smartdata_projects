﻿using AmaxIns.DataContract.AMAX30DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.AMAX30DB
{
    public interface IAgencyInfoRepository
    {
        Task<IEnumerable<AMAX30AncyInfoModel>> GetAgencyInfo(int agencyId, string agencyName);
        Task<IEnumerable<Amax30AgentInfoModel>> GetAgentInfo(string agentName, string email);
        Task<IEnumerable<AMAX30ClientInfoModel>> GetClientInfo(CountListClientInfoQueryModel countListClientInfoQueryModel);
        Task<IEnumerable<AMAX30PaymentInfoModel>> GetClientPaymentInfo(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel);
    }
}
