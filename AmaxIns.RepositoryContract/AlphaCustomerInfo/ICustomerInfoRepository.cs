﻿using AmaxIns.DataContract.AplhaCustomerInfo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.AlphaCustomerInfo
{
    public interface ICustomerInfoRepository : IBaseRepository
    {
        Task<IEnumerable<string>> GetAgencyName();
        Task<IEnumerable<string>> GetclientName(string SearchText);
        Task<IEnumerable<string>> GetCity();
        Task<IEnumerable<string>> GetPolicyType();
        Task<IEnumerable<string>> GetPolicyNumber(string SearchText);
        Task<IEnumerable<CustomerInfoModel>> CustomerInfoData();
    }
}
