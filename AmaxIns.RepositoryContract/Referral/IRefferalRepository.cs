﻿using AmaxIns.DataContract.Referral;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Referral
{
    public interface IRefferalRepository
    {
        int AddReferral(ReferralModel referralModel);
        int AddReferralByAgent(AgentRefModel referralModel);

        IEnumerable<string> GetAgents();

        string ValidateEmailAddress(string Email);
        string ValidatePhoneNumber(string PhoneNumber);
        IEnumerable<ReferralReportModel> ReferralReport(int month, int year, string type, string zip, int? agencyId);

        IEnumerable<CommentsModel> GetComments(int ReferenceId);
        void Addcomments(CommentsModel comments);
        string ValidateZip(string zip);
        IEnumerable<string> GetAgencyEmail(string agencyIds);
        IEnumerable<ReferralReportModel> ReferralTouchedUnTouchedReport(int month, bool isTouched = false);

        void AddTurboratorcomments(CommentsModel comments);
        IEnumerable<CommentsModel> GetTurboratorComments(int turboraterId);
    }

}
