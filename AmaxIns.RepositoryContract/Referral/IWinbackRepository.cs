﻿using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.SearchFilter;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Referral
{
    public interface IWinbackRepository
    {
         IEnumerable<WinbackModel> WinbackReport(List<int> month, int year, string monthstr, int agencyId, string searchKeyWord, string direction = "desc", string sortkeyword = "id", int pageNumber = 1, bool sorting = false);
        IEnumerable<WinbackCommentsModel> GetComments(int ReferenceId);
        void Addcomments(WinbackCommentWrapper comments);
        WinbackCommentWrapper GetCommentWrapper(int ReferenceId);
        int WinbackReportPageCount(List<int> month, int year,string monthstr,int agencyId,string search, bool sorting = false);
        int GetLatestMonthDataAvaliable(int agencyId);
        void UpdateWinBack(WinbackModel model, int agencyId);
        IEnumerable<WinbackModel> WinbackReportCsv(string monthstr, int year);
        void UpdateSearchFilter(SearchFilterModel search);
        IEnumerable<WinbackAPIModel> GetWinBackDataForAPI(int monthId, int year);
    }
}
