﻿using AmaxIns.DataContract.Referral.Turborator;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.Referral
{
    public interface ITurboratorRepository : IBaseRepository
    {
        int SaveTurboratorData(TurboratorModel model);
        int SaveGaragingAddressData(GaragingAddress model, int TurboratorID, int CarId);
        int SaveDriverData(DriverModel model, int TurboratorID);
        int SaveCustomerData(CustomerModel model ,int TurboratorID);
        int SaveCoverageData(CoverageModel model, int TurboratorId);
        int SaveCarData(CarModel model,  int TurboratorId);
        int SaveAddress2Data(Address2 model, int TurboratorID, int DriverId);
        int SaveAddressData(AddressModel model, int CustomerID,int TurboratorID);

        IEnumerable<TurboratorModel> TurboratorReferralReport(int month, int year, int agencyId);
        IEnumerable<CoverageModel> TurboratorCoverage(int id);



    }
}
