﻿using AmaxIns.DataContract.Referral.AgencyLogin;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.DataContract.Referral.Token;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.Referral
{
    public interface IAgencyLoginRepository
    {
        AgencyLoginModel validateAgency(AgencyLoginModel model);
        AgentLoginModel validateAgent(AgentLoginModel model);
        SearchFilterModel GetSearchParameters(AgentLoginModel model);
        void UpdateToken(TokenModel token);
        TokenModel GetToken(TokenModel model);
    }
}
