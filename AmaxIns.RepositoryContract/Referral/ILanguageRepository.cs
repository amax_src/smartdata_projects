﻿using AmaxIns.DataContract.Referral.Language;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Referral
{
   public interface ILanguageRepository
    {
        Task<IEnumerable<LanguageModel>> GetLanguage();
        Task<IEnumerable<ResourceValueModel>> GetLanguageResoure(int id, string section);
    }
}
