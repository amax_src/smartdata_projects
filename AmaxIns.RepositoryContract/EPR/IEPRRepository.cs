﻿using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.EPR
{

    public interface IEPRRepository : IBaseRepository
    {
        Task<IEnumerable<EPRModel>> GetEPRData();
        Task<IEnumerable<string>> GetEPRmonths();
        Task<IEnumerable<EPRModel>> GetEPRTotalData();
        Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string year, string location, string _selectedagent);
    }

}
