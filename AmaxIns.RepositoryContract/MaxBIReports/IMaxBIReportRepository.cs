﻿using AmaxIns.DataContract.MaxBIReports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.MaxBIReports
{
    public interface IMaxBIReportRepository
    {
        Task<IEnumerable<CatalogInfo>> GetCatalogs(Request request);
        Task<IEnumerable<HierarchyInfo>> GetHierarchyLevel(HierarchyRequest request);
        Task<IEnumerable<AgentCarrierAFeePremium>> GetAgentCarrierAFeePremium(Request request);
        Task<IEnumerable<AgencyPerPolicy>> GetAFeePremiumPerPolicy(Request request);
        Task<decimal> GetAFeePremiumPerPolicySummary(Request request);
        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotes(Request request);
        Task<decimal> GetNewModifiedQuotesSummary(Request request);
        Task<IEnumerable<InboundOutboundCalls>> GetInboundOutboundCalls(Request request);
        Task<decimal> GetInboundOutboundCallsSummary(Request request);

        Task<IEnumerable<PayrollFilterData>> GetPayrollDashboardFilterData(PayrollRequest request);
        Task<IEnumerable<DataContract.MaxBIReports.Payroll>> GetPayrollDashboardData(PayrollRequest request);

        Task<IEnumerable<EmployeeInfo>> GetmployeeSensitiveInfo(Request request);

        Task<IEnumerable<HourlyProduction>> GetAgentHourlyProduction(Request request);
        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPR(Request request);

        Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPRGraph(Request request);
    }
}
