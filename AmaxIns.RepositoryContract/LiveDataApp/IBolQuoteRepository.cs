﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.LiveDataApp
{
    public interface IBolQuoteRepository : IBaseRepository
    {
        Task<IEnumerable<BOLQuoteData>> GetDataBolData(List<string> date,string state);
        //Task<Address> GetDataBolCustAddressData(int id);

        //Task<Customer> GetDataBolCustData(int id);
        Task<BOLQuoteData> GetSingleQuote(string quoteNumber);

        Task<IEnumerable<BOLQuoteData>> GetDataBolData_Quotes(List<string> date, string state);
        Task<IEnumerable<EODLive>> GetEODLiveReport(List<string> date);
        Task<IEnumerable<CustomerJourneyInfo>> CustomerJourneyInfoReport(List<string> date);
        Task<IEnumerable<RetailCustomerJourneyInfo>> GetRetailCustomerJourneyInfoReport(List<string> date);
    }
}
