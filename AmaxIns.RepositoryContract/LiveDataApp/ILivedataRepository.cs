﻿using AmaxIns.DataContract.LiveData;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.LiveDataApp
{
    public interface ILivedataRepository : IBaseRepository
    {
        LiveDataRoot GetData(string url);
        Task<IEnumerable<QuotesData>> GetQuotes();
        Task<IEnumerable<AgencyDIDData>> GetConsolidatedDIDData(string day,string month,string year, string endday, string endmonth, string endyear, string Did);
        List<ConsolidatedAgentCount> GetConSolidatedAgentloginData();
        List<ConsolidatedAgentCount> GetAgencywiseAgentloginData();
    }
}
