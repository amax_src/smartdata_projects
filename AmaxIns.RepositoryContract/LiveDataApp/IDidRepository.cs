﻿using AmaxIns.DataContract.LiveData.DidConsolatedData;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.LiveDataApp
{
   public interface IDidRepository : IBaseRepository
    {
        List<DidNumberData> GetSummaryDIDData(string sday, string smonth, string syear, string eday, string emonth, string eyear);
    }
}
