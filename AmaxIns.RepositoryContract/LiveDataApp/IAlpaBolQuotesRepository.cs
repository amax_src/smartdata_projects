﻿using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.LiveDataApp
{
    public interface IAlpaBolQuotesRepository : IBaseRepository
    {
        Task<IEnumerable<BOLQuoteData>> GetAlpaDataBolData_Quotes(List<string> date, string state);
    }
}
