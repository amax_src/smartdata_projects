﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.LiveDataApp
{
    public interface ISpectrumLiveDataRepository:IBaseRepository
    {
        Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveData();
        //Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataV1();
        //Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWise();
        //Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWise(int weeklyId);

        Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseV2();
        Task<IEnumerable<SpectrumLiveHourlyModel>> GetStatsLiveDataAgentWiseHourlyV2();
        Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataV2();
        Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWiseV2(SpectrumParam date);
        Task<IEnumerable<SpectrumAgentCall>> GetSpectrumAgentCallDetails(string extension);
        Task<IEnumerable<AgencyAgentLogLive>> GetSpectrumLiveAgentLogV2();
        Task<IEnumerable<SpectrumAgentExtensionLog>> GetSpectrumLiveAgentExtensionLogV2(string extension);
        Task<IEnumerable<AgentCallTransfer>> GetCallTransferDetails(string extension, int AgencyId);
        Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseVSTeam_selV2();
        Task<IEnumerable<SpectrumAgentCall>> GetSpectrumExtensionWiseCallDetails(StratusExtensionParam stratusExtension);
        Task<IEnumerable<SpectrumLiveData>> GetSpectrumLiveDataCAV2();
        Task<IEnumerable<AgentCallTransfer>> GetCallTransferDetailsCA(string extension, int AgencyId);
        Task<IEnumerable<SpectrumLiveData>> GetStatsLiveDataAgentWiseCAV2();
        Task<IEnumerable<AgencyAgentLogLive>> GetSpectrumLiveAgentLogCAV2();
        Task<IEnumerable<SpectrumLiveData>> GetStatsWeeklyDataAgentWiseCAV2(SpectrumParam date);
        Task<IEnumerable<SpectrumAgentCall>> GetSpectrumExtensionWiseCallDetailsCA(StratusExtensionParam stratusExtension);


    }
}
