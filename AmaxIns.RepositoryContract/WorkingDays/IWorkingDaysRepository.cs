﻿using AmaxIns.DataContract.WorkingDays;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.WorkingDays
{
    public interface IWorkingDaysRepository
    {
        Task<IEnumerable<WorkingdayModel>> GetWorkingDaysOfMonth();
        Task<int> GetTotalWorkingDaysOfMonth(int month, int year);
    }
}
