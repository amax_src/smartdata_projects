﻿using AmaxIns.DataContract.Calendar;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Calendar
{
    public interface ICalendarRepository
    {
        Task<IEnumerable<CalendarEvents>> GetEvents(int agencyId, string month);
        Task<IEnumerable<CalendarLedgerDetails>> GetEventLedgerDetail(int eventId);
        int AddEvents(CalendarEvents addEvent);
        void AddLedgerDetails(CalendarLedgerDetails ledgerDetails, int eventId);
    }
}
