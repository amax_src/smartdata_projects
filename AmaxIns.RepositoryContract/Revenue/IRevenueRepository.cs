﻿using AmaxIns.DataContract.Revenue;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Revenue
{
    public  interface IRevenueRepository : IBaseRepository
    {
        Task<IEnumerable<RevenueModel>> GetRevenueData();
        Task<string> GetDataRefreshDate();
        Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData();
        Task<IEnumerable<TierPercentageModel>> GetTierPercentage();
        Task<IEnumerable<string>> Getmonths(string year);

        Task<IEnumerable<RevenuTodayModel>> GetRevenuToday();

        Task<IEnumerable<AmPmDepositModel>> GetAMPMDeposit();
    }
}
