﻿using AmaxIns.DataContract.Planner;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Planner
{
    public interface IPlannerRepository
    {
        Task<IEnumerable<PlannerActivity>> GetAllActivity();
        Task<IEnumerable<PlannerHour>> GetAllHours();
        Task<int> SaveUpdatePlanner(DataTable dataTable);
        Task<int> DeleteBudgetPlanner(int plannerBudgetId);
        Task<IEnumerable<PlannerBudget>> GetAllPlanner(int agencyId, string monthName);
        Task<IEnumerable<ActualPlanner>> GetActualPlanner(int agencyId, string monthName, string year);
        Task<PlannerActualDetails> GetSingelActual(int plannerBudgetId);
        Task<int> SaveUpdateActual(PlannerActualDetails plannerActualDetails);
        Task<IEnumerable<PlannerDashboard>> GetDashboardReport(string monthName, string year);
        Task<IEnumerable<DailyView>> GetDailyViewReport(string monthName, int agencyId, string year);
        Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityTotal(string monthName, string year, string agencyId);
        Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityAgencyWise(string monthName, string year);
        Task<IEnumerable<DailyView>> GetDataForWeekly(QueryPlanner queryPlanner);
    }
}
