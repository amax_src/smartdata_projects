﻿using AmaxIns.DataContract.Quotes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Quotes
{
    public interface IQuotesRepository : IBaseRepository
    {
        Task<IEnumerable<string>> GetPaydate(string year, string month);
        Task<IEnumerable<QuotesModel>> GetBoundQuotes(string paydate);

        Task<IEnumerable<QuotesModel>> GetBoundNoTurboratorQuotes(string paydate);
        Task<IEnumerable<QoutesHistory>> GetQuotesHistory();
        Task<IEnumerable<ModifiesQuotesReport>> GetModifiedQuotesReport();
        Task<IEnumerable<QuotesSale>> GetQuotesSale();
        Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotes(List<string> Agencyids, string month = null,string year=null);
        Task<IEnumerable<NewModifiedQuotes>> GetNewAndModifiedData_New(QuotesParam quotesParam, string month = null, string year = null);
        Task<IEnumerable<QuotesProjection>> GetQuotesProjection();
        Task<IEnumerable<NewModifiedQuotes>> GetDistinctDateQuotes(string month = null, string year = null);
        Task<IEnumerable<NewAndModifiedQuotesProj>> GetNewModiFiedQuotesProjection(string month = null, string year = null);
        Task<int> ModifiedQuotesSoldTiles(QuotesParam quotesParam, string month = null, string year = null);
    }
}
