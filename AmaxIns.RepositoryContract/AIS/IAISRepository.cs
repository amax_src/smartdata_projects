﻿using AmaxIns.DataContract.AIS;
using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.AIS
{

    public interface IAISRepository : IBaseRepository
    {
        Task<IEnumerable<AISModel>> GetAISData();
        Task<IEnumerable<AISModel>> GetAISDataCsv();
    }
}
