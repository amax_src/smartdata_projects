﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.Notification
{
   public interface INotificationRepository : IBaseRepository
    {
        string GetNotification();
    }
}
