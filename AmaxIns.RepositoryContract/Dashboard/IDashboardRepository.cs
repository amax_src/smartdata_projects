﻿using AmaxIns.DataContract.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Dashboard
{
    public interface IDashboardRepository : IBaseRepository
    {
        Task<IEnumerable<DashboardQuotesModel>> GetQuoteData(int agencyid,int month,int year);
        Task<IEnumerable<DashboardRevenueModel>> GetSalesData(int agencyid, int month, int year);
        Task<IEnumerable<DashboardEPRModel>> GetEprData(int agencyid, int month, int year);
        Task<IEnumerable<DashboadSpectrumModel>> GetCallsData(int agencyid, int month, int year);
        Task<IEnumerable<DashboardPayrollModel>> GetPayrollBudgetOvertime(int agencyid, int month, int year);
        Task<IEnumerable<DashboardPayrollModel>> GetPayrollActualData(int agencyid, int month, int year);
        Task<IEnumerable<DashboardPayrollModel>> GetPayrollBudgeData(int agencyid, int month, int year);
        Task<IEnumerable<DashboardPayrollModel>> GetPayrollOvertimeData(int agencyid, int month, int year);
        Task<IEnumerable<DashboardEPRModel>> GetEprDashboardDaily(int agencyid, int month, int year);
        Task<IEnumerable<DashboardEPRModel>> GetEprlDashboardWeekly(int agencyid, int month, int year);


    }
}
