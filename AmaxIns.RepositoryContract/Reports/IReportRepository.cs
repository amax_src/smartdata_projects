﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Reports
{
    public interface IReportRepository
    {
        Task<IEnumerable<AvgAgencyFee>> GetAvgAgencyFees(string Year, string month, string location);
        Task<IEnumerable<AvgAgencyFeeII>> GetAvgAgencyFeesII(string Year, string month, string location);
        Task<IEnumerable<AvgAgencyFeeIII>> GetAvgAgencyFeesIII(string Year, string month, string location);

        // Agent Performance Report Methods
        Task<IEnumerable<AgencyBreakdownI>> GetAgencyBreakdownI(string Year, string month, string location);
        Task<IEnumerable<AgencyBreakdownII>> GetAgencyBreakdownII(string Year, string month, string location);
        Task<IEnumerable<AgencyBreakdownIII>> GetAgencyBreakdownIII(string Year, string month, string location);
        

        // Overtime
        Task<IEnumerable<OvertimeI>> GetOvertimeI(string Year, string month,string  location);
        Task<IEnumerable<OvertimeII>> GetOvertimeII(string Year, string month, string location);
        Task<IEnumerable<OvertimeIII>> GetOvertimeIII(string Year, string month, string location);

        // Tracker

        Task<IEnumerable<TrackerRender>> GetRenterTracker(string Year, string month,string location);
        Task<IEnumerable<TrackerProduction>> GetProductionTracker(string Year, string month, string location);
        Task<IEnumerable<TrackerActiveCustomer>> GetActiveCustomerTracker(string Year, string month, string location);

        // Agent Performance
        Task<IEnumerable<AgentPerformance>> GetAgentPerformaceI(string Year, string month, string location);
        Task<IEnumerable<AgentPerformance>> GetAgentPerformaceII(string Year, string month, string location);
    }
}
