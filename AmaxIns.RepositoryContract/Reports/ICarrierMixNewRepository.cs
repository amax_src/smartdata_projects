﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Reports
{
   public interface ICarrierMixNewRepository
    {
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixI(int Year, string month);
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixPremium(int Year, string month);
        Task<IEnumerable<IDictionary<string, object>>> CarrierMixAgencyFee(int Year, string month);
    }
}
