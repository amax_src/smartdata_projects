﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Reports
{
    public interface IUniqueCreatedQuotesRepository
    {
        Task<IEnumerable<UniqueCreatedQuotes>> GetUniqueCreatedQuotes(int Year, string month,string location);
        Task<IEnumerable<Market>> GetAllMarket();
    }
}
