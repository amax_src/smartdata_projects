﻿using AmaxIns.DataContract.Reports;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Reports
{
    public interface IRentersReportRepository
    {
        Task<IEnumerable<RentersData>> GetRenterData(string Year, string month,string location);
        Task<IEnumerable<ActiveCustomerData>> GetActiveCustomerData(string Year, string month, string location);
    }
}
