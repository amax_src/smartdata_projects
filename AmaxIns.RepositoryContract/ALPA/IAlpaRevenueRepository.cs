﻿using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.ALPA
{
    public interface IAlpaRevenueRepository : IBaseRepository
    {
        Task<IEnumerable<RevenueModel>> GetAlpaRevenueData();

        Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData();
        Task<IEnumerable<RevenuTodayModel>> GetRevenuToday();
        Task<IEnumerable<DailyTransactionPayments>> DailyTransactionPayments(QuotesParam quotesParam, string month = null, string year = null);

    }
}
