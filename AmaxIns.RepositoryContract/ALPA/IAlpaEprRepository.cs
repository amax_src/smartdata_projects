﻿using AmaxIns.DataContract.EPR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.ALPA
{
    public interface IAlpaEprRepository : IBaseRepository
    {
        Task<IEnumerable<EPRModel>> GetEPRData();
        Task<IEnumerable<EPRModel>> GetEPRTotalData();
        Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string _year, string location, string _selectedagent);
        Task<IEnumerable<string>> GetEPRmonths();
    }
}
