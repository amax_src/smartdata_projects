﻿using AmaxIns.DataContract.AIS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.ALPA
{
    public interface IAISAlpaRepository : IBaseRepository
    {
        Task<IEnumerable<AISModel>> GetAISData();
    }
}
