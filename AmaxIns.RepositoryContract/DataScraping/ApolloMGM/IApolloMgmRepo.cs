﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.DataScraping.ApolloMGM
{
    public interface IApolloMgmRepo : IBaseRepository
    {
        IEnumerable<ApolloMgmModel> GetApolloData(int currentPage, out int TotalRecords,int AgencyId );

        IEnumerable<ApolloMgmModel> GetApolloDataExpiring(int currentPage, out int TotalRecords, int AgencyId);
        IEnumerable<ApolloMgmModel> GetApolloDatadueDate(int currentPage, out int TotalRecords, int AgencyId);
        IEnumerable<ApolloCommissionsModel> GetApolloCommissions(int currentPage, out int TotalRecords, int AgencyId);

    }
}
