﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.DataScraping.ApolloMGM
{
    public interface IApolloPaymentRepository : IBaseRepository
    {
        IEnumerable<ApolloLiveModel> GetApolloPaymentCarrierData();
    }
    
}
