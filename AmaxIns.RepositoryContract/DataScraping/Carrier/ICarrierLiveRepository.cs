﻿using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using AmaxIns.DataContract.DataScraping.Seaharbor;
using System.Collections.Generic;

namespace AmaxIns.RepositoryContract.DataScraping.Aspen
{
    public interface ICarrierLiveRepository : IBaseRepository
    {
        IEnumerable<AspenLiveModel> GetAspenLiveData();
        IEnumerable<SeaharborLiveModel> GetSeaharborLiveData();
    }
}
