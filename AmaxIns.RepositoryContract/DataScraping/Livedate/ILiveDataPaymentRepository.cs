﻿using AmaxIns.DataContract.DataScraping.InsurancPro;
using System.Collections.Generic;

namespace AmaxIns.RepositoryContract.DataScraping.Livedate
{
    public interface ILiveDataPaymentRepository : IBaseRepository
    {
        IEnumerable<IPModel> GetIPData(string Carrier);
    }
}
