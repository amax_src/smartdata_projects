﻿using AmaxIns.DataContract.CustomerRetention;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.CustomerRetention
{
    public interface ICustomerRetentionRepository : IBaseRepository
    {
        Task<IEnumerable<CustomerRetentions>> GetStoreManagersReports(string startDate, string endDate, int storeManagerId, int AgencyId);
        Task<IEnumerable<CustomerRetentionCountForZone>> StoreManagerCustomerRetentionCountForZone(string startDate, string endDate, int storeManagerId);
        Task<IEnumerable<CustomerRetentionCountForZone>> ZonalManagerCustomerRetentionCountForZone(string startDate, string endDate, int ZMID, string agencyId);
        Task<IEnumerable<CustomerRetentionCountForRegion>> RegionalManagerCustomerRetentionCount(string startDate, string endDate, int RMID, string agencyId);
        Task<IEnumerable<CustomerRetentionCountForHod>> HODCustomerRetentionCount(string startDate, string endDate,string agencyId);
    }
}
