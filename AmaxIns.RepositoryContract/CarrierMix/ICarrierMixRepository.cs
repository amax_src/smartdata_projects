﻿using AmaxIns.DataContract.CarrierMix;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.CarrierMix
{
   public  interface ICarrierMixRepository
    {
        Task<IEnumerable<string>> GetCarrier(string Month, string year);
        Task<IEnumerable<string>> GetDates(string Month, string year);
        Task<IEnumerable<CarrierMixModel>> GetCarrierMixData(string date, string agency, string month, string Carrier,string year);
    }
}
