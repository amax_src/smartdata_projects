﻿using AmaxIns.DataContract.APILoad;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.RepositoryContract.APILoad
{
    public interface IAPILoadRepository
    {
        void Save(ApiLoadModel model);
    }
}
