﻿using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Payroll
{
    /// <summary>
    ///  Repository Contract to work with PayrollRepository.
    /// </summary>
    public interface IPayrollRepository : IBaseRepository
    {
        /// <summary>
        /// IPayrollRepository is hiding(using keyword "new") the IBaseRepository's Healthcheck to enable call to PayrollRepository's HealthCheck.
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        new Task<object> HealthCheckAsync();

        /// <summary>
        /// Get a list of users.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<UserModel>> GetUserListAsync();

        Task<IEnumerable<PayRollModel>> GetPayRoleBudgetedData();
        Task<IEnumerable<PayRollModel>> GetPayRoleActualData();
      
        Task<ConsolidatedTileData> GetPayRoleTilesConsolidatedData();
    }
}
