﻿using AmaxIns.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.RepositoryContract.Authentication
{
    public interface IAuthenticationRepository
    {
        Task<LoginUser> LogInAsync(LoginModel login);
        void UpdatePassword(ResetPassword login);
        Task<LoginUser> ValidateUserName(string email, string LoginType);
        Task<string> RecoverPassword(string email, string LoginType);

        void LogUser(LoginUser login);

        Task<DesktopLoginUser> LogInDesktopAsync(string userName, string password);
        Task<Tuple<List<UserFeatureDesktop>, List<UserAgencyDesktop>>> GetAgencyFeatureAsync(int Userid);

        Task<LoginUser> LogInAsyncUsingOTPRequest(LoginModel login);
    }
}
