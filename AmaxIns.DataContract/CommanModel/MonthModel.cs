﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CommanModel
{
   public class MonthModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool Selected { get; set; }
    }
}
