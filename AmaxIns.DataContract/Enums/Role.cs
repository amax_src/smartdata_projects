﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Enums
{
    public enum Role
    {
        headofdepratment,
        regionalmanager,
        zonalmanager,
        bsm,
        storemanager,
        chieffinancialofficer,
        chiefexecutiveofficer,
        chiefOperatingofficer,
        saledirector
    }
}
