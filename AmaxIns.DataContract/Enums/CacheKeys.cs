﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Enums
{
    public enum CacheKeys
    {
        PayRoleBudgetedActualData,
        GetEPRData,
        GetEPRTotalData,
        AISData,
        SpectrumDailyDetail,
        RevenueMonthly,
        RevenueDaily,
        CallVolume,
        TotalCallSAll,
        TotalCallSAllOutBound,
        RepeatedCallers,
        EPRMonth,
        PayrollTiles,
        PBX,
        PBXDaily,
        CustomerInfoAlpha,
        SpectrumDailyDates,
        LiveData,
        LiveDataRefreshTime,
        QuotesSale,
        SpectrumLiveData,
        SpectrumLiveDataRefreshTime,
        AmaxLiveData,
        AmaxLiveDataAgencyWise,
        BindOnLineQuotes,
        SpectrumLiveDataV1,
        StatsLiveDataAgentWise,
        SpectrumLiveDataV2,
        StatsLiveDataAgentWiseV2,
        StatsLiveDataAgentLogV2,
        StratusVSTeam,
        SpectrumLiveDataCAV2,
        StatsLiveDataAgentWiseCAV2,
        StatsLiveDataAgentLogCAV2
    }
}
