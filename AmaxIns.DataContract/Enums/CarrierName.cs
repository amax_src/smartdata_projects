﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Enums
{
    public enum CarrierName
    {
        alinsco,
        assuranceamerica,
        commonwealthgeneral,
        americanaccess,
        seaharbor,
        unitedauto,
        lamargeneralagency,
        stonegate,
        evolution,
        bristolwest,
        unique
    }
    public enum AlpaCarrierName
    {
        commonwealthgeneral,
        americanaccess,
        seaharbor,
        unitedauto
    }
}
