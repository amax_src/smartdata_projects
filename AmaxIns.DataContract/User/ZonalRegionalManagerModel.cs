﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.User
{
    public class ZonalRegionalManagerModel
    {
        public List<int> Rmid { get; set; }

        public List<int> Zmid { get; set; }
    }
}
