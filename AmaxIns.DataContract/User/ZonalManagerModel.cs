﻿using System;

namespace AmaxIns.DataContract.User
{
    public class ZonalManagerModel
    {
        public int ZonalManagerId { get; set; }
        public string ZonalManager { get; set; }
        public int? EmployeeId { get; set; }

    }
}
