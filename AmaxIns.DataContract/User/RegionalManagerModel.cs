﻿namespace AmaxIns.DataContract.User
{
    public class RegionalManagerModel
    {
        public int RegionalManagerId { get; set; }
        public string RegionalManager { get; set; }
        public int? EmployeeId { get; set; }
    }
}
