﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.User
{
    public class SaleDirectorModel
    {
        public int SaleDirectorID { get; set; }
        public string SaleDirector { get; set; }
        public int? EmployeeId { get; set; }
    }
}
