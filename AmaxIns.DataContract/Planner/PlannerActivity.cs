﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Planner
{
    public class PlannerActivity
    {
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
    }

    public class PlannerHour
    {
        public int PlannerHoursId { get; set; }
        public decimal PlannerHours { get; set; }
    }
    public class PlannerBudget
    {
        public int PlannerBudgetId { get; set; }
        public int AgencyId { get; set; }
        public string PlannerDate { get; set; }
        public string Activity { get; set; }
        public string OtherActivity { get; set; }
        public decimal PlannedHours { get; set; }
        public string PlannedDetails { get; set; }
        public decimal PlannedBudgetAmount { get; set; }
        public int LoginUserId { get; set; }
    }
    public class ActualDetail
    {
        public int PlannerActualId { get; set; }
        public int PlannerBudgetId { get; set; }
        public decimal ActualHours { get; set; }
        public bool IsWent { get; set; }
        public string ActualDetails { get; set; }
        public decimal ActualAmount { get; set; }
        public int LoginUserId { get; set; }
    }
    public class ActualPolicyDetails
    {
        public int PolicyDetailId { get; set; }
        public int PlannerBudgetId { get; set; }
        public string PolicyNumber { get; set; }
        public decimal AgencyFee { get; set; }
        public decimal PremiumAmount { get; set; }
        public bool IsVerifyed { get; set; }
        public int LoginUserId { get; set; }
    }

    public class ActualPlanner
    {
        public int PlannerBudgetId { get; set; }
        public int AgencyId { get; set; }
        public string PlannerDate { get; set; }
        public string Activity { get; set; }
        public string OthersActivity { get; set; }
        public decimal PlannedHours { get; set; }
        public string PlannedDetails { get; set; }
        public decimal PlannedBudgetAmount { get; set; }
        public int PlannerActualId { get; set; }
        public decimal ActualHours { get; set; }
        public bool IsWent { get; set; }
        public string ActualDetails { get; set; }
        public decimal ActualAmount { get; set; }
        public int TotalPolicy { get; set; }
        public decimal TotalAgencyFee { get; set; }
        public decimal TotalPremiumAmount { get; set; }
        public bool IsLocked { get; set; }
        public bool IsCurrentDay { get; set; }
    }

    public class PlannerActualDetails
    {
        public PlannerBudget plannerBudget { get; set; }
        public ActualDetail actualDetail { get; set; }
        public List<ActualPolicyDetails> listActualPolicyDetails { get; set; }
    }

    public class PlannerDashboard
    {
        public int AgencyId { get; set; }
        public string PlannerDate { get; set; }
        public decimal TotalPlannedBudgetAmount { get; set; }
        public decimal TotalActualAmount { get; set; }
        public string agencyName { get; set; }
    }

    public class DailyView
    {
        public int AgencyId { get; set; }
        public string PlannerDate { get; set; }
        public string Activity { get; set; }
        public string PlannedDetails { get; set; }
        public decimal PlannedHours { get; set; }
        public string ActualDetails { get; set; }
        public decimal ActualHours { get; set; }
        public decimal TotalPlannedBudgetAmount { get; set; }
        public decimal TotalActualAmount { get; set; }
        public int PlannerActualId { get; set; }
        public bool IsWent { get; set; }
        public string MonthlyPlannerDate { get; set; }
        public int PlannerBudgetId { get; set; }
        public bool IsLocked { get; set; }
        public bool IsCurrentDay { get; set; }
    }

    public class PlannerActivityDetails
    {
        public int TotalPlanned { get; set; }
        public int TotalCompleted { get; set; }
        public int Variance { get; set; }
        public string ActivityName { get; set; }
        public int agencyId { get; set; }
        public string AgencyName { get; set; }
    }

    public class QueryPlanner
    {
        public string selectedlocation { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
