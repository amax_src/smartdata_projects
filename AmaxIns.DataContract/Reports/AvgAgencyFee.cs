﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class AvgAgencyFee
    {
        public string ROM { get; set; }
        public string ZM { get; set; }
        public string AgencyName { get; set; }
        public int Transactions { get; set; }
        public int Policy { get; set; }
        public decimal FeePerPolicy { get; set; }
        public int Endorsment { get; set; }
        public decimal FeePerEndorstment { get; set; }
        public int Year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
        public int AgencyId { get; set; }
        public string MName { get; set; }
    }

    public class AvgAgencyFeeII
    {
        public string ROM { get; set; }
        public string ZM { get; set; }
        public int Transactions { get; set; }
        public int Policy { get; set; }
        public decimal FeePerPolicy { get; set; }
        public int Endorsment { get; set; }
        public decimal FeePerEndorstment { get; set; }
        public int Year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
        public string MName { get; set; }
    }

    public class AvgAgencyFeeIII
    {
        public string ROM { get; set; }
        public int Transactions { get; set; }
        public int Policy { get; set; }
        public decimal FeePerPolicy { get; set; }
        public int Endorsment { get; set; }
        public decimal FeePerEndorstment { get; set; }
        public int Year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
        public string MName { get; set; }
    }
}
