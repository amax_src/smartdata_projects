﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class RentersData
    {
        public int RMID { get; set; }
        public string RM { get; set; }
        public int ZMID { get; set; }
        public string ZM { get; set; }
        public int agencyID { get; set; }
        public string AgencyName { get; set; }
        public double Policies { get; set; }
        public double agencyfee { get; set; }
        public double Premium { get; set; }
        public double Goal_Policies { get; set; }
        public double Goal_AgencyFee { get; set; }
        public double Goal_Premium { get; set; }
        public double PriorYear_Policies { get; set; }
        public double PriorYear_agencyfee { get; set; }
        public double PriorYear_Premium { get; set; }
        public double Total_Projecte_Policies { get; set; }
        public double Policy_Goal_vs_Actual { get; set; }
        public double Afee_Goal_vs_Actual { get; set; }
        public double Premium_Goal_vs_Actual { get; set; }
        public double Policy_YoY { get; set; }
        public double Afee_YoY { get; set; }
        public double Premium_YoY { get; set; }

    }

}
