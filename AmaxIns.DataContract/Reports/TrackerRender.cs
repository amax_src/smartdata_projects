﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class TrackerRender
    {
        public string RegionalManagerId { get; set; }
        public string RegionalManager { get; set; }
        public string ZonalManagerId { get; set; }
        public string ZonalManager { get; set; }
        public string agencyNameID { get; set; }
        public string agencyName { get; set; }
        public string MTD_Policies { get; set; }
        public string MTD_agencyfee { get; set; }
        public string MTD_Premium { get; set; }
        public string EOM_Policies { get; set; }
        public string EOM_agencyfee { get; set; }
        public string EOM_Premium { get; set; }
        public string Goal_Policies { get; set; }
        public string Goal_AgencyFee { get; set; }
        public string Goal_Premium { get; set; }
        public string EOM_PoliciesVSGOAL_Policies { get; set; }
        public string EOM_AgencyFeeVSGOAL_AgencyFee { get; set; }
        public string EOM_PremiumVSGOAL_Premium { get; set; }
        public string EOM_PoliciesVSGOAL_PoliciesNO { get; set; }
        public string EOM_AgencyFeeVSGOAL_AgencyFeeNO { get; set; }
        public string EOM_PremiumVSGOAL_PremiumNO { get; set; }
    }

    public class TrackerProduction
    {
        public string RegionalManagerId { get; set; }
        public string RegionalManager { get; set; }
        public string ZonalManagerId { get; set; }
        public string ZonalManager { get; set; }
        public string agencyNameID { get; set; }
        public string agencyName { get; set; }
        public string MTD_Policies { get; set; }
        public string MTD_agencyfee { get; set; }
        public string MTD_Premium { get; set; }
        public string EOM_Policies { get; set; }
        public string EOM_agencyfee { get; set; }
        public string EOM_Premium { get; set; }
        public string Goal_Policies { get; set; }
        public string Goal_AgencyFee { get; set; }
        public string Goal_Premium { get; set; }
        public string EOM_PoliciesVSGOAL_Policies { get; set; }
        public string EOM_AgencyFeeVSGOAL_AgencyFee { get; set; }
        public string EOM_PremiumVSGOAL_Premium { get; set; }
        public string EOM_PoliciesVSGOAL_PoliciesNO { get; set; }
        public string EOM_AgencyFeeVSGOAL_AgencyFeeNO { get; set; }
        public string EOM_PremiumVSGOAL_PremiumNO { get; set; }
    }

    public class TrackerActiveCustomer
    {
        public string RegionalManagerId { get; set; }
        public string RegionalManager { get; set; }
        public string ZonalManagerId { get; set; }
        public string ZonalManager { get; set; }
        public string agencyNameID { get; set; }
        public string agencyName { get; set; }
        public string MonthEndActiveCustomersPOLICIES { get; set; }
        public string EOMPacingPolicies { get; set; }
        public string Goals { get; set; }
        public string EOMPacingVSGoalPolicies { get; set; }
        public string EOMPacingVSGoalNoPolicies { get; set; }
    }
}
