﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class OvertimeI
    {
        public string RegionalManagersId { get; set; }
        public string RegionalManagers { get; set; }
        public string ZonalManagersId { get; set; }
        public string ZonalManagers { get; set; }
        public string AgencyNameID { get; set; }
        public string AgencyName { get; set; }
        public string AgentName { get; set; }     
        public int Total { get; set; }
        public int Regular { get; set; }
        public int OT { get; set; }
        public int Policies { get; set; }
        public int Transactions { get; set; }
        public double OTPerPolicies { get; set; }
        public double OTPerTransaction { get; set; }
        public double PoliciesInOT { get; set; }
        public double TransactionsInOT { get; set; }
        public double MonthsTillToday { get; set; }
        public double TotalhrsperPolicy { get; set; }
        public double TotalhrsperTransaction { get; set; }
    }

    public class OvertimeII
    {
        public string RegionalManagersId { get; set; }
        public string RegionalManagers { get; set; }
        public string ZonalManagersId { get; set; }
        public string ZonalManagers { get; set; }
        public int Total { get; set; }
        public int Regular { get; set; }
        public int OT { get; set; }
        public int Policies { get; set; }
        public int Transactions { get; set; }
        public double OTPerPolicies { get; set; }
        public double OTPerTransaction { get; set; }
        public double PoliciesInOT { get; set; }
        public double TransactionsInOT { get; set; }
        public double MonthsTillToday { get; set; }
        public double TotalhrsperPolicy { get; set; }
        public double TotalhrsperTransaction { get; set; }
    }

    public class OvertimeIII
    {
        public string RegionalManagersId { get; set; }
        public string RegionalManagers { get; set; }
        public int Total { get; set; }
        public int Regular { get; set; }
        public int OT { get; set; }
        public int Policies { get; set; }
        public int Transactions { get; set; }
        public double OTPerPolicies { get; set; }
        public double OTPerTransaction { get; set; }
        public double PoliciesInOT { get; set; }
        public double TransactionsInOT { get; set; }
        public double MonthsTillToday { get; set; }
        public double TotalhrsperPolicy { get; set; }
        public double TotalhrsperTransaction { get; set; }
    }
}
