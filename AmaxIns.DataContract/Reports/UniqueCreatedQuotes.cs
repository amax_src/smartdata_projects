﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class UniqueCreatedQuotes
    {
        public string Market { get; set; }
        public int agencyID { get; set; }
        public string AgencyName { get; set; }
        public int createdQuotes0 { get; set; }
        public int createdQuotes1 { get; set; }
        public double YoY { get; set; }
        public int RMID { get; set; }
        public int ZMID { get; set; }
        public int Years { get; set; }
    }

    public class Market
    {
        public string MarketName { get; set; }
    }
}
