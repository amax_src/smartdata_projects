﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class ActiveCustomerData
    {
        public int RMID { get; set; }
        public string RM { get; set; }
        public int ZMID { get; set; }
        public string ZM { get; set; }
        public int agencyID { get; set; }
        public string AgencyName { get; set; }
        public string Market { get; set; }
        public int ActiveCustomer { get; set; }
        public int PriorYearActiveCustomer { get; set; }
        public double Active_Customers_Goals { get; set; }
        public double ActiveCustomersVsGoals { get; set; }
        public double ActiveCustomersvsPriorYear { get; set; }

    }
}
