﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{

    public class AgencyBreakdownI
    {
        public string ROM { get; set; }
        public string ZM { get; set; }
        public string AgencyName { get; set; }
        public double Avg_fee { get; set; }
        public double Fee_Per_Policy { get; set; }
        public double AfeeLessThan1 { get; set; }
        public double PercentageAfeeLessThan1 { get; set; }
        public double AfeeLessThan1_10 { get; set; }
        public double PercentageAfeeLessThan1_10 { get; set; }
        public double AfeeLessThan10_25 { get; set; }
        public double PercentageAfeeLessThan10_25 { get; set; }
        public double AfeeLessThan25_50 { get; set; }
        public double PercentageAfeeLessThan25_50 { get; set; }
        public double AfeeLessThan50_100 { get; set; }
        public double PercentageAfeeLessThan50_100 { get; set; }
        public double AfeeLessThan100_150 { get; set; }
        public double PercentageAfeeLessThan100_150 { get; set; }
        public double AfeeLessThan150_200 { get; set; }
        public double PercentageAfeeLessThan150_200 { get; set; }
        public double AfeeLessThan200_500 { get; set; }
        public double PercentageAfeeLessThan200_500 { get; set; }
        public double AfeeGreaterThan500 { get; set; }
        public double PercentageAfeeGreaterThan500 { get; set; }
        public int SumOfAllBuckets { get; set; }
        public int year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
        public int AgencyID { get; set; }
    }

    public class AgencyBreakdownII
    {
        public string ROM { get; set; }
        public string ZM { get; set; }
        public double Avg_fee { get; set; }
        public double Fee_Per_Policy { get; set; }
        public double AfeeLessThan1 { get; set; }
        public double PercentageAfeeLessThan1 { get; set; }
        public double AfeeLessThan1_10 { get; set; }
        public double PercentageAfeeLessThan1_10 { get; set; }
        public double AfeeLessThan10_25 { get; set; }
        public double PercentageAfeeLessThan10_25 { get; set; }
        public double AfeeLessThan25_50 { get; set; }
        public double PercentageAfeeLessThan25_50 { get; set; }
        public double AfeeLessThan50_100 { get; set; }
        public double PercentageAfeeLessThan50_100 { get; set; }
        public double AfeeLessThan100_150 { get; set; }
        public double PercentageAfeeLessThan100_150 { get; set; }
        public double AfeeLessThan150_200 { get; set; }
        public double PercentageAfeeLessThan150_200 { get; set; }
        public double AfeeLessThan200_500 { get; set; }
        public double PercentageAfeeLessThan200_500 { get; set; }
        public double AfeeGreaterThan500 { get; set; }
        public double PercentageAfeeGreaterThan500 { get; set; }
        public int SumOfAllBuckets { get; set; }
        public int year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
    }

    public class AgencyBreakdownIII
    {
        public string ROM { get; set; }
        public double Avg_fee { get; set; }
        public double Fee_Per_Policy { get; set; }
        public double AfeeLessThan1 { get; set; }
        public double PercentageAfeeLessThan1 { get; set; }
        public double AfeeLessThan1_10 { get; set; }
        public double PercentageAfeeLessThan1_10 { get; set; }
        public double AfeeLessThan10_25 { get; set; }
        public double PercentageAfeeLessThan10_25 { get; set; }
        public double AfeeLessThan25_50 { get; set; }
        public double PercentageAfeeLessThan25_50 { get; set; }
        public double AfeeLessThan50_100 { get; set; }
        public double PercentageAfeeLessThan50_100 { get; set; }
        public double AfeeLessThan100_150 { get; set; }
        public double PercentageAfeeLessThan100_150 { get; set; }
        public double AfeeLessThan150_200 { get; set; }
        public double PercentageAfeeLessThan150_200 { get; set; }
        public double AfeeLessThan200_500 { get; set; }
        public double PercentageAfeeLessThan200_500 { get; set; }
        public double AfeeGreaterThan500 { get; set; }
        public double PercentageAfeeGreaterThan500 { get; set; }
        public int SumOfAllBuckets { get; set; }
        public int year { get; set; }
        public string month { get; set; }
        public int RMId { get; set; }
        public int ZMId { get; set; }
    }

    public class Filter
    {
        public List<string> year { get; set; }
        public List<string> months { get; set; }
        public List<int> location { get; set; }
    }
    public class paramFilterepr
    {
        public List<string> year { get; set; }
        public List<string> months { get; set; }
        public List<int> location { get; set; }
        public List<string> selectedagent { get; set; }
    }
    public class paramFilter
    {
        public int year { get; set; }
        public string months { get; set; }
        public List<int> location { get; set; }
    }
}
