﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Reports
{
    public class AgentPerformance
    {
        public int RegionalManagersId { get; set; }
        public string RegionalManagers { get; set; }
        public int ZonalManagersId { get; set; }
        public string ZonalManagers { get; set; }
        public int agencyID { get; set; }
        public string agencyName { get; set; }
        public string AgentID { get; set; }
        public string agentName { get; set; }
        public double policies { get; set; }
        public double agencyfee { get; set; }
        public double Premium { get; set; }
        public double transaction { get; set; }
        public double PolScore { get; set; }
        public double AfeeScore { get; set; }
        public double TransactionScore { get; set; }
        public double TotalScore100 { get; set; }
    }
}
