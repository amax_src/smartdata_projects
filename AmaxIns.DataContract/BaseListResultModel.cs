﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract
{
    public class BaseListResultModel<TListItem>: BaseResultModel
    {
        public List<TListItem> Data { get; set; }

        public int TotalRecordCount { get; set; }



    }
}
