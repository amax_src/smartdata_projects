﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CarrierMix
{
    public class CarrierMixUserModel
    {
        public List<string> Date { get; set; }
        public List<int> Agency { get; set; }
        public string Month { get; set; }
        public List<string> Carrier { get; set; }
        public string Year { get; set; }

    }
}
