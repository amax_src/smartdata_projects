﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CarrierMix
{
   public  class CarrierMixModel
    {
      public string Carrier { get; set; }
      public decimal Policies { get; set; }
        public decimal Transactions { get; set; }
        public decimal Premium { get; set; }
        public decimal Agencyfee { get; set; }
        public decimal AvgAgencyFeeByPolicy { get; set; }
    }
}
