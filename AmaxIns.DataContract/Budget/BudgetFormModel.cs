﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetFormModel
    {
        public string year { get; set; }
        public string Month { get; set; }
        public List<string> Budget { get; set; }
        public List<string> Months { get; set; }
        public string DepartmentName { get; set; }

        public List<string> Department { get; set; }
        public List<string> Location { get; set; }
        public List<string> User { get; set; }
        public List<string> CategoryName { get; set; }
        public List<string> BudgetName { get; set; }
        public List<string> BudgetCode { get; set; }
        public List<string> Source { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }
}
