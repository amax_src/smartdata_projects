﻿using System;

namespace AmaxIns.DataContract.Budget
{
    public class ActualTable
    {
        public string PlanID { get; set; }
        public string Source { get; set; }
        public string YearID { get; set; }
        public string PeriodID { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDescription { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string FrontBudgetCode { get; set; }
        public string LineItemDescription { get; set; }
        public string BudgetCode { get; set; }
        public string AccountID { get; set; }
        public string TRXDate { get; set; }
        public string LocationID { get; set; }
        public string TransactionDescription { get; set; }
        public string Reference { get; set; }
        public string Series { get; set; }
        public string UserName { get; set; }
        public string VendorName { get; set; }
        public string DebitAmount { get; set; }
        public string CreditAmount { get; set; }
        public string TRXAmount { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string ReferID { get; set; }
        public string OrignalDeptName { get; set; }
    }
}
