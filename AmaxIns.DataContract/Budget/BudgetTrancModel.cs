﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetTrancModel
    {
        public string TRXDate { get; set; }
        public string CategoryDescription { get; set; }
        public string FrontBudgetCode { get; set; }
        public string TransactionDescription { get; set; }
        public string VendorName { get; set; }
        public string LocationID { get; set; }
        public string Source { get; set; }
        public string UserName { get; set; }
        public string TRXAmount { get; set; }
        public string PeriodID { get; set; }
        public string DeptName { get; set; }
        public string DebitAmount { get; set; }
        public string CreditAmount { get; set; }
        public string LineItemDescription { get; set; }

        //public override string ToString()
        //{
        //    return this.TRXDate + "," + this.CategoryName.Replace(",", " ") + "," + this.BudgetNumber.Replace(",", " ") + "," + this.Description.Replace(",", " ")
        //        + "," + this.VendorName.Replace(",", " ") + "," + this.LocationName.Replace(",", " ") + "," + this.Source.Replace(",", " ")
        //        + "," + this.User.Replace(",", " ") + "," + this.UserPosted.Replace(",", " ") + "," + this.Actual;
        //}
    }

    public class BudgetSummaryModel
    {
        public string Department { get; set; }
        public string Month { get; set; }
        public double Budget { get; set; }
        public double Actual { get; set; }
        public double Variance { get; set; }
        public double VarianceRate { get; set; }
    }
}
