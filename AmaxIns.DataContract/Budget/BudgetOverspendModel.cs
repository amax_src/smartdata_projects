﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetOverspendModel
    {
        public string year { get; set; }   
        public string CategoryName { get; set; }
        public string BudgetName { get; set; }
        public decimal Budget { get; set; }
        public decimal Actual { get; set; }
        public decimal RemaningBudget { get; set; }
        public decimal Variance { get; set; }
        public decimal VarianceRate { get; set; }
        public string Spending { get; set; }
        public string FrontBudgetCode { get; set; }
        public decimal ActualPer { get; set; }
        public decimal RemPer { get; set; }
        

    }
}
