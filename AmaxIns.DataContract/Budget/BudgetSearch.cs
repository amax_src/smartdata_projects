﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
   public  class BudgetSearch
    {
        public string dept { get; set; }
        public string month { get; set; }
        public List<string> catg { get; set; }
        public List<string> department { get; set; }
        public string year { get; set; }
    }
}
