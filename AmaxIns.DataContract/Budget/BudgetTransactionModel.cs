﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetTransactionModel
    {
        public string TRXDate { get; set; }
        public string BudgetNumber { get; set; }
        public string Description { get; set; }
        public string LocationName { get; set; }
        public string Actual { get; set; }
        public string VendorName { get; set; }
        public string UserPosted { get; set; }
        public string User { get; set; }
        public string Source { get; set; }
        public string Period { get; set; }
        public string MonthName { get; set; }
        public string CategoryName { get; set; }

        public override string ToString()
        {
            return this.TRXDate + "," + this.CategoryName.Replace(","," ") + "," + this.BudgetNumber.Replace(",", " ") + "," + this.Description.Replace(",", " ")
                + "," + this.VendorName.Replace(",", " ") + "," + this.LocationName.Replace(",", " ") + "," + this.Source.Replace(",", " ")
                + "," + this.User.Replace(",", " ") + "," + this.UserPosted.Replace(",", " ") + "," + this.Actual;
        }
    }
}
