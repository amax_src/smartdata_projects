﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetModel

    {
        public string year  { get; set; }
        public string Department { get; set; }
        public string CategoryName { get; set; }
        public string BudgetName { get; set; }
        public decimal Budget { get; set; }
        public decimal Actual { get; set; }
        public decimal RemaningBudget { get; set; }
        public decimal BudgetVariance { get; set; }
        public decimal Percentage { get; set; }
        public string Period { get; set; }
         public string MonthName { get; set; }


    }
}
