﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Budget
{
    public class BudgetImportTable
    {
        public string PlanID { get; set; }
        public string YearID { get; set; }
        public string PeriodID { get; set; }
        public string DeptName { get; set; }
        public string CategoryDescription { get; set; }
        public string FrontBudgetCode { get; set; }
        public string SubBgtCode { get; set; }
        public string LineItemDescription { get; set; }
        public string Amount { get; set; }
    }

    public class BudgetTable
    {
        public string PlanID { get; set; }
        public string Year { get; set; }
        public string Period { get; set; }
        public string Department { get; set; }
        public string CategoryName { get; set; }
        public string FrontBudgetCode { get; set; }
        public string BackBudgetCode { get; set; }
        public string BudgetName { get; set; }
        public string BudgetAmount { get; set; }
        public string OriginalDeptName { get; set; }
    }
}
