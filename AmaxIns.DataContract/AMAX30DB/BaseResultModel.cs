﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class BaseResultModel
    {
        public bool IsSuccess { get; set; }

        public string ResultMessage { get; set; }
    }
}
