﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class CountListPaymentInfoQueryModel : BaseQueryModel
    {
        public DateTime payStartDate { get; set; }
        public DateTime payEndDate { get; set; }
        public string applicantName { get; set; }
        public string policyNum { get; set; }
        public string carrier { get; set; }
        public string paymentType { get; set; }
    }
}
