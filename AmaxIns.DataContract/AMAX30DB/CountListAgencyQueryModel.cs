﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class CountListAgencyQueryModel : BaseQueryModel
    {
        public int agencyId { get; set; }
        public string agencyName { get; set; }
    }
}
