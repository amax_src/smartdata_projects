﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
   public class CountListClientInfoQueryModel : BaseQueryModel
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string policyType { get; set; }
        public string addressCity { get; set; }
        public string addressZip { get; set; }
        public string email { get; set; }
        public string homePhone { get; set; }
        public string applicantName { get; set; }
        public string policyNum { get; set; }
        public string policyStatus { get; set; }
        public Int64 clientId { get; set; }
    }
}
