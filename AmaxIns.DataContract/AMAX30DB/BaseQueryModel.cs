﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class BaseQueryModel
    {
        public int RowOffset { get; set; }
        public int PageSize { get; set; }
    }
}
