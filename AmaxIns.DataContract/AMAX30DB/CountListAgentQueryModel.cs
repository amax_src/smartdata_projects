﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class CountListAgentQueryModel: BaseQueryModel
    {
        public string agentName { get; set; }
        public string email { get; set; }
    }
}
