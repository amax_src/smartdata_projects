﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class AMAX30AncyInfoModel
    {
        public int agencyID { get; set; }
        public string agencyName { get; set; }
        public string agencyAddressLine1 { get; set; }
        public string agencyAddressLine2 { get; set; }
        public string agencyAddressCity { get; set; }
        public string agencyAddressState { get; set; }
        public string agencyAddressZip { get; set; }
        public string agencyPhone1 { get; set; }
        public string agencyPhone2 { get; set; }
        public string agencyFax { get; set; }
        public string currentCheckNum { get; set; }
        public int first { get; set; }
        public string startBalance { get; set; }
        public string endBalance { get; set; }
        public DateTime startDate { get; set; }
        public int count { get; set; }
        public string logo { get; set; }
        public int logonRequired { get; set; }
        public DateTime expDate { get; set; }
        public int Registered { get; set; }
        public string codeName { get; set; }
        public string aaLocID { get; set; }
        public int Demo { get; set; }
        public int Online { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateDeleted { get; set; }
        public bool Deleted { get; set; }
        public int DeletedBy { get; set; }
        public string batchID { get; set; }
        public int DefaultBankAccountID { get; set; }
    }
}
