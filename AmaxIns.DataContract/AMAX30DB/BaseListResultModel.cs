﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class BaseListResultModel<TListItem> : BaseResultModel
    {
        public List<TListItem> Data { get; set; }
        public int currentPage { get; set; }
        public int pageCount { get; set; }
        public int pageSize { get; set; }
        public int TotalRecordCount { get; set; }
    }
}
