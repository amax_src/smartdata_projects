﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class Amax30AgentInfoModel
    {
       
        public int agentID { get; set; }
        public string agentName { get; set; }
        public string agentAddressLine1 { get; set; }
        public string agentAddressLine2 { get; set; }
        public string agentAddressCity { get; set; }
        public string agentAddressState { get; set; }
        public string agentAddressZip { get; set; }
        public string agentPhone1 { get; set; }
        public string agentPhone2 { get; set; }
        public string agentNotes { get; set; }
        public string agentPayType { get; set; }
        public int agentRate { get; set; }
        public double agentCommPercent { get; set; }
        public int agentActive { get; set; }
        public string userID { get; set; }
        public string password { get; set; }
        public int adminAccess { get; set; }
        public int accessReport { get; set; }
        public int accessDeletePay { get; set; }
        public int accessDaily { get; set; }
        public int accessSetupAgency { get; set; }
        public int accessReconcile { get; set; }
        public int accessWriteCheck { get; set; }
        public string aaAgentID { get; set; }
        public int agencyID { get; set; }
        public int accessPost { get; set; }
        public int logged { get; set; }
        public string signPic { get; set; }
        public int prefill { get; set; }
        public double agentFlatFee { get; set; }
        public int bLocked { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateDeleted { get; set; }
        public int DeletedBy { get; set; }
        public string EZLynxID { get; set; }
        public string TurboRaterID { get; set; }
        public int AgencyBuzzID { get; set; }
        public int DefaultBankAccountID { get; set; }
        public DateTime sysUTCDateCreated { get; set; }
        public DateTime sysUTCDateModified { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public int accessDelPayClients { get; set; }
        public int accesEODReports { get; set; }
        public int AccessAgencyFees { get; set; }
        public string Password_enc { get; set; }
        public DateTime passwordUpdatedDate { get; set; }
        public int resetPassword { get; set; }
    }
}
