﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AMAX30DB
{
    public class AMAX30PaymentInfoModel
    {
        public int PaymentID { get; set; }
        public int ClientID { get; set; }
        public int CompanyID { get; set; }
        public string coName { get; set; }
        public int AgentID { get; set; }
        public DateTime payDate { get; set; }
        public string payType { get; set; }
        public string payName { get; set; }
        public string payMethod { get; set; }
        public decimal payAmount { get; set; }
        public decimal payFee { get; set; }
        public decimal payTotal { get; set; }
        public decimal payPaid { get; set; }
        public decimal payBalance { get; set; }
        public string payNotes { get; set; }
        public string policyNum { get; set; }
        public short voidd { get; set; }
        public string checkNum { get; set; }
        public short recCheck { get; set; }
        public short recConfirm { get; set; }
        public int agencyID { get; set; }
        public short nsf { get; set; }
        public short sentCo { get; set; }
        public short qbExport { get; set; }
        public int bankID { get; set; }
        public decimal cash { get; set; }
        public decimal check1 { get; set; }
        public decimal check2 { get; set; }
        public decimal card { get; set; }
        public string chkNum2 { get; set; }
        public bool bDeleted { get; set; }
        public DateTime dateDeleted { get; set; }
        public int deletedBy { get; set; }
        public decimal Adjustment { get; set; }
        public int Processor { get; set; }
        public DateTime ProcessedDate { get; set; }
        public int WithdrawalID { get; set; }
        public DateTime sysUTCDateCreated { get; set; }
        public DateTime sysUTCDateModified { get; set; }
        public decimal eftAmount { get; set; }
        public decimal optionalAmount { get; set; }
        public string optionalNotes { get; set; }
        public string membershipType { get; set; }
    }
}
