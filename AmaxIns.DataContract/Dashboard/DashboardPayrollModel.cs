﻿namespace AmaxIns.DataContract.Dashboard
{
    public class DashboardPayrollModel
    {
        public int AgencyID { get; set; }
        public string Location { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public decimal PayrollBudget  { get; set; }
        public decimal PayrollActual { get; set; }
        public decimal OtActual { get; set; }
        public decimal OTBudget { get; set; }
        public decimal OTPerPolicy { get; set; }
        public decimal OTHoursPerPolicy { get; set; }
        public decimal PayrollPerPolicy { get; set; }
        public decimal PayrollPerTransaction { get; set; }
        public int SortMonthNum { get; set; }
        public string GroupName { get; set; }


    }
}
