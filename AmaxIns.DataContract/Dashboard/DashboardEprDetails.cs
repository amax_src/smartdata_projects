﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Dashboard
{
    public class DashboardEprDetails
    {
        public DashboardEprDetails()
        {
            Daily = new List<DashboardEPRModel>();
            Weekly = new List<DashboardEPRModel>();
        }
        public IEnumerable<DashboardEPRModel> Daily { get; set; }
        public IEnumerable<DashboardEPRModel> Weekly { get; set; }
    }
}
