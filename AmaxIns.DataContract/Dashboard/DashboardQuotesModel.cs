﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Dashboard
{
    public  class DashboardQuotesModel
    {

        public int AgencyID { get; set; }
        public string Location { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public int NewQuotes { get; set; }
        public decimal ClosingRatioNewQuotes { get; set; }
        public decimal ClosingRatioModQuotes { get; set; }
        public string GroupName { get; set; }

    }
}
