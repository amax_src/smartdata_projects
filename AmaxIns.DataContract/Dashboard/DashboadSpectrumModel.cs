﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Dashboard
{
    public class DashboadSpectrumModel
    {

        public int AgencyID { get; set; }
        public string Location { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public int OutboundCalls { get; set; }
        public int InboundCalls { get; set; }
        public int SortMonthNum { get; set; }
        public string GroupName { get; set; }

    }
}
