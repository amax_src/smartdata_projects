﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Dashboard
{
    public class DashboardEPRModel
    {
        public int AgencyID { get; set; }
        public string Location { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Date { get; set; }
        public string WeekStart { get; set; }
        public string WeekEnd { get; set; }
        public int Days { get; set; }
        public int averagepolicies { get; set; }
        public int averagetransactions { get; set; }
        public decimal averageagencyfee { get; set; }
        public decimal averagepremium{ get; set; }
        public int SortMonthNum { get; set; }
        public string GroupName { get; set; }
        public string csr { get; set; }
    }
}
