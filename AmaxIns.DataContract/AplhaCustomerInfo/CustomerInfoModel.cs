﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AplhaCustomerInfo
{
    public class CustomerInfoModel
    {
        public string agencyname { get; set; }
        public string clientId { get; set; }
        public string ClientName { get; set; }
        public string Businessname { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyType { get; set; }
        public string PolicyEffectiveDate { get; set; }
        public string DATEPolicyExpirationDate { get; set; }
        public string DOB { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string WorkPhone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string payType { get; set; }
        public string payAmount { get; set; }
        public string payFee { get; set; }
    }
}
