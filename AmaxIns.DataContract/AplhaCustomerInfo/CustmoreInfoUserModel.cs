﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AplhaCustomerInfo
{
    public class CustmoreInfoUserModel
    {
        public string agency { get; set; }
        public string policyType { get; set; }
        public string city { get; set; }
        public string clientName { get; set; }
        public string policyNumber { get; set; }
    }
}
