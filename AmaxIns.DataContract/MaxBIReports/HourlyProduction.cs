﻿using System;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class HourlyProduction
    {
        public int policies { get; set; }
        public decimal agencyfee { get; set; }
        public decimal premium { get; set; }
        public int transactions { get; set; }
        public DateTime paydate { get; set; }
        public int firsthour { get; set; }
        public string dayofweek { get; set; }
    }
}
