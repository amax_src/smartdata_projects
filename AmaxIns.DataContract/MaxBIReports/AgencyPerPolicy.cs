﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class AgencyPerPolicy
    {
        public string MonthName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime? Date { get; set; }
        public string PolicyType { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public int CarrierId { get; set; }
        public string CarrierName { get; set; }
        public int RMId { get; set; }
        public string RMName { get; set; }
        public int ZMId { get; set; }
        public string ZMName { get; set; }
        public decimal AgencyFee { get; set; }
        public decimal Premium { get; set; }
        public int Policies { get; set; }
    }
}
