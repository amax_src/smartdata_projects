﻿using AmaxIns.Common;
using System;
using System.Collections.Generic;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class Request
    {
        public List<string> HierarchyIds { get; set; }
        public List<string> AgentIds { get; set; }
        public List<int> AgencyIds { get; set; }
        public List<int> CompanyIds { get; set; }
        public List<string> PayTypes { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public List<DateTime> Dates { get; set; }

        public Enumeraion.MaxBIReport ReportType { get; set; }
        public string QuoteType { get; set; }
        public string CallType { get; set; }
        public Int16? ReportBy { get; set; }
        public bool IsClosingRatio { get; set; }

        public Enumeraion.MaxBICatalogFrom CatalogFrom { get; set; }
        public Enumeraion.MaxBICatalogType CatalogType { get; set; }

        public int Offset { get; set; }
        public int Limit { get; set; }
    }

    public class Response
    {
        public List<dynamic> Records { get; set; }
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
    }

    public class HierarchyRequest
    {
        public Int16 Level { get; set; }
        public int LevelId { get; set; }
        public Int16 FilterBy { get; set; }
        public string FilterByIds { get; set; }
    }

    public class CatalogInfo
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }

    public class HierarchyInfo
    {
        public int RMId { get; set; }
        public string RMTitle { get; set; }
        public int ZMId { get; set; }
        public string ZMTitle { get; set; }
        public int AgencyId { get; set; }
        public string AgencyTitle { get; set; }
    }

    public class PayrollRequest
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public List<long> AgentIds { get; set; }
        public List<string> PayPeriods { get; set; }
    }
}
