﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class NewModifiedQuotes
    {
        public string MonthName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime? Date { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgentId { get; set; }
        public string AgentName { get; set; }
        public int CarrierId { get; set; }
        public string CarrierName { get; set; }
        public int RMId { get; set; }
        public string RMName { get; set; }
        public int ZMId { get; set; }
        public string ZMName { get; set; }
        public int Quotes { get; set; }
        public int Sales { get; set; }
        public int NoOfAgents { get; set; }
        public int ClosingRatio { get; set; }
    }
}
