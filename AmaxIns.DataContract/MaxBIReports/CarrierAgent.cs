﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class AgentCarrierAFeePremium
    {
        public string MonthName { get; set; }
        public DateTime? DateTransaction { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string PolicyType { get; set; }
        public string Carrier { get; set; }
        public decimal AgencyFee { get; set; }
        public decimal Premium { get; set; }
        public int Policies { get; set; }
    }
}
