﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class EmployeeInfo
    {
        public string EmployeeCode { get; set; }
        public int TenureInDays { get; set; }
        public string LicenseType1 { get; set; }
        public string LicenseNumber1 { get; set; }
        public string LicenseType2 { get; set; }
        public string LicenseNumber2 { get; set; }
    }
}
