﻿using System;

namespace AmaxIns.DataContract.MaxBIReports
{
    public class Payroll
    {
        public long AgencyId { get; set; }
        public long EmployeeNumber { get; set; }
        public string PayPeriod { get; set; }
        public string CategoryDescription { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal DebitHours { get; set; }
    }

    public class PayrollFilterData
    {
        public string PayPeriod { get; set; }
        public DateTime PayPeriodStart { get; set; }
        public DateTime PayPeriodEnd { get; set; }
        public string PayPeriodMonth { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
