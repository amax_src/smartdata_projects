﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
   public class ConsolidatedCallVolumeModel
    {
        public IEnumerable<CallVolumeModel> ByDays { get; set; }
        public IEnumerable<CallVolumeModel> ByStatus { get; set; }
    }
}
