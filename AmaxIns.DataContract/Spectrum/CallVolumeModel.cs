﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
   public  class CallVolumeModel
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Month { get; set; }
        public string SortMonthNo { get; set; }
        public long CountOfCalls { get; set; }
        public string DayofWeek { get; set; }
        public long AsTalkTime { get; set; }
        public string Call_Status { get; set; }
    }
}
