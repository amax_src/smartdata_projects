﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
    public class RepeatedCallersModel
    {


        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Cnumber { get; set; }
        public DateTime Date { get; set; }
        public int Countofcalls { get; set; }

        public string Callerid_Internal { get; set; }
        public string Callerid_External { get; set; }
        public string CallType { get; set; }

    }
}
