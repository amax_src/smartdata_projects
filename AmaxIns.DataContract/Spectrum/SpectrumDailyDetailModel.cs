﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
    public class SpectrumDailyDetailModel
    {
        public int? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Date { get; set; }
        public string Callid { get; set; }
        public string Start_Time { get; set; }
        public string Server { get; set; }
        public string Stype { get; set; }
        public string Snumber { get; set; }
        public string Sname { get; set; }
        public string Dtype { get; set; }
        public string Dnumber { get; set; }
        public int? Talktime { get; set; }
        public string Callerid_Internal { get; set; }
        public string Callerid_External { get; set; }
        public string Uniqueid { get; set; }
        public string Call_Status { get; set; }
        public string Call_Start { get; set; }
        public string Call_End { get; set; }
        public string Media { get; set; }
        public string Totaltime { get; set; }
        public string Scustomer { get; set; }
        public string Spresent { get; set; }
        public string ctype { get; set; }
        public string Cnumber { get; set; }
        public string Dcustomer { get; set; }
        public string Callername_Internal { get; set; }
        public string Callername_External { get; set; }
        public string Ingroup { get; set; }
        public string Ingroup_Time { get; set; }
        public string Outgroup { get; set; }
        public string Outgroup_Time { get; set; }
        public string Plan_Id { get; set; }
        public string Peer { get; set; }
        public string Channel { get; set; }
        public string Invoice { get; set; }
        public string Overmax { get; set; }
        public string Taxed { get; set; }
        public string Bridged_callid { get; set; }
        public string Month { get; set; }
        public int? SortMonthNo { get; set; }
    }
}
