﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
   public class CallCountModel
    {
        public int AgencyId                 {get;set;}
        public string AgencyName               {get;set;}
        public string Cnumber                  {get;set;}
        public int CountOfIncomingCalls     {get;set;}
        public DateTime Date { get; set; }
    }
}
