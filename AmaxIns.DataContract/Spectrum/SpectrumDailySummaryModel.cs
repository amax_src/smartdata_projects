﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Spectrum
{
    public class SpectrumDailySummaryModel
    {
        public int? Agencyid { get; set; }
        public string AgencyName { get; set; }
        public string Extension { get; set; }
        public int? InboundCalls { get; set; }
        public int? OutboundCalls { get; set; }
        public int? InternalCalls { get; set; }
        public int? TotalCalls { get; set; }
        public decimal TalktimeMinutes { get; set; }
        public decimal AverageTalkTime { get; set; }
        public string Date { get; set; }

        public string Month { get; set; }
        public int? SortMonthNo { get; set; }
    }
}
