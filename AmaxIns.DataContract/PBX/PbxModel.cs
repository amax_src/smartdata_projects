﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PBX
{
    public class PbxModel
    {
        public string Date { get; set; }
        public int MonthNo { get; set; }
        public string Month { get; set; }
        public string CallId { get; set; }
        public string Direction { get; set; }
        public string StartTime { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal TalkTimeMinutes { get; set; }
        public string Extension { get; set; }
    }
}
