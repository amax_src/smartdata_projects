﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Calendar
{
    public class CalendarEvents
    {
        public CalendarEvents()
        {
            CalendarModels = new List<CalendarLedgerDetails>();
        }
        public int EventId { get; set; }
        public string EventName { get; set; }
        public string EventType { get; set; }
        public DateTime EventDate { get; set; }
        public int AgencyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public int AddedBy { get; set; }

        public List<CalendarLedgerDetails> CalendarModels { get; set; }

    }
}
