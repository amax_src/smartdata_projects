﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Calendar
{
    public class CalendarLedgerDetails
    {
        public int LedgerId { get; set; }
        public int EventId { get; set; }
        public string Memo { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AddedBy { get; set; }
    }
}
