﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.WorkingDays
{
    public class WorkingdayModel
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int TotalWorkingdays { get; set; }
        public int WorkingDaysPassed { get; set; }
        public int RemainingDays { get; set; }
    }
}
