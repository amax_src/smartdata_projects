﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract
{
    public class BaseQueryModel
    {
        public int RowOffset { get; set; }
        public int PageSize { get; set; }

        public string SortBy { get; set; }
        public string SortDir { get; set; }

    }
}
