﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.Token
{
    public class TokenModel
    {
        public int Id { get; set; }
        public int AgentId { get; set; }
        public string Token { get; set; }
    }
}
