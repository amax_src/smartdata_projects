﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral
{
    public class WinbackModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PolicyNumber { get; set; }
        public string AgentId { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public DateTime DateTransaction { get; set; }
        public DateTime LastTransactionDate { get; set; }
        public string CustomerStatus { get; set; }
        public Boolean Quoted { get; set; }
        public Boolean Sold { get; set; }
        public Boolean Text { get; set; }
        public Boolean CallAnswered { get; set; }
        public Boolean NoAnswerCallback { get; set; }
        public string LeaveUs { get; set; }
        public Boolean InsuranceProStatus { get; set; }
        public string CustomerLeaveComment { get; set; }
        public string AdditionalComment { get; set; }
        public string SortCustomerLeaveComment { get; set; }
        public string SortAdditionalComment { get; set; }
        public string Carrier { get; set; }
        public string Link { get; set; }
        public DateTime Updatedate { get; set; }
        public List<CommentsModel> Comments { get; set; }
     

        public override string ToString()
        {
            return 
                CustomerName?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," + 
                Carrier?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," +
                PolicyNumber?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," + 
                Email?.Replace(",", " ").Trim() + "," + 
                Phone?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," +
                AgencyName?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," +
                (Quoted ? "Yes" : "No") + "," + 
                (Sold ? "Yes" : "No") + "," +
                (CallAnswered ? "Yes" : "No") + "," + 
                (DateTransaction != Convert.ToDateTime("1/1/0001 12:00:00 AM") ? DateTransaction.ToString("MM/dd/yyyy") : "") + "," + 
                AgentId?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," + 
                (Updatedate != Convert.ToDateTime("1/1/0001 12:00:00 AM") ? Updatedate.ToString() : "") + "," + 
                AdditionalComment?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," + 
                CustomerLeaveComment?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," +
                LeaveUs?.Replace(",", " ").Trim().Replace(System.Environment.NewLine, " ") + "," + 
                (NoAnswerCallback ? "Yes" : "No") + "," + 
                (InsuranceProStatus ? "Active" : "Not Active");
        }
    }
    public class WinbackAPIModel
    {
       public  string Id { get; set; }
        public string CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string PaymentId { get; set; }
        public string ClientId { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PolicyNumber { get; set; }
        public int AgentId { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public DateTime DateTransaction { get; set; }
        public DateTime LastTransactionDate { get; set; }
        public string CustomerStatus { get; set; }
        public bool Quoted { get; set; }
        public bool Sold { get; set; }
        public bool Text { get; set; }
        public bool FollowEmail { get; set; }
        public bool CallAnswered { get; set; }
        public string LeaveUs { get; set; }
        public bool NoAnswerCallback { get; set; }
        public bool InsuranceProStatus { get; set; }
        public int CarriedId { get; set; }
        public string Carrier { get; set; }
        public string CustomerLeaveComment { get; set; }
        public string AdditionalComment { get; set; }
        public int EditByAgency { get; set; }
        public string DriverLicense { get; set; }
        public string VinNumber { get; set; }
    }

}
