﻿

using System;
using System.Collections.Generic;

namespace AmaxIns.DataContract.Referral
{
    public class WinbackCommentsModel
    {

        public int Id { get; set; }
        public int AgencyId { get; set; }
        public string Comments { get; set; }
        public string Date { get; set; }
        public int ReferalApp_WinbackId { get; set; }
        public string CreatedDate { get; set; }
        public string AgencyName { get; set; }
        public int AgentId { get; set; }
    }
}
