﻿

namespace AmaxIns.DataContract.Referral
{
    public class CommentsModel
    {
        public int ReferenceId { get; set; }
        public int AgencyId { get; set; }
        public string Comments { get; set; }
        public string Date { get; set; }
        public string AgencyName { get; set; }
    }
}
