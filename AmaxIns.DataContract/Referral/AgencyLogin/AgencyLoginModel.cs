﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.AgencyLogin
{
    public class AgencyLoginModel
    {
        public int AgencyId { get; set; }
        public string AgencyLogin { get; set; }
        public string Password { get; set; }
        public string Zip { get; set; }
        public string AgentId { get; set; }
    }
}
