﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.AgencyLogin
{
    public class AgentLoginModel
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string Password { get; set; }
        public string LoginAgentID { get; set; }
    }
}


