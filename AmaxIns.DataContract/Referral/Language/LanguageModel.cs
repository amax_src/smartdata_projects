﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.Language
{
    public class LanguageModel
    {
        public int Id { get; set; }
        public string Language { get; set; }

        public bool IsActive
        {
            get; set;
        }
    }
}
