﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.Language
{
   public class ResourceValueModel
    {
        public string ResourceKey { get; set; }
        public string ResourceText { get; set; }
        public int LanguageId { get; set; }
    }
}
