﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral
{
    public class WinbackCommentWrapper
    {
        public WinbackCommentWrapper()
        {
            this.Comments = new List<WinbackCommentsModel>();
        }
        public int ReferalApp_WinbackId { get; set; }
        public int AgencyId { get; set; }
        public int AgentId { get; set; }
        public string CustomerLeaveComment { get; set; }
        public string AdditionalComment { get; set; }
        public bool FollowEmail { get; set; }
        public bool Quoted { get; set; }
        public bool Sold { get; set; }
        public bool Text { get; set; }
        public bool CallAnswered { get; set; }
       public IEnumerable<WinbackCommentsModel> Comments { get; set; }
    }
}
