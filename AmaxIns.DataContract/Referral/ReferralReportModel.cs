﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral
{
    public class ReferralReportModel
    {
        public string referrelId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }
        public string AgentId { get; set; }
        public string ReferralName { get; set; }
        public string ReferralEmail { get; set; }
        public string ReferralPhone { get; set; }
        public string Year { get; set; }
        public string MonthNo { get; set; }
        public string MonthName { get; set; }
        public string AgencyID { get; set; }
        public bool IsCustomer { get; set; }

        public string ZipCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public string RoutedTo { get; set; }
        public string Comment { get; set; }
        public int ReferencecommemtsId { get; set; }
    }
}
