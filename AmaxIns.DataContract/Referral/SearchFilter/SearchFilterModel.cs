﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Referral.SearchFilter
{
    public class SearchFilterModel
    {
        public int Id { get; set; }
        public int AgentId { get; set; }
        public string SearchParameter { get; set; }
        public string SortParameter { get; set; }
        public string SortDirection { get; set; }
        public string SearchMonth { get; set; }
        public string SearchYear { get; set; }
    }
}
