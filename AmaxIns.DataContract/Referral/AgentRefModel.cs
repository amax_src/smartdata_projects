﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AmaxIns.DataContract.Referral
{
    public class AgentRefModel
    {
        public string RefName { get; set; }
        public string RefEmail { get; set; }
        public string RefPhone { get; set; }
        public string AgentId { get; set; }
        public int AgencyId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
    }
}
