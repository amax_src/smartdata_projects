﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class DriverModel
    {
        public int TurboratorID { get; set; }
        public int DriverID { get; set; }
        public string   FirstName { get; set; }
        public string    LastName { get; set; }
        public string    Relation { get; set; }
        public string   DriverLicenseNumber { get; set; }
        public DateTime Dob { get; set; }
        public string   Gender { get; set; }
        public string    MaritalStatus { get; set; }
        public bool     PriorInsurance { get; set; }
        public bool     IsDefaultInsured { get; set; }
        public Address2 Address { get; set; }
    }
}
