﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class CarModel
    {

      public int TurboratorId { get; set; }
        public int CarId { get; set; }
        public string   Vin { get; set; }
        public int      Year     { get; set; }
        public string   Maker { get; set; }
        public string   Model { get; set; }
        public bool     Liab { get; set; }
        public GaragingAddress GaragingAddress { get; set; }
    }
}
