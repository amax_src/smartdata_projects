﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class AddressModel
    {
        public int AddressId { get; set; }
        public int TurboratorID { get; set; }
        public int CustomerID { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        
    }
    public class Address2: AddressModel
    {
        public int Address2Id { get; set; }

        public int DriverId { get; set; }

    }
    public class GaragingAddress: AddressModel
    {
        public int GaragingAddressId { get; set; }
        public int CarId { get; set; }

    }
}
