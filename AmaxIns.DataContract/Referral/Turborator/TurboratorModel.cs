﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class TurboratorModel
    {
        public TurboratorModel()
        {
            Coverages = new List<CoverageModel>();
        }

        public int TurboratorID { get; set; }
        public string Term { get; set; }
        public string QuoteUrl { get; set; }
        public string QuoteNumber { get; set; }
        public string EventType { get; set; }
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        public string StoreId { get; set; }
        public string TrQuoteUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Agencyid { get; set; }
        public CustomerModel Customer { get; set; }
        public List<CoverageModel> Coverages { get; set; }
        public List<CarModel> Cars { get; set; }
        public List<DriverModel> Drivers { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CarrierName { get; set; }
        public decimal QuoteRate { get; set; }
        public string ZipCode { get; set; }

        public string AgencyName { get; set; }
        public string Comments { get; set; }
        public int TurboraterReferencecommemtsId { get; set; }
   
        public bool ConvertedToCustomer { get; set; }
    }
}
