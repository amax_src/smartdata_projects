﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class CustomerModel
    {
        public int TurboratorId { get; set; }
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public AddressModel Address { get; set; }
    }
}
