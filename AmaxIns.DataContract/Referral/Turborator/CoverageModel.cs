﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.DataContract.Referral.Turborator
{
    public class CoverageModel
    {
        public int TurboratorId { get; set; }
        public int CoverageId { get; set; }
        public string CoverageCd { get; set; }
        public int Val1 { get; set; }
        public int Val2 { get; set; }
        public int Val3 { get; set; }
    }
}
