﻿namespace AmaxIns.DataContract.Agency
{
    public class AgencyModel
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public int RegionalZonalID { get; set; }
        public string ZonalManagers { get; set; }
        public string RegionalManagers { get; set; }
        public bool IsActive { get; set; }
    }
}
