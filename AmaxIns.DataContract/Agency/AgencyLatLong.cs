﻿

namespace AmaxIns.DataContract.Agency
{
    public class AgencyLatLong
    {
        public string AgencyName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
