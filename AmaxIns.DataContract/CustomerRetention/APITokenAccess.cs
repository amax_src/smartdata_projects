﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class APITokenAccess
    {
        public string message { get; set; }
        public AccessTokenResult result { get; set; }
    }
    public class AccessTokenResult
    {
        public string token { get; set; }
    }
}
