﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public enum DueDateFilterTypeEnum
    {
        DueToday = 1,
        DueNext7Days = 2,
        PastDue = 3,
        Canceled = 4
    }

    
}
