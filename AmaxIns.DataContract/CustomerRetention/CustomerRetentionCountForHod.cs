﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CustomerRetentionCountForHod
    {
        public DateTime DueDate { get; set; }

        public string RegionManagerName { get; set; }
        public int RegionId { get; set; }

        public int PaymentDueCount { get; set; }
        public int PaymentReceivedCount { get; set; }
        public int Difference { get; set; }
        public int ContactCallCount { get; set; }
    }
}
