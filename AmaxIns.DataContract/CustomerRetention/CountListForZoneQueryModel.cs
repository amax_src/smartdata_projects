﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CountListForZoneQueryModel: BaseQueryModel
    {
        public int ZoneManagerUserId { get; set; }
        public int StoreManagerUserId { get; set; }
        public int DueDateFilterTypeId { get; set; }


        public DateTime? DueDate { get; set; }

        public List<int> AgencyIdList { get; set; }

    }
}
