﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CustomerRetentionAPIResponse
    {
        public string message { get; set; }
        public CustomerRetentionAPIResult result { get; set; }
    }

    public class CustomerRetentionAPIResult
    {
        public List<CustomerRetentionResponse> results { get; set; }
        public int currentPage { get; set; }
        public int pageCount { get; set; }
        public int pageSize { get; set; }
        public int rowCount { get; set; }
    }

    public class CustomerRetentionResponse
    {
        public string insured { get; set; }
        public string policyNo { get; set; }
        public string carrierName { get; set; }
        public string agencyName { get; set; }
        public string externalAgencyCode { get; set; }
        public DateTime effectiveDate { get; set; }
        public DateTime expirationDate { get; set; }
        public string mobilePhone { get; set; }
        public string homePhone { get; set; }
        public string workPhone { get; set; }
        public string status { get; set; }
        public string paid { get; set; }
        public decimal dueAmount { get; set; }
        public DateTime dueDate { get; set; }
        public string policyType { get; set; }
        public string cellPhone { get; set; }

    }
}
