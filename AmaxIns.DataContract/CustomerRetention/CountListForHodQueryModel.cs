﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CountListForHodQueryModel: BaseQueryModel
    {
        public int HodUserId { get; set; }

        public int DueDateFilterTypeId { get; set; }


        public DateTime? DueDate { get; set; }

        public List<int> AgencyIdList { get; set; }

        public List<int> ZoneIdList { get; set; }

        public List<int> RegionIdList { get; set; }
    }
}
