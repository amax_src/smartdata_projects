﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class ListQueryModel: BaseQueryModel
    {
        public int StoreManagerUserId { get; set; }

        public int DueDateFilterTypeId { get; set; }
        public int AgencyId { get; set; }

        public DateTime? DueDate { get; set; }

    }
}
