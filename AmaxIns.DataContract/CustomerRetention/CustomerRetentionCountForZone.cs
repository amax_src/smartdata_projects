﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CustomerRetentionCountForZone
    {
        public DateTime DueDate { get; set; }
        
        public string LocationName { get; set; }
        public int LocationId { get; set; }

        public int PaymentDueCount { get; set; }
        public int PaymentReceivedCount { get; set; }
        public int Difference { get; set; }
        public int ContactCallCount { get; set; }

    }
}
