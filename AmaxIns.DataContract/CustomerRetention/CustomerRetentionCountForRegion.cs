﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CustomerRetentionCountForRegion
    {
        public DateTime DueDate { get; set; }

        public string ZoneManagerName { get; set; }
        public int ZoneId { get; set; }

        public int PaymentDueCount { get; set; }
        public int PaymentReceivedCount { get; set; }
        public int Difference { get; set; }
        public int ContactCallCount { get; set; }
    }
}
