﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CountListForRegionQueryModel: BaseQueryModel
    {
        public int RegionManagerUserId { get; set; }

        public int DueDateFilterTypeId { get; set; }


        public DateTime? DueDate { get; set; }

        public List<int> AgencyIdList { get; set; }

        public List<int> ZoneIdList { get; set; }

    }
}
