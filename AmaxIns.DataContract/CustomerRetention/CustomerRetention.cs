﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.CustomerRetention
{
    public class CustomerRetentions
    {
        public DateTime DueDate { get; set; }
        public string ClientName { get; set; }
        public string PolicyNumber { get; set; }
        public int PolicyStatusId { get; set; }
        public string PolicyStatusName { get; set; }
        public string CompanyName { get; set; }
        public int PolicyTypeId { get; set; }
        public string PolicyTypeName { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public decimal AmountDue { get; set; }
        public string ClientCellPhone { get; set; }
        public string ClientHomePhone { get; set; }
        public string ClientWorkPhone { get; set; }
        public bool CalledStatus { get; set; }
        public string CalledStatusName { get; set; }
        public int CallsMadeCount { get; set; }
        public string CalledDuration { get; set; }
        public string Paid { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyName { get; set; }
        public string ZonalManagers { get; set; }
        public string RegionalManagers { get; set; }

    }
}
