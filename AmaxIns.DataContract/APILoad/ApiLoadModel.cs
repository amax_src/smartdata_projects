﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.APILoad
{
    public class ApiLoadModel
    {
        public string Page { get; set; }
        public string User { get; set; }
        public string Time { get; set; }
    }
}
