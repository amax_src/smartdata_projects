﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class CustomerJourneyInfo
    {
        public Int32 BindOnlineCarrierQuoteId { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public DateTime birthDate { get; set; }
        public string driverLicenseNumber { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string customerId { get; set; }
        public string Date { get; set; }
        public string quoteUrl { get; set; }
        public string quoteNumber { get; set; }
        public string carrierName { get; set; }
        public bool callCenterSent { get; set; }
        public bool buyNowOption { get; set; }
        public bool buyNowClick { get; set; }
        public string payPlanDescription { get; set; }
        public int payPlanNumOfPayments { get; set; }
        public decimal payPlanPercentDown { get; set; }
        public decimal payPlanDownPayment { get; set; }
        public decimal payPlanPaymentAmount { get; set; }
        public decimal payPlanServiceFees { get; set; }
        public decimal payPlanPaymentTotal { get; set; }
        public int term { get; set; }
        public decimal totalPremium { get; set; }
        public string carDetails { get; set; }
        public string carCoverageDetails { get; set; }
        public string PolicyNumber { get; set; }
        public bool IsSold { get; set; }
        public string gender { get; set; }
        public string maritalStatus { get; set; }
    }

    public class RetailCustomerJourneyInfo
    {
        public string quoterecordid { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public DateTime dob { get; set; }
        public string emailaddress { get; set; }
        public string cellphone { get; set; }
        public string workphone { get; set; }
        public string homephone { get; set; }
        public string address1 { get; set; }
        public DateTime visited_date { get; set; }
        public string companyname { get; set; }
        public decimal lasttotalpremiumquoted { get; set; }
        public decimal lastdownpaymentquoted { get; set; }
        public decimal lastpaymentquoted { get; set; }
        public decimal percentdown { get; set; }
        public string lastpayplanquoted { get; set; }
        public string year_car { get; set; }
        public string maker_car { get; set; }
        public string model_car { get; set; }
        public int term { get; set; }
        public int numofpayments { get; set; }
        public decimal downpayment { get; set; }
        public decimal paymenttotal { get; set; }
        public string drvlicensenumber { get; set; }
    }
}
