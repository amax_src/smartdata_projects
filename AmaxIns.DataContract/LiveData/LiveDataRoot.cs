﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace AmaxIns.DataContract.LiveData
{
    public class LiveDataRoot
    {
        [JsonProperty("@odata.context")]
        public string context { get; set; }
        public List<LiveData> value { get; set; }
        [JsonProperty("@odata.nextLink")]
        public string NextPageLink { get; set; }
    }
}
