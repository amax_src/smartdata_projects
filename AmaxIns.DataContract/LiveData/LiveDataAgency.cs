﻿using AmaxIns.DataContract.Revenue;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class LiveDataAgency
    {
        public string AgencyName { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public double Premium { get; set; }
        public int PolicyCount { get; set; }
        public double AgencyFee { get; set; }
        public int ModifiedQuotes { get; set; }
        public int NewQuotes { get; set; }

        public double TodayPremium { get; set; }
        public int TodayPolicyCount { get; set; }
        public double TodayAgencyFee { get; set; }
        public List<Agent> Agent { get; set; }
        public int AgentTotalPolicy { get; set; }
        public Double AgentTotalPremium { get; set; }
        public Double AgentTotalAgencyFee { get; set; }

        public int AgentTotalTrans { get; set; }
        public int EndorsementCount { get; set; }
        public double CashLogTotal { get; set; }
        public string Date { get {
                // return DateTime.UtcNow.ToShortDateString();
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                return newDT.ToShortDateString();
            }
        }

        public double PremiumGoal { get; set; }
        public int PolicyGoal { get; set; }
        public double AgencyFeeGoal { get; set; }

        public double MtdPremium { get; set; }
        public double MtdPolicy { get; set; }
        public double MtdAgencyFee { get; set; }

        public int TotalWorkingdays { get; set; }

        public List<Company> Companies { get; set; }

        public int TotalCompaniesSale { get; set; }

        public double CreditCardLog { get; set; }

        public int WalkIn { get; set; }
        public int PhoneInternet { get; set; }

        public int WalkInSold { get; set; }
        public int PhoneInternetSold { get; set; }

        public int RemainingDays { get; set; }

        public int AgentTotalcall { get; set; }

        public AmPmDepositModel AMPMDeposit { get; set; }
       public List<PaymentMethod> PaymentMethod { get; set; }

    }

}
