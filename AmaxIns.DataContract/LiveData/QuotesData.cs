﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class QuotesData
    {
        public string agencyName { get; set; }
        public string agencyAddressLine1 { get; set; }
        public string AlreadyExisted { get; set; }
        public string NewQuote { get; set; }
        public string agencyAddressZip { get; set; }
    }
}
