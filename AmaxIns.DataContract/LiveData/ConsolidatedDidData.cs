﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
   public class ConsolidatedDidData
    {
        //public string AgencyName { get; set; }
        //public int TvEnglish { get; set; }
        //public int TvSpanish { get; set; }
        //public int RadioEnglish { get; set; }
        //public int RadioSpanish { get; set; }
        //public int AllDigital { get; set; }
        //public int DfwEnglishTv { get; set; }
        //public int DfwSpanishTv { get; set; }
        //public int Quote { get; set; }
        //public int Sold { get; set; }

        public List<AgencyDIDData> tvEnglishHouston = new List<AgencyDIDData>();
        public List<AgencyDIDData> tvSpanishHouston = new List<AgencyDIDData>();
        public List<AgencyDIDData> radioEnglishRgv = new List<AgencyDIDData>();
        public List<AgencyDIDData> radioSpanisRrgv = new List<AgencyDIDData>();
        public List<AgencyDIDData> allDigital = new List<AgencyDIDData>();
        public List<AgencyDIDData> dfwEnglishTv = new List<AgencyDIDData>();
        public List<AgencyDIDData> dfwSpanishTv = new List<AgencyDIDData>();
    }
}
