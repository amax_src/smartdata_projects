﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class LiveData
    {
        public int PaymentID { get; set; }
        public string Applicant { get; set; }
        public string Agent { get; set; }
        public string Agency { get; set; }
        public string Company { get; set; }
        public string payDate { get; set; }
        public string payType { get; set; }
        public string payName { get; set; }
        public string payMethod { get; set; }
        public double payAmount { get; set; }
        public double payFee { get; set; }
        public double payTotal { get; set; }
        public double payPaid { get; set; }
        public double? payBalance { get; set; }
        public string payNotes { get; set; }
        public double @void { get; set; }
        public string checkNum { get; set; }
        public double? recCheck { get; set; }
        public double? recConfirm { get; set; }
        public double? nsf { get; set; }
        public double? sentCo { get; set; }
        public string Bank { get; set; }
        public double? cash { get; set; }
        public double? check1 { get; set; }
        public double? check2 { get; set; }
        public double? card { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public object DeletedBy { get; set; }
        public double? Adjustment { get; set; }
        public double? Processor { get; set; }
        public DateTime? ProcessedDate { get; set; }

        public override string ToString()
        {
            return PaymentID + "," + payType + "," + payAmount + "," + payFee+ "," + payDate;
        }
    }


    public class CommonUtility
    {
        public static double ConvertDouble(double value)
        {
            try
            {
                return value;
            }
            catch (Exception ex)
            {
                return 0.000;
            }
        }
    }
}


