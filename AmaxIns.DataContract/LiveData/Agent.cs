﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class Agent
    {
        public string Name { get; set; }
        public int PolicyCount { get; set; }
        public double AgencyFee { get; set; }
        public double Premium { get; set; }

        public int Trans { get; set; }
        public int Calls { get; set; }


    }
}
