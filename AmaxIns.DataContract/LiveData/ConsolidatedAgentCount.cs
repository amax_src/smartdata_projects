﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class ConsolidatedAgentCount
    {
        public int agent_count { get; set; }
        public string hours { get; set; }

        public string location { get; set; }
    }
}
