﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData
{
    public class BOLQuoteData
    {
        public BOLQuoteData()
        {
            this.PolicyNumber = "";
        }
        public DateTime Date { get; set; }
        public string QuoteUrl { get; set; }
        public string CarrierName { get; set; }
        public bool CallCenterSent { get; set; }
        public bool BuyNowOption { get; set; }
        public bool BuyNowClick { get; set; }
        public string PolicyNumber { get; set; }
        public int AgencyFee { get; set; }
        public string Time
        {
            get;
            set;
            //{
            //    return Date.ToShortDateString() + " " + Date.ToShortTimeString();
            //    //return Cstdate.Split('T')[1].ToString();
            //}
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public bool Isdelayed { get; set; }
        public bool IsQuoteDataMissing { get; set; }
        public string WebQuoteID { get; set; }
        public bool IsSold { get; set; }
        public decimal PremiumAmount { get; set; }
        public decimal CollectedPremium { get; set; }
    }

    //public class Address
    //{
    //    public int Id { get; set; }
    //    public string Address1 { get; set; }
    //    public string City { get; set; }
    //    public string State { get; set; }
    //    public int ZipCode { get; set; }
    //}

    //public class Customer
    //{
    //    public int Id { get; set; }
    //    public string CustomerId { get; set; }
    //    public string Name { get; set; }
    //    public string Surname { get; set; }
    //    public DateTime BirthDate { get; set; }
    //    public string DriverLicenseNumber { get; set; }
    //    public string Email { get; set; }
    //    public string PhoneNumber { get; set; }
    //    public Address Address { get; set; }
    //}
}
