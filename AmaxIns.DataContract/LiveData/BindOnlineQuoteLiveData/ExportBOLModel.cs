﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData
{
    public class ExportBOLModel
    {
        public string quoteUrl { get; set; }
        public string carrierName { get; set; }
        public string time { get; set; }
        public string policyNumber { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int agencyFee { get; set; }
        public string state { get; set; }
        public string premiumAmount { get; set; }
        public string collectedPremium { get; set; }
        public bool buyNowClick { get; set; }
        public bool buyNowOption { get; set; }
        public bool callCenterSent { get; set; }
        public string email { get; set; }
        public bool isQuoteDataMissing { get; set; }
        public bool isSold { get; set; }
        public bool isdelayed { get; set; }
        public string webQuoteID { get; set; }

    }
}
