﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData
{
   public class BolPayLoad
    {
        public List<string> carriers { get; set; }
        public List<string> date { get; set; }
        public List<string> state { get; set; }
    }
    public class SpectrumParam
    {
        public string login { get; set; }
        public List<string> date { get; set; }
    }
    public class StratusExtensionParam
    {

        public string extension { get; set; }
        public string date { get; set; }
    }
}
