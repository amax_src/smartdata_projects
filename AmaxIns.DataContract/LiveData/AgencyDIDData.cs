﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class AgencyDIDData
    {
        public int Count { get; set; }
        public string AgencyName { get; set; }
        public int Sold { get; set; }
        public int Quote { get; set; }
    }
}
