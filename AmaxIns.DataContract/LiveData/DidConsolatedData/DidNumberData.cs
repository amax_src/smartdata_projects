﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData.DidConsolatedData
{
  public  class DidNumberData
    {
        public string DID { get; set; }
        public string Callsoffered { get; set; }
        public string Callsanswered { get; set; }
        public string PercentageAns { get; set; }
        public string Duration { get; set; }
    }
}
