﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class EODLive
    {
        public string VerifiedTime { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string Store { get; set; }
        public string TotalSales { get; set; }
        public string TotalCollections { get; set; }
        public string ShortOver { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public string VarifiedBy { get; set; }
        public string Status { get; set; }
        public string Agent { get; set; }
        public DateTime sysUTCCreatedDate { get; set; }
        public int agencyId { get; set; }
    }
}
