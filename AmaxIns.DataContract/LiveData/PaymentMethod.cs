﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
   public class PaymentMethod
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}
