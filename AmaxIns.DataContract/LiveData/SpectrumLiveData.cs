﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.LiveData
{
    public class SpectrumLiveHourlyModel
    {
        public int agencyid { get; set; }
        public string Location { get; set; }
        public string extension { get; set; }
        public int CallHours { get; set; }
        public DateTime Date { get; set; }
        public int CallType { get; set; }
    }
    public class SpectrumLiveData
    {
        public string extension { get; set; }
        public int InBoundCalls { get; set; }
        public int OutboundCalls { get; set; }
        public int MissedCalls { get; set; }
        public int totalInBoundsCalls { get; set; }
        public int totalOutBoundsCall { get; set; }
        public int InboundDuration { get; set; }
        public int OutBoundDuration { get; set; }
        public string InBoundHours { get; set; }
        public string OutBoundHours { get; set; }
        public int Agencyid { get; set; }
        public string Location { get; set; }
        public string Date { get; set; }
        public int CallTransfer { get; set; }
        public int TransferCallDuration { get; set; }


    }
    public class SpectrumAgentCall
    {
        public int type { get; set; }
        public DateTime time_start { get; set; }
        public DateTime time_answer { get; set; }
        public DateTime time_release { get; set; }
        public string duration_human { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public int duration { get; set; }
    }
    public class AgencyAgentLogLive
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string LocationAddress { get; set; }
        public int extension { get; set; }
        public int Meeting { get; set; }
        public int Manual_avail { get; set; }
        public int Other { get; set; }
        public int Logins { get; set; }
        public int Web { get; set; }
        public int Breaks { get; set; }
        public int Lunch { get; set; }
        public int Manualsec { get; set; }
        public int Logoutsec { get; set; }
        public int Autosec { get; set; }
        public int Total { get; set; }
        public string MeetingHours { get; set; }
        public string ManualAvailHours { get; set; }
        public string OtherHours { get; set; }
        public string LoginHours { get; set; }
        public string WebHours { get; set; }
        public string BreaksHours { get; set; }
        public string LunchHours { get; set; }
        public string ManualHours { get; set; }
        public string LogoutHours { get; set; }
        public string AutoHours { get; set; }
        public string TotalHours { get; set; }
    }
    public class SpectrumAgentExtensionLog
    {
        public DateTime timestamp { get; set; }
        public string action { get; set; }
        public int loggedin_sec { get; set; }
        public int available_sec { get; set; }
        public int unavailable_sec { get; set; }
        public int break_sec { get; set; }
        public int lunch_sec { get; set; }
        public int meeting_sec { get; set; }
        public int other_sec { get; set; }
        public int acw_sec { get; set; }
        public int web_sec { get; set; }

    }
    public class AgentCallTransfer
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string extension { get; set; }
        public string CallType { get; set; }
        public DateTime time_start { get; set; }
        public DateTime time_answer { get; set; }
        public DateTime time_release { get; set; }
        public int duration { get; set; }
        public string duration_human { get; set; }
        public string orig_to_uri { get; set; }
        public string CallTransferExtension { get; set; }
        public string CallTransferLocation { get; set; }
    }
}
