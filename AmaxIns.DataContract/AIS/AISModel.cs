﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AIS
{
    public class AISModel
    {
        public int? RmId { get; set; }
        public int? ZmId { get; set; }
        public int? Policies { get; set; }
        public decimal? AgencyFee { get; set; }
        public decimal? Premium { get; set; }
        public string Location { get; set; }
        public string Month { get; set; }
        public string Title { get; set; }
        public string AgentName { get; set; }
        public int? SortMonthNum { get; set; }
        public int? AgencyId { get; set; }
        public string Year { get; set; }
        public int? Transactions { get; set; }
        public decimal? AvgAgencyFeeByPolicy { get; set; }
    }
}
