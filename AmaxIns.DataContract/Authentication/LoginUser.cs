﻿using AmaxIns.DataContract.User;
using System;
using System.Collections.Generic;

namespace AmaxIns.DataContract.Authentication
{
    /// <summary>
    /// Login user information including the token
    /// </summary>
    public class LoginUser
    {

        //public int UserId { get; set; }

        //public string UserName { get; set; }

        //public string Token { get; set; }

        //public IEnumerable<DirectoryRole> DirectoryRoles { get; set; }
        //public int OrganizationId { get; set; }

        //public string HomePageUrl { get; set; }

        //public string UserOnboardingUrl { get; set; }

        //public IEnumerable<FunctionalRole> FunctionalRoles { get; set; }

        //public IEnumerable<int> DirectoryRoleIds { get; set; }

        //public IEnumerable<int> FunctionalRoleIds { get; set; }

        //public string DefaultRole { get; set; }

        //public bool IsTemporaryPassword { get; set; }

        //public int TokenRefreshIntervalInMinutes { get; set; }



        // public int DirectoryUserRoleID { get; set; }
        public int UserID { get; set; }
        public int AgentInfoID { get; set; }
        public string AgencyIds { get; set; }
        public int AgencyID { get; set; }
        // public int DirectoryRoleID { get; set; }
        // public string Organization { get; set; }
        public int AgentID { get; set; }
       // public DateTime CreatedDate { get; set; }
       // public DateTime UpdatedDate { get; set; }
        //public string Name { get; set; }
        public string GroupName { get; set; }
       // public string Description { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }

        public string Role { get; set; }
        //public string Email { get; set; }
        //public string Phone { get; set; }
        public string LoginName { get; set; }
        public int? ReferenceId { get; set; }
        //public string Status { get; set; }

        public List<UserAgency> UserAgencyList { get; set; }
        public string otp { get; set; }
        public bool isOTPSent { get; set; }

    }
    public class UserAgency
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
    }
}
