﻿namespace AmaxIns.DataContract.Authentication
{
    /// <summary>
    /// Class for login information.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// User name 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        public string LoginType { get; set; }
        public string Application { get; set; }
    }
}
