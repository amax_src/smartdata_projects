﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Authentication
{
  public  class ResetPassword
    {
        public string loginName { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public string repeatPassword { get; set; }

        public string role { get; set; }

    }
}
