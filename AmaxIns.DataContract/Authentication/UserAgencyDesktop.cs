﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Authentication
{
    public class UserAgencyDesktop
    {
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
    }
}
