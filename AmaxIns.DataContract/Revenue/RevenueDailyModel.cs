﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Revenue
{
   public  class RevenueDailyModel
    {
        public string Actagencyname { get; set; }
        public string Actmonth { get; set; }
        public int? Actnewbusinesscount { get; set; }
        public decimal Actagencyfee { get; set; }
        public decimal Actpremium { get; set; }
        public string ActYear { get; set; }
        public string ActDate { get; set; }
        public string regionalmanagers { get; set; }
        public string zonalmanagers { get; set; }
        public int? RmID { get; set; }
        public int? ZmID { get; set; }
        public int? AgencyID { get; set; }
        public decimal SortMonthNum { get; set; }
        public decimal ProjectionPolicies { get; set; }
        public decimal Projectionagencyfee { get; set; }
        public decimal Projectionpremium { get; set; }

    }


    public class DailyTransactionPayments
    {
        public DateTime DatePayDate { get; set; }
        public string PaymentId { get; set; }
        public string BankAccount { get; set; }
        public string PolicyNumber { get; set; }
        public string ApplicantName { get; set; }
        public string Email { get; set; }
        public string PayType { get; set; }
        public string PayMethod { get; set; }
        public string coName { get; set; }
        public decimal MoneyPayAmount { get; set; }
        public string AgentName { get; set; }
        public string AgencyName { get; set; }
        public int AgencyId { get; set; }
        public string Location { get; set; }
        public string FeeType { get; set; }
        public string SentToCompany { get; set; }
        public decimal MoneyPayFee { get; set; }
        public decimal MoneyPayBalance { get; set; }
        public string PolicyType { get; set; }
        public string ReferralSource { get; set; }
        public string PayNotes { get; set; }
        public string PolicyStatus { get; set; }
        public decimal MoneypayTotal { get; set; }
    }
}
