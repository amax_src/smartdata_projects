﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Revenue
{
    public class RevenueModel
    {
        public string Actagencyname { get; set; }
        public string Actmonth { get; set; }
        public string ActYear { get; set; }
        public int? Actnewbusinesscount { get; set; }
        public double? Actagencyfee { get; set; }
        public double? Actpremium { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public int? New_business_count { get; set; }
        public double Agency_fee { get; set; }
        public double Premium { get; set; }
        public int? SalesPerDay { get; set; }
        public int? CountofAgents { get; set; }
        public int? PoliciesPerAgent { get; set; }
        public int? Sortmonthnum { get; set; }
        public string Regionalmanagers { get; set; }
        public int? Rmid { get; set; }
        public string Zonalmanagers { get; set; }
        public int? Zmid { get; set; }
        public double? Percentage { get; set; }
        public int Agencyid { get; set; }
        public int Tier1Premium { get; set; }
        public int Tier2Premium { get; set; }
        public int Tier3Premium { get; set; }
        public int Tier4Premium { get; set; }
        public int Tier5Premium { get; set; }

        public int GoalPremium { get; set; }
        public int GoalPolicies { get; set; }
        public int GoalAgencyFee{ get; set; }
        public double? policiespercentage { get; set; }
        public double? agencyfeepercentage { get; set; }


    }
}
