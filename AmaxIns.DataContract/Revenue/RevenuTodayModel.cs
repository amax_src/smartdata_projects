﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Revenue
{
    public class RevenuTodayModel
    {
        public string AgencyName { get; set; }
        public int PolicyCount { get; set; }
        public int AgencyFee { get; set; }
        public int Premium { get; set; }

    }
}
