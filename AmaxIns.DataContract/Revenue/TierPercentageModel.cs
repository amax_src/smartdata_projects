﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Revenue
{
    public class TierPercentageModel
    {
        public int Id { get; set; }
        public string TierName { get; set; }
        public decimal TierPercentage { get; set; }
        public string Commission { get; set; }

    }
}
