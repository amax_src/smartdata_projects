﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Revenue
{
    public class AmPmDepositModel
    {
        public string AgencyName { get; set; }
        public double AmDeposit { get; set; }
        public double PmDeposit { get; set; }

    }
}
