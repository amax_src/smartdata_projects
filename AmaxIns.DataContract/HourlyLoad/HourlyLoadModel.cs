﻿using AmaxIns.DataContract.Quotes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.HourlyLoad
{
    public class HourlyLoadModel
    {
        public int Policies { get; set; }
        public decimal Agencyfee { get; set; }
        public decimal Premium { get; set; }
        public int Transactions { get; set; }
        public string Location { get; set; }
        public int Agencyid { get; set; }
        public string Paydate { get; set; }
        public string HourIntervel { get; set; }
        public string Dayofweek { get; set; }

    }
}
