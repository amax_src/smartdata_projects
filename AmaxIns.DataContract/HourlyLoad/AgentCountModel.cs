﻿using AmaxIns.DataContract.Quotes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.HourlyLoad
{
    public class AgentCountModel : HourlyNewModifiedQuotes
    {
        public int AgentCount { get; set; }
        public string AgentNames { get; set; }
    }
}
