﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.HourlyLoad
{
    public class PeakHoursModel
    {

        public int AgencyId{ get;set;}

        public string  Location { get; set; }

        public string PayDate { get; set; }

        public string peakhourpremium { get; set; }

        public int premium { get; set; }
        public string peakhourafee { get; set; }

        public int  agencyfee { get; set; }
        public string peakhourpolicies { get; set; }

        public int policies { get; set; }
        public string peakhourtransactions { get; set; }
         
        public int transactions { get; set; }
        public string peakhourmodifiedquotes { get; set; }

        public int modifiedquotes { get; set; }
        public string peakhournewquotes { get; set; }

        public int newquotes { get; set; }



    }
}
