﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PayRoll
{
    public class PayRollModel
    {
        public int RmId { get; set; }
        public string RegionalManager { get; set; }
        public int ZmId { get; set; }
        public string ZonalManager { get; set; }
        public int Agencyid { get; set; }
        public string Location { get; set; }
        public string Months { get; set; }
        public string CategoryDescription { get; set; }
        public float DebitAmount { get; set; }
        public float DebitHours { get; set; }
        public int Year { get; set; }
        public string PayPeriod { get; set; }
    }
}
