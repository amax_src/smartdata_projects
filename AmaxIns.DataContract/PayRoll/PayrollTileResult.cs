﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PayRoll
{
    public class PayrollTileResult
    {
       public decimal OvertimeperPolicy { get; set; }
       public decimal Overtimehoursperpolicy { get; set; }
        public  decimal PayrollperPolicy { get; set; }
        public  decimal Payrollpertransaction { get; set; }
    }
}
