﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PayRoll
{
    public class CombinePayRollModel
    {   
        public CombinePayRollModel()
        {
            Actual = new List<PayRollModel>();
            Budgted = new List<PayRollModel>();
        }
        public IEnumerable<PayRollModel> Actual{get;set; }
        public IEnumerable<PayRollModel> Budgted { get; set; }

    }
}
