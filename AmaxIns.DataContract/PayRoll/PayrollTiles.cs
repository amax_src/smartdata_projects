﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PayRoll
{
    public class PayrollTiles
    {
        public string ActYear { get; set; }
        public long AgencyID { get; set; }
        public string Location { get; set; }
        public string ActMonth { get; set; }
        public int SortMonthNum { get; set; }
        public int Actnewbusinesscount { get; set; }
        public decimal DebitAmount { get; set; }
        public int DebitHours { get; set; }
        public decimal PayrollPerPolicy { get; set; }
        public int OvertimeHoursPerPolicy { get; set; }
        public int OvertimePerPolicy { get; set; }
        public decimal Transactions { get; set; }

    }
}
