﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.PayRoll
{
    public class ConsolidatedTileData
    {
        public ConsolidatedTileData()
        {
            PayrollAndOverTimePerPolicy = new List<PayrollTiles>();
            PayrollPerPolicy = new List<PayrollTiles>();
            TransPerPolicy = new List<PayrollTiles>();
        }

       public List<PayrollTiles> PayrollAndOverTimePerPolicy { get; set; }
        public List<PayrollTiles> PayrollPerPolicy { get; set; }
        public  List<PayrollTiles> TransPerPolicy { get; set; }
    }
}
