﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.EPR
{
    public class EPRModel
    {
        public int Policies { get; set; }
        public decimal? Agencyfee { get; set; }
        public decimal? Premium { get; set; }
        public string Location { get; set; }
        public int? AgencyID { get; set; }
        public string Csr { get; set; }
        public int? Transactions { get; set; }
        public string Date { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public decimal Avgagencyfeebypolicy { get; set; }
        public int? Policiesbyagent { get; set; }
        public decimal Transactionsbyagent { get; set; }
        public decimal Agencyfeebyagent { get; set; }
        public decimal Premiumbyagent { get; set; }
        public decimal Avgagencyfeebypolicybyagent { get; set; }
        public string RegionalManagers { get; set; }
        public decimal? RegionalManagerID { get; set; }
        public string ZonalManagers { get; set; }
        public decimal? ZonalManagerID { get; set; }
        public int agentID { get; set; }
    }

    public class EPRGraphModel
    {
        public int Policies { get; set; }
        public decimal AgencyFee { get; set; }
        public string Month { get; set; }
        public string PayType { get; set; }
        public decimal eprValue { get; set; }
    }
}
