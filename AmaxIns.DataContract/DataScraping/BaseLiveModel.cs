﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.DataScraping
{
   public class BaseLiveModel
    {
        public int AgencyId { get; set; }
        public string Location { get; set; }
        public string PolicyName { get; set; }
        public string Insured { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime CreatedDate { get; set; }
        public Boolean IsFoundinIP { get; set; }
    }
}
