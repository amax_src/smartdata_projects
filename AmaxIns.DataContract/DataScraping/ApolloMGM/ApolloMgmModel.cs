﻿
using System;
using System.Collections.Generic;
namespace AmaxIns.DataContract.DataScraping.ApolloMGM
{
    public class ApolloMgmModel
    {
        public string Carrier { get; set; }
        public string Policynum { get; set; }
        public string PolicyExpDate { get; set; }
        public string Policyeffdate { get; set; }
        public string Applicant { get; set; }
        public string Paydate { get; set; }
        public string PayMethod { get; set; }
        public string PayAmount { get; set; }
        public string PayFee { get; set; }
        public string PayTotal { get; set; }
        public string PayPaid { get; set; }
        public string PayBalance { get; set; }
        public string NamedInsured { get; set; }
        public string Effective { get; set; }
        public string Expiration { get; set; }
        public string PostedDate { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Payment { get; set; }
        public string Policy { get; set; }
        public string DateoflastPayment { get; set; }
        public DateTime DueDate { get; set; }
        public string BilledMinimumPaymentDue { get; set; }
        public string CurrentMinimumPaymentDue { get; set; }
        public string AmountofLastPayment { get; set; }
        public string BalanceAsOfToday { get; set; }
        public string AmountDue { get; set; }
        public string Location { get; set; }
        public int  AgencyId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}