﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.DataScraping.ApolloMGM
{
    public class ApolloCommissionsModel
    {
        public string Name { get; set; }
        public string Policy { get; set; }
        public string Effective { get; set; }
        public string Type { get; set; }
        public string Premium { get; set; }
        public string PaidPremium { get; set; }
        public string Rate { get; set; }
        public string TotalCommission { get; set; }
        public string CommPaid { get; set; }
        public string CommDue { get; set; }
        public string PremPaidinMonth { get; set; }
        public int AgencyID { get; set; }
        public string Location { get; set; }
    }
}
