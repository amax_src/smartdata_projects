﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.DataScraping.InsurancPro
{
    public class IPModel
    {
        public string Applicant { get; set; }
        public string Agency { get; set; }
        public string Company { get; set; }
        public DateTime Date { get; set; }
        public string PayMethod { get; set; }
        public decimal PayAmount { get; set; }
        public bool IsFoundinIP { get; set; }
        

    }
}

