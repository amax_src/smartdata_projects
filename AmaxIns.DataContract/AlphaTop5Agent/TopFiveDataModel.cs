﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AlphaTop5Agent
{
   public class TopFiveDataModel
    {
        public string AgentName { get; set; }
        public int Amount { get; set; }
    }
}
