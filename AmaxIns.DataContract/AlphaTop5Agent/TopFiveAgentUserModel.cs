﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.AlphaTop5Agent
{
   public class TopFiveAgentUserModel
    {
        public List<int> agencyId { get; set; }
        public List<int> year { get; set; }
        public  List<string> month { get; set; }
    }
}
