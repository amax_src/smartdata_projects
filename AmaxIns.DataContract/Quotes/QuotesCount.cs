﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
   public class QuotesCount
    {
        public string AgencyName { get; set; }
        public int Count { get; set; }
        public List<QuotesModel> quotesModels { get; set; }
    }
}
