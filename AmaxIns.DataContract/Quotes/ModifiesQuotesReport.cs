﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
    public class ModifiesQuotesReport
    {
        public string Identifier { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Windows_record_id { get; set; }
        public string Date_created { get; set; }
        public string Quote_agency_address { get; set; }
        public string Quote_agency_address_city { get; set; }
        public string Agent_name_turborater { get; set; }
        public string Ag_name { get; set; }
        public string Last_modified_date { get; set; }
        public string Agent_id { get; set; }
        public string Agent_location { get; set; }
        public string Count { get; set; }

    }
}
