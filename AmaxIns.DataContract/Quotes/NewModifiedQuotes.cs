﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
    public class NewModifiedQuotes
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string AgencyName { get; set; }
        public string IProAgency { get; set; }
        public string QuoteType { get; set; }
        //public string HomePhone { get; set; }
        public string Phone { get; set; }
        //public string CellNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string TCreateDate { get; set; }
        public string TModifiedDate { get; set; }
        public string CreateDate { get; set; }
        public int cat { get; set; }
        public int cat1 { get; set; }
    }

    public class QuotesProjection
    {
        public string AgencyName { get; set; }
        public int AgencyId { get; set; }
        public int ProjectionYear { get; set; }
        public int ProjectionMonthId { get; set; }
        public string ProjectionMonth { get; set; }
        public int CreatedQuotesProjection { get; set; }
        public decimal CreatedQuotesCR { get; set; }
        public int ModifiedQuotesProjection { get; set; }
        public decimal ModifiedQuotesCR { get; set; }
    }

    public class NewAndModifiedQuotesProj
    {
        public string AgencyName { get; set; }
        public int AgencyId { get; set; }
        public int CreatedQuotesCR { get; set; }
        public int ModifiedQuotesCR { get; set; }
        public int NewActualCR { get; set; }
        public int ModifiedAtualCR { get; set; }
        public int DifferenceNewQuotes { get; set; }
        public int DifferenceModifiedQuotes { get; set; }
        public int TotalNewQuotes { get; set; }
        public int SoldNewQuotes { get; set; }
        public int ClosingRatioNewQuotes { get; set; }
        public int TotalModifiyQuotes { get; set; }
        public int SoldModifiedQuotes { get; set; }
        public int ClosingRatioModQuotes { get; set; }
        public int CreatedPolicies { get; set; }
        public int ModifiedPolicies { get; set; }
        public int DifferenceNewPolicies { get; set; }
        public int DifferenceModifiedPolicies { get; set; }
    }

    public class QuotesParam
    {
        public List<string> Agencyids { get; set; }
        public List<string> dates { get; set; }
    }
}
