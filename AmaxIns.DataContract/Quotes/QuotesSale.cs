﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
    public class QuotesSale
    {
        public string AgencyName { get; set; }
        public int NewQuotes { get; set; }
        public int ModifiedQuotes { get; set; }
        public int NewQuotesSold { get; set; }
        public int ModifiedQuotesSold { get; set; }
        public int SoldByCarrier { get; set; }

    }
}
