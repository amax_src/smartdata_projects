﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
   public  class QuotesModel
    {
        public string identifier { get; set; }
        public string I_quoteuid { get; set; }
        public string I_c_name { get; set; }
        public string I_agencyName { get; set; }
        public string I_paydate { get; set; }
        public string I_c_dob { get; set; }
        public string I_c_Zip { get; set; }
        public string T_agencyName { get; set; }
        public string T_quote_uid { get; set; }
        public string T_agency_address { get; set; }
        public string T_c_zip { get; set; }
        public string T_c_dob { get; set; }
        public string T_date_created { get; set; }
        public string T_c_firstname { get; set; }
        public string T_c_lastname { get; set; }
        public string t_date { get; set; }
        public string T_c_lead_source { get; set; }
        public string T_c_contact_source { get; set; }
    }
}
