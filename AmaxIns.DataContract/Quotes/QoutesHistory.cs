﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.DataContract.Quotes
{
    public class QoutesHistory
    {
        public int identifier { get; set; }
        public int total { get; set; }
        public string quote_uid { get; set; }
        public string last_quoted_by_last_name { get; set; }
        public int counter { get; set; }
        public string quote_agency_address { get; set; }
        public string agencyName { get; set; }
        public string addresss { get; set; }
        public string last_modified_date { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string date_created { get; set; }
        public string c_lead_source { get; set; }
        public string c_contact_source { get; set; }
        public string c_zip { get; set; }
        public string c_dob { get; set; }
    }
}
