import { Component, OnInit } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { LiveDataAgency } from '../../Model/LiveDataAgency';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  data: Array<LiveDataAgency> = new Array<LiveDataAgency>();
  filterdata: Array<LiveDataAgency> = new Array<LiveDataAgency>();
  Topthree: Array<LiveDataAgency> = new Array<LiveDataAgency>();
  TotalagencyFee: number = 0;
  TotalpolicyCount: number = 0;
  Totalpremium: number = 0;
  showLoader: boolean = true;
  deviceInfo = null;
  AgencyList: Array<string> = new Array<string>();
  selectedAgency: string = '';
  AgencyItem: SelectItem[] = [];
  refreshTime: string = "";
  TotalModifiedQuotes: number = 0;
  TotalNewQuotes: number = 0
  TotalFee: number = 0;
  TotalNewCount: number = 0;
  Totalprem: number = 0;
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";

  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute) {
  }


  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });
    //if (!this.userId) {
    //  this.userId = this._auth.userId.toString();
    //}
    //if (!this.userRole) {
    //  this.userRole = this._auth.userRole;
    //}

    this.selectedAgency = "Select All";
    this.GetRefeshTime();
    this.service.getData(this.userId, this.userRole).subscribe(m => {
      this.data = m;
      this.filterdata = this.data;
      //console.log(this.filterdata);
      this.FillDropDown();
      this.GetTopfthree();
      this.GetTotal()
      this.showLoader = false;
      this.getTotalQuotes();
    },
      error => {
        this.router.navigateByUrl("/");
      })
  }

  onSelect(e) {
    this.filterdata = new Array<LiveDataAgency>();
    if (this.AgencyList.length === 0) {
      this.filterdata = this.data;
    }
    else {
      this.AgencyList.forEach(m => {
        let tempfilterdata = new Array<LiveDataAgency>()
        tempfilterdata = this.data.filter(z => z.agencyName === m)
        if (tempfilterdata.length > 0) {
          tempfilterdata.forEach(a => {
            this.filterdata.push(a);
          })
        }
      })
    }
    this.getTotalQuotes();
  }

  FillDropDown() {
    this.AgencyItem = [];
    this.data.forEach(m => {
      this.AgencyItem.push(
        {
          label: m.agencyName, value: m.agencyName
        })
    })
  }

  GetTopfthree() {

    this.Topthree = new Array<LiveDataAgency>();
    this.data.forEach(m => {
      if (m.policyCount > 0) {
        if (this.Topthree.length < 3) {
          this.Topthree.push(m);
        }
      }
    })
  }

  GetTotal() {
    this.data.forEach(m => {
      this.TotalagencyFee = this.TotalagencyFee + (isNaN(m.agencyFee) ? 0 : m.agencyFee);
      this.TotalpolicyCount = this.TotalpolicyCount + (isNaN(m.policyCount) ? 0 : m.policyCount);
      this.Totalpremium = this.Totalpremium + (isNaN(m.premium) ? 0 : m.premium);
    })
  }

  GetRefeshTime() {
    this.service.getRefeshTime().subscribe(m => {
      this.refreshTime = m;
    })

  }

  getTotalQuotes() {
    this.TotalFee = 0;
    this.TotalNewCount = 0;
    this.Totalprem = 0;
    this.TotalModifiedQuotes = 0;
    this.TotalNewQuotes = 0;
    this.filterdata.forEach(m => {
      this.TotalModifiedQuotes = this.TotalModifiedQuotes + m.modifiedQuotes;
      this.TotalNewQuotes = this.TotalNewQuotes + m.newQuotes;
      this.TotalFee = this.TotalFee + (isNaN(m.agencyFee) ? 0 : m.agencyFee);
      this.TotalNewCount = this.TotalNewCount + (isNaN(m.policyCount) ? 0 : m.policyCount);
      this.Totalprem = this.Totalprem + (isNaN(m.premium) ? 0 : m.premium);
    })

  }

  sortDirection: string = "desc";
  sortBy: string = "";
  sortData(e) {

    if (this.sortBy === e) {
      if (this.sortDirection === "desc") {
        this.sortDirection = "asc";
      }
      else {
        this.sortDirection = "desc";
      }
    }
    this.sortBy = e;

    let Data: Array<LiveDataAgency> = new Array<LiveDataAgency>();
    switch (e) {
      case "Policies":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByPolicy);
          }
          else {
            Data = this.filterdata.sort(this.sortByPolicy);
          }
        }
        break
      case "AgencyFee":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByAgencyFee);
          }
          else {
            Data = this.filterdata.sort(this.sortByAgencyFee);
          }
        }
        break
      case "Premium":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByPremium);
          }
          else {
            Data = this.filterdata.sort(this.sortByPremium);
          }
        }
        break
      case "ModifiesQuotes":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByModifiesQuotes);
          }
          else {
            Data = this.filterdata.sort(this.sortByModifiesQuotes);
          }
        }
        break
      case "NewQuotes":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByNewQuotes);
          }
          else {
            Data = this.filterdata.sort(this.sortByNewQuotes);
          }
        }
        break
    }

    this.filterdata = Data;

  }


  sortdescByPolicy(a, b) {
    return b["policyCount"] - a["policyCount"];
  }

  sortByPolicy(a, b) {
    return a["policyCount"] - b["policyCount"];
  }

  sortdescByAgencyFee(a, b) {
    return b["agencyFee"] - a["agencyFee"];
  }

  sortByAgencyFee(a, b) {
    return a["agencyFee"] - b["agencyFee"];
  }

  sortdescByPremium(a, b) {
    return b["premium"] - a["premium"];
  }

  sortByPremium(a, b) {
    return a["premium"] - b["premium"];
  }

  sortdescByModifiesQuotes(a, b) {
    return b["modifiedQuotes"] - a["modifiedQuotes"];
  }

  sortByModifiesQuotes(a, b) {
    return a["modifiedQuotes"] - b["modifiedQuotes"];
  }

  sortdescByNewQuotes(a, b) {
    return b["newQuotes"] - a["newQuotes"];
  }

  sortByNewQuotes(a, b) {
    return a["newQuotes"] - b["newQuotes"];
  }

}
