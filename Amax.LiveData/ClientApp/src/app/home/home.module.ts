import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
@NgModule({
  imports: [
    MultiSelectModule,
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
   
  ],
  declarations: [HomeComponent],
  exports: []
 })
export class HomeModule { }
