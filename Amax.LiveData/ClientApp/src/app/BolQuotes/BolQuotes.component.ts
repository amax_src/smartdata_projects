import { Component, OnInit } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { BolData } from '../../Model/BolQuotes';
import { NameValue } from '../../Model/NameValue';
import { SelectItem } from 'primeng/api';
import { concat } from 'rxjs/operators';
import { DownloadexcelService } from 'src/services/downloadexcel.service';
@Component({
  selector: 'app-BolQuotes',
  templateUrl: './BolQuotes.component.html',
})
export class BolQuotesComponent implements OnInit {
    ShowExportButton: boolean = false;
  showLoader: boolean = false;
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";

  PolicyCount: number = 0;
  BuyNowOption: number = 0;
  BuyNowClick: number = 0;
  Total: number = 0;
  totalpremium: number = 0;
  totalCollectedPremium: number = 0;
  totalAfee: number = 0;

  BolQuotes: Array<BolData> = new Array<BolData>();
  Carriers: Array<string> = new Array<string>();
  States: Array<string> = new Array<string>();
  CarriersItem: SelectItem[] = [];
  StateItem: SelectItem[] = [];

  FilerData: Array<BolData> = new Array<BolData>();
  count: number = 0;
  date: Array<Date>;

  delayedPolicy: number = 0;
  txState: number = 0;
    azState: number = 0;
    ilState: number = 0;
    cAState: number = 0;

    txQuotes: number = 0;
    azQuotes: number = 0;
    ilQuotes: number = 0;
    cAQuotes: number = 0;

    txvisitedQuotes: number = 0;
    azvisitedQuotes: number = 0;
    ilvisitedQuotes: number = 0;
    cAvisitedQuotes: number = 0;

    constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService
        , private _auth: authenticationService, private encryp: encryption, private _downloadExcel: DownloadexcelService,
    private route: ActivatedRoute) {
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

    //if (!this.userId) {
    //  this.userId = this._auth.userId.toString();
    //}
    //if (!this.userRole) {
    //  this.userRole = this._auth.userRole;
    //}
    this.BindState();
    this.GetBolData();
  }

  BindState() {
    this.StateItem.push(
      {
        label: 'TX', value: 'TX'
      }, {
        label: 'AZ', value: 'AZ'
    }, {
        label: 'IL', value: 'IL'
    }, {
        label: 'CA', value: 'CA'
    })
  }

  GetCarrierData() {
    this.service.GetCarrierNames().subscribe(m => {
      this.CarriersItem = [];
      m.forEach(m => {
        this.CarriersItem.push(
          {
            label: m.name, value: m.id
          })
      })
      
    })
  }

  //GetBolData() {
    
  //  if (this.Carriers.length == 0) {
  //      this.GetCarrierData();
  //  }
  //  if (this.date && this.date[0] != null && this.date[1] != null) {
  //    this.showLoader = true;
  //    this.totalAfee = 0;
     
  //    this.service.BindOnLineQuotes(this.Carriers, this.date,this.States).subscribe(m => {
  //      this.BolQuotes = m;
  //      let BolQuotesFilter: Array<BolData> = new Array<BolData>();
  //      BolQuotesFilter = this.BolQuotes.filter(m => m.policyNumber !== '' && m.buyNowClick === true);
  //      this.PolicyCount = BolQuotesFilter.length;

  //      let _txState = this.BolQuotes.filter(m => m.state === "TX" && m.policyNumber !== '' && m.buyNowClick === true);
  //      this.txState = _txState.length;

  //      let _azState = this.BolQuotes.filter(m => m.state === "AZ" && m.policyNumber !== '' && m.buyNowClick === true);
  //      this.azState = _azState.length;

  //      BolQuotesFilter = new Array<BolData>();
  //      BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowClick === true);
  //      this.BuyNowClick = BolQuotesFilter.length;

  //      BolQuotesFilter = new Array<BolData>();
  //      BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowOption === true);
  //      this.BuyNowOption = BolQuotesFilter.length;

  //      this.Total = this.BolQuotes.length;
  //      this.FilerData = this.BolQuotes.filter(m => m.buyNowClick === true);
  //      this.showLoader = false;
  //      let delayed = this.FilerData.filter(m => m.isdelayed === true);
  //      this.delayedPolicy = delayed.length;
  //      //TX AZ
  //      this.totalAfee=0;
  //      this.FilerData.forEach(z => {
  //        this.totalAfee = this.totalAfee + z.agencyFee;
  //      })
  //    })
  //  }
  //}

    GetBolData() {
        this.ShowExportButton = false;
    this.FilerData = new Array<BolData>();
    this.BolQuotes = new Array<BolData>();
    this.Total = 0;
    this.totalAfee = 0; this.totalpremium = 0; this.totalCollectedPremium = 0;
    this.BuyNowClick = 0;
        this.BuyNowOption = 0; this.PolicyCount = 0; this.txState = 0; this.azState = 0; this.ilState = 0; this.cAState = 0;
        this.txQuotes = 0;
        this.azQuotes = 0;
        this.ilQuotes = 0;
        this.cAQuotes = 0;

        this.txvisitedQuotes = 0;
        this.azvisitedQuotes = 0;
        this.ilvisitedQuotes = 0;
        this.cAvisitedQuotes = 0;
       
    if (this.Carriers.length == 0) {
      this.GetCarrierData();
    }
    if (
      (this.date && this.date[0] != null && this.date[1] != null && this.Carriers.length > 0)
      || (this.date && this.date[0] != null && this.date[1] != null && this.States.length > 0)
      || (this.date && this.date[0] != null && this.date[1] != null && this.States.length > 0 && this.Carriers.length > 0)
    ) {
      this.showLoader = true;
     

      this.service.GetDataBolData_Quotes(this.Carriers, this.date, this.States).subscribe(m => {
        this.BolQuotes = m;

        console.log(this.BolQuotes);
        let BolQuotesFilter: Array<BolData> = new Array<BolData>();
        BolQuotesFilter = this.BolQuotes.filter(m => m.policyNumber !== '' && m.buyNowClick === true);
        this.PolicyCount = BolQuotesFilter.length;

        let _txState = this.BolQuotes.filter(m => m.state === "TX" && m.policyNumber !== '' && m.buyNowClick === true);
        this.txState = _txState.length;

        let _azState = this.BolQuotes.filter(m => m.state === "AZ" && m.policyNumber !== '' && m.buyNowClick === true);
        this.azState = _azState.length;

          let _ilState = this.BolQuotes.filter(m => m.state === "IL" && m.policyNumber !== '' && m.buyNowClick === true);
          this.ilState = _ilState.length;

          let _cAState = this.BolQuotes.filter(m => m.state === "CA" && m.policyNumber !== '' && m.buyNowClick === true);
          this.cAState = _cAState.length;

        BolQuotesFilter = new Array<BolData>();
        BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowClick === true);
          this.BuyNowClick = BolQuotesFilter.length;


          let _txQuotes = this.BolQuotes.filter(m => m.state === "TX");
          this.txQuotes = _txQuotes.length;

          let _azQuotes = this.BolQuotes.filter(m => m.state === "AZ");
          this.azQuotes = _azQuotes.length;

          let _ilQuotes = this.BolQuotes.filter(m => m.state === "IL");
          this.ilQuotes = _ilQuotes.length;

          let _cAQuotes = this.BolQuotes.filter(m => m.state === "CA");
          this.cAQuotes = _cAQuotes.length;

          let _txVisitedQuotes = this.BolQuotes.filter(m => m.state === "TX" && m.buyNowClick === true);
          this.txvisitedQuotes = _txVisitedQuotes.length;

          let _azVisitedQuotes = this.BolQuotes.filter(m => m.state === "AZ" && m.buyNowClick === true);
          this.azvisitedQuotes = _azVisitedQuotes.length;

          let _ilVisitedQuotes = this.BolQuotes.filter(m => m.state === "IL" && m.buyNowClick === true);
          this.ilvisitedQuotes = _ilVisitedQuotes.length;

          let _cAVisitedQuotes = this.BolQuotes.filter(m => m.state === "CA" && m.buyNowClick === true);
          this.cAvisitedQuotes = _cAVisitedQuotes.length;


        BolQuotesFilter = new Array<BolData>();
        //BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowOption === true);
        BolQuotesFilter = this.BolQuotes;
        this.BuyNowOption = BolQuotesFilter.length;

        this.Total = this.BolQuotes.length;
        this.FilerData = this.BolQuotes.filter(m => m.buyNowClick === true);
        this.showLoader = false;
        let delayed = this.FilerData.filter(m => m.isdelayed === true);
        this.delayedPolicy = delayed.length;
        //TX AZ
        this.totalAfee = 0; this.totalpremium = 0; this.totalCollectedPremium = 0;
        this.FilerData.forEach(z => {
          this.totalAfee = this.totalAfee + z.agencyFee;
          this.totalpremium = this.totalpremium + z.premiumAmount;
          this.totalCollectedPremium = this.totalCollectedPremium + z.collectedPremium;
        })

          this.ShowExportButton = true;
      })
    }
  }

  onSelect(e) {
    this.GetBolData();
  }
  onStateSelect(e) {
    this.GetBolData();
  }

  onDateSelect(e) {
    //this.date = e;
    this.GetBolData();

  }
    SelectBackGround(e) {
        if (e.isdelayed) {
            return 'green'
        }
        else if (e.isQuoteDataMissing) {
            return 'red'
        }
        else {
            return ''
        }
    }

    exportExcel() {
        let data = [];
        data = this.FilerData;
        this.service.ExportOnlineQuote(data).subscribe(x => {
            this._downloadExcel.downloadFile(x);
            console.log("FileName", x);
            this.showLoader = false;
        })
    }
}
