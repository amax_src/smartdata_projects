import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BolQuotesComponent } from "./BolQuotes.component";


const routes: Routes = [
  { path: 'BindOnlineQuotes', component: BolQuotesComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BolQuotesRoutingModule { }
