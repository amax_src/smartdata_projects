import { NgModule } from '@angular/core';
import { BolQuotesComponent } from './BolQuotes.component';
import { BolQuotesRoutingModule } from './BolQuotes-routing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { CalendarModule } from 'primeng/calendar';
@NgModule({
  imports: [
    MultiSelectModule,
    CommonModule,
    FormsModule,
    BolQuotesRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    CalendarModule,
  ],
  declarations: [BolQuotesComponent],
  exports: []
 })
export class BolQuotesModule { }
