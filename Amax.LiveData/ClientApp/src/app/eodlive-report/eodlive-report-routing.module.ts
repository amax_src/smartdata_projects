import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EodLiveReportComponent } from './eod-live-report.component';


const routes: Routes = [{ path: 'EODLiveReport', component: EodLiveReportComponent }]//, ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EodliveReportRoutingModule { }
