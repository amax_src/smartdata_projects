import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EodliveReportRoutingModule } from './eodlive-report-routing.module';
import { EodLiveReportComponent } from './eod-live-report.component';

import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [EodLiveReportComponent],
  imports: [
    CommonModule,
    EodliveReportRoutingModule,
     MultiSelectModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    CalendarModule,
  ]
})
export class EodliveReportModule { }
