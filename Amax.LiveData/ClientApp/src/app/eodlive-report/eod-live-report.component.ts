import { Component, OnInit, HostListener } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { BolData } from '../../Model/BolQuotes';
import { EODLiveReport } from '../../Model/EODLiveReport';
import { SelectItem } from 'primeng/api';
import { concat } from 'rxjs/operators';
import {PageService } from './../../services/pageService';

@Component({
  selector: 'app-eod-live-report',
  templateUrl: './eod-live-report.component.html',
  styleUrls: ['./eod-live-report.component.css']
})
export class EodLiveReportComponent implements OnInit {
  showLoader: boolean = false;
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "null";
  userRole: string = "null";
  date: Array<Date>;
  eODLiveReport: Array<EODLiveReport>;
  filtereODLiveReport: Array<EODLiveReport>;
  AgencyList: Array<string> = new Array<string>();
  selectedAgency: string = '';
  AgencyItem: SelectItem[] = [];

  innerHeight: number = 0;
  innercontainer: string = "400px";

  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute, private _pageService: PageService) {
  }

  ngOnInit() {
    this.innerHeight = (window.innerHeight - 300);
    this.innercontainer = this.innerHeight.toString() + "px";

    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

    this.getAgency();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  getAgency() {
    this.service.getAgency(this.userId, this.userRole).subscribe(data => {
      data.forEach(m => {
        this.AgencyItem.push(
          {
            label: m.agencyName, value: m.agencyId
          })
      })

      console.log("this.AgencyItem", this.AgencyItem);
    },
      error => {
        this.router.navigateByUrl("/");
      })
  }
  onDateSelect(e) {
    //this.date = e;
    

  }

  SearchData() {
    this.GetEODLiveReport();
  }
  GetEODLiveReport() {
    if (this.date && this.date[0] != null && this.date[1] != null) {
      this.showLoader = true;
      this.service.GetEODLiveReport(this.date).subscribe(m => {
        this.eODLiveReport = m;
        this.showLoader = false;
       
        this.FilterData();
      });
    }
  }

  onSelect(e) {

  }

  FilterData() {
    this.filtereODLiveReport = new Array<EODLiveReport>();
    if (this.AgencyList.length == 0) {
      this.AgencyItem.forEach(m => {
        var listof = this.eODLiveReport.filter(x => x.agencyId == parseInt(m.value));
        listof.forEach(j => {
          this.filtereODLiveReport.push(j);
        })
      })
    }
    else {
      this.AgencyList.forEach(m => {
        var listof = this.eODLiveReport.filter(x => x.agencyId == parseInt(m));
        listof.forEach(j => {
          this.filtereODLiveReport.push(j);
        })
      })
    }
    console.log("AgencyList", this.AgencyList);
    console.log("AgencyList", this.filtereODLiveReport);
    this.setPolicyCountPaging(1);
  }

  pagerPolicyCount: any = {};
  pagedPolicyCount: any[];

  setPolicyCountPaging(page: number) {
    if (page < 1 || page > this.pagerPolicyCount.totalPages) {
      return;
    }
    // get pager object from service
    this.pagerPolicyCount = this._pageService.getPayrollPager(this.filtereODLiveReport.length, page, 100);
    // get current page of items
    this.pagedPolicyCount = this.filtereODLiveReport.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);
  }

}
