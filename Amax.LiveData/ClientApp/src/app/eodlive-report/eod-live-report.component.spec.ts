import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EodLiveReportComponent } from './eod-live-report.component';

describe('EodLiveReportComponent', () => {
  let component: EodLiveReportComponent;
  let fixture: ComponentFixture<EodLiveReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EodLiveReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EodLiveReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
