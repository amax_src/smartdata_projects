import { Component, OnInit } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { AmaxHourlyLoad } from '../../Model/AmaxHourlyLoad';
import { SelectItem } from 'primeng/api';
import { DownloadexcelService } from '../../services/downloadexcel.service';
@Component({
  selector: 'app-AmaxAgencyHourly',
  templateUrl: './AmaxAgencyHourly.component.html',
})
export class AmaxAgencyHourly implements OnInit {
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";
  showLoader: boolean=true;
  refreshTime: string;
  data: Array<AmaxHourlyLoad>;
  filterdata: Array<AmaxHourlyLoad> = new Array<AmaxHourlyLoad>();
  selectedAgency: string = '';    
  TotalNewCount: number = 0
  Totalprem: number = 0;
  TotalFee: number = 0;
  TotalTrans: number = 0;
  ConsolatedChartData: any;
  displayLinegraphAmaxHourly: boolean = false;
  AgencyItem: SelectItem[] = [];
  AgencyList: Array<string> = new Array<string>();

  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'black',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context && (context.dataset.label === "Agency Fee" || context.dataset.label === "Premium")) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
          else {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          if (data && data.datasets && data.datasets.length > 0 && (data.datasets[tooltipItem.datasetIndex].label === "Agency Fee" || data.datasets[tooltipItem.datasetIndex].label === "Premium")) {
            return data.datasets[tooltipItem.datasetIndex].label + `- $${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
          else {
            return data.datasets[tooltipItem.datasetIndex].label + `- ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

          }
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: "#000"
        },
        scaleLabel: {
          display: true,
          fontColor: "#000"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#000"
        }
      }]
    }

  }

  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute, private _downloadExcel: DownloadexcelService) {
  }


  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

    this.data = new Array<AmaxHourlyLoad>();
    this.TotalNewCount = 0;
    this.Totalprem = 0;
    this.TotalFee = 0;
    this.TotalTrans = 0;
    this.selectedAgency = "Select All";

    this.GetRefeshTime();
    this.service.GetAmaxLiveAppAgencyWise(this.userId, this.userRole).subscribe(m => {
      this.data = m;
      this.filterdata = this.data;
      this.FillDropDown();
      this.showLoader = false;
      this.data.forEach((e, i) => {

        this.TotalNewCount = this.TotalNewCount + e.policies;
        this.Totalprem = this.Totalprem + e.premium;
        this.TotalFee = this.TotalFee + e.agencyfee;
        this.TotalTrans = this.TotalTrans + e.transactions;
      });

    },
      error => {
        this.router.navigateByUrl("/");
      })
  }

  GetRefeshTime() {
    this.service.getRefeshTime().subscribe(m => {
      this.refreshTime = m;
    })
  }

  onSelect(e) {
    this.filterdata = new Array<AmaxHourlyLoad>();
    if (this.AgencyList.length === 0) {
      this.filterdata = this.data;
    }
    else {
      this.AgencyList.forEach(m => {
        let tempfilterdata = new Array<AmaxHourlyLoad>()
        tempfilterdata = this.data.filter(z => z.location === m)
        if (tempfilterdata.length > 0) {
          tempfilterdata.forEach(a => {
            this.filterdata.push(a);
          })
        }
      })
    }
//    this.getTotalQuotes();
  }

  FillDropDown() {
    this.AgencyItem = [];
    this.data.forEach(m => {
      let dataexist = this.AgencyItem.filter(x => x.value === m.location);

      if (dataexist.length === 0) {
        this.AgencyItem.push(
          {
            label: m.location, value: m.location
          })
      }
      
    })
  }

  TabChange(e) {
    if (e === 1) {
      this.CreatChartViewForAmax();
    }
  }

  CreatChartViewForAmax() {

    let Intervel = [];
    let Transactions = []
    let Premium = []
    let Policies = []
    let AgencyFee = []
    let AgentCount = []
    this.data.forEach((e, i) => {
      Intervel.push(e.hourIntervel);
      Transactions.push(e.transactions)
      Premium.push(e.premium)
      Policies.push(e.policies)
      AgencyFee.push(e.agencyfee)
      AgentCount.push(e.agentCount)
    })

    this.ConsolatedChartData = {
      labels: Intervel,
      datasets: [
        {
          label: 'Policies',
          data: Policies,
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'Agency Fee',
          data: AgencyFee,
          fill: false,
          borderColor: '#565656'
        }
        ,
        {
          label: 'Premium',
          data: Premium,
          fill: false,
          borderColor: '#42A5F5'
        }
        ,
        {
          label: 'Transactions',
          data: Transactions,
          fill: false,
          borderColor: '#9CCC65'
        },
        {
          label: 'Agents',
          data: AgentCount,
          fill: false,
          borderColor: '#9FFC65'
        }
      ]
    }
  }

  exportExcel() {
    this.service.ExportHourlyDataAgencyWise(this.filterdata).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      console.log("FileName", x);
      this.showLoader = false;
    })
  }
}
