import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AmaxAgencyHourly } from "./AmaxAgencyHourly.component";


const routes: Routes = [
  { path: 'AgencyHourly', component: AmaxAgencyHourly}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AmaxAgencyHourlyRoutingModule { }
