import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { ChartModule } from 'primeng/chart';
import { MatTabsModule } from '@angular/material'
import { AmaxAgencyHourlyRoutingModule } from './AmaxAgencyHourly-routing.module';
import { AmaxAgencyHourly } from './AmaxAgencyHourly.component';

@NgModule({
  imports: [
    MultiSelectModule,
    MultiSelectModule,
    CommonModule,
    FormsModule,
    AmaxAgencyHourlyRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    ChartModule,
    MatTabsModule
  ],
  declarations: [AmaxAgencyHourly],
  exports: []
 })
export class AmaxAgencyHourlyModule { }
