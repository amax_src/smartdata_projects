import { Component, OnInit,Pipe } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { Agency, AgencyAgentLog, AgentCallTransfer, RegionalManagers, SaleDirector, SpectrumAgentExtensionLog, SpectrumLiveAgentCallDetails, SpectrumLiveData, ZonalManager } from '../../Model/SpectrumLiveData';
import { LiveDataService } from '../../services/LiveDataService';
import { DownloadexcelService } from 'src/services/downloadexcel.service';
import { DatePipe } from '@angular/common';
@Component({
    selector: 'app-SpectrumLiveDataNew',
    templateUrl: './SpectrumLiveDataNew.component.html',
    providers: [DatePipe]
})
export class SpectrumLiveDataNewComponent implements OnInit {

    stateSelectItem: SelectItem[] = [];
    state: string = 'TX';

    cadata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    cafilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caagencyfilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    caagentWiseData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caagentWisefilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caliveSpectrumExtensionData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caliveSpectrumExtensionfilterData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    caagentExtensionWiseData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caagentWiseWeeklyData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caagentWiseWeeklyfilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    caagentWiseWeeklyfilterdataStored: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    agentLogAgencyWiseDataCA: Array<AgencyAgentLog> = new Array<AgencyAgentLog>();
    filterAgentLogAgencyWiseDataCA: Array<AgencyAgentLog> = new Array<AgencyAgentLog>();

    caagencyData: Array<Agency> = new Array<Agency>();
    caAgencyList: Array<string> = new Array<string>();
    caAgencyItem: SelectItem[] = [];

    catotalInbound: number = 0;
    catotalOutbound: number = 0;
    catotalInboundCall: number = 0;
    catotalOutboundCall: number = 0;
    catotalmissedCalls: number = 0;
    catotalTransferCalls: number = 0;
    caagentWisetotalInbound: number = 0;
    caagentWisetotalOutbound: number = 0;
    caagentWisetotalInboundCall: number = 0;
    caagentWisetotalOutboundCall: number = 0;
    caagentWisemissedCall: number = 0;
    caagentwiseInBoundCallDuration: number = 0;
    caagentwiseOutBoundCallDuration: number = 0;
    caagentwiseTotalTransferCall: number = 0;
    caagentwiseTotalTransferCallDuration: number = 0;

    caweeklytotalInbound: number = 0;
    caweeklytotalOutbound: number = 0;
    caweeklytotalInboundCall: number = 0;
    caweeklytotalOutboundCall: number = 0;
    caweeklytotalMissedCall: number = 0;

    data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    filterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    agentWiseData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    agentWisefilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    agentExtensionWiseData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    agentWiseWeeklyData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    agentWiseWeeklyfilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    agentWiseWeeklyfilterdataStored: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    singleAgencyData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    singleAgencyfilterData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    liveSpectrumExtensionData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    liveSpectrumExtensionfilterData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    spectrumLiveAgentCallDetailsData: Array<SpectrumLiveAgentCallDetails> = new Array<SpectrumLiveAgentCallDetails>();
    spectrumLiveAgentCallDetailsfilterData: Array<SpectrumLiveAgentCallDetails> = new Array<SpectrumLiveAgentCallDetails>();

    SpectrumAgentExtensionLogData: Array<SpectrumAgentExtensionLog> = new Array<SpectrumAgentExtensionLog>();
    
    agentLogAgencyWiseData: Array<AgencyAgentLog> = new Array<AgencyAgentLog>();
    filterAgentLogAgencyWiseData: Array<AgencyAgentLog> = new Array<AgencyAgentLog>();

    agentCallTransferData: Array<AgentCallTransfer> = new Array<AgentCallTransfer>();

    saleDirector: Array<SaleDirector>;
    saleDirectorSelectItem: SelectItem[] = [];
    selectedSaleDirector: Array<number> = [];
    selectedSaleDirectortemp: Array<number> = [];

    regionalManagersData: Array<RegionalManagers> = new Array<RegionalManagers>();
    selectedRegionalManager: Array<number> = new Array<number>();
    allRegionalManagerIds: Array<number> = new Array<number>();
    regionalManagerSelectItem: SelectItem[] = [];

    selectedZonalManager: Array<number> = new Array<number>();
    allZonalManagerIds: Array<number> = new Array<number>();
    zonalManagerSelectItem: SelectItem[] = [];
    zonalManager: Array<ZonalManager>;

    agencyData: Array<Agency> = new Array<Agency>();

    listWeek: SelectItem[] = [];
    listDate: SelectItem[] = [];

    showLoader: boolean = true;
    deviceInfo = null;
    AgencyList: Array<string> = new Array<string>();
    selectedAgency: string = '';
    AgencyItem: SelectItem[] = [];
    refreshTime: string = "";

    userIdencrypted: string = "";
    userRoleencrypted: string = "";
    userId: string = "";
    userRole: string = "";
    totalInbound: number = 0;
    totalOutbound: number = 0;
    totalInboundCall: number = 0;
    totalOutboundCall: number = 0;
    totalmissedCalls: number = 0;
    totalTransferCalls: number = 0;

    agentWisetotalInbound: number = 0;
    agentWisetotalOutbound: number = 0;
    agentWisetotalInboundCall: number = 0;
    agentWisetotalOutboundCall: number = 0;
    agentWisemissedCall: number = 0;
    agentwiseInBoundCallDuration: number = 0;
    agentwiseOutBoundCallDuration: number = 0;
    agentwiseTotalTransferCall: number = 0;
    agentwiseTotalTransferCallDuration: number = 0;

    weeklytotalInbound: number = 0;
    weeklytotalOutbound: number = 0;
    weeklytotalInboundCall: number = 0;
    weeklytotalOutboundCall: number = 0;
    weeklytotalMissedCall: number = 0;

    weeklytotallocationWiseInbound: number = 0;
    weeklytotallocationWiseOutbound: number = 0;
    weeklytotallocationWiseInboundCall: number = 0;
    weeklytotallocationWiseOutboundCall: number = 0;
    weeklytotallocationWiseMissedCall: number = 0;

    TotalMeetingHours: number = 0;
    TotalManualAvailHours: number = 0;
    TotalOtherHours: number = 0;
    TotalLoginHours: number = 0;
    TotalWebHours: number = 0;
    TotalBreaksHours: number = 0;
    TotalLunchHours: number = 0;
    TotalManualHours: number = 0;
    TotalLogoutHours: number = 0;
    TotalAutoHours: number = 0;
    TotalTotalHours: number = 0;

    weekId: string = '0';
    dateId: string = 'Select Date';
    startDate: Date;
    endDate: Date;

    warningPopup: boolean = false;
    AgentCallPopup: boolean = false;
    AgentLogLiveCallPopup: boolean = false;
    AgentCallsTranserPopup: boolean = false;

    agencyName: string = '';
    extension: string = '';
    tabindex: number = 0;
    date: Array<Date>;
    constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
        private route: ActivatedRoute, private datePipe: DatePipe, private _downloadExcel: DownloadexcelService) {

    }


    ngOnInit(): void {
        this.fillState();
        this.tabindex = 0;
        let date: Date = new Date();
        this.date = new Array<Date>();
        if (this.weekId == "0") {
            this.startDate = new Date(date.setDate(date.getUTCDate() - 7));
            date = new Date();
            this.endDate = new Date(date.setDate(date.getUTCDate() - 1));

            this.date.push(this.startDate); this.date.push(this.endDate);
        }

        this.route.queryParams.subscribe((data) => {
            this.userIdencrypted = data['userId'];
            this.userRoleencrypted = data['UserRole'];
            this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
            this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
        });

        if (this.userRole === undefined) {
            this.getAllSaleDirectors();
            this.getAllRegionalManagers();
            this.getAllZonalManagers();
            this.getAllLocations();
        }
        else if (this.userRole === 'headofdepratment') {
            this.getAllSaleDirectors();
            this.getAllRegionalManagers();
            this.getAllZonalManagers();
            this.getAllLocations();
        }
        else if (this.userRole === 'saledirector') {
            this.selectedSaleDirector = new Array<number>();
            this.selectedSaleDirector.push(parseInt(this.userId));
            this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
        }
        else if (this.userRole === 'regionalmanager') {
            this.allRegionalManagerIds.push(parseInt(this.userId));
            this.getZonalManagerByRegionalManagers(this.allRegionalManagerIds);
        }
        else if (this.userRole === 'zonalmanager') {
            this.allZonalManagerIds.push(parseInt(this.userId));
            this.getAllLocationsRegionalZonalManager(null, this.allZonalManagerIds);
        }
        else if (this.userRole === 'storemanager') {
            this.getStoreManagerAgency(parseInt(this.userId));
        }

        this.weekId = '0';
        this.getWeek();


        this.selectedAgency = "Select All";
        this.GetRefeshTime();
        this.service.getSpectrumDataNewV2(this.userId, this.userRole).subscribe(m => {
            this.data = m;
            if (this.data) {
                this.filterdata = this.data;
                console.log("this.filterdata", this.filterdata)
                this.GetTotal()
                this.showLoader = false;
                this.sortData("Incoming");
                this.GetAgentWiseData();
                this.GetAgentWiseWeeklyData();
                this.GetAgentLogLive();

                this.GetCASpectrumV2();
                this.GetAgentWiseDataCA();
                this.GetAgentLogLiveCA();
                this.GetAgentWiseWeeklyDataCA();
            }
            else {
                this.showLoader = false;
            }
        },
            error => {
                this.showLoader = false;
                //this.router.navigateByUrl("/");
            })
    }

    

    OpenWarningPopup(agencyId: any, location: any,state:any) {
        if (state == 'TX') {
            this.singleAgencyData = this.agentWiseWeeklyData.filter(x => x.agencyid == parseInt(agencyId))
            this.singleAgencyfilterData = this.singleAgencyData;
            this.warningPopup = true;

            let dateList = this.singleAgencyfilterData.map(item => this.datePipe.transform(item.date, 'MM/dd/yyyy'))
                .filter((value, index, self) => self.indexOf(value) === index)
            this.bindDateList(dateList);
            this.GetTotalLocationWise();
            this.singleAgencyfilterData = this.singleAgencyfilterData.sort((n1, n2) => this.CastDate(n2.date) - this.CastDate(n1.date));
            this.agencyName = location;
        }
        else if (state == 'CA') {
            this.singleAgencyData = this.caagentWiseWeeklyData.filter(x => x.agencyid == parseInt(agencyId))
            this.singleAgencyfilterData = this.singleAgencyData;
            this.warningPopup = true;

            let dateList = this.singleAgencyfilterData.map(item => this.datePipe.transform(item.date, 'MM/dd/yyyy'))
                .filter((value, index, self) => self.indexOf(value) === index)
            this.bindDateList(dateList);
            this.GetTotalLocationWise();
            this.singleAgencyfilterData = this.singleAgencyfilterData.sort((n1, n2) => this.CastDate(n2.date) - this.CastDate(n1.date));
            this.agencyName = location;
        }
    }

    OpenAgentCallPopup(location: any, extension: any) {
        this.showLoader = true;
        this.service.GetSpectrumAgentCallDetails(this.userId, this.userRole, extension).subscribe(m => {
            this.spectrumLiveAgentCallDetailsData = this.spectrumLiveAgentCallDetailsfilterData = m;
            this.showLoader = false;
            this.agencyName = location;
            this.extension = extension;
            this.AgentCallPopup = true;
            console.log("m", m);
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    bindDateList(listDate: any) {
        this.listDate = [];
        this.listDate.push(
            {
                label: "Select Date", value: "Select Date"
            })
        listDate.forEach(m => {
            this.listDate.push(
                {
                    label: m, value: m
                })
        })
        this.listDate = this.listDate.sort((n1, n2) => this.CastDate(n2.value) - this.CastDate(n1.value));
    }

    CastDate(date: any) {
        return new Date(date).getTime();
    }

    onDateSelect() {
        if (this.dateId == 'Select Date') {
            this.singleAgencyfilterData = this.singleAgencyData;
        }
        else if (this.dateId != 'Select Date') {
            this.singleAgencyfilterData = this.singleAgencyData.filter(x => this.CastDate(x.date) == this.CastDate(this.dateId));
        }
        this.singleAgencyfilterData = this.singleAgencyfilterData.sort((n1, n2) => this.CastDate(n2.date) - this.CastDate(n1.date));
        this.GetTotalLocationWise();
    }

    getWeek() {
        this.listWeek = [];
        this.listWeek.push(
            {
                label: 'This Week', value: '0'
            })
    }
    //    , {
    //    label: 'Revious Week', value: '1'
    //}
    GetTotalLocationWise() {
        this.weeklytotallocationWiseInbound = this.singleAgencyfilterData.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
        this.weeklytotallocationWiseOutbound = this.singleAgencyfilterData.reduce((acc, cur) => acc + cur.outboundCalls, 0);
        this.weeklytotallocationWiseInboundCall = this.singleAgencyfilterData.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
        this.weeklytotallocationWiseOutboundCall = this.singleAgencyfilterData.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
        this.weeklytotallocationWiseMissedCall = this.singleAgencyfilterData.reduce((acc, cur) => acc + cur.missedCalls, 0);
    }

    SearchAgencyAgent(event: any) {
        console.log("Search", event.target.value);
        this.singleAgencyfilterData = this.singleAgencyData.filter(a => a.extension.includes(event.target.value));
        this.singleAgencyfilterData = this.singleAgencyfilterData.sort((n1, n2) => this.CastDate(n2.date) - this.CastDate(n1.date));
        this.GetTotalLocationWise();
    }

    GetAgentWiseData() {
        this.showLoader = true;
        this.service.GetStatsLiveDataAgentWiseV2(this.userId, this.userRole).subscribe(m => {
            this.agentWiseData = this.agentWisefilterdata = this.liveSpectrumExtensionData = this.liveSpectrumExtensionfilterData = m;
            this.showLoader = false;
            this.GetTotalAgentWise();
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    GetAgentCallDetailsData(extension: any) {

    }

    GetAgentWiseWeeklyData() {
        this.showLoader = true;
        this.service.GetStatsWeeklyDataAgentWiseV2(this.userId, this.userRole, this.date).subscribe(m => {
            this.agentWiseWeeklyData = m;
            this.TotalCallAgencyWise();
            this.GetAgentExtensionWiseData();
            this.showLoader = false;
           
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    GetAgentExtensionWiseData() {
        //this.agentExtensionWiseData = this.agentWiseWeeklyData;
        this.agentExtensionWiseData = new Array<SpectrumLiveData>();
        if (this.AgencyList.length === 0) {
            this.AgencyItem.forEach(m => {
                this.agentWiseWeeklyData.filter(x => x.location == m.value).forEach(j => {
                    this.agentExtensionWiseData.push(
                        {
                            location: j.location
                            , agencyid: j.agencyid
                            , inBoundCalls: j.inBoundCalls
                            , outboundCalls: j.outboundCalls
                            , totalInBoundsCalls: j.totalInBoundsCalls
                            , totalOutBoundsCall: j.totalOutBoundsCall
                            , missedCalls: j.missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: j.extension
                            , date: j.date
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                })
            })
        }
        else {
            this.AgencyList.forEach(m => {
                this.agentWiseWeeklyData.filter(x => x.location == m).forEach(j => {
                    this.agentExtensionWiseData.push(
                        {
                            location: j.location
                            , agencyid: j.agencyid
                            , inBoundCalls: j.inBoundCalls
                            , outboundCalls: j.outboundCalls
                            , totalInBoundsCalls: j.totalInBoundsCalls
                            , totalOutBoundsCall: j.totalOutBoundsCall
                            , missedCalls: j.missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: j.extension
                            , date: j.date
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                })

            })
        }
        this.agentExtensionWiseData = this.agentExtensionWiseData.sort(this.sortascByAgency);
        //this.agentExtensionWiseData.sort((a, b) => a.location.localeCompare(b.location));

    }

    OpenExtensionCallPopup(extension: any, date: any, location: any, state: any) {
        this.spectrumLiveAgentCallDetailsData = new Array<SpectrumLiveAgentCallDetails>();
        this.spectrumLiveAgentCallDetailsfilterData = new Array<SpectrumLiveAgentCallDetails>();
        this.agencyName = ""; this.extension = "";
        this.showLoader = true;
        if (state == 'TX') {
            this.service.GetSpectrumLiveAgentExtensionWiseCallLogV2(extension, date).subscribe(m => {
                this.spectrumLiveAgentCallDetailsData = this.spectrumLiveAgentCallDetailsfilterData = m;
                this.showLoader = false;
                this.agencyName = location;
                this.extension = extension;
                this.AgentCallPopup = true;

            },
                error => {
                    this.showLoader = false;
                    this.router.navigateByUrl("/");
                })
        }
        else if (state == 'CA') {
            this.service.GetSpectrumLiveAgentExtensionWiseCallLogCAV2(extension, date).subscribe(m => {
                this.spectrumLiveAgentCallDetailsData = this.spectrumLiveAgentCallDetailsfilterData = m;
                this.showLoader = false;
                this.agencyName = location;
                this.extension = extension;
                this.AgentCallPopup = true;

            },
                error => {
                    this.showLoader = false;
                    this.router.navigateByUrl("/");
                })
        }
    }
    GetAgentLogLive() {
        this.agentLogAgencyWiseData = new Array<AgencyAgentLog>();
        this.filterAgentLogAgencyWiseData = new Array<AgencyAgentLog>();

        this.showLoader = this.showLoader == false ? true : true;
        this.service.GetSpectrumAgencyAgentLiveLogV2(this.userId, this.userRole).subscribe(m => {
            this.agentLogAgencyWiseData = this.filterAgentLogAgencyWiseData= m;
            this.GetTotalAgentLiveLog();
            this.showLoader = false;
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })

    }

    filterDataAgentLogLive() {
        this.filterAgentLogAgencyWiseData = new Array<AgencyAgentLog>();
        if (this.AgencyList.length === 0) {
            this.agencyData.forEach(m => {
                let tempfilterdata = new Array<AgencyAgentLog>()
                tempfilterdata = this.agentLogAgencyWiseData.filter(z => z.agencyName === m.agencyName)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.filterAgentLogAgencyWiseData.push(a);
                    })
                }
            })
        }
        else {
            this.AgencyList.forEach(m => {
                let tempfilterdata = new Array<AgencyAgentLog>()
                tempfilterdata = this.agentLogAgencyWiseData.filter(z => z.agencyName === m)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.filterAgentLogAgencyWiseData.push(a);
                    })
                }
            })
        }
        this.GetTotalAgentLiveLog();
    }

    GetTotalAgentLiveLog() {
        this.TotalMeetingHours = 0;
        this.TotalManualAvailHours = 0;
        this.TotalOtherHours = 0;
        this.TotalLoginHours = 0;
        this.TotalWebHours = 0;
        this.TotalBreaksHours = 0;
        this.TotalLunchHours = 0;
        this.TotalManualHours = 0;
        this.TotalLogoutHours = 0;
        this.TotalAutoHours = 0;
        this.TotalTotalHours = 0;

        this.TotalMeetingHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.meeting, 0);
        this.TotalManualAvailHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.manual_avail, 0);
        this.TotalOtherHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.other, 0);
        this.TotalLoginHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.logins, 0);
        this.TotalWebHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.web, 0);
        this.TotalBreaksHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.breaks, 0);
        this.TotalLunchHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.lunch, 0);
        this.TotalManualHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.manualsec, 0);
        this.TotalLogoutHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.logoutsec, 0);
        this.TotalAutoHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.autosec, 0);
        this.TotalTotalHours = this.filterAgentLogAgencyWiseData.reduce((acc, cur) => acc + cur.total, 0);

        
    }

    SearchAgentLog(event: any) {
        console.log("Search", event.target.value);
        this.filterAgentLogAgencyWiseData = this.agentLogAgencyWiseData.filter(a => a.extension.toString().includes(event.target.value));
        console.log("this.filterAgentLogAgencyWiseData", this.filterAgentLogAgencyWiseData)
        this.GetTotalAgentLiveLog();
    }

    OpenAgentLogCallPopup(location: any, extension: any) {
        this.SpectrumAgentExtensionLogData = new Array<SpectrumAgentExtensionLog>();
        this.showLoader = true;
        this.service.GetSpectrumAgentLogCallDetails(this.userId, this.userRole, extension).subscribe(m => {
            this.SpectrumAgentExtensionLogData  = m;
            this.showLoader = false;
            this.agencyName = location;
            this.extension = extension;
            this.AgentLogLiveCallPopup = true;
            console.log("m", m);
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    TotalCallAgencyWise() {
        this.agentWiseWeeklyfilterdata = new Array<SpectrumLiveData>();
        if (this.AgencyList.length === 0) {
            this.AgencyItem.forEach(m => {
                let _data = this.agentWiseWeeklyData.filter(x => x.location == m.value);
                if (_data.length > 0) {
                    let _incall = _data.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
                    let _outcall = _data.reduce((acc, cur) => acc + cur.outboundCalls, 0);
                    let _totalincall = _data.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
                    let _totaloutcall = _data.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
                    let _missedCalls = _data.reduce((acc, cur) => acc + cur.missedCalls, 0);
                    this.agentWiseWeeklyfilterdata.push(
                        {
                            location: _data[0].location
                            , agencyid: _data[0].agencyid
                            , inBoundCalls: _incall
                            , outboundCalls: _outcall
                            , totalInBoundsCalls: _totalincall
                            , totalOutBoundsCall: _totaloutcall
                            , missedCalls: _missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: ''
                            , date: ''
                            , callTransfer: 0
                            ,transferCallDuration:0
                        })
                }
            })
        }
        else {

            this.AgencyList.forEach(m => {
                let _data = this.agentWiseWeeklyData.filter(x => x.location == m);
                if (_data.length > 0) {
                    let _incall = _data.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
                    let _outcall = _data.reduce((acc, cur) => acc + cur.outboundCalls, 0);
                    let _totalincall = _data.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
                    let _totaloutcall = _data.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
                    let _missedCalls = _data.reduce((acc, cur) => acc + cur.missedCalls, 0);
                    this.agentWiseWeeklyfilterdata.push(
                        {
                            location: _data[0].location
                            , agencyid: _data[0].agencyid
                            , inBoundCalls: _incall
                            , outboundCalls: _outcall
                            , totalInBoundsCalls: _totalincall
                            , totalOutBoundsCall: _totaloutcall
                            , missedCalls: _missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: ''
                            , date: ''
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                }
            })
        }

        this.GetTotalWeeklyWise();
        this.agentWiseWeeklyfilterdataStored = this.agentWiseWeeklyfilterdata;
        this.sortDataWeekly('Incoming');

    }

    onSelect(e) {
        if (this.state == 'TX') {
            this.filterdata = new Array<SpectrumLiveData>();
            if (this.AgencyList.length === 0) {
                //this.filterdata = this.data;
                //this.agentWisefilterdata = this.agentWiseData;
                this.agencyData.forEach(m => {
                    let tempfilterdata = new Array<SpectrumLiveData>()
                    tempfilterdata = this.data.filter(z => z.location === m.agencyName)
                    if (tempfilterdata.length > 0) {
                        tempfilterdata.forEach(a => {
                            this.filterdata.push(a);
                        })
                    }
                })
            }
            else {
                this.AgencyList.forEach(m => {
                    let tempfilterdata = new Array<SpectrumLiveData>()
                    tempfilterdata = this.data.filter(z => z.location === m)
                    if (tempfilterdata.length > 0) {
                        tempfilterdata.forEach(a => {
                            this.filterdata.push(a);
                        })
                    }
                })

            }
            this.filterDataAgentWise();
            //this.TotalCallAgencyWise();
            this.GetAgentWiseWeeklyData();
            this.GetTotal();
            this.filterDataAgentLogLive();
        }
        else if (this.state == 'CA') {
            this.cafilterdata = new Array<SpectrumLiveData>();
            if (this.caAgencyList.length === 0) {
                this.caAgencyItem.forEach(m => {
                    let tempfilterdata = new Array<SpectrumLiveData>()
                    tempfilterdata = this.cadata.filter(z => z.location === m.value)
                    if (tempfilterdata.length > 0) {
                        tempfilterdata.forEach(a => {
                            this.cafilterdata.push(a);
                        })
                    }
                })
            }
            else {
                this.caAgencyList.forEach(m => {
                    let tempfilterdata = new Array<SpectrumLiveData>()
                    tempfilterdata = this.cadata.filter(z => z.location === m)
                    if (tempfilterdata.length > 0) {
                        tempfilterdata.forEach(a => {
                            this.cafilterdata.push(a);
                        })
                    }
                })

            }
            this.filterDataAgentWiseCA();
            //this.GetAgentWiseWeeklyData();
            this.GetCATotal();
            this.filterDataAgentLogLiveCA();
            this.GetAgentWiseWeeklyDataCA();
        }
    }

    onWeekSelect() {
        this.GetAgentWiseWeeklyData();
        let date: Date = new Date();
        if (this.weekId == "0") {
            this.startDate = new Date(date.setDate(date.getUTCDate() - 7));
            date = new Date();
            this.endDate = new Date(date.setDate(date.getUTCDate() - 1));
        }
        else if (this.weekId == "1") {
            this.startDate = new Date(date.setDate(date.getUTCDate() - 14));
            date = new Date();
            this.endDate = new Date(date.setDate(date.getUTCDate() - 8));
        }
        else if (this.weekId == "2") {
            this.startDate = new Date(date.setDate(date.getUTCDate() - 21));
            date = new Date();
            this.endDate = new Date(date.setDate(date.getUTCDate() - 15));
        }
    }
    filterDataAgentWise() {
        this.agentWisefilterdata = new Array<SpectrumLiveData>();
        if (this.AgencyList.length === 0) {
            //this.agentWisefilterdata = this.agentWiseData;
            this.agencyData.forEach(m => {
                let tempfilterdata = new Array<SpectrumLiveData>()
                tempfilterdata = this.agentWiseData.filter(z => z.location === m.agencyName)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.agentWisefilterdata.push(a);
                    })
                }
            })
        }
        else {
            this.AgencyList.forEach(m => {
                let tempfilterdata = new Array<SpectrumLiveData>()
                tempfilterdata = this.agentWiseData.filter(z => z.location === m)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.agentWisefilterdata.push(a);
                    })
                }
            })
        }
        this.GetTotalAgentWise();
        //this.sortDataAgent('Incoming')
    }

    FillDropDown() {
        this.AgencyItem = [];
        this.data.forEach(m => {
            this.AgencyItem.push(
                {
                    label: m.location, value: m.location
                })
        })
    }

    GetTotal() {
        this.totalInbound = 0;
        this.totalOutbound = 0;
        this.totalInboundCall = 0;
        this.totalOutboundCall = 0;
        this.totalmissedCalls = 0;
        this.totalTransferCalls = 0;
        this.filterdata.forEach(m => {
            this.totalInbound = this.totalInbound + m.inBoundCalls;
            this.totalOutbound = this.totalOutbound + m.outboundCalls;
            this.totalInboundCall = this.totalInboundCall + m.totalInBoundsCalls;
            this.totalOutboundCall = this.totalOutboundCall + m.totalOutBoundsCall;
            this.totalmissedCalls = this.totalmissedCalls + m.missedCalls;
            this.totalTransferCalls = this.totalTransferCalls + m.callTransfer;
        })
    }
    GetTotalAgentWise() {
        this.agentWisetotalInbound = 0;
        this.agentWisetotalOutbound = 0;
        this.agentWisetotalInboundCall = 0;
        this.agentWisetotalOutboundCall = 0;
        this.agentWisemissedCall = 0;
        this.agentwiseInBoundCallDuration = 0;
        this.agentwiseOutBoundCallDuration = 0;
        this.agentwiseTotalTransferCall = 0;
        this.agentwiseTotalTransferCallDuration = 0;
        this.agentWisefilterdata.forEach(m => {
            this.agentWisetotalInbound = this.agentWisetotalInbound + m.inBoundCalls;
            this.agentWisetotalOutbound = this.agentWisetotalOutbound + m.outboundCalls;
            this.agentWisetotalInboundCall = this.agentWisetotalInboundCall + m.totalInBoundsCalls;
            this.agentWisetotalOutboundCall = this.agentWisetotalOutboundCall + m.totalOutBoundsCall;
            this.agentWisemissedCall = this.agentWisemissedCall + m.missedCalls;
            this.agentwiseInBoundCallDuration = this.agentwiseInBoundCallDuration + m.inboundDuration;
            this.agentwiseOutBoundCallDuration = this.agentwiseOutBoundCallDuration + m.outBoundDuration;

            this.agentwiseTotalTransferCall = this.agentwiseTotalTransferCall + m.callTransfer;
            this.agentwiseTotalTransferCallDuration = this.agentwiseTotalTransferCallDuration + m.transferCallDuration;
        })
    }

    GetTotalWeeklyWise() {
        this.weeklytotalInbound = 0;
        this.weeklytotalOutbound = 0;
        this.weeklytotalInboundCall = 0;
        this.weeklytotalOutboundCall = 0;
        this.weeklytotalMissedCall = 0;

        this.weeklytotalInbound = this.agentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
        this.weeklytotalOutbound = this.agentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.outboundCalls, 0);
        this.weeklytotalInboundCall = this.agentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
        this.weeklytotalOutboundCall = this.agentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
        this.weeklytotalMissedCall = this.agentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.missedCalls, 0);
    }
    Search(event: any) {
        console.log("Search", event.target.value);
        this.agentWisefilterdata = this.agentWiseData.filter(a => a.extension.includes(event.target.value));
        this.GetTotalAgentWise();
    }


    GetRefeshTime() {
        this.service.getSpectrumRefeshTime().subscribe(m => {
            this.refreshTime = m;
        })
    }



    sortDirection: string = "desc";
    sortBy: string = "";

    sortData(e) {
        if (this.sortBy === e) {
            if (this.sortDirection === "desc") {
                this.sortDirection = "asc";
            }
            else {
                this.sortDirection = "desc";
            }
        }
        this.sortBy = e;

        let Data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
        switch (e) {

            case "Incoming":
                {

                    if (this.sortDirection === "desc") {
                        Data = this.filterdata.sort(this.sortdescByIncoming);
                    }
                    else {
                        Data = this.filterdata.sort(this.sortByIncoming);
                    }
                }
                break
            case "Outgoing":
                {
                    if (this.sortDirection === "desc") {
                        Data = this.filterdata.sort(this.sortdescByOutgoing);
                    }
                    else {
                        Data = this.filterdata.sort(this.sortByOutgoing);
                    }
                }
                break
        }
        this.filterdata = Data;
    }


    sortDirectionAgent: string = "desc";
    sortByAgent: string = "";

    sortdescByIncoming(a, b) {
        return b["inBoundCalls"] - a["inBoundCalls"];
    }
    sortdescByAgency(a, b) {
        return b["location"] - a["location"];
    }
    sortascByAgency(a, b) {
        return a["location"] - b["location"];
    }
    sortByIncoming(a, b) {
        return a["inBoundCalls"] - b["inBoundCalls"];
    }

    sortDataAgent(e) {
        if (this.sortByAgent === e) {
            if (this.sortDirectionAgent === "desc") {
                this.sortDirectionAgent = "asc";
            }
            else {
                this.sortDirectionAgent = "desc";
            }
        }
        this.sortByAgent = e;

        let Data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
        switch (e) {

            case "Incoming":
                {

                    if (this.sortDirectionAgent === "desc") {
                        Data = this.agentWisefilterdata.sort(this.sortdescByIncoming);
                    }
                    else {
                        Data = this.agentWisefilterdata.sort(this.sortByIncoming);
                    }
                }
                break
            case "Location":
                {
                    if (this.sortDirectionAgent === "desc") {
                        Data = this.agentWisefilterdata.sort(this.sortdescByAgency);
                    }
                    else {
                        Data = this.agentWisefilterdata.sort(this.sortascByAgency);
                    }
                }
                break
            case "Outgoing":
                {
                    if (this.sortDirectionAgent === "desc") {
                        Data = this.agentWisefilterdata.sort(this.sortdescByOutgoing);
                    }
                    else {
                        Data = this.agentWisefilterdata.sort(this.sortByOutgoing);
                    }
                }
                break
        }
        this.agentWisefilterdata = Data;
    }


    sortDirectionWeekly: string = "desc";
    sortByWeekly: string = "";

    sortDataWeekly(e) {
        if (this.sortByWeekly === e) {
            if (this.sortDirectionWeekly === "desc") {
                this.sortDirectionWeekly = "asc";
            }
            else {
                this.sortDirectionWeekly = "desc";
            }
        }
        this.sortByWeekly = e;

        let Data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
        switch (e) {

            case "Incoming":
                {

                    if (this.sortDirectionWeekly === "desc") {
                        Data = this.agentWiseWeeklyfilterdata.sort(this.sortdescByIncoming);
                    }
                    else {
                        Data = this.agentWiseWeeklyfilterdata.sort(this.sortByIncoming);
                    }
                }
                break
            case "Outgoing":
                {
                    if (this.sortDirectionWeekly === "desc") {
                        Data = this.agentWiseWeeklyfilterdata.sort(this.sortdescByOutgoing);
                    }
                    else {
                        Data = this.agentWiseWeeklyfilterdata.sort(this.sortByOutgoing);
                    }
                }
                break
        }
        this.agentWiseWeeklyfilterdata = Data;
    }

    sortdescByOutgoing(a, b) {
        return b["outboundCalls"] - a["outboundCalls"];
    }

    sortByOutgoing(a, b) {
        return a["outboundCalls"] - b["outboundCalls"];
    }

    secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? "h, " : "h, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? "m, " : "m, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? "s" : "s") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    convertDate(date: Date) {
        let yourDate = new Date(date).getUTCDate;
        yourDate.toString();
    }

    //---------------------------bind drop down----------------
    getAllSaleDirectors() {
        this.saleDirectorSelectItem = [];
        this.service.getAllSaleDirectors().subscribe(m => {
            this.saleDirector = m;
            console.log("this.saleDirector", this.saleDirector)
            this.saleDirector.forEach(i => {
                this.saleDirectorSelectItem.push(
                    {
                        label: i.saleDirector, value: i.saleDirectorID
                    })
                this.selectedSaleDirectortemp.push(i.saleDirectorID);
            })
        });
    }

    onSaleDirectorchange(e) {
        this.selectedZonalManager = new Array<number>();
        this.selectedRegionalManager = new Array<number>();
        this.agencyData = new Array<Agency>();
        if (this.selectedSaleDirector.length > 0) {
            this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
        } else if (this.selectedSaleDirectortemp.length > 0) {
            this.getRegionalManagersBySaleDirector(this.selectedSaleDirectortemp);
        }
        else {
            this.getAllRegionalManagers();
            this.getAllZonalManagers();
        }
       
    }

    getRegionalManagersBySaleDirector(selectedManager: Array<number>) {
        this.regionalManagerSelectItem = [];
        this.allRegionalManagerIds = [];
        this.service.getRegionalManagerBySaleDirector(selectedManager).subscribe(m => {
            this.regionalManagersData = m;
            this.regionalManagersData.forEach(i => {
                this.regionalManagerSelectItem.push(
                    {
                        label: i.regionalManager, value: i.regionalManagerId
                    })
                this.allRegionalManagerIds.push(i.regionalManagerId)
            })
            if (this.selectedRegionalManager.length > 0) {
                this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
            }
            else {
                this.getZonalManagerByRegionalManagers(this.allRegionalManagerIds);
            }
            
        });
    }

    getAllRegionalManagers() {
        this.showLoader = true;
        this.regionalManagersData = [];
        this.service.getAllRegionalManagers().subscribe(m => {
            this.regionalManagersData = m;
            this.regionalManagersData.forEach(i => {
                this.regionalManagerSelectItem.push(
                    {
                        label: i.regionalManager, value: i.regionalManagerId
                    })
                this.allRegionalManagerIds.push(i.regionalManagerId);
            })
            this.showLoader = false;
        });
    }

    getAllZonalManagers() {
        this.showLoader = true;
        this.zonalManagerSelectItem = [];
        this.allZonalManagerIds=[];
        this.service.getAllZonalManagers().subscribe(m => {
            this.zonalManager = m;
            this.zonalManager.forEach(i => {
                this.zonalManagerSelectItem.push(
                    {
                        label: i.zonalManager, value: i.zonalManagerId
                    })
                this.allZonalManagerIds.push(i.zonalManagerId);
            })
            this.showLoader = false;
        });
    }

    getZonalManagerByRegionalManagers(selectedManager: Array<number>) {
        this.showLoader = true;
        this.zonalManagerSelectItem = [];
        this.allZonalManagerIds = [];
        this.service.getZonalManagersByRegionalManager(selectedManager).subscribe(m => {
            this.zonalManager = m;
            this.zonalManager.forEach(i => {
                this.zonalManagerSelectItem.push(
                    {
                        label: i.zonalManager, value: i.zonalManagerId
                    })
                this.allZonalManagerIds.push(i.zonalManagerId);

                if (this.selectedRegionalManager.length > 0 && this.selectedZonalManager.length > 0) {
                    this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedZonalManager);
                }
                else if (this.selectedRegionalManager.length > 0 && this.selectedZonalManager.length == 0) {
                    this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.allZonalManagerIds);
                }
                else if(this.selectedRegionalManager.length== 0 && this.selectedZonalManager.length == 0) {
                    this.getAllLocationsRegionalZonalManager(selectedManager, this.allZonalManagerIds);
                }
            });
            this.showLoader = false;
        });
    }

    getAllLocationsRegionalZonalManager(Rids: Array<number>, Zids: Array<number>) {
        this.showLoader = true;
        this.service.getAllLocationsRegionalZonalManager(Rids, Zids).subscribe(m => {
            this.agencyData = m;
            this.AgencyItem = [];
            this.agencyData.forEach(m => {
                this.AgencyItem.push(
                    {
                        label: m.agencyName, value: m.agencyName
                    })
            })
            this.showLoader = false;
        });
    }

    onRegionalManagerSelect(e) {
        if (this.selectedRegionalManager.length > 0) {
            this.getZonalManagerByRegionalManagers(this.selectedRegionalManager);
        }
        else {
            this.getAllZonalManagers();
        }
        if (this.selectedZonalManager.length == 0) {
            this.getAllLocations();
        }
    }
    onZonalManagerSelect(e) {
        if (this.selectedRegionalManager.length > 0 && this.selectedZonalManager.length > 0) {
            this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.selectedZonalManager);
        }
        else if (this.selectedRegionalManager.length > 0 && this.selectedZonalManager.length == 0) {
            this.getAllLocationsRegionalZonalManager(this.selectedRegionalManager, this.allZonalManagerIds);
        }
        else if (this.selectedRegionalManager.length == 0 && this.selectedZonalManager.length == 0) {
            this.getAllLocationsRegionalZonalManager(this.allRegionalManagerIds, this.allZonalManagerIds);
        }

        
    }

    getAllLocations() {
        this.showLoader = true;
        this.service.getAllLocations().subscribe(m => {
            this.agencyData = m;
            this.AgencyItem = [];
            this.agencyData.forEach(m => {
                this.AgencyItem.push(
                    {
                        label: m.agencyName, value: m.agencyName
                    })
            })
            this.showLoader = false;
            
        });
    }
    getStoreManagerAgency(Id: number) {
        this.showLoader = true;
        this.service.getStoreManagerAgency(Id).subscribe(m => {
            this.agencyData = m;
            this.AgencyItem = [];
            this.agencyData.forEach(m => {
                this.AgencyItem.push(
                    {
                        label: m.agencyName, value: m.agencyName
                    })
            })
            this.showLoader = false;

        });
    }

    SearchData() {
        var e: any;
        this.onSelect(e);
    }

    Reset() {
        this.selectedAgency = '';
        this.selectedRegionalManager = [];
        this.selectedZonalManager = [];
        this.allRegionalManagerIds = [];
        this.allZonalManagerIds = [];
        this.AgencyList = [];
        this.caAgencyList = [];
        this.saleDirectorSelectItem = []; this.selectedSaleDirectortemp = [];

        if (this.userRole === undefined) {
            this.getAllSaleDirectors();
            this.getAllRegionalManagers();
            this.getAllZonalManagers();
            this.getAllLocations();
        }
        else if (this.userRole === 'headofdepratment') {
            this.getAllSaleDirectors();
            this.getAllRegionalManagers();
            this.getAllZonalManagers();
            this.getAllLocations();
        }
        else if (this.userRole === 'saledirector') {
            this.selectedSaleDirector = new Array<number>();
            this.selectedSaleDirector.push(parseInt(this.userId));
            this.getRegionalManagersBySaleDirector(this.selectedSaleDirector);
        }
        else if (this.userRole === 'regionalmanager') {
            this.allRegionalManagerIds.push(parseInt(this.userId));
            this.getZonalManagerByRegionalManagers(this.allRegionalManagerIds);
           
        }
        else if (this.userRole === 'zonalmanager') {
            this.allZonalManagerIds.push(parseInt(this.userId));
            this.getAllLocationsRegionalZonalManager(null, this.allZonalManagerIds);
        }
        else if (this.userRole === 'storemanager') {
            this.getStoreManagerAgency(parseInt(this.userId));
        }
        var e: any;
        this.onSelect(e);
    }

    onClanderDateSelect(e) {
        if (
            (this.date && this.date[0] != null && this.date[1] != null)
        ) {
            this.startDate = new Date(this.date[0].setDate(this.date[0].getUTCDate()));
            this.endDate = new Date(this.date[1].setDate(this.date[1].getUTCDate()));
            console.log("this.startDate", this.startDate);
        }
    }
    setActualHeader(index) {
        this.tabindex = index;
    }

    exportExcel() {
        let data = [];
        if (this.state == 'TX') {
            if (this.tabindex == 0) {
                data = this.filterdata;
                this.service.ExportSpectrumCallLocationWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 1) {
                data = this.agentWisefilterdata;
                this.service.ExportSpectrumCallExtensionWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 2) {
                data = this.filterAgentLogAgencyWiseData;
                this.service.ExportLiveStratusAgentLog(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 3) {
                data = this.agentWiseWeeklyfilterdata;
                this.service.ExportStratusWeeklyDataAgentWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            } else if (this.tabindex == 4) {
                data = this.agentExtensionWiseData; 
                this.service.ExportStratusDataAgentExtensionWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
        }
        else if (this.state == 'CA') {

            if (this.tabindex == 0) {
                data = this.cafilterdata;
                this.service.ExportSpectrumCallLocationWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 1) {
                data = this.caagentWisefilterdata;
                this.service.ExportSpectrumCallExtensionWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 2) {
                data = this.filterAgentLogAgencyWiseDataCA;
                this.service.ExportLiveStratusAgentLog(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 3) {
                data = this.caagentWiseWeeklyfilterdata;
                this.service.ExportStratusWeeklyDataAgentWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
            else if (this.tabindex == 4) {
                data = this.caagentExtensionWiseData;
                this.service.ExportStratusDataAgentExtensionWise(data).subscribe(x => {
                    this._downloadExcel.downloadFile(x);
                    console.log("FileName", x);
                    this.showLoader = false;
                })
            }
        }
    }

    OpenCallTransferPopup(location: any, extension: any, agencyId: any, project:any) {
        this.agentCallTransferData = new Array<AgentCallTransfer>();
        this.showLoader = true;
        if (project == 'TX') {
            this.service.GetCallTransferDetails(this.userId, this.userRole, extension, agencyId).subscribe(m => {
                this.agentCallTransferData = m;
                this.showLoader = false;
                this.agencyName = location;
                this.extension = extension;
                this.AgentCallsTranserPopup = true;
                console.log("m", m);
            },
                error => {
                    this.showLoader = false;
                    this.router.navigateByUrl("/");
                })
        }
        else if (project == 'CA') {
            this.service.GetCallTransferDetailsCA(this.userId, this.userRole, extension, agencyId).subscribe(m => {
                this.agentCallTransferData = m;
                this.showLoader = false;
                this.agencyName = location;
                this.extension = extension;
                this.AgentCallsTranserPopup = true;
                console.log("m", m);
            },
                error => {
                    this.showLoader = false;
                    this.router.navigateByUrl("/");
                })
        }
    }
    //**********************************************8CA************************************
    GetCASpectrumV2() {
        this.service.GetCASpectrumLiveDataV2(this.userId, this.userRole).subscribe(m => {
            this.cafilterdata = this.caagencyfilterdata = this.cadata = m;
            this.FillCAAgency();
            this.GetCATotal();
        },
            error => {
                this.router.navigateByUrl("/");
            })
    }
    FillCAAgency() {
        this.caAgencyItem = [];
        this.caagencyfilterdata.forEach(m => {
            this.caAgencyItem.push(
                {
                    label: m.location, value: m.location
                })
        })
    }

    GetAgentWiseDataCA() {
        this.showLoader = true;
        this.service.GetStatsLiveDataAgentWiseCAV2(this.userId, this.userRole).subscribe(m => {
            this.caagentWiseData = this.caagentWisefilterdata = this.caliveSpectrumExtensionData = this.caliveSpectrumExtensionfilterData = m;
            this.GetTotalCAAgentWise();
            this.showLoader = false;
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }
    SearchCAExtension(event: any) {
        console.log("Search", event.target.value);
        this.caagentWisefilterdata = this.caagentWiseData.filter(a => a.extension.includes(event.target.value));
        this.GetTotalCAAgentWise();
    }
    GetTotalCAAgentWise() {
        this.caagentWisetotalInbound = 0;
        this.caagentWisetotalOutbound = 0;
        this.caagentWisetotalInboundCall = 0;
        this.caagentWisetotalOutboundCall = 0;
        this.caagentWisemissedCall = 0;
        this.caagentwiseInBoundCallDuration = 0;
        this.caagentwiseOutBoundCallDuration = 0;
        this.caagentwiseTotalTransferCall = 0;
        this.caagentwiseTotalTransferCallDuration = 0;
        this.caagentWisefilterdata.forEach(m => {
            this.caagentWisetotalInbound = this.caagentWisetotalInbound + m.inBoundCalls;
            this.caagentWisetotalOutbound = this.caagentWisetotalOutbound + m.outboundCalls;
            this.caagentWisetotalInboundCall = this.caagentWisetotalInboundCall + m.totalInBoundsCalls;
            this.caagentWisetotalOutboundCall = this.caagentWisetotalOutboundCall + m.totalOutBoundsCall;
            this.caagentWisemissedCall = this.caagentWisemissedCall + m.missedCalls;
            this.caagentwiseInBoundCallDuration = this.caagentwiseInBoundCallDuration + m.inboundDuration;
            this.caagentwiseOutBoundCallDuration = this.caagentwiseOutBoundCallDuration + m.outBoundDuration;

            this.caagentwiseTotalTransferCall = this.caagentwiseTotalTransferCall + m.callTransfer;
            this.caagentwiseTotalTransferCallDuration = this.caagentwiseTotalTransferCallDuration + m.transferCallDuration;
        })
    }

    sortDatacaAgent(e) {
        if (this.sortByAgent === e) {
            if (this.sortDirectionAgent === "desc") {
                this.sortDirectionAgent = "asc";
            }
            else {
                this.sortDirectionAgent = "desc";
            }
        }
        this.sortByAgent = e;

        let Data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
        switch (e) {

            case "Incoming":
                {

                    if (this.sortDirectionAgent === "desc") {
                        Data = this.caagentWisefilterdata.sort(this.sortdescByIncoming);
                    }
                    else {
                        Data = this.caagentWisefilterdata.sort(this.sortByIncoming);
                    }
                }
                break
            case "Location":
                {
                    if (this.sortDirectionAgent === "desc") {
                        Data = this.caagentWisefilterdata.sort(this.sortdescByAgency);
                    }
                    else {
                        Data = this.caagentWisefilterdata.sort(this.sortascByAgency);
                    }
                }
                break
            case "Outgoing":
                {
                    if (this.sortDirectionAgent === "desc") {
                        Data = this.caagentWisefilterdata.sort(this.sortdescByOutgoing);
                    }
                    else {
                        Data = this.caagentWisefilterdata.sort(this.sortByOutgoing);
                    }
                }
                break
        }
        this.caagentWisefilterdata = Data;
    }

    GetCATotal() {
        this.catotalInbound = 0;
        this.catotalOutbound = 0;
        this.catotalInboundCall = 0;
        this.catotalOutboundCall = 0;
        this.catotalmissedCalls = 0;
        this.catotalTransferCalls = 0;
        this.cafilterdata.forEach(m => {
            this.catotalInbound = this.catotalInbound + m.inBoundCalls;
            this.catotalOutbound = this.catotalOutbound + m.outboundCalls;
            this.catotalInboundCall = this.catotalInboundCall + m.totalInBoundsCalls;
            this.catotalOutboundCall = this.catotalOutboundCall + m.totalOutBoundsCall;
            this.catotalmissedCalls = this.catotalmissedCalls + m.missedCalls;
            this.catotalTransferCalls = this.catotalTransferCalls + m.callTransfer;
        })
    }

    fillState() {
        this.stateSelectItem = [];
        this.stateSelectItem.push(
            {
                label: 'TX', value: 'TX'
            })
        this.stateSelectItem.push(
            {
                label: 'CA', value: 'CA'
            })

    }



    GetAgentLogLiveCA() {
        this.agentLogAgencyWiseDataCA = new Array<AgencyAgentLog>();
        this.filterAgentLogAgencyWiseDataCA = new Array<AgencyAgentLog>();

        this.showLoader = this.showLoader == false ? true : true;
        this.service.GetSpectrumAgencyAgentLiveLogCAV2(this.userId, this.userRole).subscribe(m => {
            this.agentLogAgencyWiseDataCA = this.filterAgentLogAgencyWiseDataCA = m;
            this.showLoader = false;
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })

    }

    SearchAgentLogCA(event: any) {
        this.filterAgentLogAgencyWiseDataCA = this.agentLogAgencyWiseDataCA.filter(a => a.extension.toString().includes(event.target.value));
    }

    GetAgentWiseWeeklyDataCA() {
        console.log("this.date", this.date)
        this.showLoader = true;
        this.service.GetStatsWeeklyDataAgentWiseCAV2(this.userId, this.userRole, this.date).subscribe(m => {
            this.caagentWiseWeeklyData = m;
            this.TotalCallAgencyWiseCA();
            this.GetAgentExtensionWiseDataCA();
            this.showLoader = false;
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    TotalCallAgencyWiseCA() {
        this.caagentWiseWeeklyfilterdata = new Array<SpectrumLiveData>();
        if (this.caAgencyList.length === 0) {
            this.caAgencyItem.forEach(m => {
                let _data = this.caagentWiseWeeklyData.filter(x => x.location == m.value);
                if (_data.length > 0) {
                    let _incall = _data.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
                    let _outcall = _data.reduce((acc, cur) => acc + cur.outboundCalls, 0);
                    let _totalincall = _data.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
                    let _totaloutcall = _data.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
                    let _missedCalls = _data.reduce((acc, cur) => acc + cur.missedCalls, 0);
                    this.caagentWiseWeeklyfilterdata.push(
                        {
                            location: _data[0].location
                            , agencyid: _data[0].agencyid
                            , inBoundCalls: _incall
                            , outboundCalls: _outcall
                            , totalInBoundsCalls: _totalincall
                            , totalOutBoundsCall: _totaloutcall
                            , missedCalls: _missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: ''
                            , date: ''
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                }
            })
        }
        else {

            this.caAgencyList.forEach(m => {
                let _data = this.caagentWiseWeeklyData.filter(x => x.location == m);
                if (_data.length > 0) {
                    let _incall = _data.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
                    let _outcall = _data.reduce((acc, cur) => acc + cur.outboundCalls, 0);
                    let _totalincall = _data.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
                    let _totaloutcall = _data.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
                    let _missedCalls = _data.reduce((acc, cur) => acc + cur.missedCalls, 0);
                    this.caagentWiseWeeklyfilterdata.push(
                        {
                            location: _data[0].location
                            , agencyid: _data[0].agencyid
                            , inBoundCalls: _incall
                            , outboundCalls: _outcall
                            , totalInBoundsCalls: _totalincall
                            , totalOutBoundsCall: _totaloutcall
                            , missedCalls: _missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: ''
                            , date: ''
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                }
            })
        }

        this.GetTotalWeeklyWiseCA();
        this.caagentWiseWeeklyfilterdataStored = this.caagentWiseWeeklyfilterdata;
        this.sortDataWeekly('Incoming');

    }

    GetAgentExtensionWiseDataCA() {
        //this.agentExtensionWiseData = this.agentWiseWeeklyData;

        this.caagentExtensionWiseData = new Array<SpectrumLiveData>();
        if (this.caAgencyList.length === 0) {
            this.caAgencyItem.forEach(m => {
                this.caagentWiseWeeklyData.filter(x => x.location == m.value).forEach(j => {
                    this.caagentExtensionWiseData.push(
                        {
                            location: j.location
                            , agencyid: j.agencyid
                            , inBoundCalls: j.inBoundCalls
                            , outboundCalls: j.outboundCalls
                            , totalInBoundsCalls: j.totalInBoundsCalls
                            , totalOutBoundsCall: j.totalOutBoundsCall
                            , missedCalls: j.missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: j.extension
                            , date: j.date
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                })
            })
        }
        else {
            this.caAgencyList.forEach(m => {
                this.caagentWiseWeeklyData.filter(x => x.location == m).forEach(j => {
                    this.caagentExtensionWiseData.push(
                        {
                            location: j.location
                            , agencyid: j.agencyid
                            , inBoundCalls: j.inBoundCalls
                            , outboundCalls: j.outboundCalls
                            , totalInBoundsCalls: j.totalInBoundsCalls
                            , totalOutBoundsCall: j.totalOutBoundsCall
                            , missedCalls: j.missedCalls
                            , inboundDuration: 0
                            , outBoundDuration: 0
                            , inBoundHours: ''
                            , outBoundHours: ''
                            , extension: j.extension
                            , date: j.date
                            , callTransfer: 0
                            , transferCallDuration: 0
                        })
                })

            })
        }
        this.caagentExtensionWiseData = this.caagentExtensionWiseData.sort(this.sortascByAgency);
        //this.agentExtensionWiseData.sort((a, b) => a.location.localeCompare(b.location));

    }

    GetTotalWeeklyWiseCA() {
        this.caweeklytotalInbound = 0;
        this.caweeklytotalOutbound = 0;
        this.caweeklytotalInboundCall = 0;
        this.caweeklytotalOutboundCall = 0;
        this.caweeklytotalMissedCall = 0;

        this.caweeklytotalInbound = this.caagentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.inBoundCalls, 0);
        this.caweeklytotalOutbound = this.caagentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.outboundCalls, 0);
        this.caweeklytotalInboundCall = this.caagentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.totalInBoundsCalls, 0);
        this.caweeklytotalOutboundCall = this.caagentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.totalOutBoundsCall, 0);
        this.caweeklytotalMissedCall = this.caagentWiseWeeklyfilterdata.reduce((acc, cur) => acc + cur.missedCalls, 0);
    }

    filterDataAgentWiseCA() {
        this.caagentWisefilterdata = new Array<SpectrumLiveData>();
        if (this.caAgencyList.length === 0) {
            //this.agentWisefilterdata = this.agentWiseData;
            this.caAgencyItem.forEach(m => {
                let tempfilterdata = new Array<SpectrumLiveData>()
                tempfilterdata = this.caagentWiseData.filter(z => z.location === m.value)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.caagentWisefilterdata.push(a);
                    })
                }
            })
        }
        else {
            this.caAgencyList.forEach(m => {
                let tempfilterdata = new Array<SpectrumLiveData>()
                tempfilterdata = this.caagentWiseData.filter(z => z.location === m)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.caagentWisefilterdata.push(a);
                    })
                }
            })
        }
        this.GetTotalCAAgentWise();

    }

    filterDataAgentLogLiveCA() {
        this.filterAgentLogAgencyWiseDataCA = new Array<AgencyAgentLog>();
        if (this.caAgencyList.length === 0) {
            this.caAgencyItem.forEach(m => {
                let tempfilterdata = new Array<AgencyAgentLog>()
                tempfilterdata = this.agentLogAgencyWiseDataCA.filter(z => z.agencyName === m.value)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.filterAgentLogAgencyWiseDataCA.push(a);
                    })
                }
            })
        }
        else {
            this.caAgencyList.forEach(m => {
                let tempfilterdata = new Array<AgencyAgentLog>()
                tempfilterdata = this.agentLogAgencyWiseDataCA.filter(z => z.agencyName === m)
                if (tempfilterdata.length > 0) {
                    tempfilterdata.forEach(a => {
                        this.filterAgentLogAgencyWiseDataCA.push(a);
                    })
                }
            })
        }

    }

}
