import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { VsTeamComponent} from './vs-team.component';

const routes: Routes = [
    { path: 'vs-team', component: VsTeamComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class vsTeamRoustingModule { }
