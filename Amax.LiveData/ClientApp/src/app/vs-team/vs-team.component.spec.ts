import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VsTeamComponent } from './vs-team.component';

describe('VsTeamComponent', () => {
  let component: VsTeamComponent;
  let fixture: ComponentFixture<VsTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VsTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VsTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
