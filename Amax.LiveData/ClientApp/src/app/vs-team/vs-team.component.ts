import { Component, OnInit, Pipe } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { Agency, AgencyAgentLog, AgentCallTransfer, RegionalManagers, SpectrumAgentExtensionLog, SpectrumLiveAgentCallDetails, SpectrumLiveData, ZonalManager } from '../../Model/SpectrumLiveData';
import { LiveDataService } from '../../services/LiveDataService';
//import { DownloadexcelService } from 'src/services/downloadexcel.service';
//import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vs-team',
  templateUrl: './vs-team.component.html',
  styleUrls: ['./vs-team.component.css']
})
export class VsTeamComponent implements OnInit {

    AgentCallPopup: boolean = false;
    AgentCallsTranserPopup: boolean = false;
    agencyName: string = '';
    extension: string = '';

    refreshTime: string = "";
    userIdencrypted: string = "";
    userRoleencrypted: string = "";
    userId: string = "";
    userRole: string = "";
    showLoader: boolean = false;
    agentWisefilterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    agentWiseData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    liveSpectrumExtensionData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    liveSpectrumExtensionfilterData: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

    spectrumLiveAgentCallDetailsData: Array<SpectrumLiveAgentCallDetails> = new Array<SpectrumLiveAgentCallDetails>();
    spectrumLiveAgentCallDetailsfilterData: Array<SpectrumLiveAgentCallDetails> = new Array<SpectrumLiveAgentCallDetails>();

    agentCallTransferData: Array<AgentCallTransfer> = new Array<AgentCallTransfer>();

    agentWisetotalInbound: number = 0;
    agentWisetotalOutbound: number = 0;
    agentWisetotalInboundCall: number = 0;
    agentWisetotalOutboundCall: number = 0;
    agentWisemissedCall: number = 0;
    agentwiseInBoundCallDuration: number = 0;
    agentwiseOutBoundCallDuration: number = 0;
    agentwiseTotalTransferCall: number = 0;
    agentwiseTotalTransferCallDuration: number = 0;

    constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.GetRefeshTime();

        this.route.queryParams.subscribe((data) => {
            this.userIdencrypted = data['userId'];
            this.userRoleencrypted = data['UserRole'];
            this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
            this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
        });

        this.GetAgentWiseData();
    }

    GetAgentWiseData() {
        this.showLoader = true;
        this.service.GetStatsLiveDataAgentWiseVSTeamV2(this.userId, this.userRole).subscribe(m => {
            this.agentWiseData = this.agentWisefilterdata = this.liveSpectrumExtensionData = this.liveSpectrumExtensionfilterData = m;
            this.showLoader = false;
            this.GetTotalAgentWise();
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    GetTotalAgentWise() {
        this.agentWisetotalInbound = 0;
        this.agentWisetotalOutbound = 0;
        this.agentWisetotalInboundCall = 0;
        this.agentWisetotalOutboundCall = 0;
        this.agentWisemissedCall = 0;
        this.agentwiseInBoundCallDuration = 0;
        this.agentwiseOutBoundCallDuration = 0;
        this.agentwiseTotalTransferCall = 0;
        this.agentwiseTotalTransferCallDuration = 0;
        this.agentWisefilterdata.forEach(m => {
            this.agentWisetotalInbound = this.agentWisetotalInbound + m.inBoundCalls;
            this.agentWisetotalOutbound = this.agentWisetotalOutbound + m.outboundCalls;
            this.agentWisetotalInboundCall = this.agentWisetotalInboundCall + m.totalInBoundsCalls;
            this.agentWisetotalOutboundCall = this.agentWisetotalOutboundCall + m.totalOutBoundsCall;
            this.agentWisemissedCall = this.agentWisemissedCall + m.missedCalls;
            this.agentwiseInBoundCallDuration = this.agentwiseInBoundCallDuration + m.inboundDuration;
            this.agentwiseOutBoundCallDuration = this.agentwiseOutBoundCallDuration + m.outBoundDuration;

            this.agentwiseTotalTransferCall = this.agentwiseTotalTransferCall + m.callTransfer;
            this.agentwiseTotalTransferCallDuration = this.agentwiseTotalTransferCallDuration + m.transferCallDuration;
        })
    }

    GetRefeshTime() {
        this.service.getSpectrumRefeshTime().subscribe(m => {
            this.refreshTime = m;
        })
    }

    secondsToHms(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? "h, " : "h, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? "m, " : "m, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? "s" : "s") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    convertDate(date: Date) {
        let yourDate = new Date(date).getUTCDate;
        yourDate.toString();
    }

    OpenAgentCallPopup(location: any, extension: any) {
        this.showLoader = true;
        this.service.GetSpectrumAgentCallDetails(this.userId, this.userRole, extension).subscribe(m => {
            this.spectrumLiveAgentCallDetailsData = this.spectrumLiveAgentCallDetailsfilterData = m;
            this.showLoader = false;
            this.agencyName = location;
            this.extension = extension;
            this.AgentCallPopup = true;
            console.log("m", m);
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }

    OpenCallTransferPopup(location: any, extension: any, agencyId: any) {
        this.agentCallTransferData = new Array<AgentCallTransfer>();
        this.showLoader = true;
        this.service.GetCallTransferDetails(this.userId, this.userRole, extension, agencyId).subscribe(m => {
            this.agentCallTransferData = m;
            this.showLoader = false;
            this.agencyName = location;
            this.extension = extension;
            this.AgentCallsTranserPopup = true;
            console.log("m", m);
        },
            error => {
                this.showLoader = false;
                this.router.navigateByUrl("/");
            })
    }
}
