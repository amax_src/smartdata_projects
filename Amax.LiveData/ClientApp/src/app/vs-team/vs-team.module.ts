import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';

import { MatTabsModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';

import { VsTeamComponent} from './vs-team.component';

@NgModule({
  declarations: [VsTeamComponent],
  imports: [
      MultiSelectModule,
      CommonModule,
      FormsModule,
      BrowserModule,
      BrowserAnimationsModule,
      SidebarModule,
      MatTabsModule,
      MatIconModule, DropdownModule, DialogModule, CalendarModule,
  ]
})
export class VsTeamModule { }
