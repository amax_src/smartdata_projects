import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { AppRoutingModule } from './app-routing.module';
import { SidebarModule } from './shared/sidebar/sidebar.module';
import { ReportModule } from './report/report.module';
import { LoginComponent } from './login/login.component';
import { LoginModule } from './login/login.module';
import { VanityNumberReportModule } from './vanityNumberReport/vanityNumberReport.module';
import { ConsolidatedVanityNumberReportModule } from './consolidatedVanityNumberReport/consolidatedVanityNumberReport.module';
import { SpectrumLiveDataModule } from './SpectrumLiveData/SpectrumLiveData.module';
import { AmaxConsolidatedHourlyModule } from './AmaxConsolidatedHourly/AmaxConsolidatedHourly.module';
import { AmaxAgencyHourlyModule } from './AmaxAgencyHourly/AmaxAgencyHourly.module';
import { BolQuotesModule } from './BolQuotes/BolQuotes.module';
import {EodliveReportModule } from './eodlive-report/eodlive-report.module';
import { CustomerJourneyModule } from './customer-journey/customer-journey.module';
import { SpectrumLiveDataNewModule } from './SpectrumLiveDataNew/SpectrumLiveDataNew.module';
import { VsTeamModule } from './vs-team/vs-team.module';
import { AlpaBolReportModule } from './alpa-bol-report/alpa-bol-report.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HomeModule,
    SidebarModule,
    HttpClientModule,
    AppRoutingModule,
    ReportModule,
    LoginModule,
    AmaxConsolidatedHourlyModule,
    VanityNumberReportModule,
    ConsolidatedVanityNumberReportModule,
    SpectrumLiveDataModule,
    AmaxAgencyHourlyModule,
    BolQuotesModule,
    EodliveReportModule,
      CustomerJourneyModule, SpectrumLiveDataNewModule, VsTeamModule, AlpaBolReportModule
   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
