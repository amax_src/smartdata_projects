import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { LiveDataService } from '../../services/LiveDataService';
import { LiveDataAgency } from '../../Model/LiveDataAgency';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { ReasonForWalkOutCustomer } from '../../Model/ReasonForWalkOutCustomer';
import { SpectrumLiveData } from '../../Model/SpectrumLiveData';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html'
})
export class ReportComponent implements OnInit {
  Agency: string = "Select one";
  //selectedAgency: string = '';
  AgencyItem: SelectItem[] = [];
  showLoader: boolean = true;
  data: Array<LiveDataAgency> = new Array<LiveDataAgency>();
  singleAgencyData: LiveDataAgency = new LiveDataAgency();
  refreshTime: string = "";
  userId: string = "";
  userRole: string = "";
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  remaningDays: number = 0;
  policyGoal: number = 0;
  AgencyFeeGoal: number;
  PremiumGoal: number; 
  walkOutReason: Array<ReasonForWalkOutCustomer> = new Array<ReasonForWalkOutCustomer>();
    Spectrumdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    SpectrumdataNew: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
  SpectrumdataSingleAgency: SpectrumLiveData = new SpectrumLiveData();


  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private route: ActivatedRoute, private encryp: encryption) { }

    ngOnInit() {
        this.GetRefeshTime();
        this.getSpectrumStratsData();
        this.getSpectrumData()
        //  this.selectedAgency = "Select All";
        this.route.queryParams.subscribe((data) => {
            this.userIdencrypted = data['userId'];
            this.userRoleencrypted = data['UserRole'];
            this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
            this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
        });
        //if (!this.userId) {
        //  this.userId = this._auth.userId.toString();
        //}
        //if (!this.userRole) {
        //  this.userRole = this._auth.userRole;
        //}
        this.service.getData(this.userId, this.userRole).subscribe(m => {
            this.data = m;
            this.FillDropDown();
            this.showLoader = false;
        })
    }

    FillDropDown() {
        this.AgencyItem = [];
        this.AgencyItem.push(
            {
                label: "Select one", value: "Select one"
            })
        this.data.forEach(m => {
            this.AgencyItem.push(
                {
                    label: m.agencyName, value: m.agencyName
                })
        })
    }

    onSelect() {
        this.remaningDays = 0;
        this.SpectrumdataNew.forEach(m => {
            if (m.location === this.Agency) {
                this.SpectrumdataSingleAgency = m;
            }
        })

        if (this.SpectrumdataSingleAgency.inBoundCalls == 0 && this.SpectrumdataSingleAgency.outboundCalls == 0) {
            this.Spectrumdata.forEach(m => {
                if (m.location === this.Agency) {
                    this.SpectrumdataSingleAgency = m;
                }
            })
        }

        this.Spectrumdata.forEach(m => {
            if (m.location === this.Agency) {
                this.SpectrumdataSingleAgency = m;
            }
        })
        console.log(this.SpectrumdataSingleAgency);

        this.singleAgencyData = new LiveDataAgency();
        let tempfilterdata = new Array<LiveDataAgency>()
        console.log(this.data);
        tempfilterdata = this.data.filter(z => z.agencyName === this.Agency)

        console.log(tempfilterdata);
        if (tempfilterdata.length > 0) {
            tempfilterdata.forEach(a => {
                this.singleAgencyData = a;

            })
            this.caculateDailyGoal();
            this.GetReasonForWalkOutCustomer();
        }
    }

  caculateDailyGoal() {
    this.remaningDays = this.singleAgencyData.remainingDays;
    console.log("this.singleAgencyData", this.singleAgencyData);

    var currentdate = new Date();
    var dayNumber=currentdate.getDay();
    console.log("this.refreshTime", this.refreshTime)
    console.log("his.refreshTime", this.refreshTime.split(':', 2)[0]);
    console.log("his.refreshTimeAM", this.refreshTime.split(' ', 2)[1]);

    if (this.refreshTime != null && this.refreshTime != undefined) {
      let time = this.refreshTime.split(':', 2)[0];
      let AMPM = this.refreshTime.split(' ', 2)[1];
      if (parseInt(time) >= 7 && parseInt(time) < 12 && AMPM == 'PM' && dayNumber != 6) {
        console.log("AMPM")
        this.remaningDays = this.remaningDays - 1;
        if (this.remaningDays == 0) {
          this.remaningDays = 1;
        }
      }
      else if (parseInt(time) >= 5 && parseInt(time) < 12 && AMPM == 'PM' && dayNumber == 6) {
        console.log("Saturday")
        this.remaningDays = this.remaningDays - 1;
        if (this.remaningDays == 0) {
          this.remaningDays = 1;
        }
      }
    }
    console.log("this.remaningDayslll", this.remaningDays);

    this.singleAgencyData.todayPolicyCount = ((this.singleAgencyData.policyGoal - (this.singleAgencyData.mtdPolicy + this.singleAgencyData.policyCount)) / this.remaningDays);
    this.singleAgencyData.todayAgencyFee = ((this.singleAgencyData.agencyFeeGoal - (this.singleAgencyData.mtdAgencyFee + this.singleAgencyData.agencyFee)) / this.remaningDays);
    this.singleAgencyData.todayPremium = ((this.singleAgencyData.premiumGoal - (this.singleAgencyData.mtdPremium + this.singleAgencyData.premium)) / this.remaningDays);
  }
  //caculateDailyGoal() {
  //  this.remaningDays = this.singleAgencyData.remainingDays;
  //  this.singleAgencyData.todayPolicyCount = ((this.singleAgencyData.policyGoal - (this.singleAgencyData.mtdPolicy + this.singleAgencyData.policyCount)) / this.remaningDays);
  //  this.singleAgencyData.todayAgencyFee = ((this.singleAgencyData.agencyFeeGoal - (this.singleAgencyData.mtdAgencyFee + this.singleAgencyData.agencyFee)) / this.remaningDays);
  //  this.singleAgencyData.todayPremium = ((this.singleAgencyData.premiumGoal - (this.singleAgencyData.mtdPremium + this.singleAgencyData.premium)) / this.remaningDays);
  //}

  GetRefeshTime() {
    this.service.getRefeshTime().subscribe(m => {
      this.refreshTime = m;
    })

  }

  GetReasonForWalkOutCustomer() {
    this.walkOutReason = new Array<ReasonForWalkOutCustomer>();
    this.service.GetReasonForWalkOutCustomer(this.Agency).subscribe(e => {
      let _Reason: Array<ReasonForWalkOutCustomer> = new Array<ReasonForWalkOutCustomer>();
      _Reason = e;
      _Reason.forEach(m => {
        //let indexOfSold = m.note.toLowerCase().indexOf("sold")
        //if (indexOfSold == -1) {
          this.walkOutReason.push(m);
        //}
      })
    })
  }

    getSpectrumData() {
        this.service.getSpectrumData(this.userId, this.userRole).subscribe(m => {
            this.Spectrumdata = m;
        })
    }
    getSpectrumStratsData() {
        this.service.getSpectrumDataNewV2(this.userId, this.userRole).subscribe(m => {
            this.SpectrumdataNew = m;
        })
    }

}
