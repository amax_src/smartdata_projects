import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report.component';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  imports: [
    DropdownModule,
    CommonModule,
    FormsModule,
    ReportRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
  ],
  declarations: [ReportComponent],
  exports: []
 })
export class ReportModule { }
