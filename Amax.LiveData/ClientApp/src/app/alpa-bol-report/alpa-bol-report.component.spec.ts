import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlpaBolReportComponent } from './alpa-bol-report.component';

describe('AlpaBolReportComponent', () => {
  let component: AlpaBolReportComponent;
  let fixture: ComponentFixture<AlpaBolReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlpaBolReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlpaBolReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
