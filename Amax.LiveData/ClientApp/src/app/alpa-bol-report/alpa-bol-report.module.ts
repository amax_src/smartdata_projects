import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AlpaBolReportRoutingModule } from './alpa-bol-report-routing.module';

import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { CalendarModule } from 'primeng/calendar';
import { AlpaBolReportComponent } from './alpa-bol-report.component';

@NgModule({
  declarations: [AlpaBolReportComponent],
  imports: [
      CommonModule,
      FormsModule,
      AlpaBolReportRoutingModule, MultiSelectModule, BrowserModule, BrowserAnimationsModule, SidebarModule, CalendarModule
  ]
})
export class AlpaBolReportModule { }
