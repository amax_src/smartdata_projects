import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AlpaBolReportComponent} from './alpa-bol-report.component';

const routes: Routes = [{ path: 'alpa-bol-report', component: AlpaBolReportComponent }]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AlpaBolReportRoutingModule { }
