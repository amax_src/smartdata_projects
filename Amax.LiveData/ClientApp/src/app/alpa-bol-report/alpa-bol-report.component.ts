import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { BolData } from '../../Model/BolQuotes';
import { NameValue } from '../../Model/NameValue';
import { SelectItem } from 'primeng/api';
import { concat } from 'rxjs/operators';
import { DownloadexcelService } from 'src/services/downloadexcel.service';
import { AlpaBolQuotesService } from '../../services/AlpaBolQuotesService';
@Component({
  selector: 'app-alpa-bol-report',
  templateUrl: './alpa-bol-report.component.html',
  styleUrls: ['./alpa-bol-report.component.css']
})
export class AlpaBolReportComponent implements OnInit {
    showLoader: boolean = false;
    PolicyCount: number = 0;
    BuyNowOption: number = 0;
    BuyNowClick: number = 0;
    Total: number = 0;
    totalpremium: number = 0;
    totalCollectedPremium: number = 0;
    totalAfee: number = 0;
    txState: number = 0;
    azState: number = 0;

    BolQuotes: Array<BolData> = new Array<BolData>();
    Carriers: Array<string> = new Array<string>();
    States: Array<string> = new Array<string>();
    FilerData: Array<BolData> = new Array<BolData>();

    CarriersItem: SelectItem[] = [];
    StateItem: SelectItem[] = [];

    date: Array<Date>;
    delayedPolicy: number = 0;

    constructor(private service: AlpaBolQuotesService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
        private route: ActivatedRoute, private _downloadExcel: DownloadexcelService) {
    }

    ngOnInit() {
        this.BindState();
        this.GetCarrierData();
  }

    BindState() {
        this.StateItem.push(
            {
                label: 'TX', value: 'TX'
            }, {
            label: 'AZ', value: 'AZ'
        })
    }
    GetCarrierData() {
        this.service.GetAlpaCarrierNames().subscribe(m => {
            this.CarriersItem = [];
            m.forEach(m => {
                this.CarriersItem.push(
                    {
                        label: m.name, value: m.id
                    })
            })

        })
    }

    GetBolData() {
        this.FilerData = new Array<BolData>();
        this.BolQuotes = new Array<BolData>();
        this.Total = 0;
        this.totalAfee = 0; this.totalpremium = 0; this.totalCollectedPremium = 0;
        this.BuyNowClick = 0;
        this.BuyNowOption = 0; this.PolicyCount = 0; this.txState = 0; this.azState = 0;
        if (this.Carriers.length == 0) {
            this.GetCarrierData();
        }
        if (
            (this.date && this.date[0] != null && this.date[1] != null && this.Carriers.length > 0)
            || (this.date && this.date[0] != null && this.date[1] != null && this.States.length > 0)
            || (this.date && this.date[0] != null && this.date[1] != null && this.States.length > 0 && this.Carriers.length > 0)
        ) {
            this.showLoader = true;


            this.service.GetAlpaDataBolData_Quotes(this.Carriers, this.date, this.States).subscribe(m => {
                this.BolQuotes = m;

                console.log(this.BolQuotes);
                let BolQuotesFilter: Array<BolData> = new Array<BolData>();
                BolQuotesFilter = this.BolQuotes.filter(m => m.policyNumber !== '' && m.buyNowClick === true);
                this.PolicyCount = BolQuotesFilter.length;

                let _txState = this.BolQuotes.filter(m => m.state === "TX" && m.policyNumber !== '' && m.buyNowClick === true);
                this.txState = _txState.length;

                let _azState = this.BolQuotes.filter(m => m.state === "AZ" && m.policyNumber !== '' && m.buyNowClick === true);
                this.azState = _azState.length;

                BolQuotesFilter = new Array<BolData>();
                BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowClick === true);
                this.BuyNowClick = BolQuotesFilter.length;

                BolQuotesFilter = new Array<BolData>();
                //BolQuotesFilter = this.BolQuotes.filter(m => m.buyNowOption === true);
                BolQuotesFilter = this.BolQuotes;
                this.BuyNowOption = BolQuotesFilter.length;

                this.Total = this.BolQuotes.length;
                this.FilerData = this.BolQuotes.filter(m => m.buyNowClick === true);
                this.showLoader = false;
                let delayed = this.FilerData.filter(m => m.isdelayed === true);
                this.delayedPolicy = delayed.length;
                //TX AZ
                this.totalAfee = 0; this.totalpremium = 0; this.totalCollectedPremium = 0;
                this.FilerData.forEach(z => {
                    this.totalAfee = this.totalAfee + z.agencyFee;
                    this.totalpremium = this.totalpremium + z.premiumAmount;
                    this.totalCollectedPremium = this.totalCollectedPremium + z.collectedPremium;
                })
            })
        }
    }

    onSelect(e) {
        this.GetBolData();
    }
    onStateSelect(e) {
        this.GetBolData();
    }

    onDateSelect(e) {
        //this.date = e;
        this.GetBolData();

    }
    SelectBackGround(e) {
        if (e.isdelayed) {
            return 'green'
        }
        else if (e.isQuoteDataMissing) {
            return 'red'
        }
        else {
            return ''
        }
    }

    //exportExcel() {
    //    let data = [];
    //    data = this.FilerData;
    //    this.service.ExportOnlineQuote(data).subscribe(x => {
    //        this._downloadExcel.downloadFile(x);
    //        console.log("FileName", x);
    //        this.showLoader = false;
    //    })
    //}
}
