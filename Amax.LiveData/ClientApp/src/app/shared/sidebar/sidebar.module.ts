import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [
    BrowserModule,
    RouterModule 
  ],
  declarations: [SidebarComponent],
  exports: [SidebarComponent]
 })
export class SidebarModule { }
