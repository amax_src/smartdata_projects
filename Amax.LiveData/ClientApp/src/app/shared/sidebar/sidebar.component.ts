import { Component, AfterViewChecked } from '@angular/core';
import { CommonService } from '../../../services/CommonService';
import { ActivatedRoute, Router } from '@angular/router';
import { authenticationService } from '../../login/authenticationService.service';
import { encryption } from '../../../services/Encryption';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent {

  userId: string;
  userRole: string;
  decryptedUserRole: string;
  constructor(private cmnSrv: CommonService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: authenticationService,
    private encryp: encryption
  ) {
    this.route.queryParams.subscribe((data) => {
      this.userId = data['userId'];
      this.userRole = data['UserRole'];
    });
    if (this.userRole !== '') {
      this.decryptedUserRole = this.encryp.encrytptvalue(this.userRole, false)
    }
    else {
      this.decryptedUserRole = this.authenticationService.userRole;
    }
    console.log("this.userId", this.userId)
    if (this.userId == undefined || this.userId == '') {
      //this.router.navigateByUrl("/");
    }
  }

  Logout() {
    this.authenticationService.logout();
      this.router.navigateByUrl("/");
      this.authenticationService.userRole
    }
    //if(this.authenticationService.userRole === "zonalmanager"
    //    || this.authenticationService.userRole === "regionalmanager"
    //    || this.authenticationService.userRole === "storemanager"
    //    || this.authenticationService.userRole === "headofdepratment"
    //    || this.authenticationService.userRole === "chieffinancialofficer"
    //    || this.authenticationService.userRole === "chiefexecutiveofficer"
    //    || this.authenticationService.userRole === "chiefOperatingofficer"
    //    || this.authenticationService.userRole === "saledirector"
    getRoleName(role: any) {
        if (role == "headofdepratment") {
            return "Head of Depratment";
        }
        else if (role == "chieffinancialofficer") {
            return "Chief Financial Officer";
        }
        else if (role == "chiefexecutiveofficer") {
            return "";
        }
        else if (role == "chiefOperatingofficer") {
            return "";
        }
        else if (role == "saledirector") {
            return "Sale of Director";
        }
        else if (role == "regionalmanager") {
            return "Regional Manager";
        }
        else if (role == "zonalmanager") {
            return "Distict Manager";
        }
        else if (role == "storemanager") {
            return "Store Manager";
        }
        
    }
}
