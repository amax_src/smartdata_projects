import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { AmaxConsolidatedHourly } from './AmaxConsolidatedHourly.component';
import { AmaxConsolidatedHourlyRoutingModule } from './AmaxConsolidatedHourly-routing.module';
import { ChartModule } from 'primeng/chart';
import { MatTabsModule } from '@angular/material'

@NgModule({
  imports: [
    MultiSelectModule,
    MultiSelectModule,
    CommonModule,
    FormsModule,
    AmaxConsolidatedHourlyRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    ChartModule,
    MatTabsModule
  ],
  declarations: [AmaxConsolidatedHourly],
  exports: []
 })
export class AmaxConsolidatedHourlyModule { }
