import { Component, OnInit } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { AmaxHourlyLoad } from '../../Model/AmaxHourlyLoad';
import { DownloadexcelService } from '../../services/downloadexcel.service';

@Component({
  selector: 'app-AmaxConsolidatedHourly',
  templateUrl: './AmaxConsolidatedHourly.component.html',
})
export class AmaxConsolidatedHourly implements OnInit {
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";
  showLoader: boolean;
  refreshTime: string;
  data: Array<AmaxHourlyLoad>;
  TotalNewCount: number = 0
  Totalprem: number = 0;
  TotalFee: number = 0;
  TotalTrans: number = 0;
  ConsolatedChartData: any;
  displayLinegraphAmaxHourly: boolean = false;
  chartOptions: any = {
    animation: {
      duration: 0
    },
    responsive: false,
    maintainAspectRatio: false,
    layout: {
      padding: {
        right: 50
      }
    },
    plugins: {
      datalabels: {
        align: 'end',
        anchor: 'end',
        color: 'black',
        font: {
          weight: 'bold'
        },
        formatter: function (value, context) {
          if (context && (context.dataset.label === "Agency Fee" || context.dataset.label === "Premium")) {
            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
          else {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
        }
      }
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          if (data && data.datasets && data.datasets.length > 0 && (data.datasets[tooltipItem.datasetIndex].label === "Agency Fee" || data.datasets[tooltipItem.datasetIndex].label === "Premium")) {
            return data.datasets[tooltipItem.datasetIndex].label + `- $${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
          else {
            return data.datasets[tooltipItem.datasetIndex].label + `- ${(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;

          }
        }
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          callback: function (label) {
            return label;
          },
          fontColor: "#000"
        },
        scaleLabel: {
          display: true,
          fontColor: "#000"
        }
      }],
      yAxes: [{
        ticks: {
          fontColor: "#000"
        }
      }]
    }

  }

  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute, private _downloadExcel:DownloadexcelService) {
  }


  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

    this.data = new Array<AmaxHourlyLoad>();
    this.TotalNewCount = 0;
    this.Totalprem = 0;
    this.TotalFee = 0;
    this.TotalTrans = 0;

    this.GetRefeshTime();
    this.service.getAmaxConsolidated(this.userId, this.userRole).subscribe(m => {
      this.data = m;
      this.showLoader = false;
      this.data.forEach((e, i) => {
        this.TotalNewCount = this.TotalNewCount + e.policies;
        this.Totalprem = this.Totalprem + e.premium;
        this.TotalFee = this.TotalFee + e.agencyfee;
        this.TotalTrans = this.TotalTrans + e.transactions;
      });

    },
      error => {
        this.router.navigateByUrl("/");
      })
  }

  GetRefeshTime() {
    this.service.getRefeshTime().subscribe(m => {
      this.refreshTime = m;
    })
  }

  TabChange(e) {
    if (e === 1) {
      this.CreatChartViewForAmax();
    }
  }

  CreatChartViewForAmax() {

    let Intervel = [];
    let Transactions = []
    let Premium = []
    let Policies = []
    let AgencyFee = []
    let AgentCount = []
    this.data.forEach((e, i) => {
      Intervel.push(e.hourIntervel);
      Transactions.push(e.transactions)
      Premium.push(e.premium)
      Policies.push(e.policies)
      AgencyFee.push(e.agencyfee)
      AgentCount.push(e.agentCount)
    })

    this.ConsolatedChartData = {
      labels: Intervel,
      datasets: [
        {
          label: 'Policies',
          data: Policies,
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'Agency Fee',
          data: AgencyFee,
          fill: false,
          borderColor: '#565656'
        }
        ,
        {
          label: 'Premium',
          data: Premium,
          fill: false,
          borderColor: '#42A5F5'
        }
        ,
        {
          label: 'Transactions',
          data: Transactions,
          fill: false,
          borderColor: '#9CCC65'
        },
        {
          label: 'Agents',
          data: AgentCount,
          fill: false,
          borderColor: '#9FFC65'
        }
      ]
    }
  }

  exportExcel() {
    this.service.ExportHourlyData(this.data).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      console.log("FileName", x);
      this.showLoader = false;
    })
  }
}
