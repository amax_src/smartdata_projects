import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AmaxConsolidatedHourly } from "./AmaxConsolidatedHourly.component";

const routes: Routes = [
  { path: 'AmaxConsolidated', component: AmaxConsolidatedHourly}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AmaxConsolidatedHourlyRoutingModule { }
