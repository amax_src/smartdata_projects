import { Component, OnInit, HostListener } from '@angular/core';
import { LiveDataService } from '../../services/LiveDataService';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { CustomerJourneyInfo } from '../../Model/EODLiveReport';
import { SelectItem } from 'primeng/api';
import { concat } from 'rxjs/operators';
import { PageService } from './../../services/pageService';
import { DownloadexcelService} from '../../services/downloadexcel.service';

@Component({
  selector: 'app-customer-journey-info',
  templateUrl: './customer-journey-info.component.html',
  styleUrls: ['./customer-journey-info.component.css']
})
export class CustomerJourneyInfoComponent implements OnInit {
  showLoader: boolean = false;
  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute, private _pageService: PageService, private _downloadExcel: DownloadexcelService) { }

  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "null";
  userRole: string = "null";

  eODLiveReport: Array<CustomerJourneyInfo>;
  filtereODLiveReport: Array<CustomerJourneyInfo>;
  PolicySold: SelectItem[] = [];

  date: Array<Date>;
  AgencyList: Array<string> = new Array<string>();
  AgencyItem: SelectItem[] = [];
  IsSoldPolicy: string = "0";
  innerHeight: number = 0;
  innercontainer: string = "400px";

  ngOnInit() {
    this.cmnSrv.navbarToggle();
    this.innerHeight = (window.innerHeight - 250);
    this.innercontainer = this.innerHeight.toString() + "px";
    this.FillDropDown();
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = 0;
    this.innerHeight = (window.innerHeight - 210);
    this.innercontainer = this.innerHeight.toString() + "px";
  }

  FillDropDown() {
    this.PolicySold = [];
    this.PolicySold.push(
      {
        label: "All Quotes", value: "0"
      })
    this.PolicySold.push(
      {
        label: "Sold Quote", value: "1"
      })
  }

  GetCustomerJourneyInfoReport() {
    this.filtereODLiveReport = new Array<CustomerJourneyInfo>();
    if (this.date && this.date[0] != null && this.date[1] != null) {
      this.showLoader = true;
      this.service.GetCustomerJourneyInfoReport(this.date).subscribe(m => {
        this.eODLiveReport = m;
        this.showLoader = false;
        if (this.IsSoldPolicy == "1") {
          this.eODLiveReport = this.eODLiveReport.filter(x => x.isSold == true);
          this.filtereODLiveReport = this.eODLiveReport;
        }
        else {
          this.filtereODLiveReport = this.eODLiveReport;
        }
        

        console.log(this.eODLiveReport)
        this.setPolicyCountPaging(1);
      });
    }
  }

  onDateSelect(e) {
  }

  SearchData() {
    this.GetCustomerJourneyInfoReport();
  }
  onSelect(e) {

  }

  pagerPolicyCount: any = {};
  pagedPolicyCount: any[];

  setPolicyCountPaging(page: number) {
    if (page < 1 || page > this.pagerPolicyCount.totalPages) {
      return;
    }
    // get pager object from service
    this.pagerPolicyCount = this._pageService.getPayrollPager(this.filtereODLiveReport.length, page, 100);
    // get current page of items
    this.pagedPolicyCount = this.filtereODLiveReport.slice(this.pagerPolicyCount.startIndex, this.pagerPolicyCount.endIndex + 1);
  }


  exportExcel() {
    if (this.date && this.date[0] != null && this.date[1] != null) {
      this.showLoader = true;
      this.service.ExportCustomerJourneyInfoReport(this.date).subscribe(x => {
        this._downloadExcel.downloadFile(x);
        console.log("FileName", x);
        this.showLoader = false;
      })
    }
    else {
      alert("Please select date");
    }
  }
}
