import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerJourneyRoutingModule } from './customer-journey-routing.module';
import { CustomerJourneyInfoComponent } from './customer-journey-info.component';

import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  declarations: [CustomerJourneyInfoComponent],
  imports: [
    CommonModule,
    CustomerJourneyRoutingModule,
    FormsModule,
    MultiSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    CalendarModule,
    DropdownModule
  ]
})
export class CustomerJourneyModule { }
