import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerJourneyInfoComponent } from './customer-journey-info.component';

describe('CustomerJourneyInfoComponent', () => {
  let component: CustomerJourneyInfoComponent;
  let fixture: ComponentFixture<CustomerJourneyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerJourneyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerJourneyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
