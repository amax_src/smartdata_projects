import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {CustomerJourneyInfoComponent } from './customer-journey-info.component';

const routes: Routes = [{ path: 'CustomerJourneyInfoReport', component: CustomerJourneyInfoComponent }]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerJourneyRoutingModule { }
