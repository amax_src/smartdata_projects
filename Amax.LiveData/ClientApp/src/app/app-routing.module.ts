import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from './home/home.component';
import { ReportComponent } from './report/report.component';
import { LoginComponent } from "./login/login.component";
import { VanityNumberReportComponent } from "./vanityNumberReport/vanityNumberReport.component";
import { SpectrumLiveDataComponent } from "./SpectrumLiveData/SpectrumLiveData.component";
import { AmaxConsolidatedHourly } from "./AmaxConsolidatedHourly/AmaxConsolidatedHourly.component";
import { AmaxAgencyHourly } from "./AmaxAgencyHourly/AmaxAgencyHourly.component";
import { BolQuotesComponent } from "./BolQuotes/BolQuotes.component";
import { EodLiveReportComponent } from "./eodlive-report/eod-live-report.component";
import { CustomerJourneyInfoComponent} from './customer-journey/customer-journey-info.component';
import { SpectrumLiveDataNewComponent } from "./SpectrumLiveDataNew/SpectrumLiveDataNew.component";
import { VsTeamComponent } from './vs-team/vs-team.component';
import { AlpaBolReportComponent } from './alpa-bol-report/alpa-bol-report.component';
const routes: Routes = [
  //{ path: '', component: LoginComponent },
  { path: '', component: HomeComponent },
  { path: 'Home', component: HomeComponent },
  { path: 'EODReport', component: ReportComponent },
  { path: 'VanityNumberReport', component: VanityNumberReportComponent },
  //  { path: 'Spectrumlive', component: SpectrumLiveDataComponent },
    { path: 'Spectrumlive', component: SpectrumLiveDataNewComponent },
  { path: 'AmaxConsolidated', component: AmaxConsolidatedHourly },
  { path: 'AgencyHourly', component: AmaxAgencyHourly },
  { path: 'BindOnlineQuotes', component: BolQuotesComponent},
    { path: 'CustomerJourneyInfoReport', component: CustomerJourneyInfoComponent },
    { path: 'vs-team', component: VsTeamComponent },
    { path: 'alpa-bol-report', component: AlpaBolReportComponent }
 ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
