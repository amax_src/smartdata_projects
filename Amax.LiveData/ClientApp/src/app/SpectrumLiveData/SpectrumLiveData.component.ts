import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { SpectrumLiveData } from '../../Model/SpectrumLiveData';
import { LiveDataService } from '../../services/LiveDataService';

@Component({
  selector: 'app-SpectrumLiveData',
  templateUrl: './SpectrumLiveData.component.html',
})
export class SpectrumLiveDataComponent implements OnInit {
  data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
  filterdata: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();

 
  showLoader: boolean = true;
  deviceInfo = null;
  AgencyList: Array<string> = new Array<string>();
  selectedAgency: string = '';
  AgencyItem: SelectItem[] = [];
  refreshTime: string = "";
  
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";
  totalInbound: number = 0;
  totalOutbound: number = 0;
  constructor(private service: LiveDataService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute) {

  }


  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });

    //if (!this.userId) {
    //  this.userId = this._auth.userId.toString();
    //}
    //if (!this.userRole) {
    //  this.userRole = this._auth.userRole;
    //}

    this.selectedAgency = "Select All";
    this.GetRefeshTime();
    this.service.getSpectrumData(this.userId, this.userRole).subscribe(m => {
      this.data = m;
      this.filterdata = this.data;
      //console.log(this.filterdata);
      this.FillDropDown();
      this.GetTotal()
      this.showLoader = false;
      this.sortData("Incoming");
    },
      error => {
        this.router.navigateByUrl("/");
      })
  }

  onSelect(e) {
    this.filterdata = new Array<SpectrumLiveData>();
    if (this.AgencyList.length === 0) {
      this.filterdata = this.data;
    }
    else {
      this.AgencyList.forEach(m => {
        let tempfilterdata = new Array<SpectrumLiveData>()
        tempfilterdata = this.data.filter(z => z.location === m)
        if (tempfilterdata.length > 0) {
          tempfilterdata.forEach(a => {
            this.filterdata.push(a);
          })
        }
      })
    }
  }

  FillDropDown() {
    this.AgencyItem = [];
    this.data.forEach(m => {
      this.AgencyItem.push(
        {
          label: m.location, value: m.location
        })
    })
  }

  GetTotal() {
    this.totalInbound = 0;
    this.totalOutbound = 0;
    this.data.forEach(m => {
      this.totalInbound = this.totalInbound + m.inBoundCalls;
      this.totalOutbound = this.totalOutbound + m.outboundCalls;
    })
  }

  GetRefeshTime() {
    this.service.getSpectrumRefeshTime().subscribe(m => {
      this.refreshTime = m;
    })

  }



  sortDirection: string = "desc";
  sortBy: string = "";
  sortData(e) {
    if (this.sortBy === e) {
      if (this.sortDirection === "desc") {
        this.sortDirection = "asc";
      }
      else {
        this.sortDirection = "desc";
      }
    }
    this.sortBy = e;

    let Data: Array<SpectrumLiveData> = new Array<SpectrumLiveData>();
    switch (e) {
      
      case "Incoming":
        {

          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByIncoming);
          }
          else {
            Data = this.filterdata.sort(this.sortByIncoming);
          }
        }
        break
      case "Outgoing":
        {
          if (this.sortDirection === "desc") {
            Data = this.filterdata.sort(this.sortdescByOutgoing);
          }
          else {
            Data = this.filterdata.sort(this.sortByOutgoing);
          }
        }
        break
    }
    this.filterdata = Data;
  }



  sortdescByIncoming(a, b) {
    return b["inBoundCalls"] - a["inBoundCalls"];
  }

  sortByIncoming(a, b) {
    return a["inBoundCalls"] - b["inBoundCalls"];
  }

  sortdescByOutgoing(a, b) {
    return b["outboundCalls"] - a["outboundCalls"];
  }

  sortByOutgoing(a, b) {
    return a["outboundCalls"] - b["outboundCalls"];
  }

}
