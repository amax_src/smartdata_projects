import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { SpectrumLiveDataRoutingModule } from './SpectrumLiveData-routing.module';
import { SpectrumLiveDataComponent } from './SpectrumLiveData.component';
@NgModule({
  imports: [
    MultiSelectModule,
    CommonModule,
    FormsModule,
    SpectrumLiveDataRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
   
  ],
  declarations: [SpectrumLiveDataComponent],
  exports: []
 })
export class SpectrumLiveDataModule { }
