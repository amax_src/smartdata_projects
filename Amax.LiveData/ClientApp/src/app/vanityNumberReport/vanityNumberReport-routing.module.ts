import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VanityNumberReportComponent } from "./vanityNumberReport.component";

const routes: Routes = [
  { path: 'VanityNumberReport', component: VanityNumberReportComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class VanityNumberReportRoutingModule { }
