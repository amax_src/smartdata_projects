import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { VanityNumberReportComponent } from './vanityNumberReport.component';
import { VanityNumberReportRoutingModule } from './vanityNumberReport-routing.module';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  imports: [
    MultiSelectModule,
    CommonModule,
    FormsModule,
    VanityNumberReportRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    CalendarModule,
    DropdownModule
  ],
  declarations: [VanityNumberReportComponent],
  exports: []
 })
export class VanityNumberReportModule { }
