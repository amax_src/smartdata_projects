import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api'
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { VanityNumberReportService } from 'src/services/VanityNumberReportService';
import { ConsolidatedDidData } from '../../Model/ConsolidatedDidData';
import { parse } from 'querystring';

@Component({
  selector: 'app-vanity',
  templateUrl: './vanityNumberReport.component.html',
})
export class VanityNumberReportComponent implements OnInit {
  data: ConsolidatedDidData = new ConsolidatedDidData();
  filterdata: ConsolidatedDidData = new ConsolidatedDidData();
  allData: Array<ConsolidatedDidData> = new Array<ConsolidatedDidData>();
  AgencyList: Array<string> = new Array<string>();
  selectedAgency: string = '';
  AgencyItem: SelectItem[] = [];
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";
  allDigital: number = 0;
  dfwEnglishTv: number = 0;
  dfwSpanishTv: number = 0;
  radioEnglish: number = 0;
  radioSpanish: number = 0;
  tvEnglish: number = 0;
  tvSpanish: number = 0;
  quote: number = 0;
  sold: number = 0;
  year: string = "2020";
  dates: Array<Date>;
  day: string = "";
  month: string = "";
  endday: string = "";
  endmonth: string = "";
  endyear: string = "";
  vanityNumber: SelectItem[];
  selectedVanityNumber: Array<string>;
  callcenterCount: number = 0;
  constructor(private vanityService: VanityNumberReportService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute) {

  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });
    //Need to uncomment after
    //if (!this.userId) {
    //  this.userId = this._auth.userId.toString();
    //}
    //if (!this.userRole) {
    //  this.userRole = this._auth.userRole;
    //}


    this.vanityNumber = [
      { label: "All Digital", value: "allDigital" },
      { label: "DFW English TV", value: "dfwEnglishTv" },
      { label: "DFW Spanish TV", value: "dfwSpanishTv" },
      { label: "Radio English", value: "radioEnglishRgv" },
      { label: "Radio Spanish", value: "radioSpanisRrgv" },
      { label: "TV English", value: "tvEnglishHouston" },
      { label: "TV Spanish", value: "tvSpanishHouston" }
    ];
    this.selectedAgency = "Select All";
    this.getData();

  }
  getData() {
    debugger;
    this.filterdata = new ConsolidatedDidData();
    this.vanityService.getData(this.day, this.month, this.year, this.endday, this.endmonth, this.endyear, this.userId, this.userRole).subscribe(m => {
      this.data = m;
      this.filterdata = this.data;
      this.setTabsData();
      //this.FillDropDown();
    },
      error => {
        this.router.navigateByUrl("/");
      })
  }
  onSelect(e) {
    this.filterdata = new ConsolidatedDidData();
    this.selectedVanityNumber.forEach(x => {
      switch (x) {
        case 'allDigital': this.filterdata.allDigital = this.data.allDigital;
          break;
        case 'dfwEnglishTv': this.filterdata.dfwEnglishTv = this.data.dfwEnglishTv;
          break;
        case 'dfwSpanishTv': this.filterdata.dfwSpanishTv = this.data.dfwSpanishTv;
          break;
        case 'radioEnglishRgv': this.filterdata.radioEnglishRgv = this.data.radioEnglishRgv;
          break;
        case 'radioSpanisRrgv': this.filterdata.radioSpanisRrgv = this.data.radioSpanisRrgv;
          break;
        case 'tvEnglishHouston': this.filterdata.tvEnglishHouston = this.data.tvEnglishHouston;
          break;
        case 'tvSpanishHouston': this.filterdata.tvSpanishHouston = this.data.tvSpanishHouston;
          break;
      }

    });
    if (this.selectedVanityNumber.length === 0) {
      this.filterdata = this.data;
    }
  }


  onDateSelect(e) {

    var tempDate = new Date(this.dates[0]);
    this.day = tempDate.getDate().toString();
    this.month = (tempDate.getMonth() + 1).toString();
    this.year = tempDate.getFullYear().toString();
    if (this.dates.length > 1) {
      var endDate = new Date(this.dates[1]);
      this.endday = endDate.getDate().toString();
      this.endmonth = (endDate.getMonth() + 1).toString();
      this.endyear = endDate.getFullYear().toString();

      if (parseInt(this.year) > parseInt(this.endyear)) {
        this.endday = ""
        this.endmonth = "";
        this.endyear = "";
      }
    }
    this.getData();
    this.setTabsData();
  }

  setTabsData() {
    this.allDigital = 0;
    this.dfwEnglishTv = 0;
    this.dfwSpanishTv = 0;
    this.radioEnglish = 0;
    this.radioSpanish = 0;
    this.tvEnglish = 0;
    this.tvSpanish = 0;
    this.quote = 0;
    this.sold = 0;
    this.callcenterCount = 0;
    this.filterdata.allDigital.forEach(x => {
      this.allDigital = this.allDigital + (isNaN(x.count) ? 0 : x.count);
      if (x.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(x.count) ? 0 : x.count);
      }
    });
    this.filterdata.dfwEnglishTv.forEach(m => {
      this.dfwEnglishTv = this.dfwEnglishTv + (isNaN(m.count) ? 0 : m.count);
      if (m.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(m.count) ? 0 : m.count);
      }
    });
    this.filterdata.dfwSpanishTv.forEach(a => {
      this.dfwSpanishTv = this.dfwSpanishTv + (isNaN(a.count) ? 0 : a.count);
      if (a.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(a.count) ? 0 : a.count);
      }
    });
    this.filterdata.radioEnglishRgv.forEach(b => {
      this.radioEnglish = this.radioEnglish + (isNaN(b.count) ? 0 : b.count);
      if (b.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(b.count) ? 0 : b.count);
      }
    });
    this.filterdata.radioSpanisRrgv.forEach(c => {
      this.radioSpanish = this.radioSpanish + (isNaN(c.count) ? 0 : c.count);
      if (c.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(c.count) ? 0 : c.count);
      }
    });
    this.filterdata.tvEnglishHouston.forEach(e => {
      this.tvEnglish = this.tvEnglish + (isNaN(e.count) ? 0 : e.count);
      if (e.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(e.count) ? 0 : e.count);
      }
    });
    this.filterdata.tvSpanishHouston.forEach(f => {
      this.tvSpanish = this.tvSpanish + (isNaN(f.count) ? 0 : f.count);
      if (f.agencyName === "Call Center") {
        this.callcenterCount = this.callcenterCount + (isNaN(f.count) ? 0 : f.count);
      }
    });
  }
}
