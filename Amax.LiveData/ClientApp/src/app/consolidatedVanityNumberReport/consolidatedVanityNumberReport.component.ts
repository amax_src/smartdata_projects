import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/CommonService';
import { authenticationService } from '../login/authenticationService.service';
import { encryption } from '../../services/Encryption';
import { ConsolidatedVanityNumberReportService } from '../../services/ConsolidatedVanityNumberReportService ';
import { VanityNumberReport } from '../../Model/VanityNumberReport';

@Component({
  selector: 'app-consolidatedvanity',
  templateUrl: './consolidatedVanityNumberReport.component.html',
})
export class ConsolidatedVanityNumberReportComponent implements OnInit {
  showLoader: boolean = false;
  allData: Array<VanityNumberReport> = new Array<VanityNumberReport>();
  FilterData: Array<VanityNumberReport> = new Array<VanityNumberReport>();
  userIdencrypted: string = "";
  userRoleencrypted: string = "";
  userId: string = "";
  userRole: string = "";
  allDigital: string = "0";
  dfwEnglishTv: string = "0";
  dfwSpanishTv: string = "0";
  radioEnglish: string = "0";
  radioSpanish: string = "0";
  tvEnglish: string = "0";
  tvSpanish: string = "0";
  dates: Array<Date>;
  day: string = "";
  month: string = "";
  year: string = "";
  endday: string = "";
  endmonth: string = "";
  endyear: string = "";
  constructor(private vanityService: ConsolidatedVanityNumberReportService, private router: Router, private cmnSrv: CommonService, private _auth: authenticationService, private encryp: encryption,
    private route: ActivatedRoute) {

  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((data) => {
      this.userIdencrypted = data['userId'];
      this.userRoleencrypted = data['UserRole'];
      this.userId = this.encryp.encrytptvalue(this.userIdencrypted, false);
      this.userRole = this.encryp.encrytptvalue(this.userRoleencrypted, false);
    });
    //Need to uncomment after
    //if (!this.userId) {
    //  this.userId = this._auth.userId.toString();
    //}
    //if (!this.userRole) {
    //  this.userRole = this._auth.userRole;
    //}
  }

  getData() {
    this.allData = new Array<VanityNumberReport>();
    this.vanityService.getData(this.day, this.month, this.year, this.endday, this.endmonth, this.endyear, this.userId, this.userRole).subscribe(m => {
      this.allData = m;
      
      this.setTabsData();

    },
      error => {
       
        this.router.navigateByUrl("/");
      })
  }

  onDateSelect(e) {
    var tempDate = new Date(this.dates[0]);
    this.day = tempDate.getDate().toString();
    this.month = (tempDate.getMonth() + 1).toString();
    this.year = tempDate.getFullYear().toString();
    if (this.dates.length > 1) {
      var endDate = new Date(this.dates[1]);
      this.endday = endDate.getDate().toString();
      this.endmonth = (endDate.getMonth() + 1).toString();
      this.endyear = endDate.getFullYear().toString();

      if (parseInt(this.year) > parseInt(this.endyear)) {
        this.endday = ""
        this.endmonth = "";
        this.endyear = "";
      }
      this.getData();
    }
    
  }

  setTabsData() {
    this.allDigital = '0';
    this.dfwEnglishTv = '0';
    this.dfwSpanishTv = '0';
    this.radioEnglish = '0';
    this.radioSpanish = '0';
    this.tvEnglish = '0';
    this.tvSpanish = '0';
    this.FilterData = new Array<VanityNumberReport>();
    this.allData.forEach(m => {
      switch (m.did) {
        case '18006000038':
          this.allDigital = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18009000068':
          this.dfwEnglishTv = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18009000078':
          this.dfwSpanishTv = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18005000027':
          this.radioEnglish = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18005000042':
          this.radioSpanish = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18003000075':
          this.tvEnglish = m.callsoffered;
          this.FilterData.push(m);
          break;
        case '18003000078':
          this.tvSpanish = m.callsoffered;
          this.FilterData.push(m);
          break;
      }
    })
  }
}
