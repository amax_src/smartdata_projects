import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from '../shared/sidebar/sidebar.module';
import { ConsolidatedVanityNumberReportComponent } from './consolidatedVanityNumberReport.component';
import { ConsolidatedVanityNumberReportRoutingModule } from './consolidatedVanityNumberReport-routing.module';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  imports: [
    MultiSelectModule,
    CommonModule,
    FormsModule,
    ConsolidatedVanityNumberReportRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SidebarModule,
    CalendarModule,
    DropdownModule
  ],
  declarations: [ConsolidatedVanityNumberReportComponent],
  exports: []
 })
export class ConsolidatedVanityNumberReportModule { }
