import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ConsolidatedVanityNumberReportComponent } from "./consolidatedVanityNumberReport.component";

const routes: Routes = [
  { path: 'ConsolidatedVanityNumberReport', component: ConsolidatedVanityNumberReportComponent}//, 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
}) 
export class ConsolidatedVanityNumberReportRoutingModule { }
