import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { authenticationService } from './authenticationService.service';
import { encryption } from '../../services/Encryption';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Chatdata: any;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string = '';
  loginSectionDisplay: string = "none";
  homeSectionDisplay: string = "block"
  logintype: string = '';
  showLoader: boolean = false;

  isValidEmail: boolean = true;
  message: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: authenticationService, private encryp: encryption
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }




  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.showLoader = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value, this.logintype)
      .pipe(first())
      .subscribe(
      data => {
        this.showLoader = false;
          if (this.authenticationService.userRole === "zonalmanager"
            || this.authenticationService.userRole === "regionalmanager"
            || this.authenticationService.userRole === "storemanager"
            || this.authenticationService.userRole === "headofdepratment"
            || this.authenticationService.userRole === "chieffinancialofficer"
            || this.authenticationService.userRole === "chiefexecutiveofficer"
            || this.authenticationService.userRole === "chiefOperatingofficer"
              || this.authenticationService.userRole === "saledirector") {
             
            this.router.navigateByUrl("Home?userId=" + this.encryp.encrytptvalue(this.authenticationService.userId, true) + "&UserRole=" + this.encryp.encrytptvalue(this.authenticationService.userRole, true));
          }
          else if (this.authenticationService.userRole === "marketing")
          {
            this.router.navigateByUrl("ConsolidatedVanityNumberReport?userId=" + this.encryp.encrytptvalue(this.authenticationService.userId, true) + "&UserRole=" + this.encryp.encrytptvalue(this.authenticationService.userRole, true));
          }
          else {
            this.router.navigateByUrl("/");
          }
        },
        error => {
          try {
            this.error = error.error.message;
          }
          catch{

          }
          this.showLoader = false;
        });
  }


  showLoginSection(value) {
    this.loginSectionDisplay = "block";
    this.homeSectionDisplay = "none";
    this.logintype = value;
  }

  showHomeSection() {
    this.loginSectionDisplay = "none";
    this.homeSectionDisplay = "block"
    this.logintype = '';
    this.submitted = false;
  }

}
