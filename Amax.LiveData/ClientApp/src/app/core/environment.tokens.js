"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("../../environments/environment");
var provideValue = function (value, provideIn) {
    if (provideIn === void 0) { provideIn = "root"; }
    return {
        provideIn: provideIn,
        factory: function () { return value; },
    };
};
// Injection Tokens
exports.BASE_API_URL = new core_1.InjectionToken("Base url for the Web.API based on the environment.", provideValue(environment_1.environment.baseApiUrl));
//# sourceMappingURL=environment.tokens.js.map