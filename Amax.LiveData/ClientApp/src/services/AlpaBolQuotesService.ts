import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { BASE_API_URL } from '../app/core/environment.tokens';
import { authenticationService } from '../app/login/authenticationService.service';
import { concat } from 'rxjs';

//import { BASE_API_URL } from '../../core/environment.tokens';


@Injectable({
  providedIn: 'root'
})
export class AlpaBolQuotesService {
  _baseUrl: string = "http://18.216.112.189:8085/Export/";
  //_baseUrl: string = "https://portal.amaxinsurance.com/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  GetAlpaCarrierNames() {
    let headers = this.header;
      return this.http.get<any>(`${this.baseApiUrl}/AlpaBolQuotes/AlpaBolCarrierName`, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAlpaDataBolData_Quotes(carriers: Array<string>, date: Array<Date>, state: Array<string>) {
    let headers = this.header;
      return this.http.post<any>(`${this.baseApiUrl}/AlpaBolQuotes/GetAlpaDataBolData_Quotes`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

}
