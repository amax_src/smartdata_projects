import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { BASE_API_URL } from '../app/core/environment.tokens';
import { authenticationService } from '../app/login/authenticationService.service';
import { concat } from 'rxjs';

//import { BASE_API_URL } from '../../core/environment.tokens';


@Injectable({
  providedIn: 'root'
})
export class LiveDataService {
  //_baseUrl: string = "http://18.216.112.189:8082/Export/";
  _baseUrl: string = "https://portal.amaxinsurance.com/Export/";
  //_baseUrl: string = "http://localhost:44305/Export/";
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }


  getData(userId: string, userRole: string) {

    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata?login=` + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getRefeshTime() {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetRefreshTime`, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getSpectrumData(userId: string, userRole: string) {

    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveData?login=` + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
    }


    //getSpectrumDataNew(userId: string, userRole: string) {
    //    let headers = this.header;
    //    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveDataV1?login=` + userId + '-' + userRole, { headers })
    //        .pipe(map(x => {
    //            return x;
    //        }));
    //}

    //GetStatsLiveDataAgentWise(userId: string, userRole: string) {
    //    let headers = this.header;
    //    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetStatsLiveDataAgentWise?login=` + userId + '-' + userRole, { headers })
    //        .pipe(map(x => {
    //            return x;
    //        }));
    //}

    //GetStatsWeeklyDataAgentWise(userId: string, userRole: string, weeklyId) {
    //    let headers = this.header;
    //    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetStatsWeeklyDataAgentWise?login=` + userId + '-' + userRole + '&weeklyId=' + weeklyId, { headers })
    //        .pipe(map(x => {
    //            return x;
    //        }));
    //}

  getSpectrumRefeshTime() {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/SpectrumLiveDataRefreshTime`, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAmaxConsolidated(userId: string, userRole: string) {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetAmaxLiveApp?login=` + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetAmaxLiveAppAgencyWise(userId: string, userRole: string) {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetAmaxLiveAppAgencyWise?login=` + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetReasonForWalkOutCustomer(agency: string) {
    let headers = this.header;
    return this.http.post<any>('http://18.222.69.131:8080/api/locationNoteForCreatedQuote', { agencyName: agency })
      .pipe(map(x => {
        return x;
      }));
  }

  BindOnLineQuotes(carriers: Array<string>, date: Array<Date>,state:Array<string>) {
    let headers = this.header;
    return this.http.post<any>(`${this.baseApiUrl}/Livedata/BolQuotes`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetCarrierNames() {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/BolCarrierName`, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetDataBolData_Quotes(carriers: Array<string>, date: Array<Date>, state: Array<string>) {
    let headers = this.header;
    return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetDataBolData_Quotes`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetEODLiveReport(date: Array<Date>) {
    let carriers: Array<string>;
    let state: Array<string>;
    let headers = this.header;
    return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetEODLiveReport`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getAgency(userId: string, userRole: string) {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/getAgency?login=` + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  ExportHourlyData(HourlyData: any) {
    const url = `${this.baseApiUrl}/Livedata/ExportHourlyData/`;
    let headers = this.header;
    return this.http.post<any>(url, HourlyData, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }
  ExportHourlyDataAgencyWise(HourlyData: any) {
    const url = `${this.baseApiUrl}/Livedata/ExportHourlyDataAgencyWise/`;
    let headers = this.header;
    return this.http.post<any>(url, HourlyData, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }

  GetCustomerJourneyInfoReport(date: Array<Date>) {
    let carriers: Array<string>;
    let state: Array<string>;
    let headers = this.header;
    return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetCustomerJourneyInfoReport`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        return x;
      }));
  }


  ExportCustomerJourneyInfoReport(date: Array<Date>) {
    let carriers: Array<string>;
    let state: Array<string>;
    let headers = this.header;
    return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportCustomerJourneyInfoReport`, { carriers, date, state }, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
    }

    ExportOnlineQuote(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportOnlineQuote`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }

    //**************************************** STRATUS CODE******************************

    getSpectrumDataNewV2(userId: string, userRole: string) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveDataV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    GetSpectrumAgentCallDetails(userId: string, userRole: string, extension) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumAgentCallDetails?login=` + userId + '-' + userRole + '&extension=' + extension, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetStatsLiveDataAgentWiseV2(userId: string, userRole: string) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetStatsLiveDataAgentWiseV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    GetStatsWeeklyDataAgentWiseV2(userId: string, userRole: string, date: Array<Date>) {
        let headers = this.header;
        var _login = userId + '-' + userRole;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetStatsWeeklyDataAgentWiseV2`, { _login, date }, { headers })
            .pipe(map(x => {
                return x;
            }));

    }
    GetSpectrumAgencyAgentLiveLogV2(userId: string, userRole: string) {
        let headers = this.header;
        var _login = userId + '-' + userRole;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveAgentLogV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));

    }
    GetSpectrumLiveAgentExtensionWiseCallLogV2(extension: string, date: string) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetSpectrumExtensionWiseCallDetailsLogV2`, { extension, date }, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetSpectrumAgentLogCallDetails(userId: string, userRole: string, extension) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveAgentExtensionLogV2?login=` + userId + '-' + userRole + '&extension=' + extension, { headers })
            .pipe(map(x => {
                return x;
            }));

    }
    getAllRegionalManagers() {
        const url = `${this.baseApiUrl}/RegionalManager`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAllSaleDirectors() {
        const url = `${this.baseApiUrl}/SaleDirector`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getRegionalManagerBySaleDirector(id: Array<number>) {
        const url = `${this.baseApiUrl}/RegionalManager/ByMultipleSaleDirector`;
        let headers = this.header;
        return this.http.post<any>(url, id, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getZonalManagersByRegionalManager(id: Array<number>) {
        const url = `${this.baseApiUrl}/ZonalManager/ByMultipleRegionalManager`;
        let headers = this.header;
        return this.http.post<any>(url, id, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAllZonalManagers() {
        const url = `${this.baseApiUrl}/ZonalManager`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    getAllLocationsRegionalZonalManager(rid: Array<number>, zid: Array<number>,) {
        console.log("rid", rid)
        console.log("Zmid", zid)
        const url = `${this.baseApiUrl}/Agency/GetByRegionalAndZonalManager`;
        let headers = this.header;
        return this.http.post<any>(url, { "Rmid": rid, "Zmid": zid }, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    getAllLocations() {
        const url = `${this.baseApiUrl}/Agency`;
        let headers = this.header;
        return this.http.get<any>(url, { headers })
            .pipe(map(x => {
                return x;
            }));
    }
    getStoreManagerAgency(id: number) {
        const url = `${this.baseApiUrl}/Agency/GetStoreManagerAgency`;
        let headers = this.header;
        return this.http.post<any>(url, id, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    ExportSpectrumCallLocationWise(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportSpectrumCallLocationWise`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }
    ExportSpectrumCallExtensionWise(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportSpectrumCallExtensionWise`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }

    ExportLiveStratusAgentLog(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportLiveStratusAgentLog`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }

    ExportStratusWeeklyDataAgentWise(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportStratusWeeklyDataAgentWise`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }
    ExportStratusDataAgentExtensionWise(list: Array<any>) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/ExportStratusDataAgentExtensionWise`, list, { headers })
            .pipe(map(x => {
                var filename = this._baseUrl + x;
                return filename;
            }));
    }
    GetCallTransferDetails(userId: string, userRole: string, extension: string, agencyId: any) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetCallTransferDetails?login=` + userId + '-' + userRole + '&extension=' + extension + '&agencyId=' + agencyId, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetStatsLiveDataAgentWiseVSTeamV2(userId: string, userRole: string) {
        let headers = this.header;
        var _login = userId + '-' + userRole;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetStatsLiveDataAgentWiseVSTeamV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    //************************************CA Spectrum ***********************************
    GetCASpectrumLiveDataV2(userId: string, userRole: string) {

        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetCASpectrumLiveDataV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetCallTransferDetailsCA(userId: string, userRole: string, extension: string, agencyId: any) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetCallTransferDetailsCA?login=` + userId + '-' + userRole + '&extension=' + extension + '&agencyId=' + agencyId, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetStatsLiveDataAgentWiseCAV2(userId: string, userRole: string) {
        let headers = this.header;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetStatsLiveDataAgentWiseCAV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));
    }

    GetSpectrumAgencyAgentLiveLogCAV2(userId: string, userRole: string) {
        let headers = this.header;
        var _login = userId + '-' + userRole;
        return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSpectrumLiveAgentLogCAV2?login=` + userId + '-' + userRole, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetStatsWeeklyDataAgentWiseCAV2(userId: string, userRole: string, date: Array<Date>) {
        let headers = this.header;
        var _login = userId + '-' + userRole;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetStatsWeeklyDataAgentWiseCAV2`, { _login, date }, { headers })
            .pipe(map(x => {
                return x;
            }));

    }

    GetSpectrumLiveAgentExtensionWiseCallLogCAV2(extension: string, date: string) {
        let headers = this.header;
        return this.http.post<any>(`${this.baseApiUrl}/Livedata/GetSpectrumExtensionWiseCallDetailsLogCAV2`, { extension, date }, { headers })
            .pipe(map(x => {
                return x;
            }));

    }
}
