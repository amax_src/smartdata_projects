import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { BASE_API_URL } from '../app/core/environment.tokens';
import { authenticationService } from '../app/login/authenticationService.service';
//import { BASE_API_URL } from '../../core/environment.tokens';


@Injectable({
  providedIn: 'root'
})
export class ConsolidatedVanityNumberReportService {
  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient, private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  getData(sday: string, smonth: string, syear: string, eday: string, emonth: string, eyear: string, userId: string, userRole: string) {
    let headers = this.header;
    return this.http.get<any>(`${this.baseApiUrl}/Livedata/GetSummaryDIDData?sday=` + sday + '&smonth=' + smonth + '&syear=' + syear + '&eday=' + eday + '&emonth=' + emonth + '&eyear=' + eyear + '&login=' + userId + '-' + userRole, { headers })
      .pipe(map(x => {
        return x;
      }));
  }
}
