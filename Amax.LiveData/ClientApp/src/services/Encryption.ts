import base64 from 'base-64';
import utf8 from 'utf8';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class encryption {

  encrytptvalue(value: any, isEncrypt: boolean = true) {

    let response: any;
    if (value != null && value != '') {

      let bytes: any;
      if (isEncrypt) {
        bytes = utf8.encode(value.toString());
        response = base64.encode(bytes);
      }
      else {
        bytes = base64.decode(value.toString());
        response = utf8.decode(bytes);

      }
    }
    return response;
  }

}
