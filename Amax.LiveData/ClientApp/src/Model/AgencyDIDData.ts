export class AgencyDIDData {
  public count: number = 0;
  public agencyName: string = "";
  public sold: number = 0;
  public quote: number = 0;
}
