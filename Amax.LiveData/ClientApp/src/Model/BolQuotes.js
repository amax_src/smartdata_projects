"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BolData = /** @class */ (function () {
    function BolData() {
        this.date = '';
        this.quoteUrl = '';
        this.carrierName = '';
        this.callCenterSent = false;
        this.buyNowOption = false;
        this.buyNowClick = false;
        this.time = '';
        this.policyNumber = '';
        this.name = '';
        this.surname = '';
        this.email = '';
        this.phoneNumber = '';
        this.isdelayed = false;
        this.isQuoteDataMissing = false;
        //customer: Customer = new Customer();
    }
    return BolData;
}());
exports.BolData = BolData;
//# sourceMappingURL=BolQuotes.js.map