export class AmaxHourlyLoad {
  public agentCount: number= 0;
  public modifiedQuotes: number = 0;
  public newQuotes: number = 0;
  public policies: number = 0;
  public agencyfee: number = 0;
  public premium: number =0;
  public transactions: number = 0;
  public agencyid: number =0;
  public hourIntervel: string = '';
  public location: string = '';
}
