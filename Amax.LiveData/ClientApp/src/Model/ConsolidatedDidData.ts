import { AgencyDIDData } from "./AgencyDIDData";

export class ConsolidatedDidData {
  public tvEnglishHouston: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public tvSpanishHouston: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public radioEnglishRgv: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public radioSpanisRrgv: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public allDigital: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public dfwEnglishTv: Array<AgencyDIDData> = new Array<AgencyDIDData>();
  public dfwSpanishTv: Array<AgencyDIDData> = new Array<AgencyDIDData>();
}
