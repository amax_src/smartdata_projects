export class user {
  userID: number;
  groupName: string;
  userName: string;
  token: string;
  loginName: string;
}
