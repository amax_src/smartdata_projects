export class BolData {
  date: string = '';
  quoteUrl: string = '';
  carrierName: string = '';
  callCenterSent: boolean = false;
  buyNowOption: boolean = false;
  buyNowClick: boolean = false;
  time: string = '';
  policyNumber: string = '';
  name: string = '';
  surname: string = '';
  email: string = '';
  phoneNumber: string = '';
  agencyFee: number;
  state: string;
  isdelayed: boolean = false;
  isQuoteDataMissing: boolean = false
  premiumAmount: number = 0;
  collectedPremium: number = 0;
  //customer: Customer = new Customer();
}

