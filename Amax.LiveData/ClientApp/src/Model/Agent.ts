export class Agent {
  name: string = '';
  policyCount: number = 0;
  agencyFee: number = 0;
  premium: number = 0;
  trans: number = 0;
  calls: number = 0;
}
