export class EODLiveReport {
  verifiedTime: string = "";
  verifiedDate: Date;
  store: string = "";
  totalSales: string = "";
  totalCollections: string = "";
  shortOver: string = "";
  date: Date;
  notes: string = "";
  varifiedBy: string = "";
  status: string = "";
  agent: string = "";
  sysUTCCreatedDate :Date;
  agencyId: number = 0;
}
export class CustomerJourneyInfo {
  bindOnlineCarrierQuoteId: number = 0;
  name: string = '';
  surname: string = '';
  birthDate: Date;
  driverLicenseNumber: string = '';
  email: string = '';
  phoneNumber: string = '';
  address1: string = '';
  city: string = '';
  state: string = '';
  zipCode: string = '';
  customerId: string = '';
  date: Date;
  quoteUrl: string = '';
  quoteNumber: string = '';
  carrierName: string = '';
  callCenterSent: boolean = false;
  buyNowOption: boolean = false;
  buyNowClick: boolean = false;
  payPlanDescription: string = '';
  payPlanNumOfPayments: number = 0;
  payPlanPercentDown: number = 0;
  payPlanDownPayment: number = 0;
  payPlanPaymentAmount: number = 0;
  payPlanServiceFees: number = 0;
  payPlanPaymentTotal: number = 0;
  term: number = 0;
  totalPremium: number = 0;
  carDetails: string = '';
  carCoverageDetails: string = '';
  policyNumber: string = '';
  isSold: boolean = false;
}
