import { Company } from "./Company";
import { Agent } from "./Agent";
import { AMPMDeposit } from "./AMPMDeposit";
import { paymentMethod } from "./paymentMethod";

export class LiveDataAgency {
  agencyName: string;
  long: number;
  lat: number;
  premium: number;
  policyCount: number;
  agencyFee: number;
  modifiedQuotes: number;
  newQuotes: number;
  agent: Array<Agent> = new Array<Agent>();
  endorsementCount: number = 0;
  cashLogTotal: number = 0;
  date: string = '';
  premiumGoal: number = 0;
  policyGoal: number = 0;
  agencyFeeGoal: number = 0;
  mtdPremium: number = 0;
  mtdPolicy: number = 0;
  mtdAgencyFee: number = 0;
  totalWorkingdays: number = 0;
  company: Array<Company> = new Array<Company>();
  agentTotalPolicy: number = 0;
  agentTotalPremium: number = 0;
  agentTotalAgencyFee: number = 0;
  agentTotalTrans: number = 0;
  creditCardLog: number = 0;
  walkIn: number = 0;
  phoneInternet: number = 0;
  walkInSold: number = 0;
  phoneInternetSold: number = 0;
  remainingDays: number = 0;
  agentTotalcall: number = 0;
  ampmDeposit: AMPMDeposit = new AMPMDeposit();
  paymentMethod: paymentMethod = new paymentMethod();
  todayPolicyCount: number = 0;
  todayAgencyFee: number = 0;
  todayPremium: number = 0;
}



