export class SpectrumLiveData {
    public inBoundCalls: number = 0;
    public outboundCalls: number = 0;
    public totalInBoundsCalls: number = 0;
    public totalOutBoundsCall: number = 0;
    public inboundDuration: number = 0;
    public outBoundDuration: number = 0;
    public inBoundHours: string = '';
    public outBoundHours: string = '';
    public missedCalls: number = 0;
    public agencyid: number = 0;
    public location: string = '';
    public date: string = '';
    public extension: string = '';
    public callTransfer: number = 0;
    public transferCallDuration: number = 0;
}

export class SpectrumLiveAgentCallDetails {
    public type: number = 0;
    public time_start: Date;
    public time_answer: Date;
    public time_release: Date;
    public duration_human: string = '';
    public number: string = '';
    public name: string = '';
    public duration: number = 0;
}
export class SaleDirector {
    saleDirectorID: number;
    saleDirector: string;
    employeeId: string;
}
export class RegionalManagers {
    public regionalManagerId: number = 0;
    public regionalManager: string = '';
    public employeeId: number = 0;
}

export class ZonalManager {
    zonalManagerId: number;
    zonalManager: string;
    employeeId: number;
}

export class Agency {
    agencyId: number = 0;
    agencyName: string = '';
    isActive: boolean = false;
    regionalZonalID: number = 0;
}
export class AgencyAgentLog {

    agencyId: number = 0;
    agencyName: string = "";
    locationAddress: string = "";
    extension: string = "";
    meeting: number = 0;
    manual_avail: number = 0;
    other: number = 0;
    logins: number = 0;
    web: number = 0;
    breaks: number = 0;
    lunch: number = 0;
    manualsec: number = 0;
    logoutsec: number = 0;
    autosec: number = 0;
    total: number = 0;
    meetingHours: string = "";
    manualAvailHours: string = "";
    otherHours: string = "";
    loginHours: string = "";
    webHours: string = "";
    breaksHours: string = "";
    lunchHours: string = "";
    manualHours: string = "";
    logoutHours: string = "";
    autoHours: string = "";
    totalHours: string = "";

}
export class SpectrumAgentExtensionLog {
    timestamp: Date;
    action: string = "";
    loggedin_sec: number = 0;
    available_sec: number = 0;
    unavailable_sec: number = 0;
    break_sec: number = 0;
    lunch_sec: number = 0;
    meeting_sec: number = 0;
    other_sec: number = 0;
    acw_sec: number = 0;
    web_sec: number = 0;
}
export class AgentCallTransfer {
    AgencyId: number = 0;
    agencyName: string = "";
    extension: string = "";
    callType: string = "";
    time_start: Date;
    time_answer: Date;
    time_release: Date;
    duration: string = "";
    duration_human: string = "";
    orig_to_uri: string = "";
    callTransferExtension: string = "";
    callTransferLocation: string = "";
}
