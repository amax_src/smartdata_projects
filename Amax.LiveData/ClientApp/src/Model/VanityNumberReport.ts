export class VanityNumberReport {
  did: string = '';
  callsoffered: string = '';
  callsanswered: string = '';
  percentageAns: string = '';
  duration: string ='';
}
