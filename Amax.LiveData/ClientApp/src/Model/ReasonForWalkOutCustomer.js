"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReasonForWalkOutCustomer = /** @class */ (function () {
    function ReasonForWalkOutCustomer() {
        this.customer_last_name = '';
        this.agent_name = '';
        this.note = '';
        this.agent_Location = '';
    }
    return ReasonForWalkOutCustomer;
}());
exports.ReasonForWalkOutCustomer = ReasonForWalkOutCustomer;
//# sourceMappingURL=ReasonForWalkOutCustomer.js.map