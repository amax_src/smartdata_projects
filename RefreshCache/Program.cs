﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.IO;
using System.Net.Http;

namespace RefreshCache
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        public static IConfigurationRoot Configuration;
        public static readonly TimeSpan InfiniteTimeSpan;
        static  void Main(string[] args)
        {
            var services = new ServiceCollection();

            // Set up configuration sources.
            var builder = new ConfigurationBuilder()

                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", optional: true);

            Configuration = builder.Build();

            Serilog.Log.Logger = new LoggerConfiguration().WriteTo.File("Logs\\CacheRefresh_"+DateTime.Now.ToShortDateString().Replace("/","-")).CreateLogger();
            if (args.Length > 0 && args[0].ToLower()=="liveapp")
            {
                Log.Information("LiveData app for cache Reset Started");
                var url = Configuration.GetSection("liveAppUrl").Value;
                ProcessRepositories(url);
                Log.Information("LiveData app cache Reset Successfull");
            }
            else
            {
                Log.Information("Application for cache Reset Started");
                var url = Configuration.GetSection("url").Value;
                ProcessRepositories(url);
                Log.Information("Application for cache Reset Successfull");
            }

           
        }

        private static void ProcessRepositories(string url)
        {
            Log.Information("Refresh URL:-"+ url);
            try
            {
                client.Timeout = TimeSpan.FromMinutes(30); 
                var stringTask = client.GetStringAsync(url).Result;
                var msg = stringTask;
                Log.Information("Response:-" + msg);
                Console.WriteLine(msg);
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
