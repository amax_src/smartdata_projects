﻿using AmaxIns.DataContract.AplhaCustomerInfo;
using AmaxIns.RepositoryContract.AlphaCustomerInfo;
using AmaxIns.ServiceContract.AlphaCustomerInfo;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.AlphaCustomerInfo
{
    public class CustomerInfoService : BaseService, ICustomerInfoService
    {
        ICustomerInfoRepository _repository;
        public CustomerInfoService(ICustomerInfoRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public async Task<IEnumerable<string>> GetAgencyName()
        {
            return await _repository.GetAgencyName();
        }

        public async Task<IEnumerable<string>> GetclientName(string SearchText)
        {
            return await _repository.GetclientName(SearchText);
        }


        public async Task<IEnumerable<string>> GetCity()
        {
            return await _repository.GetCity();
        }


        public async Task<IEnumerable<string>> GetPolicyType()
        {
            return await _repository.GetPolicyType();
        }

        public async Task<IEnumerable<string>> GetPolicyNumber(string SearchText)
        {
            return await _repository.GetPolicyNumber(SearchText);
        }

        public async Task<IEnumerable<CustomerInfoModel>> CustomerInfoData()
        {
            return await _repository.CustomerInfoData();
        }
    }
}
