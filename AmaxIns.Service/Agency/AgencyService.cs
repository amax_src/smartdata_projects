﻿using AmaxIns.DataContract.Agency;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.ServiceContract.Agency;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Service.Agency
{
    public class AgencyService : BaseService, IAgencyService
    {
        private readonly IAgencyRepository _agencyRepository;

        public AgencyService(IAgencyRepository agencyRepository, IConfiguration configuration)
           : base(configuration)
        {
            _agencyRepository = agencyRepository;
        }

        public async Task<IEnumerable<AgencyModel>> Get()
        {
            var result = await _agencyRepository.Get();
            result = result.Where(m => m.AgencyId != 1 ).ToList();
            result = result.Where(m => m.AgencyId != 99).OrderBy(x => x.AgencyName).ToList();
            return result;
        }
        

             public async Task<IEnumerable<AgencyModel>> GetSaleOfDirector(List<int> SaleDirectorId)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            foreach (var x in SaleDirectorId)
            {
                var result = await this._agencyRepository.GetSaleOfDirector(x);
                foreach (var z in result)
                {
                    if (z.AgencyId != 1 || z.AgencyId != 99)
                    {
                        _agencies.Add(new AgencyModel { AgencyId = z.AgencyId, AgencyName = z.AgencyName, IsActive = z.IsActive });
                    }
                }
            }
            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;


        }
        public async Task<IEnumerable<AgencyModel>> Get(List<int> RegionalManagerIds)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            foreach (var x in RegionalManagerIds)
            {
                var result = await this._agencyRepository.Get(x);
                foreach (var z in result)
                {
                    if (z.AgencyId != 1 || z.AgencyId != 99)
                    {
                        _agencies.Add(new AgencyModel { AgencyId = z.AgencyId, AgencyName = z.AgencyName,IsActive=z.IsActive });
                    }
                }
            }
            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;


        }

        public async Task<IEnumerable<AgencyModel>> Get(List<int> RegionalManagerId, List<int> ZonalManagerId)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            if (RegionalManagerId != null && RegionalManagerId.Count > 0)
            {
                foreach (var x in RegionalManagerId)
                {
                    foreach (var z in ZonalManagerId)
                    {
                        var result = await _agencyRepository.Get(x, z); ;
                        foreach (var item in result)
                        {
                            if (item.AgencyId != 1 || item.AgencyId != 99)
                            {
                                if (!_agencies.Any(m => m.AgencyId == item.AgencyId))
                                {
                                    _agencies.Add(new AgencyModel { AgencyId = item.AgencyId, AgencyName = item.AgencyName, IsActive = item.IsActive });
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var z in ZonalManagerId)
                {
                    var result = await _agencyRepository.GetbyZonalManager(z); ;
                    foreach (var item in result)
                    {
                        if (item.AgencyId != 1 || item.AgencyId != 99)
                        {
                            if (!_agencies.Any(m => m.AgencyId == item.AgencyId))
                            {
                                _agencies.Add(new AgencyModel { AgencyId = item.AgencyId, AgencyName = item.AgencyName, IsActive = item.IsActive });
                            }
                        }
                    }
                }
            }

            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;

        }

        public async Task<IEnumerable<AgencyModel>> GetBSMAgency(List<int> BsmId)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            foreach (var x in BsmId)
            {
                var result = await this._agencyRepository.GetBSMAgency(x);
                foreach (var z in result)
                {
                    if (z.AgencyId != 1 || z.AgencyId != 99)
                    {
                        _agencies.Add(new AgencyModel { AgencyId = z.AgencyId, AgencyName = z.AgencyName, IsActive = z.IsActive });
                    }
                }
            }
            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;
        }

        public async Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id)
        {
            var result = await this._agencyRepository.GetStoreManagerAgency(Id);
            result = result.OrderBy(x => x.AgencyName).ToList();
            return result;
        }
    }
}
