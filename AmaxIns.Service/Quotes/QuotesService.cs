﻿using AmaxIns.DataContract.Quotes;
using AmaxIns.RepositoryContract.Quotes;
using AmaxIns.ServiceContract.Quotes;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.IO;
using OfficeOpenXml;
using System.Drawing;

namespace AmaxIns.Service.Quotes
{
    public class QuotesService : BaseService, IQuotesService
    {
        private readonly IQuotesRepository _QuotesRepository;
        public QuotesService(IQuotesRepository QuotesRepository
            , IConfiguration configuration) : base(QuotesRepository, configuration)
        {
            _QuotesRepository = QuotesRepository;
        }

        public Task<IEnumerable<string>> GetPaydate(string year, string month)
        {
            return _QuotesRepository.GetPaydate(year, month);
        }

        //public async Task<IEnumerable<QuotesModel>> GetBoundQuotes(string paydate)
        //{
        //    return await _QuotesRepository.GetBoundQuotes(paydate);
        //}

        public IEnumerable<QuotesCount> GetBoundQuotes(string paydate)
        {
            var result = _QuotesRepository.GetBoundQuotes(paydate).Result;

            var data = result.Where(m => Convert.ToDateTime(m.I_paydate) > DateTime.MinValue);

            List<QuotesCount> quotes = new List<QuotesCount>();
            List<QuotesModel> quotemodel;
            var quotesCounts = from x in data group x by x.I_agencyName into quortGroup select new { AgencyName = quortGroup.Key, Count = quortGroup.Count() };
            foreach (var x in quotesCounts)
            {
                quotemodel = new List<QuotesModel>();
                quotemodel = data.Where(m => m.I_agencyName == x.AgencyName).ToList();
                quotes.Add(new QuotesCount { AgencyName = x.AgencyName, Count = x.Count, quotesModels = quotemodel });
            }
            return quotes;
            //return quotesCounts;
        }

        public IEnumerable<QuotesCount> GetBoundNoTurboratorQuotes(string paydate)
        {
            var result = _QuotesRepository.GetBoundNoTurboratorQuotes(paydate).Result;

            var data = result.Where(m => Convert.ToDateTime(m.I_paydate) > DateTime.MinValue);

            List<QuotesCount> quotes = new List<QuotesCount>();
            List<QuotesModel> quotemodel;
            var quotesCounts = from x in data group x by x.I_agencyName into quortGroup select new { AgencyName = quortGroup.Key, Count = quortGroup.Count() };
            foreach (var x in quotesCounts)
            {
                quotemodel = new List<QuotesModel>();
                quotemodel = data.Where(m => m.I_agencyName == x.AgencyName).ToList();
                quotes.Add(new QuotesCount { AgencyName = x.AgencyName, Count = x.Count, quotesModels = quotemodel });
            }
            return quotes;
            //return quotesCounts;
        }

        public IEnumerable<QuotesSale> GetQuotesSale()
        {
            var quoteResult = _QuotesRepository.GetQuotesSale().Result;
            var AmaxWebsiteQuotesSale = quoteResult.Where(m => m.AgencyName.Trim().ToLower() == "amax website").FirstOrDefault();
            foreach (var z in quoteResult)
            {
                if (z.AgencyName.Trim().ToLower() == "call center" && AmaxWebsiteQuotesSale!=null)
                {
                   
                        z.NewQuotes = z.NewQuotes + AmaxWebsiteQuotesSale.NewQuotes;
                        z.ModifiedQuotes = z.ModifiedQuotes + AmaxWebsiteQuotesSale.ModifiedQuotes;
                        z.NewQuotesSold = z.NewQuotesSold + AmaxWebsiteQuotesSale.NewQuotesSold;
                        z.ModifiedQuotesSold = z.ModifiedQuotesSold + AmaxWebsiteQuotesSale.ModifiedQuotesSold;
                }
            }
            var retundate = quoteResult.Where(m => m.AgencyName.ToLower().Trim() != "amax website").ToList();
            return retundate;
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModiFiedQuotes(List<string> Agencyids, string month = null,string year=null)
        {
            return await _QuotesRepository.GetNewModiFiedQuotes(Agencyids, month,year);
        }
        /// <summary>
        /// Created 19-Feb-2022
        /// </summary>
        /// <param name="quotesParam"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewAndModifiedData_New(QuotesParam quotesParam, string month = null, string year = null)
        {
            return await _QuotesRepository.GetNewAndModifiedData_New(quotesParam, month, year);
        }
        public async Task<int> ModifiedQuotesSoldTiles(QuotesParam quotesParam, string month = null, string year = null)
        {
            return await _QuotesRepository.ModifiedQuotesSoldTiles(quotesParam, month, year);
        }
        public async Task<IEnumerable<QuotesProjection>> GetQuotesProjection()
        {
            return await _QuotesRepository.GetQuotesProjection();
        }

        public async Task<IEnumerable<NewModifiedQuotes>> GetDistinctDateQuotes(string month = null, string year = null)
        {
            return await _QuotesRepository.GetDistinctDateQuotes(month, year);
        }
        public async Task<IEnumerable<NewAndModifiedQuotesProj>> GetNewModiFiedQuotesProjection(string month = null, string year = null)
        {
            return await _QuotesRepository.GetNewModiFiedQuotesProjection(month, year);
        }
        public string ExportNewModiFiedQuotes(IEnumerable<NewModifiedQuotes> list, string pathRoot, int reportType)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"New-And-Modified-Quotes-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 6].Merge = true;
                    workSheet.Cells[1, 1, 1, 6].Value = "* Modified Quotes *";
                    if (reportType == 0)
                    {
                        workSheet.Cells[1, 1, 1, 6].Value = "* New Quotes *";
                    }


                    workSheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "First Name";
                    workSheet.Cells[2, 2].Value = "Last Name";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Phone";
                    workSheet.Cells[2, 5].Value = "Policy Number";
                    workSheet.Cells[2, 6].Value = "Last Modified Date";
                    if (reportType == 0)
                    {
                        workSheet.Cells[2, 6].Value = "Created Date";
                    }


                    workSheet.Cells[2, 1, 2, 6].Style.Font.Bold = true;

                    int row = 2;
                    foreach (NewModifiedQuotes obj in list)
                    {
                        row++;

                        workSheet.Cells[row, 1].Value = obj.FirstName;
                        workSheet.Cells[row, 2].Value = obj.Lastname;
                        workSheet.Cells[row, 3].Value = obj.AgencyName;
                        workSheet.Cells[row, 4].Value = obj.Phone;
                        workSheet.Cells[row, 5].Value = obj.PolicyNumber;
                        workSheet.Cells[row, 6].Value = obj.TModifiedDate;
                        if (reportType == 0)
                        {
                            workSheet.Cells[row, 6].Value = obj.TCreateDate;
                        }
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
    }
}
