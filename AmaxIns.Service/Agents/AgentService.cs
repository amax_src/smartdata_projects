﻿using AmaxIns.RepositoryContract.Agents;
using AmaxIns.ServiceContract.Agents;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.Agents
{
    public class AgentService  : BaseService, IAgentService
    {
        IAgentRepository _Repository;
        public AgentService(IAgentRepository Repository,IConfiguration configuration) : base(configuration)
        {
            _Repository = Repository;
        }

        public Task<IEnumerable<string>> GetAgents()
        {
            return _Repository.GetAgents();
        }
    }
}
