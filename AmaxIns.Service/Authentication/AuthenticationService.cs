﻿using AmaxIns.Common;
using AmaxIns.Common.Interface;
using AmaxIns.DataContract.Authentication;
using AmaxIns.RepositoryContract.Authentication;
using AmaxIns.ServiceContract.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Authentication
{
    public class AuthenticationService : BaseService, IAuthenticationService
    {
        private readonly IAuthenticationRepository _authenticationRepository;
        IConfiguration configuration;
        IEmailService emailService;

        public AuthenticationService(IAuthenticationRepository authenticationRepository, IConfiguration _configuration, IEmailService _emailService)
           : base(_configuration)
        {
            _authenticationRepository = authenticationRepository;
            configuration = _configuration;
            emailService = _emailService;
        }

        private string  OTPMessage()
        {
            string _body = @"<table><tr>
 <td>Dear Customer,</td>
 </tr>
<tr><td>Your one time password for Live Report Login</td></tr>
<tr><td>OTP is below </td></tr>
<tr><td> <h2 style='background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;'>[OTP]</h2></td></tr>
<tr><td> Please use this passcode to complete your login.Do not share this passcode with any one.</td></tr>
    <tr><td> Thank you,</td></tr>
         <tr><td> AMAX Team </td></tr>
              </table> ";
            return _body;
        }
        public async Task<LoginUser> LogInAsyncUsingOTPRequest(LoginModel login)
        {
            LoginUser loginUser = null;
            loginUser = await _authenticationRepository.LogInAsyncUsingOTPRequest(login);
            if (loginUser != null)
            {
                loginUser.UserID = loginUser.ReferenceId.HasValue ? loginUser.ReferenceId.Value : -1;//-1 is setting for the HOD and Admin
                loginUser.isOTPSent = emailService.SendOTPViaEmail("Login OTP", OTPMessage().Replace("[OTP]", loginUser.otp), "chandanksharma077@gmail.com");
            }
            return loginUser;
        }

        public async Task<LoginUser> LogInAsyncValidateOTP(LoginModel login)
        {
            LoginUser loginUser = null;
            var ApiKey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;

            loginUser = await _authenticationRepository.LogInAsync(login);

            if (loginUser != null)
            {
                loginUser.UserID = loginUser.ReferenceId.HasValue ? loginUser.ReferenceId.Value : -1;//-1 is setting for the HOD and Admin

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ApiKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, loginUser.UserID.ToString()),
                        //new Claim(ClaimTypes.Role, loginUser.GroupName) 
                    }),
                    //if(login.Application=="Turborater")
                    Expires = login.Application == "Turborater" ? DateTime.UtcNow.AddDays(1) : DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                loginUser.Token = tokenHandler.WriteToken(token);
                if (login.Application != "LiveData")
                {
                    _authenticationRepository.LogUser(loginUser);
                }
            }

            return loginUser;
        }
        public async Task<LoginUser> LogInAsync(LoginModel login)
        {
            LoginUser loginUser = null;
            var ApiKey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;

            loginUser = await _authenticationRepository.LogInAsync(login);

            if (loginUser != null)
            {
                loginUser.UserID = loginUser.ReferenceId.HasValue ? loginUser.ReferenceId.Value : -1;//-1 is setting for the HOD and Admin

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ApiKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, loginUser.UserID.ToString()),
                        //new Claim(ClaimTypes.Role, loginUser.GroupName) 
                    }),
                    //if(login.Application=="Turborater")
                    Expires = login.Application == "Turborater"? DateTime.UtcNow.AddDays(1) : DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                loginUser.Token = tokenHandler.WriteToken(token);
                if (login.Application != "LiveData")
                {
                    _authenticationRepository.LogUser(loginUser);
                }
            }

            return loginUser;
        }
        public async Task<LoginUser> FDLogInAsync(LoginModel login)
        {
            LoginUser loginUser = new LoginUser();
            var ApiKey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;

            loginUser.UserID = 999;
            loginUser.GroupName = "FD";
            loginUser.ReferenceId = 2;
            loginUser.UserName = "Power BI FD";

            if (loginUser != null)
            {
                loginUser.UserID = loginUser.ReferenceId.HasValue ? loginUser.ReferenceId.Value : -1;//-1 is setting for the HOD and Admin

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ApiKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, loginUser.UserID.ToString()),
                        //new Claim(ClaimTypes.Role, loginUser.GroupName) 
                    }),
                    //if(login.Application=="Turborater")
                    Expires = login.Application == "Turborater" ? DateTime.UtcNow.AddDays(1) : DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                loginUser.Token = tokenHandler.WriteToken(token);
                if (login.Application != "LiveData")
                {
                    _authenticationRepository.LogUser(loginUser);
                }
            }

            return loginUser;
        }
        public async Task<bool> LogOutAsync(string userName)
        {
            LoginUser = null;

            return true;
        }

        public string UpdatePassword(ResetPassword login)
        {
            StringBuilder builder = new StringBuilder();
            var user = _authenticationRepository.LogInAsync(new LoginModel { LoginType = login.role, Password = login.password, UserName = login.loginName });

            if (user == null)
            {
                builder.AppendLine("Invalid current password");
            }

            if (login.newPassword.Length < 8)
            {
                builder.AppendLine("New password atleast 8 char in length");
            }

            if (login.newPassword != login.repeatPassword)
            {
                builder.AppendLine("New password is not same as Repeat password");
            }

            if (builder.ToString().Length <= 0)
            {
                try
                {
                    _authenticationRepository.UpdatePassword(login);
                    builder.Append("Success");
                }
                catch
                {
                    builder.Append("Failed");
                }
            }

            return builder.ToString();
        }

        public async Task<bool> ValidateUserName(string email, string LoginType)
        {
            var user = await _authenticationRepository.ValidateUserName(email, LoginType);
            if (user == null)
            {
                return false;
            }

            return true;

        }

        public async Task<bool> SendPasswordEmail(string email, string LoginType)
        {
            string emailMessage = "Hello ,<br>Your password is @@password@@.<br>Thank you";
            var smtp = configuration.GetSection("Smtp").Value;
            var from = configuration.GetSection("from").Value;
            var Password = configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(configuration.GetSection("Port").Value);
            var EmailTo = configuration.GetSection("TestEmail").Value;

            if (string.IsNullOrWhiteSpace(EmailTo))
            {
                EmailTo = email;
            }

            var user = await _authenticationRepository.RecoverPassword(email, LoginType);
            bool isemailsend = true;
            if (!string.IsNullOrEmpty(user))
            {
                emailMessage = emailMessage.Replace("@@password@@", user);
                isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, "Amax- Password Recovery", emailMessage, Port);
            }

            return isemailsend;

        }

        public async Task<DesktopLoginUser> LogInDesktopAsync(string userName, string password)
        {
            DesktopLoginUser loginUser = null;
            var ApiKey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;

            loginUser = await _authenticationRepository.LogInDesktopAsync(userName, password);

            if (loginUser != null)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ApiKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, loginUser.Id.ToString()),
                        //new Claim(ClaimTypes.Role, loginUser.GroupName) 
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                loginUser.Token = tokenHandler.WriteToken(token);

                var x = _authenticationRepository.GetAgencyFeatureAsync(loginUser.Id).Result;
                loginUser.Features = x.Item1;
                loginUser.Agencies = x.Item2;

            }

            return loginUser;
        }
    }
}
