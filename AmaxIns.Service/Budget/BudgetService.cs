﻿using AmaxIns.Common.ImportData;
using AmaxIns.Common.ImportData.CommonConstants;
using AmaxIns.DataContract.Budget;
using AmaxIns.DataContract.Response;
using AmaxIns.Repository.Budget;
using AmaxIns.RepositoryContract.Budget;
using AmaxIns.ServiceContract.Budget;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Budget
{
    public class BudgetService : BaseService, IBudgetService
    {
        IBudgetRepository _repository;
        public BudgetService(IBudgetRepository repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = repository;
        }

        public Task<IEnumerable<BudgetModel>> GetBudgetAnnual(BudgetFormModel budget)
        {
            return this._repository.GetBudgetAnnual(budget);
        }

        public Task<IEnumerable<string>> GetBudgetBudgetName(BudgetSearch obj)
        {
            return this._repository.GetBudgetBudgetName(obj);
        }

        public Task<BudgetModel> GetBudgetByMonth(BudgetFormModel model)
        {
            return this._repository.GetBudgetByMonth(model);
        }

        public Task<IEnumerable<string>> GetBudgetCategories(BudgetSearch obj)
        {
            return this._repository.GetBudgetCategories(obj);
        }

        public Task<IEnumerable<string>> GetBudgetDept(int userId)
        {
            return this._repository.GetBudgetDept(userId);
        }

        public Task<IEnumerable<BudgetOverspendModel>> GetBudgetSpending(BudgetFormModel budget)
        {
            return this._repository.GetBudgetSpending(budget);
        }

        public Task<IEnumerable<BudgetTransactionModel>> GetBudgetTrx(BudgetFormModel budget)
        {
            return this._repository.GetBudgetTrx(budget);
        }

        public Task<IEnumerable<string>> GetBudgetUser(BudgetSearch budgetSearch)
        {
            return this._repository.GetBudgetUser(budgetSearch);
        }

        public Task<IEnumerable<string>> GetBudgetLocation(BudgetSearch budgetSearch)
        {
            return this._repository.GetBudgetLocation(budgetSearch);
        }

        public Task<IEnumerable<string>> GetBudgetSource(BudgetSearch budgetSearch)
        {
            return this._repository.GetBudgetSource(budgetSearch);
        }

        public Task<IEnumerable<string>> GetBudgetCode(BudgetSearch budgetSearch)
        {
            return this._repository.GetBudgetCode(budgetSearch);
        }

        public Task<IEnumerable<BudgetTrancModel>> GetBudgetTranc(BudgetFormModel budget)
        {
            return this._repository.GetBudgetTranc(budget);
        }
        public Task<IEnumerable<BudgetSummaryModel>> GetBudgetSummary(BudgetFormModel budget)
        {
            return this._repository.GetBudgetSummary(budget);
        }

        public ResponseViewModel UploadExcelFile(IFormCollection formCollection)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var file = formCollection.Files.First();
                string monthValue = formCollection.First().Value;
                string yearValue = formCollection.Where(x => x.Key == "year").First().Value;

                if (file.Name == ExcelMode.actualFile)
                {
                    response = UploadActualExcelFile(file, monthValue, yearValue);
                }
                else if (file.Name == ExcelMode.budgetFile)
                {
                    response = UploadBudgetExcelFile(file, monthValue, yearValue);
                }
                else
                {
                    response.StatusCode = 404;
                    response.Message = "Please select correct file";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 501;
                response.Message = "Something went wrong";
                return response;

            }
            return response;
        }


        public ResponseViewModel UploadActualExcelFile(IFormFile file, string monthValue, string yearValue)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<ActualTable> actualTablesList = new List<ActualTable>();

                if (file.ContentType == ExcelMode.xls || file.ContentType == ExcelMode.xlsx || file.ContentType == ExcelMode.csv)
                {

                    Stream fileStream = file.OpenReadStream();
                    ExcelEngine engine = new ExcelEngine();
                    // set the stream position to zero
                    fileStream.Position = 0;
                    var workbook = engine.Excel.Workbooks.Open(fileStream, ExcelOpenType.Automatic, ExcelParseOptions.Default);
                    IWorksheet worksheet = workbook.Worksheets[0];

                    var missingColumns = CheckActualColumns(worksheet);
                    if (missingColumns.Length > 0)
                    {
                        response.StatusCode = 400;
                        response.Message = "Columns are missing " + missingColumns + ". Please check";
                        return response;
                    }

                    actualTablesList = worksheet.ExportData<ActualTable>(1, 1, worksheet.Rows.Count(), worksheet.Columns.Count()).Where(x => x.PeriodID == monthValue && x.YearID == yearValue).ToList();
                    var dateError = CheckDateFormat(actualTablesList);
                    if(dateError)
                    {
                        response.StatusCode = 400;
                        response.Message = "Transaction Date is not in correct format. Please check file";
                        return response;
                    }

                    foreach(var actualTable in actualTablesList.Where(x => x.TRXAmount.Contains('$')))
                    {
                        actualTable.TRXAmount = UpdateAmountValue(actualTable.TRXAmount);
                    }

                    this._repository.DeleteActualFileExestingData(Convert.ToInt32(monthValue), Convert.ToInt32(yearValue));
                    return this._repository.UploadActualExcelFile(actualTablesList);
                }
                else
                {
                    response.StatusCode = 404;
                    response.Message = "File is not in correct format";
                    return response;
                }
            }

            catch (Exception ex)
            {
                response.StatusCode = 501;
                response.Message = "Something went wrong";
                return response;
            }
        }

        public ResponseViewModel UploadBudgetExcelFile(IFormFile file, string monthValue, string yearValue)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<BudgetImportTable> budgetImportTablesList = new List<BudgetImportTable>();
                List<BudgetTable> budgetTablesList = new List<BudgetTable>();

                if (file.ContentType == ExcelMode.xls || file.ContentType == ExcelMode.xlsx || file.ContentType == ExcelMode.csv)
                {

                    Stream fileStream = file.OpenReadStream();
                    ExcelEngine engine = new ExcelEngine();
                    // set the stream position to zero
                    fileStream.Position = 0;
                    var workbook = engine.Excel.Workbooks.Open(fileStream, ExcelOpenType.Automatic, ExcelParseOptions.Default);
                    IWorksheet worksheet = workbook.Worksheets[0];
                    //DataTable dataTable = worksheet.ExportDataTable(worksheet.UsedRange, ExcelExportDataTableOptions.ColumnNames);

                    var missingColumns = CheckBudgetColumns(worksheet);
                    if (missingColumns.Length > 0)
                    {
                        response.StatusCode = 400;
                        response.Message = "Columns are missing " + missingColumns + ". Please check";
                        return response;
                    }

                    budgetImportTablesList = worksheet.ExportData<BudgetImportTable>(1, 1, worksheet.Rows.Count(), worksheet.Columns.Count()).Where(x => x.PeriodID == monthValue && x.YearID == yearValue).ToList();

                    budgetTablesList = budgetImportTablesList.Select(x => new BudgetTable()
                    {
                        PlanID = x.PlanID,
                        Year = x.YearID,
                        Period = x.PeriodID,
                        Department = x.DeptName,
                        CategoryName = x.CategoryDescription,
                        FrontBudgetCode = x.FrontBudgetCode,
                        BackBudgetCode = x.SubBgtCode,
                        BudgetName = x.LineItemDescription,
                        BudgetAmount = UpdateAmountValue(x.Amount),
                        OriginalDeptName = null
                    }).Where(x => x.Period == monthValue && x.Year == yearValue).ToList();

                    this._repository.DeleteBudgetFileExestingData(Convert.ToInt32(monthValue), Convert.ToInt32(yearValue));

                    

                    return this._repository.UploadBudgetExcelFile(budgetTablesList);

                }
                else
                {
                    response.StatusCode = 404;
                    response.Message = "File not in correct format";
                    return response;
                }
            }


            catch (Exception ex)
            {
                response.StatusCode = 501;
                response.Message = "Something went wrong";
                return response;
            }
        }

        public string UpdateAmountValue(string amount)
        {
            if(amount.Contains('$'))
            {
                amount = amount.Replace("$", "").Trim();
                if(amount.Length == 1 && amount.Contains("-"))
                {
                    amount = "0";
                }
            }
            return amount;
        }

        public string CheckActualColumns(IWorksheet worksheet)
        {
            string missingColumns = "";
            var columnList = ColumnList.GetActualColumnList();

            for (int i = 0; i < columnList.Count(); i++)
            {
                var column = worksheet.Columns.Where(x => x.DisplayText == columnList[i]);
                if (column.Count() == 0)
                {
                    missingColumns += columnList[i] + ',';
                }
            }
            return missingColumns;
        }

        public string CheckBudgetColumns(IWorksheet worksheet)
        {
            string missingColumns = "";
            var columnList = ColumnList.GetBudgetColumnList();

            for (int i = 0; i < columnList.Count(); i++)
            {
                var column = worksheet.Columns.Where(x => x.DisplayText == columnList[i]);
                if (column.Count() == 0)
                {
                    missingColumns += columnList[i] + ',';
                }
            }
            return missingColumns;
        }

        public bool CheckDateFormat(List<ActualTable> actualTablesList)
        {
            var data = actualTablesList.Select(x => x.TRXDate).Distinct().ToList();
            var dateError = false;
            foreach(var d in data)
            {
                if(!d.Contains("/"))
                {
                    dateError = true;
                }
            }
            return dateError;
        }

        public Task<IEnumerable<string>> GetLatestMonthValue(int year)
        {
            return this._repository.GetLatestMonthValue(year);
        }

        public string ExportAccountDetals(IEnumerable<BudgetOverspendModel> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Account-Deatils-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                decimal budgetTotal = 0; decimal actualTotal = 0; decimal varianceTotal = 0; decimal variancePercentage = 0;

                budgetTotal= list.AsEnumerable().Sum(o => o.Budget);
                actualTotal = list.AsEnumerable().Sum(o => o.Actual);
                varianceTotal = budgetTotal - actualTotal;
                variancePercentage = (varianceTotal / budgetTotal) * 100;

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 6].Merge = true;
                    workSheet.Cells[1, 1, 1, 6].Value = "* Account Deatils *";
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "Category Name";
                    workSheet.Cells[2, 2].Value = "Budget Code";
                    workSheet.Cells[2, 3].Value = "Budget Name";
                    workSheet.Cells[2, 4].Value = "Budget";
                    workSheet.Cells[2, 5].Value = "Actual";
                    workSheet.Cells[2, 6].Value = "Variance";
                    workSheet.Cells[2, 7].Value = "Variance Rate";

                    workSheet.Cells[2, 1, 2, 7].Style.Font.Bold = true;

                    list= from s in list
                          orderby s.CategoryName
                    select s;
                    string _colorCode = string.Empty;
                    int row = 2;
                    foreach (BudgetOverspendModel obj in list)
                    {
                        if (obj.Spending == "Safe")
                        {
                            _colorCode = "#59a14f";
                        }
                        else if (obj.Spending == "Good")
                        {
                            _colorCode = "#f28e2b";
                        }
                        else if (obj.Spending == "Overspending")
                        {
                            _colorCode = "#e15759";
                        }
                        row++;

                        workSheet.Cells[row, 1].Value = obj.CategoryName;
                        workSheet.Cells[row, 2].Value = obj.FrontBudgetCode;
                        workSheet.Cells[row, 3].Value = obj.BudgetName;
                        workSheet.Cells[row, 4].Value = obj.Budget;
                        workSheet.Cells[row, 5].Value = obj.Actual;
                        workSheet.Cells[row, 6].Value = obj.Variance;
                        workSheet.Cells[row, 7].Value = obj.VarianceRate;

                        workSheet.Cells[row, 4].Style.Font.Color.SetColor(ColorTranslator.FromHtml(_colorCode));
                        workSheet.Cells[row, 5].Style.Font.Color.SetColor(ColorTranslator.FromHtml(_colorCode));
                        workSheet.Cells[row,6].Style.Font.Color.SetColor(ColorTranslator.FromHtml(_colorCode));
                        workSheet.Cells[row, 7].Style.Font.Color.SetColor(ColorTranslator.FromHtml(_colorCode));

                    }
                    row = row + 1;
                    workSheet.Cells[row, 1].Value = "";
                    workSheet.Cells[row, 2].Value = "";
                    workSheet.Cells[row, 3].Value = "Total";
                    workSheet.Cells[row, 4].Value = budgetTotal;
                    workSheet.Cells[row, 5].Value = actualTotal;
                    workSheet.Cells[row, 6].Value = varianceTotal;
                    workSheet.Cells[row, 7].Value = variancePercentage;

                 

                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
    }
}
