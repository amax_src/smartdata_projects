﻿using AmaxIns.DataContract.Authentication;
using AmaxIns.RepositoryContract.Budget;
using AmaxIns.ServiceContract.Budget;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Budget
{
    public class BudgetAuthenticationService : BaseService, IBudgetAuthenticationService
    {
        private readonly IBudgetAuthenticationRepository _authenticationRepository;
        IConfiguration configuration;

        public BudgetAuthenticationService(IBudgetAuthenticationRepository authenticationRepository, IConfiguration _configuration)
           : base(_configuration)
        {
            _authenticationRepository = authenticationRepository;
            configuration = _configuration;
        }

        public async Task<LoginUser> LogInAsync(LoginModel login)
        {
            LoginUser loginUser = null;
            var ApiKey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;

            loginUser = await _authenticationRepository.LogInAsync(login);

            if (loginUser != null)
            {
                loginUser.UserID = loginUser.ReferenceId.HasValue ? loginUser.ReferenceId.Value : -1;//-1 is setting for the HOD and Admin

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ApiKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, loginUser.UserID.ToString()),
                        //new Claim(ClaimTypes.Role, loginUser.GroupName) 
                    }),
                    //if(login.Application=="Turborater")
                    Expires = login.Application == "Turborater" ? DateTime.UtcNow.AddDays(1) : DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                loginUser.Token = tokenHandler.WriteToken(token);
            }
            return loginUser;
        }
    }
}
