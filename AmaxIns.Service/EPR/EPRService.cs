﻿using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.Revenue;
using AmaxIns.RepositoryContract.EPR;
using AmaxIns.RepositoryContract.Revenue;
using AmaxIns.ServiceContract.EPR;
using AmaxIns.ServiceContract.Revenue;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.EPR
{
    public class EPRService : BaseService, IEPRService
    {
        IEPRRepository _repository;
        public EPRService(IEPRRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public Task<IEnumerable<EPRModel>> GetEPRData()
        {
            return _repository.GetEPRData();
        }

        public Task<IEnumerable<string>> GetEPRmonths()
        {
            return _repository.GetEPRmonths();
        }

        public Task<IEnumerable<EPRModel>> GetEPRTotalData()
        {
            return _repository.GetEPRTotalData();
        }
        public Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string _year, string location, string _selectedagent)
        {
            return _repository.GetEPRGraph(_year, location, _selectedagent);
        }
    }
}
