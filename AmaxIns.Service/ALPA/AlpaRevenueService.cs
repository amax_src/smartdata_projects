﻿using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using AmaxIns.RepositoryContract.ALPA;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.ALPA
{
    public class AlpaRevenueService : BaseService, IAlpaRevenueService
    {
        IAlpaRevenueRepository _revenueRepository;
        public AlpaRevenueService(IAlpaRevenueRepository revenueRepository, IConfiguration configuration) : base(configuration)
        {
            this._revenueRepository = revenueRepository;
        }

        public async Task<IEnumerable<RevenueModel>> GetAlpaRevenueData()
        {
            var result = await _revenueRepository.GetAlpaRevenueData();
            return result;
        }

        public async Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData()
        {
            var result = await _revenueRepository.GetRevenueDailyData();
            return result;
        }

        public async Task<IEnumerable<DailyTransactionPayments>> DailyTransactionPayments(QuotesParam quotesParam, string month = null, string year = null)
        {
            var result = await _revenueRepository.DailyTransactionPayments(quotesParam, month, year);
            return result;
        }

        public string ExportDailyTransactonPayment(IEnumerable<DailyTransactionPayments> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Daily-Transacton-Payment-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 22].Merge = true;
                    workSheet.Cells[1, 1, 1, 22].Value = "* Daily-Transacton-Payment *";



                    workSheet.Cells[1, 1, 1, 22].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 22].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 22].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "Date";
                    workSheet.Cells[2, 2].Value = "Receipt #";
                    workSheet.Cells[2, 3].Value = "Bank Account";
                    workSheet.Cells[2, 4].Value = "Agency Name";
                    workSheet.Cells[2, 5].Value = "Location";
                    workSheet.Cells[2, 6].Value = "Policy Type";
                    workSheet.Cells[2, 7].Value = "Company";
                    workSheet.Cells[2, 8].Value = "Policy Number";
                    workSheet.Cells[2, 9].Value = "Policy Status";
                    workSheet.Cells[2, 10].Value = "Customer Name";
                    workSheet.Cells[2, 11].Value = "Email Address";
                    workSheet.Cells[2, 12].Value = "Referral Source";
                    workSheet.Cells[2, 13].Value = "Received by";
                    workSheet.Cells[2, 14].Value = "Sent to";
                    workSheet.Cells[2, 15].Value = "Payment For";
                    workSheet.Cells[2, 16].Value = "PaymentMethod";
                    workSheet.Cells[2, 17].Value = "Fee Type";
                    workSheet.Cells[2, 18].Value = "Amount";
                    workSheet.Cells[2, 19].Value = "Fee";
                    workSheet.Cells[2, 20].Value = "Total";
                    workSheet.Cells[2, 21].Value = "Balance";
                    workSheet.Cells[2, 22].Value = "Payment Notes";



                    workSheet.Cells[2, 1, 2, 22].Style.Font.Bold = true;

                    int row = 2;
                    foreach (DailyTransactionPayments obj in list)
                    {
                        row++;

                        workSheet.Cells[row, 1].Value = obj.DatePayDate.ToString("MM/dd/yyyy hh:mm:ss");
                        workSheet.Cells[row, 2].Value = obj.PaymentId;
                        workSheet.Cells[row, 3].Value = obj.BankAccount;
                        workSheet.Cells[row, 4].Value = obj.AgencyName;
                        workSheet.Cells[row, 5].Value = obj.Location;
                        workSheet.Cells[row, 6].Value = obj.PolicyType;
                        workSheet.Cells[row, 7].Value = obj.coName;
                        workSheet.Cells[row, 8].Value = obj.PolicyNumber;
                        workSheet.Cells[row, 9].Value = obj.PolicyStatus;
                        workSheet.Cells[row, 10].Value = obj.ApplicantName;
                        workSheet.Cells[row, 11].Value = obj.Email;
                        workSheet.Cells[row, 12].Value = obj.ReferralSource;
                        workSheet.Cells[row, 13].Value = obj.AgentName;
                        workSheet.Cells[row, 14].Value = obj.SentToCompany;
                        workSheet.Cells[row, 15].Value = obj.PayType;
                        workSheet.Cells[row, 16].Value = obj.PayMethod;
                        workSheet.Cells[row, 17].Value = obj.FeeType;
                        workSheet.Cells[row, 18].Value = obj.MoneyPayAmount;
                        workSheet.Cells[row, 19].Value = obj.MoneyPayFee;
                        workSheet.Cells[row, 20].Value = obj.MoneypayTotal;
                        workSheet.Cells[row, 21].Value = obj.MoneyPayBalance;
                        workSheet.Cells[row, 22].Value = obj.PayNotes;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {
                return "";
            }

            return fileName;
        }

    }
}
