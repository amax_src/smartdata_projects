﻿using AmaxIns.DataContract.AIS;
using AmaxIns.RepositoryContract.ALPA;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.ALPA
{
    public class AISAlpaService : BaseService, IAISAlpaService
    {
        IAISAlpaRepository _repository;
        public AISAlpaService(IAISAlpaRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public Task<IEnumerable<AISModel>> GetAISData()
        {
            return _repository.GetAISData();
        }
    }
}
