﻿using AmaxIns.DataContract.EPR;
using AmaxIns.RepositoryContract.ALPA;
using AmaxIns.RepositoryContract.EPR;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.ALPA
{
   
    public class AlpaEprService : BaseService, IAlpaEprService
    {
        IAlpaEprRepository _repository;
        public AlpaEprService(IAlpaEprRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public Task<IEnumerable<EPRModel>> GetEPRData()
        {
            return _repository.GetEPRData();
        }

        public Task<IEnumerable<string>> GetEPRmonths()
        {
            return _repository.GetEPRmonths();
        }

        public Task<IEnumerable<EPRModel>> GetEPRTotalData()
        {
            return _repository.GetEPRTotalData();
        }

        public Task<IEnumerable<EPRGraphModel>> GetEPRGraph(string _year, string location, string _selectedagent)
        {
            return _repository.GetEPRGraph(_year, location, _selectedagent);
        }
    }
}
