﻿using System.Collections.Generic;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using AmaxIns.ServiceContract.User;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace AmaxIns.Service.User
{
    public class SaleDirectorService : BaseService, ISaleDirectorService
    {
        private readonly ISaleDirectorRepository _saleDirectorRepository;
        public SaleDirectorService(IConfiguration configuration, ISaleDirectorRepository saleDirectorRepository) : base(configuration)
        {
            this._saleDirectorRepository = saleDirectorRepository;
        }
        public Task<IEnumerable<SaleDirectorModel>> GetAll()
        {
            return this._saleDirectorRepository.GetAll();
        }

        public Task<SaleDirectorModel> GetById(int Id)
        {
            return this._saleDirectorRepository.GetById(Id);
        }
    }
}

