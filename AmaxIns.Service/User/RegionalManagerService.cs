﻿
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using AmaxIns.ServiceContract.User;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.User
{
    public class RegionalManagerService : BaseService, IRegionalManagerService
    {
        private readonly IRegionalManagerRepository _regionalManagerRepository;
        public RegionalManagerService(IConfiguration configuration, IRegionalManagerRepository regionalManagerRepository) : base(configuration)
        {
            this._regionalManagerRepository = regionalManagerRepository;
        }
        public Task<IEnumerable<RegionalManagerModel>> GetAll()
        {
            return this._regionalManagerRepository.GetAll();
        }

        public Task<RegionalManagerModel> GetById(int Id)
        {
            return this._regionalManagerRepository.GetById(Id);
        }

        public Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(int Id)
        {
            return this._regionalManagerRepository.GetBySaleDirectorId(Id);
        }

        public async Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(List<int> Ids)
        {
            List<RegionalManagerModel> _listZonalManager = new List<RegionalManagerModel>();
            foreach (var x in Ids)
            {
                var result = this._regionalManagerRepository.GetBySaleDirectorId(x);
                foreach (var z in result.Result)
                {
                    _listZonalManager.Add(new RegionalManagerModel { EmployeeId = z.EmployeeId, RegionalManager = z.RegionalManager, RegionalManagerId = z.RegionalManagerId });
                }
            }

            return _listZonalManager;
        }

    }

}
