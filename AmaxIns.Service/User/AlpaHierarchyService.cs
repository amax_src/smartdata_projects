﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using AmaxIns.ServiceContract.User;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.User
{
    public class AlpaHierarchyService : BaseService, IAlpaHierarchyService
    {
        private readonly IAlpaHierarchyRepository _AlpaHierarchyRepository;
        public AlpaHierarchyService(IConfiguration configuration, IAlpaHierarchyRepository alpaHierarchyRepository) : base(configuration)
        {
            this._AlpaHierarchyRepository = alpaHierarchyRepository;
        }

        #region RegionalManager

        public Task<IEnumerable<RegionalManagerModel>> RegionalManagerALPA()
        {
            return this._AlpaHierarchyRepository.RegionalManagerALPA();
        }
        #endregion


        #region SaleDirector

        public Task<IEnumerable<SaleDirectorModel>> GetAllAlpaSaleDirector()
        {
            return this._AlpaHierarchyRepository.GetAllAlpaSaleDirector();
        }

        #endregion

        #region ZonalManager
        public Task<IEnumerable<ZonalManagerModel>> ZonalManagerALPA()
        {
            return this._AlpaHierarchyRepository.ZonalManagerALPA();
        }
        #endregion

        public async Task<IEnumerable<AgencyModel>> GetAllAgency()
        {
            var result = await _AlpaHierarchyRepository.GetAllAgency();
            result = result.Where(m => m.AgencyId != 1).ToList();
            result = result.Where(m => m.AgencyId != 99).OrderBy(x => x.AgencyName).ToList();
            return result;
        }

        public async Task<IEnumerable<RegionalManagerModel>> GetBySaleDirectorId(List<int> Ids)
        {
            List<RegionalManagerModel> _listZonalManager = new List<RegionalManagerModel>();
            foreach (var x in Ids)
            {
                var result = this._AlpaHierarchyRepository.GetBySaleDirectorId(x);
                foreach (var z in result.Result)
                {
                    _listZonalManager.Add(new RegionalManagerModel { EmployeeId = z.EmployeeId, RegionalManager = z.RegionalManager, RegionalManagerId = z.RegionalManagerId });
                }
            }

            return _listZonalManager;
        }

        public async Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(List<int> Ids)
        {
            List<ZonalManagerModel> _listZonalManager = new List<ZonalManagerModel>();
            foreach (var x in Ids)
            {
                var result = this._AlpaHierarchyRepository.GetByRegionalManagerId(x);
                foreach (var z in result.Result)
                {
                    _listZonalManager.Add(new ZonalManagerModel { EmployeeId = z.EmployeeId, ZonalManager = z.ZonalManager, ZonalManagerId = z.ZonalManagerId });
                }
            }

            return _listZonalManager;
        }

        public async Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(List<int> RegionalManagerId, List<int> ZonalManagerId)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            if (RegionalManagerId != null && RegionalManagerId.Count > 0)
            {
                foreach (var x in RegionalManagerId)
                {
                    foreach (var z in ZonalManagerId)
                    {
                        var result = await _AlpaHierarchyRepository.GetAllLocationsRegionalZonalManager(x, z); ;
                        foreach (var item in result)
                        {
                            if (item.AgencyId != 1 || item.AgencyId != 99)
                            {
                                if (!_agencies.Any(m => m.AgencyId == item.AgencyId))
                                {
                                    _agencies.Add(new AgencyModel { AgencyId = item.AgencyId, AgencyName = item.AgencyName, IsActive = item.IsActive });
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var z in ZonalManagerId)
                {
                    var result = await _AlpaHierarchyRepository.GetbyZonalManager(z); ;
                    foreach (var item in result)
                    {
                        if (item.AgencyId != 1 || item.AgencyId != 99)
                        {
                            if (!_agencies.Any(m => m.AgencyId == item.AgencyId))
                            {
                                _agencies.Add(new AgencyModel { AgencyId = item.AgencyId, AgencyName = item.AgencyName, IsActive = item.IsActive });
                            }
                        }
                    }
                }
            }

            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;

        }

        public async Task<IEnumerable<AgencyModel>> GetAllLocationsRegionalZonalManager(List<int> RegionalManagerIds)
        {
            List<AgencyModel> _agencies = new List<AgencyModel>();
            foreach (var x in RegionalManagerIds)
            {
                var result = await this._AlpaHierarchyRepository.GetAllLocationsRegional(x);
                foreach (var z in result)
                {
                    if (z.AgencyId != 1 || z.AgencyId != 99)
                    {
                        _agencies.Add(new AgencyModel { AgencyId = z.AgencyId, AgencyName = z.AgencyName, IsActive = z.IsActive });
                    }
                }
            }
            _agencies = _agencies.OrderBy(x => x.AgencyName).ToList();
            return _agencies;


        }

        public async Task<IEnumerable<AgencyModel>> GetStoreManagerAgency(int Id)
        {
            var result = await this._AlpaHierarchyRepository.GetStoreManagerAgency(Id);
            result = result.OrderBy(x => x.AgencyName).ToList();
            return result;
        }
    }
}
