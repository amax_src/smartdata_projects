﻿
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.User;
using AmaxIns.ServiceContract.User;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.User
{
    public class ZonalManagerService : BaseService, IZonalManagerService
    {
        private readonly IZonalManagerRepository _zonalManagerRepository;
        public ZonalManagerService(IConfiguration configuration, IZonalManagerRepository zonalManagerRepository) :base(configuration)
        {
            this._zonalManagerRepository = zonalManagerRepository;
        }
        public Task<IEnumerable<ZonalManagerModel>> GetAll()
        {
           return this._zonalManagerRepository.GetAll();
        }

        public Task<ZonalManagerModel> GetById(int Id)
        {
            return this._zonalManagerRepository.GetById(Id);
        }

        public Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(int Id)
        {
            return this._zonalManagerRepository.GetByRegionalManagerId(Id);
        }

        public async Task<IEnumerable<ZonalManagerModel>> GetByRegionalManagerId(List<int> Ids)
        {
            List<ZonalManagerModel> _listZonalManager = new List<ZonalManagerModel>();
            foreach (var x in Ids)
            {
                var result = this._zonalManagerRepository.GetByRegionalManagerId(x);
                foreach(var z in result.Result)
                {
                    _listZonalManager.Add(new ZonalManagerModel {EmployeeId=z.EmployeeId,ZonalManager=z.ZonalManager,ZonalManagerId=z.ZonalManagerId });
                }
            }

            return _listZonalManager;
        }
    }
}
