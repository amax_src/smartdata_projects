﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.LiveDataApp
{
   public class BolQuoteService : BaseService, IBolQuoteService
    {
        private IBolQuoteRepository Repository;
        public BolQuoteService(IBolQuoteRepository _Repository, IConfiguration configuration) : base(_Repository, configuration)
        {
            Repository = _Repository;
        }

        
        public List<MonthModel> GetCarrierName()
        {
            List<MonthModel> carriers = new List<MonthModel>();

            var carr = Enum.GetValues(typeof(CarrierName))
                         .Cast<CarrierName>()
                         .Select(v => v.ToString())
                         .ToList();

            foreach(var x in carr)
            {
                switch (x)
                {
                    case "alinsco":
                        carriers.Add(new MonthModel { Id = x, Name = "Alinsco" });
                        break;
                    case "assuranceamerica":
                        carriers.Add(new MonthModel { Id = x, Name = "Assurance America" });
                        break;
                    case "commonwealthgeneral":
                        carriers.Add(new MonthModel { Id = x, Name = "Common Wealth" });
                        break;
                    case "americanaccess":
                        carriers.Add(new MonthModel { Id = x, Name = "American Access" });
                        break;
                    case "seaharbor":
                        carriers.Add(new MonthModel { Id = x, Name = "SeaHarbor" });
                        break;
                    case "unitedauto":
                        carriers.Add(new MonthModel { Id = x, Name = "United Auto" });
                        break;
                    case "lamargeneralagency":
                        carriers.Add(new MonthModel { Id = x, Name = "Lamar General Agency" });
                        break;
                    case "stonegate":
                        carriers.Add(new MonthModel { Id = x, Name = "Stonegate" });
                        break;
                    case "evolution":
                        carriers.Add(new MonthModel { Id = x, Name = "Evolution" });
                        break;
                    case "bristolwest":
                        carriers.Add(new MonthModel { Id = x, Name = "Bristol West" });
                        break;
                    case "unique":
                        carriers.Add(new MonthModel { Id = x, Name = "Unique" });
                        break;
                }
            }

            return carriers;

        }

        public async Task<IEnumerable<BOLQuoteData>> GetDataBolData(List<string> date,string state)
        {
            var data = await Repository.GetDataBolData(date, state);
            return data;
        }

        public async Task<IEnumerable<BOLQuoteData>> GetDataBolData_Quotes(List<string> date, string state)
        {
            var data = await Repository.GetDataBolData_Quotes(date, state);
            return data;
        }

        public async Task<BOLQuoteData> GetSingleQuote(string quoteNumber)
        {
            var data = await Repository.GetSingleQuote(quoteNumber);
            return data;
        }
        public async Task<IEnumerable<EODLive>> GetEODLiveReport(List<string> date)
        {
            var data = await Repository.GetEODLiveReport(date);
            return data;
        }

        public string AmaxHourlyData(IEnumerable<AgentCountModel> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Amax-Hourly-Data-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 6].Merge = true;
                    workSheet.Cells[1, 1, 1, 6].Value = "* Amax Hourly Data *";
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "Hour Interval";
                    workSheet.Cells[2, 2].Value = "Policy Count";
                    workSheet.Cells[2, 3].Value = "Premium";
                    workSheet.Cells[2, 4].Value = "Agency Fee";
                    workSheet.Cells[2, 5].Value = "Transactions";
                    workSheet.Cells[2, 6].Value = "Agents";

                    workSheet.Cells[2, 1, 2, 6].Style.Font.Bold = true;
                    int TotalNewCount = 0;
                    decimal Totalprem=0; decimal TotalFee = 0; decimal TotalTrans = 0;
                    int row = 2;
                    foreach (AgentCountModel obj in list)
                    {
                        row++;
                        
                        workSheet.Cells[row, 1].Value = obj.HourIntervel;
                        workSheet.Cells[row, 2].Value = obj.Policies;
                        workSheet.Cells[row, 3].Value = obj.Premium;
                        workSheet.Cells[row, 4].Value = obj.Agencyfee;
                        workSheet.Cells[row, 5].Value = obj.Transactions;
                        workSheet.Cells[row, 6].Value = obj.AgentCount;
                        TotalNewCount = TotalNewCount + obj.Policies;
                        Totalprem = Totalprem + obj.Premium;
                        TotalFee= TotalFee+ obj.Agencyfee;
                        TotalTrans= TotalTrans+ obj.Transactions;
                    }

                    workSheet.Cells[row, 1].Value ="Total";
                    workSheet.Cells[row, 2].Value = TotalNewCount;
                    workSheet.Cells[row, 3].Value = Totalprem;
                    workSheet.Cells[row, 4].Value = TotalFee;
                    workSheet.Cells[row, 5].Value = TotalTrans;
                    workSheet.Cells[row, 6].Value ="";

                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

         public string AmaxHourlyDataAgencyWise(IEnumerable<AgentCountModel> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Amax-Hourly-Data-Agency-Wise-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 6].Merge = true;
                    workSheet.Cells[1, 1, 1, 6].Value = "* Amax Hourly Data Agency Wise *";
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "Hour Interval";
                    workSheet.Cells[2, 2].Value = "Location";
                    workSheet.Cells[2, 3].Value = "Policy Count";
                    workSheet.Cells[2, 4].Value = "Premium";
                    workSheet.Cells[2, 5].Value = "Agency Fee";
                    workSheet.Cells[2, 6].Value = "Transactions";
                    workSheet.Cells[2, 7].Value = "Agents";

                    workSheet.Cells[2, 1, 2, 7].Style.Font.Bold = true;
                  
                    int row = 2;
                    foreach (AgentCountModel obj in list)
                    {
                        row++;
                        
                        workSheet.Cells[row, 1].Value = obj.HourIntervel;
                        workSheet.Cells[row, 2].Value = obj.Location;
                        workSheet.Cells[row, 3].Value = obj.Policies;
                        workSheet.Cells[row, 4].Value = obj.Premium;
                        workSheet.Cells[row, 5].Value = obj.Agencyfee;
                        workSheet.Cells[row, 6].Value = obj.Transactions;
                        workSheet.Cells[row, 7].Value = obj.AgentCount;
                       
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public async Task<IEnumerable<CustomerJourneyInfo>> GetCustomerJourneyInfoReport(List<string> date)
        {
            var data = await Repository.CustomerJourneyInfoReport(date);
            return data;
        }
        public async Task<IEnumerable<RetailCustomerJourneyInfo>> GetRetailCustomerJourneyInfoReport(List<string> date)
        {
            var data = await Repository.GetRetailCustomerJourneyInfoReport(date);
            return data;
        }

        public string ExportCustomerJourneyInfo(List<string> date, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                IEnumerable<CustomerJourneyInfo> customerJourneyInfos = Repository.CustomerJourneyInfoReport(date).GetAwaiter().GetResult();

                fileName = $"Customer-Journey-Info-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if(customerJourneyInfos.Count()>0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 27].Merge = true;
                        workSheet.Cells[1, 1, 1, 27].Value = "* Customer Journey Info *";
                        workSheet.Cells[1, 1, 1, 27].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 27].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 27].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "CustomerId";
                        workSheet.Cells[2, 2].Value = "Policy Number";
                        workSheet.Cells[2, 3].Value = "IsSold";
                        workSheet.Cells[2, 4].Value = "Name";
                        workSheet.Cells[2, 5].Value = "Birth Date";
                        workSheet.Cells[2, 6].Value = "License No";
                        workSheet.Cells[2, 7].Value = "Email";
                        workSheet.Cells[2, 8].Value = "Phone No";
                        workSheet.Cells[2, 9].Value = "Address";
                        workSheet.Cells[2, 10].Value = "Visited Date";
                        workSheet.Cells[2, 11].Value = "Quotes URL";
                        workSheet.Cells[2, 12].Value = "Quotes Number";
                        workSheet.Cells[2, 13].Value = "Carrier Name";
                        workSheet.Cells[2, 14].Value = "CallCenterSent";
                        workSheet.Cells[2, 15].Value = "BuyNowOption";
                        workSheet.Cells[2, 16].Value = "BuyNowClick";
                        workSheet.Cells[2, 17].Value = "PayPlanDescription";
                        workSheet.Cells[2, 18].Value = "PayPlanNoofPayments";
                        workSheet.Cells[2, 19].Value = "PayPlanPercentDown";
                        workSheet.Cells[2, 20].Value = "PayPlanDownPayment";
                        workSheet.Cells[2, 21].Value = "PayPlanPaymentAmount";
                        workSheet.Cells[2, 22].Value = "PayPlanServiceFee";
                        workSheet.Cells[2, 23].Value = "PayPlanPaymentTotal";
                        workSheet.Cells[2, 24].Value = "Term";
                        workSheet.Cells[2, 25].Value = "TotalPremium";
                        workSheet.Cells[2, 26].Value = "CarDetails";
                        workSheet.Cells[2, 27].Value = "Car Coverage Details";
                       

                        workSheet.Cells[2, 1, 2, 27].Style.Font.Bold = true;

                        int row = 2;
                        foreach (CustomerJourneyInfo obj in customerJourneyInfos)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.customerId;
                            workSheet.Cells[row, 2].Value = obj.PolicyNumber;
                            workSheet.Cells[row, 3].Value = obj.IsSold == true ? "Yes" : "No";
                            workSheet.Cells[row, 4].Value = obj.name+" "+ obj.surname;
                            workSheet.Cells[row, 5].Value = obj.birthDate == null ? "" : obj.birthDate.ToString("MM/dd/yyyy");
                            workSheet.Cells[row, 6].Value = obj.driverLicenseNumber;
                            workSheet.Cells[row, 7].Value = obj.email;
                            workSheet.Cells[row, 8].Value = obj.phoneNumber;
                            workSheet.Cells[row, 9].Value = obj.address1+", "+ obj.city + ", " + obj.state + ", " + obj.zipCode;
                            workSheet.Cells[row, 10].Value = obj.Date;
                            workSheet.Cells[row, 11].Value = obj.quoteUrl;
                            workSheet.Cells[row, 12].Value = obj.quoteNumber;
                            workSheet.Cells[row, 13].Value = obj.carrierName;
                            workSheet.Cells[row, 14].Value = obj.callCenterSent==true?"Yes":"No";
                            workSheet.Cells[row, 15].Value = obj.buyNowOption == true ? "Yes" : "No";
                            workSheet.Cells[row, 16].Value = obj.buyNowClick == true ? "Yes" : "No";
                            workSheet.Cells[row, 17].Value = obj.payPlanDescription;
                            workSheet.Cells[row, 18].Value = obj.payPlanNumOfPayments;
                            workSheet.Cells[row, 19].Value = obj.payPlanPercentDown+"%";
                            workSheet.Cells[row, 20].Value = obj.payPlanDownPayment;
                            workSheet.Cells[row, 21].Value = obj.payPlanPaymentAmount;
                            workSheet.Cells[row, 22].Value = obj.payPlanServiceFees;
                            workSheet.Cells[row, 23].Value = obj.payPlanPaymentTotal;
                            workSheet.Cells[row, 24].Value = obj.term;
                            workSheet.Cells[row, 25].Value = obj.totalPremium;
                            workSheet.Cells[row, 26].Value = obj.carDetails?.Replace("<b>","").Replace("</b>", "").Replace("</br>", Environment.NewLine);
                            workSheet.Cells[row, 27].Value = obj.carCoverageDetails?.Replace("<b>", "").Replace("</b>", "").Replace("</br>", Environment.NewLine);
                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }
                
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportOnlineQuote(IEnumerable<ExportBOLModel> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"BOL_Quotes-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 8].Merge = true;
                    workSheet.Cells[1, 1, 1, 8].Value = "**  BOL Quotes Data  **";
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "Customer Name";
                    //workSheet.Cells[2, 2].Value = "Quote Url";
                    workSheet.Cells[2, 2].Value = "State";
                    workSheet.Cells[2, 3].Value = "Carrier Name";
                    workSheet.Cells[2, 4].Value = "Time";
                    workSheet.Cells[2, 5].Value = "Afee";
                    workSheet.Cells[2, 6].Value = "Policy Sold";
                    workSheet.Cells[2, 7].Value = "Written Premium";
                    workSheet.Cells[2, 8].Value = "Collected Premium";


                    workSheet.Cells[2, 1, 2, 8].Style.Font.Bold = true;

                    int row = 2;
                    foreach (ExportBOLModel obj in list.Where(x=>!string.IsNullOrEmpty(x.policyNumber))?.ToList())
                    {
                        row++;
                        workSheet.Cells[row, 1].Value = obj.name + ' ' + obj.surname;
                        //workSheet.Cells[row, 2].Value = obj.quoteUrl;
                        workSheet.Cells[row, 2].Value = obj.state;
                        workSheet.Cells[row, 3].Value = obj.carrierName;
                        workSheet.Cells[row, 4].Value = obj.time;
                        workSheet.Cells[row, 5].Value = obj.agencyFee;
                        workSheet.Cells[row, 6].Value = obj.policyNumber;
                        workSheet.Cells[row, 7].Value = obj.premiumAmount;
                        workSheet.Cells[row, 8].Value = obj.collectedPremium;
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return fileName;
        }

    }
}
