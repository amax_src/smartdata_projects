﻿using AmaxIns.DataContract.LiveData;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using AmaxIns.RepositoryContract.Revenue;
using AmaxIns.RepositoryContract.WorkingDays;
using AmaxIns.RepositoryContract.Quotes;
using System.IO;
using AmaxIns.Common;
using AmaxIns.DataContract.HourlyLoad;
using System.Threading.Tasks;
using AmaxIns.DataContract.Agency;

namespace AmaxIns.Service.LiveDataApp
{
    public class LiveDataAppService : BaseService, ILiveDataAppService
    {
        private readonly ILivedataRepository Repository;
        private readonly IAgencyRepository AgencyRepo;
        private IRevenueRepository revenueRepository;
        private IWorkingDaysRepository workingDaysRepository;
        IQuotesRepository quotesRepository;
        public LiveDataAppService(ILivedataRepository _Repository, IConfiguration configuration, IAgencyRepository _AgencyRepo, IRevenueRepository _revenueRepository, IWorkingDaysRepository _workingDaysRepository, IQuotesRepository _quotesRepository) : base(_Repository, configuration)
        {
            Repository = _Repository;
            AgencyRepo = _AgencyRepo;
            revenueRepository = _revenueRepository;
            workingDaysRepository = _workingDaysRepository;
            quotesRepository = _quotesRepository;
        }
        public List<LiveDataAgency> GetData(out List<AgentCountModel> AmaxHourlyData, out List<AgentCountModel> AmaxHourlyDataAgencyWise)
        {
            LiveDataRoot data = new LiveDataRoot();
            List<LiveData> lb = new List<LiveData>();
            List<LiveDataAgency> liveDataAgencies = new List<LiveDataAgency>();
            List<LiveDataAgency> liveSortedDataAgencies = new List<LiveDataAgency>();
            double cashLog = 0;
            double creditcardLog = 0;
            int EndorsementCount = 0;
            data = Repository.GetData(string.Empty);
            CreateListData(data.value, lb);
            while (!string.IsNullOrEmpty(data.NextPageLink))
            {
                data = Repository.GetData(data.NextPageLink);
                lb = CreateListData(data.value, lb);
            }
            var agencys = AgencyRepo.GetAgencyAndLatLong().Result.ToList();
            agencys.Add(new DataContract.Agency.AgencyLatLong { AgencyName = "Amax Website" });
            //var quotsHistory = quotesRepository.GetQuotesHistory().Result;
            var workingdaysofyear = workingDaysRepository.GetWorkingDaysOfMonth().Result;
            var workingdays = workingDaysRepository.GetTotalWorkingDaysOfMonth(DateTime.UtcNow.Month, DateTime.UtcNow.Year).Result;
            var ModifiedquoteReport = quotesRepository.GetModifiedQuotesReport().Result;
            var AMPMDepositAllAgencies = revenueRepository.GetAMPMDeposit().Result;
            //calling the hourly report
            AmaxHourlyData = GetHourlyReportLiveData(lb);
            AmaxHourlyDataAgencyWise = GetHourlyLiveDataAgencyWise(lb);
            //end
            foreach (var x in agencys)
            {
                List<Agent> agent = new List<Agent>();
                List<PaymentMethod> PaymentMethod = new List<PaymentMethod>();
                double agencyFee = 0;
                double premium = 0;
                int policyCount = 0;
                var dataValues = lb.Where(m => m.Agency == x.AgencyName && m.@void == 0).ToList();
                //this.WriteToFile(dataValues, x.AgencyName);
                //var agencyQuotes = quotsHistory.Where(m => m.agencyName == x.AgencyName && m.counter == 0).ToList();
                // var quotsWalking = agencyQuotes.Where(m => m.c_contact_source.ToLower() == "walk-in").ToList();
                //var quotsPhoneInternet = agencyQuotes.Where(m => m.c_contact_source.ToLower() != "walk-in").ToList();
                int walkinSold = 0;
                int PhoneSold = 0;
                if (dataValues != null)
                {
                    foreach (var z in dataValues)
                    {
                        if (!agent.Any(m => m.Name == z.Agent))
                        {
                            agent.Add(new Agent
                            {
                                Name = z.Agent,
                                Trans = 1
                            });
                        }
                        else
                        {
                            foreach (var agentCount in agent)
                            {
                                if (agentCount.Name == z.Agent)
                                {
                                    agentCount.Trans = agentCount.Trans + 1;
                                }
                            }
                        }

                        agencyFee = agencyFee + z.payFee;
                        premium = premium + z.payAmount;

                        if (!PaymentMethod.Any(m => m.Name == z.payMethod))
                        {
                            PaymentMethod.Add(new PaymentMethod
                            {
                                Name = z.payMethod,
                                Amount = z.payTotal
                            });
                        }
                        else
                        {
                            foreach (var payment in PaymentMethod)
                            {
                                if (payment.Name == z.payMethod)
                                {
                                    payment.Amount = payment.Amount + z.payTotal;
                                }
                            }
                        }

                    }



                    foreach (var agentCount in agent)
                    {
                        agentCount.PolicyCount = dataValues.Where(m => m.payType.ToLower().Trim() == "down payment" && m.Agent.ToLower().Trim() == agentCount.Name.ToLower().Trim()).Count();
                        agentCount.Premium = dataValues.Where(m => m.Agent.ToLower().Trim() == agentCount.Name.ToLower().Trim()).Sum(m => m.payAmount);
                        agentCount.AgencyFee = dataValues.Where(m => m.Agent.ToLower().Trim() == agentCount.Name.ToLower().Trim()).Sum(m => m.payFee);
                        if (ModifiedquoteReport != null)
                        {
                            agentCount.Calls = ModifiedquoteReport.Where(m => m.Agent_name_turborater == agentCount.Name.Replace(":", string.Empty)).Count();
                        }
                    }
                    var remainingdays = workingdaysofyear.Where(m => m.Month == GetMonthName(DateTime.Now.Month) && m.Year == DateTime.Now.Year).FirstOrDefault();

                    EndorsementCount = dataValues.Where(m => m.payType.ToLower().Trim() == "endorsement").Count();
                    policyCount = dataValues.Where(m => m.payType.ToLower().Trim() == "down payment").Count();
                    cashLog = dataValues.Where(m => m.payMethod.ToLower().Trim() == "cash").Sum(m => m.payTotal);
                    creditcardLog = dataValues.Where(m => m.payMethod.ToLower().Trim() == "credit card").Sum(m => m.payTotal);
                    var AMPMDeposit = AMPMDepositAllAgencies.Where(m => m.AgencyName == x.AgencyName).FirstOrDefault();
                    liveDataAgencies.Add(new LiveDataAgency
                    {
                        AgencyFee = agencyFee,
                        AgencyName = x.AgencyName,
                        PolicyCount = policyCount,
                        Lat = x.Latitude,
                        Long = x.Longitude,
                        Premium = premium,
                        Agent = agent,
                        EndorsementCount = EndorsementCount,
                        CashLogTotal = cashLog,
                        AgentTotalAgencyFee = agent.Sum(m => m.AgencyFee),
                        AgentTotalPolicy = agent.Sum(m => m.PolicyCount),
                        AgentTotalPremium = agent.Sum(m => m.Premium),
                        AgentTotalTrans = agent.Sum(m => m.Trans),
                        AgentTotalcall = agent.Sum(m => m.Calls),
                        CreditCardLog = creditcardLog,
                        //WalkIn = quotsWalking == null ? 0 : quotsWalking.Count(),
                        //PhoneInternet = quotsPhoneInternet == null ? 0 : quotsPhoneInternet.Count(),
                        WalkInSold = walkinSold,
                        PhoneInternetSold = PhoneSold,
                        RemainingDays = remainingdays == null ? workingdays : remainingdays.RemainingDays,
                        AMPMDeposit = AMPMDeposit != null ? AMPMDeposit : new DataContract.Revenue.AmPmDepositModel(),
                        PaymentMethod = PaymentMethod
                    });
                }
            }
            liveSortedDataAgencies = liveDataAgencies.OrderByDescending(m => m.PolicyCount).ToList();
            var QuotesData = Repository.GetQuotes().Result;
            var dailyReveune = revenueRepository.GetRevenuToday().Result;
            var MonthlyReveune = revenueRepository.GetRevenueData().Result;
            foreach (var x in liveSortedDataAgencies)
            {
                List<Company> _comp = new List<Company>();
                var agenyDataLive = lb.Where(m => m.Agency == x.AgencyName).ToList();
                var results = from p in agenyDataLive
                              group p.Company by p.Company into g
                              select new { name = g.Key, count = g.Count() };

                foreach (var m in results)
                {
                        if (!string.IsNullOrWhiteSpace(m.name))
                        {
                        var _Count = lb.Where(c => c.payType.ToLower().Trim() == "down payment" && c.Company != null && c.Company.ToLower().Trim() == m.name.ToLower().Trim() && c.Agency == x.AgencyName);
                        _comp.Add(
                                      new
                                      Company
                                      {
                                          CompanyName = m.name,
                                          Count = _Count != null && _Count.Any() ? _Count.Count() : 0,
                                          //Count = lb.Where(c => c.payType.ToLower().Trim() == "down payment" && c.Company.ToLower().Trim() == m.name.ToLower().Trim() && c.Agency == x.AgencyName).Count()
                                      }); ;
                    }


                }

                var temp = QuotesData.Where(m => m.agencyName.Trim().ToLower() == x.AgencyName.Trim().ToLower()).ToList();
                var daily = dailyReveune.Where(m => m.AgencyName.Trim() == x.AgencyName.Trim()).ToList();
                var monthly = MonthlyReveune.Where(m => m.Actagencyname.Trim() == x.AgencyName.Trim() && m.Sortmonthnum == DateTime.UtcNow.Month && m.ActYear == DateTime.UtcNow.Year.ToString()).ToList();
                if (temp != null && temp.Any())
                {
                    x.NewQuotes = Convert.ToInt32(temp.FirstOrDefault().NewQuote);
                    x.ModifiedQuotes = Convert.ToInt32(temp.FirstOrDefault().AlreadyExisted);
                }

                if (daily != null && daily.Any())
                {
                    x.TodayAgencyFee = Convert.ToInt32(daily.FirstOrDefault().AgencyFee);
                    x.TodayPolicyCount = Convert.ToInt32(daily.FirstOrDefault().PolicyCount);
                    x.TodayPremium = Convert.ToInt32(daily.FirstOrDefault().Premium);
                    
                }

                if (monthly != null && monthly.Any())
                {
                    if (Convert.ToInt32(monthly.FirstOrDefault().GoalAgencyFee) > 0)
                    {
                        x.AgencyFeeGoal = Convert.ToInt32(monthly.FirstOrDefault().GoalAgencyFee);
                    }
                    else
                    {
                        x.AgencyFeeGoal = Convert.ToInt32(monthly.FirstOrDefault().Agency_fee);
                    }
                    if (Convert.ToInt32(monthly.FirstOrDefault().GoalPolicies) > 0)
                    {
                        x.PolicyGoal = Convert.ToInt32(monthly.FirstOrDefault().GoalPolicies);
                    }
                    else
                    {
                        x.PolicyGoal = Convert.ToInt32(monthly.FirstOrDefault().New_business_count);
                    }

                    //x.AgencyFeeGoal = Convert.ToInt32(monthly.FirstOrDefault().Agency_fee);
                    //x.PolicyGoal = Convert.ToInt32(monthly.FirstOrDefault().New_business_count);
                    x.PremiumGoal = Convert.ToInt32(monthly.FirstOrDefault().GoalPremium);
                    x.MtdAgencyFee = Convert.ToInt32(monthly.FirstOrDefault().Actagencyfee);
                    x.MtdPolicy = Convert.ToInt32(monthly.FirstOrDefault().Actnewbusinesscount);
                    x.MtdPremium = Convert.ToInt32(monthly.FirstOrDefault().Actpremium);
                    
                }


                x.TotalWorkingdays = workingdays;
                x.Companies = _comp;
                x.TotalCompaniesSale = _comp.Sum(m => m.Count);
            }

            return liveSortedDataAgencies;
        }

        private List<LiveData> CreateListData(List<LiveData> dataObject, List<LiveData> lb)
        {
            foreach (var x in dataObject)
            {
                lb.Add(x);
            }
            return lb;
        }

        private string GetMonthName(int month)
        {
            string monthname = "";
            switch (month)
            {
                case 1:
                    monthname = "January";
                    break;
                case 2:
                    monthname = "February";
                    break;
                case 3:
                    monthname = "March";
                    break;
                case 4:
                    monthname = "April";
                    break;
                case 5:
                    monthname = "May";
                    break;
                case 6:
                    monthname = "June";
                    break;
                case 7:
                    monthname = "July";
                    break;

                case 8:
                    monthname = "August";
                    break;
                case 9:
                    monthname = "September";
                    break;

                case 10:
                    monthname = "October";
                    break;

                case 11:
                    monthname = "November";

                    break;
                case 12:
                    monthname = "December";
                    break;
            }
            return monthname;
        }

        public ConsolidatedDidData GetConsolidatedDIDData(string day, string month, string year, string endday, string endmonth, string endyear)
        {
            List<AgencyDIDData> tvEnglishHouston = new List<AgencyDIDData>();
            List<AgencyDIDData> tvSpanishHouston = new List<AgencyDIDData>();
            List<AgencyDIDData> radioEnglishRgv = new List<AgencyDIDData>();
            List<AgencyDIDData> radioSpanisRrgv = new List<AgencyDIDData>();
            List<AgencyDIDData> allDigital = new List<AgencyDIDData>();
            List<AgencyDIDData> dfwEnglishTv = new List<AgencyDIDData>();
            List<AgencyDIDData> dfwSpanishTv = new List<AgencyDIDData>();

            ConsolidatedDidData consolidatedDidDatas = new ConsolidatedDidData();
            var agencys = AgencyRepo.GetAgencyAndLatLong().Result.ToList();
            var DidNumbers = DidList.GetDidNumbers();

            foreach (var x in DidNumbers)
            {
                switch (x)
                {
                    case "18003000075":
                        tvEnglishHouston = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18003000075").Result.ToList();
                        break;

                    case "18003000078":
                        tvSpanishHouston = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18003000078").Result.ToList();
                        break;

                    case "18005000027":
                        radioEnglishRgv = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18005000027").Result.ToList();
                        break;

                    case "18005000042":
                        radioSpanisRrgv = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18005000042").Result.ToList();
                        break;

                    case "18006000038":
                        allDigital = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18006000038").Result.ToList();
                        break;

                    case "18009000068":
                        dfwEnglishTv = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18009000068").Result.ToList();
                        break;

                    case "18009000078":
                        dfwSpanishTv = Repository.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear, "18009000078").Result.ToList();
                        break;
                }
            }

            consolidatedDidDatas.allDigital = allDigital;
            consolidatedDidDatas.dfwEnglishTv = dfwEnglishTv;
            consolidatedDidDatas.dfwSpanishTv = dfwSpanishTv;
            consolidatedDidDatas.radioEnglishRgv = radioEnglishRgv;
            consolidatedDidDatas.radioSpanisRrgv = radioSpanisRrgv;
            consolidatedDidDatas.tvEnglishHouston = tvEnglishHouston;
            consolidatedDidDatas.tvSpanishHouston = tvSpanishHouston;

            return consolidatedDidDatas;
        }

        public List<SpectrumLiveData> GetSpectrumLiveData()
        {
            throw new NotImplementedException();
        }

        private List<AgentCountModel> GetHourlyReportLiveData(List<LiveData> lb)
        {
            List<AgentCountModel> ac = new List<AgentCountModel>();
            for (int i = 8; i <= 21; i++)
            {
                var data = lb.Where(m => GetHourdata(m, i)).ToList();
                ac.Add(new AgentCountModel
                {
                    Agencyfee = (decimal)data.Sum(m => m.payFee),
                    Premium = (decimal)data.Sum(m => m.payAmount),
                    Policies = data.Where(m => m.payType.ToLower().Trim() == "down payment").Count(),
                    HourIntervel = (i + " - " + (i + 1)),
                    Transactions = data.Count()
                });
            }

            var AgenctData = Repository.GetConSolidatedAgentloginData();
            foreach (var x in ac)
            {
                var count = AgenctData.Where(m => m.hours == x.HourIntervel).FirstOrDefault();
                if (count != null)
                {
                    x.AgentCount = count.agent_count;
                }
            }

            return ac;
        }


        private List<AgentCountModel> GetHourlyLiveDataAgencyWise(List<LiveData> lb)
        {
            List<AgentCountModel> ac = new List<AgentCountModel>();
            List<string> agencies;
            agencies = lb.Select(m => m.Agency).Distinct().ToList();
            if (agencies != null && !agencies.Any())
            {
                agencies = new List<string>();
                var locations = AgencyRepo.GetAgencyAndLatLong().Result;
                foreach (var x in locations)
                {
                    agencies.Add(x.AgencyName);
                }
            }
            foreach (var x in agencies)
            {
                for (int i = 8; i <= 21; i++)
                {
                    var data = lb.Where(m => GetHourdata(m, i)).ToList();
                    ac.Add(new AgentCountModel
                    {
                        Agencyfee = (decimal)data.Where(m => m.Agency == x).Sum(m => m.payFee),
                        Premium = (decimal)data.Where(m => m.Agency == x).Sum(m => m.payAmount),
                        Policies = data.Where(m => m.payType.ToLower().Trim() == "down payment" && m.Agency == x).Count(),
                        HourIntervel = (i + " - " + (i + 1)),
                        Transactions = data.Count(m => m.Agency == x),
                        Location = x,
                    });
                }
            }
            var AgenctData = Repository.GetAgencywiseAgentloginData();

            foreach (var x in ac)
            {
                var count = AgenctData.Where(m => m.hours == x.HourIntervel && m.location == x.Location).FirstOrDefault();
                if (count != null)
                {
                    x.AgentCount = count.agent_count;
                }
            }
            return ac;
        }



        private bool GetHourdata(LiveData live, int h)
        {
            string[] z = live.payDate.ToString().Split('T');
            int hour = 0;
            if (z.Length >= 1)
            {
                string[] payhour = z[1].Split(':');
                if (payhour.Length > 0)
                    hour = Convert.ToInt32(payhour[0].TrimStart('0'));
            }


            //var utcdate2 = Convert.ToDateTime(live.payDate);
            //int hour = utcdate2.AddMinutes(-240).TimeOfDay.Hours;
            if (hour == h && live.@void == 0)
            {
                return true;
            }
            return false;
        }
    }


}
