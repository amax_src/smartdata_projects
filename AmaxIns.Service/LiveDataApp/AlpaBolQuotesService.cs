﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.LiveDataApp
{
    public class AlpaBolQuotesService : BaseService, IAlpaBolQuotesService
    {
        private IAlpaBolQuotesRepository Repository;
        public AlpaBolQuotesService(IAlpaBolQuotesRepository _Repository, IConfiguration configuration) : base(_Repository, configuration)
        {
            Repository = _Repository;
        }

        public List<MonthModel> GetAlpaCarrierName()
        {
            List<MonthModel> carriers = new List<MonthModel>();

            var carr = Enum.GetValues(typeof(AlpaCarrierName))
                         .Cast<AlpaCarrierName>()
                         .Select(v => v.ToString())
                         .ToList();

            foreach (var x in carr)
            {
                switch (x)
                {
                    case "commonwealthgeneral":
                        carriers.Add(new MonthModel { Id = x, Name = "Common Wealth" });
                        break;
                    case "americanaccess":
                        carriers.Add(new MonthModel { Id = x, Name = "American Access" });
                        break;
                    case "seaharbor":
                        carriers.Add(new MonthModel { Id = x, Name = "SeaHarbor" });
                        break;
                    case "unitedauto":
                        carriers.Add(new MonthModel { Id = x, Name = "United Auto" });
                        break;
                }
            }
            return carriers;
        }

        public async Task<IEnumerable<BOLQuoteData>> GetAlpaDataBolData_Quotes(List<string> date, string state)
        {
            var data = await Repository.GetAlpaDataBolData_Quotes(date, state);
            return data;
        }
    }
}
