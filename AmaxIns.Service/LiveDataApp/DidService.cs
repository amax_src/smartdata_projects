﻿using AmaxIns.DataContract.LiveData.DidConsolatedData;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Service.LiveDataApp
{
   public class DidService : BaseService, IDidService
    {
        private IDidRepository Repository;
        public DidService(IDidRepository _Repository,IConfiguration configuration) : base(_Repository, configuration)
        {
            Repository = _Repository;
        }
        public List<DidNumberData> GetSummaryDIDData(string sday, string smonth, string syear, string eday, string emonth, string eyear)
        {
            return Repository.GetSummaryDIDData(sday, smonth, syear, eday, emonth, eyear);
        }
    }
}
