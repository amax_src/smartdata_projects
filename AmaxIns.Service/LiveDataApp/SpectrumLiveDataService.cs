﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;

namespace AmaxIns.Service.LiveDataApp
{
    public class SpectrumLiveDataService : BaseService, ISpectrumLiveDataService
    {

        private readonly ISpectrumLiveDataRepository _Repository;
        private readonly IAgencyRepository _AgencyRepo;
       

        public SpectrumLiveDataService(ISpectrumLiveDataRepository Repository, IConfiguration configuration, IAgencyRepository AgencyRepo) : base(Repository, configuration)
        {
            _Repository = Repository;
            _AgencyRepo = AgencyRepo;
        }

        public List<SpectrumLiveData> GetSpectrumLiveData()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            var spectrum = _Repository.GetSpectrumLiveData().Result.ToList();
            var agencys = _AgencyRepo.Get().Result.Where(x=>(!x.AgencyName.Contains("Call Center"))).ToList();
           
            foreach (var x in agencys)
            {
                SpectrumLiveData _sd = new SpectrumLiveData();
                _sd.Agencyid = x.AgencyId;
                _sd.Location = x.AgencyName;

                var dataValues = spectrum.Where(m => m.Agencyid == x.AgencyId).FirstOrDefault();
                if(dataValues!=null)
                {
                    _sd.InBoundCalls = dataValues.InBoundCalls;
                    _sd.OutboundCalls = dataValues.OutboundCalls;
                    _sd.totalInBoundsCalls = dataValues.totalInBoundsCalls;
                    _sd.totalOutBoundsCall = dataValues.totalOutBoundsCall;
                    _sd.Date = dataValues.Date;
                }
                lb.Add(_sd);
               
            }
            var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
            return data;
        }
        //public List<SpectrumLiveData> GetSpectrumLiveDataV1()
        //{
        //    List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
        //    var spectrum = _Repository.GetSpectrumLiveDataV1().Result.ToList();
        //    var agencys = _AgencyRepo.Get().Result.Where(x => (!x.AgencyName.Contains("Call Center"))).ToList();

        //    foreach (var x in agencys)
        //    {
        //        SpectrumLiveData _sd = new SpectrumLiveData();
        //        _sd.Agencyid = x.AgencyId;
        //        _sd.Location = x.AgencyName;

        //        var dataValues = spectrum.Where(m => m.Agencyid == x.AgencyId).FirstOrDefault();
        //        if (dataValues != null)
        //        {
        //            _sd.InBoundCalls = dataValues.InBoundCalls;
        //            _sd.OutboundCalls = dataValues.OutboundCalls;
        //            _sd.totalInBoundsCalls = dataValues.totalInBoundsCalls;
        //            _sd.totalOutBoundsCall = dataValues.totalOutBoundsCall;
        //            _sd.Date = dataValues.Date;
        //        }
        //        lb.Add(_sd);

        //    }
        //    var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
        //    return data;
        //}

        //public List<SpectrumLiveData> GetStatsLiveDataAgentWise()
        //{
        //    List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
        //    lb = _Repository.GetStatsLiveDataAgentWise().Result.ToList();
        //    var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
        //    return data;
        //}
        //public List<SpectrumLiveData> GetStatsWeeklyDataAgentWise(int weeklyId)
        //{
        //    List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
        //    lb = _Repository.GetStatsWeeklyDataAgentWise(weeklyId).Result.ToList();
        //    var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
        //    return data;
        //}

        #region SpectrumV2
        public List<SpectrumLiveData> GetSpectrumLiveDataV2()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            var spectrum = _Repository.GetSpectrumLiveDataV2().Result.ToList();
            var agencys = _AgencyRepo.Get().Result.Where(x => (!x.AgencyName.Contains("Call Center"))).ToList();

            foreach (var x in agencys)
            {
                SpectrumLiveData _sd = new SpectrumLiveData();
                _sd.Agencyid = x.AgencyId;
                _sd.Location = x.AgencyName;

                var dataValues = spectrum.Where(m => m.Agencyid == x.AgencyId).FirstOrDefault();
                if (dataValues != null)
                {
                    _sd.InBoundCalls = dataValues.InBoundCalls;
                    _sd.OutboundCalls = dataValues.OutboundCalls;
                    _sd.totalInBoundsCalls = dataValues.totalInBoundsCalls;
                    _sd.totalOutBoundsCall = dataValues.totalOutBoundsCall;
                    _sd.MissedCalls = dataValues.MissedCalls;
                    _sd.CallTransfer = dataValues.CallTransfer;
                    _sd.TransferCallDuration = dataValues.TransferCallDuration;
                    _sd.Date = dataValues.Date;
                }
                lb.Add(_sd);
            }
            var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
            return data;
        }

        public List<SpectrumLiveData> GetStatsLiveDataAgentWiseV2()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            lb = _Repository.GetStatsLiveDataAgentWiseV2().Result.ToList();
            var data = lb.ToList();
            return data;
        }
        public List<AgencyAgentLogLive> GetSpectrumLiveAgentLogV2()
        {
            List<AgencyAgentLogLive> lb = new List<AgencyAgentLogLive>();
            lb = _Repository.GetSpectrumLiveAgentLogV2().Result.ToList();
            var data = lb.ToList();
            return data;
        }

        public List<SpectrumLiveData> GetStatsWeeklyDataAgentWiseV2(SpectrumParam date)
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            lb = _Repository.GetStatsWeeklyDataAgentWiseV2(date).Result.ToList();
            var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
            return data;
        }
        public List<SpectrumAgentCall> GetSpectrumAgentCallDetails(string extension)
        {
            List<SpectrumAgentCall> lb = new List<SpectrumAgentCall>();
            lb = _Repository.GetSpectrumAgentCallDetails(extension).Result.ToList();
            var data = lb.OrderByDescending(m => m.time_start).ThenByDescending(m => m.time_start).ToList();
            return data;
        }

        public List<SpectrumAgentExtensionLog> GetSpectrumLiveAgentExtensionLogV2(string extension)
        {
            List<SpectrumAgentExtensionLog> lb = new List<SpectrumAgentExtensionLog>();
            lb = _Repository.GetSpectrumLiveAgentExtensionLogV2(extension).Result.ToList();
            var data = lb.OrderByDescending(m => m.timestamp).ThenByDescending(m => m.timestamp).ToList();
            return data;
        }

        public List<AgentCallTransfer> GetCallTransferDetails(string extension, int AgencyId)
        {
            List<AgentCallTransfer> lb = new List<AgentCallTransfer>();
            lb = _Repository.GetCallTransferDetails(extension, AgencyId).Result.ToList();
            var data = lb;
            return data;
        }
        public List<SpectrumAgentCall> GetSpectrumExtensionWiseCallDetails(StratusExtensionParam stratusExtension)
        {
            List<SpectrumAgentCall> lb = new List<SpectrumAgentCall>();
            lb = _Repository.GetSpectrumExtensionWiseCallDetails(stratusExtension).Result.ToList();
            var data = lb.ToList();
            return data;
        }

        public string ExportSpectrumCallLocationWise(IEnumerable<SpectrumLiveData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {

                fileName = $"Stratus-Call-Location-Wise-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if (list.Count() > 0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 9].Merge = true;
                        workSheet.Cells[1, 1, 1, 9].Value = "* Stratus Live Call's *";
                        workSheet.Cells[1, 1, 1, 9].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 9].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "AgencyName";
                        workSheet.Cells[2, 2].Value = "Total Inbound Calls";
                        workSheet.Cells[2, 3].Value = "Inbound Answer Calls";
                        workSheet.Cells[2, 4].Value = "Inbound(%)";
                        workSheet.Cells[2, 5].Value = "Total OutBound Calls";
                        workSheet.Cells[2, 6].Value = "OutBound Answer calls";
                        workSheet.Cells[2, 7].Value = "OutBound(%)";
                        workSheet.Cells[2, 8].Value = "Missed Calls";
                        workSheet.Cells[2, 9].Value = "Transfer Calls";

                        workSheet.Cells[2, 1, 2, 9].Style.Font.Bold = true;

                        int row = 2;
                        foreach (SpectrumLiveData obj in list)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.Location;
                            workSheet.Cells[row, 2].Value = obj.totalInBoundsCalls;
                            workSheet.Cells[row, 3].Value = obj.InBoundCalls;
                            workSheet.Cells[row, 4].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 5].Value = obj.totalOutBoundsCall;
                            workSheet.Cells[row, 6].Value = obj.OutboundCalls;
                            workSheet.Cells[row, 7].Value = (obj.totalOutBoundsCall > 0 ? Math.Round(((Convert.ToDecimal(obj.OutboundCalls) / Convert.ToDecimal(obj.totalOutBoundsCall) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 8].Value = obj.MissedCalls;
                            workSheet.Cells[row, 9].Value = obj.CallTransfer;

                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportSpectrumCallExtensionWise(IEnumerable<SpectrumLiveData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {

                fileName = $"Startus-Call-Extension-Wise-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if (list.Count() > 0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 13].Merge = true;
                        workSheet.Cells[1, 1, 1, 13].Value = "* Startus Live Call's *";
                        workSheet.Cells[1, 1, 1, 13].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "AgencyName";
                        workSheet.Cells[2, 2].Value = "Extension";
                        workSheet.Cells[2, 3].Value = "Total Inbound Calls";
                        workSheet.Cells[2, 4].Value = "Inbound Answer Calls";
                        workSheet.Cells[2, 5].Value = "Call Duration";
                        workSheet.Cells[2, 6].Value = "Inbound(%)";
                        workSheet.Cells[2, 7].Value = "Total OutBound Calls";
                        workSheet.Cells[2, 8].Value = "OutBound Answer calls";
                        workSheet.Cells[2, 9].Value = "Call Duration";
                        workSheet.Cells[2, 10].Value = "OutBound(%)";
                        workSheet.Cells[2, 11].Value = "Missed Calls";
                        workSheet.Cells[2, 12].Value = "Total Call Transfer";
                        workSheet.Cells[2, 13].Value = "Call Transfer Duration";

                        workSheet.Cells[2, 1, 2, 13].Style.Font.Bold = true;

                        int row = 2;
                        foreach (SpectrumLiveData obj in list)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.Location;
                            workSheet.Cells[row, 2].Value = obj.extension;
                            workSheet.Cells[row, 3].Value = obj.totalInBoundsCalls;
                            workSheet.Cells[row, 4].Value = obj.InBoundCalls;
                            workSheet.Cells[row, 5].Value = secondsToHms(obj.InboundDuration);
                            workSheet.Cells[row, 6].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 7].Value = obj.totalOutBoundsCall;
                            workSheet.Cells[row, 8].Value = obj.OutboundCalls;
                            workSheet.Cells[row, 9].Value = secondsToHms(obj.InboundDuration);
                            workSheet.Cells[row, 10].Value = (obj.totalOutBoundsCall > 0 ? Math.Round(((Convert.ToDecimal(obj.OutboundCalls) / Convert.ToDecimal(obj.totalOutBoundsCall) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 11].Value = obj.MissedCalls;
                            workSheet.Cells[row, 12].Value = obj.CallTransfer;
                            workSheet.Cells[row, 13].Value = secondsToHms(obj.TransferCallDuration);

                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportLiveStratusAgentLog(IEnumerable<AgencyAgentLogLive> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {

                fileName = $"Startus-Agent-Log-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if (list.Count() > 0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 9].Merge = true;
                        workSheet.Cells[1, 1, 1, 9].Value = "* Agent Log Live *";
                        workSheet.Cells[1, 1, 1, 9].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 9].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "Agency Name";
                        workSheet.Cells[2, 2].Value = "Extension";
                        workSheet.Cells[2, 3].Value = "Login";
                        workSheet.Cells[2, 4].Value = "Logout";
                        workSheet.Cells[2, 5].Value = "Meeting";
                        workSheet.Cells[2, 6].Value = "Break";
                        workSheet.Cells[2, 7].Value = "Lunch";
                        workSheet.Cells[2, 8].Value = "Others";
                        workSheet.Cells[2, 9].Value = "Total";

                        workSheet.Cells[2, 1, 2, 9].Style.Font.Bold = true;

                        int row = 2;
                        foreach (AgencyAgentLogLive obj in list)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.AgencyName;
                            workSheet.Cells[row, 2].Value = obj.extension;
                            workSheet.Cells[row, 3].Value = secondsToHms(obj.Logins);
                            workSheet.Cells[row, 4].Value = secondsToHms(obj.Logoutsec);
                            workSheet.Cells[row, 5].Value = secondsToHms(obj.Meeting);
                            workSheet.Cells[row, 6].Value = secondsToHms(obj.Breaks);
                            workSheet.Cells[row, 7].Value = secondsToHms(obj.Lunch);
                            workSheet.Cells[row, 8].Value = secondsToHms(obj.Other);
                            workSheet.Cells[row, 9].Value = secondsToHms((obj.Logins + obj.Logoutsec + obj.Meeting + obj.Breaks + obj.Lunch + obj.Other));


                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportStratusWeeklyDataAgentWise(IEnumerable<SpectrumLiveData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {

                fileName = $"Location-Extension-Calls-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if (list.Count() > 0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 8].Merge = true;
                        workSheet.Cells[1, 1, 1, 8].Value = "* Location-Extension-Calls *";
                        workSheet.Cells[1, 1, 1, 8].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 8].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "Agency Name";
                        workSheet.Cells[2, 2].Value = "Total Inbound Calls";
                        workSheet.Cells[2, 3].Value = "Inbound Answer Calls";
                        workSheet.Cells[2, 4].Value = "Inbound(%)";
                        workSheet.Cells[2, 5].Value = "Total OutBound calls";
                        workSheet.Cells[2, 6].Value = "OutBound Answer calls";
                        workSheet.Cells[2, 7].Value = "OutBound(%)";
                        workSheet.Cells[2, 8].Value = "Missed Calls";

                        workSheet.Cells[2, 1, 2, 8].Style.Font.Bold = true;

                        int row = 2;
                        foreach (SpectrumLiveData obj in list)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.Location;
                            workSheet.Cells[row, 2].Value = obj.totalInBoundsCalls;
                            workSheet.Cells[row, 3].Value = obj.InBoundCalls;
                            workSheet.Cells[row, 4].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 5].Value = obj.totalOutBoundsCall;
                            workSheet.Cells[row, 6].Value = obj.OutboundCalls;
                            workSheet.Cells[row, 7].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 8].Value = obj.MissedCalls;
                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
        public string ExportStratusDataAgentExtensionWise(IEnumerable<SpectrumLiveData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {

                fileName = $"Location-Extension-Wise-Calls-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));
                if (list.Count() > 0)
                {
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 10].Merge = true;
                        workSheet.Cells[1, 1, 1, 10].Value = "* Location-Extension-Calls *";
                        workSheet.Cells[1, 1, 1, 10].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                        workSheet.Cells[1, 1, 1, 10].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 10].Style.Font.Bold = true;


                        workSheet.Cells[2, 1].Value = "Agency Name";
                        workSheet.Cells[2, 2].Value = "Extension";
                        workSheet.Cells[2, 3].Value = "Total Inbound Calls";
                        workSheet.Cells[2, 4].Value = "Inbound Answer Calls";
                        workSheet.Cells[2, 5].Value = "Inbound(%)";
                        workSheet.Cells[2, 6].Value = "Total OutBound calls";
                        workSheet.Cells[2, 7].Value = "OutBound Answer calls";
                        workSheet.Cells[2, 8].Value = "OutBound(%)";
                        workSheet.Cells[2, 9].Value = "Missed Calls";
                        workSheet.Cells[2, 10].Value = "Date";

                        workSheet.Cells[2, 1, 2, 10].Style.Font.Bold = true;

                        int row = 2;
                        foreach (SpectrumLiveData obj in list)
                        {
                            row++;

                            workSheet.Cells[row, 1].Value = obj.Location;
                            workSheet.Cells[row, 2].Value = obj.extension;
                            workSheet.Cells[row, 3].Value = obj.totalInBoundsCalls;
                            workSheet.Cells[row, 4].Value = obj.InBoundCalls;
                            workSheet.Cells[row, 5].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 6].Value = obj.totalOutBoundsCall;
                            workSheet.Cells[row, 7].Value = obj.OutboundCalls;
                            workSheet.Cells[row, 8].Value = (obj.totalInBoundsCalls > 0 ? Math.Round(((Convert.ToDecimal(obj.InBoundCalls) / Convert.ToDecimal(obj.totalInBoundsCalls) * 100)), 2) : 0) + " %";
                            workSheet.Cells[row, 9].Value = obj.MissedCalls;
                            workSheet.Cells[row, 10].Value = obj.Date;
                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }


        private string secondsToHms(decimal d)
        {

            var h = Math.Floor(d / 3600);
            var m = Math.Floor(d % 3600 / 60);
            var s = Math.Floor(d % 3600 % 60);

            var hDisplay = h > 0 ? h + (h == 1 ? "h, " : "h, ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? "m, " : "m, ") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? "s" : "s") : "";
            return hDisplay + mDisplay + sDisplay;
        }

        public List<SpectrumLiveData> GetStatsLiveDataAgentWiseVSTeam_selV2()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            lb = _Repository.GetStatsLiveDataAgentWiseVSTeam_selV2().Result.ToList();
            var data = lb.ToList();
            return data;
        }
        #endregion

        #region CA_SpectrumV2
        public List<SpectrumLiveData> GetSpectrumLiveDataCAV2()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            var spectrum = _Repository.GetSpectrumLiveDataCAV2().Result.ToList();
            var data = spectrum.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
            return data;
        }

        public List<AgentCallTransfer> GetCallTransferDetailsCA(string extension, int AgencyId)
        {
            List<AgentCallTransfer> lb = new List<AgentCallTransfer>();
            lb = _Repository.GetCallTransferDetailsCA(extension, AgencyId).Result.ToList();
            var data = lb;
            return data;
        }

        public List<SpectrumLiveData> GetStatsLiveDataAgentWiseCAV2()
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            lb = _Repository.GetStatsLiveDataAgentWiseCAV2().Result.ToList();
            var data = lb.ToList();
            return data;
        }

        public List<AgencyAgentLogLive> GetSpectrumLiveAgentLogCAV2()
        {
            List<AgencyAgentLogLive> lb = new List<AgencyAgentLogLive>();
            lb = _Repository.GetSpectrumLiveAgentLogCAV2().Result.ToList();
            var data = lb.ToList();
            return data;
        }
        public List<SpectrumLiveData> GetStatsWeeklyDataAgentWiseCAV2(SpectrumParam date)
        {
            List<SpectrumLiveData> lb = new List<SpectrumLiveData>();
            lb = _Repository.GetStatsWeeklyDataAgentWiseCAV2(date).Result.ToList();
            var data = lb.OrderByDescending(m => m.InBoundCalls).ThenByDescending(m => m.OutboundCalls).ToList();
            return data;
        }
        public List<SpectrumAgentCall> GetSpectrumExtensionWiseCallDetailsCA(StratusExtensionParam stratusExtension)
        {
            List<SpectrumAgentCall> lb = new List<SpectrumAgentCall>();
            lb = _Repository.GetSpectrumExtensionWiseCallDetailsCA(stratusExtension).Result.ToList();
            var data = lb.ToList();
            return data;
        }
        #endregion
    }
}
