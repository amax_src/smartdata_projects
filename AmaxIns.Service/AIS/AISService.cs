﻿using AmaxIns.DataContract.AIS;
using AmaxIns.DataContract.Authentication;
using AmaxIns.RepositoryContract.AIS;
using AmaxIns.ServiceContract.AIS;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.AIS
{
    public class AISService : BaseService, IAISService
    {
        IAISRepository _repository;
        public AISService(IAISRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public Task<IEnumerable<AISModel>> GetAISData()
        {
            return _repository.GetAISData();
        }

        public Task<IEnumerable<AISModel>> GetAISDataCsv()
        {
            return _repository.GetAISDataCsv();
        }
    }
}
