﻿
using AmaxIns.DataContract.Planner;
using AmaxIns.RepositoryContract.Planner;
using AmaxIns.ServiceContract.Planner;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Planner
{
    public class PlannerService : BaseService, IPlannerService
    {
        private readonly IPlannerRepository _repo;
        public PlannerService(IConfiguration configuration, IPlannerRepository plannerRepo) : base(configuration)
        {
            _repo = plannerRepo;
        }

        public async Task<IEnumerable<PlannerActivity>> GetAllActivity()
        {
            return await _repo.GetAllActivity();
        }

        public async Task<IEnumerable<PlannerHour>> GetAllHours()
        {
            return await _repo.GetAllHours();
        }
        public async Task<int> SaveUpdatePlanner(List<PlannerBudget> plannerBudget)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("PlannerBudgetId", typeof(int));
            dt.Columns.Add("AgencyId", typeof(int));
            dt.Columns.Add("PlannerDate", typeof(DateTime));
            dt.Columns.Add("Activity", typeof(string));
            dt.Columns.Add("OthersActivity", typeof(string));
            dt.Columns.Add("PlannedHours", typeof(decimal));
            dt.Columns.Add("PlannedDetails", typeof(string));
            dt.Columns.Add("PlannedBudgetAmount", typeof(decimal));
            dt.Columns.Add("LoginUserId", typeof(int));

            DataRow row = dt.NewRow();
            foreach (PlannerBudget obj in plannerBudget)
            {
                row = dt.NewRow();
                row["PlannerBudgetId"] = obj.PlannerBudgetId;
                row["AgencyId"] = obj.AgencyId; ;
                row["PlannerDate"] = obj.PlannerDate;
                row["Activity"] = obj.Activity;
                row["OthersActivity"] = obj.OtherActivity;
                row["PlannedHours"] = obj.PlannedHours;
                row["PlannedDetails"] = obj.PlannedDetails;
                row["PlannedBudgetAmount"] = obj.PlannedBudgetAmount;
                row["LoginUserId"] = obj.LoginUserId;
                dt.Rows.Add(row);
            }
            return await _repo.SaveUpdatePlanner(dt);
        }

        public async Task<int> DeleteBudgetPlanner(int plannerBudgetId)
        {
            return await _repo.DeleteBudgetPlanner(plannerBudgetId);
        }

        public async Task<IEnumerable<PlannerBudget>> GetAllPlanner(int agencyId, string monthName)
        {
            return await _repo.GetAllPlanner(agencyId, monthName);
        }

        public async Task<IEnumerable<ActualPlanner>> GetActualPlanner(int agencyId, string monthName,string year)
        {
            return await _repo.GetActualPlanner(agencyId, monthName,year);
        }

        public async Task<PlannerActualDetails> GetSingelActual(int plannerBudgetId)
        {
            return await _repo.GetSingelActual(plannerBudgetId);
        }

        public async Task<int> SaveUpdateActual(PlannerActualDetails plannerActualDetails)
        {
            return await _repo.SaveUpdateActual(plannerActualDetails);
        }
        public async Task<IEnumerable<PlannerDashboard>> GetDashboardReport(string monthName,string year)
        {
            return await _repo.GetDashboardReport(monthName,year);
        }
        public async Task<IEnumerable<DailyView>> GetDailyViewReport(string monthName, int agencyId,string year)
        {
            return await _repo.GetDailyViewReport(monthName, agencyId, year);
        }

        public async Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityTotal(string monthName, string year, List<int> agencyId)
        {
            string _agencyId = "-1";
            if (agencyId != null && agencyId.Count>0)
            {
                _agencyId = string.Join(",", agencyId);
            }
            return await _repo.GetPlannerActivityTotal(monthName, year, _agencyId);
        }

        public async Task<IEnumerable<PlannerActivityDetails>> GetPlannerActivityAgencyWise(string monthName, string year)
        {
            return await _repo.GetPlannerActivityAgencyWise(monthName, year);
        }

        public async Task<IEnumerable<DailyView>> GetDataForWeekly(QueryPlanner queryPlanner)
        {
            return await _repo.GetDataForWeekly(queryPlanner);
        }
    }
}
