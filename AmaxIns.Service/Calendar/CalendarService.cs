﻿using AmaxIns.DataContract.Calendar;
using AmaxIns.RepositoryContract.Calendar;
using AmaxIns.ServiceContract.Calendar;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Calendar
{
    public class CalendarService: BaseService, ICalendarService
    {
        private readonly ICalendarRepository _repo;
        public CalendarService(IConfiguration configuration, ICalendarRepository calendarRepo) : base(configuration)
        {
            _repo = calendarRepo;
        }

        public List<CalendarEvents> GetEvents(List<int> agencyId, string month)
        {
            List<CalendarEvents> EventData=new List<CalendarEvents>();
            foreach (var item in agencyId)
            {
                var tempEventData = _repo.GetEvents(item, month).Result.ToList();
                foreach (var x in tempEventData)
                {
                    x.CalendarModels = _repo.GetEventLedgerDetail(x.EventId).Result.ToList();
                    EventData.Add(x);
                }
            }
            return EventData;
        }
        public int AddEvents(CalendarEvents addEvent)
        {
            try
            {
            if(addEvent.EventId>0)
            {
                AddLedgerDetails(addEvent.CalendarModels, addEvent.EventId);
            }
            else
            {
                var eventId = _repo.AddEvents(addEvent);
                AddLedgerDetails(addEvent.CalendarModels, eventId);
            }
                return 1;
            }catch(Exception ex)
            {
                return -1;
            }
        }

        private void AddLedgerDetails(List<CalendarLedgerDetails> ledgerDetailsList,int eventId)
        {
            foreach (var ledger in ledgerDetailsList)
            {
                if (ledger.LedgerId == 0)
                {
                    _repo.AddLedgerDetails(ledger, eventId);
                }
            }
        }
    }
}
