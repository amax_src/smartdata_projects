﻿using AmaxIns.DataContract.Revenue;
using AmaxIns.RepositoryContract.Revenue;
using AmaxIns.ServiceContract.Revenue;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.Revenue
{
    public class RevenueService : BaseService, IRevenueService
    {
        IRevenueRepository _revenueRepository;
        public RevenueService(IRevenueRepository revenueRepository, IConfiguration configuration) : base(configuration)
        {
            this._revenueRepository = revenueRepository;
        }
        public async Task<IEnumerable<RevenueModel>> GetRevenueData()
        {
            var result = await _revenueRepository.GetRevenueData();
            return result;
        }

        public async Task<string> GetDataRefreshDate()
        {
            var result = await _revenueRepository.GetDataRefreshDate();
            return result;
        }

        public async Task<IEnumerable<RevenueDailyModel>> GetRevenueDailyData()
        {
            var result = await _revenueRepository.GetRevenueDailyData();
            return result;
        }

        public async Task<IEnumerable<TierPercentageModel>> GetTierPercentage()
        {
            var result = await _revenueRepository.GetTierPercentage();
            return result;
        }

        public async Task<IEnumerable<string>> Getmonths(string year)
        {
            var result = await _revenueRepository.Getmonths(year);
            return result;
        }
    }
}
