﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.CustomerRetention;
using AmaxIns.DataContract.Enums;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.CustomerRetention;
using AmaxIns.ServiceContract;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AmaxIns.Service
{
    public class CustomerRetentionService: BaseService, ICustomerRetentionService
    {
        private readonly IAgencyRepository _agencyRepository;
        private readonly ICustomerRetentionRepository _customerRetentionRepository;
        private readonly IEnumerable<AgencyModel> _listOfAgency;
        private readonly string _apiToketKey;
        public CustomerRetentionService(IConfiguration configuration, IAgencyRepository agencyRepository, ICustomerRetentionRepository customerRetentionRepository) 
            : base(customerRetentionRepository,configuration)
        {
            //_apiToketKey = GetToken();
            _agencyRepository = agencyRepository;
            _customerRetentionRepository = customerRetentionRepository;
        }

        /*
        public async Task<ActivityListResultModel> GetAllPagingAsync(ActivityQueryModel queryModel)
        {
            var result = await _activityRepository.GetAllPagingAsync(queryModel);

            return result;
        }
        */

        private string GetToken()
        {
            APITokenAccess accessToken = new APITokenAccess();
            string ApiUrl = "https://app.izsurance.com/api/v1/Token/Login";
            string requestbody = @"{'userNameOrEmail':'amaxapi@bermuda.com.tr','password':'ATG567./(!fs45'}";
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(ApiUrl);
            request1.Method = "POST";
            Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(requestbody);
            request1.ContentType = "application/json";
            request1.ContentLength = data.Length;
            Stream stream = request1.GetRequestStream();
            stream.Write(data, 0, data.Length);
            stream.Close();
            HttpWebResponse webResponse1 = (HttpWebResponse)request1.GetResponse();

            StreamReader sr1 = new StreamReader(webResponse1.GetResponseStream());
            string responseText1 = sr1.ReadToEnd();
            accessToken = JsonConvert.DeserializeObject<APITokenAccess>(responseText1);
            return accessToken.result.token;
        }

        private async Task<CustomerRetentionAPIResponse> GetPolicyPremium(string token, string startDate, string endDate,string ExternalAgencyCode,int PageNumber)
        {
            try
            {
                CustomerRetentionAPIResponse policyPremium = new CustomerRetentionAPIResponse();
                string ApiUrl = "https://app.izsurance.com/api/v1/Policy/GetRetentionReport?StartDate="+ startDate + "&EndDate="+ endDate + "&CompanyIdentifier=amax1&"+ ExternalAgencyCode + "FilterType=3&PageNumber="+ PageNumber + "&PageSize=1000";
                Console.WriteLine("Call Method ");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.BaseAddress = new Uri(ApiUrl);
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072 | (SecurityProtocolType)768 | SecurityProtocolType.Tls;//SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | (SecurityProtocolType)12288;
                HttpResponseMessage response = await client.GetAsync(ApiUrl);
                if (response.IsSuccessStatusCode)
                {
                    var httpResponseResult = await response.Content.ReadAsStringAsync();
                    policyPremium = JsonConvert.DeserializeObject<CustomerRetentionAPIResponse>(httpResponseResult);
                }
                return policyPremium;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        
        private IEnumerable<AgencyModel> GetAgencyList(int KeyId, int roleId = 0, List<int> AgencyIdList=null)
        {
            IEnumerable<AgencyModel> listOfAgency = null;
            if (roleId == (int)Role.storemanager)
            {
                listOfAgency = _agencyRepository.GetStoreManagerAgency(KeyId).GetAwaiter().GetResult();
            }
            else if (roleId == (int)Role.zonalmanager)
            {
                listOfAgency = _agencyRepository.GetbyZonalManager(KeyId).GetAwaiter().GetResult();
            }
            else if (roleId == (int)Role.regionalmanager)
            {
                listOfAgency = _agencyRepository.Get(KeyId).GetAwaiter().GetResult();
            }
            else if (roleId == (int)Role.headofdepratment)
            {
                listOfAgency = _agencyRepository.Get().GetAwaiter().GetResult().Where(x=>x.IsActive==true).ToList();
            }
            if(AgencyIdList?.Count>0)
            {
                listOfAgency = listOfAgency.Where(item => AgencyIdList.Contains(item.AgencyId));
            }
            return listOfAgency;
        }
        private List<CustomerRetentions> MergeListObjects(CustomerRetentionAPIResponse policyPremium,  IEnumerable<AgencyModel> listOfAgency)
        {
            List<CustomerRetentions> customerRetentions = new List<CustomerRetentions>();
            if (policyPremium.result!=null && policyPremium.result.results.Count > 0)
            {
                try
                {
                    customerRetentions = policyPremium.result.results.Select(x => new CustomerRetentions()
                    {
                        AmountDue = x.dueAmount,
                        CalledDuration = "0",
                        CalledStatus = true,
                        CalledStatusName = "Yes",
                        CallsMadeCount = 0,
                        ClientName = x.insured,
                        ClientCellPhone = string.IsNullOrEmpty(x.mobilePhone)?x.cellPhone:x.mobilePhone,
                        ClientHomePhone = x.homePhone,
                        ClientWorkPhone = x.workPhone,
                        CompanyName = x.carrierName,
                        DueDate = x.dueDate,
                        EffDate = x.effectiveDate,
                        ExpDate = x.expirationDate,
                        PolicyNumber = x.policyNo,
                        PolicyStatusId = 1,
                        PolicyStatusName = x.status,
                        PolicyTypeId = 1,
                        PolicyTypeName = x.policyType,
                        Paid = x.paid,
                        AgencyCode = x.externalAgencyCode,
                        AgencyName = x.agencyName,
                        ZonalManagers = (listOfAgency.Where(k => k.AgencyId == Convert.ToInt32(x.externalAgencyCode)).Select(j => j.ZonalManagers).FirstOrDefault()),
                        RegionalManagers = (listOfAgency.Where(k => k.AgencyId == Convert.ToInt32(x.externalAgencyCode)).Select(j => j.RegionalManagers).FirstOrDefault())
                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return customerRetentions;
        }

        private List<CustomerRetentions> CustomerRetention(string startDate,string endDate,int KeyId,int roleId=0, List<int> AgencyIdList = null)
        {
            IEnumerable<AgencyModel> listOfAgency=null;
            List<CustomerRetentions> customerRetentions = new List<CustomerRetentions>();
            CustomerRetentionAPIResponse policyPremium = new CustomerRetentionAPIResponse();
            int _pageNumber = 1;int _totalPage = 0;
            listOfAgency = GetAgencyList(KeyId, roleId, AgencyIdList);
            string AgencyList = string.Empty;
            foreach (AgencyModel obj in listOfAgency)
            {
                AgencyList += "ExternalAgencyCode=" + obj.AgencyId + "&";
            }
            string _token = _apiToketKey;
            policyPremium = GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber).GetAwaiter().GetResult();
            customerRetentions = MergeListObjects(policyPremium, listOfAgency);
            _totalPage = policyPremium.result.pageCount;

            if (policyPremium.result.pageCount > 1)
            {
                for (_pageNumber = 2; _pageNumber <= _totalPage; _pageNumber++)
                {
                    policyPremium = GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber).GetAwaiter().GetResult();
                    customerRetentions.AddRange(MergeListObjects(policyPremium, listOfAgency));
                }
            }

            return customerRetentions;
        }

        private async Task<List<CustomerRetentions>> CustomerRetentionHOD(string startDate, string endDate, int KeyId, int roleId = 0, List<int> AgencyIdList = null)
        {
            IEnumerable<AgencyModel> listOfAgency = null;
            List<CustomerRetentions> customerRetentions = new List<CustomerRetentions>();
            CustomerRetentionAPIResponse policyPremium = new CustomerRetentionAPIResponse();
            int _pageNumber = 1; 
            listOfAgency = GetAgencyList(KeyId, roleId, AgencyIdList);
            string AgencyList = string.Empty;
            foreach (AgencyModel obj in listOfAgency)
            {
                AgencyList += "ExternalAgencyCode=" + obj.AgencyId + "&";
            }
            string _token = _apiToketKey;
            policyPremium = GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber).GetAwaiter().GetResult();
            customerRetentions = MergeListObjects(policyPremium, listOfAgency);

            if (policyPremium.result.pageCount > 1)
            {
                List<int> number = new List<int>();

                List<Task<CustomerRetentionAPIResponse>> listOfTasks = new List<Task<CustomerRetentionAPIResponse>>();

                for (_pageNumber = 2; _pageNumber <= policyPremium.result.pageCount; _pageNumber++)
                {
                    listOfTasks.Add(GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber));
                    Thread.Sleep(500);
                }
                var _policyPremium = await Task.WhenAll<CustomerRetentionAPIResponse>(listOfTasks);
                int counterPage = 1;
                foreach (var obj in _policyPremium)
                {
                    counterPage++;
                    if (obj.result != null)
                    {
                        customerRetentions.AddRange(MergeListObjects(obj, listOfAgency));
                    }
                    else
                    {
                        number.Add(counterPage);
                    }
                }

                if (number.Count > 0)
                {
                    listOfTasks = new List<Task<CustomerRetentionAPIResponse>>();
                    foreach (int _number in number)
                    {
                        listOfTasks.Add(GetPolicyPremium(_token, startDate, endDate, AgencyList, _number));
                    }
                    _policyPremium = await Task.WhenAll<CustomerRetentionAPIResponse>(listOfTasks);
                    foreach (var obj in _policyPremium)
                    {

                        if (obj.result != null)
                        {
                            customerRetentions.AddRange(MergeListObjects(obj, listOfAgency));
                        }
                    }
                }
            }

            return customerRetentions;
        }

        private async Task<List<CustomerRetentions>> CustomerRetentionHODPastDue(string startDate, string endDate, int KeyId, int roleId = 0, List<int> AgencyIdList = null)
        {
            IEnumerable<AgencyModel> listOfAgency = null;
            List<CustomerRetentions> customerRetentions = new List<CustomerRetentions>();
            CustomerRetentionAPIResponse policyPremium = new CustomerRetentionAPIResponse();
            int _pageNumber = 1;
            listOfAgency = GetAgencyList(KeyId, roleId, AgencyIdList);
            string AgencyList = string.Empty;
            foreach (AgencyModel obj in listOfAgency)
            {
                AgencyList += "ExternalAgencyCode=" + obj.AgencyId + "&";
            }
            string _token = _apiToketKey;
            policyPremium = GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber).GetAwaiter().GetResult();
            customerRetentions = MergeListObjects(policyPremium, listOfAgency);

            if (policyPremium.result.pageCount > 1)
            {
                List<int> number = new List<int>();

                List<Task<CustomerRetentionAPIResponse>> listOfTasks = new List<Task<CustomerRetentionAPIResponse>>();

                for (_pageNumber = 2; _pageNumber <= policyPremium.result.pageCount; _pageNumber++)
                {
                    listOfTasks.Add(GetPolicyPremium(_token, startDate, endDate, AgencyList, _pageNumber));
                    Thread.Sleep(500);
                }
                var _policyPremium = await Task.WhenAll<CustomerRetentionAPIResponse>(listOfTasks);
                int counterPage = 1;
                foreach (var obj in _policyPremium)
                {
                    counterPage++;
                    if (obj.result != null)
                    {
                        customerRetentions.AddRange(MergeListObjects(obj, listOfAgency));
                    }
                    else
                    {
                        number.Add(counterPage);
                    }
                }
                if (number.Count > 0)
                {
                    listOfTasks = new List<Task<CustomerRetentionAPIResponse>>();
                    foreach (int _number in number)
                    {
                        listOfTasks.Add(GetPolicyPremium(_token, startDate, endDate, AgencyList, _number));
                    }
                    _policyPremium = await Task.WhenAll<CustomerRetentionAPIResponse>(listOfTasks);
                    foreach (var obj in _policyPremium)
                    {
                        if (obj.result != null)
                        {
                            customerRetentions.AddRange(MergeListObjects(obj, listOfAgency));
                        }
                    }
                }
            }
            return customerRetentions;
        }

        private string GetStartDate(int days)
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime CstDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone).AddDays(days);

            string _Day = (CstDate.Day < 10 ? "0" + (CstDate.Day).ToString() : (CstDate.Day).ToString());
            string _Month = (CstDate.Month < 10 ? "0" + (CstDate.Month).ToString() : (CstDate.Month).ToString());
            string _Year = CstDate.Year.ToString();
            string startDateString = _Year + "-" + _Month + "-" + _Day;

            return startDateString;
        }

        private string GetDateUTC(DateTime? date)
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime CstDate = (DateTime)date;
            string _Day = (CstDate.Day < 10 ? "0" + (CstDate.Day).ToString() : (CstDate.Day).ToString());
            string _Month = (CstDate.Month < 10 ? "0" + (CstDate.Month).ToString() : (CstDate.Month).ToString());
            string _Year = CstDate.Year.ToString();
            string startDateString = _Year + "-" + _Month + "-" + _Day;
            return startDateString;
        }
        private string GetDateUTC(DateTime? date,int days)
        {
            TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime CstDate = (DateTime)date;
            CstDate = CstDate.AddDays(days);
            string _Day = (CstDate.Day < 10 ? "0" + (CstDate.Day).ToString() : (CstDate.Day).ToString());
            string _Month = (CstDate.Month < 10 ? "0" + (CstDate.Month).ToString() : (CstDate.Month).ToString());
            string _Year = CstDate.Year.ToString();
            string startDateString = _Year + "-" + _Month + "-" + _Day;
            return startDateString;
        }

        private List<CustomerRetentionCountForZone> CustomerRetentionForZone(List<CustomerRetentions> listCustomerRetentions)
        {
            List<CustomerRetentionCountForZone> list = null;
            if(listCustomerRetentions.Any())
            {
                var _jdtemp = from e in listCustomerRetentions
                              group e by new
                              {
                                  e.DueDate,
                                  e.AgencyName
                              } into g
                              select new { DueDate = g.Key.DueDate, PaymentsDue = g.Count(o => o.Paid == "No"), PaymentsRecived = g.Count(o => o.Paid == "Yes"), LocationName = g.Key.AgencyName };

                try
                {
                    if(_jdtemp.Any())
                    {
                        list = _jdtemp.Select(x => new CustomerRetentionCountForZone()
                        {
                            DueDate = Convert.ToDateTime(x.DueDate),
                            PaymentDueCount = x.PaymentsDue,
                            PaymentReceivedCount = x.PaymentsRecived,
                            LocationName = x.LocationName,
                            Difference = (x.PaymentsDue - x.PaymentsRecived)
                        }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return list;
        }

        private List<CustomerRetentionCountForRegion> CustomerRetentionForRegional(List<CustomerRetentions> listCustomerRetentions)
        {
            List<CustomerRetentionCountForRegion> list = null;
            if (listCustomerRetentions.Any())
            {
                var _jdtemp = from e in listCustomerRetentions
                              group e by new
                              {
                                  e.DueDate,
                                  e.ZonalManagers
                              } into g
                              select new { DueDate = g.Key.DueDate, PaymentsDue = g.Count(o => o.Paid == "No"), PaymentsRecived = g.Count(o => o.Paid == "Yes"), ZoneManagerName = g.Key.ZonalManagers };

                try
                {
                    if (_jdtemp.Any())
                    {
                        list = _jdtemp.Select(x => new CustomerRetentionCountForRegion()
                        {
                            DueDate = Convert.ToDateTime(x.DueDate),
                            PaymentDueCount = x.PaymentsDue,
                            PaymentReceivedCount = x.PaymentsRecived,
                            ZoneManagerName = x.ZoneManagerName,
                            Difference = (x.PaymentsDue - x.PaymentsRecived)
                        }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return list;
        }

        private List<CustomerRetentionCountForHod> CustomerRetentionForHOD(List<CustomerRetentions> listCustomerRetentions)
        {
            List<CustomerRetentionCountForHod> list = null;
            if (listCustomerRetentions.Any())
            {
                var _jdtemp = from e in listCustomerRetentions
                              group e by new
                              {
                                  e.DueDate,
                                  e.RegionalManagers
                              } into g
                              select new { DueDate = g.Key.DueDate, PaymentsDue = g.Count(o => o.Paid == "No"), PaymentsRecived = g.Count(o => o.Paid == "Yes"), RegionalManagers = g.Key.RegionalManagers };

                try
                {
                    if (_jdtemp.Any())
                    {
                        list = _jdtemp.Select(x => new CustomerRetentionCountForHod()
                        {
                            DueDate = Convert.ToDateTime(x.DueDate),
                            PaymentDueCount = x.PaymentsDue,
                            PaymentReceivedCount = x.PaymentsRecived,
                            RegionManagerName = x.RegionalManagers,
                            Difference = (x.PaymentsDue - x.PaymentsRecived)
                        }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return list;
        }

        public async Task<ListResultModel> GetAllPagingAsync(ListQueryModel queryModel)
        {
            var result = await Task.Run(() => {
                string startDateString = string.Empty;
                string endDateString = string.Empty;
                IEnumerable<CustomerRetentions> list = null;
                if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueToday)
                {
                     startDateString = GetStartDate(0);
                     endDateString = GetStartDate(0);
                    //list = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId,(int)Role.storemanager);
                    list = _customerRetentionRepository.GetStoreManagersReports(startDateString, endDateString, queryModel.StoreManagerUserId, queryModel.AgencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueNext7Days)
                {
                     startDateString = GetStartDate(1);
                     endDateString = GetStartDate(7);
                    //list = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId, (int)Role.storemanager);
                    list = _customerRetentionRepository.GetStoreManagersReports(startDateString, endDateString, queryModel.StoreManagerUserId, queryModel.AgencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.PastDue)
                {
                     startDateString = GetStartDate(-9);
                     endDateString = GetStartDate(-1);
                    //list = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId, (int)Role.storemanager);
                    list = _customerRetentionRepository.GetStoreManagersReports(startDateString, endDateString, queryModel.StoreManagerUserId, queryModel.AgencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.Canceled)
                {

                }

                var query = list.AsQueryable(); 

                if (!string.IsNullOrEmpty(queryModel.SortBy) && !string.IsNullOrEmpty(queryModel.SortDir))
                {
                    query = query.OrderBy(string.Format("{0} {1}", queryModel.SortBy, queryModel.SortDir));
                }

                if (queryModel.PageSize > 0)
                {
                    query = query.Skip(queryModel.RowOffset).Take(queryModel.PageSize);
                }

                var resultTask = new ListResultModel();
                resultTask.Data = query.ToList();
                resultTask.IsSuccess = true;
                resultTask.ResultMessage = "";
                resultTask.TotalRecordCount = list.Count();

                return resultTask;
            });

            return result;
        }

        public async Task<string> GetAllPagingExportAsync(ListQueryModel queryModel,string pathRoot)
        {
            var result = await Task.Run(() =>
              {
                  string fileName=string.Empty;
                  ListResultModel listResultModel = new ListResultModel();
                  var  listResultModels=GetAllPagingAsync(queryModel).GetAwaiter().GetResult();

                  try
                  {
                      fileName = $"Customer-Retention-For-Store-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                      FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                      using (var package = new ExcelPackage(file))
                      {
                          var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                          workSheet.Cells[1, 1, 1, 12].Merge = true;
                          workSheet.Cells[1, 1, 1, 12].Value = "** Customer-Retention-For-Store **";
                          workSheet.Cells[1, 1, 1, 12].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                          workSheet.Cells[1, 1, 1, 12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                          workSheet.Cells[1, 1, 1, 12].Style.Font.Bold = true;

                          workSheet.Cells[2, 1].Value = "Due Date";
                          workSheet.Cells[2, 2].Value = "Client Name";
                          workSheet.Cells[2, 3].Value = "Policy #";
                          workSheet.Cells[2, 4].Value = "Policy Status";
                          workSheet.Cells[2, 5].Value = "Company Name";
                          workSheet.Cells[2, 6].Value = "Policy Type";
                          workSheet.Cells[2, 7].Value = "Eff Date";
                          workSheet.Cells[2, 8].Value = "Exp Date";
                          workSheet.Cells[2, 9].Value = "Amount Due";
                          workSheet.Cells[2, 10].Value = "Client Cell Phone #";
                          workSheet.Cells[2, 11].Value = "Client Home Phone #";
                          workSheet.Cells[2, 12].Value = "Client Work Phone #";
                          workSheet.Cells["A2:L2"].AutoFilter = true;
                          workSheet.Row(2).Style.Fill.PatternType = ExcelFillStyle.Solid;
                          workSheet.Row(2).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                          workSheet.Row(2).Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"));
                          workSheet.Cells[2, 1, 2, 12].Style.Font.Bold = true;

                          int row = 2;
                          foreach (CustomerRetentions obj in listResultModels.Data)
                          {
                              row++;
                              if(row % 2 == 0)
                              {
                                  workSheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                                  workSheet.Row(row).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#B1C7E1"));
                              }
                                 
                              workSheet.Cells[row, 1].Value = obj.DueDate.ToString("MM/dd/yyyy");
                              workSheet.Cells[row, 2].Value = obj.ClientName;
                              workSheet.Cells[row, 3].Value = obj.PolicyNumber;
                              workSheet.Cells[row, 4].Value = obj.PolicyStatusName;
                              workSheet.Cells[row, 5].Value = obj.CompanyName;
                              workSheet.Cells[row, 6].Value = obj.PolicyTypeName;
                              workSheet.Cells[row, 7].Value = obj.EffDate.ToString("MM/dd/yyyy"); ;
                              workSheet.Cells[row, 8].Value = obj.ExpDate.ToString("MM/dd/yyyy"); ;
                              workSheet.Cells[row, 9].Value = obj.AmountDue;
                              workSheet.Cells[row, 10].Value = obj.ClientCellPhone;
                              workSheet.Cells[row, 11].Value = obj.ClientHomePhone;
                              workSheet.Cells[row, 12].Value = obj.ClientWorkPhone;

                          }
                          workSheet.Cells.AutoFitColumns();
                          package.Save();
                      }
                  }
                  catch (Exception ex)
                  {
                      throw;
                  }

                  return fileName;
              });
            return result;
        }


        public async Task<CountListForZoneResultModel> GetAllCountForZonePagingAsync(CountListForZoneQueryModel queryModel)
        {
            var result = await Task.Run(() =>
            {

                IEnumerable<CustomerRetentionCountForZone> list = null;

                string startDateString = string.Empty;
                string endDateString = string.Empty;
                string _agencyId = string.Empty;
                if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueToday)
                {
                    startDateString = GetStartDate(0);
                    endDateString = GetStartDate(0);
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 0);
                        endDateString = GetDateUTC(queryModel.DueDate, 0);
                    }

                    if (queryModel.StoreManagerUserId > 0 && queryModel.ZoneManagerUserId == 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId, (int)Role.storemanager);
                        list = _customerRetentionRepository.StoreManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.StoreManagerUserId).GetAwaiter().GetResult();
                    }
                    else if (queryModel.StoreManagerUserId == 0 && queryModel.ZoneManagerUserId > 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.ZoneManagerUserId, (int)Role.zonalmanager, queryModel.AgencyIdList);
                        _agencyId = string.Join(",", queryModel.AgencyIdList);
                        list = _customerRetentionRepository.ZonalManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.ZoneManagerUserId, _agencyId).GetAwaiter().GetResult();
                    }
                    //list = CustomerRetentionForZone(listCustomerRetentions);
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueNext7Days)
                {
                    startDateString = GetStartDate(1);
                    endDateString = GetStartDate(7);

                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 1);
                        endDateString = GetDateUTC(queryModel.DueDate, 7);
                    }

                    if (queryModel.StoreManagerUserId > 0 && queryModel.ZoneManagerUserId == 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId, (int)Role.storemanager);
                        list = _customerRetentionRepository.StoreManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.StoreManagerUserId).GetAwaiter().GetResult();
                    }
                    else if (queryModel.StoreManagerUserId == 0 && queryModel.ZoneManagerUserId > 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.ZoneManagerUserId, (int)Role.zonalmanager, queryModel.AgencyIdList);
                        _agencyId = string.Join(",", queryModel.AgencyIdList);
                        list = _customerRetentionRepository.ZonalManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.ZoneManagerUserId, _agencyId).GetAwaiter().GetResult();
                    }
                    //list = CustomerRetentionForZone(listCustomerRetentions);
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.PastDue)
                {
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, -9);
                        endDateString = GetDateUTC(queryModel.DueDate, -1);
                    }
                    if (queryModel.StoreManagerUserId > 0 && queryModel.ZoneManagerUserId == 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.StoreManagerUserId, (int)Role.storemanager);
                        list = _customerRetentionRepository.StoreManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.StoreManagerUserId).GetAwaiter().GetResult();
                    }
                    else if (queryModel.StoreManagerUserId == 0 && queryModel.ZoneManagerUserId > 0)
                    {
                        //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.ZoneManagerUserId, (int)Role.zonalmanager,queryModel.AgencyIdList);
                        _agencyId = string.Join(",", queryModel.AgencyIdList);
                        list = _customerRetentionRepository.ZonalManagerCustomerRetentionCountForZone(startDateString, endDateString, queryModel.ZoneManagerUserId, _agencyId).GetAwaiter().GetResult();
                    }
                    //list = CustomerRetentionForZone(listCustomerRetentions);
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.Canceled)
                {

                }
                var query = list.AsQueryable();

                if (!string.IsNullOrEmpty(queryModel.SortBy) && !string.IsNullOrEmpty(queryModel.SortDir))
                {
                    query = query.OrderBy(string.Format("{0} {1}", queryModel.SortBy, queryModel.SortDir));
                }

                if (queryModel.PageSize > 0)
                {
                    query = query.Skip(queryModel.RowOffset).Take(queryModel.PageSize);
                }

                var resultTask = new CountListForZoneResultModel();
                resultTask.Data = query.ToList();
                resultTask.IsSuccess = true;
                resultTask.ResultMessage = "";
                resultTask.TotalRecordCount = list.Count();

                return resultTask;
            });

            return result;
        }

        public async Task<string> GetAllCountForZonePagingAsyncExport(CountListForZoneQueryModel queryModel, string pathRoot)
        {
            var result = await Task.Run(() =>
            {
                string fileName = string.Empty;
                
                var listResultModels = GetAllCountForZonePagingAsync(queryModel).GetAwaiter().GetResult();

                try
                {
                    fileName = $"Customer-Retention-For-Zone-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                    FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 5].Merge = true;
                        workSheet.Cells[1, 1, 1, 5].Value = "** Customer Retention For Zone **";
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Cells[1, 1, 1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Bold = true;

                        workSheet.Cells[2, 1].Value = "Due Date";
                        workSheet.Cells[2, 2].Value = "Location Name";
                        workSheet.Cells[2, 3].Value = "# Payments Due";
                        workSheet.Cells[2, 4].Value = "# Payments Received";
                        workSheet.Cells[2, 5].Value = "Difference";
                      
                        workSheet.Cells["A2:E2"].AutoFilter = true;
                        workSheet.Row(2).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Row(2).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Row(2).Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"));
                        workSheet.Cells[2, 1, 2, 5].Style.Font.Bold = true;

                        int row = 2;
                        foreach (CustomerRetentionCountForZone obj in listResultModels.Data)
                        {
                            row++;
                            if (row % 2 == 0)
                            {
                                workSheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Row(row).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#B1C7E1"));
                            }

                            workSheet.Cells[row, 1].Value = obj.DueDate.ToString("MM/dd/yyyy");
                            workSheet.Cells[row, 2].Value = obj.LocationName;
                            workSheet.Cells[row, 3].Value = obj.PaymentDueCount;
                            workSheet.Cells[row, 4].Value = obj.PaymentReceivedCount;
                            workSheet.Cells[row, 5].Value = obj.Difference;

                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

                return fileName;
            });
            return result;
        }
        public async Task<CountListForRegionResultModel> GetAllCountForRegionPagingAsync(CountListForRegionQueryModel queryModel)
        {
            var result = await Task.Run(() => {
                //List<CustomerRetentions> listCustomerRetentions = null;
                IEnumerable<CustomerRetentionCountForRegion> list = null;
                string startDateString = string.Empty;
                string endDateString = string.Empty;
                string _agencyId = string.Empty;
                // DueDateFilterTypeId
                if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueToday)
                {
                     startDateString = GetStartDate(0);
                     endDateString = GetStartDate(0);
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 0);
                        endDateString = GetDateUTC(queryModel.DueDate, 0);
                    }
                    //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.RegionManagerUserId, (int)Role.regionalmanager, queryModel.AgencyIdList);
                    //list = CustomerRetentionForRegional(listCustomerRetentions);
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.RegionalManagerCustomerRetentionCount(startDateString, endDateString, queryModel.RegionManagerUserId, _agencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueNext7Days)
                {
                     startDateString = GetStartDate(1);
                     endDateString = GetStartDate(7);
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 1);
                        endDateString = GetDateUTC(queryModel.DueDate, 7);
                    }
                    //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.RegionManagerUserId, (int)Role.regionalmanager, queryModel.AgencyIdList);
                    //list = CustomerRetentionForRegional(listCustomerRetentions);
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.RegionalManagerCustomerRetentionCount(startDateString, endDateString, queryModel.RegionManagerUserId, _agencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.PastDue)
                {
                     startDateString = GetStartDate(9);
                     endDateString = GetStartDate(-1);

                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate,-9);
                        endDateString = GetDateUTC(queryModel.DueDate,-1);
                    }
                    //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.RegionManagerUserId, (int)Role.regionalmanager, queryModel.AgencyIdList);
                    //list = CustomerRetentionForRegional(listCustomerRetentions);
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.RegionalManagerCustomerRetentionCount(startDateString, endDateString, queryModel.RegionManagerUserId, _agencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.Canceled)
                {

                }

                var query = list.AsQueryable(); 

                if (!string.IsNullOrEmpty(queryModel.SortBy) && !string.IsNullOrEmpty(queryModel.SortDir))
                {
                    query = query.OrderBy(string.Format("{0} {1}", queryModel.SortBy, queryModel.SortDir));
                }

                if (queryModel.PageSize > 0)
                {
                    query = query.Skip(queryModel.RowOffset).Take(queryModel.PageSize);
                }

                var resultTask = new CountListForRegionResultModel();
                resultTask.Data = query.ToList();
                resultTask.IsSuccess = true;
                resultTask.ResultMessage = "";
                resultTask.TotalRecordCount = list.Count();

                return resultTask;
            });

            return result;
        }

        public async Task<string> GetAllCountForRegionPagingAsyncExport(CountListForRegionQueryModel queryModel, string pathRoot)
        {
            var result = await Task.Run(() =>
            {
                string fileName = string.Empty;

                var listResultModels = GetAllCountForRegionPagingAsync(queryModel).GetAwaiter().GetResult();

                try
                {
                    fileName = $"Customer-Retention-For-Region-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                    FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 5].Merge = true;
                        workSheet.Cells[1, 1, 1, 5].Value = "** Customer Retention For Region **";
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Cells[1, 1, 1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Bold = true;

                        workSheet.Cells[2, 1].Value = "Due Date";
                        workSheet.Cells[2, 2].Value = "ZM Name";
                        workSheet.Cells[2, 3].Value = "# Payments Due";
                        workSheet.Cells[2, 4].Value = "# Payments Received";
                        workSheet.Cells[2, 5].Value = "Difference";

                        workSheet.Cells["A2:E2"].AutoFilter = true;
                        workSheet.Row(2).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Row(2).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Row(2).Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"));
                        workSheet.Cells[2, 1, 2, 5].Style.Font.Bold = true;

                        int row = 2;
                        foreach (CustomerRetentionCountForRegion obj in listResultModels.Data)
                        {
                            row++;
                            if (row % 2 == 0)
                            {
                                workSheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Row(row).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#B1C7E1"));
                            }

                            workSheet.Cells[row, 1].Value = obj.DueDate.ToString("MM/dd/yyyy");
                            workSheet.Cells[row, 2].Value = obj.ZoneManagerName;
                            workSheet.Cells[row, 3].Value = obj.PaymentDueCount;
                            workSheet.Cells[row, 4].Value = obj.PaymentReceivedCount;
                            workSheet.Cells[row, 5].Value = obj.Difference;

                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

                return fileName;
            });
            return result;
        }
        public async Task<CountListForHodResultModel> GetAllCountForHodPagingAsync(CountListForHodQueryModel queryModel)
        {
            var result = await Task.Run(() => {
                //List<CustomerRetentions> listCustomerRetentions = null;
                IEnumerable<CustomerRetentionCountForHod> list = null;
                string startDateString = string.Empty;
                string endDateString = string.Empty;
                string _agencyId = string.Empty;
                // DueDateFilterTypeId
                if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueToday)
                {
                     startDateString = GetStartDate(0);
                     endDateString = GetStartDate(0);
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 0);
                        endDateString = GetDateUTC(queryModel.DueDate, 0);
                    }
                    //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.HodUserId, (int)Role.headofdepratment, queryModel.AgencyIdList);
                    //list = CustomerRetentionForHOD(listCustomerRetentions);
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.HODCustomerRetentionCount(startDateString, endDateString, _agencyId).GetAwaiter().GetResult();

                    //list = _listCustomerRetentionCountForHod;
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.DueNext7Days)
                {
                     startDateString = GetStartDate(1);
                     endDateString = GetStartDate(7);
                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate, 1);
                        endDateString = GetDateUTC(queryModel.DueDate, 7);
                    }
                    //listCustomerRetentions = CustomerRetention(startDateString, endDateString, queryModel.HodUserId, (int)Role.headofdepratment, queryModel.AgencyIdList);
                    //listCustomerRetentions = CustomerRetentionHOD(startDateString, endDateString, queryModel.HodUserId, (int)Role.headofdepratment, queryModel.AgencyIdList).GetAwaiter().GetResult(); 
                    //list = CustomerRetentionForHOD(listCustomerRetentions);
                    //list = _listCustomerRetentionCountForHodDueNextSevenDays;
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.HODCustomerRetentionCount(startDateString, endDateString, _agencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.PastDue)
                {
                     startDateString = GetStartDate(-7);
                     endDateString = GetStartDate(-1);

                    if (queryModel.DueDate != null)
                    {
                        startDateString = GetDateUTC(queryModel.DueDate,-7);
                        endDateString = GetDateUTC(queryModel.DueDate,-1);
                    }
                    //listCustomerRetentions = CustomerRetentionHODPastDue(startDateString, endDateString, queryModel.HodUserId, (int)Role.headofdepratment, queryModel.AgencyIdList).GetAwaiter().GetResult();
                    //list = CustomerRetentionForHOD(listCustomerRetentions);
                    _agencyId = string.Join(",", queryModel.AgencyIdList);
                    list = _customerRetentionRepository.HODCustomerRetentionCount(startDateString, endDateString, _agencyId).GetAwaiter().GetResult();
                }
                else if (queryModel.DueDateFilterTypeId == (int)DueDateFilterTypeEnum.Canceled)
                {

                }
                var query = list.AsQueryable(); 

                if (!string.IsNullOrEmpty(queryModel.SortBy) && !string.IsNullOrEmpty(queryModel.SortDir))
                {
                    query = query.OrderBy(string.Format("{0} {1}", queryModel.SortBy, queryModel.SortDir));
                }

                if (queryModel.PageSize > 0)
                {
                    query = query.Skip(queryModel.RowOffset).Take(queryModel.PageSize);
                }

                var resultTask = new CountListForHodResultModel();
                resultTask.Data = query.ToList();
                resultTask.IsSuccess = true;
                resultTask.ResultMessage = "";
                resultTask.TotalRecordCount = list.Count();

                return resultTask;
            });

            return result;
        }

        public async Task<string> GetAllCountForHodPagingAsyncExport(CountListForHodQueryModel queryModel, string pathRoot)
        {
            var result = await Task.Run(() =>
            {
                string fileName = string.Empty;

                var listResultModels = GetAllCountForHodPagingAsync(queryModel).GetAwaiter().GetResult();

                try
                {
                    fileName = $"Customer-Retention-For-Region-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                    FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells[1, 1, 1, 5].Merge = true;
                        workSheet.Cells[1, 1, 1, 5].Value = "** Customer Retention For Region **";
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Cells[1, 1, 1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        workSheet.Cells[1, 1, 1, 5].Style.Font.Bold = true;

                        workSheet.Cells[2, 1].Value = "Due Date";
                        workSheet.Cells[2, 2].Value = "ROM Name";
                        workSheet.Cells[2, 3].Value = "# Payments Due";
                        workSheet.Cells[2, 4].Value = "# Payments Received";
                        workSheet.Cells[2, 5].Value = "Difference";

                        workSheet.Cells["A2:E2"].AutoFilter = true;
                        workSheet.Row(2).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Row(2).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF4F81BD"));
                        workSheet.Row(2).Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"));
                        workSheet.Cells[2, 1, 2, 5].Style.Font.Bold = true;

                        int row = 2;
                        foreach (CustomerRetentionCountForHod obj in listResultModels.Data)
                        {
                            row++;
                            if (row % 2 == 0)
                            {
                                workSheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                                workSheet.Row(row).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#B1C7E1"));
                            }

                            workSheet.Cells[row, 1].Value = obj.DueDate.ToString("MM/dd/yyyy");
                            workSheet.Cells[row, 2].Value = obj.RegionManagerName;
                            workSheet.Cells[row, 3].Value = obj.PaymentDueCount;
                            workSheet.Cells[row, 4].Value = obj.PaymentReceivedCount;
                            workSheet.Cells[row, 5].Value = obj.Difference;

                        }
                        workSheet.Cells.AutoFitColumns();
                        package.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

                return fileName;
            });
            return result;
        }

    }
}
