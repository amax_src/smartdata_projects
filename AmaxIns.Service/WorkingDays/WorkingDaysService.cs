﻿using AmaxIns.DataContract.WorkingDays;
using AmaxIns.RepositoryContract.WorkingDays;
using AmaxIns.ServiceContract.WorkingDays;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.WorkingDays
{
    public class WorkingDaysService : BaseService, IWorkingDaysService
    {
        private readonly IWorkingDaysRepository _workingDaysRepository;
        public WorkingDaysService(IConfiguration configuration, IWorkingDaysRepository workingDaysRepository) : base(configuration)
        {
            this._workingDaysRepository = workingDaysRepository;
        }
        public Task<IEnumerable<WorkingdayModel>> GetWorkingDaysOfMonth()
        {
            return _workingDaysRepository.GetWorkingDaysOfMonth();
        }
    }
}
