﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AmaxIns.Service.Utility
{
    public static class EmailUtilty
    {
        public static bool SendEmail(string smtpserver,  string password, string To, string from, string subject, string body,int port )
        {

            try
            {
                SmtpClient client = new SmtpClient(smtpserver);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(from, password);
                client.Port = port;
                client.EnableSsl = true;
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(from,"A-MAX");
                mailMessage.To.Add(To);
                mailMessage.Body = body;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml=true;
                client.Send(mailMessage);
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
