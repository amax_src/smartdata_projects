﻿using AmaxIns.DataContract.AMAX30DB;
using AmaxIns.RepositoryContract.AMAX30DB;
using AmaxIns.ServiceContract.AMAX30DB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.AMAX30DB
{
    public class AgencyInfoService : BaseService, IAgencyInfoService
    {
        private readonly IAgencyInfoRepository _repo;
        public AgencyInfoService(IConfiguration configuration, IAgencyInfoRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }

        public async Task<CountListForAgencyInfoResultModel> GetAgencyInfo(CountListAgencyQueryModel queryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForAgencyInfoResultModel();
                try
                {
                    IEnumerable<AMAX30AncyInfoModel> list = null;
                    list = _repo.GetAgencyInfo(queryModel.agencyId, queryModel.agencyName).Result;

                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        queryModel.RowOffset = queryModel.RowOffset == 0 ? 1 : queryModel.RowOffset;
                        queryModel.PageSize = queryModel.PageSize == 0 ? 1000 : queryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / queryModel.PageSize);

                        }
                        if (queryModel.PageSize > 0)
                        {
                            query = query.Skip((queryModel.RowOffset - 1) * queryModel.PageSize).Take(queryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = queryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = queryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForAgencyInfoResultModel> GetAgencyInfo_V1(CountListAgencyQueryModel queryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForAgencyInfoResultModel();
                try
                {
                    IEnumerable<AMAX30AncyInfoModel> list = null;
                    list = _repo.GetAgencyInfo(queryModel.agencyId, queryModel.agencyName).Result;

                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        queryModel.RowOffset = 1;// queryModel.RowOffset == 0 ? 1 : queryModel.RowOffset;
                        queryModel.PageSize = int.MaxValue;// queryModel.PageSize == 0 ? int.MaxValue : queryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / queryModel.PageSize);

                        }
                        if (queryModel.PageSize > 0)
                        {
                            query = query.Skip((queryModel.RowOffset - 1) * queryModel.PageSize).Take(queryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = 0;// queryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = queryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForAgentInfoResultModel> GetAgentInfo(CountListAgentQueryModel countListAgentQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForAgentInfoResultModel();
                try
                {
                    IEnumerable<Amax30AgentInfoModel> list = null;
                    list = _repo.GetAgentInfo(countListAgentQueryModel.agentName, countListAgentQueryModel.email).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListAgentQueryModel.RowOffset = countListAgentQueryModel.RowOffset == 0 ? 1 : countListAgentQueryModel.RowOffset;
                        countListAgentQueryModel.PageSize = countListAgentQueryModel.PageSize == 0 ? 1000 : countListAgentQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListAgentQueryModel.PageSize);

                        }
                        if (countListAgentQueryModel.PageSize > 0)
                        {
                            //query = query.Skip(countListAgentQueryModel.RowOffset).Take(countListAgentQueryModel.PageSize);
                            query = query.Skip((countListAgentQueryModel.RowOffset - 1) * countListAgentQueryModel.PageSize).Take(countListAgentQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = countListAgentQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListAgentQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForAgentInfoResultModel> GetAgentInfo_V1(CountListAgentQueryModel countListAgentQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForAgentInfoResultModel();
                try
                {
                    IEnumerable<Amax30AgentInfoModel> list = null;
                    list = _repo.GetAgentInfo(countListAgentQueryModel.agentName, countListAgentQueryModel.email).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListAgentQueryModel.RowOffset = 1;// countListAgentQueryModel.RowOffset == 0 ? 1 : countListAgentQueryModel.RowOffset;
                        countListAgentQueryModel.PageSize = int.MaxValue;// countListAgentQueryModel.PageSize == 0 ? int.MaxValue : countListAgentQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListAgentQueryModel.PageSize);

                        }
                        if (countListAgentQueryModel.PageSize > 0)
                        {
                            //query = query.Skip(countListAgentQueryModel.RowOffset).Take(countListAgentQueryModel.PageSize);
                            query = query.Skip((countListAgentQueryModel.RowOffset - 1) * countListAgentQueryModel.PageSize).Take(countListAgentQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = 0;// countListAgentQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListAgentQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForClientInfoResultModel> GetClientInfo(CountListClientInfoQueryModel countListAgentQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForClientInfoResultModel();
                try
                {
                    IEnumerable<AMAX30ClientInfoModel> list = null;
                    list = _repo.GetClientInfo(countListAgentQueryModel).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListAgentQueryModel.RowOffset = countListAgentQueryModel.RowOffset == 0 ? 1 : countListAgentQueryModel.RowOffset;
                        countListAgentQueryModel.PageSize = countListAgentQueryModel.PageSize == 0 ? 1000 : countListAgentQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListAgentQueryModel.PageSize);

                        }
                        if (countListAgentQueryModel.PageSize > 0)
                        {
                            query = query.Skip((countListAgentQueryModel.RowOffset - 1) * countListAgentQueryModel.PageSize).Take(countListAgentQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = countListAgentQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListAgentQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForClientInfoResultModel> GetClientInfo_V1(CountListClientInfoQueryModel countListAgentQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForClientInfoResultModel();
                try
                {
                    IEnumerable<AMAX30ClientInfoModel> list = null;
                    list = _repo.GetClientInfo(countListAgentQueryModel).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListAgentQueryModel.RowOffset = 1;// countListAgentQueryModel.RowOffset == 0 ? 1 : countListAgentQueryModel.RowOffset;
                        countListAgentQueryModel.PageSize = int.MaxValue;// countListAgentQueryModel.PageSize == 0 ? 1000 : countListAgentQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListAgentQueryModel.PageSize);

                        }
                        if (countListAgentQueryModel.PageSize > 0)
                        {
                            query = query.Skip((countListAgentQueryModel.RowOffset - 1) * countListAgentQueryModel.PageSize).Take(countListAgentQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = 0;// countListAgentQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListAgentQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForPaymentInfoResultModel> GetClientPaymentInfo(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForPaymentInfoResultModel();
                try
                {
                    IEnumerable<AMAX30PaymentInfoModel> list = null;
                    list = _repo.GetClientPaymentInfo(countListPaymentInfoQueryModel).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListPaymentInfoQueryModel.RowOffset = countListPaymentInfoQueryModel.RowOffset == 0 ? 1 : countListPaymentInfoQueryModel.RowOffset;
                        countListPaymentInfoQueryModel.PageSize = countListPaymentInfoQueryModel.PageSize == 0 ? 1000 : countListPaymentInfoQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListPaymentInfoQueryModel.PageSize);

                        }
                        if (countListPaymentInfoQueryModel.PageSize > 0)
                        {
                            query = query.Skip((countListPaymentInfoQueryModel.RowOffset - 1) * countListPaymentInfoQueryModel.PageSize).Take(countListPaymentInfoQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = countListPaymentInfoQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListPaymentInfoQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }

        public async Task<CountListForPaymentInfoResultModel> GetClientPaymentInfo_V1(CountListPaymentInfoQueryModel countListPaymentInfoQueryModel)
        {
            var result = await Task.Run(() =>
            {
                var resultTask = new CountListForPaymentInfoResultModel();
                try
                {
                    IEnumerable<AMAX30PaymentInfoModel> list = null;
                    list = _repo.GetClientPaymentInfo(countListPaymentInfoQueryModel).Result;
                    if (list.Any() && list.Count() > 0)
                    {
                        var query = list.AsQueryable();
                        int _totalPage = 0;
                        countListPaymentInfoQueryModel.RowOffset = 1;// countListPaymentInfoQueryModel.RowOffset == 0 ? 1 : countListPaymentInfoQueryModel.RowOffset;
                        countListPaymentInfoQueryModel.PageSize = int.MaxValue;// countListPaymentInfoQueryModel.PageSize == 0 ? 1000 : countListPaymentInfoQueryModel.PageSize;
                        if (list.Any() && list.Count() > 0)
                        {
                            _totalPage = (int)Math.Ceiling((double)list.Count() / countListPaymentInfoQueryModel.PageSize);

                        }
                        if (countListPaymentInfoQueryModel.PageSize > 0)
                        {
                            query = query.Skip((countListPaymentInfoQueryModel.RowOffset - 1) * countListPaymentInfoQueryModel.PageSize).Take(countListPaymentInfoQueryModel.PageSize);
                        }

                        resultTask.Data = query.ToList();
                        resultTask.IsSuccess = true;
                        resultTask.pageSize = 0;// countListPaymentInfoQueryModel.PageSize;
                        resultTask.pageCount = _totalPage;
                        resultTask.currentPage = countListPaymentInfoQueryModel.RowOffset;
                        resultTask.ResultMessage = "Record successfully get.";
                        resultTask.TotalRecordCount = list.Count();
                    }
                    else
                    {
                        resultTask.Data = null;
                        resultTask.IsSuccess = false;
                        resultTask.pageSize = 0;
                        resultTask.pageCount = 0;
                        resultTask.currentPage = 0;
                        resultTask.ResultMessage = "Data not found.";
                        resultTask.TotalRecordCount = 0;
                    }

                    return resultTask;
                }
                catch (Exception ex)
                {
                    resultTask.Data = null;
                    resultTask.IsSuccess = false;
                    resultTask.ResultMessage = ex.Message;
                    resultTask.TotalRecordCount = 0;

                    return resultTask;
                }

            });
            return result;
        }
    }
   

}
