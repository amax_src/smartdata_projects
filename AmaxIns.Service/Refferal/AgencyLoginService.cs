﻿using AmaxIns.DataContract.Referral.AgencyLogin;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Service.Refferal
{
    public class AgencyLoginService : BaseService, IAgencyLoginService 
    {
        IAgencyLoginRepository _Repository;
        public AgencyLoginService(IAgencyLoginRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._Repository = Repository;
        }

        public AgencyLoginModel validateAgency(AgencyLoginModel model)
        {
            return this._Repository.validateAgency(model);
        }

        public AgentLoginModel validateAgent(AgentLoginModel model)
        {
            return this._Repository.validateAgent(model);
        }

        public SearchFilterModel GetSearchParameters(AgentLoginModel model)
        {
            return this._Repository.GetSearchParameters(model);
        }
        public void UpdateToken(TokenModel token)
        {
            this._Repository.UpdateToken(token);
        }
        public bool ValidateToken(TokenModel token)
        {
            if (token.AgentId != 0)
            {
                var data = this._Repository.GetToken(token);
                if(data.Token == token.Token)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
