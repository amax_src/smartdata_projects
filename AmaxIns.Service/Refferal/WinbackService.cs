﻿using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace AmaxIns.Service.Refferal
{
    public class WinbackService : BaseService, IWinbackService
    {
        IWinbackRepository _winbackRepository;
        public WinbackService(IWinbackRepository winbackRepository, IConfiguration configuration) : base(configuration)
        {
            _winbackRepository = winbackRepository;
        }
        public IEnumerable<WinbackModel> WinbackReport(List<int> month, int year, string monthstr, int agencyId, string searchKeyWord, int pageNumber, out int totalpage, string direction = "desc", string sortkeyword = "id", bool sorting = false)
        {

            totalpage = _winbackRepository.WinbackReportPageCount(month, year, monthstr, agencyId, searchKeyWord, sorting);
            return _winbackRepository.WinbackReport(month, year, monthstr, agencyId, searchKeyWord, direction, sortkeyword, pageNumber, sorting);
        }


        public WinbackCommentWrapper GetComments(int ReferenceId)
        {
            WinbackCommentWrapper comments_warapper = new WinbackCommentWrapper();
            comments_warapper = _winbackRepository.GetCommentWrapper(ReferenceId);
            comments_warapper.Comments = _winbackRepository.GetComments(ReferenceId);
            return comments_warapper;
        }

        public void Addcomments(WinbackCommentWrapper comments)
        {
            this._winbackRepository.Addcomments(comments);
        }

        public int GetLatestMonthDataAvaliable(int agencyId)
        {
            return this._winbackRepository.GetLatestMonthDataAvaliable(agencyId);
        }

        public void UpdateWinBack(WinbackModel model, int agencyId)
        {
            this._winbackRepository.UpdateWinBack(model, agencyId);
        }

        public IEnumerable<WinbackModel> WinbackReportCsv(string monthstr, int year)
        {
            return this._winbackRepository.WinbackReportCsv(monthstr, year);
        }

        public void UpdateSearchFilter(SearchFilterModel search)
        {
            this._winbackRepository.UpdateSearchFilter(search);
        }
        public IEnumerable<WinbackAPIModel> GetWinBackDataForAPI(int monthId, int year)
        {
           return this._winbackRepository.GetWinBackDataForAPI(monthId, year);
        }
    }
}
