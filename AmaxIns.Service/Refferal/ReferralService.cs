﻿using AmaxIns.DataContract.Referral;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Service.Refferal
{
    public class ReferralService : BaseService, IReferralService
    {
        IRefferalRepository _referralRepository;
        IConfiguration _configuration;
        public ReferralService(IRefferalRepository referralRepository, IConfiguration configuration) : base(configuration)
        {
            this._referralRepository = referralRepository;
            this._configuration = configuration;
        }
        
        public void AddReferral(ReferralModel referralModel, string emailtoCustomer, string emailtoReferal, string subject,string subjectRef)
        {
            try
            {
                int assignedAgencyId = 0;
                assignedAgencyId= this._referralRepository.AddReferral(referralModel);
                SendEmailToAgency(referralModel.ZipCode, assignedAgencyId);// pass 99999 as agency id because it will search on the basis of Zip 
                SendEmailToCustomer(referralModel, emailtoCustomer,subject);
                SendEmailToReferance(referralModel, emailtoReferal,subjectRef);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddReferralByAgent(AgentRefModel Model, string emailtoCustomer, string emailtoReferal, string subject, string subjectRef)
        {
            try
            {
                int assignedAgencyId = 0;
                assignedAgencyId=this._referralRepository.AddReferralByAgent(Model);
                SendEmailToAgency(null, assignedAgencyId);
                SendEmailToCustomer(Model, emailtoCustomer,subject);
                SendEmailToReferance(Model, emailtoReferal, subjectRef);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<string> GetAgents()
        {
            return this._referralRepository.GetAgents();
        }

        public bool ValidateEmailAddress(string Email)
        {
            if (string.IsNullOrWhiteSpace(this._referralRepository.ValidateEmailAddress(Email)))
            {
                return true;
            }

            return false;
        }

        public bool ValidatePhoneNumber(string PhoneNumber)
        {
            if (string.IsNullOrWhiteSpace(this._referralRepository.ValidatePhoneNumber(PhoneNumber)))
            {
                return true;
            }

            return false;
        }
        public bool ValidateZip(string zip)
        {
            if (!string.IsNullOrWhiteSpace(this._referralRepository.ValidateZip(zip)))
            {
                return true;
            }

            return false;
        }
        public IEnumerable<ReferralReportModel> ReferralReport(int month, int year, string type, string zip, int agencyId)
        {
            return this._referralRepository.ReferralReport(month, year, type, zip, agencyId);
        }

        public bool SendEmailToCustomer(AgentRefModel referralModel, string EmailMessage, string subject)
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var Password = _configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            var EmailTo = _configuration.GetSection("TestEmail").Value;

            if (string.IsNullOrWhiteSpace(EmailTo))
            {
                EmailTo = referralModel.CustomerEmail;
            }

            EmailMessage = EmailMessage.Replace("@@CustName@@", referralModel.CustomerName);


            bool isemailsend = true;

            isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, subject, EmailMessage, Port);
            return isemailsend;

        }

        public bool SendEmailToReferance(AgentRefModel referralModel, string EmailMessage, string subject)
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var Password = _configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            var EmailTo = _configuration.GetSection("TestEmail").Value;

            if (string.IsNullOrWhiteSpace(EmailTo))
            {
                EmailTo = referralModel.RefEmail;
            }

            EmailMessage = EmailMessage.Replace("@@CustName@@", referralModel.CustomerName);
            EmailMessage = EmailMessage.Replace("@@RefName@@", referralModel.RefName);


            bool isemailsend = true;

            isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, subject, EmailMessage, Port);
            return isemailsend;

        }

        public bool SendEmailToCustomer(ReferralModel referralModel, string EmailMessage, string subject)
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var Password = _configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            var EmailTo = _configuration.GetSection("TestEmail").Value;

            if (string.IsNullOrWhiteSpace(EmailTo))
            {
                EmailTo = referralModel.CustomerEmail;
            }

            EmailMessage = EmailMessage.Replace("@@CustName@@", referralModel.CustomerName);


            bool isemailsend = true;

            isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, subject, EmailMessage, Port);
            return isemailsend;

        }

        public bool SendEmailToReferance(ReferralModel referralModel, string EmailMessage, string subject)
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var Password = _configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            var EmailTo = _configuration.GetSection("TestEmail").Value;

            if (string.IsNullOrWhiteSpace(EmailTo))
            {
                EmailTo = referralModel.RefEmail;
            }

            EmailMessage = EmailMessage.Replace("@@CustName@@", referralModel.CustomerName);
            EmailMessage = EmailMessage.Replace("@@RefName@@", referralModel.RefName);


            bool isemailsend = true;

            isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, subject, EmailMessage, Port);
            return isemailsend;

        }

        public IEnumerable<CommentsModel> GetComments(int ReferenceId)
        {
            return this._referralRepository.GetComments(ReferenceId);
        }

        public void Addcomments(CommentsModel comments)
        {
            this._referralRepository.Addcomments(comments);
        }

        public IEnumerable<ReferralReportModel> ReferralTouchedUnTouchedReport(int month, bool isTouched=false)
        {
           return this._referralRepository.ReferralTouchedUnTouchedReport(month, isTouched);
        }


        private void SendEmailToAgency(string zip, int? agencyId)
        {
            string subject = "A-MAX - New Referral from Website";
            string body = "Hi,<br/><br/>Thank you for entering a referral. Please check it at: <br/><br/>" + "https://ref.amaxinsurance.com/ReferenceReport." + "<br/><br/>" + "Thanks!";
            List<string> agencyIds = new List<string>();
            if (!string.IsNullOrWhiteSpace(zip))
            {
                body = "Hi,<br/><br/>You’ve got a new sales lead from our website. Please check it at: <br/><br/>" + "https://ref.amaxinsurance.com/ReferenceReport." + "<br/><br/>" + "Thanks!";
            }

            agencyIds.Add(agencyId.Value.ToString());

            var listofAgencies = string.Join(",", agencyIds);

            var agencyEmail = _referralRepository.GetAgencyEmail(listofAgencies);


            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var Password = _configuration.GetSection("Password").Value;
            int Port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            string EmailTo= _configuration.GetSection("TestEmail").Value;

            foreach (var x in agencyEmail)
            {
                if (string.IsNullOrWhiteSpace(EmailTo))
                {
                    EmailTo = x;
                }
                else
                {
                    body = "Mail send to agency :- " + x;
                }

                bool isemailsend = true;

                isemailsend = Utility.EmailUtilty.SendEmail(smtp, Password, EmailTo, from, subject, body, Port);
               
            }
        }

        public void AddTurboratorcomments(CommentsModel comments)
        {
            this._referralRepository.AddTurboratorcomments(comments);
        }

        public IEnumerable<CommentsModel> GetTurboratorComments(int turboraterId)
        {
          return  this._referralRepository.GetTurboratorComments(turboraterId);
        }
    }
}
