﻿using AmaxIns.DataContract.Referral.Language;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.Refferal
{
    public class LanguageService : BaseService, ILanguageService
    {
        ILanguageRepository _Repository;
        public LanguageService(ILanguageRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._Repository = Repository;
        }
        public async Task<IEnumerable<LanguageModel>> GetLanguage()
        {
            return await this._Repository.GetLanguage();
        }

        public async Task<IEnumerable<ResourceValueModel>> GetLanguageResoure(int id, string section)
        {
            return await this._Repository.GetLanguageResoure(id,section);
        }
    }
}
