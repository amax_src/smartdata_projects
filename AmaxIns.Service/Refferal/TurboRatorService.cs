﻿using AmaxIns.DataContract.Referral.Turborator;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmaxIns.Service.Refferal
{
    public class TurboratorService : BaseService, ITurboratorService
    {
        ITurboratorRepository _turborator;
        public TurboratorService(ITurboratorRepository turborator, IConfiguration configuration) : base(configuration)
        {
            _turborator = turborator;
        }

        public void SaveData(TurboratorModel turboratorModel)
        {

            int turboratorid, driverid, carid, customerid;

            turboratorid = _turborator.SaveTurboratorData(turboratorModel);

            customerid = _turborator.SaveCustomerData(turboratorModel.Customer, turboratorid);
            foreach (var x in turboratorModel.Drivers)
            {
                driverid = _turborator.SaveDriverData(x, turboratorid);
                if (x.Address != null)
                {
                    _turborator.SaveAddress2Data(x.Address, turboratorid, driverid);
                    _turborator.SaveAddressData(x.Address, customerid, turboratorid);
                }
            }
            foreach (var x in turboratorModel.Cars)
            {
                carid = _turborator.SaveCarData(x, turboratorid);
                if (x.GaragingAddress != null)
                {
                    _turborator.SaveGaragingAddressData(x.GaragingAddress, turboratorid, carid);
                }
            }

            foreach (var x in turboratorModel.Coverages)
            {

                _turborator.SaveCoverageData(x, turboratorid);
            }
        }

        public IEnumerable<TurboratorModel> TurboratorReferralReport(int month, int year, int agencyId)
        {
            var data= this._turborator.TurboratorReferralReport(month, year, agencyId);
            //foreach(var x in data)
            //{
            //    x.Coverages= this._turborator.TurboratorCoverage(x.TurboratorID).ToList();
            //}
            return data;
        }
        
    }
}
