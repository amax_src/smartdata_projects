﻿using AmaxIns.RepositoryContract.Notification;
using AmaxIns.ServiceContract.Notification;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Service.Notification
{

    public class NotificationService : BaseService, INotificationService
    {

        INotificationRepository Repository;
        public NotificationService(INotificationRepository _Repository, IConfiguration configuration) : base(_Repository, configuration)
        {
            Repository = _Repository;
        }
        public string GetNotification()
        {
            return Repository.GetNotification();
        }
    }
}
