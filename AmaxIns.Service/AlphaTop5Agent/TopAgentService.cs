﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.AlphaTop5Agent;
using AmaxIns.RepositoryContract.AlphaTop5Agent;
using AmaxIns.Service;
using AmaxIns.ServiceContract.AlphaTop5Agent;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Repository.AlphaTop5Agent
{
    public class TopAgentService : BaseService, ITopAgentService
    {
        ITopAgentRepository _repository;
        public TopAgentService(IConfiguration configuration, ITopAgentRepository repository) : base(configuration)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<AgencyModel>> GetAgency()
        {
            return await _repository.GetAgency();
        }

        public async Task<IEnumerable<int>> GetYear()
        {
            return await _repository.GetYear();
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetPremiumData(TopFiveAgentUserModel model)
        {
            string agency = string.Empty;
            string year = string.Empty;
            string month = string.Empty;

            if (model.agencyId.Count > 0)
            {
                agency = string.Join(",", model.agencyId.Select(n => n.ToString()).ToArray());
            }
            if (model.year.Count > 0)
            {
                year = string.Join(",", model.year.Select(n => n.ToString()).ToArray());
            }

            if (model.month.Count > 0)
            {
                month = string.Join(",", model.month.Select(n => n.ToString()).ToArray());
            }

            return await _repository.GetPremiumData(agency, year, month);
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetAgencyFeeData(TopFiveAgentUserModel model)
        {
            string agency = string.Empty;
            string year = string.Empty;
            string month = string.Empty;

            if (model.agencyId.Count > 0)
            {
                agency = string.Join(",", model.agencyId.Select(n => n.ToString()).ToArray());
            }
            if (model.year.Count > 0)
            {
                year = string.Join(",", model.year.Select(n => n.ToString()).ToArray());
            }

            if (model.month.Count > 0)
            {
                month = string.Join(",", model.month.Select(n => n.ToString()).ToArray());
            }

            return await _repository.GetAgencyFeeData(agency, year, month);
        }

        public async Task<IEnumerable<TopFiveDataModel>> GetPolicyCount(TopFiveAgentUserModel model)
        {
            string agency = string.Empty;
            string year = string.Empty;
            string month = string.Empty;

            if (model.agencyId.Count > 0)
            {
                agency = string.Join(",", model.agencyId.Select(n => n.ToString()).ToArray());
            }
            if (model.year.Count > 0)
            {
                year = string.Join(",", model.year.Select(n => n.ToString()).ToArray());
            }

            if (model.month.Count > 0)
            {
                month = string.Join(",", model.month.Select(n => n.ToString()).ToArray());
            }

            return await _repository.GetPolicyCount(agency, year, month);
        }
    }
}
