﻿using AmaxIns.DataContract.Dashboard;
using AmaxIns.RepositoryContract.Dashboard;
using AmaxIns.ServiceContract.Dashboard;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Service.Service
{
    public class DashboardService : BaseService, IDashboardService
    {
        IDashboardRepository _repository;
        public DashboardService(IDashboardRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = Repository;
        }

        public async Task<IEnumerable<DashboadSpectrumModel>> GetCallsData(List<int> agencyid, int month, int year)
        {
            List<DashboadSpectrumModel> _spectrum = new List<DashboadSpectrumModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetCallsData(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _spectrum.Add(z);
                    }
                }


            }
            return _spectrum;
        }



        public async Task<List<DashboardEPRModel>> GetEprData(List<int> agencyid, int month, int year)
        {
            List<DashboardEPRModel> _epr = new List<DashboardEPRModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetEprData(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _epr.Add(z);
                    }
                }


            }
            return _epr;
        }
        public async Task<DashboardEprDetails> GetEprDetails(List<int> agencyid, int month, int year)
        {
            DashboardEprDetails details = new DashboardEprDetails();
            details.Daily = await GetEprDashboardDaily(agencyid, month, year);
            details.Weekly = await GetEprlDashboardWeekly(agencyid, month, year);
            return details;
        }

        public async Task<IEnumerable<DashboardPayrollModel>> GetPayrollDashboardData(List<int> agencyid, int month, int year)
        {
            List<DashboardPayrollModel> _payroll = new List<DashboardPayrollModel>();
            foreach (var x in agencyid)
            {
                var payrollactual = _repository.GetPayrollActualData(x, month, year).Result;
                var payrollbudget = _repository.GetPayrollBudgeData(x, month, year).Result;
                var budgetot = _repository.GetPayrollBudgetOvertime(x, month, year).Result;
                var payrollot = _repository.GetPayrollOvertimeData(x, month, year).Result;
                var _payrolltemp = new DashboardPayrollModel();
                _payrolltemp.AgencyID = x;
                _payrolltemp.Year = year;
                _payrolltemp.SortMonthNum = month;
                if (payrollactual != null && payrollactual.Any())
                {
                    _payrolltemp.PayrollActual = payrollactual.FirstOrDefault().PayrollActual;
                    _payrolltemp.PayrollPerTransaction = payrollactual.FirstOrDefault().PayrollPerTransaction;
                    _payrolltemp.PayrollPerPolicy = payrollactual.FirstOrDefault().PayrollPerPolicy;

                }


                if (payrollbudget != null && payrollbudget.Any())
                {
                    _payrolltemp.PayrollBudget = payrollbudget.FirstOrDefault().PayrollBudget;
                }

                if (budgetot != null && budgetot.Any())
                {
                    _payrolltemp.OTBudget = budgetot.FirstOrDefault().OTBudget;
                }

                if (payrollot != null && payrollot.Any())
                {
                    _payrolltemp.OtActual = payrollot.FirstOrDefault().OtActual;
                    _payrolltemp.OTPerPolicy = payrollot.FirstOrDefault().OTPerPolicy;
                    _payrolltemp.OTHoursPerPolicy = payrollot.FirstOrDefault().OTHoursPerPolicy;
                }

                _payroll.Add(_payrolltemp);
            }
            return _payroll;
        }
        public async Task<List<DashboardQuotesModel>> GetQuoteData(List<int> agencyid, int month, int year)
        {
            List<DashboardQuotesModel> _quotes = new List<DashboardQuotesModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetQuoteData(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _quotes.Add(z);
                    }
                }


            }
            return _quotes;
        }

        public async Task<List<DashboardRevenueModel>> GetSalesData(List<int> agencyid, int month, int year)
        {
            List<DashboardRevenueModel> _sale = new List<DashboardRevenueModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetSalesData(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _sale.Add(z);
                    }
                }
            }
            return _sale;
        }

        private async Task<IEnumerable<DashboardEPRModel>> GetEprDashboardDaily(List<int> agencyid, int month, int year)
        {
            List<DashboardEPRModel> _eprdaily = new List<DashboardEPRModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetEprDashboardDaily(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _eprdaily.Add(z);
                    }
                }


            }
            return _eprdaily;
        }

        private async Task<IEnumerable<DashboardEPRModel>> GetEprlDashboardWeekly(List<int> agencyid, int month, int year)
        {
            List<DashboardEPRModel> _eprweekly = new List<DashboardEPRModel>();
            foreach (var x in agencyid)
            {
                var result = _repository.GetEprlDashboardWeekly(x, month, year).Result;
                if (result != null && result.Any())
                {
                    foreach (var z in result)
                    {
                        _eprweekly.Add(z);
                    }
                }

            }
            return _eprweekly;
        }

    }
}
