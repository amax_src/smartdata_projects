﻿using AmaxIns.DataContract.PBX;
using AmaxIns.RepositoryContract.PBX;
using AmaxIns.ServiceContract.PBX;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.PBX
{
    public class PbxService : BaseService,IPbxService
    {
        private readonly IPbxRepository _pbxRepository;
        public PbxService(IPbxRepository pbxRepository
            , IConfiguration configuration) : base(pbxRepository, configuration)
        {
            _pbxRepository = pbxRepository;
        }

        public async Task<IEnumerable<string>> GetDate()
        {
            return await _pbxRepository.GetDate();
        }

        public async Task<IEnumerable<string>> GetExtention()
        {
            return await _pbxRepository.GetExtention();
        }

        public async Task<IEnumerable<string>> GetMonth()
        {
            return await _pbxRepository.GetMonth();
        }

        public async Task<IEnumerable<PbxModel>> GetPBXData()
        {
            return await _pbxRepository.GetPBXData();
        }

        public async Task<IEnumerable<PbxModel>> GetPBXDailyData()
        {
            return await _pbxRepository.GetPBXDailyData();
        }
    }
}
