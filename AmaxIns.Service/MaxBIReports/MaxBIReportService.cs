﻿using AmaxIns.DataContract.MaxBIReports;
using AmaxIns.RepositoryContract.MaxBIReports;
using AmaxIns.ServiceContract.MaxBIReports;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.MaxBIReports
{
    public class MaxBIReportService : BaseService, IMaxBIReportService
    {
        private readonly IMaxBIReportRepository _repo;
        public MaxBIReportService(IConfiguration configuration, IMaxBIReportRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }

        public async Task<IEnumerable<CatalogInfo>> GetCatalogs(Request request)
        {
            return await _repo.GetCatalogs(request);
        }
        public async Task<IEnumerable<HierarchyInfo>> GetHierarchyLevel(HierarchyRequest request)
        {
            return await _repo.GetHierarchyLevel(request);
        }

        #region Carrier Agents
        public async Task<IEnumerable<AgentCarrierAFeePremium>> GetAgentCarrierAFeePremium(Request request)
        {
            return await _repo.GetAgentCarrierAFeePremium(request);
        }
        #endregion

        #region Agency Per Policy Reports
        public async Task<IEnumerable<AgencyPerPolicy>> GetAFeePremiumPerPolicy(Request request)
        {
            return await _repo.GetAFeePremiumPerPolicy(request);
        }
        public async Task<decimal> GetAgencyPerPolicyReportSummary(Request request)
        {
            return await _repo.GetAFeePremiumPerPolicySummary(request);
        }
        #endregion

        #region New Modified Quotes
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotes(Request request)
        {
            return await _repo.GetNewModifiedQuotes(request);
        }
        public async Task<decimal> GetNewModifiedQuotesSummary(Request request)
        {
            return await _repo.GetNewModifiedQuotesSummary(request);
        }
        #endregion

        #region Inbound / Outbound Calls
        public async Task<IEnumerable<InboundOutboundCalls>> GetInboundOutboundCalls(Request request)
        {
            return await _repo.GetInboundOutboundCalls(request);
        }
        public async Task<decimal> GetInboundOutboundCallsSummary(Request request)
        {
            return await _repo.GetInboundOutboundCallsSummary(request);
        }
        #endregion

        #region Payroll Dashboad
        public async Task<IEnumerable<PayrollFilterData>> GetPayrollDashboardFilterData(PayrollRequest request)
        {
            return await _repo.GetPayrollDashboardFilterData(request);
        }
        public async Task<IEnumerable<DataContract.MaxBIReports.Payroll>> GetPayrollDashboardData(PayrollRequest request)
        {
            return await _repo.GetPayrollDashboardData(request);
        }
        #endregion

        public async Task<IEnumerable<EmployeeInfo>> GetmployeeSensitiveInfo(Request request)
        {
            return await _repo.GetmployeeSensitiveInfo(request);
        }

        public async Task<IEnumerable<HourlyProduction>> GetAgentHourlyProduction(Request request)
        {
            return await _repo.GetAgentHourlyProduction(request);
        }
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPR(Request request)
        {
            return await _repo.GetNewModifiedQuotesForEPR(request);
        }
        public async Task<IEnumerable<NewModifiedQuotes>> GetNewModifiedQuotesForEPRGraph(Request request)
        {
            return await _repo.GetNewModifiedQuotesForEPRGraph(request);
        }

    }
}
