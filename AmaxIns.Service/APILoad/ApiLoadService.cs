﻿using AmaxIns.DataContract.APILoad;
using AmaxIns.RepositoryContract.APILoad;
using AmaxIns.ServiceContract.APILoad;
using Microsoft.Extensions.Configuration;
using System;


namespace AmaxIns.Service.APILoad
{
    public class ApiLoadService : BaseService, IApiLoadService
    {
        IAPILoadRepository _Repo;
        public ApiLoadService(IConfiguration configuration, IAPILoadRepository Repo) : base(configuration)
        {
            _Repo = Repo;
        }

        public void Save(ApiLoadModel model)
        {
            _Repo.Save(model);
        }
    }
}
