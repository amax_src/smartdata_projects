﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AmaxIns.DataContract.CarrierMix;
using AmaxIns.RepositoryContract.CarrierMix;
using AmaxIns.ServiceContract.CarrierMix;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace AmaxIns.Service.CarrierMix
{
    public class CarrierMixService : BaseService, ICarrierMixService
    {
        ICarrierMixRepository _Repo;
        public CarrierMixService(IConfiguration configuration, ICarrierMixRepository Repo) : base(configuration)
        {
            _Repo = Repo;
        }

        public async Task<IEnumerable<string>> GetCarrier(string Month,string year)
        {
            return await _Repo.GetCarrier(Month,year);
        }

        public async Task<IEnumerable<string>> GetDates(string Month, string year)
        {
            return await _Repo.GetDates(Month,year);
        }
        public async Task<IEnumerable<CarrierMixModel>> GetCarrierMixData(CarrierMixUserModel model)
        {
            string date = string.Empty;
            string agency = string.Empty;
            string month = string.Empty;
            string Carrier = string.Empty;
            string year = string.Empty;

            if (model.Agency.Count > 0)
            {
                agency = string.Join(",", model.Agency.Select(n => n.ToString()).ToArray());
            }

            if (model.Date.Count > 0)
            {
                date = string.Join(",", model.Date.Select(n => n.ToString()).ToArray());
            }

            if (model.Carrier.Count > 0)
            {
                Carrier = string.Join(",", model.Carrier.Select(n => n.ToString()).ToArray());
            }

            month = model.Month;
            year = model.Year;


            return await _Repo.GetCarrierMixData(date,agency,month,Carrier, year);
        }
    }
}
