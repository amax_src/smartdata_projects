﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.Quotes;
using AmaxIns.RepositoryContract.HourlyLoad;
using AmaxIns.ServiceContract.HourlyLoad;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Service.HourlyLoad
{
    public class HourlyLoadService : BaseService, IHourlyLoadService
    {
        private IHourlyLoadRepository _repository;
        public HourlyLoadService(IHourlyLoadRepository repository, IConfiguration configuration) : base(configuration)
        {
            this._repository = repository;
        }

        public Task<IEnumerable<HourlyLoadModel>> GetData(string date, int agencyId)
        {
            return _repository.GetData(date, agencyId);
        }


        public Task<IEnumerable<HourlyNewModifiedQuotes>> GetHourlynewModifiedQuotes(string date, int agencyId)
        {
            return _repository.GetHourlynewModifiedQuotes(date, agencyId);
        }


        public Task<IEnumerable<string>> GetDates(int month, string year)
        {
            return _repository.GetDates(month, year);
        }

        public Task<IEnumerable<MonthModel>> GetMonth(string year)
        {
            return _repository.GetMonth(year);
        }

        public IEnumerable<AgentCountModel> MergeData(string date, int agencyId)
        {
            List<AgentCountModel> data = new List<AgentCountModel>();
            var hourlyData = _repository.GetData(date, agencyId).Result;
            var hourlyModifiedData = _repository.GetHourlynewModifiedQuotes(date, agencyId).Result;
            var agentCountData= _repository.GetAgentCountPerhour(date, agencyId).Result;
            foreach (var x in hourlyData)
            {
                data.Add(new AgentCountModel
                {
                    Agencyfee = x.Agencyfee,
                    Policies = x.Policies,
                    HourIntervel = x.HourIntervel,
                    Premium = x.Premium,
                    Transactions = x.Transactions,
                    ModifiedQuotes = 0,
                    NewQuotes = 0

                });
            }

            foreach (var x in data)
            {
                var quote = hourlyModifiedData.Where(m => m.HourIntervel == x.HourIntervel).ToList();
                var agentCount = agentCountData.Where(m => m.HourIntervel == x.HourIntervel).ToList();

                if (quote != null && quote.Any())
                {
                    x.ModifiedQuotes = quote.Sum(m => m.ModifiedQuotes);
                    x.NewQuotes = quote.Sum(m => m.NewQuotes);
                }
                if (agentCount != null && agentCount.Any())
                {
                    x.AgentCount = agentCount.FirstOrDefault().AgentCount;
                    x.AgentNames = agentCount.FirstOrDefault().AgentNames;
                }
            }

            return data;
        }

        public IEnumerable<AgentCountModel> MergeData(string startdate, string enddate)
        {
            List<AgentCountModel> data = new List<AgentCountModel>();
            var hourlyData = _repository.GetData(startdate, enddate).Result;
            var hourlyModifiedData = _repository.GetHourlynewModifiedQuotes(startdate, enddate).Result;
            var agentCountData = _repository.GetAgentCountPerhour(startdate, enddate).Result;

            foreach (var x in hourlyData)
            {
                data.Add(new AgentCountModel
                {
                    Location = x.Location,
                    Agencyfee = x.Agencyfee,
                    Policies = x.Policies,
                    HourIntervel = x.HourIntervel,
                    Premium = x.Premium,
                    Transactions = x.Transactions,
                    ModifiedQuotes = 0,
                    NewQuotes = 0,
                    Agencyid = x.Agencyid,
                    Date = x.Paydate

                });
            }

            foreach (var x in data)
            {
                var quote = hourlyModifiedData.Where(m => m.Date == x.Date && m.HourIntervel == x.HourIntervel && m.Agencyid == x.Agencyid).ToList();
                var agentCount = agentCountData.Where(m => m.HourIntervel == x.HourIntervel && m.Agencyid == x.Agencyid).ToList();
                if (quote != null && quote.Any())
                {
                    x.ModifiedQuotes = quote.Sum(m => m.ModifiedQuotes);
                    x.NewQuotes = quote.Sum(m => m.NewQuotes);
                }
                if (agentCount != null && agentCount.Any())
                {
                    x.AgentCount = agentCount.FirstOrDefault().AgentCount;
                }
            }
            return data;
        }

        public async Task<IEnumerable<PeakHoursModel>> GetPeakHoursData(string startdate, string enddate, List<int> agencyid)
        {

            List<PeakHoursModel> data = new List<PeakHoursModel>();
            var result = this._repository.GetPeakHoursData(startdate, enddate).Result;
            if (agencyid != null && agencyid.Count > 0)
            {
                foreach (var x in agencyid)
                {
                    var z = result.Where(m => m.AgencyId == x).ToList();
                    foreach (var item in z)
                    {
                        data.Add(item);
                    }
                }
            }
            else
            {
                data = result.ToList();
            }

            return data;
        }
    }
}
