﻿using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using AmaxIns.RepositoryContract.Payroll;
using AmaxIns.ServiceContract.Payroll;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.Payroll
{
    public class PayrollService : BaseService, IPayrollService
    {
        private readonly IPayrollRepository _payrollRepository;
        public PayrollService(IPayrollRepository payrollRepository
            , IConfiguration configuration) : base(payrollRepository, configuration)
        {
            _payrollRepository = payrollRepository;
        }

        /// <summary>
        /// PayrollService for Healthcheck
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        public async Task<object> HealthCheckAsync()
        {
            return await _payrollRepository.HealthCheckAsync();
        }

        /// <summary>
        /// Get a list of users.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserModel>> GetUserListAsync()
        {
            return await _payrollRepository.GetUserListAsync();
        }

      
        public async Task<CombinePayRollModel> GetPayRoleActualAndBudgetedData()
        {
            CombinePayRollModel cprm = new CombinePayRollModel();
            cprm.Actual= await _payrollRepository.GetPayRoleActualData();
            cprm.Budgted = await _payrollRepository.GetPayRoleBudgetedData();
            return cprm;
        }
       
        public async Task<ConsolidatedTileData> GetPayRoleTilesConsolidatedData()
        {
            return await _payrollRepository.GetPayRoleTilesConsolidatedData();
        }
    }
}
