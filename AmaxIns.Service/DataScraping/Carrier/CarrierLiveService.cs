﻿using AmaxIns.DataContract.DataScraping.Aspen;
using AmaxIns.RepositoryContract.DataScraping.Aspen;
using AmaxIns.ServiceContract.DataScraping.Carrier;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using AmaxIns.RepositoryContract.DataScraping.Livedate;
using AmaxIns.DataContract.DataScraping.Seaharbor;
using System;

namespace AmaxIns.Service.DataScraping.Carrier
{
    public class CarrierLiveService : BaseService, ICarrierLiveService

    {
        ICarrierLiveRepository _Repo;
        ILiveDataPaymentRepository _LiveDataRepo;
        public CarrierLiveService(IConfiguration configuration, ICarrierLiveRepository Repo, ILiveDataPaymentRepository LiveDataRepo) : base(configuration)
        {
            _Repo = Repo;
            _LiveDataRepo = LiveDataRepo;
        }
        //ip live 
        public IEnumerable<IPModel> GetAspenIPData()
        {
            var data = _LiveDataRepo.GetIPData("Aspen Managing General Agency");
            var ipdata = _Repo.GetAspenLiveData();
            if (ipdata.Any())
            {
                foreach (var x in data)
                {
                    var temp = ipdata.Where(m => m.PaymentAmount == x.PayAmount && m.Location == x.Agency && m.Insured.Split(' ')[0].ToLower() == x.Applicant.Split(' ')[0].ToLower());
                    if (temp.Any())
                    {
                        x.IsFoundinIP = true;
                    }
                }
            }
            return data;
        }
        //aspen
        public IEnumerable<AspenLiveModel> GetAspenLiveData()
        {

            var data = _Repo.GetAspenLiveData();
            var ipdata = _LiveDataRepo.GetIPData("Aspen Managing General Agency");
            if (ipdata.Any())
            {
                foreach (var x in data)
                {
                    var temp = ipdata.Where(m => m.PayAmount == x.PaymentAmount && m.Agency == x.Location && m.Applicant.Split(' ')[0].ToLower() == x.Insured.Split(' ')[0].ToLower());
                    if (temp.Any())
                    {
                        x.IsFoundinIP = true;
                    }
                }
            }

            return data;


        }

        public IEnumerable<IPModel> GetSeaharborIPData()
        {
            var data = _LiveDataRepo.GetIPData("SeaHarbor");
            var ipdata = _Repo.GetSeaharborLiveData();
            if (ipdata.Any())
            {
                foreach (var x in data)
                {
                    //var temp = ipdata.Where(m => m.PaymentAmount == x.PayAmount && m.Location == x.Agency && m.Insured.Split(' ')[0].ToLower() == x.Applicant.Split(' ')[0].ToLower());
                    var temp = ipdata.Where(m => IPDataWithSeaHarborCarrier(x, m));

                    if (temp.Any())
                    {
                        x.IsFoundinIP = true;
                    }
                }
            }
            return data;
        }

        public IEnumerable<SeaharborLiveModel> GetSeaharborLiveData()
        {
            var data = _Repo.GetSeaharborLiveData();
            var ipdata = _LiveDataRepo.GetIPData("SeaHarbor");
            if (ipdata.Any())
            {
                foreach (var x in data)
                {
                    if (x.Location != null)
                    {
                        // var temp = ipdata.Where(m => m.PayAmount == x.PaymentAmount && Convert.ToString(m.Agency).ToLower() ==x.Location.ToLower() && m.Applicant.Split(' ')[0].ToLower() == x.Insured.Split(' ')[0].ToLower());
                        var temp = ipdata.Where(m => IPDataWithSeaHarborCarrier(m, x));
                        if (temp.Any())
                        {
                            x.IsFoundinIP = true;
                        }
                    }
                }
            }

            return data;

        }

        private static bool IPDataWithSeaHarborCarrier(IPModel model, SeaharborLiveModel sm)
        {
            bool ismatch = false;
            if (Math.Floor(model.PayAmount) != Math.Floor(sm.PaymentAmount))
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(sm.Insured) || string.IsNullOrWhiteSpace(model.Applicant))
            {
                return false;
            }

            var IpApplicant = model.Applicant.ToLower().Split(' ');
            var CApplicant = sm.Insured.ToLower().Split(' ');
            int postivecase = 0;

            if (IpApplicant.Length > 0 && CApplicant.Length > 0)
            {
                if (IpApplicant.Length >= CApplicant.Length)
                {
                    foreach (var x in CApplicant)
                    {
                        if (IpApplicant.Any(m => m == x))
                        {
                            postivecase = postivecase + 1;
                        }
                    }
                }
                else
                {
                    foreach (var x in IpApplicant)
                    {
                        if (CApplicant.Any(m => m == x))
                        {
                            postivecase = postivecase + 1;
                        }
                    }
                }

                if(postivecase<=1)
                {
                    return false;
                }
            
            }

            if (sm.Location != null && (sm.Location.ToLower().Trim() != model.Agency.ToLower()))
            {
                return false;
            }

            return true;
        }
    }
}
