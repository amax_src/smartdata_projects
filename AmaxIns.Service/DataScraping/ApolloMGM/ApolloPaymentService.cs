﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.DataContract.DataScraping.InsurancPro;
using AmaxIns.RepositoryContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.Livedate;
using AmaxIns.ServiceContract.DataScraping.ApolloMGM;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmaxIns.Service.DataScraping.ApolloMGM
{
    public class ApolloPaymentService : BaseService, IApolloPaymentService
    {
        IApolloPaymentRepository _Repo;
        ILiveDataPaymentRepository _LiveDataRepo;
       public ApolloPaymentService(IConfiguration configuration, IApolloPaymentRepository Repo, ILiveDataPaymentRepository LiveDataRepo) :base(configuration)
        {
            _Repo = Repo;
            _LiveDataRepo = LiveDataRepo;
        }
        public IEnumerable<IPModel> GetApolloIPData()
        {
            var Ipdata = _LiveDataRepo.GetIPData("Apollo,Apollo 6 mo,Apollo 6 Mon,Apollo Monthly");
            var data = _Repo.GetApolloPaymentCarrierData();
            if (data.Any())
            {
                foreach (var x in Ipdata)
                {
                    var temp = data.Where(m => m.PaymentAmount == x.PayAmount && m.Location == x.Agency && m.Insured.Split(' ')[0].ToLower() == x.Applicant.Split(' ')[0].ToLower());
                    if (temp.Any())
                    {
                        x.IsFoundinIP = true;
                    }
                }
            }
            return Ipdata;
        }

        public IEnumerable<ApolloLiveModel> GetApolloLiveData()
        {
            var data = _Repo.GetApolloPaymentCarrierData();
            var ipdata = _LiveDataRepo.GetIPData("Apollo,Apollo 6 mo,Apollo 6 Mon,Apollo Monthly");
            if (ipdata.Any())
            {
                foreach (var x in data)
                {
                    var temp = ipdata.Where(m => m.PayAmount == x.PaymentAmount && m.Agency == x.Location && m.Applicant.Split(' ')[0].ToLower() == x.Insured.Split(' ')[0].ToLower());
                    if (temp.Any())
                    {
                        x.IsFoundinIP = true;
                    }
                }
            }
            return data;
        }
    }
}
