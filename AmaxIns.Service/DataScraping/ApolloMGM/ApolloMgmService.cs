﻿using AmaxIns.DataContract.DataScraping.ApolloMGM;
using AmaxIns.RepositoryContract.DataScraping.ApolloMGM;
using AmaxIns.ServiceContract.DataScraping.ApolloMGM;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.DataScraping.ApolloMGM
{
    public class ApolloMgmService : BaseService, IApolloMgmService
    {
        IApolloMgmRepo _Repo;
        public ApolloMgmService(IConfiguration configuration, IApolloMgmRepo Repo) : base(configuration)
        {
            _Repo = Repo;
        }

        public IEnumerable<ApolloCommissionsModel> GetApolloCommissions(int currentPage, out int TotalRecords, int AgencyId)
        {
            var data = _Repo.GetApolloCommissions(currentPage, out TotalRecords, AgencyId);
            return data;
        }

        public  IEnumerable<ApolloMgmModel> GetApolloData( int currentPage, out int TotalRecords,int AgencyId)
        {

            var data = _Repo.GetApolloData(currentPage,out TotalRecords,AgencyId);
            return data;
            
        }

        public IEnumerable<ApolloMgmModel> GetApolloDatadueDate(int currentPage, out int TotalRecords, int AgencyId)
        {
            var data = _Repo.GetApolloDatadueDate(currentPage, out TotalRecords, AgencyId);
            return data;
        }

        public IEnumerable<ApolloMgmModel> GetApolloDataExpiring(int currentPage, out int TotalRecords, int AgencyId)
        {
            var data = _Repo.GetApolloDataExpiring(currentPage, out TotalRecords, AgencyId);
            return data;
        }
    }
}
