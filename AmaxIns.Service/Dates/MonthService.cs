﻿using AmaxIns.DataContract.CommanModel;
using AmaxIns.ServiceContract.Budget;
using AmaxIns.ServiceContract.Dates;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Service.Dates
{
    public class MonthService : BaseService, IMonthService
    {
        IBudgetService _budgetService;
        public MonthService(IConfiguration configuration, IBudgetService budgetService) : base(configuration)
        {
            _budgetService = budgetService;
        }

        public async Task<List<string>> GetMonths(int Year)
        {
            int currentMonth = 12;
            List<string> keyValues = new List<string>();
            if (Year == DateTime.Now.Year)
            {
                currentMonth = DateTime.Now.Month;
            }

            for (int i = 1; i <= currentMonth; i++)
            {
                keyValues.Add(GetMonth(i));
            }

            return keyValues;
        }

        public async Task<List<MonthModel>> GetMonthKeyValue(int Year)
        {
            int currentMonth = 12;
            List<MonthModel> keyValues = new List<MonthModel>();
            if (Year == DateTime.Now.Year)
            {
                currentMonth = DateTime.Now.Month;
            }
            var monthValue = await _budgetService.GetLatestMonthValue(Year);
            for (int i = 1; i <= currentMonth; i++)
            {
                keyValues.Add(new MonthModel {
                    Name= GetMonth(i),
                    Id= GetMonthkey(GetMonth(i))
                }); ;
            }
            keyValues.Where(p => p.Id == monthValue.FirstOrDefault()).ToList().ForEach(x => x.Selected = true);
            return keyValues;
        }

        public async Task<List<MonthModel>> GetExtraMonthKeyValue(int Year)
        {
            int currentMonth = 12;
            List<MonthModel> keyValues = new List<MonthModel>();
            if (Year == DateTime.Now.Year)
            {
                currentMonth = DateTime.Now.Month;
            }

            for (int i = 1; i <= currentMonth + 1; i++)
            {
                keyValues.Add(new MonthModel
                {
                    Name = GetMonth(i),
                    Id = GetMonthkey(GetMonth(i))
                }); ;
            }

            return keyValues;
        }

        public async Task<List<MonthModel>> GetMonthPlanner()
        {
            List<MonthModel> keyValues = new List<MonthModel>();
            keyValues.Add(new MonthModel
            {
                Name = GetMonth(DateTime.Now.AddMonths(1).Month),
                Id = GetMonthkey(GetMonth(DateTime.Now.AddMonths(1).Month))
            }); ;
            return keyValues;
        }
        public async Task<List<string>> GetDates()
        {
            DateTime _date = DateTime.UtcNow;
            int _year = DateTime.Now.Year;
            int _month = _date.AddMonths(1).Month;
            if (_month == 1)
            {
                _year = DateTime.Now.AddYears(1).Year;
            }
            var dates = new List<string>();

            // Loop from the first day of the month until we hit the next month, moving forward a day at a time
            for (var date = new DateTime(_year, _month, 1); date.Month == _month; date = date.AddDays(1))
            {
                dates.Add(date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
            }

            return dates;
        }
        public async Task<List<string>> GetDatesByMonth(string month, string year)
        {
            DateTime _date = DateTime.UtcNow;
            int _year = DateTime.Now.Year;
            int _month = Convert.ToInt16(GetMonthkey(month));
            if(!string.IsNullOrEmpty(year))
            {
                _year = Convert.ToInt32(year);
            }
            var dates = new List<string>();

            // Loop from the first day of the month until we hit the next month, moving forward a day at a time
            for (var date = new DateTime(_year, _month, 1); date.Month == _month; date = date.AddDays(1))
            {
                dates.Add(date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
            }

            return dates;
        }
        private string GetMonthkey(string month)
        {

            switch (month)
            {
                case "January":
                    return "1";
                case "February":
                    return "2";
                case "March":
                    return "3";
                case "April":
                    return "4";
                case "May":
                    return "5";
                case "June":
                    return "6";
                case "July":
                    return "7";
                case "August":
                    return "8";
                case "September":
                    return "9";
                case "October":
                    return "10";
                case "November":
                    return "11";
                case "December":
                    return "12";
                default:
                    return "1";
            }
        }
        private string GetMonth(int month)
        {

            switch (month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "January";
            }
        }
    }
}
