﻿using AmaxIns.ServiceContract.Dates;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Service.Dates
{
    public class YearService : BaseService, IYearService
    {
        public YearService(IConfiguration configuration) : base(configuration)
        {

        }
        public async Task<IEnumerable<string>> GetYears()
        {
            List<string> years = new List<string>();
            int startyear = DateTime.Now.Year;
            if (startyear!=2023)
            {
                years.Add("2023");
            }
            for (int i= startyear; i>=2019;i--)
            {
                years.Add(i.ToString());
            }
           
            return years;
        }
    }
}
