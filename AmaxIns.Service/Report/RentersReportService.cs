﻿using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace AmaxIns.Service.Report
{
    public class RentersReportService : BaseService, IRentersReportService
    {
        private readonly IRentersReportRepository _repo;
        public RentersReportService(IConfiguration configuration, IRentersReportRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }

        public async Task<IEnumerable<RentersData>> GetRenterData(int Year, string month, string location)
        {
            return await _repo.GetRenterData(Year.ToString(), month, location);
        }
        public async Task<IEnumerable<ActiveCustomerData>> GetActiveCustomerData(int Year, string month, string location)
        {
            return await _repo.GetActiveCustomerData(Year.ToString(), month, location);
        }

        public string ExportExcelRenterData(IEnumerable<RentersData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Renters-Report-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 13].Merge = true;
                    workSheet.Cells[1, 1, 1, 13].Value = "**  Renters Report  **";
                    workSheet.Cells[1, 1, 1, 13].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;

                    workSheet.Cells[2, 1].Value = "ROM";
                    workSheet.Cells[2, 2].Value = "ZM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Policies";
                    workSheet.Cells[2, 5].Value = "Agency Fee";
                    workSheet.Cells[2, 6].Value = "Premium";
                    workSheet.Cells[2, 7].Value = "Goal Policies";
                    workSheet.Cells[2, 8].Value = "Goal Agency Fee";
                    workSheet.Cells[2, 9].Value = "Goal Premium";
                    workSheet.Cells[2, 10].Value = "Prior Year Policies";
                    workSheet.Cells[2, 11].Value = "Prior Year Agencyfee";
                    workSheet.Cells[2, 12].Value = "Prior Year Premium";
                    workSheet.Cells[2, 13].Value = "Total Project Policies";
                    workSheet.Cells[2, 1, 2, 13].Style.Font.Bold = true;

                    int row = 2;
                    foreach (RentersData obj in list)
                    {
                        row++;

                        if (obj.RM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RM;
                        }

                        if (obj.ZM == "ZZ_Total" && obj.RM != "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZM == "ZZ_Total" && obj.RM == "ZM_Total" && obj.AgencyName == "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }
                        if (obj.AgencyName != "ZZ_Total" && obj.ZM == "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.AgencyName == "ZZ_Total" && obj.ZM != "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.AgencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.Policies;
                        workSheet.Cells[row, 5].Value = obj.agencyfee;
                        workSheet.Cells[row, 6].Value = obj.Premium;
                        workSheet.Cells[row, 7].Value = obj.Goal_Policies;
                        workSheet.Cells[row, 8].Value = obj.Goal_AgencyFee;
                        workSheet.Cells[row, 9].Value = obj.Goal_Premium;
                        workSheet.Cells[row, 10].Value = obj.PriorYear_Policies;
                        workSheet.Cells[row, 11].Value = obj.PriorYear_agencyfee;
                        workSheet.Cells[row, 12].Value = obj.PriorYear_Premium;
                        workSheet.Cells[row, 13].Value = obj.Total_Projecte_Policies;
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return fileName;
        }

        public string ExportExcelActiveCustomer(IEnumerable<ActiveCustomerData> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Active-Customer-Data-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 9].Merge = true;
                    workSheet.Cells[1, 1, 1, 9].Value = "**  Active Customer Data  **";
                    workSheet.Cells[1, 1, 1, 9].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 9].Style.Font.Bold = true;

                    workSheet.Cells[2, 1].Value = "ROM";
                    workSheet.Cells[2, 2].Value = "ZM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Market";
                    workSheet.Cells[2, 5].Value = "Active Customer";
                    workSheet.Cells[2, 6].Value = "Prior Year Active Customer";
                    workSheet.Cells[2, 7].Value = "Active Customers Goals";
                    workSheet.Cells[2, 8].Value = "Active Customer vs Goal%";
                    workSheet.Cells[2, 9].Value = "Active Customer vs Prior Year%";
                    workSheet.Cells[2, 1, 2, 9].Style.Font.Bold = true;

                    int row = 2;
                    foreach (ActiveCustomerData obj in list)
                    {
                        row++;

                        if (obj.RM == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RM;
                        }

                        if (obj.ZM == "ZZZZ_Total" && obj.RM != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZM == "ZZZZ_Total" && obj.RM == "ZZZZ_Total" && obj.AgencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }
                        if (obj.AgencyName != "ZZZZ_Total" && obj.ZM == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.AgencyName == "ZZZZ_Total" && obj.ZM != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.AgencyName;
                        }
                        workSheet.Cells[row, 4].Value = obj.Market;
                        workSheet.Cells[row, 5].Value = obj.ActiveCustomer;
                        workSheet.Cells[row, 6].Value = obj.PriorYearActiveCustomer;
                        workSheet.Cells[row, 7].Value = obj.Active_Customers_Goals;
                        workSheet.Cells[row, 8].Value = obj.ActiveCustomersVsGoals;
                        workSheet.Cells[row, 9].Value = obj.ActiveCustomersvsPriorYear;
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return fileName;
        }

    }
}
