﻿using AmaxIns.RepositoryContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Report
{
    public class CarrierMixNewService : BaseService, ICarrierMixNewService
    {
        private readonly ICarrierMixNewRepository _repo;
        public CarrierMixNewService(IConfiguration configuration, ICarrierMixNewRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }
        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixI(int Year, string month)
        {
            return await _repo.CarrierMixI(Year, month);
        }
        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixPremium(int Year, string month)
        {
            return await _repo.CarrierMixPremium(Year, month);
        }
        public async Task<IEnumerable<IDictionary<string, object>>> CarrierMixAgencyFee(int Year, string month)
        {
            return await _repo.CarrierMixAgencyFee(Year, month);
        }

        public string ExportExcelCarrierMix(IEnumerable<IDictionary<string, object>> list, string pathRoot,int reportType)
        {
            string fileName = string.Empty;
            try
            {
                string _headerTitle = "Carrier Mix Policy";
                fileName = $"Carrier-Mix-Policy-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                if (reportType==1)
                {
                    _headerTitle = "Carrier Mix Premium";
                    fileName = $"Carrier-Mix-Premium-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                }
                else if(reportType==2)
                {
                    _headerTitle = "Carrier Mix AgencyFee";
                    fileName = $"Carrier-Mix-AgencyFee-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                }
                
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    int col = 1;
                    foreach (var item in list)
                    {
                        foreach (var k in item.Keys)
                        {
                            workSheet.Cells[2, col].Value = k.Contains("%") ? "%" : k.ToString();
                            col = col + 1;
                        }
                        break;
                    }

                    workSheet.Cells[1, 1, 1, col].Merge = true;
                    workSheet.Cells[1, 1, 1, col].Value = "** "+ _headerTitle + " **";
                    workSheet.Cells[1, 1, 1, col].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, col].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, col].Style.Font.Bold = true;

                   
                    col = 1;
                    int row = 2;
                    foreach (var item in list)
                    {
                        col = 0;
                        row++;
                        foreach (var k in item)
                        {
                            if(k.Value?.ToString()=="Total")
                            {
                                row--;
                                break;
                            }
                            col = col + 1;
                            workSheet.Cells[row, col].Value = k.Value;// +" "+ (k.Key.Contains("%") ? "%" : "");
                        }
                    }
                    string flag = "";
                    foreach (var item in list)
                    {
                        col = 0;
                        foreach (var k in item)
                        {
                            if (k.Value?.ToString() == "Total")
                            {
                                flag = "Total";
                                row++;
                            }
                            if(flag== "Total")
                            {
                                col = col + 1;
                                workSheet.Cells[row, col].Value = k.Value;
                            }
                        }
                        if (flag == "Total")
                        {
                            break;
                        }
                    }

                    
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
    }
}
