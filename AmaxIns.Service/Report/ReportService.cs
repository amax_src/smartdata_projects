﻿using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Report
{
    public class ReportService : BaseService, IReportService
    {
        private readonly IReportRepository _repo;
        public ReportService(IConfiguration configuration, IReportRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }

        #region AvgAgencyFee
        public async Task<IEnumerable<AvgAgencyFee>> GetAvgAgencyFees(string Year, string month, string location)
        {
            return await _repo.GetAvgAgencyFees(Year, month, location);
        }
        public async Task<IEnumerable<AvgAgencyFeeII>> GetAvgAgencyFeesII(string Year, string month, string location)
        {
            return await _repo.GetAvgAgencyFeesII(Year, month, location);
        }
        public async Task<IEnumerable<AvgAgencyFeeIII>> GetAvgAgencyFeesIII(string Year, string month, string location)
        {
            return await _repo.GetAvgAgencyFeesIII(Year, month, location);
        }
        #endregion

        #region AgentPerformanceReportMethods
        // Agent Performance Report Methods
        public async Task<IEnumerable<AgencyBreakdownI>> GetAgencyBreakdownI(string Year, string month, string location)
        {
            return await _repo.GetAgencyBreakdownI(Year, month, location);
        }
        public async Task<IEnumerable<AgencyBreakdownII>> GetAgencyBreakdownII(string Year, string month, string location)
        {
            return await _repo.GetAgencyBreakdownII(Year, month, location);
        }
        public async Task<IEnumerable<AgencyBreakdownIII>> GetAgencyBreakdownIII(string Year, string month, string location)
        {
            return await _repo.GetAgencyBreakdownIII(Year, month, location);
        }
        #endregion
       

        #region OverTimeReport
        public async Task<IEnumerable<OvertimeI>> GetOvertimeI(string Year, string month, string location)
        {
            return await _repo.GetOvertimeI(Year, month, location);
        }
        public async Task<IEnumerable<OvertimeII>> GetOvertimeII(string Year, string month, string location)
        {
            return await _repo.GetOvertimeII(Year, month, location);
        }
        public async Task<IEnumerable<OvertimeIII>> GetOvertimeIII(string Year, string month, string location)
        {
            return await _repo.GetOvertimeIII(Year, month, location);
        }
        #endregion

     

        // Tracker

        public async Task<IEnumerable<TrackerRender>> GetRenterTracker(string Year, string month, string location)
        {
            return await _repo.GetRenterTracker(Year, month, location);
        }
        public async Task<IEnumerable<TrackerProduction>> GetProductionTracker(string Year, string month, string location)
        {
            return await _repo.GetProductionTracker(Year, month, location);
        }
        public async Task<IEnumerable<TrackerActiveCustomer>> GetActiveCustomerTracker(string Year, string month, string location)
        {
            return await _repo.GetActiveCustomerTracker(Year, month, location);
        }

        public async Task<IEnumerable<AgentPerformance>> GetAgentPerformaceI(string Year, string month, string location)
        {
            return await _repo.GetAgentPerformaceI(Year, month, location);
        }

        public async Task<IEnumerable<AgentPerformance>> GetAgentPerformaceII(string Year, string month, string location)
        {
            return await _repo.GetAgentPerformaceII(Year, month, location);
        }


        #region Report_CreateExcel

        #region Avg_Agency_Fee

      
        public string ExportExcelAgencyFeeI(IEnumerable<AvgAgencyFee> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Fee-by-Office-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 8].Merge = true;
                    workSheet.Cells[1, 1, 1, 8].Value = "** Agency Fee By Office **";
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Bold = true;

                   
                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Transaction";
                    workSheet.Cells[2, 5].Value = "Policy";
                    workSheet.Cells[2, 6].Value = "Fee Per Policy";
                    workSheet.Cells[2, 7].Value = "Endorstment";
                    workSheet.Cells[2, 8].Value = "Fee Per Endorstment";
                  
                    workSheet.Cells[2, 1, 2, 8].Style.Font.Bold = true;

                    int row = 2;
                    foreach(AvgAgencyFee obj in list)
                    {
                        row++;
                        if (obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }

                        if (obj.ZM == "ZZ_Total" && obj.ROM != "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZM == "ZZ_Total" && obj.ROM == "ZM_Total" && obj.AgencyName == "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }
                        if (obj.AgencyName != "ZZ_Total" && obj.ZM == "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.AgencyName == "ZZ_Total" && obj.ZM != "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.AgencyName;
                        }

                        //workSheet.Cells[row, 1].Value = obj.ROM;
                        //workSheet.Cells[row, 2].Value = obj.ZM;
                        //workSheet.Cells[row, 3].Value = obj.AgencyName;
                        workSheet.Cells[row, 4].Value = obj.Transactions;
                        workSheet.Cells[row, 5].Value = obj.Policy;
                        workSheet.Cells[row, 6].Value = obj.FeePerPolicy;
                        workSheet.Cells[row, 7].Value = obj.Endorsment;
                        workSheet.Cells[row, 8].Value = obj.FeePerEndorstment;
                       
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelAgencyFeeII(IEnumerable<AvgAgencyFeeII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Fee-by-ZM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 7].Merge = true;
                    workSheet.Cells[1, 1, 1, 7].Value = "** Agency Fee By Office **";
                    workSheet.Cells[1, 1, 1, 7].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 7].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Transaction";
                    workSheet.Cells[2, 4].Value = "Policy";
                    workSheet.Cells[2, 5].Value = "Fee Per Policy";
                    workSheet.Cells[2, 6].Value = "Endorstment";
                    workSheet.Cells[2, 7].Value = "Fee Per Endorstment";
                   
                    workSheet.Cells[2, 1, 2, 7].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AvgAgencyFeeII obj in list)
                    {
                        row++;
                        if (obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }

                        if (obj.ZM == "ZZ_Total" && obj.ROM != "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZM == "ZZ_Total" && obj.ROM == "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }
                        //workSheet.Cells[row, 1].Value = obj.ROM;
                        //workSheet.Cells[row, 2].Value = obj.ZM;
                        //workSheet.Cells[row, 3].Value = obj.AgencyName;
                        workSheet.Cells[row, 3].Value = obj.Transactions;
                        workSheet.Cells[row, 4].Value = obj.Policy;
                        workSheet.Cells[row, 5].Value = obj.FeePerPolicy;
                        workSheet.Cells[row, 6].Value = obj.Endorsment;
                        workSheet.Cells[row, 7].Value = obj.FeePerEndorstment;
                       
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelAgencyFeeIII(IEnumerable<AvgAgencyFeeIII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Fee-by-ROM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 6].Merge = true;
                    workSheet.Cells[1, 1, 1, 6].Value = "** Agency Fee By Office **";
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 6].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "Transaction";
                    workSheet.Cells[2, 3].Value = "Policy";
                    workSheet.Cells[2, 4].Value = "Fee Per Policy";
                    workSheet.Cells[2, 5].Value = "Endorstment";
                    workSheet.Cells[2, 6].Value = "Fee Per Endorstment";
                  
                    workSheet.Cells[2, 1, 2, 7].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AvgAgencyFeeIII obj in list)
                    {
                        row++;
                        if (obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }

                        //workSheet.Cells[row, 1].Value = obj.ROM;
                        //workSheet.Cells[row, 2].Value = obj.ZM;
                        //workSheet.Cells[row, 3].Value = obj.AgencyName;
                        workSheet.Cells[row, 2].Value = obj.Transactions;
                        workSheet.Cells[row, 3].Value = obj.Policy;
                        workSheet.Cells[row, 4].Value = obj.FeePerPolicy;
                        workSheet.Cells[row, 5].Value = obj.Endorsment;
                        workSheet.Cells[row, 6].Value = obj.FeePerEndorstment;
                        
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
        #endregion

        #region AgencyBreakDownByOffice
        public string ExportExcelAgencyBreakDownByOfficeI(IEnumerable<AgencyBreakdownI> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Break-Down-By-Office-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 24].Merge = true;
                    workSheet.Cells[1, 1, 1, 24].Value = "** Agency Fee Breakdown by office  **";
                    workSheet.Cells[1, 1, 1, 24].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 24].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 24].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Avg fee";
                    workSheet.Cells[2, 5].Value = "Fee Per Policy";
                    workSheet.Cells[2, 6].Value = "Afee";
                    workSheet.Cells[2, 7].Value = "%Afee";
                    workSheet.Cells[2, 8].Value = "Afee<(1-10)";
                    workSheet.Cells[2, 9].Value = "%Afee<(1-10)";
                    workSheet.Cells[2, 10].Value = "Afee<(10-25)";
                    workSheet.Cells[2, 11].Value = "%Afee<(10-25)";
                    workSheet.Cells[2, 12].Value = "Afee<(25-50)";
                    workSheet.Cells[2, 13].Value = "%Afee<(25-50)";
                    workSheet.Cells[2, 14].Value = "Afee<(50-100)";
                    workSheet.Cells[2, 15].Value = "%Afee<(50-100)";
                    workSheet.Cells[2, 16].Value = "Afee<(100-150)";
                    workSheet.Cells[2, 17].Value = "%Afee<(100-150)";
                    workSheet.Cells[2, 18].Value = "Afee<(150-200)";
                    workSheet.Cells[2, 19].Value = "%Afee<(150-200)";
                    workSheet.Cells[2, 20].Value = "Afee<(200-500)";
                    workSheet.Cells[2, 21].Value = "%Afee<(200-500)";
                    workSheet.Cells[2, 22].Value = "Afee<(>500)";
                    workSheet.Cells[2, 23].Value = "%Afee<(>500)";
                    workSheet.Cells[2, 24].Value = "Sum Of All Buckets";
                    workSheet.Cells[2, 1, 2, 24].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AgencyBreakdownI obj in list)
                    {
                        row++;
                        
                        if(obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }

                        if(obj.ZM=="ZZ_Total" && obj.ROM!="ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if(obj.ZM == "ZZ_Total" && obj.ROM == "ZM_Total" && obj.AgencyName == "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }
                        if (obj.AgencyName != "ZZ_Total" && obj.ZM == "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.AgencyName == "ZZ_Total" && obj.ZM != "ZM_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.AgencyName;
                        }
                            
                        workSheet.Cells[row, 4].Value = obj.Avg_fee;
                        workSheet.Cells[row, 5].Value = obj.Fee_Per_Policy;
                        workSheet.Cells[row, 6].Value = obj.AfeeLessThan1;
                        workSheet.Cells[row, 7].Value = obj.PercentageAfeeLessThan1;
                        workSheet.Cells[row, 8].Value = obj.AfeeLessThan1_10;
                        workSheet.Cells[row, 9].Value = obj.PercentageAfeeLessThan1_10;
                        workSheet.Cells[row, 10].Value = obj.AfeeLessThan10_25;
                        workSheet.Cells[row, 11].Value = obj.PercentageAfeeLessThan10_25;
                        workSheet.Cells[row, 12].Value = obj.AfeeLessThan25_50;
                        workSheet.Cells[row, 13].Value = obj.PercentageAfeeLessThan25_50;
                        workSheet.Cells[row, 14].Value = obj.AfeeLessThan50_100;
                        workSheet.Cells[row, 15].Value = obj.PercentageAfeeLessThan50_100;
                        workSheet.Cells[row, 16].Value = obj.AfeeLessThan100_150;
                        workSheet.Cells[row, 17].Value = obj.PercentageAfeeLessThan100_150;
                        workSheet.Cells[row, 18].Value = obj.AfeeLessThan150_200;
                        workSheet.Cells[row, 19].Value = obj.PercentageAfeeLessThan150_200;
                        workSheet.Cells[row, 20].Value = obj.AfeeLessThan200_500;
                        workSheet.Cells[row, 21].Value = obj.PercentageAfeeLessThan200_500;
                        workSheet.Cells[row, 22].Value = obj.AfeeGreaterThan500;
                        workSheet.Cells[row, 23].Value = obj.PercentageAfeeGreaterThan500;
                        workSheet.Cells[row, 24].Value = obj.SumOfAllBuckets;
                       
                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelAgencyBreakDownByOfficeII(IEnumerable<AgencyBreakdownII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Fee-Breakdown-ZM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 23].Merge = true;
                    workSheet.Cells[1, 1, 1, 23].Value = "** Agency Fee Breakdown ZM  **";
                    workSheet.Cells[1, 1, 1, 23].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 23].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 23].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Avg fee";
                    workSheet.Cells[2, 4].Value = "Fee Per Policy";
                    workSheet.Cells[2, 5].Value = "Afee";
                    workSheet.Cells[2, 6].Value = "%Afee";
                    workSheet.Cells[2, 7].Value = "Afee<(1-10)";
                    workSheet.Cells[2, 8].Value = "%Afee<(1-10)";
                    workSheet.Cells[2, 9].Value = "Afee<(10-25)";
                    workSheet.Cells[2, 10].Value = "%Afee<(10-25)";
                    workSheet.Cells[2, 11].Value = "Afee<(25-50)";
                    workSheet.Cells[2, 12].Value = "%Afee<(25-50)";
                    workSheet.Cells[2, 13].Value = "Afee<(50-100)";
                    workSheet.Cells[2, 14].Value = "%Afee<(50-100)";
                    workSheet.Cells[2, 15].Value = "Afee<(100-150)";
                    workSheet.Cells[2, 16].Value = "%Afee<(100-150)";
                    workSheet.Cells[2, 17].Value = "Afee<(150-200)";
                    workSheet.Cells[2, 18].Value = "%Afee<(150-200)";
                    workSheet.Cells[2, 19].Value = "Afee<(200-500)";
                    workSheet.Cells[2, 20].Value = "%Afee<(200-500)";
                    workSheet.Cells[2, 21].Value = "Afee<(>500)";
                    workSheet.Cells[2, 22].Value = "%Afee<(>500)";
                    workSheet.Cells[2, 23].Value = "Sum Of All Buckets";
                    workSheet.Cells[2, 1, 2, 23].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AgencyBreakdownII obj in list)
                    {
                        row++;

                        if (obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }

                        if (obj.ZM == "ZZ_Total" && obj.ROM != "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZM == "ZZ_Total" && obj.ROM == "ZM_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZM;
                        }

                        workSheet.Cells[row, 3].Value = obj.Avg_fee;
                        workSheet.Cells[row, 4].Value = obj.Fee_Per_Policy;
                        workSheet.Cells[row, 5].Value = obj.AfeeLessThan1;
                        workSheet.Cells[row, 6].Value = obj.PercentageAfeeLessThan1;
                        workSheet.Cells[row, 7].Value = obj.AfeeLessThan1_10;
                        workSheet.Cells[row, 8].Value = obj.PercentageAfeeLessThan1_10;
                        workSheet.Cells[row, 9].Value = obj.AfeeLessThan10_25;
                        workSheet.Cells[row, 10].Value = obj.PercentageAfeeLessThan10_25;
                        workSheet.Cells[row, 11].Value = obj.AfeeLessThan25_50;
                        workSheet.Cells[row, 12].Value = obj.PercentageAfeeLessThan25_50;
                        workSheet.Cells[row, 13].Value = obj.AfeeLessThan50_100;
                        workSheet.Cells[row, 14].Value = obj.PercentageAfeeLessThan50_100;
                        workSheet.Cells[row, 15].Value = obj.AfeeLessThan100_150;
                        workSheet.Cells[row, 16].Value = obj.PercentageAfeeLessThan100_150;
                        workSheet.Cells[row, 17].Value = obj.AfeeLessThan150_200;
                        workSheet.Cells[row, 18].Value = obj.PercentageAfeeLessThan150_200;
                        workSheet.Cells[row, 19].Value = obj.AfeeLessThan200_500;
                        workSheet.Cells[row, 20].Value = obj.PercentageAfeeLessThan200_500;
                        workSheet.Cells[row, 21].Value = obj.AfeeGreaterThan500;
                        workSheet.Cells[row, 22].Value = obj.PercentageAfeeGreaterThan500;
                        workSheet.Cells[row, 23].Value = obj.SumOfAllBuckets;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelAgencyBreakDownByOfficeIII(IEnumerable<AgencyBreakdownIII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agency-Fee-Breakdown-ROM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 22].Merge = true;
                    workSheet.Cells[1, 1, 1, 22].Value = "** Agency Fee Breakdown ROM  **";
                    workSheet.Cells[1, 1, 1, 22].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 22].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 22].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "Avg fee";
                    workSheet.Cells[2, 3].Value = "Fee Per Policy";
                    workSheet.Cells[2, 4].Value = "Afee";
                    workSheet.Cells[2, 5].Value = "%Afee";
                    workSheet.Cells[2, 6].Value = "Afee<(1-10)";
                    workSheet.Cells[2, 7].Value = "%Afee<(1-10)";
                    workSheet.Cells[2, 8].Value = "Afee<(10-25)";
                    workSheet.Cells[2, 9].Value = "%Afee<(10-25)";
                    workSheet.Cells[2, 10].Value = "Afee<(25-50)";
                    workSheet.Cells[2, 11].Value = "%Afee<(25-50)";
                    workSheet.Cells[2, 12].Value = "Afee<(50-100)";
                    workSheet.Cells[2, 13].Value = "%Afee<(50-100)";
                    workSheet.Cells[2, 14].Value = "Afee<(100-150)";
                    workSheet.Cells[2, 15].Value = "%Afee<(100-150)";
                    workSheet.Cells[2, 16].Value = "Afee<(150-200)";
                    workSheet.Cells[2, 17].Value = "%Afee<(150-200)";
                    workSheet.Cells[2, 18].Value = "Afee<(200-500)";
                    workSheet.Cells[2, 19].Value = "%Afee<(200-500)";
                    workSheet.Cells[2, 20].Value = "Afee<(>500)";
                    workSheet.Cells[2, 21].Value = "%Afee<(>500)";
                    workSheet.Cells[2, 22].Value = "Sum Of All Buckets";
                    workSheet.Cells[2, 1, 2, 22].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AgencyBreakdownIII obj in list)
                    {
                        row++;

                        if (obj.ROM == "ZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.ROM;
                        }
                        workSheet.Cells[row, 2].Value = obj.Avg_fee;
                        workSheet.Cells[row, 3].Value = obj.Fee_Per_Policy;
                        workSheet.Cells[row, 4].Value = obj.AfeeLessThan1;
                        workSheet.Cells[row, 5].Value = obj.PercentageAfeeLessThan1;
                        workSheet.Cells[row, 6].Value = obj.AfeeLessThan1_10;
                        workSheet.Cells[row, 7].Value = obj.PercentageAfeeLessThan1_10;
                        workSheet.Cells[row, 8].Value = obj.AfeeLessThan10_25;
                        workSheet.Cells[row, 9].Value = obj.PercentageAfeeLessThan10_25;
                        workSheet.Cells[row, 10].Value = obj.AfeeLessThan25_50;
                        workSheet.Cells[row, 11].Value = obj.PercentageAfeeLessThan25_50;
                        workSheet.Cells[row, 12].Value = obj.AfeeLessThan50_100;
                        workSheet.Cells[row, 13].Value = obj.PercentageAfeeLessThan50_100;
                        workSheet.Cells[row, 14].Value = obj.AfeeLessThan100_150;
                        workSheet.Cells[row, 15].Value = obj.PercentageAfeeLessThan100_150;
                        workSheet.Cells[row, 16].Value = obj.AfeeLessThan150_200;
                        workSheet.Cells[row, 17].Value = obj.PercentageAfeeLessThan150_200;
                        workSheet.Cells[row, 18].Value = obj.AfeeLessThan200_500;
                        workSheet.Cells[row, 19].Value = obj.PercentageAfeeLessThan200_500;
                        workSheet.Cells[row, 20].Value = obj.AfeeGreaterThan500;
                        workSheet.Cells[row, 21].Value = obj.PercentageAfeeGreaterThan500;
                        workSheet.Cells[row, 22].Value = obj.SumOfAllBuckets;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelGetOvertimeI(IEnumerable<OvertimeI> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Overtime-by-Office-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 16].Merge = true;
                    workSheet.Cells[1, 1, 1, 16].Value = "** Overtime by Office  **";
                    workSheet.Cells[1, 1, 1, 16].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 16].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 16].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Agent Name";
                    workSheet.Cells[2, 5].Value = "Total";
                    workSheet.Cells[2, 6].Value = "Regular";
                    workSheet.Cells[2, 7].Value = "OT";
                    workSheet.Cells[2, 8].Value = "Policies";
                    workSheet.Cells[2, 9].Value = "Transactions";
                    workSheet.Cells[2, 10].Value = "OT Per Policies";
                    workSheet.Cells[2, 11].Value = "OT Per Transaction";
                    workSheet.Cells[2, 12].Value = "Policies In OT";
                    workSheet.Cells[2, 13].Value = "Transactions In OT";
                    workSheet.Cells[2, 14].Value = "Total hrs per Policy";
                    workSheet.Cells[2, 15].Value = "Total Hrs per Transaction";
                    workSheet.Cells[2, 16].Value = "Months Till Today";
                  
                    workSheet.Cells[2, 1, 2, 16].Style.Font.Bold = true;

                    int row = 2;
                    foreach (OvertimeI obj in list)
                    {
                        row++;

                        if (obj.RegionalManagersId == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 1].Value = "Grand Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManagers;
                        }

                        if (obj.ZonalManagers == "ZZZZZZ" && obj.RegionalManagersId != "ZZZZZZ")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManagers == "ZZZZZZ" && obj.RegionalManagersId == "ZZZZZZ" && obj.AgencyName == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZonalManagers;
                        }

                        if (obj.AgencyName != "ZZZZZZ" && obj.ZonalManagers == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.AgencyName == "ZZZZZZ" && obj.ZonalManagers != "ZZZZZZ")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.AgencyName;
                        }

                         if (obj.AgentName == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 4].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 4].Value = obj.AgentName;
                        }
                        workSheet.Cells[row, 5].Value = obj.Total;
                        workSheet.Cells[row, 6].Value = obj.Regular;
                        workSheet.Cells[row, 7].Value = obj.OT;
                        workSheet.Cells[row, 8].Value = obj.Policies;
                        workSheet.Cells[row, 9].Value = obj.Transactions;
                        workSheet.Cells[row, 10].Value = obj.OTPerPolicies;
                        workSheet.Cells[row, 11].Value = obj.OTPerTransaction;
                        workSheet.Cells[row, 12].Value = obj.PoliciesInOT;
                        workSheet.Cells[row, 13].Value = obj.TransactionsInOT;
                        workSheet.Cells[row, 14].Value = obj.TotalhrsperPolicy;
                        workSheet.Cells[row, 15].Value = obj.TotalhrsperTransaction;
                        workSheet.Cells[row, 16].Value = obj.MonthsTillToday;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelGetOvertimeII(IEnumerable<OvertimeII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Overtime-ZM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 14].Merge = true;
                    workSheet.Cells[1, 1, 1, 14].Value = "** Overtime ZM  **";
                    workSheet.Cells[1, 1, 1, 14].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 14].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 14].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Total";
                    workSheet.Cells[2, 4].Value = "Regular";
                    workSheet.Cells[2, 5].Value = "OT";
                    workSheet.Cells[2, 6].Value = "Policies";
                    workSheet.Cells[2, 7].Value = "Transactions";
                    workSheet.Cells[2, 8].Value = "OT Per Policies";
                    workSheet.Cells[2, 9].Value = "OT Per Transaction";
                    workSheet.Cells[2, 10].Value = "Policies In OT";
                    workSheet.Cells[2, 11].Value = "Transactions In OT";
                    workSheet.Cells[2, 12].Value = "Total hrs per Policy";
                    workSheet.Cells[2, 13].Value = "Total Hrs per Transaction";
                    workSheet.Cells[2, 14].Value = "Months Till Today";
                    workSheet.Cells[2, 1, 2, 14].Style.Font.Bold = true;

                    int row = 2;
                    foreach (OvertimeII obj in list)
                    {
                        row++;

                        if (obj.RegionalManagersId == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 1].Value = "Grand Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManagers;
                        }

                        if (obj.ZonalManagers == "ZZZZZZ" && obj.RegionalManagersId != "ZZZZZZ")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManagers == "ZZZZZZ" && obj.RegionalManagersId == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.ZonalManagers;
                        }

                      
                        workSheet.Cells[row, 3].Value = obj.Total;
                        workSheet.Cells[row, 4].Value = obj.Regular;
                        workSheet.Cells[row, 5].Value = obj.OT;
                        workSheet.Cells[row, 6].Value = obj.Policies;
                        workSheet.Cells[row, 7].Value = obj.Transactions;
                        workSheet.Cells[row, 8].Value = obj.OTPerPolicies;
                        workSheet.Cells[row, 9].Value = obj.OTPerTransaction;
                        workSheet.Cells[row, 10].Value = obj.PoliciesInOT;
                        workSheet.Cells[row, 11].Value = obj.TransactionsInOT;
                        workSheet.Cells[row, 12].Value = obj.TotalhrsperPolicy;
                        workSheet.Cells[row, 13].Value = obj.TotalhrsperTransaction;
                        workSheet.Cells[row, 14].Value = obj.MonthsTillToday;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportExcelGetOvertimeIII(IEnumerable<OvertimeIII> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Overtime ROM-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 13].Merge = true;
                    workSheet.Cells[1, 1, 1, 13].Value = "** Overtime ROM  **";
                    workSheet.Cells[1, 1, 1, 13].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "Total";
                    workSheet.Cells[2, 3].Value = "Regular";
                    workSheet.Cells[2, 4].Value = "OT";
                    workSheet.Cells[2, 5].Value = "Policies";
                    workSheet.Cells[2, 6].Value = "Transactions";
                    workSheet.Cells[2, 7].Value = "OT Per Policies";
                    workSheet.Cells[2, 8].Value = "OT Per Transaction";
                    workSheet.Cells[2, 9].Value = "Policies In OT";
                    workSheet.Cells[2, 10].Value = "Transactions In OT";
                    workSheet.Cells[2, 11].Value = "Total hrs per Policy";
                    workSheet.Cells[2, 12].Value = "Total Hrs per Transaction";
                    workSheet.Cells[2, 13].Value = "Months Till Today";
                    workSheet.Cells[2, 1, 2, 13].Style.Font.Bold = true;

                    int row = 2;
                    foreach (OvertimeIII obj in list)
                    {
                        row++;

                        if (obj.RegionalManagersId == "ZZZZZZ")
                        {
                            workSheet.Cells[row, 1].Value = "Grand Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManagers;
                        }
                        workSheet.Cells[row, 2].Value = obj.Total;
                        workSheet.Cells[row, 3].Value = obj.Regular;
                        workSheet.Cells[row, 4].Value = obj.OT;
                        workSheet.Cells[row, 5].Value = obj.Policies;
                        workSheet.Cells[row, 6].Value = obj.Transactions;
                        workSheet.Cells[row, 7].Value = obj.OTPerPolicies;
                        workSheet.Cells[row, 8].Value = obj.OTPerTransaction;
                        workSheet.Cells[row, 9].Value = obj.PoliciesInOT;
                        workSheet.Cells[row, 10].Value = obj.TransactionsInOT;
                        workSheet.Cells[row, 11].Value = obj.TotalhrsperPolicy;
                        workSheet.Cells[row, 12].Value = obj.TotalhrsperTransaction;
                        workSheet.Cells[row, 13].Value = obj.MonthsTillToday;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
        #endregion


        #region Tracker_Excel

        public string ExportRenterTracker(IEnumerable<TrackerRender> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Renter-Tracker-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 18].Merge = true;
                    workSheet.Cells[1, 1, 1, 18].Value = "** Renter Tracker **";
                    workSheet.Cells[1, 1, 1, 18].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 18].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 18].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "MTD Policies";
                    workSheet.Cells[2, 5].Value = "MTD Agencyfee";
                    workSheet.Cells[2, 6].Value = "MTD_Premium";
                    workSheet.Cells[2, 7].Value = "EOM_Policies";
                    workSheet.Cells[2, 8].Value = "EOM_agencyfee";
                    workSheet.Cells[2, 9].Value = "EOM_Premium";
                    workSheet.Cells[2, 10].Value = "Goal_Policies";
                    workSheet.Cells[2, 11].Value = "Goal_AgencyFee";
                    workSheet.Cells[2, 12].Value = "Goal_Premium";
                    workSheet.Cells[2, 13].Value = "EOM_Policies VS GOAL_Policies +/-%";
                    workSheet.Cells[2, 14].Value = "EOM_AgencyFee VS GOAL_AgencyFee +/-%";
                    workSheet.Cells[2, 15].Value = "EOM_Premium VS GOAL_Premium +/-%";
                    workSheet.Cells[2, 16].Value = "EOM_Policies VS GOAL_Policies +/-%NO";
                    workSheet.Cells[2, 17].Value = "EOM_AgencyFee VS GOAL_AgencyFee +/-%NO";
                    workSheet.Cells[2, 18].Value = "EOM_Premium VS GOAL_Premium +/-%NO";
                   

                    workSheet.Cells[2, 1, 2, 18].Style.Font.Bold = true;

                    int row = 2;
                    foreach (TrackerRender obj in list)
                    {
                        row++;
                        if (obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManager;
                        }

                        if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total" && obj.agencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.RegionalManager;
                        }
                        if (obj.agencyName != "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.agencyName == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.agencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.MTD_Policies;
                        workSheet.Cells[row, 5].Value = obj.MTD_agencyfee;
                        workSheet.Cells[row, 6].Value = obj.MTD_Premium;
                        workSheet.Cells[row, 7].Value = obj.EOM_Policies;
                        workSheet.Cells[row, 8].Value = obj.EOM_agencyfee;
                        workSheet.Cells[row, 9].Value = obj.EOM_Premium;
                        workSheet.Cells[row, 10].Value = obj.Goal_Policies;
                        workSheet.Cells[row, 11].Value = obj.Goal_AgencyFee;
                        workSheet.Cells[row, 12].Value = obj.Goal_Premium;
                        workSheet.Cells[row, 13].Value = obj.EOM_PoliciesVSGOAL_Policies;
                        workSheet.Cells[row, 14].Value = obj.EOM_AgencyFeeVSGOAL_AgencyFee;
                        workSheet.Cells[row, 15].Value = obj.EOM_PremiumVSGOAL_Premium;
                        workSheet.Cells[row, 16].Value = obj.EOM_PoliciesVSGOAL_PoliciesNO;
                        workSheet.Cells[row, 17].Value = obj.EOM_AgencyFeeVSGOAL_AgencyFeeNO;
                        workSheet.Cells[row, 18].Value = obj.EOM_PremiumVSGOAL_PremiumNO;
                        

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportProductionTracker(IEnumerable<TrackerProduction> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Production-Tracker-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 18].Merge = true;
                    workSheet.Cells[1, 1, 1, 18].Value = "** Production Tracker **";
                    workSheet.Cells[1, 1, 1, 18].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 18].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 18].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "MTD Policies";
                    workSheet.Cells[2, 5].Value = "MTD Agencyfee";
                    workSheet.Cells[2, 6].Value = "MTD_Premium";
                    workSheet.Cells[2, 7].Value = "EOM_Policies";
                    workSheet.Cells[2, 8].Value = "EOM_agencyfee";
                    workSheet.Cells[2, 9].Value = "EOM_Premium";
                    workSheet.Cells[2, 10].Value = "Goal_Policies";
                    workSheet.Cells[2, 11].Value = "Goal_AgencyFee";
                    workSheet.Cells[2, 12].Value = "Goal_Premium";
                    workSheet.Cells[2, 13].Value = "EOM_Policies VS GOAL_Policies +/-%";
                    workSheet.Cells[2, 14].Value = "EOM_AgencyFee VS GOAL_AgencyFee +/-%";
                    workSheet.Cells[2, 15].Value = "EOM_Premium VS GOAL_Premium +/-%";
                    workSheet.Cells[2, 16].Value = "EOM_Policies VS GOAL_Policies +/-%NO";
                    workSheet.Cells[2, 17].Value = "EOM_AgencyFee VS GOAL_AgencyFee +/-%NO";
                    workSheet.Cells[2, 18].Value = "EOM_Premium VS GOAL_Premium +/-%NO";


                    workSheet.Cells[2, 1, 2, 18].Style.Font.Bold = true;

                    int row = 2;
                    foreach (TrackerProduction obj in list)
                    {
                        row++;
                        if (obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManager;
                        }

                        if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total" && obj.agencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.RegionalManager;
                        }
                        if (obj.agencyName != "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.agencyName == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.agencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.MTD_Policies;
                        workSheet.Cells[row, 5].Value = obj.MTD_agencyfee;
                        workSheet.Cells[row, 6].Value = obj.MTD_Premium;
                        workSheet.Cells[row, 7].Value = obj.EOM_Policies;
                        workSheet.Cells[row, 8].Value = obj.EOM_agencyfee;
                        workSheet.Cells[row, 9].Value = obj.EOM_Premium;
                        workSheet.Cells[row, 10].Value = obj.Goal_Policies;
                        workSheet.Cells[row, 11].Value = obj.Goal_AgencyFee;
                        workSheet.Cells[row, 12].Value = obj.Goal_Premium;
                        workSheet.Cells[row, 13].Value = obj.EOM_PoliciesVSGOAL_Policies;
                        workSheet.Cells[row, 14].Value = obj.EOM_AgencyFeeVSGOAL_AgencyFee;
                        workSheet.Cells[row, 15].Value = obj.EOM_PremiumVSGOAL_Premium;
                        workSheet.Cells[row, 16].Value = obj.EOM_PoliciesVSGOAL_PoliciesNO;
                        workSheet.Cells[row, 17].Value = obj.EOM_AgencyFeeVSGOAL_AgencyFeeNO;
                        workSheet.Cells[row, 18].Value = obj.EOM_PremiumVSGOAL_PremiumNO;


                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportActiveCustomerTracker(IEnumerable<TrackerActiveCustomer> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Production-Tracker-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 8].Merge = true;
                    workSheet.Cells[1, 1, 1, 8].Value = "** Production Tracker **";
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 8].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 8].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Month End Active Customers";
                    workSheet.Cells[2, 5].Value = "EOM Pacing Policies";
                    workSheet.Cells[2, 6].Value = "Goals";
                    workSheet.Cells[2, 7].Value = "EOM Pacing VS Goal +/-%";
                    workSheet.Cells[2, 8].Value = "EOM Pacing VS Goal +/-%No";

                    workSheet.Cells[2, 1, 2, 8].Style.Font.Bold = true;

                    int row = 2;
                    foreach (TrackerActiveCustomer obj in list)
                    {
                        row++;
                        if (obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManager;
                        }

                        if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManager == "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total" && obj.agencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.RegionalManager;
                        }
                        if (obj.agencyName != "ZZZZ_Total" && obj.RegionalManager == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.agencyName == "ZZZZ_Total" && obj.RegionalManager != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.agencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.MonthEndActiveCustomersPOLICIES;
                        workSheet.Cells[row, 5].Value = obj.EOMPacingPolicies;
                        workSheet.Cells[row, 6].Value = obj.Goals;
                        workSheet.Cells[row, 7].Value = obj.EOMPacingVSGoalPolicies;
                        workSheet.Cells[row, 8].Value = obj.EOMPacingVSGoalNoPolicies;
                       


                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        #endregion


        #region AgentPerformaceExcel

        public string ExportAgentPerformaceI(IEnumerable<AgentPerformance> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Agent-Performance-by-Office-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 12].Merge = true;
                    workSheet.Cells[1, 1, 1, 12].Value = "** Agent Performance by Office **";
                    workSheet.Cells[1, 1, 1, 12].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 12].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Agent Name";
                    workSheet.Cells[2, 5].Value = "Policies";
                    workSheet.Cells[2, 6].Value = "Agency Fee";
                    workSheet.Cells[2, 7].Value = "Premium";
                    workSheet.Cells[2, 8].Value = "Transaction";
                    workSheet.Cells[2, 9].Value = "Pol Score";
                    workSheet.Cells[2, 10].Value = "Afee Score";
                    workSheet.Cells[2, 11].Value = "Transaction Score";
                    workSheet.Cells[2, 12].Value = "Total Score 100";
                   


                    workSheet.Cells[2, 1, 2, 12].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AgentPerformance obj in list)
                    {
                        row++;
                        if (obj.RegionalManagers == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManagers;
                        }

                        if (obj.ZonalManagers == "ZZZZ_Total" && obj.RegionalManagers != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManagers == "ZZZZ_Total" && obj.RegionalManagers == "ZZZZ_Total" && obj.agencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.RegionalManagers;
                        }
                        if (obj.agencyName != "ZZZZ_Total" && obj.RegionalManagers == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.agencyName == "ZZZZ_Total" && obj.RegionalManagers != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.agencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.agentName;
                        workSheet.Cells[row, 5].Value = obj.policies;
                        workSheet.Cells[row, 6].Value = obj.agencyfee;
                        workSheet.Cells[row, 7].Value = obj.Premium;
                        workSheet.Cells[row, 8].Value = obj.transaction;
                        workSheet.Cells[row, 9].Value = obj.PolScore;
                        workSheet.Cells[row, 10].Value = obj.AfeeScore;
                        workSheet.Cells[row, 11].Value = obj.TransactionScore;
                        workSheet.Cells[row, 12].Value = obj.TotalScore100;
                      


                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        public string ExportAgentPerformaceII(IEnumerable<AgentPerformance> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Top-Agents-by-Zone-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 12].Merge = true;
                    workSheet.Cells[1, 1, 1, 12].Value = "** Top agents by Zone **";
                    workSheet.Cells[1, 1, 1, 12].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 12].Style.Font.Bold = true;


                    workSheet.Cells[2, 1].Value = "RSM";
                    workSheet.Cells[2, 2].Value = "DSM";
                    workSheet.Cells[2, 3].Value = "Agency Name";
                    workSheet.Cells[2, 4].Value = "Agent Name";
                    workSheet.Cells[2, 5].Value = "Policies";
                    workSheet.Cells[2, 6].Value = "Agency Fee";
                    workSheet.Cells[2, 7].Value = "Premium";
                    workSheet.Cells[2, 8].Value = "Transaction";
                    workSheet.Cells[2, 9].Value = "Pol Score";
                    workSheet.Cells[2, 10].Value = "Afee Score";
                    workSheet.Cells[2, 11].Value = "Transaction Score";
                    workSheet.Cells[2, 12].Value = "Total Score 100";



                    workSheet.Cells[2, 1, 2, 12].Style.Font.Bold = true;

                    int row = 2;
                    foreach (AgentPerformance obj in list)
                    {
                        row++;
                        if (obj.RegionalManagers == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 1].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 1].Value = obj.RegionalManagers;
                        }

                        if (obj.ZonalManagers == "ZZZZ_Total" && obj.RegionalManagers != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "Total";
                        }
                        else if (obj.ZonalManagers == "ZZZZ_Total" && obj.RegionalManagers == "ZZZZ_Total" && obj.agencyName == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 2].Value = "";
                        }
                        else
                        {
                            workSheet.Cells[row, 2].Value = obj.RegionalManagers;
                        }
                        if (obj.agencyName != "ZZZZ_Total" && obj.RegionalManagers == "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "";
                        }
                        else if (obj.agencyName == "ZZZZ_Total" && obj.RegionalManagers != "ZZZZ_Total")
                        {
                            workSheet.Cells[row, 3].Value = "Total";
                        }
                        else
                        {
                            workSheet.Cells[row, 3].Value = obj.agencyName;
                        }

                        workSheet.Cells[row, 4].Value = obj.agentName;
                        workSheet.Cells[row, 5].Value = obj.policies;
                        workSheet.Cells[row, 6].Value = obj.agencyfee;
                        workSheet.Cells[row, 7].Value = obj.Premium;
                        workSheet.Cells[row, 8].Value = obj.transaction;
                        workSheet.Cells[row, 9].Value = obj.PolScore;
                        workSheet.Cells[row, 10].Value = obj.AfeeScore;
                        workSheet.Cells[row, 11].Value = obj.TransactionScore;
                        workSheet.Cells[row, 12].Value = obj.TotalScore100;



                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }

        #endregion
        #endregion



    }
}
