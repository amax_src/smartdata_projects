﻿using AmaxIns.DataContract.Reports;
using AmaxIns.RepositoryContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Service.Report
{
    public class UniqueCreatedQuotesService : BaseService, IUniqueCreatedQuotesService
    {
        private readonly IUniqueCreatedQuotesRepository _repo;
        public UniqueCreatedQuotesService(IConfiguration configuration, IUniqueCreatedQuotesRepository reportRepo) : base(configuration)
        {
            _repo = reportRepo;
        }

        public async Task<IEnumerable<UniqueCreatedQuotes>> GetUniqueCreatedQuotes(int Year, string month, string location)
        {
            return await _repo.GetUniqueCreatedQuotes(Year, month, location);
        }

        public async Task<IEnumerable<Market>> GetAllMarket()
        {
            return await _repo.GetAllMarket();
        }

        public string ExportExcelUniqueCretedQuotes(IEnumerable<UniqueCreatedQuotes> list, string pathRoot)
        {
            string fileName = string.Empty;
            try
            {
                fileName = $"Unique-Created-Quotes-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
                FileInfo file = new FileInfo(Path.Combine(pathRoot, fileName));

                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells[1, 1, 1, 5].Merge = true;
                    workSheet.Cells[1, 1, 1, 5].Value = "** Unique Created Quotes **";
                    workSheet.Cells[1, 1, 1, 5].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#e3165b"));
                    workSheet.Cells[1, 1, 1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    workSheet.Cells[1, 1, 1, 5].Style.Font.Bold = true;

                  int  year=  list.FirstOrDefault().Years;

                    workSheet.Cells[2, 1].Value = "Market";
                    workSheet.Cells[2, 2].Value = "Agency Name";
                    workSheet.Cells[2, 3].Value = "Created Quotes "+ (year-1).ToString();
                    workSheet.Cells[2, 4].Value = "Created Quotes "+ year.ToString();
                    workSheet.Cells[2, 5].Value = "YoY+/-%";
                  

                    workSheet.Cells[2, 1, 2, 5].Style.Font.Bold = true;

                    int row = 2;
                    foreach (UniqueCreatedQuotes obj in list)
                    {
                        row++;
                        workSheet.Cells[row, 1].Value = obj.Market;
                        workSheet.Cells[row, 2].Value = obj.AgencyName;
                        workSheet.Cells[row, 3].Value = obj.createdQuotes0;
                        workSheet.Cells[row, 4].Value = obj.createdQuotes1;
                        workSheet.Cells[row, 5].Value = obj.YoY;

                    }
                    workSheet.Cells.AutoFitColumns();
                    package.Save();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return fileName;
        }
    }
}
