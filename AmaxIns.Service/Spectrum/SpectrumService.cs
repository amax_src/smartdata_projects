﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AmaxIns.DataContract.Spectrum;
using AmaxIns.RepositoryContract.Spectrum;
using AmaxIns.ServiceContract.Spectrum;
using Microsoft.Extensions.Configuration;


namespace AmaxIns.Service.Spectrum
{
    public class SpectrumService : BaseService, ISpectrumService
    {
        ISpectrumRepository _Repository;
        public SpectrumService(ISpectrumRepository Repository, IConfiguration configuration) : base(configuration)
        {
            this._Repository = Repository;
        }

        public Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumDailySummary()
        {
            return this._Repository.GetSpectrumDailySummary();
        }

        public Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumDailyDetail(string date)
        {
            return this._Repository.GetSpectrumDailyDetail(date);
        }

        public Task<IEnumerable<SpectrumDailySummaryModel>> GetSpectrumMonthlySummary()
        {
            return this._Repository.GetSpectrumMonthlySummary();
        }

        public Task<IEnumerable<SpectrumDailyDetailModel>> GetSpectrumMonthlyDetail(string month, int? agencyId)
        {
            return this._Repository.GetSpectrumMonthlyDetail(month, agencyId);
        }

        public Task<ConsolidatedCallVolumeModel>  GetSpectrumCallVolume()
        {
            return this._Repository.GetSpectrumCallVolume();
        }

        public Task<IEnumerable<CallCountModel>> GetCallCount(string date)
        {
            return this._Repository.GetCallCount(date);
        }

        public Task<IEnumerable<CallCountModel>> GetCallCountOutBound(string date)
        {
            return this._Repository.GetCallCountOutBound(date);
        }

        public Task<IEnumerable<RepeatedCallersModel>> GetRepeatedCallers(string date)
        {
            return this._Repository.GetRepeatedCallers(date);
        }
        public Task<IEnumerable<string>> GetDates()
        {
            return this._Repository.GetDates();
        }
    }
}
