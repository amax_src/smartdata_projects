// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// http://localhost:44305/api/    http://18.216.112.189:8082/api/
export const environment = {
  production: false,
  //Live
  baseApiUrl: "http://18.216.112.189:8086/api/"

  //UAT
  //baseApiUrl: "http://18.216.112.189:8095/api/"

  //Dev
  //baseApiUrl: "http://18.216.112.189:8093/api/"

  //Localhost
  //baseApiUrl: "http://localhost:44305/api/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
