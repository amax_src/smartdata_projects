export const environment = {
  production: true,
  //Live
  baseApiUrl: "http://18.216.112.189:8086/api/"

  //UAT
  //baseApiUrl: "http://18.216.112.189:8095/api/"

  //Dev
  //baseApiUrl: "http://18.216.112.189:8093/api/"

  //Localhost
  //baseApiUrl: "http://localhost:44305/api/"
};
