import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { TooltipModule, TooltipOptions } from 'ng2-tooltip-directive';
import { CalendarModule } from 'primeng/calendar';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { CustomFilterModule} from '../../Common/customFilter/custom-filter.module';

import { DashboardtwoRoutingModule } from './dashboardtwo-routing.module';
import { DashboardTwoComponent} from './dashboardtwo.component';

import { ProductService } from '../../productservice';
@NgModule({
  declarations: [DashboardTwoComponent],
  imports: [
    CommonModule,
    DashboardtwoRoutingModule
    , FormsModule
    , DropdownModule
    , ChartModule
    , TableModule
    , TooltipModule
    , CalendarModule
    , TagCloudModule
    , BrowserAnimationsModule
    , CustomFilterModule
    , MultiSelectModule
  ],
  providers: [ProductService]
})
export class DashboardtwoModule { }
