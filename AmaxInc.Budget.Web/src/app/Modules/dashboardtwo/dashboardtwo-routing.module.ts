import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardTwoComponent } from './dashboardtwo.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: 'account-detail', component: DashboardTwoComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardtwoRoutingModule { }
