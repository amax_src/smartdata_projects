import { Component, OnInit } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from '../../productservice';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { DateService } from '../../Services/dateService';
import { MonthModel } from '../../model/monthModel';
import { bugetService } from '../../Services/bugetService';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { BudgetOverspendModel } from '../../model/budgetOverspendModel';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { DownloadexcelService} from '../../Services/downloadexcel.service';
@Component({
  selector: 'dashboard-two',
  templateUrl: './dashboardtwo.component.html',
  styleUrls: ['./dashboardtwo.component.scss']
})


export class DashboardTwoComponent implements OnInit {
  showLoader: boolean;
  isAdmin: boolean = true;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  department: Array<string> = new Array<string>();
  deptSelectItem: SelectItem[] = [];
  selectedDepartment: Array<string> = new Array<string>();

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<any> = new Array<any>();


  cat: Array<string> = new Array<string>();
  catSelectItem: SelectItem[] = [];
  selectedCat: Array<string> = new Array<string>();

  buget: Array<string> = new Array<string>();
  Budget: Array<string> = new Array<string>();
  selectedBudget: Array<string> = new Array<string>();
  BudgetSelectItem: SelectItem[] = [];

  SpendingModel: Array<BudgetOverspendModel> = new Array<BudgetOverspendModel>();
  SpendingSearch: BudgetActualVsPlanDonut;

  budgetTotal: any;
  actualTotal: any;
  varianceTotal: any;
  variancePercentage: any;
  spending: any;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];


  products: Product[];
  cols: any[];
  value: number = 12;
  myOptions = {
    'autoPlacement': true,
    'show-delay': 10,
  }

  listOfDistinctCategory: any;
  userRole: any;
  constructor(private productService: ProductService, private router: Router, private dateservice: DateService,
    private _bugetService: bugetService, private authService: authenticationService, private ApiLoad: ApiLoadTimeService, private _downloadExcel: DownloadexcelService) {

  }

  ngOnInit() {
    this.userRole = this.authService.userRole === "head";
    //this.productService.getProductsSmall().then(data => this.products = data);
    this.cols = [
      { field: 'code', header: 'Code' },
      { field: 'name', header: 'Name' },
      { field: 'category', header: 'Category' },
      { field: 'quantity', header: 'Quantity' }
    ];
    this.getYears();
    this.getDept();
  }

  Navigate(e) {
    if (e === "dashboardone") {
      this.router.navigateByUrl('/budget-summary')
    }
    if (e === "dashboardthree") {
      this.router.navigateByUrl('/transactions-detail')
    }
    if (e === "dashboardfour") {
      this.router.navigateByUrl('/budget-variance-report')
    }
  }

  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date().getFullYear() - 1).toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
      //if (this.selectedBudget.length > 0) {
      //  this.getAnualData();
      //}
    });
  }

  getMonth() {
    this.monthSelectItem = [];
    this.selectedmonth = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice
      .GetMonthskeyvalue(this.selectedyear)
      .toPromise()
      .then((m) => {
        months = m;
        months.forEach((i) => {
          this.monthSelectItem.push({ label: i.name, value: i.id });
        });
        var selectedMonth = months.filter(x => x.selected == true)
        if (this.selectedyear != (new Date().getFullYear() - 1).toString()) {
          this.selectedmonth.push(selectedMonth[0].id)
        } else {
          this.selectedmonth.push(selectedMonth[0].id)
        }
      });
  }

  getDept() {
    this.deptSelectItem = [];
    this.selectedDepartment = [];
    this._bugetService.getDepartMent(this.authService.userId).subscribe(x => {
      this.department = x;

      this.department.forEach(m => {
        this.deptSelectItem.push({ label: m, value: m })
        this.selectedDepartment.push(m)
      })
      this.isAdmin = false;
      if (this.department.length > 0) {
        if (this.department.length > 2) {
          this.isAdmin = true;
        }

        this.getCat();
      }
    })
  }

  getCat() {
    this.catSelectItem = [];
    this.selectedCat = [];
    this._bugetService.getCategories("All", this.selectedDepartment).subscribe(x => {
      this.cat = x;
      this.cat.forEach(m => {
        this.catSelectItem.push({ label: m, value: m })
        this.selectedCat.push(m)
      })
      if (this.cat.length > 0) {
        this.getBudgetName();
      }
    })
  }

  getBudgetName() {
    this.BudgetSelectItem = [];
    this.selectedBudget = [];
    this._bugetService.getBudgetName("All", this.selectedCat, this.selectedDepartment).subscribe(x => {
      this.Budget = x;
      this.Budget.forEach(m => {
        this.BudgetSelectItem.push({ label: m, value: m })
        this.selectedBudget.push(m)
      })
      this.GetBudgetSpend();
    })
  }

  onYearchange(e) {
    this.getMonth();
    this.GetBudgetSpend();
  }


  onMonthchange() {
    this.GetBudgetSpend();
  }

  onDeptchange(e) {
    this.getCat();
  }

  onCatChange(e) {
    this.getBudgetName();
  }

  onBudgetChange() {
    this.GetBudgetSpend();
  }

  GetBudgetSpend() {
    this.showLoader = true;
    this.SpendingModel = new Array<BudgetOverspendModel>();
    this.SpendingSearch = new BudgetActualVsPlanDonut();
    this.SpendingSearch.year = this.selectedyear;
    this.SpendingSearch.months = this.selectedmonth;
    this.SpendingSearch.budget = this.selectedBudget
    this.SpendingSearch.departmentName = "All";
    this.SpendingSearch.department = this.selectedDepartment;
    let startTime: number = new Date().getTime();
    this._bugetService.GetBudgetSpend(this.SpendingSearch).subscribe(m => {
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Account Detail (Budget and Variance)"
      model.time = ResponseTime
      this.SpendingModel = m;
      this.listOfDistinctCategory = this.SpendingModel.map(item => item.categoryName)
        .filter((value, index, self) => self.indexOf(value) === index);

      this.budgetTotal = this.SpendingModel.reduce((subtotal, item) => subtotal + item.budget, 0);
      this.actualTotal = this.SpendingModel.reduce((subtotal, item) => subtotal + item.actual, 0);
      this.varianceTotal = this.budgetTotal - this.actualTotal;
      this.variancePercentage = (this.varianceTotal / this.budgetTotal) * 100;
      if (this.budgetTotal < this.actualTotal) {
        this.spending = 'Overspending';
      } else if (this.budgetTotal > this.actualTotal && (this.budgetTotal * .9) > this.actualTotal) {
        this.spending = 'Safe';
      }
      else {
        this.spending = 'Good';
      }

      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    }, z => {
      this.showLoader = false;
    })
  }

  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl("/");
  }

  exportExcel() {
    this._bugetService.ExportAccountDetals(this.SpendingModel).subscribe(x => {
      this._downloadExcel.downloadFile(x);
      console.log("FileName", x);
      this.showLoader = false;
    })
  }

}
