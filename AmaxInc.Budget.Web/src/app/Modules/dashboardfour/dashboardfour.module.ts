import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { TooltipModule, TooltipOptions } from 'ng2-tooltip-directive';
import { CalendarModule } from 'primeng/calendar';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomFilterModule } from '../../Common/customFilter/custom-filter.module';
import { DashboardfourRoutingModule } from './dashboardfour-routing.module';
import { DashboardFourComponent } from './dashboardfour.component';
import { MultiSelectModule } from 'primeng/multiselect';
//import { DountchartComponent} from '../../shared/dountchart/dountchart.component';

@NgModule({
  declarations: [DashboardFourComponent],
  imports: [
    CommonModule,
    DashboardfourRoutingModule
    , FormsModule
    , DropdownModule
    , ChartModule
    , TableModule
    , TooltipModule
    , CalendarModule
    , TagCloudModule
    , BrowserAnimationsModule,
    CustomFilterModule,
    MultiSelectModule,
  ]
})
export class DashboardfourModule { }
