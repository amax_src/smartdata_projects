import { Component, OnInit } from '@angular/core';
import { CloudData, CloudOptions } from 'angular-tag-cloud-module';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { bugetService } from '../../Services/bugetService';
import * as moment from 'moment';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { MonthModel } from '../../model/monthModel';
import { DateService } from '../../Services/dateService';
import { BudgetOverspendModel } from '../../model/budgetOverspendModel';
import { Product } from '../../product';
import { BudgetDonutsChartData } from '../../model/budgetdonutsChartData';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import { Directive, Input, Renderer2, ElementRef, HostListener } from '@angular/core';
import { Sort } from '../../utilities/sort';
import $ from "jquery";

@Component({
  selector: 'dashboard-four',
  templateUrl: './dashboardfour.component.html',
  styleUrls: ['./dashboardfour.component.css']
})


export class DashboardFourComponent implements OnInit {
  @Input() appSort: Array<any>;
  startDate: Date;
  endDate: Date;

  Chartdata: any;
  isAdmin: boolean = true;

  showLoader: boolean;

  isgetBudgetNameRun: boolean;
  isgetLocationRun: boolean;
  isgetUserRun: boolean;
  isgetSourceRun: boolean;
  isgetBudgetCodeRun: boolean;
  bugetLoader: boolean;
  trxLoader: boolean;

  trxList: any[] = [];
  listOfDistinctDate: any;
  listOfDistinctCatg: any;

  selectedDepartment: string;
  department: Array<string> = new Array<string>();
  deptSelectItem: SelectItem[] = [];

  location: Array<string> = new Array<string>();
  locationSelectItem: SelectItem[] = [];
  selectedLocation: Array<string> = new Array<string>();

  user: Array<string> = new Array<string>();
  userSelectItem: SelectItem[] = [];
  selectedUser: Array<string> = new Array<string>();

  source: Array<string> = new Array<string>();
  sourceSelectItem: SelectItem[] = [];
  selectedSource: Array<string> = new Array<string>();

  cat: Array<string> = new Array<string>();
  catSelectItem: SelectItem[] = [];
  selectedCat: Array<string> = new Array<string>();

  budgetcode: Array<string> = new Array<string>();
  budgetcodeSelectItem: SelectItem[] = [];
  selectedBudgetCode: Array<string> = new Array<string>();

  buget: Array<string> = new Array<string>();
  Budget: Array<string> = new Array<string>();
  selectedBudget: Array<string> = new Array<string>();
  BudgetSelectItem: SelectItem[] = [];
  isExistDepartment: boolean = false;

  TrxSearch: BudgetActualVsPlanDonut;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<any> = new Array<any>();

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  SpendingModel: Array<BudgetOverspendModel> = new Array<BudgetOverspendModel>();
  SpendingSearch: BudgetActualVsPlanDonut;

  products: Product[];
  cols: any[];
  value: number = 12;
  myOptions = {
    'autoPlacement': true,
    'show-delay': 10,
  }

  listOfDistinctCategory: any;


  donoutdata: any;
  linedata: any;
  options: any;
  donughtoptions: any;

  option: CloudOptions = {
    width: 0.8,
    height: 400,
    overflow: false,
    zoomOnHover: {
      scale: 1.2,
      transitionTime: 0.3,
      delay: 0.3
    },
    realignOnResize: true
  };

  data: CloudData[];


  SelectedMonthsearch: BudgetActualVsPlanDonut;
  LatestMonthsearch: BudgetActualVsPlanDonut;
  YTMsearch: BudgetActualVsPlanDonut;
  BudgetData: BudgetDonutsChartData;

  BudgetAnual: Array<BudgetDonutsChartData> = new Array<BudgetDonutsChartData>();

  BudgetSeletedMonth: BudgetDonutsChartData = new BudgetDonutsChartData();

  donoutdataSelectedMonth: any;
  donoutdataLastMonth: any;
  donoutdataYTM: any;

  constructor(private router: Router, private _bugetService: bugetService,private renderer: Renderer2, private authService: authenticationService,
    private dateservice: DateService, private ApiLoad: ApiLoadTimeService) {

    this.donoutdata = {
      labels: ['A', 'B'],
      datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "#bab0ac",
            "#4e79a7"

          ]
        }]
    };

    this.donughtoptions = {
      responsive: true,
      legend: {
        position: 'top',
      },
      tooltips: {
        enabled: true,
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var label = data.labels[tooltipItems.index] || '';
            return label + ': $' + data.datasets[0].data[tooltipItems.index].toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
        }
      },
    };

    this.options = {
      maintainAspectRatio: false,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var label = data.datasets[tooltipItem.datasetIndex].label || '';
            label = label + ': $' + tooltipItem.value.toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return label;
          }
        }
      },
      scales: {
        yAxes: [
          {
            display: true,
            position: 'left',
            type: "linear",
            scaleLabel: {
              display: true,
              labelString: 'USD',
              beginAtZero: true,
            },
            //yAxisID: "id1"
            id: "id1" // incorrect property name.
          },
          {
            scaleLabel: {
              display: true,
              labelString: 'Commissions',
              beginAtZero: true,
            },
            //display: false,
            display: true, // Hopefully don't have to explain this one.
            type: "linear",
            position: "right",
            gridLines: {
              display: false
            },
            //yAxisID: "id2"
            id: "id2" // incorrect property name.
          }
        ]
      }
    };
    this.donoutdataSelectedMonth = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: [
            "#bab0ac",
            "#4e79a7",
            "#e15759"
          ]
        }]
    };
    this.donoutdataLastMonth = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: [
            "#bab0ac",
            "#4e79a7",
            "#e15759"
          ]
        }]
    };
    this.donoutdataYTM = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: [
            "#bab0ac",
            "#4e79a7",
            "#e15759"
          ]
        }]
    };
    this.linedata = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [
        {
          label: 'First Dataset',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#4bc0c0'
        },
        {
          label: 'Second Dataset',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#4e79a7'
        },
        {
          label: 'Second Dataset1',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#e15759'
        }
      ]
    }
  }

  ngOnInit(): void {
    this.showLoader = false;

    this.donoutdataYTM = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [300, 50, 1200],
          backgroundColor: [
            "#bab0ac",
            "#4e79a7",
            "#e15759"
          ]
        }]
    };

    this.getYears();
    this.getDept();
  }

  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date().getFullYear() - 1).toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
      //if (this.selectedBudget.length > 0) {
      //  this.getAnualData();
      //}
    });
  }

  getMonth() {
    this.monthSelectItem = [];
    this.selectedmonth = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice
      .GetMonthskeyvalue(this.selectedyear)
      .toPromise()
      .then((m) => {
        months = m;
        months.forEach((i) => {
          this.monthSelectItem.push({ label: i.name, value: i.id });
        });
        var selectedMonth = months.filter(x => x.selected == true)
        if (this.selectedyear != (new Date().getFullYear() - 1).toString()) {
          this.selectedmonth.push(selectedMonth[0].id)
        } else {
          this.selectedmonth.push(selectedMonth[0].id)
        }
      });
  }


  getDept() {
    this.isExistDepartment = false;
    this._bugetService.getDepartMent(this.authService.userId).subscribe(x => {
      this.department = x;
      this.department.forEach(m => {
        this.deptSelectItem.push({ label: m, value: m })
      })
      this.isAdmin = false;
      if (this.department.length > 0) {
        if (this.department.length > 2) {
          this.isAdmin = true;
        }
        this.isExistDepartment = true;
        this.selectedDepartment = this.department[0];
        this.getCat();
      }
    })
  }

  getCat() {

    this.isgetBudgetNameRun = false;
    this.isgetLocationRun = false;
    this.isgetUserRun = false;
    this.isgetSourceRun = false;
    this.isgetBudgetCodeRun = false;
    this.catSelectItem = [];
    this.selectedCat = [];
    const ccdd = [];
    this._bugetService.getCategories(this.selectedDepartment, this.department).subscribe(x => {
      this.cat = x;
      this.cat.forEach(m => {
        this.catSelectItem.push({ label: m, value: m })
        this.selectedCat.push(m)
      })
      if (this.cat.length > 0) {
        this.data = ccdd;
        this.getBudgetName();
        this.getLocation();
        this.getUser();
        this.getSource();
        this.getBudgetCode();
      }

      const cd: CloudData[] = [];

      for (let i = 0; i < this.cat.length; i++) {
        let color: string;
        let external: boolean;
        let weight = 5;
        let text = '';
        let rotate = 0;

        // randomly set link attribute and external
        if (Math.random() >= 0.5) {
          if (Math.random() >= 0.5) { external = true; }
        }

        // randomly rotate some elements (less probability)
        if (Math.random() >= 0.8) {
          const plusMinus = Math.random() >= 0.5 ? '' : '-';
          rotate = Math.floor(Math.random() * Number(`${plusMinus}20`) + 1);
        }

        // randomly set color attribute
        if (Math.random() >= 0.5) {
          color = '#' + Math.floor(Math.random() * 16777215).toString(16);
        }

        // set random weight
        weight = Math.floor((Math.random() * 10) + 1);

        text = this.cat[i];

        const el: CloudData = {
          text: text,
          weight: weight,
          color: color,
          external: external,
          rotate: rotate
        };

        cd.push(el);
      }

      this.data = cd;



    })
  }

  getBudgetName() {
    this.BudgetSelectItem = [];
    this.selectedBudget = [];
    this._bugetService.getBudgetName(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.Budget = x;
      this.Budget.forEach(m => {
        this.BudgetSelectItem.push({ label: m, value: m })
        this.selectedBudget.push(m)
      })

      this.isgetBudgetNameRun = true;
      this.checkAllMethodeRun();

    })
  }

  getUser() {
    this.userSelectItem = [];
    this.selectedUser = [];
    this._bugetService.getUser(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.user = x;
      this.user.forEach(m => {
        this.userSelectItem.push({ label: m, value: m })
        this.selectedUser.push(m)
      })
      this.isgetUserRun = true;
      this.checkAllMethodeRun();
    })
  }

  getLocation() {
    this.locationSelectItem = [];
    this.selectedLocation = [];
    this._bugetService.getLocation(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.location = x;
      this.location.forEach(m => {
        this.locationSelectItem.push({ label: m, value: m })
        this.selectedLocation.push(m)
      })
      this.isgetLocationRun = true;
      this.checkAllMethodeRun();
    })
  }

  getSource() {
    this.sourceSelectItem = [];
    this.selectedSource = [];
    this._bugetService.getSource(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.source = x;
      this.source.forEach(m => {
        this.sourceSelectItem.push({ label: m, value: m })
        this.selectedSource.push(m)
      })
      this.isgetSourceRun = true
      this.checkAllMethodeRun();
    })
  }

  getBudgetCode() {
    this.budgetcodeSelectItem = [];
    this.selectedBudgetCode = [];
    this._bugetService.getBudgetCode(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.budgetcode = x;
      this.budgetcode.forEach(m => {
        this.budgetcodeSelectItem.push({ label: m, value: m })
        this.selectedBudgetCode.push(m)
      })
      this.isgetBudgetCodeRun = true;
      this.checkAllMethodeRun();
    })
  }

  checkAllMethodeRun() {
    if (this.isgetBudgetCodeRun == true && this.isgetBudgetNameRun == true &&
      this.isgetLocationRun == true && this.isgetSourceRun == true && this.isgetUserRun == true) {
      this.GetBudgetTrx();
      this.GetBudgetSpend();
      this.LoadAllChart();
    }
  }


  onDeptchange(e) {
    this.getCat();
    this.GetBudgetSpend();
  }

  onCatChange(e) {
    // this.selectedCat = [];
    // this.selectedCat.push(e.text);
    this.getBudgetName();
    this.GetBudgetSpend();
  }

  onBudgetChange(e) {
    this.GetBudgetTrx();
    this.GetBudgetSpend();
  }

  onLocationChange(e) {
    this.GetBudgetTrx();
    this.GetBudgetSpend();
  }

  onUserChange(e) {
    this.GetBudgetTrx();
    this.GetBudgetSpend();
  }

  onBudgetCodeChange(e) {
    this.GetBudgetTrx();
    this.GetBudgetSpend();
  }

  onSourceChange(e) {
    this.GetBudgetTrx();
    this.GetBudgetSpend();
  }


  onYearchange(e) {
    this.getMonth();
    this.GetBudgetSpend();
    this.GetBudgetTrx();
    this.getSelectedMonthBudgetData();
    this.getAnualData();
  }


  onMonthchange() {
    this.GetBudgetSpend();
    this.GetBudgetTrx();
    this.getSelectedMonthBudgetData();
  }

  GetBudgetSpend() {
    this.showLoader = true;
    this.bugetLoader = false;
    this.SpendingModel = new Array<BudgetOverspendModel>();
    this.SpendingSearch = new BudgetActualVsPlanDonut();
    this.SpendingSearch.year = this.selectedyear;
    this.SpendingSearch.months = this.selectedmonth;
    this.SpendingSearch.budget = this.selectedBudget
    this.SpendingSearch.departmentName = this.selectedDepartment;
    this.SpendingSearch.department = this.department;
    this._bugetService.GetBudgetSpend(this.SpendingSearch).subscribe(m => {
      this.SpendingModel = m;
      this.listOfDistinctCategory = this.SpendingModel.map(item => item.categoryName)
        .filter((value, index, self) => self.indexOf(value) === index);
      this.bugetLoader = true;
      this.offLoader();
    }, z => {
      this.showLoader = false;
    })
  }

  GetBudgetTrx() {
    $('#tblTrx tr th i').removeClass("fa-arrow-up");
    $('#tblTrx tr th i').removeClass("fa-arrow-down");
    $('#tblTrx tr th i').addClass("fa-arrow-up");
    $('#tblTrx tr th i').attr("data-order", "desc");
    this.trxList = [];
    this.showLoader = true;
    this.trxLoader = false;
    this.TrxSearch = new BudgetActualVsPlanDonut();
    this.TrxSearch.departmentName = this.selectedDepartment;
    this.TrxSearch.year = this.selectedyear;
    this.TrxSearch.months = this.selectedmonth;
    this.TrxSearch.department = this.department;
    this.TrxSearch.location = this.selectedLocation;
    this.TrxSearch.user = this.selectedUser;
    this.TrxSearch.categoryName = this.selectedCat;
    this.TrxSearch.budgetName = this.selectedBudget;
    this.TrxSearch.budgetCode = this.selectedBudgetCode;
    this.TrxSearch.source = this.selectedSource;
    let startTime: number = new Date().getTime();
    this._bugetService.GetBudgetTrx(this.TrxSearch).subscribe(m => {
      this.trxList = m;
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Budget Variance Report (Budget and Variance)"
      model.time = ResponseTime

      this.listOfDistinctDate = this.trxList.map(item => item.trxDate)
        .filter((value, index, self) => self.indexOf(value) === index);

      this.listOfDistinctCatg = this.trxList.map(item => item.categoryDescription)
        .filter((value, index, self) => self.indexOf(value) === index);
      this.trxLoader = true;
      this.offLoader();
      this.ApiLoad.SaveApiTime(model).subscribe();
    }, z => {
      this.showLoader = false;
    })
  }

  offLoader() {
    if (this.trxLoader == true && this.bugetLoader == true) {
      this.showLoader = false;
    }
  }


  LoadAllChart() {
    this.getSelectedMonthBudgetData();
    this.getAnualData();

  }

  getSelectedMonthBudgetData() {
    this.SelectedMonthsearch = new BudgetActualVsPlanDonut();
    this.SelectedMonthsearch.year = this.selectedyear;
    this.SelectedMonthsearch.month = '1';
    this.SelectedMonthsearch.months = this.selectedmonth;
    this.SelectedMonthsearch.budget = this.selectedBudget
    this.SelectedMonthsearch.departmentName = this.selectedDepartment;
    this.SelectedMonthsearch.department = this.department;
    this.SelectedMonthsearch.requestType = "Selected";
    this.createChart();
  }


  SelectMonthBudgetEmitter(e) {
    this.BudgetSeletedMonth = e;
  }

  getAnualData() {
    let search: BudgetActualVsPlanDonut = new BudgetActualVsPlanDonut();
    search.year = this.selectedyear;
    search.budget = this.selectedBudget
    search.month = null;
    search.departmentName = this.selectedDepartment;
    search.department = this.department;
    this._bugetService.getBudgetAnuallData(search).subscribe(m => {
      this.BudgetAnual = m;

      let Actual: Array<number> = []
      let Budget: Array<number> = []
      let MonthName: Array<string> = []

      this.BudgetAnual.forEach(e => {
        Actual.push(parseInt(e.actual.toFixed(0)))
        Budget.push(parseInt(e.budget.toFixed(0)))
        MonthName.push(e.monthName)
      })

      setTimeout(() => {
        this.linedata = {
          labels: MonthName,
          datasets: [
            {
              label: 'Actual',
              data: Actual,
              fill: false,
              borderColor: '#4e79a7'
            },
            {
              label: 'Budget Amount',
              data: Budget,
              fill: false,
              borderColor: '#4bc0c0'
            }
          ]
        }
      }, 100)


    });
    //BudgetAnual
  }


  getStringLocalDateTimeFromUTC(date) {
    const stillUtc = moment.utc(date).toDate();
    const local = moment(stillUtc).local().format('YYYY-MM-DD');
    return local;
  }

  createChart() {
    this.showLoader = true;
    this.Chartdata = [];
    if (this.SelectedMonthsearch.year) {
      this._bugetService.getBudgetDonutData(this.SelectedMonthsearch).subscribe(m => {
        this.BudgetData = m;
        if (this.BudgetData) {
          if (this.BudgetData.budget >= this.BudgetData.actual) {
            if (this.BudgetData.actual == 0) {
              var remaningBudget = this.BudgetData.budget;
              this.Chartdata = {
                labels: ['Actual', 'Remaning'],
                datasets: [
                  {
                    data: [this.BudgetData.actual.toFixed(0), remaningBudget.toFixed(0)],
                    backgroundColor: [
                      "#4e79a7", // Blue
                      "#bab0ac" // Grey
                      //"#e15759"  // Red
                    ]
                  }]
              };
            } else {
              this.Chartdata = {
                labels: ['Actual', 'Remaning'],
                datasets: [
                  {
                    data: [this.BudgetData.actual.toFixed(0), this.BudgetData.remaningBudget.toFixed(0)],
                    backgroundColor: [
                      "#4e79a7", // Blue
                      "#bab0ac" // Grey
                      //"#e15759"  // Red
                    ]
                  }]
              };
            }
          } else if (this.BudgetData.budget < this.BudgetData.actual) {
            var overspending = this.BudgetData.actual - this.BudgetData.budget;
            this.Chartdata = {
              labels: ['Actual', 'Overspending'],
              datasets: [
                {
                  data: [this.BudgetData.actual.toFixed(0), overspending.toFixed(0)],
                  backgroundColor: [
                    "#4e79a7", // Blue
                    //"#bab0ac" // Grey
                    "#e15759"  // Red
                  ]
                }]
            };

          }

        } else {
          this.BudgetData = new BudgetDonutsChartData();
          this.BudgetData.actual = 0; this.BudgetData.budget = 0; this.BudgetData.remaningBudget = 0;
          this.Chartdata = {
            labels: ['Actual', 'Remaning'],
            datasets: [
              {
                data: [0, 0],
                backgroundColor: [
                  "#bab0ac", // Grey
                  "#4e79a7" // Blue
                ]
              }]
          };
        }

      }, error => {
        this.showLoader = false;
      })
    }
  }

  Navigate(e) {
    if (e === "dashboardone") {
      this.router.navigateByUrl('/budget-summary')
    }
    if (e === "dashboardtwo") {
      this.router.navigateByUrl('/dashboardtwo')
    }
    if (e === "dashboardthree") {
      this.router.navigateByUrl('/transactions-detail')
    }
  }

  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl("/");
  }

  sortData(e) {
    const sort = new Sort();
    this.appSort = this.trxList;
    const elem = e.target;

    const order = elem.getAttribute("data-order");
    const type = elem.getAttribute("data-type");
    const property = elem.getAttribute("data-name");
    this.renderer.removeClass(elem, "fa-arrow-up");
    this.renderer.removeClass(elem, "fa-arrow-down");

    if (order === "desc") {
      this.trxList = this.appSort.sort(sort.startSort(property, order, type));
      elem.setAttribute("data-order", "asc");
      this.renderer.addClass(elem, "fa-arrow-down");
    } else {
      this.trxList = this.appSort.sort(sort.startSort(property, order, type));
      elem.setAttribute("data-order", "desc");
      this.renderer.addClass(elem, "fa-arrow-up");
    }

  }

}
