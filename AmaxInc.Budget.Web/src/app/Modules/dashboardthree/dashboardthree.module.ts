import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { TooltipModule, TooltipOptions } from 'ng2-tooltip-directive';
import { CalendarModule } from 'primeng/calendar';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MultiSelectModule } from 'primeng/multiselect';
import { CustomFilterModule} from '../../Common/customFilter/custom-filter.module';

import { DashboardthreeRoutingModule } from './dashboardthree-routing.module';

import { DashboardThreeComponent} from './dashboardthree.component';
@NgModule({
  declarations: [DashboardThreeComponent],
  imports: [
    DashboardthreeRoutingModule,
    CommonModule,
    DropdownModule,
    ChartModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    TagCloudModule,
    MultiSelectModule,
    FormsModule,
    BrowserAnimationsModule,
    CustomFilterModule
  ]
})
export class DashboardthreeModule { }
