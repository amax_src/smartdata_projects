import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { bugetService } from '../../Services/bugetService';
import * as moment from 'moment';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { MonthModel } from '../../model/monthModel';
import { DateService } from '../../Services/dateService';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { Directive, Input, Renderer2, ElementRef, HostListener } from '@angular/core';
import { Sort } from '../../utilities/sort';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';
import $ from "jquery";

@Component({
  selector: 'dashboard-three',
  templateUrl: './dashboardthree.component.html',
  styleUrls: ['./dashboardthree.component.scss']
})


export class DashboardThreeComponent implements OnInit {
  @Input() appSort: Array<any>;
  startDate: Date;
  endDate: Date;
  isAdmin: boolean = true;

  showLoader: boolean;

  isgetBudgetNameRun: boolean;
  isgetLocationRun: boolean;
  isgetUserRun: boolean;
  isgetSourceRun: boolean;
  isgetBudgetCodeRun: boolean;

  trxList: any[] = [];
  listOfDistinctDate: any;

  selectedDepartment: string;
  department: Array<string> = new Array<string>();
  deptSelectItem: SelectItem[] = [];

  location: Array<string> = new Array<string>();
  locationSelectItem: SelectItem[] = [];
  selectedLocation: Array<string> = new Array<string>();

  user: Array<string> = new Array<string>();
  userSelectItem: SelectItem[] = [];
  selectedUser: Array<string> = new Array<string>();

  source: Array<string> = new Array<string>();
  sourceSelectItem: SelectItem[] = [];
  selectedSource: Array<string> = new Array<string>();

  cat: Array<string> = new Array<string>();
  catSelectItem: SelectItem[] = [];
  selectedCat: Array<string> = new Array<string>();

  budgetcode: Array<string> = new Array<string>();
  budgetcodeSelectItem: SelectItem[] = [];
  selectedBudgetCode: Array<string> = new Array<string>();

  buget: Array<string> = new Array<string>();
  Budget: Array<string> = new Array<string>();
  selectedBudget: Array<string> = new Array<string>();
  BudgetSelectItem: SelectItem[] = [];
  isExistDepartment: boolean = false;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<any> = new Array<any>();

  TrxSearch: BudgetActualVsPlanDonut;

  monthNames: Array<any> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];


  constructor(private router: Router, private _bugetService: bugetService, private authService: authenticationService,
    private dateservice: DateService, private renderer: Renderer2, private targetElement: ElementRef, private ApiLoad: ApiLoadTimeService) {

  }

  ngOnInit(): void {
    this.showLoader = false;
    this.getYears();
    this.getDept();
  }

  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date().getFullYear() - 1).toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
      //if (this.selectedBudget.length > 0) {
      //  this.getAnualData();
      //}
    });
  }

  getMonth() {
    this.monthSelectItem = [];
    this.selectedmonth = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice
      .GetMonthskeyvalue(this.selectedyear)
      .toPromise()
      .then((m) => {
        months = m;
        months.forEach((i) => {
          this.monthSelectItem.push({ label: i.name, value: i.id });
        });
        var selectedMonth = months.filter(x => x.selected == true)
        if (this.selectedyear != (new Date().getFullYear() - 1).toString()) {
          this.selectedmonth.push(selectedMonth[0].id)
        } else {
          this.selectedmonth.push(selectedMonth[0].id)
        }
      });
  }

  getDept() {
    this.isExistDepartment = false;
    this._bugetService.getDepartMent(this.authService.userId).subscribe(x => {
      this.department = x;
      this.department.forEach(m => {
        this.deptSelectItem.push({ label: m, value: m })
      })
      this.isAdmin = false;
      if (this.department.length > 0) {
        if (this.department.length > 2) {
          this.isAdmin = true;
        }
        this.isExistDepartment = true;
        this.selectedDepartment = this.department[0];
        this.getCat();
      }
    })
  }

  getCat() {
    this.isgetBudgetNameRun = false;
    this.isgetLocationRun = false;
    this.isgetUserRun = false;
    this.isgetSourceRun = false;
    this.isgetBudgetCodeRun = false;
    this.catSelectItem = [];
    this.selectedCat = [];
    this._bugetService.getCategories(this.selectedDepartment, this.department).subscribe(x => {
      this.cat = x;
      this.cat.forEach(m => {
        this.catSelectItem.push({ label: m, value: m })
        this.selectedCat.push(m)
      })
      if (this.cat.length > 0) {
        this.getBudgetName();
        this.getLocation();
        this.getUser();
        this.getSource();
        this.getBudgetCode();
      }
    })
  }

  getBudgetName() {
    this.BudgetSelectItem = [];
    this.selectedBudget = [];
    this._bugetService.getBudgetName(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.Budget = x;
      this.Budget.forEach(m => {
        this.BudgetSelectItem.push({ label: m, value: m })
        this.selectedBudget.push(m)
      })

      this.isgetBudgetNameRun = true;
      this.checkAllMethodeRun();

    })
  }

  getUser() {
    this.userSelectItem = [];
    this.selectedUser = [];
    this._bugetService.getUser(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.user = x;
      this.user.forEach(m => {
        this.userSelectItem.push({ label: m, value: m })
        this.selectedUser.push(m)
      })
      this.isgetUserRun = true;
      this.checkAllMethodeRun();
    })
  }

  getLocation() {
    this.locationSelectItem = [];
    this.selectedLocation = [];
    this._bugetService.getLocation(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.location = x;
      this.location.forEach(m => {
        this.locationSelectItem.push({ label: m, value: m })
        this.selectedLocation.push(m)
      })
      this.isgetLocationRun = true;
      this.checkAllMethodeRun();
    })
  }

  getSource() {
    this.sourceSelectItem = [];
    this.selectedSource = [];
    this._bugetService.getSource(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.source = x;
      this.source.forEach(m => {
        this.sourceSelectItem.push({ label: m, value: m })
        this.selectedSource.push(m)
      })
      this.isgetSourceRun = true
      this.checkAllMethodeRun();
    })
  }

  getBudgetCode() {
    this.budgetcodeSelectItem = [];
    this.selectedBudgetCode = [];
    this._bugetService.getBudgetCode(this.selectedDepartment, this.selectedCat, this.department).subscribe(x => {
      this.budgetcode = x;
      this.budgetcode.forEach(m => {
        this.budgetcodeSelectItem.push({ label: m, value: m })
        this.selectedBudgetCode.push(m)
      })
      this.isgetBudgetCodeRun = true;
      this.checkAllMethodeRun();
    })
  }

  checkAllMethodeRun() {
    if (this.isgetBudgetCodeRun == true && this.isgetBudgetNameRun == true &&
      this.isgetLocationRun == true && this.isgetSourceRun == true && this.isgetUserRun == true) {
      this.GetBudgetTrx();
    }
  }
  GetBudgetTrx() {
    this.trxList = [];
    this.showLoader = true;
    this.TrxSearch = new BudgetActualVsPlanDonut();
    this.TrxSearch.year = this.selectedyear;
    this.TrxSearch.months = this.selectedmonth;
    this.TrxSearch.departmentName = this.selectedDepartment;
    this.TrxSearch.department = this.department;
    this.TrxSearch.location = this.selectedLocation;
    this.TrxSearch.user = this.selectedUser;
    this.TrxSearch.categoryName = this.selectedCat;
    this.TrxSearch.budgetName = this.selectedBudget;
    this.TrxSearch.budgetCode = this.selectedBudgetCode;
    this.TrxSearch.source = this.selectedSource;
    let startTime: number = new Date().getTime();
    this._bugetService.GetBudgetTrx(this.TrxSearch).subscribe(m => {
      this.trxList = m;
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = "Transactions Detail (Budget and Variance)"
      model.time = ResponseTime

      this.listOfDistinctDate = this.trxList.map(item => item.trxDate)
        .filter((value, index, self) => self.indexOf(value) === index);
      this.showLoader = false;
      this.ApiLoad.SaveApiTime(model).subscribe();
    }, z => {
      this.showLoader = false;
    })
  }

  onDeptchange(e) {
    this.getCat();
  }

  onCatChange(e) {
    this.getBudgetName();
  }

  onBudgetChange(e) {
    //this.GetBudgetTrx();
  }

  onLocationChange(e) {
    //this.GetBudgetTrx();
  }

  onUserChange(e) {
    //this.GetBudgetTrx();
  }

  onBudgetCodeChange(e) {
    //this.GetBudgetTrx();
  }

  onSourceChange(e) {
    //this.GetBudgetTrx();
  }


  onYearchange(e) {
    this.getMonth();
    //this.GetBudgetTrx();
  }

  onMonthchange() {
    //this.GetBudgetTrx();
  }

  Navigate(e) {
   
    if (e === "dashboardone") {
      this.router.navigateByUrl('/budget-summary')
    }
    if (e === "dashboardtwo") {
      this.router.navigateByUrl('/account-detail')
    }
  }

  getStringLocalDateTimeFromUTC(date) {
    const stillUtc = moment.utc(date).toDate();
    const local = moment(stillUtc).local().format('YYYY-MM-DD');
    return local;
  }

  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl("/");
  }

  Search() {
    $('#tblTrx tr th i').removeClass("fa-arrow-up");
    $('#tblTrx tr th i').removeClass("fa-arrow-down");
    $('#tblTrx tr th i').addClass("fa-arrow-up");
    $('#tblTrx tr th i').attr("data-order", "desc");

    this.GetBudgetTrx();
  }

  sortData(e) {
    const sort = new Sort();
    this.appSort = this.trxList;
    const elem = e.target;

    const order = elem.getAttribute("data-order");
    const type = elem.getAttribute("data-type");
    const property = elem.getAttribute("data-name");
    this.renderer.removeClass(elem, "fa-arrow-up");
    this.renderer.removeClass(elem, "fa-arrow-down");

    if (order === "desc") {
      this.trxList = this.appSort.sort(sort.startSort(property, order, type));
      elem.setAttribute("data-order", "asc");
      this.renderer.addClass(elem, "fa-arrow-down");
    } else {
      this.trxList = this.appSort.sort(sort.startSort(property, order, type));
      elem.setAttribute("data-order", "desc");
      this.renderer.addClass(elem, "fa-arrow-up");
    }

  }

}
