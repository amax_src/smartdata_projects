import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardThreeComponent } from './dashboardthree.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: 'transactions-detail', component: DashboardThreeComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardthreeRoutingModule { }
