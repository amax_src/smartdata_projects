import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { bugetService } from '../../Services/bugetService';
import { DateService } from '../../Services/dateService';
import { MonthModel } from '../../model/monthModel';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { BudgetDonutsChartData } from '../../model/budgetdonutsChartData';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';

@Component({
  selector: 'dashboard-one',
  templateUrl: './dashboardone.component.html',
  styleUrls: ['./dashboardone.component.css'],
})
export class DashboardOneComponent implements OnInit {
  showLoader: boolean;
  selectedDepartment: string;
  department: Array<string> = new Array<string>();
  deptSelectItem: SelectItem[] = [];
  isAdmin: boolean = true;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = '';

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<any> = new Array<any>();

  cat: Array<string> = new Array<string>();
  catSelectItem: SelectItem[] = [];
  selectedCat: Array<string> = new Array<string>();

  Budget: Array<string> = new Array<string>();
  selectedBudget: Array<string> = new Array<string>();
  BudgetSelectItem: SelectItem[] = [];

  isExistDepartment: boolean = false;
  SelectedMonthsearch: BudgetActualVsPlanDonut;
  LatestMonthsearch: BudgetActualVsPlanDonut;
  YTMsearch: BudgetActualVsPlanDonut;
  BudgetData: BudgetDonutsChartData;

  BudgetAnual: Array<BudgetDonutsChartData> =
    new Array<BudgetDonutsChartData>();

  BudgetSeletedMonth: BudgetDonutsChartData = new BudgetDonutsChartData();

  monthNames: Array<any> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  donoutdataSelectedMonth: any;
  donoutdataLastMonth: any;
  donoutdataYTM: any;

  linedata: any;
  options: any;

  constructor(
    private router: Router,
    private _bugetService: bugetService,
    private dateservice: DateService,
    private authService: authenticationService,
    private ApiLoad: ApiLoadTimeService
  ) {
    this.options = {
      maintainAspectRatio: false,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var label = data.datasets[tooltipItem.datasetIndex].label || '';
            label =
              label +
              ': $' +
              tooltipItem.value
                .toString()
                .replace('.', ',')
                .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return label;
          },
        },
      },
      scales: {
        yAxes: [
          {
            display: true,
            position: 'left',
            type: 'linear',
            scaleLabel: {
              display: true,
              labelString: 'USD',
              beginAtZero: true,
            },
            //yAxisID: "id1"
            id: 'id1', // incorrect property name.
          },
          {
            scaleLabel: {
              display: true,
              labelString: 'Commissions',
              beginAtZero: true,
            },
            //display: false,
            display: true, // Hopefully don't have to explain this one.
            type: 'linear',
            position: 'right',
            gridLines: {
              display: false,
            },
            //yAxisID: "id2"
            id: 'id2', // incorrect property name.
          },
        ],
      },
    };
    this.donoutdataSelectedMonth = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: ['#bab0ac', '#4e79a7', '#e15759'],
        },
      ],
    };
    this.donoutdataLastMonth = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: ['#bab0ac', '#4e79a7', '#e15759'],
        },
      ],
    };
    this.donoutdataYTM = {
      labels: ['Budget', 'Actual', 'Remaning'],
      datasets: [
        {
          data: [0, 0, 0],
          backgroundColor: ['#bab0ac', '#4e79a7', '#e15759'],
        },
      ],
    };
    this.linedata = {
      labels: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      datasets: [
        {
          label: 'First Dataset',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#4bc0c0',
        },
        {
          label: 'Second Dataset',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#4e79a7',
        },
        {
          label: 'Second Dataset1',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          fill: false,
          borderColor: '#e15759',
        },
      ],
    };
  }

  ngOnInit(): void {
    this.showLoader = false;
    this.BudgetData = new BudgetDonutsChartData();
    this.LatestMonthsearch = new BudgetActualVsPlanDonut();
    this.YTMsearch = new BudgetActualVsPlanDonut();

    this.getYears();
    this.getDept();
  }

  getDept() {
    this.isExistDepartment = false;
    this._bugetService.getDepartMent(this.authService.userId).subscribe((x) => {
      this.department = x;
      this.department.forEach((m) => {
        this.deptSelectItem.push({ label: m, value: m });
      });
      this.isAdmin = false;
      if (this.department.length > 0) {
        if (this.department.length > 2) {
          this.isAdmin = true;
        }
        this.isExistDepartment = true;
        this.selectedDepartment = this.department[0];
        this.getCat();
      }
    });
  }

  getYears() {
    this.dateservice
      .getYear()
      .toPromise()
      .then((m) => {
        this.years = m;
        this.years.forEach((i) => {
          this.yearsSelectItem.push({
            label: i,
            value: i,
          });
        });
        let _selectedYear = (new Date().getFullYear() - 1).toString();//new Date().getFullYear().toString();
        this.selectedyear = _selectedYear;
        this.getMonth();
        if (this.selectedBudget.length > 0) {
          this.getAnualData();
        }
      });
  }

  getMonth() {
    console.log("Yaer", new Date().getFullYear()-1);
    this.monthSelectItem = [];
    this.selectedmonth = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice
      .GetMonthskeyvalue(this.selectedyear)
      .toPromise()
      .then((m) => {
        months = m;
        months.forEach((i) => {
          this.monthSelectItem.push({ label: i.name, value: i.id });
        });
        var selectedMonth = months.filter(x => x.selected == true)
        if (this.selectedyear != (new Date().getFullYear()-1).toString()) {
          this.selectedmonth.push(selectedMonth[0].id)
        } else {
          this.selectedmonth.push(selectedMonth[0].id)

          this.displayTextContent();
        }
      });
  }

  displayTextContent() {
    if (this.selectedmonth.length > 1) {
      document
        .getElementById('EventTypeSelection')
        .getElementsByClassName('p-multiselect-label')[0].textContent =
        this.selectedmonth.length + ' Months';
    } else if (this.selectedmonth.length == 1) {
      var monthName = this.monthSelectItem.filter(
        (x) => x.value == this.selectedmonth[0]
      )[0].label;
      document
        .getElementById('EventTypeSelection')
        .getElementsByClassName('p-multiselect-label')[0].textContent =
        monthName;
    } else if (this.selectedmonth.length == 0) {
      document
        .getElementById('EventTypeSelection')
        .getElementsByClassName('p-multiselect-label')[0].textContent =
        'Months';
    }
  }

  getCat() {
    this.catSelectItem = [];
    this.selectedCat = [];
    this._bugetService
      .getCategories(this.selectedDepartment, this.department)
      .subscribe((x) => {
        this.cat = x;
        this.cat.forEach((m) => {
          this.catSelectItem.push({ label: m, value: m });
          this.selectedCat.push(m);
        });
        if (this.cat.length > 0) {
          this.getBudgetName();
        }
      });
  }

  getBudgetName() {
    this.BudgetSelectItem = [];
    this.selectedBudget = [];
    this._bugetService
      .getBudgetName(this.selectedDepartment, this.selectedCat, this.department)
      .subscribe((x) => {
        this.Budget = x;
        this.Budget.forEach((m) => {
          this.BudgetSelectItem.push({ label: m, value: m });
          this.selectedBudget.push(m);
        });
        this.LoadAllChart();
      });
  }

  onYearchange(e) {
    this.getMonth();
    this.getYtdBudgetData();
    this.getAnualData();
  }

  onMonthchange() {
    this.getSelectedMonthBudgetData();
    this.displayTextContent();
  }

  onDeptchange(e) {
    this.getCat();
  }

  onCatChange(e) {
    this.getBudgetName();
  }

  onBudgetChange() {
    this.LoadAllChart();
  }

  LoadAllChart() {
    this.getSelectedMonthBudgetData();
    this.getLatestMonthBudgetData();
    this.getYtdBudgetData();
    this.getAnualData();
  }

  getSelectedMonthBudgetData() {
    this.SelectedMonthsearch = new BudgetActualVsPlanDonut();
    this.SelectedMonthsearch.year = this.selectedyear;
    this.SelectedMonthsearch.month = '1'; //For Filterd Month
    this.SelectedMonthsearch.months = this.selectedmonth;
    this.SelectedMonthsearch.budget = this.selectedBudget;
    this.SelectedMonthsearch.departmentName = this.selectedDepartment;
    this.SelectedMonthsearch.department = this.department;
    this.SelectedMonthsearch.requestType = 'Selected';
  }

  getLatestMonthBudgetData() {
    this.LatestMonthsearch = new BudgetActualVsPlanDonut();
    this.LatestMonthsearch.year = this.selectedyear;
    this.LatestMonthsearch.budget = this.selectedBudget;
    this.LatestMonthsearch.month = null;
    this.LatestMonthsearch.departmentName = this.selectedDepartment;
    this.LatestMonthsearch.department = this.department;
    this.LatestMonthsearch.requestType = 'Latest';
  }

  getYtdBudgetData() {
    this.YTMsearch = new BudgetActualVsPlanDonut();
    this.YTMsearch.year = this.selectedyear;
    this.YTMsearch.month = '0';
    this.YTMsearch.budget = this.selectedBudget;
    this.YTMsearch.departmentName = this.selectedDepartment;
    this.YTMsearch.department = this.department;
    this.YTMsearch.requestType = 'YTM';
  }
  SelectMonthBudgetEmitter(e) {
    this.BudgetSeletedMonth = e;
  }

  getAnualData() {
    let search: BudgetActualVsPlanDonut = new BudgetActualVsPlanDonut();
    search.year = this.selectedyear;
    search.budget = this.selectedBudget;
    search.month = null;
    search.departmentName = this.selectedDepartment;
    search.department = this.department;
    let startTime: number = new Date().getTime();
    this._bugetService.getBudgetAnuallData(search).subscribe((m) => {
      let model: ApiLoadModel = new ApiLoadModel();
      let EndTime: number = new Date().getTime();
      let ResponseTime: number = EndTime - startTime;
      model.page = 'Budget Summary (Budget and Variance)';
      model.time = ResponseTime;

      this.BudgetAnual = m;
      let Actual: Array<number> = [];
      let Budget: Array<number> = [];
      let MonthName: Array<string> = [];
      this.BudgetAnual.forEach((e) => {
        Actual.push(parseInt(e.actual.toFixed(0)));
        Budget.push(parseInt(e.budget.toFixed(0)));
        MonthName.push(e.monthName);
      });
      setTimeout(() => {
        this.linedata = {
          labels: MonthName,
          datasets: [
            {
              label: 'Actual',
              data: Actual,
              fill: false,
              borderColor: '#4e79a7',
            },
            {
              label: 'Budget Amount',
              data: Budget,
              fill: false,
              borderColor: '#4bc0c0',
            },
          ],
        };
      }, 100);
      this.ApiLoad.SaveApiTime(model).subscribe();
    });
    //BudgetAnual
  }

  Navigate(e) {
    if (e === 'dashboardtwo') {
      this.router.navigateByUrl('/account-detail');
    }
    if (e === 'dashboardthree') {
      this.router.navigateByUrl('/transactions-detail');
    }
    if (e === 'dashboardfour') {
      this.router.navigateByUrl('/budget-variance-report');
    }
    if (e === 'BudgetSummary') {
      this.router.navigateByUrl('/account-summary');
    }
    if (e === 'excelimport') {
      this.router.navigateByUrl('/excel-import');
    }
  }

  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}
