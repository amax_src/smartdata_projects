import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { TooltipModule, TooltipOptions } from 'ng2-tooltip-directive';
import { CalendarModule } from 'primeng/calendar';
import { TagCloudModule } from 'angular-tag-cloud-module';


import { DashboardOneComponent } from './dashboardone.component';
import { DountchartComponent} from '../../shared/dountchart/dountchart.component';
import { DashboardoneRoutingModule } from './dashboardone-routing.module';

import { ProductService } from '../../productservice';
@NgModule({
  declarations: [DashboardOneComponent,DountchartComponent],
  imports: [
    CommonModule,
    DashboardoneRoutingModule,
    DropdownModule,
    ChartModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    TagCloudModule,
    MultiSelectModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [ProductService]
})
export class DashboardoneModule { }
