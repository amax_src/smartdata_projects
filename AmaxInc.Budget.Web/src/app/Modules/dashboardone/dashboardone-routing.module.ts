import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardOneComponent } from './dashboardone.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: 'budget-summary', component: DashboardOneComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardoneRoutingModule { }
