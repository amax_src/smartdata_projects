import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExcelImportRoutingModule } from './excel-import-routing.module';
import { ExcelImportComponent } from './excel-import.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  declarations: [ExcelImportComponent],
  imports: [
    CommonModule,
    ExcelImportRoutingModule,
    CommonModule,
    DropdownModule,
    MultiSelectModule,
    FormsModule,
    BrowserAnimationsModule
  ],
})
export class ExcelImportModule { }




