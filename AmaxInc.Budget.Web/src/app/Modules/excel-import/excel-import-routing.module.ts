import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExcelImportComponent } from './excel-import.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: 'excel-import', component: ExcelImportComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExcelImportRoutingModule { }