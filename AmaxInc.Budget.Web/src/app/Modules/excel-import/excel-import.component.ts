import { Component, OnInit, Pipe, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { bugetService } from '../../Services/bugetService';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SelectItem } from 'primeng/api/selectitem';
import { DateService } from '../../Services/dateService';
import { MonthModel } from '../../model/monthModel';


@Component({
  selector: 'app-excel-import',
  templateUrl: './excel-import.component.html',
  styleUrls: ['./excel-import.component.scss']
})
export class ExcelImportComponent implements OnInit {
  errorActualMessage = "";
  errorBudgetMessage = "";
  textColor = "";
  showLoader: boolean;
  isAdmin: boolean = true;
  userRole: any;
  @ViewChild('actualFileInput') actualFileInput;
  @ViewChild('budgetFileInput') budgetFileInput;
  message: string;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: string = "";

  constructor(
    private router: Router,
    private authService: authenticationService,
    private http: HttpClient, private service: bugetService,
    private dateservice: DateService,
  ) { }

  ngOnInit() {
    this.userRole = this.authService.userRole === 'head';
    this.getYears();
  }

  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
    });
  }

  getMonth() {
    this.monthSelectItem = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice.GetMonthskeyvalue("2020").toPromise().then(m => {
      months = m;
      months.forEach(i => {
        this.monthSelectItem.push({ label: i.name, value: i.id })
      })
      this.selectedmonth = (new Date()).getMonth().toString();
    })
  }

  onYearchange(e) {
    this.getMonth();
  }

  onMonthchange() {
  }


  Navigate(e) {
    if (e === 'dashboardone') {
      this.router.navigateByUrl('/budget-summary');
    }
    if (e === 'dashboardthree') {
      this.router.navigateByUrl('/transactions-detail');
    }
    if (e === 'dashboardfour') {
      this.router.navigateByUrl('/budget-variance-report');
    }
  }

  uploadActualFile() {
    this.showLoader = true;
    this.errorActualMessage = "";
    this.errorBudgetMessage = "";
    let formData = new FormData();
    if (this.actualFileInput.nativeElement.files.length > 0) {
      formData.append('ActualFile', this.actualFileInput.nativeElement.files[0])
      formData.append('month', this.selectedmonth);
      formData.append('year', this.selectedyear);
      this.service.UploadExcel(formData).subscribe(result => {
        if (result.statusCode == 200) {
          this.errorActualMessage = result.message;
          this.textColor = "green";
          this.showLoader = false;
        } else if (result.statusCode == 400) {
          this.errorActualMessage = result.message;
          this.textColor = "red";
          this.showLoader = false;
        } else if (result.statusCode == 404) {
          this.errorActualMessage = result.message;
          this.textColor = "red";
          this.showLoader = false;
        } else if (result.statusCode == 501) {
          this.errorActualMessage = result.message;
          this.textColor = "red";
          this.showLoader = false;
        }
        this.showLoader = false;
      },
        z => {
          this.showLoader = false;
        });
    } else {
      this.errorActualMessage = "Please select Actual file"
      this.textColor = "red";
      this.showLoader = false;
    }
  }

  uploadBudgetFile() {
    this.showLoader = true;
    this.errorActualMessage = "";
    this.errorBudgetMessage = "";
    let formData = new FormData();
    if (this.budgetFileInput.nativeElement.files.length > 0) {
      formData.append('BudgetFile', this.budgetFileInput.nativeElement.files[0])
      formData.append('month', this.selectedmonth);
      formData.append('year', this.selectedyear);
      this.service.UploadExcel(formData).subscribe(result => {
        if (result.statusCode == 200) {
          this.errorBudgetMessage = result.message;
          this.textColor = "green";
          this.showLoader = false;
        } else if (result.statusCode == 400) {
          this.errorBudgetMessage = result.message;
          this.textColor = "red";
          this.showLoader = false;
        } else if (result.statusCode == 404) {
          this.errorBudgetMessage = result.message;
          this.textColor = "red";
          this.showLoader = false;
        }
        this.showLoader = false;
      },
        z => {
          this.showLoader = false;
        });
    } else {
      this.errorBudgetMessage = "Please select Budget file"
      this.textColor = "red";
      this.showLoader = false;
    }
  }


  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}


