import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminBudgetSummaryRoutingModule } from './admin-budget-summary-routing.module';
import { AdminBudgetSummaryComponent } from './admin-budget-summary.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { CustomFilterModule} from '../../Common/customFilter/custom-filter.module';
import { ProductService } from '../../productservice';


@NgModule({
  declarations: [AdminBudgetSummaryComponent],
  imports: [
    CommonModule,
    AdminBudgetSummaryRoutingModule
    , FormsModule
    , DropdownModule
    , TableModule
    , TagCloudModule
    , BrowserAnimationsModule
    , CustomFilterModule
    , MultiSelectModule
  ],
  providers: [ProductService]
})
export class AdminBudgetSummaryModule { }

