import { Component, OnInit, Pipe } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from '../../productservice';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api/selectitem';
import { DateService } from '../../Services/dateService';
import { MonthModel } from '../../model/monthModel';
import { bugetService } from '../../Services/bugetService';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { BudgetSummary, DistinctMonths } from '../../model/budgetOverspendModel';
import { authenticationService } from 'src/app/login/authenticationService.service';
import { ApiLoadModel } from '../../model/ApiLoadModel';
import { ApiLoadTimeService } from '../../Services/ApiLoadTimeService';

@Component({
  selector: 'app-admin-budget-summary',
  templateUrl: './admin-budget-summary.component.html',
  styleUrls: ['./admin-budget-summary.component.scss'],
})
export class AdminBudgetSummaryComponent implements OnInit {
  showLoader: boolean;
  isAdmin: boolean = true;

  years: Array<any>;
  yearsSelectItem: SelectItem[] = [];
  selectedyear: string = "";

  department: Array<string> = new Array<string>();
  deptSelectItem: SelectItem[] = [];
  selectedDepartment: Array<string> = new Array<string>();

  month: Array<any>;
  monthSelectItem: SelectItem[] = [];
  selectedmonth: Array<any> = new Array<any>();

  SpendingModel: Array<BudgetSummary> =
    new Array<BudgetSummary>();
  SpendingSearch: BudgetActualVsPlanDonut;
  distinctDepartment: Array<any>;
  distinctMonth: Array<DistinctMonths> =
    new Array<DistinctMonths>();


  budgetTotal: any;
  actualTotal: any;
  varianceTotal: any;
  variancePercentage: any;
  spending: any;
  listofHeader: any;


  monthNames: Array<any> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  products: Product[];
  value: number = 12;
  myOptions = {
    autoPlacement: true,
    'show-delay': 10,
  };

  listOfDistinctCategory: any;
  userRole: any;
  constructor(
    private productService: ProductService,
    private router: Router,
    private dateservice: DateService,
    private _bugetService: bugetService,
    private authService: authenticationService,
    private ApiLoad: ApiLoadTimeService
  ) { }

  ngOnInit() {
    this.userRole = this.authService.userRole === 'head';
    this.getYears();
    this.getDept();
  }

  Navigate(e) {
    if (e === 'dashboardone') {
      this.router.navigateByUrl('/budget-summary');
    }
    if (e === 'dashboardthree') {
      this.router.navigateByUrl('/transactions-detail');
    }
    if (e === 'dashboardfour') {
      this.router.navigateByUrl('/budget-variance-report');
    }
  }

  getYears() {
    this.dateservice.getYear().toPromise().then(m => {
      this.years = m;
      this.years.forEach(i => {
        this.yearsSelectItem.push(
          {
            label: i, value: i
          })
      })
      let _selectedYear = (new Date()).getFullYear().toString();
      this.selectedyear = _selectedYear;
      this.getMonth();
    });
  }

  getMonth() {
    this.monthSelectItem = [];
    this.selectedmonth = [];
    let months: Array<MonthModel> = new Array<MonthModel>();
    this.dateservice
      .GetMonthskeyvalue(this.selectedyear)
      .toPromise()
      .then((m) => {
        months = m;
        months.forEach((i) => {
          this.monthSelectItem.push({
            label: i.name,
            value: i.id,
          });
        });

        if (this.selectedyear != '2021') {
          for (var i = 0; i < this.monthSelectItem.length; i++) {
            this.selectedmonth.push(
              this.monthSelectItem[i].value
            );
          }
        }
        else {
          for (var i = 0; i < this.monthSelectItem.length - 1; i++) {
            this.selectedmonth.push(
              this.monthSelectItem[i].value
            );
          }
        }
        //this.selectedmonth.push(
        //  this.monthSelectItem[this.monthSelectItem.length - 1].value
        //);
        this.GetBudgetSpend();
      });
  }

  getDept() {
    this.deptSelectItem = [];
    this.selectedDepartment = [];
    this._bugetService.getDepartMent(this.authService.userId).subscribe((x) => {
      this.department = x;

      this.department.forEach((m) => {
        this.deptSelectItem.push({ label: m, value: m });
        this.selectedDepartment.push(m);
      });
      this.isAdmin = false;
      if (this.department.length > 0) {
        if (this.department.length > 2) {
          this.isAdmin = true;
        }

        // this.GetBudgetSpend();
      }
    });
  }

  onYearchange(e) {
    this.getMonth();
  }

  onMonthchange() {
    this.GetBudgetSpend();
  }

  onDeptchange(e) {
    this.GetBudgetSpend();
  }


  sortMonth(a, b) {
    return a["monthNumber"] - b["monthNumber"];
  }
  GetBudgetSpend() {
    this.showLoader = true;
    this.distinctDepartment = []; this.distinctMonth = new Array<DistinctMonths>(); this.listofHeader = [];
    this.SpendingModel = new Array<BudgetSummary>();
    this.SpendingSearch = new BudgetActualVsPlanDonut();
    this.SpendingSearch.year = this.selectedyear;
    this.SpendingSearch.months = this.selectedmonth;
    this.SpendingSearch.departmentName = 'All';
    this.SpendingSearch.department = this.selectedDepartment;
    let startTime: number = new Date().getTime();
    this._bugetService.GetBudgetSummary(this.SpendingSearch).subscribe(
      (m) => {
        let model: ApiLoadModel = new ApiLoadModel();
        let EndTime: number = new Date().getTime();
        let ResponseTime: number = EndTime - startTime;
        model.page = 'Account Detail (Budget and Variance)';
        model.time = ResponseTime;
        this.SpendingModel = m;
        let templistDepartment = this.SpendingModel.filter(x => x.department != 'EBITDA' && x.department != 'BTotal' && x.department != 'BGTotal' && x.department != 'Revenue');
        let templistMonth = this.SpendingModel.filter(x => x.month != 'Grand Total' && x.month != 'LGrand Total' && x.department != 'Revenue');
        this.distinctDepartment = templistDepartment.map(item => item.department)
          .filter((value, index, self) => self.indexOf(value) === index);

        let _distinctMonth = templistMonth.map(item => item.month)
          .filter((value, index, self) => self.indexOf(value) === index);

        _distinctMonth.forEach(x => {
          let index = this.monthNames.indexOf(x);
          this.distinctMonth.push({ month: x, monthNumber: index + 1 });
          this.listofHeader.push('$ Budget');
          this.listofHeader.push('$ Actual');
          this.listofHeader.push('$ Variance');
          this.listofHeader.push('Variance Rate %');

        })

        this.distinctMonth = this.distinctMonth.sort(this.sortMonth);
        this.showLoader = false;
        this.ApiLoad.SaveApiTime(model).subscribe();
      },
      (z) => {
        this.showLoader = false;
      }
    );
  }

  LogOut() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}
