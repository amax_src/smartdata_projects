import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminBudgetSummaryComponent } from './admin-budget-summary.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: 'account-summary', component: AdminBudgetSummaryComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminBudgetSummaryRoutingModule { }

