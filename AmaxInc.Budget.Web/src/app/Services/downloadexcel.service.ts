import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DownloadexcelService {

  constructor() { }

  downloadPath: string = '';
  pendingDownloadFile = "";
  downloadFile(downloadPath: string) {
    if (downloadPath != "") {
      var element = document.createElement('a');
      element.setAttribute('href', downloadPath);
      //element.setAttribute('target', "_blank");
      //element.innerHTML = "";
      //if ((downloadPath.indexOf('.xlsx') !== -1) || (downloadPath.indexOf('.xls') !== -1) || (downloadPath.indexOf('.zip') !== -1)) {
      //} else {
      //element.setAttribute('target', "_blank");
      //}
      element.innerHTML = "";
      element.click();
      element.remove();
      return true;
    }
    else {
      return false;
    }
  }
}
