import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BASE_API_URL } from '../core/environment.tokens';
import { authenticationService } from '../login/authenticationService.service';
import { BudgetSearch, BudgetActualVsPlanDonut } from '../model/budgetSearch';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class bugetService {
  //_baseUrl: string = "http://localhost:44305/Export/";
  _baseUrl: string = "http://18.216.112.189:8093/Export/";

  header: HttpHeaders;
  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient,
    private _auth: authenticationService) {
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + this._auth.userToken
    });
  }

  getBudgetDonutData(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetByMonth`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetAnuallData(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetAnnual`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  GetBudgetSpend(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/BudgetSpend`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers });
  }

  GetBudgetSummary(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/GetBudgetSummary`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers });
  }

  GetBudgetTrx(searh: BudgetActualVsPlanDonut) {
    const url = `${this.baseApiUrl}Budget/GetBudgetTranc`;
    let headers = this.header;
    return this.http.post<any>(url, searh, { headers });
  }

  // For Drop Down List
  getDepartMent(userId: number) {
    const url = `${this.baseApiUrl}Budget/Department/` + userId;
    let headers = this.header;
    return this.http.get<any>(url, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getCategories(dept: string, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/Categories`;
    let headers = this.header;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.department = department;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetName(dept: string, catg: Array<string>, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/BudgetName`;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    budgetSearch.department = department;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getUser(dept: string, catg: Array<string>, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/User`;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    budgetSearch.department = department;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getLocation(dept: string, catg: Array<string>, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/Location`;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    budgetSearch.department = department;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getSource(dept: string, catg: Array<string>, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/Source`;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    budgetSearch.department = department;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  getBudgetCode(dept: string, catg: Array<string>, department: Array<string>) {
    const url = `${this.baseApiUrl}Budget/BudgetCode`;
    let budgetSearch = new BudgetSearch();
    budgetSearch.dept = dept;
    budgetSearch.catg = catg;
    budgetSearch.department = department;
    let headers = this.header;
    return this.http.post<any>(url, budgetSearch, { headers })
      .pipe(map(x => {
        return x;
      }));
  }

  UploadExcel(formData: FormData) {  
    const url = `${this.baseApiUrl}Budget/UploadExcel`;
    let headers = new HttpHeaders();  
    headers.append('Content-Type', 'multipart/form-data');  
    headers.append('Accept', 'application/json');    
    const httpOptions = { headers: headers };  
    return this.http.post<any>(url, formData, httpOptions)
    .pipe(map(x => {
      return x;
    }));
  }  

  ExportAccountDetals(accountDeatils: any) {
    const url = `${this.baseApiUrl}Budget/ExportAccountDetals/`;
    let headers = this.header;
    return this.http.post<any>(url, accountDeatils, { headers })
      .pipe(map(x => {
        var filename = this._baseUrl + x;
        return filename;
      }));
  }
}
