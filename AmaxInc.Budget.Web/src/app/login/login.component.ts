import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { authenticationService } from './authenticationService.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string = '';
  logintype: string = '';
  showLoader: boolean = false;
  isValidEmail: boolean = true;
  message: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: authenticationService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
    
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    
    this.showLoader = true;
    this.logintype = 'headofdepratment';
    this.authenticationService.login(this.f.username.value, this.f.password.value, this.logintype)
      .pipe(first())
      .subscribe(
        data => {
          this.showLoader = false;
          if (this.authenticationService.userRole === "head" || this.authenticationService.userRole === "vp" ) {
            this.router.navigateByUrl("budget-summary");
          }
          else if (this.authenticationService.userRole === "alpa") {
            this.router.navigateByUrl("PBX");
          }
          else {
            this.router.navigateByUrl("/");
          }
        },
        error => {
          try {
            this.error = error.error.message;
          }
          catch{

          }
          this.showLoader = false;
        });
  }


}
