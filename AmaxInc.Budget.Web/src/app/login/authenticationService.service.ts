import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { SessionService } from '../core/storage/sessionservice';
import { user } from '../model/user';
import { BASE_API_URL } from '../core/environment.tokens';
import { StringHelper } from '../utilities/contracts/string-helper';

@Injectable({
  providedIn: 'root'
})
export class authenticationService {
  public currentUser: Observable<user>;
  readonly AUTHENTICATION_KEY = "authentication";
  redirectUrl = "";

  constructor(@Inject(BASE_API_URL) private readonly baseApiUrl: string, private http: HttpClient, private sessionService: SessionService, private router: Router) {

  }

  public get currentUserValue(): user {
    return this.user;
  }

  get user(): user {
    return this.sessionService.get<user>('currentUser', new user());
  }

  get userToken(): string {
    return this.user.token;
  }

  get userRole(): string {
    let role: string = this.user.groupName.toLowerCase().replace(/ /g, '');
    return role;
  }

  get username(): string {
    return this.user.userName;
  }

  get userId(): number {
    return this.user.userID;
  }

  get isAuthenticated(): boolean {
    if (this.userToken) {
      return StringHelper.isAvailable(this.userToken);
    }
    return false;
  }

  get loginName(): string {
    return this.user.loginName;
  }

  login(username: string, password: string, logintype: string) {
    const url = `${this.baseApiUrl}BudgetAuthentication/Validate`;
    return this.http.post<any>(url, { username, password, logintype })
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.sessionService.put('currentUser', user)
        }
        return user;
      }));
  }

  validateUserName(email: string, role: string) {
    const url = `${this.baseApiUrl}Authentication/CheckUserName/` + email + "?loginType=" + role;
    return this.http.get<any>(url)
      .pipe(map(x => {
        return x;
      }));
  }

  OnForgotPasswordSubmit(email: string, role: string) {
    const url = `${this.baseApiUrl}Authentication/ForgotPassword/` + email + "?loginType=" + role;
    return this.http.get<any>(url)
      .pipe(map(x => {
        return x;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    this.sessionService.delete('currentUser');
  }

  GetNotification() {
    const url = `${this.baseApiUrl}Notification`;

    return this.http.get<any>(url)
      .pipe(map(x => {
        return x;
      }));
  }
}
