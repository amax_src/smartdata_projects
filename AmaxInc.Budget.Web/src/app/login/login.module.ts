import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { authenticationService } from './authenticationService.service';


@NgModule({
  declarations: [
    LoginComponent
  ],

  providers: [authenticationService],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    LoginRoutingModule,
    FormsModule
  ]

})

export class LoginModule {

}
