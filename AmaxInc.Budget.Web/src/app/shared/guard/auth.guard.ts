import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    const data = JSON.parse(localStorage.getItem('currentUser'));
    if (data == null || data == undefined) {
      window.sessionStorage.removeItem('currentUser');
      localStorage.removeItem('currentUser');
      this.router.navigateByUrl("/");
      return false;
    } else {
      if (data.userID == undefined || data.userID == null || data.userID == 0) {
        this.router.navigateByUrl("/");
        window.sessionStorage.removeItem('currentUser');
        localStorage.removeItem('currentUser');
        return false;
      }
      return true;
    }
  }
}





