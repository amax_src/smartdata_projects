import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DountchartComponent } from './dountchart.component';

describe('DountchartComponent', () => {
  let component: DountchartComponent;
  let fixture: ComponentFixture<DountchartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DountchartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DountchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
