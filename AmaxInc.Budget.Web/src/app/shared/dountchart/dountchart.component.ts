import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BudgetActualVsPlanDonut } from '../../model/budgetSearch';
import { BudgetDonutsChartData } from '../../model/budgetdonutsChartData';
import { bugetService } from '../../Services/bugetService';
@Component({
  selector: 'app-dountchart',
  templateUrl: './dountchart.component.html',
  styleUrls: ['./dountchart.component.scss']
})
export class DountchartComponent implements OnInit {
  donoutdataYTM: any;
  showLoader: boolean;

  _search: BudgetActualVsPlanDonut
  _ChartName: string;
  constructor(private _bugetService: bugetService) {



  this.options = {
    responsive: true,
    legend: {
      position: 'top',
    },
    tooltips: {
      enabled: true,
      mode: 'single',
      callbacks: {
        label: function (tooltipItems, data) {
          var label = data.labels[tooltipItems.index] || '';
          return label +': $' + data.datasets[0].data[tooltipItems.index].toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ',') ;
        }
      }
    },
  };


  }

  @Input() set ChartName(_ChartName: string) {
    this._ChartName = _ChartName
  }

  @Input() set search(_search: BudgetActualVsPlanDonut) {
    this._search = _search
    if (_search) {
      this.createChart();
    }
  }
  @Output() SelectMonthBudgetOut = new EventEmitter();

  Chartdata: any;
  options: any;

  BudgetData: BudgetDonutsChartData = new BudgetDonutsChartData();

  ngOnInit(): void {
    this.showLoader = true;
    this.donoutdataYTM = {
      labels: ['Actual', 'Remaning'],
      datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "#bab0ac", // Grey
            "#4e79a7" // Blue
          ]
        }]
    };
    //this.showLoader = false;
  }

  createChart() {
    this.showLoader = true;
    this.Chartdata = [];
    if (this._search.year) {
      this._bugetService.getBudgetDonutData(this._search).subscribe(m => {
        this.BudgetData = m;
        if (this.BudgetData) {
          if (this._search.requestType == "Selected") {
            this.SelectMonthBudgetOut.emit(this.BudgetData)
          }
          if (this.BudgetData.budget >= this.BudgetData.actual) {
            if (this.BudgetData.actual == 0) {
              var remaningBudget = this.BudgetData.budget;
              this.Chartdata = {
                labels: ['Actual', 'Remaning'],
                datasets: [
                  {
                    data: [this.BudgetData.actual.toFixed(0), remaningBudget.toFixed(0)],
                    backgroundColor: [
                      "#4e79a7", // Blue
                      "#bab0ac" // Grey
                      //"#e15759"  // Red
                    ]
                  }]
              };
            } else {
              this.Chartdata = {
                labels: ['Actual', 'Remaning'],
                datasets: [
                  {
                    data: [this.BudgetData.actual.toFixed(0), this.BudgetData.remaningBudget.toFixed(0)],
                    backgroundColor: [
                      "#4e79a7", // Blue
                      "#bab0ac" // Grey
                      //"#e15759"  // Red
                    ]
                  }]
              };
            }
          } else if (this.BudgetData.budget < this.BudgetData.actual) {
            var overspending = this.BudgetData.actual - this.BudgetData.budget;
            this.Chartdata = {
              labels: ['Actual', 'Overspending'],
              datasets: [
                {
                  data: [this.BudgetData.actual.toFixed(0), overspending.toFixed(0)],
                  backgroundColor: [
                    "#4e79a7", // Blue
                    //"#bab0ac" // Grey
                    "#e15759"  // Red
                  ]
                }]
            };

          }

        } else {
          this.BudgetData = new BudgetDonutsChartData();
          this.BudgetData.actual = 0; this.BudgetData.budget = 0; this.BudgetData.remaningBudget = 0;
          this.SelectMonthBudgetOut.emit(this.BudgetData)
          this.Chartdata = {
            labels: ['Actual', 'Remaning'],
            datasets: [
              {
                data: [0, 0],
                backgroundColor: [
                  "#bab0ac", // Grey
                  "#4e79a7" // Blue
                ]
              }]
          };
        }
        this.showLoader = false;

      }, error => {
        this.showLoader = false;
      })
    }
  }


}
