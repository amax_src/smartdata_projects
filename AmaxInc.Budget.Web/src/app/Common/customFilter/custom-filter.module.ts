import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { CustomFilter } from "./custom-filter";
import { OrderByPipe} from './order-by.pipe';
@NgModule({
  imports: [],
  declarations: [CustomFilter, OrderByPipe ],
  exports: [CustomFilter, OrderByPipe ]
})

export class CustomFilterModule { }
