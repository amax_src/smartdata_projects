import { Injectable } from "@angular/core";
import { StringHelper } from "../../utilities/contracts/string-helper";
import { StorageTypes } from "./storage-type.enum";
import { StorageValueHelper } from "./storage-value-helper.model";

@Injectable({
  providedIn: "root"
})


export class SessionService {
  test: number;
  test2: number;
  put(key: string, value: StorageTypes): void {
    StringHelper.throwIsAvailableError(key, "key");

    const stringValue = StorageValueHelper.create(value);
    window.sessionStorage.setItem(key, stringValue);
  }

  get<T>(key: string, defaultValue: T): T {
    StringHelper.throwIsAvailableError(key, "key");
    const value = localStorage.getItem(key);
    const realValue = StringHelper.isAvailable(value) ? JSON.parse(value) : defaultValue;
    return realValue as any;
  }

  // get<T>(key: string, defaultValue: T): T {
  //   StringHelper.throwIsAvailableError(key, "key");
  //   const value = window.sessionStorage.getItem(key);
  //   const realValue = StringHelper.isAvailable(value) ? JSON.parse(value) : defaultValue;
  //   return realValue as any;
  // }

  delete(key: string = ""): void {
    window.sessionStorage.removeItem(key);
    localStorage.removeItem(key);
  }

  clear(): void {
    window.sessionStorage.clear();
    localStorage.clear();
  }

  private isAvailableKey(key: string): boolean {
    const value = window.sessionStorage.getItem(key);
    return StringHelper.isAvailable(value);
  }

  private throwIsAvailableKeyError(key: string): void {
    if (!this.isAvailableKey(key)) {
      throw new Error(`Key '${key}' not found.`);
    }
  }
}
