import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, RoutingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { DashboardoneModule } from './Modules/dashboardone/dashboardone.module';
import { AuthGuard } from './shared/guard/auth.guard';
// import { ExcelImportComponent } from './Modules/excel-import/excel-import.component';
//import { AdminBudgetSummaryComponent } from './Modules/admin-budget-summary/admin-budget-summary.component';
//import { DashboardtwoModule} from './Modules/dashboardtwo/dashboardtwo.module';
//import { DashboardthreeModule} from './Modules/dashboardthree/dashboardthree.module';
//import { DashboardfourModule} from './Modules/dashboardfour/dashboardfour.module';
@NgModule({
  declarations: [
    AppComponent,
    RoutingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    LoginModule,
    DashboardoneModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
