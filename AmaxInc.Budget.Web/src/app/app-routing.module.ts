import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardoneModule } from './Modules/dashboardone/dashboardone.module';
import { DashboardtwoModule } from './Modules/dashboardtwo/dashboardtwo.module';
import { DashboardthreeModule } from './Modules/dashboardthree/dashboardthree.module';
import { DashboardfourModule } from './Modules/dashboardfour/dashboardfour.module';
import { AdminBudgetSummaryModule } from './Modules/admin-budget-summary/admin-budget-summary.module';
import { ExcelImportModule } from './Modules/excel-import/excel-import.module';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', loadChildren: () => import(`./Modules/dashboardone/dashboardone.module`).then(m => m.DashboardoneModule), canActivate: [AuthGuard] },
  { path: 'dashboardtwo', loadChildren: () => import(`./Modules/dashboardtwo/dashboardtwo.module`).then(m => m.DashboardtwoModule), canActivate: [AuthGuard] },
  { path: 'dashboardthree', loadChildren: () => import(`./Modules/dashboardthree/dashboardthree.module`).then(m => m.DashboardthreeModule), canActivate: [AuthGuard] },
  { path: 'dashboardfour', loadChildren: () => import(`./Modules/dashboardfour/dashboardfour.module`).then(m => m.DashboardfourModule), canActivate: [AuthGuard] },
  { path: 'account-summary', loadChildren: () => import(`./Modules/admin-budget-summary/admin-budget-summary.module`).then(m => m.AdminBudgetSummaryModule), canActivate: [AuthGuard] },
  { path: 'excel-import', loadChildren: () => import(`./Modules/excel-import/excel-import-routing.module`).then(m => m.ExcelImportRoutingModule), canActivate: [AuthGuard] }

];

@NgModule({
  imports: [DashboardoneModule, DashboardtwoModule, DashboardthreeModule, DashboardfourModule,AdminBudgetSummaryModule,ExcelImportModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const RoutingComponent = [

]
