export class BudgetDonutsChartData {
  year: string = "";
  budget: number = 0;
  actual: number = 0;
  remaningBudget: number = 0;
  budgetVariance: number = 0;
  percentage: number = 0;
  department: string = "";
  categoryName: string = "";
  budgetName: string = "";
  period: string = "";
  monthName: string = "";
}
