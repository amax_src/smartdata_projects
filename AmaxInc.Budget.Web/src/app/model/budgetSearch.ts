export class BudgetSearch {
  dept: string = "";
  catg: Array<string> = new Array<string>();
  department: Array<string> = new Array<string>();
  month: string ="";
}

export class BudgetActualVsPlanDonut {
  year: string;
  month: string;
  budget: Array<string> = new Array<string>();
  requestType: string = '';
  departmentName: string = '';

  department: Array<string> = new Array<string>();
  months: Array<string> = new Array<string>();
  location: Array<string> = new Array<string>();
  user: Array<string> = new Array<string>();
  categoryName: Array<string> = new Array<string>();
  budgetName: Array<string> = new Array<string>();
  budgetCode: Array<string> = new Array<string>();
  source: Array<string> = new Array<string>();
  startDate: string;
  endDate: string;
}
