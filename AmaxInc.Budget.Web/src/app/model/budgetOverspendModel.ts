export class BudgetOverspendModel {
  public year: string = "";
  public categoryName: string = "";
  public budgetName: string = "";
  public frontBudgetCode: string = "";
  public budget: number = 0;
  public actual: number = 0;
  public remaningBudget: number = 0;
  public variance: number = 0;
  public varianceRate: number = 0;
  public spending: string = "";
  public actualPer: number = 0;
  public remPer: number = 0;
}

export class BudgetSummary {
  public department: string = '';
  public month: string = '';
  public budget: number = 0;
  public actual: number = 0;
  public variance: number = 0;
  public varianceRate: number = 0;
}
export class DistinctMonths {
  month: string = '';
  monthNumber: number = 0;
}

