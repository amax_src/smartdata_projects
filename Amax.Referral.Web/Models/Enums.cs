﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amax.Referral.Web.Models
{
    public enum Resource
    {
        Home,
        Customer,
        Agent,
        WelcomeHeading,
        WelcomeMessage,
        CopyRight,
        PrivacyPolicy,
        TermsAndConditions,
        Submit,
        Name,
        Email,
        PhoneNumber,
        Zip,
        SelfDetails,
        ReferralsDetails,
        ReferralSavedSuccessfull,
        ErrorSavingReferral,
        TermsAndConditionContent,
        TermsAndConditionContentTitle,
        SelfNameValidation,
        ReferralNameValidation,
        EmailValidation,
        PhoneNumberValidation,
        AgentIdValidation,
        AgencyValidation,
        EnterReferralsDetails,
        EnterYourAgentId,
        ChooseAgency,
        PrivacyPolicyContent,
        PrivacyPolicyContentTitle,
        PhoneOrEmailValidation,
        CustomerEmailRequiredValidation,
        ReferralEmailRequiredValidation,
        EmailUniqueValidation,
        PhoneUniqueValidation,
        CustomerInfo,
        SubmitAndMore,
        EmailCustomer,
        EmailReferance,
        Sms,
        UnsuscribeSms,
        PhoneNumberRequiredValidation,
        ZipCodeValidation,
        RefferAfriendPageTitle,
        AgentCustomerNameValidation,
        EmailCustomerSubject,
        EmailReferanceSubject,
        LablePhoneNumber,
        LableEmail,
        LableCustomerName,
        RequestAQuote,
        LearnMore,
        WeRespectPrivacy,
        SMSCustomer,
        SMSReferance

    }

    public enum SelectedLanguage
    {
        Language,
    }

    public enum Section
    {
        Header,
        Home,
        Footer,
        Common,
        Customer,
        TermsAndCondition,
        Agent,
        PrivacyPolicy,
    }

    public enum Language
    {
        English = 1,
        Spanish = 2
    }

    public enum ReferedBy
    {
        Agent = 0,
        Customer = 1
    }

    public enum Submit
    {
        Add,
        AddMore
    }

}
