﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Amax.Referral.Web.Models
{
    public class AgencyLoginModel
    {
        public int AgencyId { get; set; }
        [Required(ErrorMessage ="Username is required")]
        public string AgencyLogin { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        public string Zip { get; set; }

        [Required(ErrorMessage = "AgentId is required")]
        public string AgentId { get; set; }


        // For Searching data

        public string SearchParameter { get; set; }
        public string SortParameter { get; set; }
        public string SortDirection { get; set; }
        public string SearchMonth { get; set; }
        public string SearchYear { get; set; }
        public bool start { get; set; }

        // For TOken Authentication
        public string Token { get; set; }
    }
}
