﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Amax.Referral.Web.Models
{
    public class AgentRefModel
    {
        [Required(ErrorMessage = "Enter Referral's Name")]
        public string RefName { get; set; }
        [RegularExpression("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", ErrorMessage = "E-mail is not valid")]
        //[Required(ErrorMessage = "Enter Referral's Email")]
        [Remote(action: "ValidateEmail", controller: "Validation", ErrorMessage = "This email address is already referred.")]

        public string RefEmail { get; set; }
        [RegularExpression("[0-9]+", ErrorMessage = "Enter a valid phone number")]
        [MinLength(10, ErrorMessage = "Please enter valid phone number.")]
        [Remote(action: "ValidatePhone", controller: "Validation", ErrorMessage = "This phone number is already referred.")]
        [Required(ErrorMessage = "Enter phone number.")]
        public string RefPhone { get; set; }
        [Required(ErrorMessage = "Enter Agent-Id")]
        [RegularExpression("[0-9]+", ErrorMessage = "Enter a valid agent Id")]
        public string AgentId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please select Agency")]
        public int AgencyId { get; set; }
        [Required(ErrorMessage = "Enter customer name.")]
        public string CustomerName { get; set; }
        [RegularExpression("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", ErrorMessage = "E-mail is not valid")]
        public string CustomerEmail { get; set; }
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid phone number.")]
        [MinLength(10, ErrorMessage = "Please enter valid phone number.")]
        [Required(ErrorMessage = "Enter phone number.")]
        public string CustomerPhone { get; set; }
    }
}
