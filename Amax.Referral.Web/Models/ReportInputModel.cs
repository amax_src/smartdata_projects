﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amax.Referral.Web.Models
{
    public class ReportInputModel
    {
        public int month { get; set; }
        public string type { get; set; }
    }
}
