﻿using AmaxIns.Repository.Agency;
using AmaxIns.Repository.Payroll;
using AmaxIns.Repository.Referral;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.Payroll;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.Service.Agency;
using AmaxIns.Service.Payroll;
using AmaxIns.Service.Refferal;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Payroll;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Amax.Referral.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<FormOptions>(options =>
            {
                options.ValueCountLimit = int.MaxValue;
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(m =>
            {
                m.LoginPath = "/Authentication";
                m.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                //m.LoginPath = "/Authentication",
             });
            services.AddMvc().AddSessionStateTempDataProvider().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            ConfigureDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<IReferralService, ReferralService>();
            services.AddScoped<IRefferalRepository, ReferralRepository>();

            services.AddTransient<IAgencyService, AgencyService>();
            services.AddScoped<IAgencyRepository, AgencyRepository>();

            services.AddTransient<ILanguageService, LanguageService>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();

            services.AddTransient<IAgencyLoginService, AgencyLoginService>();
            services.AddScoped<IAgencyLoginRepository, AgencyLoginRepository>();


            services.AddTransient<ITurboratorService, TurboratorService>();
            services.AddScoped<ITurboratorRepository,TurboratorRepository>();


            services.AddTransient<IWinbackService, WinbackService>();
            services.AddScoped<IWinbackRepository, WinbackRepository>();

            services.AddSingleton<IConfiguration>(Configuration);
        }
    }
}
