﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Amax.Referral.Web.Models;
using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.DataContract.Referral.Turborator;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using X.PagedList;

namespace Amax.Referral.Web.Controllers
{
    [Authorize]
    public class TurboratorController : Controller
    {
        IConfiguration _config;
        IAgencyService _agencyService;
        ITurboratorService _turboratorService;
        IReferralService _referralService;
        IAgencyLoginService _agencyLogin;
        public TurboratorController(ITurboratorService turboratorService, IConfiguration config, IAgencyService agencyService, IReferralService referralService, IAgencyLoginService agencyLogin)
        {
           _turboratorService = turboratorService;
            _config = config;
            _agencyService = agencyService;
            _referralService = referralService;
            _agencyLogin = agencyLogin;
        }

        public IActionResult Index(int? page, int month, int year)
        {

            int agencyId = 0; ;
            try
            {
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                TokenModel token = new TokenModel();
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if (!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
                // zip = userdata.Zip;
                agencyId = userdata.AgencyId;
            }
            catch
            {

            }
            if (month == 0)
            {
                month = DateTime.Now.Month;
            }

            if (year == 0)
            {
                year = DateTime.Now.Year;
            }
            var result = _turboratorService.TurboratorReferralReport(month, year, agencyId);
            
            var pageNumber = page ?? 1;
            var PageSize = _config.GetSection("PageSize").Value;
            var data = result.ToPagedList(pageNumber, Convert.ToInt32(PageSize));
            ViewBag.IsAdmin = agencyId == -1 ? true : false;
            ViewBag.month = month;
            ViewBag.year = year;
            ViewBag.Months = Utility.Utility.GetMonthSelectList();
            ViewBag.Years = Utility.Utility.GetYearSelectList();
            return View(data);

        }

        public IActionResult TurboraterCommentBox(int id)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            IEnumerable<CommentsModel> comments = new List<CommentsModel>();
            comments = _referralService.GetTurboratorComments(id);
            var model = new CommentsModel();
            model.ReferenceId = id;
            model.AgencyId = userdata.AgencyId;
            ViewBag.CommentList = comments;
            return View(model);
        }

        public IActionResult SaveComment(CommentsModel model)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            if (!string.IsNullOrWhiteSpace(model.Comments))
            {
                _referralService.AddTurboratorcomments(model);
                return Json("success");
            }
            return Json("error");
       }

        public IActionResult ExportToCSV(int month, int year)
        {
            string zip = "";
            int agencyId = 0; ;
            try
            {
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                TokenModel token = new TokenModel();
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if (!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
                zip = userdata.Zip;
                agencyId = userdata.AgencyId;
            }
            catch
            {
              
            }
            var result = _turboratorService.TurboratorReferralReport(month, year, agencyId);
            var agencies = _agencyService.Get().Result.ToList();
            agencies.Add(new AmaxIns.DataContract.Agency.AgencyModel { AgencyId = 99, AgencyName = "Haltom City" });
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Location,Customer Name,Email,Phone,Zip Code,Converted to Sale,Comments");
            foreach (var x in result)
            {
                string agencyName = string.Empty;
                string RouteToAgencyName = string.Empty;
                if (x.Agencyid>0)
                {
                    var agencyData = agencies.Where(m => m.AgencyId == x.Agencyid).FirstOrDefault();
                    if (agencyData != null)
                    {
                        agencyName = agencyData.AgencyName;
                    }
                }
                
                sb.AppendLine(agencyName + "," + string.Concat(x.Name," ",x.Surname) + "," + x.Email + "," + x.PhoneNumber + "," + x.ZipCode + ","  + (x.ConvertedToCustomer ? "Yes" : "No") + "," + x.Comments );
            }


            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
            return File(bytes, "application/octet-stream", "Reports.csv"); // Fileresult;
        }
    }
}