﻿using Amax.Referral.Web.Models;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Twilio;
using Twilio.AspNet.Core;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;

namespace Amax.Referral.Web.Controllers
{

    public class SmsController : TwilioController
    {
        ILanguageService _Service;
        IConfiguration _config;
        IReferralService _referralService;
        public SmsController( ILanguageService Service,IConfiguration config, IReferralService referralService)
        {
          
            _Service = Service;
            _config = config;
            _referralService = referralService;
        }


        [HttpPost]
        public IActionResult Index()
        {
            
            int selectedLanguageId = GetSelectedLangage();
            var languageResources = _Service.GetLanguageResoure(selectedLanguageId, Section.Common.ToString()).Result;
            string smsMessage = languageResources.Where(m => m.ResourceKey == Resource.UnsuscribeSms.ToString()).FirstOrDefault().ResourceText;

            var requestBody = Request.Form["Body"];
            var number = Request.Form["From"];
            var response = new MessagingResponse();

            if (requestBody.ToString().ToLower() == "stop"|| requestBody.ToString().ToLower() == "cancelar")
            {
                response.Message(smsMessage);
            }
            return TwiML(response);
        }

        public IActionResult SendMessage()
        {
            const string accountSid = "ACfa43db289971a6b998259c74365dc523";
            const string authToken = "f38ee7f229fb23cb2857a7ceafc37bee";
            TwilioClient.Init(accountSid, authToken);
            var message = MessageResource.Create(
                body: "Please reply this message say 'hello' ",
                from: new Twilio.Types.PhoneNumber("+18634000180"),
                to: new Twilio.Types.PhoneNumber("+13467707078")
            );

            return new JsonResult("Message Send");
        }

        [NonAction]
        private int GetSelectedLangage()
        {
            var SelectedLangauge = HttpContext.Session.Get(SelectedLanguage.Language.ToString());
            if (SelectedLangauge == null)
            {
                var languages = _Service.GetLanguage().Result;
                var defaultLanguage = 1;
                foreach (var x in languages)
                {
                    defaultLanguage = x.Id;
                    break;
                }
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), defaultLanguage.ToString());
            }

            int selectedLanguageId = 1;
            string seletedlanguage = HttpContext.Session.GetString(SelectedLanguage.Language.ToString());
            if (!Int32.TryParse(seletedlanguage, out selectedLanguageId))
            {
                selectedLanguageId = 1;
            }

            return selectedLanguageId;
        }
    }


}