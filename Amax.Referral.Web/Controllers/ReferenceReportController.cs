﻿using Amax.Referral.Web.Models;
using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using X.PagedList;

namespace Amax.Referral.Web.Controllers
{
    [Authorize]
    public class ReferenceReportController : Controller
    {
        IReferralService referralService;
        IConfiguration config;
        IAgencyService agencyService;
        IAgencyLoginService _agencyLogin;
        public ReferenceReportController(IReferralService _referralService, IConfiguration _config, IAgencyService _agencyService, IAgencyLoginService agencyLogin)
        {
            referralService = _referralService;
            config = _config;
            agencyService = _agencyService;
            _agencyLogin = agencyLogin;
        }
        public IActionResult Index(int? page, int month, int year, string type)
        {
            var ShowTouchReport = config.GetSection("ShowTouchReport").Value;
            ViewBag.ShowTouchReport = ShowTouchReport;
            string zip = "";
            int agencyId = 0;
            try
            {
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                TokenModel token = new TokenModel();
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if (!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
                zip = userdata.Zip;
                agencyId = userdata.AgencyId;
            }
            catch
            {

            }

            if (month == 0)
            {
                month = DateTime.Now.Month;
            }
            if(year == 0)
            {
                year = DateTime.Now.Year;
            }
            if (type == null)
            {
                type = ReferedBy.Agent.ToString();
            }
            var result = referralService.ReferralReport(month, year, type, zip, agencyId);
            ViewBag.type = type;
            ViewBag.IsAdmin = agencyId == -1 ? true : false;
            ViewBag.month = month;
            ViewBag.year = year;
            var pageNumber = page ?? 1;
            var PageSize = config.GetSection("PageSize").Value;
            var data = result.ToPagedList(pageNumber, Convert.ToInt32(PageSize));
            ViewBag.Months = Utility.Utility.GetMonthSelectList();
            ViewBag.Years = Utility.Utility.GetYearSelectList();
            return View(data);

        }

        public IActionResult CommentBox(int refid)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            IEnumerable<CommentsModel> comments = new List<CommentsModel>();
            comments = referralService.GetComments(refid);
            var model = new CommentsModel();
            model.ReferenceId = refid;
            model.AgencyId = userdata.AgencyId;
            ViewBag.CommentList = comments;
            return View(model);
        }

        public IActionResult SaveComment(CommentsModel model)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            if (!string.IsNullOrWhiteSpace(model.Comments))
            {
                referralService.Addcomments(model);
                return Json("success");
            }
            return Json("error");


        }


        public IActionResult ExportToCSV(int month, int year)
        {
            string zip = "";
            int agencyId = 0; ;
            try
            {
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                TokenModel token = new TokenModel();
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if (!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
                zip = userdata.Zip;
                agencyId = userdata.AgencyId;
            }
            catch
            {

            }
            var result = referralService.ReferralReport(month, year, "", zip, agencyId);
            var agencies = agencyService.Get().Result.ToList();
            agencies.Add(new AmaxIns.DataContract.Agency.AgencyModel { AgencyId = 99, AgencyName = "Haltom City" });
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Referral ID ,Customer Name,Customer Email,Customer Phone,Zip Code,Referral Name,Referral Email,Referral Phone,Year,Month,Agent ID,Agency ID,Agency Name,Converted to Sale,Entry Date,Routed to,Comment");
            foreach (var x in result)
            {
                string agencyName = string.Empty;
                string RouteToAgencyName = string.Empty;
                if (!string.IsNullOrWhiteSpace(x.AgencyID))
                {
                    var agencyData = agencies.Where(m => m.AgencyId.ToString() == x.AgencyID).FirstOrDefault();
                    if(agencyData != null)
                    {
                        agencyName = agencyData.AgencyName;
                    }
                }

                if (!string.IsNullOrWhiteSpace(x.RoutedTo))
                {
                    var RouteToAgencyData = agencies.Where(m => m.AgencyId.ToString() == x.RoutedTo).FirstOrDefault();
                    if (RouteToAgencyData != null)
                    {
                        RouteToAgencyName = RouteToAgencyData.AgencyName;
                    }
                }

                sb.AppendLine(x.referrelId + "," + x.UserName + "," + x.UserEmail + "," + x.UserPhone + "," + x.ZipCode + "," + x.ReferralName + "," + x.ReferralEmail + "," + x.ReferralPhone + "," + x.Year + "," + x.MonthName + "," + x.AgentId + "," + x.AgencyID + "," + agencyName + "," + (x.IsCustomer ? "Yes" : "No") + "," + x.CreatedDate.ToShortDateString() + "," + RouteToAgencyName + "," + x.Comment);
            }


            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
            //var Fileresult = new FileContentResult(bytes, "application/octet-stream");
            //Fileresult.FileDownloadName = "Reports.csv";
            return File(bytes, "application/octet-stream", "Reports.csv"); // Fileresult;
        }

        public IActionResult ExportUntouchedOrTouchedToCSV(int month, int year, string type)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            bool seachType = type == "touched" ? true : false;
            string filename = type == "touched" ? "TouchedQuotes.csv" : "UntouchedQuotes.csv";
            var result = referralService.ReferralTouchedUnTouchedReport(month, seachType);
            var agencies = agencyService.Get().Result.ToList();
            agencies.Add(new AmaxIns.DataContract.Agency.AgencyModel { AgencyId = 99, AgencyName = "Haltom City" });
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Referral ID ,Customer Name,Customer Email,Customer Phone,Zip Code,Referral Name,Referral Email,Referral Phone,Year,Month,Agent ID,Agency ID,Agency Name,Converted to Sale,Entry Date,Routed to");
            foreach (var x in result)
            {
                string agencyName = string.Empty;
                string RouteToAgencyName = string.Empty;
                if (!string.IsNullOrWhiteSpace(x.AgencyID))
                {
                    var agencyData = agencies.Where(m => m.AgencyId.ToString() == x.AgencyID).FirstOrDefault();
                    if (agencyData != null)
                    {
                        agencyName = agencyData.AgencyName;
                    }
                }

                if (!string.IsNullOrWhiteSpace(x.RoutedTo))
                {
                    var RouteToAgencyData = agencies.Where(m => m.AgencyId.ToString() == x.RoutedTo).FirstOrDefault();
                    if (RouteToAgencyData != null)
                    {
                        RouteToAgencyName = RouteToAgencyData.AgencyName;
                    }
                }

                sb.AppendLine(x.referrelId + "," + x.UserName + "," + x.UserEmail + "," + x.UserPhone + "," + x.ZipCode + "," + x.ReferralName + "," + x.ReferralEmail + "," + x.ReferralPhone + "," + x.Year + "," + x.MonthName + "," + x.AgentId + "," + x.AgencyID + "," + agencyName + "," + (x.IsCustomer ? "Yes" : "No") + "," + x.CreatedDate.ToShortDateString() + "," + RouteToAgencyName);
            }


            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
            return File(bytes, "application/octet-stream", filename); // Fileresult;
        }
    }
}