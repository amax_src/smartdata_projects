﻿using Amax.Referral.Web.Models;
using Amax.Referral.Web.Utility;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Amax.Referral.Web.Controllers
{
    public class CustomerController : Controller
    {

        IReferralService referralService;
        ILanguageService _Service;
        IConfiguration config;
        public CustomerController(IReferralService _referralService, ILanguageService Service, IConfiguration _config)
        {
            referralService = _referralService;
            _Service = Service;
            config = _config;
        }
        public IActionResult Index([FromQuery(Name = "lang")] string lang)
        {
            int selectedLanguageId = GetSelectedLangage(lang);
            ViewBag.selectedLangage = selectedLanguageId;
            ViewBag.Resourses = _Service.GetLanguageResoure(selectedLanguageId, Section.Customer.ToString()).Result;
            ViewBag.TermAndCondition = _Service.GetLanguageResoure(selectedLanguageId, Section.TermsAndCondition.ToString()).Result;

            var model = new ReferralModel();
            if (TempData["customerModel"] != null)
            {
                var customerModel = TempData.Get<ReferralModel>("customerModel");
                if (customerModel != null)
                {
                    model.CustomerName = customerModel.CustomerName;
                    model.CustomerPhone = customerModel.CustomerPhone;
                    model.CustomerEmail = customerModel.CustomerEmail;
                }
            }

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveReferral(ReferralModel modle, string type)
        {
            int selectedLanguageId = GetSelectedLangage("");
            var languageResources = _Service.GetLanguageResoure(selectedLanguageId, Section.Customer.ToString()).Result;
            string emailMessageToCustomer = languageResources.Where(m => m.ResourceKey == Resource.EmailCustomer.ToString()).FirstOrDefault().ResourceText;
            string emailMessageToReferance = languageResources.Where(m => m.ResourceKey == Resource.EmailReferance.ToString()).FirstOrDefault().ResourceText;
            //string smsMessage = languageResources.Where(m => m.ResourceKey == Resource.Sms.ToString()).FirstOrDefault().ResourceText;
            string subject = languageResources.Where(m => m.ResourceKey == Resource.EmailCustomerSubject.ToString()).FirstOrDefault().ResourceText;
            string subjectReferance = languageResources.Where(m => m.ResourceKey == Resource.EmailReferanceSubject.ToString()).FirstOrDefault().ResourceText;

             string SMSCustomerMessage= languageResources.Where(m => m.ResourceKey == Resource.SMSCustomer.ToString()).FirstOrDefault().ResourceText;
            string SMSReferanceMessage = languageResources.Where(m => m.ResourceKey == Resource.SMSReferance.ToString()).FirstOrDefault().ResourceText;

            string result = "success";
            if (ModelState.IsValid)
            {
                try
                {
                    referralService.AddReferral(new AmaxIns.DataContract.Referral.ReferralModel
                    {
                        CustomerEmail = modle.CustomerEmail,
                        CustomerName = modle.CustomerName,
                        CustomerPhone = modle.CustomerPhone,
                        RefEmail = modle.RefEmail,
                        RefName = modle.RefName,
                        RefPhone = modle.RefPhone,
                        ZipCode = modle.ZipCode,

                    }, emailMessageToCustomer, emailMessageToReferance,subject, subjectReferance);
                    if (type == Submit.AddMore.ToString())
                    {
                        TempData.Put<ReferralModel>("customerModel", modle);
                    }
                    result = "success";
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(modle.CustomerPhone))
                        {
                            var SmsAccountSid = config.GetSection("SmsAccountSid").Value;
                            var SmsAuthToken = config.GetSection("SmsAuthToken").Value;
                            var SmsFrom = config.GetSection("SmsFrom").Value;
                            TwilioClient.Init(SmsAccountSid, SmsAuthToken);
                            var message = MessageResource.Create(
                                body: SMSCustomerMessage,
                                from: new Twilio.Types.PhoneNumber(SmsFrom),
                                to: new Twilio.Types.PhoneNumber("+1" + modle.CustomerPhone)
                            );

                            var messageToReferance = MessageResource.Create(
                              body: SMSReferanceMessage,
                              from: new Twilio.Types.PhoneNumber(SmsFrom),
                              to: new Twilio.Types.PhoneNumber("+1" + modle.RefPhone)
                          );
                        }
                    }
                    catch
                    {

                    }
                }
                catch(Exception ex)
                {
                    result = "error";
                }
            }
            else
            {
                result = "error";
            }

            return RedirectToAction("Index", "Customer", new { message = result });
        }

        [NonAction]
        private int GetSelectedLangage(string lang)
        {
            var SelectedLangauge = HttpContext.Session.Get(SelectedLanguage.Language.ToString());
            if(lang!=null && lang.ToLower()=="en")
            {
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), "1");
            }
            else if(lang != null && lang.ToLower() == "es")
            {
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), "2");
            }
            else if (SelectedLangauge == null)
            {
                var languages = _Service.GetLanguage().Result;
                var defaultLanguage = 1;
                foreach (var x in languages)
                {
                    defaultLanguage = x.Id;
                    break;
                }
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), defaultLanguage.ToString());
            }

            int selectedLanguageId = 1;
            string seletedlanguage = HttpContext.Session.GetString(SelectedLanguage.Language.ToString());
            if (!Int32.TryParse(seletedlanguage, out selectedLanguageId))
            {
                selectedLanguageId = 1;
            }

            return selectedLanguageId;
        }



    }
}