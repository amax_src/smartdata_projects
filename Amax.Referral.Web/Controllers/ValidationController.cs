﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Mvc;

namespace Amax.Referral.Web.Controllers
{
    public class ValidationController : Controller
    {
        IReferralService referralService;
        public ValidationController(IReferralService _referralService)
        {
            referralService = _referralService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ValidateEmail(string RefEmail)
        {
            return Json(referralService.ValidateEmailAddress(RefEmail));
        }

        public IActionResult ValidatePhone(string RefPhone)
        {
            return Json(referralService.ValidatePhoneNumber(RefPhone));
        }

        public IActionResult ValidateZip(string ZipCode)
        {
            return Json(referralService.ValidateZip(ZipCode));
        }
    }
}