﻿using Amax.Referral.Web.Models;
using AmaxIns.DataContract.Referral.Language;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Amax.Referral.Web.Controllers
{

    public class HomeController : Controller
    {
        ILanguageService _Service;
        public HomeController(ILanguageService Service)
        {
            _Service = Service;
        }
        public IActionResult Index()
        {
            int selectedLanguageId = GetSelectedLangage();
            ViewBag.selectedLangage = selectedLanguageId;
            ViewBag.Resourses = _Service.GetLanguageResoure(selectedLanguageId, Section.Home.ToString()).Result;
            return View();
        }

       
        public IActionResult Privacy()
        {
            int selectedLanguageId = GetSelectedLangage();
            ViewBag.Resourses = _Service.GetLanguageResoure(selectedLanguageId, Section.PrivacyPolicy.ToString()).Result;

            return View();
        }

        public IActionResult TermAndCondition()
        {
            int selectedLanguageId = GetSelectedLangage();
            ViewBag.Resourses = _Service.GetLanguageResoure(selectedLanguageId, Section.TermsAndCondition.ToString()).Result;

            return View();
        }

        public IActionResult SetLanguage(string id)
        {
           
            HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), id);
            return Json("ok");
        }

        

        [NonAction]
        private int GetSelectedLangage()
        {
            var SelectedLangauge = HttpContext.Session.Get(SelectedLanguage.Language.ToString());
            if (SelectedLangauge == null)
            {
                var languages = _Service.GetLanguage().Result;
                var defaultLanguage = 1;
                foreach (var x in languages)
                {
                    defaultLanguage = x.Id;
                    break;
                }
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), defaultLanguage.ToString());
            }

            int selectedLanguageId = 1;
            string seletedlanguage = HttpContext.Session.GetString(SelectedLanguage.Language.ToString());
            if (!Int32.TryParse(seletedlanguage, out selectedLanguageId))
            {
                selectedLanguageId = 1;
            }

            return selectedLanguageId;
        }



    }
}
