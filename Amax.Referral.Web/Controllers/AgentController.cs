﻿using Amax.Referral.Web.Models;
using Amax.Referral.Web.Utility;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Amax.Referral.Web.Controllers
{
    public class AgentController : Controller
    {
        IReferralService referralService;
        IAgencyService agencyService;
        ILanguageService _Service;
        IConfiguration _config;
        public AgentController(IReferralService _referralService, IAgencyService _agencyService, ILanguageService Service, IConfiguration config)
        {
            referralService = _referralService;
            agencyService = _agencyService;
            _Service = Service;
            _config = config;
        }
        public IActionResult Index()
        {
            int selectedLanguageId = GetSelectedLangage();
            ViewBag.Resourses = _Service.GetLanguageResoure(selectedLanguageId, Section.Agent.ToString()).Result;
            ViewBag.Agency = Utility.Utility.GetAgencySelectList(agencyService.Get().Result);
            var model = new AgentRefModel();
            if (TempData["AgentModel"] != null)
            {
                var AgentModel = TempData.Get<AgentRefModel>("AgentModel");
                if (AgentModel != null)
                {
                    model.CustomerName = AgentModel.CustomerName;
                    model.CustomerEmail = AgentModel.CustomerEmail;
                    model.CustomerPhone = AgentModel.CustomerPhone;
                    model.AgencyId = AgentModel.AgencyId;
                    model.AgentId = AgentModel.AgentId;
                }
            }
            
            
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult SaveAgentReferral(AgentRefModel model, string type)
        {
            string result = "success";
            if (ModelState.IsValid)
            {
                try
                {
                    int selectedLanguageId = GetSelectedLangage();
                    var languageResources = _Service.GetLanguageResoure(selectedLanguageId, Section.Agent.ToString()).Result;
                    string emailMessageToCustomer = languageResources.Where(m => m.ResourceKey == Resource.EmailCustomer.ToString()).FirstOrDefault().ResourceText;
                    string emailMessageToReferance = languageResources.Where(m => m.ResourceKey == Resource.EmailReferance.ToString()).FirstOrDefault().ResourceText;
                    string subject = languageResources.Where(m => m.ResourceKey == Resource.EmailCustomerSubject.ToString()).FirstOrDefault().ResourceText;
                    string subjectReferance = languageResources.Where(m => m.ResourceKey == Resource.EmailReferanceSubject.ToString()).FirstOrDefault().ResourceText;

                    string SMSCustomerMessage = languageResources.Where(m => m.ResourceKey == Resource.SMSCustomer.ToString()).FirstOrDefault().ResourceText;
                    string SMSReferanceMessage = languageResources.Where(m => m.ResourceKey == Resource.SMSReferance.ToString()).FirstOrDefault().ResourceText;

                    referralService.AddReferralByAgent(new AmaxIns.DataContract.Referral.AgentRefModel
                    {
                        AgencyId = model.AgencyId,
                        AgentId = model.AgentId,
                        RefEmail = model.RefEmail,
                        RefName = model.RefName,
                        RefPhone = model.RefPhone,
                        CustomerName = model.CustomerName,
                        CustomerEmail = model.CustomerEmail,
                        CustomerPhone = model.CustomerPhone
                    }, emailMessageToCustomer, emailMessageToReferance, subject, subjectReferance);
                    if (type == Submit.AddMore.ToString())
                    {
                        TempData.Put<AgentRefModel>("AgentModel", model);
                    }
                    result = "success";
                   
                    //string smsMessage = languageResources.Where(m => m.ResourceKey == Resource.Sms.ToString()).FirstOrDefault().ResourceText;
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(model.CustomerPhone))
                        {
                            var SmsAccountSid = _config.GetSection("SmsAccountSid").Value;
                            var SmsAuthToken = _config.GetSection("SmsAuthToken").Value;
                            var SmsFrom = _config.GetSection("SmsFrom").Value;
                            TwilioClient.Init(SmsAccountSid, SmsAuthToken);
                            var message = MessageResource.Create(
                                body: SMSCustomerMessage,
                                from: new Twilio.Types.PhoneNumber(SmsFrom),
                                to: new Twilio.Types.PhoneNumber("+1" + model.CustomerPhone)
                            );

                            var messageToReferance = MessageResource.Create(
                              body: SMSReferanceMessage,
                              from: new Twilio.Types.PhoneNumber(SmsFrom),
                              to: new Twilio.Types.PhoneNumber("+1" + model.RefPhone)
                          );

                        }
                    }
                    catch
                    {

                    }
                }
                catch
                {
                    result = "error";
                }
            }
            else
            {
                result = "error";
            }

            return RedirectToAction("Index", "Agent", new { message = result });
        }

        public IActionResult GetAgents()
        {
            var result = referralService.GetAgents();
            return Json(result);
        }

        [NonAction]
        private int GetSelectedLangage()
        {
            var SelectedLangauge = HttpContext.Session.Get(SelectedLanguage.Language.ToString());
            if (SelectedLangauge == null)
            {
                var languages = _Service.GetLanguage().Result;
                var defaultLanguage = 1;
                foreach (var x in languages)
                {
                    defaultLanguage = x.Id;
                    break;
                }
                HttpContext.Session.SetString(SelectedLanguage.Language.ToString(), defaultLanguage.ToString());
            }

            int selectedLanguageId = 1;
            string seletedlanguage = HttpContext.Session.GetString(SelectedLanguage.Language.ToString());
            if (!Int32.TryParse(seletedlanguage, out selectedLanguageId))
            {
                selectedLanguageId = 1;
            }

            return selectedLanguageId;
        }



    }
}