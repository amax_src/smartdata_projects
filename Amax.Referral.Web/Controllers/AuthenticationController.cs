﻿using Amax.Referral.Web.Models;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.Service.Refferal;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Security.Claims;

namespace Amax.Referral.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        IAgencyLoginService service;
        public AuthenticationController(IAgencyLoginService _service)
        {
            service = _service;
        }
        public IActionResult Index(bool isNotValidUser, bool isNotValidAgent)
        {
            ViewBag.isNotValidUser = isNotValidUser;
            ViewBag.isNotValidAgent = isNotValidAgent;
            return View(new AgencyLoginModel());
        }

        [HttpPost]
        public IActionResult Validate(AgencyLoginModel model)
        {
            AgencyLoginModel agencyData = new AgencyLoginModel();
            var authtoken = Guid.NewGuid().ToString();
            ClaimsIdentity identity = null;
            bool isAuthenticated = false;
            bool isNotValidUser = false;
            bool isNotValidAgent = false;
            var data =service.validateAgency(new AmaxIns.DataContract.Referral.AgencyLogin.AgencyLoginModel { AgencyLogin = model.AgencyLogin.Trim(), Password = model.Password.Trim() });
            if (!string.IsNullOrEmpty(model.AgentId))
            {
                var agentData = service.validateAgent(new AmaxIns.DataContract.Referral.AgencyLogin.AgentLoginModel { LoginAgentID = model.AgentId.Trim() });
                
                if(agentData == null)
                {
                    isNotValidAgent = true;
                }
            }
            if(data!=null)
            {
                agencyData.Token = authtoken;
                agencyData.AgencyId = data.AgencyId;
                agencyData.Zip = data.Zip;
                agencyData.AgentId = model.AgentId;
                agencyData.start = true;
                identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name,data.AgencyLogin ),
                    new Claim(ClaimTypes.UserData,JsonConvert.SerializeObject(agencyData) )
                }, CookieAuthenticationDefaults.AuthenticationScheme);
                if (isNotValidAgent == false)
                {
                    isAuthenticated = true;
                }
            }
            else
            {
                isNotValidUser = true;
            }

            if (isAuthenticated)
            {
                if(!string.IsNullOrEmpty(model.AgentId))
                {
                    TokenModel token = new TokenModel();
                    token.AgentId = Convert.ToInt32(model.AgentId);
                    token.Token = authtoken;
                    service.UpdateToken(token);
                }
                var principal = new ClaimsPrincipal(identity);
                var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                return RedirectToAction("Index", "ReferenceReport");
            }

            return RedirectToAction("Index", new { isNotValidUser, isNotValidAgent });
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index");
        }
    }
}