﻿
using Amax.Referral.Web.Models;
using Amax.Referral.Web.Utility;
using AmaxIns.DataContract.Referral;
using AmaxIns.DataContract.Referral.SearchFilter;
using AmaxIns.DataContract.Referral.Token;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Referral;
using Dapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using X.PagedList;

namespace Amax.Referral.Web.Controllers
{
    [Authorize]
    public class WinBackController : Controller
    {

        IConfiguration _config;
        IWinbackService _winbackService;
        IAgencyLoginService _agencyLogin;


        public WinBackController(IWinbackService winbackService, IConfiguration config, IAgencyLoginService agencyLogin)
        {
            _winbackService = winbackService;
            _config = config;
            _agencyLogin = agencyLogin;
        }

        public IActionResult Index(int? page, List<int> month, int year, string monthstr = "", string search = "", string direction = "desc", string sortkeyword = "id", bool sorting = false)
        {

            int agencyId = 0;
            string agentId = null;
            try
            {
                if (sorting == true)
                {
                    month = monthstr.Split(',').Select(int.Parse).ToList();
                }
                TokenModel token = new TokenModel();
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                agencyId = userdata.AgencyId;
                agentId = userdata.AgentId; 
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if(!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
               
                if (month.Count() == 0)
                {
                    if (!string.IsNullOrEmpty(agentId))
                    {
                        var searchData = _agencyLogin.GetSearchParameters(new AmaxIns.DataContract.Referral.AgencyLogin.AgentLoginModel { LoginAgentID = agentId.Trim() });
                        if (searchData != null)
                        {
                            List<int> months = new List<int>();
                            months[0] = Convert.ToInt32(searchData.SearchMonth);
                            year = Convert.ToInt32(searchData.SearchYear);
                            direction = searchData.SortDirection;
                            sortkeyword = searchData.SortParameter;
                            search = searchData.SearchParameter;
                            month.Add(months[0]);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(agentId))
                    {
                        SearchFilterModel searchFilter = new SearchFilterModel();
                        searchFilter.AgentId = Convert.ToInt32(agentId);
                        searchFilter.SearchMonth = month[0].ToString();
                        searchFilter.SearchYear = year.ToString();
                        searchFilter.SortDirection = direction;
                        searchFilter.SortParameter = sortkeyword;
                        searchFilter.SearchParameter = search;

                        _winbackService.UpdateSearchFilter(searchFilter);
                    }
                }

            }
            catch
            {

            }
            if (month.Count() == 0)
            {
                month.Add(_winbackService.GetLatestMonthDataAvaliable(agencyId));
            }

            if (year == 0)
            {
                year = 2021;
            }
            if (string.IsNullOrEmpty(direction))
            {
                direction = "desc";
            }
            if (string.IsNullOrEmpty(sortkeyword))
            {
                sortkeyword = "id";
            }

            int totalcount = 0;
            var pageNumber = page ?? 1;

            var result = _winbackService.WinbackReport(month, year, monthstr, agencyId, search, pageNumber, out totalcount, direction, sortkeyword,sorting).ToList();
            var data = result;
            int totalpages = GetTotalPages(totalcount, 100);


            int _startPage = pageNumber - 5;
            //Paging to be end with
            int _endPage = pageNumber + 4;
            if (_startPage <= 0)
            {
                _endPage -= (_startPage - 1);
                _startPage = 1;
            }
            if (_endPage > totalpages)
            {
                _endPage = totalpages;
                if (_endPage > 10)
                {
                    _startPage = _endPage - 9;
                }
            }

            ViewBag.agentId = agentId;
            ViewBag.start = false;
            ViewBag.direction = direction;
            ViewBag.Sortkeyword = sortkeyword;
            ViewBag.search = search;
            ViewBag.selectedpage = pageNumber;
            ViewBag.TotalPages = totalpages;
            ViewBag.IsAdmin = agencyId == -1 ? true : false;
            ViewBag.year = year;
            ViewBag.month = month;
            ViewBag.EndPage = _endPage;
            ViewBag.StartPage = _startPage;
            ViewBag.Months = Utility.Utility.GetMonthSelectList().Where(x => x.Value != "13");
            ViewBag.monthStr = string.Join(",", month).Replace("'", "''");
            ViewBag.Years = Utility.Utility.GetYearSelectList();
            ViewBag.LeaveUs = Utility.Utility.GetLeaveUsSelectList();
            ViewBag.minDate = DateTime.MinValue;


            return View(data);

        }

        public IActionResult WinbackCommentBox(int id)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            var comments = _winbackService.GetComments(id);
            comments.ReferalApp_WinbackId = id;
            comments.AgencyId = userdata.AgencyId;
            comments.AgentId = Convert.ToInt32(userdata.AgentId);
            ViewBag.AgentId = Convert.ToInt32(userdata.AgentId);
            return View(comments);
        }

        public IActionResult SaveComment(WinbackCommentWrapper model)
        {
            try
            {
                AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
                TokenModel token = new TokenModel();
                token.AgentId = Convert.ToInt32(userdata.AgentId);
                token.Token = userdata.Token;
                if (!_agencyLogin.ValidateToken(token))
                {
                    return RedirectToAction("Logout", "Authentication");
                }
                _winbackService.Addcomments(model);
                return Json("success");
            }
            catch (Exception ex)
            {
                return Json("error");
            }
            //return Json("error");
        }

        public IActionResult UpdateWinBack(WinbackModel _model)
        {
            int agencyId = 0;
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }

            agencyId = userdata.AgencyId;

            try
            {
                if (_model.AgentId == userdata.AgentId)
                {
                    _winbackService.UpdateWinBack(_model, agencyId);
                    return Json("success");
                }
                else
                {
                    return Json("idError");
                }
            }
            catch (Exception ex)
            {
                return Json("error");
            }
        }

        public IActionResult ExportToCSV(string monthstr, int year)
        {
            AgencyLoginModel userdata = (AgencyLoginModel)JsonConvert.DeserializeObject(User.FindFirst(ClaimTypes.UserData).Value, typeof(AgencyLoginModel));
            TokenModel token = new TokenModel();
            token.AgentId = Convert.ToInt32(userdata.AgentId);
            token.Token = userdata.Token;
            if (!_agencyLogin.ValidateToken(token))
            {
                return RedirectToAction("Logout", "Authentication");
            }
            var result = _winbackService.WinbackReportCsv(monthstr, year);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"CustomerName,Carrier,PolicyNumber,EmailID,Phone,AgencyLocation,Quoted,Sold,Active,DateofLastTransaction,AgentId,Updatedate,AdditionalComment,CustomerLeaveComment,WhyDidyYouLeaveUs?,NoAnswer/Callback,InsuranceProStatus");
            foreach (var x in result)
            {
                sb.AppendLine(x.ToString());
            }
            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
            return File(bytes, "application /octet-stream", "Reports(" + monthstr + "/" + year + ").csv");
        }
        private static int GetTotalPages(int records, int pagesize)
        {
            int mod = records % pagesize;
            int pagecount = records / pagesize;

            if (mod > 0)
            {
                pagecount = pagecount + 1;
            }

            return pagecount;

        }



    }
}