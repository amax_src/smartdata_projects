﻿using Amax.Referral.Web.Models;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amax.Referral.Web.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        ILanguageService _Service;

        public HeaderViewComponent(ILanguageService Service)
        {
            _Service = Service;
        }

        public IViewComponentResult Invoke()
        {
            var seletedlanguage = HttpContext.Session.GetString(SelectedLanguage.Language.ToString());
            ViewBag.Language = Utility.Utility.GetLanguageSelectList(_Service.GetLanguage().Result, seletedlanguage.ToString());
            int languageId = 1;
            if (!Int32.TryParse(seletedlanguage, out languageId))
            {
                languageId = 1;
            }
            var model = _Service.GetLanguageResoure(languageId, Section.Header.ToString()).Result;
            return View("~/Views/Shared/_Header.cshtml", model);
        }
    }
}
