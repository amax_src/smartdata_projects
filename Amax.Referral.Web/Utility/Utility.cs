﻿using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.Referral.Language;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Amax.Referral.Web.Utility
{
    public static class Utility
    {
        public static List<SelectListItem> GetAgencySelectList(IEnumerable<AgencyModel> model)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Value = "0",
                Text = "Select",
            });
            foreach (var x in model)
            {
                if (!items.Any(m => m.Value == x.AgencyId.ToString()))
                {
                    items.Add(new SelectListItem
                    {
                        Value = x.AgencyId.ToString(),
                        Text = x.AgencyName,
                    });
                }
            }

            items.Add(new SelectListItem
            {
                Value = "99",
                Text = "Haltom City",
            });

            return items;
        }

        public static List<SelectListItem> GetLanguageSelectList(IEnumerable<LanguageModel> model, string seletedId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var x in model)
            {
                items.Add(new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Language,
                    Selected=x.Id.ToString()== seletedId
                });
            }

            return items;
        }

        public static IEnumerable<SelectListItem> GetMonthSelectList()
        {
            return DateTimeFormatInfo
               .InvariantInfo
               .MonthNames
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName
               });

        }

        public static IEnumerable<SelectListItem> GetYearSelectList()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            int currentYear = DateTime.Now.Year;
            
            while(currentYear >= 2019)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = currentYear.ToString();
                selectListItem.Value = currentYear.ToString();
                selectListItem.Selected = currentYear == DateTime.Now.Year ? true : false;

                selectListItems.Add(selectListItem);
                currentYear = currentYear - 1;
            }
            selectListItems.OrderBy(x => Convert.ToInt32(x.Value));
            return selectListItems.Select(x => new SelectListItem
               {
                   Value = x.Value,
                   Text = x.Text,
                   Selected = x.Selected
               }).OrderBy(x => x.Value);
        }

        public static IEnumerable<SelectListItem> GetLeaveUsSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            var selectListItems = new List<SelectListItem>() {
                new SelectListItem(){ Text = "Select option", Value= null},
                new SelectListItem(){ Text = "Price - monthly payment", Value="Price - monthly payment"},
                new SelectListItem(){ Text = "Price - down payment", Value="Price - down payment"},
                new SelectListItem(){ Text = "Convenience", Value="Convenience"},
                new SelectListItem(){ Text = "Customer Service", Value="Customer Service"},
                new SelectListItem(){ Text = "Couldn't afford / No longer need insurance", Value="Couldn't afford / No longer need insurance"}
            };

            return selectListItems.Select(x => new SelectListItem
            {
                Value = x.Value,
                Text = x.Text,
                Selected = x.Selected
            }).OrderBy(x => x.Value);
        }
    }
}
