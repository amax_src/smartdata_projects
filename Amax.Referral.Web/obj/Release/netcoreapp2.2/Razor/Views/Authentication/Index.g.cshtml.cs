#pragma checksum "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "917405212fa50d015e4ff32f7fbdbb4580a77e08"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Authentication_Index), @"mvc.1.0.view", @"/Views/Authentication/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Authentication/Index.cshtml", typeof(AspNetCore.Views_Authentication_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Chandan Sharma\Amax.Referral.Web\Views\_ViewImports.cshtml"
using Amax.Referral.Web;

#line default
#line hidden
#line 2 "D:\Chandan Sharma\Amax.Referral.Web\Views\_ViewImports.cshtml"
using Amax.Referral.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"917405212fa50d015e4ff32f7fbdbb4580a77e08", @"/Views/Authentication/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24d1ffa21415689e2e418e2d136d3492c0462bd7", @"/Views/_ViewImports.cshtml")]
    public class Views_Authentication_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Amax.Referral.Web.Models.AgencyLoginModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/Login.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery-validation/dist/jquery.validate.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
  
    Layout = null;

#line default
#line hidden
            BeginContext(77, 29, true);
            WriteLiteral("\r\n<!Doctype html>\r\n<html>\r\n\r\n");
            EndContext();
            BeginContext(106, 383, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "917405212fa50d015e4ff32f7fbdbb4580a77e085199", async() => {
                BeginContext(112, 320, true);
                WriteLiteral(@"
    <title>Login Page</title>
    <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
    <!-- Custom fonts for this template-->
    <link href=""https://fonts.googleapis.com/css?family=Montserrat:300i,400,500,600,700,800,900"" rel=""stylesheet"">

    <!-- Custom styles for this template-->

    ");
                EndContext();
                BeginContext(432, 48, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "917405212fa50d015e4ff32f7fbdbb4580a77e085907", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(480, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(489, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(493, 3577, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "917405212fa50d015e4ff32f7fbdbb4580a77e088040", async() => {
                BeginContext(499, 833, true);
                WriteLiteral(@"
    <div class=""home-page"">
        <div class=""container-fluid"">
            <div class=""row"">
                <div class=""col-sm-12 col-md-12"">
                    <div class=""home-bg new_login_page h-100vh"">
                        <div class=""row justify-content-center v-center"">
                            <div class=""col-md-6 col-lg-5 col-sm-8"">
                                <div class=""logo-content"">
                                    <div class=""home-content"">
                                        <div class=""login-content d-block"">
                                            <div class=""heading text-center"">
                                                <h2>Welcome</h2>
                                            </div>
                                            <div class=""login-form"">

");
                EndContext();
#line 36 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                 using (Html.BeginForm("Validate", "Authentication", FormMethod.Post))
                                                {

#line default
#line hidden
                BeginContext(1503, 220, true);
                WriteLiteral("                                                    <div class=\"form-group\">\r\n                                                        <div class=\"icon-block\">\r\n                                                            ");
                EndContext();
                BeginContext(1724, 96, false);
#line 40 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                       Write(Html.TextBoxFor(m => m.AgencyLogin, new { @class = "form-control", @placeholder = "User Name" }));

#line default
#line hidden
                EndContext();
                BeginContext(1820, 228, true);
                WriteLiteral("\r\n\r\n                                                        </div>\r\n                                                        <span style=\"color:red;text-align:center\">\r\n                                                            ");
                EndContext();
                BeginContext(2049, 45, false);
#line 44 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                       Write(Html.ValidationMessageFor(m => m.AgencyLogin));

#line default
#line hidden
                EndContext();
                BeginContext(2094, 347, true);
                WriteLiteral(@"
                                                        </span>
                                                    </div>
                                                    <div class=""form-group"">
                                                        <div class=""icon-block"">
                                                            ");
                EndContext();
                BeginContext(2442, 93, false);
#line 49 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                       Write(Html.PasswordFor(m => m.Password, new { @class = "form-control", @placeholder = "Password" }));

#line default
#line hidden
                EndContext();
                BeginContext(2535, 226, true);
                WriteLiteral("\r\n                                                        </div>\r\n                                                        <span style=\"color:red;text-align:center\">\r\n                                                            ");
                EndContext();
                BeginContext(2762, 42, false);
#line 52 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                       Write(Html.ValidationMessageFor(m => m.Password));

#line default
#line hidden
                EndContext();
                BeginContext(2804, 391, true);
                WriteLiteral(@"
                                                        </span>
                                                    </div>
                                                    <div class=""login-button text-center"">
                                                        <button type=""submit"" class=""login-btn"">Login</button>
                                                    </div>
");
                EndContext();
#line 58 "D:\Chandan Sharma\Amax.Referral.Web\Views\Authentication\Index.cshtml"
                                                }

#line default
#line hidden
                BeginContext(3246, 644, true);
                WriteLiteral(@"
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src=""https://code.jquery.com/jquery-3.2.1.slim.min.js""></script>
    <script src=""https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js""></script>
    <script src=""https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js""></script>

    ");
                EndContext();
                BeginContext(3890, 71, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "917405212fa50d015e4ff32f7fbdbb4580a77e0813815", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3961, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(3967, 90, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "917405212fa50d015e4ff32f7fbdbb4580a77e0815071", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4057, 6, true);
                WriteLiteral("\r\n\r\n\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4070, 11, true);
            WriteLiteral("\r\n\r\n</html>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Amax.Referral.Web.Models.AgencyLoginModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
