#pragma checksum "D:\Chandan Sharma\Amax.Referral.Web\Views\Home\Privacy.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d8a73a261b7742a209e494ce731b003008decd59"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Privacy), @"mvc.1.0.view", @"/Views/Home/Privacy.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Privacy.cshtml", typeof(AspNetCore.Views_Home_Privacy))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Chandan Sharma\Amax.Referral.Web\Views\_ViewImports.cshtml"
using Amax.Referral.Web;

#line default
#line hidden
#line 2 "D:\Chandan Sharma\Amax.Referral.Web\Views\_ViewImports.cshtml"
using Amax.Referral.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d8a73a261b7742a209e494ce731b003008decd59", @"/Views/Home/Privacy.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24d1ffa21415689e2e418e2d136d3492c0462bd7", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Privacy : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\Chandan Sharma\Amax.Referral.Web\Views\Home\Privacy.cshtml"
  
    List<AmaxIns.DataContract.Referral.Language.ResourceValueModel> resources = ViewBag.Resourses as List<AmaxIns.DataContract.Referral.Language.ResourceValueModel>;

#line default
#line hidden
            BeginContext(174, 326, true);
            WriteLiteral(@"
<div class=""strip""></div>

<!-- Privacy policy -->
<section class=""privacy-policy"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-lg-12"">
                <div class=""privacy-policy-box"">
                    <h1 class=""ty-mainbox-title text-left m-tb-30"">
                        ");
            EndContext();
            BeginContext(501, 124, false);
#line 14 "D:\Chandan Sharma\Amax.Referral.Web\Views\Home\Privacy.cshtml"
                   Write(Html.Raw(resources.Where(m => m.ResourceKey == Resource.PrivacyPolicyContentTitle.ToString()).FirstOrDefault().ResourceText));

#line default
#line hidden
            EndContext();
            BeginContext(625, 80, true);
            WriteLiteral("\r\n                    </h1>\r\n                    <div>\r\n                        ");
            EndContext();
            BeginContext(706, 119, false);
#line 17 "D:\Chandan Sharma\Amax.Referral.Web\Views\Home\Privacy.cshtml"
                   Write(Html.Raw(resources.Where(m => m.ResourceKey == Resource.PrivacyPolicyContent.ToString()).FirstOrDefault().ResourceText));

#line default
#line hidden
            EndContext();
            BeginContext(825, 188, true);
            WriteLiteral("\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<!-- Privacy policy /. -->\r\n\r\n<div class=\"strip margin-bottom-70\"></div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
