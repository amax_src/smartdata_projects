﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Common.Interface
{
    public interface IEmailService
    {
        bool SendEmail(string subject, string body, string to, string cc, string cc2 = "");
        bool SendOTPViaEmail(string subject, string body, string to);
    }
}
