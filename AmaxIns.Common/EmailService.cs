﻿using AmaxIns.Common.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AmaxIns.Common
{
    public  class EmailService: IEmailService
    {
        IConfiguration _configuration;
        public EmailService( IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public bool SendEmail(string subject, string body, string to,string cc,string cc2="")
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var password = _configuration.GetSection("Password").Value;
            int port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            try
            {
                SmtpClient client = new SmtpClient(smtp);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(from, password);
                client.Port = port;
                client.EnableSsl = true;
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(from, "A-MAX");
                mailMessage.To.Add(to);
                mailMessage.CC.Add(cc);
                if(!string.IsNullOrWhiteSpace(cc2))
                {
                    mailMessage.CC.Add(cc2);
                }
                mailMessage.Body = body;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool SendOTPViaEmail(string subject, string body, string to)
        {
            var smtp = _configuration.GetSection("Smtp").Value;
            var from = _configuration.GetSection("from").Value;
            var password = _configuration.GetSection("Password").Value;
            int port = Convert.ToInt32(_configuration.GetSection("Port").Value);
            try
            {
                SmtpClient client = new SmtpClient(smtp);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(from, password);
                client.Port = port;
                client.EnableSsl = true;
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(from, "A-MAX");
                mailMessage.To.Add(to);
                mailMessage.Body = body;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

    }
}
