﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Common
{
    public static class DidList
    {
        public static List<string> GetDidNumbers()
        {
            List<string> _data = new List<string>();
            _data.Add("18003000075");
            _data.Add("18003000078");
            _data.Add("18005000027");
            _data.Add("18006000038");
            _data.Add("18005000042");
            _data.Add("18009000068");
            _data.Add("18009000078");
            return _data;
        }
    }
}
