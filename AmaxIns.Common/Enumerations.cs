﻿using System.ComponentModel;

namespace AmaxIns.Common
{
    public class Enumeraion
    {
        public enum MaxBIReport
        {
            [Description("Carrier Agent")]
            CarrierAgent = 0,

            [Description("Agency Fee Per Policy")]
            AgencyFeePerPolicy = 1,

            [Description("Premium Per Policy")]
            PremiumPerPolicy = 2
        }

        public enum MaxBICatalogFrom
        {
            [Description("Payment Info")]
            PaymentInfo = 1,

            [Description("Quotes")]
            Quotes = 2,

            [Description("Calls")]
            Calls = 3
        }

        public enum MaxBICatalogType
        {
            [Description("Agent")]
            Agent = 1,

            [Description("Company")]
            Company = 2
        }

    }
}
