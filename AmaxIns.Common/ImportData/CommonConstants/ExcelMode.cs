﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Common.ImportData.CommonConstants
{
    public class ExcelMode
    {
        public const string xls = "application/vnd.ms-excel";
        public const string xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string csv = "text/csv";
        
        public const string actualFile = "ActualFile";
        public const string budgetFile = "BudgetFile";
    }
}
