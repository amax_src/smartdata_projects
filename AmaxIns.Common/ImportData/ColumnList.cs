﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmaxIns.Common.ImportData
{
    public static class ColumnList
    {
        public static List<string> GetActualColumnList()
        {
            List<string> _data = new List<string>();
            _data.Add("PlanID");
            _data.Add("Source");
            _data.Add("YearID");
            _data.Add("PeriodID");
            _data.Add("CategoryCode");
            _data.Add("CategoryDescription");
            _data.Add("DeptCode");
            _data.Add("DeptName");
            _data.Add("FrontBudgetCode");
            _data.Add("LineItemDescription");
            _data.Add("BudgetCode");
            _data.Add("AccountID");
            _data.Add("TRXDate");
            _data.Add("LocationID");
            _data.Add("TransactionDescription");
            _data.Add("Reference");
            _data.Add("Series");
            _data.Add("UserName");
            _data.Add("VendorName");
            _data.Add("DebitAmount");
            _data.Add("CreditAmount");
            _data.Add("TRXAmount");
            _data.Add("Note1");
            _data.Add("Note2");
            _data.Add("ReferID");
            return _data;
        }

        public static List<string> GetBudgetColumnList()
        {
            List<string> _data = new List<string>();
            _data.Add("PlanID");
            _data.Add("YearID");
            _data.Add("PeriodID");
            _data.Add("DeptName");
            _data.Add("CategoryDescription");
            _data.Add("FrontBudgetCode");
            _data.Add("SubBgtCode");
            _data.Add("LineItemDescription");
            _data.Add("Amount");
            return _data;
        }
    }
}

