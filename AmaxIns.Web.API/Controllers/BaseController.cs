﻿using AmaxIns.ServiceContract;
using AmaxIns.ServiceContract.Calendar;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BaseController : Controller
    {
        IBaseService _baseService;

        public IBaseService BaseService
        {
            get { return _baseService; }
        }

        public BaseController(IBaseService baseService)
        {
            _baseService = baseService;
        }
      
    }
}