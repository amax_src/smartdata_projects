﻿using AmaxIns.DataContract.Spectrum;
using AmaxIns.ServiceContract.Spectrum;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using AmaxIns.DataContract.Enums;
using Microsoft.AspNetCore.Authorization;

namespace AmaxIns.Web.API.Controllers.Spectrum
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SpectrumController : BaseController
    {
        protected readonly ISpectrumService _service;
        private IMemoryCache _cache;

        public SpectrumController(ISpectrumService service, IMemoryCache memoryCache) : base(service)
        {
            _service = service;
            _cache = memoryCache;
        }

        [Route("/api/Spectrum/GetSpectrumDailySummary")]
        [HttpGet]
        public async Task<IActionResult> GetSpectrumDailySummary()
        {
            var result = await _service.GetSpectrumDailySummary();
            var response = Ok(result);
            return response;
        }


        [Route("/api/Spectrum/GetSpectrumDailyDetail")]
        [HttpGet]
        public async Task<IActionResult> GetSpectrumDailyDetail([FromQuery(Name = "date")] string date, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            IActionResult response = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailModels = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailResult = new List<SpectrumDailyDetailModel>();
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailAgencyFilterResult = new List<SpectrumDailyDetailModel>();
            if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDetail.ToString(), out spectrumDailyDetailModels))
            {
                spectrumDailyDetailModels = await _service.GetSpectrumDailyDetail(date);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumDailyDetail.ToString(), spectrumDailyDetailModels, cacheEntryOptions);
            }

            spectrumDailyDetailResult = spectrumDailyDetailModels.Where(m => Convert.ToDateTime(m.Start_Time).ToShortDateString() == Convert.ToDateTime(date).ToShortDateString()).ToList();
            spectrumDailyDetailAgencyFilterResult = spectrumDailyDetailResult.Where(m => m.AgencyId == AgencyId);
            var result = spectrumDailyDetailAgencyFilterResult;//await _Service.GetSpectrumDailyDetail(date);
            response = Ok(result);
            return response;
        }

        [Route("/api/Spectrum/GetSpectrumMonthlySummary")]
        [HttpGet]
        public async Task<IActionResult> GetSpectrumMonthlySummary()
        {
            var result = await _service.GetSpectrumMonthlySummary();
            var response = Ok(result);
            return response;
        }


        [Route("/api/Spectrum/GetSpectrumMonthlyDetail")]
        [HttpGet]
        public async Task<IActionResult> GetSpectrumMonthlyDetail([FromQuery(Name = "month")] string month, [FromQuery(Name = "agencyId")] int agencyId)
        {

            var result = await _service.GetSpectrumMonthlyDetail(month, agencyId);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Spectrum/GetSpectrumCallVolume")]
        [HttpGet]
        public async Task<IActionResult> GetSpectrumCallVolume()
        {
            IActionResult response = null;
            ConsolidatedCallVolumeModel callvolume = new ConsolidatedCallVolumeModel();
            if (!_cache.TryGetValue(CacheKeys.CallVolume.ToString(), out callvolume))
            {
                callvolume = await _service.GetSpectrumCallVolume();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.CallVolume.ToString(), callvolume, cacheEntryOptions);
            }
            response = Ok(callvolume);
            return response;
        }

        [Route("/api/Spectrum/GetCallCountByLocationAndDate")]
        [HttpPost]
        public async Task<IActionResult> GetCallCountByLocationAndDate([FromQuery(Name = "date")] string date, [FromBody] List<int> Agencyid)
        {
            IActionResult response = null;
            IEnumerable<CallCountModel> TotalCallSAllresult = new List<CallCountModel>();
            if (!_cache.TryGetValue(CacheKeys.TotalCallSAll.ToString(), out TotalCallSAllresult))
            {
                TotalCallSAllresult = await _service.GetCallCount(null);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.TotalCallSAll.ToString(), TotalCallSAllresult, cacheEntryOptions);
            }

            var dateFilter = TotalCallSAllresult.Where(m => Convert.ToDateTime(m.Date).ToShortDateString() == Convert.ToDateTime(date).ToShortDateString()).ToList();
            var filterByLocations = new List<CallCountModel>();
            foreach (var x in Agencyid)
            {
                var agencyData=dateFilter.Where(m => m.AgencyId == x).ToList();
                foreach(var z in agencyData)
                {
                    filterByLocations.Add(z);
                }
            }


            var GorupByResult = filterByLocations.GroupBy(c => new
            {
                c.Cnumber
            }).Select(gcs => new CallCountModel()
            {
                Cnumber = gcs.Key.Cnumber,
                CountOfIncomingCalls = gcs.Sum(m => m.CountOfIncomingCalls)
            });

            var Top5CallCount = GorupByResult.OrderByDescending(m => m.CountOfIncomingCalls).ToList().Take(5);
            List<CallCountModel> Filterresulttop5 = new List<CallCountModel>();
            foreach (var x in Top5CallCount)
            {
                var filter = GorupByResult.Where(m => m.CountOfIncomingCalls == x.CountOfIncomingCalls);
                foreach (var z in filter)
                {
                    Filterresulttop5.Add(z);
                }
            }

            var Distintresult = Filterresulttop5.GroupBy(c => new
            {
                c.Cnumber
            }).Select(gcs => new CallCountModel()
            {
                Cnumber = gcs.Key.Cnumber,
                CountOfIncomingCalls =gcs.First().CountOfIncomingCalls //gcs.Sum(m => m.CountOfIncomingCalls)
            }); 

            response = Ok(Distintresult);
            return response;

        }



        [Route("/api/Spectrum/GetCallCountAll")]
        [HttpGet]
        public async Task<IActionResult> GetCallCountAll([FromQuery(Name = "date")] string date)
        {
            IActionResult response = null;
            IEnumerable<CallCountModel> TotalCallSAllresult = new List<CallCountModel>();
            TotalCallSAllresult = await _service.GetCallCount(date);
            response = Ok(TotalCallSAllresult);
            return response;

        }



        [Route("/api/Spectrum/GetCallCountAllOutBound")]
        [HttpGet]
        public async Task<IActionResult> GetCallCountAllOutBound([FromQuery(Name = "date")] string date)
        {
            IActionResult response = null;
            IEnumerable<CallCountModel> TotalCallSAllresult = new List<CallCountModel>();
            TotalCallSAllresult = await _service.GetCallCountOutBound(date);
            response = Ok(TotalCallSAllresult);
            return response;

        }

        [Route("/api/Spectrum/GetCallCountByLocationAndDateOutBound")]
        [HttpPost]
        public async Task<IActionResult> GetCallCountByLocationAndDateOutBound([FromQuery(Name = "date")] string date, [FromBody] List<int> Agencyid)
        {
            IActionResult response = null;
            IEnumerable<CallCountModel> TotalCallSAllresult = new List<CallCountModel>();
            if (!_cache.TryGetValue(CacheKeys.TotalCallSAllOutBound.ToString(), out TotalCallSAllresult))
            {
                TotalCallSAllresult = await _service.GetCallCountOutBound(null);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.TotalCallSAllOutBound.ToString(), TotalCallSAllresult, cacheEntryOptions);
            }

            var dateFilter = TotalCallSAllresult.Where(m => Convert.ToDateTime(m.Date).ToShortDateString() == Convert.ToDateTime(date).ToShortDateString()).ToList();
            var filterByLocations = new List<CallCountModel>();
            foreach (var x in Agencyid)
            {
                var agencyData = dateFilter.Where(m => m.AgencyId == x).ToList();
                foreach (var z in agencyData)
                {
                    filterByLocations.Add(z);
                }
            }


            var GorupByResult = filterByLocations.GroupBy(c => new
            {
                c.Cnumber
            }).Select(gcs => new CallCountModel()
            {
                Cnumber = gcs.Key.Cnumber,
                CountOfIncomingCalls = gcs.Sum(m => m.CountOfIncomingCalls)
            });

            var Top5CallCount = GorupByResult.OrderByDescending(m => m.CountOfIncomingCalls).ToList().Take(5);
            List<CallCountModel> Filterresulttop5 = new List<CallCountModel>();
            foreach (var x in Top5CallCount)
            {
                var filter = GorupByResult.Where(m => m.CountOfIncomingCalls == x.CountOfIncomingCalls);
                foreach (var z in filter)
                {
                    Filterresulttop5.Add(z);
                }
            }

            var Distintresult = Filterresulttop5.GroupBy(c => new
            {
                c.Cnumber
            }).Select(gcs => new CallCountModel()
            {
                Cnumber = gcs.Key.Cnumber,
                CountOfIncomingCalls = gcs.First().CountOfIncomingCalls //gcs.Sum(m => m.CountOfIncomingCalls)
            });

            response = Ok(Distintresult);
            return response;

        }


        [Route("/api/Spectrum/RepeatedCallers")]
        [HttpGet]
        public async Task<IActionResult> RepeatedCallers([FromQuery(Name = "date")] string date)
        {
            IActionResult response = null;
            IEnumerable<RepeatedCallersModel> callvolume = new List<RepeatedCallersModel>();
            IEnumerable<RepeatedCallersModel> callvolumeResult = new List<RepeatedCallersModel>();

            if (!_cache.TryGetValue(CacheKeys.RepeatedCallers.ToString(), out callvolume))
            {
                callvolume = await _service.GetRepeatedCallers(null);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.RepeatedCallers.ToString(), callvolume, cacheEntryOptions);
            }

            callvolumeResult = callvolume.Where(m => Convert.ToDateTime(m.Date).ToShortDateString() == Convert.ToDateTime(date).ToShortDateString());
            response = Ok(callvolumeResult);
            return response;
        }


        [Route("/api/Spectrum/GetCallOutBoundLastSevenDay")]
        [HttpPost]
        public async Task<IActionResult> GetCallOutBoundLastSevenDay([FromBody] List<int> agencyId)
        {
            IActionResult response = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailModels = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailResult = new List<SpectrumDailyDetailModel>();
            long outboundcalls = 0;
            Dictionary<long, string> output = new Dictionary<long, string>();
            if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDetail.ToString(), out spectrumDailyDetailModels))
            {
                spectrumDailyDetailModels = await _service.GetSpectrumDailyDetail(string.Empty);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumDailyDetail.ToString(), spectrumDailyDetailModels, cacheEntryOptions);
            }

            foreach (var x in agencyId)
            {
                outboundcalls = outboundcalls + spectrumDailyDetailModels.Where(m => m.AgencyId == x && m.Uniqueid.ToLower().Contains("out")).Count();

            }
            var startdate = spectrumDailyDetailModels.Min(m => Convert.ToDateTime(m.Date)).ToString("MMMM dd");
            var enddate = spectrumDailyDetailModels.Max(m => Convert.ToDateTime(m.Date)).ToString("MMMM dd");
            output.Add(outboundcalls, startdate + " - " + enddate);

            var result = output;//await _Service.GetSpectrumDailyDetail(date);
            response = Ok(result);
            return response;
        }

        [Route("/api/Spectrum/GetTalkTimeOutBoundLastSevenDay")]
        [HttpPost]
        public async Task<IActionResult> GetTalkTimeOutBoundLastSevenDay([FromBody] List<int> agencyIds)
        {
            IActionResult response = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailModels = null;
            IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailResult = new List<SpectrumDailyDetailModel>();
            long outboundCallMints = 0;
            Dictionary<long, string> output = new Dictionary<long, string>();
            if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDetail.ToString(), out spectrumDailyDetailModels))
            {
                spectrumDailyDetailModels = await _service.GetSpectrumDailyDetail(string.Empty);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumDailyDetail.ToString(), spectrumDailyDetailModels, cacheEntryOptions);
            }

            foreach(var x in agencyIds)
            {
                outboundCallMints = outboundCallMints + spectrumDailyDetailModels.Where(m => m.AgencyId == x && m.Uniqueid.ToLower().Contains("out")).Sum(m => Convert.ToInt64(m.Talktime));

            }
            var startdate = spectrumDailyDetailModels.Min(m => Convert.ToDateTime(m.Date)).ToString("MMMM dd");
            var enddate = spectrumDailyDetailModels.Max(m => Convert.ToDateTime(m.Date)).ToString("MMMM dd");
            output.Add((outboundCallMints / 60), startdate+" - "+enddate);

            var result = output;//await _Service.GetSpectrumDailyDetail(date);
            response = Ok(result);
            return response;
        }

        [Route("/api/Spectrum/GetDate")]
        [HttpGet]
        public async Task<IActionResult> GetDate()
        {
            IActionResult response = null;
            IEnumerable<string> Dates;
            if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDates.ToString(), out Dates))
            {
                Dates = await _service.GetDates();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumDailyDates.ToString(), Dates, cacheEntryOptions);
            }
            response = Ok(Dates);
            return response;
        }

    }
}