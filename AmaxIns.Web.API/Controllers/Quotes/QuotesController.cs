﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.Quotes;
using AmaxIns.ServiceContract.Quotes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace AmaxIns.Web.API.Controllers.Quotes
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class QuotesController : BaseController
    {
        protected readonly IQuotesService _service;
        private IMemoryCache _cache;
        private readonly IHostingEnvironment _hostingEnvironment;
        public QuotesController(IQuotesService service, IMemoryCache memoryCache, IHostingEnvironment hostingEnvironment) : base(service)
        {
            _service = service;
            _cache = memoryCache;
            _hostingEnvironment = hostingEnvironment;
        }

        [Route("/api/Quotes/GetPaydate")]
        [HttpGet]
        public async Task<IActionResult> GetPaydate([FromQuery(Name = "year")] string year, [FromQuery(Name = "Month")] string Month)
        {
            var result = await _service.GetPaydate(year, Month);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/GetBoundQuotes")]
        [HttpGet]
        public async Task<IActionResult> GetBoundQuotes([FromQuery(Name = "date")] string date)
        {
            var data = _service.GetBoundQuotes(date);
            var response = Ok(data);
            return response;
        }

        [Route("/api/Quotes/GetBoundNoTurboratorQuotes")]
        [HttpGet]
        public async Task<IActionResult> GetBoundNoTurboratorQuotes([FromQuery(Name = "date")] string date)
        {
            var data = _service.GetBoundNoTurboratorQuotes(date);
            var response = Ok(data);
            return response;
        }

        [Route("/api/Quotes/GetQuotesSale")]
        [HttpGet]
        public async Task<IActionResult> GetQuotesSale()
        {
            IEnumerable<QuotesSale> QuotesSaleResult = null;
            if (!_cache.TryGetValue(CacheKeys.QuotesSale.ToString(), out QuotesSaleResult))
            {
                QuotesSaleResult = _service.GetQuotesSale();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.QuotesSale.ToString(), QuotesSaleResult, cacheEntryOptions);
            }

            var response = Ok(QuotesSaleResult);
            return response;
        }

        [Route("/api/Quotes/GetNewModiFiedQuotes")]
        [HttpPost]
        public async Task<IActionResult> GetNewModiFiedQuotes([FromBody] List<string> AgencyIds, [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            var result = await _service.GetNewModiFiedQuotes(AgencyIds, month, year);
            var response = Ok(result);
            return response;
        }


        [Route("/api/Quotes/GetNewAndModifiedData_New")]
        [HttpPost]
        public async Task<IActionResult> GetNewAndModifiedData_New([FromBody] QuotesParam quotesParam, [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            var result = await _service.GetNewAndModifiedData_New(quotesParam, month, year);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/ModifiedQuotesSoldTiles")]
        [HttpPost]
        public async Task<IActionResult> ModifiedQuotesSoldTiles([FromBody] QuotesParam quotesParam, [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            var result = await _service.ModifiedQuotesSoldTiles(quotesParam, month, year);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/GetDistinctDateQuotes")]
        [HttpGet]
        public async Task<IActionResult> GetDistinctDateQuotes([FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            var result = await _service.GetDistinctDateQuotes(month, year);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/GetNewModiFiedQuotesProjection")]
        [HttpGet]
        public async Task<IActionResult> GetNewModiFiedQuotesProjection( [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            var result = await _service.GetNewModiFiedQuotesProjection(month, year);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/GetQuotesProjection")]
        [HttpGet]
        public async Task<IActionResult> GetQuotesProjection()
        {
            var result = await _service.GetQuotesProjection();
            var response = Ok(result);
            return response;
        }

        [Route("/api/Quotes/ExportNewQuotes/")]
        [HttpPost]
        public async Task<IActionResult> ExportNewQuotes([FromBody] IEnumerable<NewModifiedQuotes> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _service.ExportNewModiFiedQuotes(list, root + "\\", 0);
            var response = Ok(FileName);
            return response;
        }

        [Route("/api/Quotes/ExportModiFiedQuotes/")]
        [HttpPost]
        public async Task<IActionResult> ExportModiFiedQuotes([FromBody] IEnumerable<NewModifiedQuotes> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _service.ExportNewModiFiedQuotes(list, root + "\\", 1);
            var response = Ok(FileName);
            return response;
        }
    }
}