﻿using AmaxIns.ServiceContract.Dates;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.Date
{
    [Route("api/[controller]")]
    [ApiController]
    public class DateController : BaseController
    {
        protected readonly IYearService _yearService;
        protected readonly IMonthService _monthService;

        public DateController(IYearService yearService, IMonthService monthService) : base(yearService)
        {
            _yearService = yearService;
            _monthService = monthService;
        }


        [HttpGet]
        [Route("/api/Date/GetYears")]
        public async Task<IActionResult> GetYears()
        {
            var result = await _yearService.GetYears();

            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetMonth/{year}")]
        public async Task<IActionResult> GetMonth(int year)
        {
            var result = await _monthService.GetMonths(year);

            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetMonthKeyValue/{year}")]
        public async Task<IActionResult> GetMonthKeyValue(int year)
        {
            var result = await _monthService.GetMonthKeyValue(year);

            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetMonthsCurrentYear")]
        public async Task<IActionResult> GetMonthsCurrentYear()
        {
            var result = await _monthService.GetMonths(DateTime.Now.Year-1);
            var response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetDates")]
        public async Task<IActionResult> GetDates()
        {
            var result = await _monthService.GetDates();

            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetExtraMonthKeyValue/{year}")]
        public async Task<IActionResult> GetExtraMonthKeyValue(int year)
        {
            var result = await _monthService.GetExtraMonthKeyValue(year);

            var response = Ok(result);

            return response;
        }


        [HttpGet]
        [Route("/api/Date/GetMonthPlanner")]
        public async Task<IActionResult> GetMonthPlanner()
        {
            var result = await _monthService.GetMonthPlanner();

            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Date/GetDatesByMonth/{monthName}/{year}")]
        public async Task<IActionResult> GetDatesByMonth(string monthName,string year)
        {
            var result = await _monthService.GetDatesByMonth(monthName,year);

            var response = Ok(result);

            return response;
        }

    }
}