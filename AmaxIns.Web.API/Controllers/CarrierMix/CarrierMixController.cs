﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.CarrierMix;
using AmaxIns.ServiceContract.CarrierMix;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.CarrierMix
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarrierMixController : BaseController
    {
        protected readonly ICarrierMixService _carrierMixService;
        public CarrierMixController(ICarrierMixService carrierMixService) : base(carrierMixService)
        {
            _carrierMixService = carrierMixService;
        }

        [HttpGet]
        [Route("/api/CarrierMix/GetCarrier/{month}/{year}")]
        public async Task<IActionResult> GetCarrier(string month,string year)
        {
            var result = await _carrierMixService.GetCarrier(month,year);
            var response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/CarrierMix/GetDate/{month}/{year}")]
        public async Task<IActionResult> GetDate(string month, string year)
        {
            var result = await _carrierMixService.GetDates(month,year);
            var response = Ok(result);
            return response;
        }


        [HttpPost]
        [Route("/api/CarrierMix/GetData")]
        public async Task<IActionResult> GetData([FromBody] CarrierMixUserModel model)
        {
            var result = await _carrierMixService.GetCarrierMixData(model);
            var response = Ok(result);
            return response;
        }
    }
}