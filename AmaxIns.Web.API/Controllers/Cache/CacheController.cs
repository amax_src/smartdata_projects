﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.AIS;
using AmaxIns.DataContract.AplhaCustomerInfo;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.PBX;
using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using AmaxIns.DataContract.Spectrum;
using AmaxIns.ServiceContract.AIS;
using AmaxIns.ServiceContract.AlphaCustomerInfo;
using AmaxIns.ServiceContract.EPR;
using AmaxIns.ServiceContract.LiveDataApp;
using AmaxIns.ServiceContract.Payroll;
using AmaxIns.ServiceContract.PBX;
using AmaxIns.ServiceContract.Quotes;
using AmaxIns.ServiceContract.Revenue;
using AmaxIns.ServiceContract.Spectrum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace AmaxIns.Web.API.Controllers.Cache
{
    [Produces("application/json")]
    public class CacheController : BaseController
    {

        protected readonly IPayrollService _payrollService;
        protected readonly IAISService _aisService;
        private IMemoryCache _cache;
        protected readonly ISpectrumService _spectrumService;
        protected readonly IEPRService _ePRService;
        protected readonly IRevenueService _revenueService;
        protected readonly IPbxService _pbxService;
        protected readonly ICustomerInfoService _customerInfoService;
        protected readonly ILiveDataAppService _liveDataAppService;
        protected readonly IQuotesService _quotesService;
        protected readonly ISpectrumLiveDataService _spectrumLiveDataService;
        protected readonly IBolQuoteService _IBolQuoteService;
        public CacheController(IPayrollService payrollService, IMemoryCache memoryCache, IAISService aisService, IPbxService pbxService,
            ISpectrumService spectrumService, IEPRService ePRService, IRevenueService revenueService, ICustomerInfoService customerInfoService,
            ILiveDataAppService liveDataAppService, IQuotesService quotesService, ISpectrumLiveDataService spectrumLiveDataService
            , IBolQuoteService IBolQuoteService) : base(payrollService)
        {
            _payrollService = payrollService;
            _cache = memoryCache;
            _aisService = aisService;
            _spectrumService = spectrumService;
            _ePRService = ePRService;
            _revenueService = revenueService;
            _pbxService = pbxService;
            _customerInfoService = customerInfoService;
            _liveDataAppService = liveDataAppService;
            _quotesService = quotesService;
            _spectrumLiveDataService = spectrumLiveDataService;
            _IBolQuoteService = IBolQuoteService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Cache/ResetCache")]
        public async Task<IActionResult> ResetCache()
        {
            IActionResult response = null;
            CombinePayRollModel Payrollresult = null;
            IEnumerable<AISModel> AisResult = null;
            IEnumerable<EPRModel> Eprresult = null;
            IEnumerable<RevenueModel> Revenueresult = null;
            IEnumerable<RevenueDailyModel> RevenueDailyresult = null;
            ConsolidatedCallVolumeModel callvolume = new ConsolidatedCallVolumeModel();
            IEnumerable<CallCountModel> TotalCallSAllresult = new List<CallCountModel>();
            IEnumerable<CallCountModel> TotalCallSAllOutBoundresult = new List<CallCountModel>();
            IEnumerable<RepeatedCallersModel> RepeatedCallersModel = new List<RepeatedCallersModel>();
            IEnumerable<PbxModel> PbxResult = null;
            ConsolidatedTileData PayrollTileresult = new ConsolidatedTileData();
            IEnumerable<PbxModel> PbxDailyResult = null;
            IEnumerable<string> months = new List<string>();
            IEnumerable<string> Dates = new List<string>();
            IEnumerable<CustomerInfoModel> CustomerInfoResult = null;
            IEnumerable<QuotesSale> QuotesSaleResult = null;
            List<BOLQuoteData> BOLQuoteData = null;

            try
            {

                _cache.Remove(CacheKeys.PBX.ToString());
                if (!_cache.TryGetValue(CacheKeys.PBX.ToString(), out PbxResult))
                {
                    PbxResult = await _pbxService.GetPBXData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.PBX.ToString(), PbxResult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.SpectrumDailyDates.ToString());
                if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDates.ToString(), out Dates))
                {
                    Dates = await _spectrumService.GetDates();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.SpectrumDailyDates.ToString(), Dates, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.PBXDaily.ToString());
                if (!_cache.TryGetValue(CacheKeys.PBXDaily.ToString(), out PbxDailyResult))
                {
                    PbxDailyResult = await _pbxService.GetPBXDailyData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.PBXDaily.ToString(), PbxDailyResult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.PayRoleBudgetedActualData.ToString());
                if (!_cache.TryGetValue(CacheKeys.PayRoleBudgetedActualData.ToString(), out Payrollresult))
                {
                    Payrollresult = await _payrollService.GetPayRoleActualAndBudgetedData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.PayRoleBudgetedActualData.ToString(), Payrollresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.PayrollTiles.ToString());
                if (!_cache.TryGetValue(CacheKeys.PayrollTiles.ToString(), out PayrollTileresult))
                {
                    PayrollTileresult = await _payrollService.GetPayRoleTilesConsolidatedData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.PayrollTiles.ToString(), PayrollTileresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.GetEPRData.ToString());
                if (!_cache.TryGetValue(CacheKeys.GetEPRData.ToString(), out Eprresult))
                {
                    Eprresult = await _ePRService.GetEPRData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.GetEPRData.ToString(), Eprresult, cacheEntryOptions);
                }

                IEnumerable<string> EPRMonth = null;
                _cache.Remove(CacheKeys.EPRMonth.ToString());
                if (!_cache.TryGetValue(CacheKeys.EPRMonth.ToString(), out EPRMonth))
                {
                    EPRMonth = await _ePRService.GetEPRmonths();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.EPRMonth.ToString(), EPRMonth, cacheEntryOptions);
                }


                _cache.Remove(CacheKeys.AISData.ToString());
                if (!_cache.TryGetValue(CacheKeys.AISData.ToString(), out AisResult))
                {
                    AisResult = await _aisService.GetAISData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.AISData.ToString(), AisResult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.RevenueMonthly.ToString());
                if (!_cache.TryGetValue(CacheKeys.RevenueMonthly.ToString(), out Revenueresult))
                {
                    Revenueresult = await _revenueService.GetRevenueData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.RevenueMonthly.ToString(), Revenueresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.RevenueDaily.ToString());
                if (!_cache.TryGetValue(CacheKeys.RevenueDaily.ToString(), out RevenueDailyresult))
                {
                    RevenueDailyresult = await _revenueService.GetRevenueDailyData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.RevenueDaily.ToString(), RevenueDailyresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.CallVolume.ToString());
                if (!_cache.TryGetValue(CacheKeys.CallVolume.ToString(), out callvolume))
                {
                    callvolume = await _spectrumService.GetSpectrumCallVolume();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.CallVolume.ToString(), callvolume, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.TotalCallSAll.ToString());
                if (!_cache.TryGetValue(CacheKeys.TotalCallSAll.ToString(), out TotalCallSAllresult))
                {
                    TotalCallSAllresult = await _spectrumService.GetCallCount(null);
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.TotalCallSAll.ToString(), TotalCallSAllresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.TotalCallSAllOutBound.ToString());
                if (!_cache.TryGetValue(CacheKeys.TotalCallSAllOutBound.ToString(), out TotalCallSAllresult))
                {
                    TotalCallSAllOutBoundresult = await _spectrumService.GetCallCountOutBound(null);
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.TotalCallSAllOutBound.ToString(), TotalCallSAllOutBoundresult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.RepeatedCallers.ToString());
                if (!_cache.TryGetValue(CacheKeys.RepeatedCallers.ToString(), out callvolume))
                {
                    RepeatedCallersModel = await _spectrumService.GetRepeatedCallers(null);
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.RepeatedCallers.ToString(), RepeatedCallersModel, cacheEntryOptions);
                }


                _cache.Remove(CacheKeys.SpectrumDailyDetail.ToString());
                IEnumerable<SpectrumDailyDetailModel> spectrumDailyDetailModels = null;
                if (!_cache.TryGetValue(CacheKeys.SpectrumDailyDetail.ToString(), out spectrumDailyDetailModels))
                {
                    spectrumDailyDetailModels = await _spectrumService.GetSpectrumDailyDetail(null);
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.SpectrumDailyDetail.ToString(), spectrumDailyDetailModels, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.CustomerInfoAlpha.ToString());
                if (!_cache.TryGetValue(CacheKeys.CustomerInfoAlpha.ToString(), out CustomerInfoResult))
                {
                    CustomerInfoResult = await _customerInfoService.CustomerInfoData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.CustomerInfoAlpha.ToString(), CustomerInfoResult, cacheEntryOptions);
                }

                _cache.Remove(CacheKeys.QuotesSale.ToString());
                if (!_cache.TryGetValue(CacheKeys.QuotesSale.ToString(), out QuotesSaleResult))
                {
                    QuotesSaleResult = _quotesService.GetQuotesSale();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.QuotesSale.ToString(), QuotesSaleResult, cacheEntryOptions);
                }
                
                response = Ok("Success");

            }
            catch (Exception ex)
            {
                response = Ok(ex.Message + System.Environment.NewLine + ex.InnerException.ToString());
            }


            return response; 
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Cache/ResetRevenueCache")]
        public async Task<IActionResult> ResetRevenueCache()
        {
            //if(_cache.)
            IActionResult response = null;
            try
            {
                IEnumerable<RevenueModel> Revenueresult = null;
                _cache.Remove(CacheKeys.RevenueMonthly.ToString());
                if (!_cache.TryGetValue(CacheKeys.RevenueMonthly.ToString(), out Revenueresult))
                {
                    Revenueresult = await _revenueService.GetRevenueData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.RevenueMonthly.ToString(), Revenueresult, cacheEntryOptions);
                }

                response = Ok("Success");
            }
            catch
            {
                response = Ok("Error");
            }


            return response;
        }



        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Cache/ResetPayrollCache")]
        public async Task<IActionResult> ResetPayrollCache()
        {
            //if(_cache.)
            IActionResult response = null;
            try
            {
                _cache.Remove(CacheKeys.PayRoleBudgetedActualData.ToString());

                CombinePayRollModel Payrollresult = null;
                if (!_cache.TryGetValue(CacheKeys.PayRoleBudgetedActualData.ToString(), out Payrollresult))
                {
                    Payrollresult = await _payrollService.GetPayRoleActualAndBudgetedData();
                    var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                    _cache.Set(CacheKeys.PayRoleBudgetedActualData.ToString(), Payrollresult, cacheEntryOptions);
                }

                response = Ok("Success");
            }
            catch
            {
                response = Ok("Error");
            }


            return response;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Cache/LiveAppCache")]
        public IActionResult LiveAppCache()
        {
            IActionResult response = null;
            try
            {
                List<AgentCountModel> _AmaxHourlyData;
                List<AgentCountModel> _AmaxHourlyDataAgencywise;
                List<LiveDataAgency> _livedata = null;
                _livedata = _liveDataAppService.GetData(out _AmaxHourlyData,out _AmaxHourlyDataAgencywise);
                _cache.Remove(CacheKeys.LiveData.ToString());
                _cache.Remove(CacheKeys.AmaxLiveData.ToString());
                _cache.Remove(CacheKeys.AmaxLiveDataAgencyWise.ToString());

                //if (!_cache.TryGetValue(CacheKeys.LiveData.ToString(), out _livedata))
                //{
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.LiveData.ToString(), _livedata, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveData.ToString(), _AmaxHourlyData, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveDataAgencyWise.ToString(), _AmaxHourlyDataAgencywise, cacheEntryOptions);
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.LiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
                // }
                response = Ok("Live data app cache Success");
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError, "Live data app" + System.Environment.NewLine + ex);
            }

            return response;
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Cache/LiveSpectrumCache")]
        public IActionResult LiveSpectrumCache()
        {
            IActionResult response = null;
            try
            {
                IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumLiveData.ToString(), spectrumLiveDataResult, cacheEntryOptions);

                //IEnumerable<SpectrumLiveData> spectrumLiveDataResultV1 = null;
                //spectrumLiveDataResultV1 = _spectrumLiveDataService.GetSpectrumLiveDataV1();
                //_cache.Set(CacheKeys.SpectrumLiveDataV1.ToString(), spectrumLiveDataResultV1, cacheEntryOptions);

                //IEnumerable<SpectrumLiveData> StatsLiveDataAgentWiseResult = null;
                //StatsLiveDataAgentWiseResult = _spectrumLiveDataService.GetStatsLiveDataAgentWise();
                //_cache.Set(CacheKeys.StatsLiveDataAgentWise.ToString(), StatsLiveDataAgentWiseResult, cacheEntryOptions);

                IEnumerable<SpectrumLiveData> spectrumLiveDataResultV2 = null;
                spectrumLiveDataResultV2 = _spectrumLiveDataService.GetSpectrumLiveDataV2();
                _cache.Set(CacheKeys.SpectrumLiveDataV2.ToString(), spectrumLiveDataResultV2, cacheEntryOptions);

                IEnumerable<SpectrumLiveData> StatsLiveDataAgentWiseResultVSTeamV2 = null;
                StatsLiveDataAgentWiseResultVSTeamV2 = _spectrumLiveDataService.GetStatsLiveDataAgentWiseVSTeam_selV2();
                _cache.Set(CacheKeys.StratusVSTeam.ToString(), StatsLiveDataAgentWiseResultVSTeamV2, cacheEntryOptions);

                IEnumerable<SpectrumLiveData> StatsLiveDataAgentWiseResultV2 = null;
                StatsLiveDataAgentWiseResultV2 = _spectrumLiveDataService.GetStatsLiveDataAgentWiseV2();
                _cache.Set(CacheKeys.StatsLiveDataAgentWiseV2.ToString(), StatsLiveDataAgentWiseResultV2, cacheEntryOptions);

                IEnumerable<AgencyAgentLogLive> StatsLiveDataAgentLogResultV2 = null;
                StatsLiveDataAgentLogResultV2 = _spectrumLiveDataService.GetSpectrumLiveAgentLogV2();
                _cache.Set(CacheKeys.StatsLiveDataAgentLogV2.ToString(), StatsLiveDataAgentLogResultV2, cacheEntryOptions);

                // CA location
                IEnumerable<SpectrumLiveData> spectrumLiveDataResultcaV2 = null;
                spectrumLiveDataResultcaV2 = _spectrumLiveDataService.GetSpectrumLiveDataCAV2();
                _cache.Set(CacheKeys.SpectrumLiveDataCAV2.ToString(), spectrumLiveDataResultcaV2, cacheEntryOptions);

                IEnumerable<SpectrumLiveData> StatsLiveDataAgentWiseResultCaV2 = null;
                StatsLiveDataAgentWiseResultCaV2 = _spectrumLiveDataService.GetStatsLiveDataAgentWiseCAV2();
                _cache.Set(CacheKeys.StatsLiveDataAgentWiseCAV2.ToString(), StatsLiveDataAgentWiseResultCaV2, cacheEntryOptions);

                IEnumerable<AgencyAgentLogLive> StatsLiveDataAgentLogResultCAV2 = null;
                StatsLiveDataAgentLogResultCAV2 = _spectrumLiveDataService.GetSpectrumLiveAgentLogCAV2();
                _cache.Set(CacheKeys.StatsLiveDataAgentLogCAV2.ToString(), StatsLiveDataAgentLogResultCAV2, cacheEntryOptions);


                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.SpectrumLiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
                response = Ok("LiveSpectrumCache data app cache Success");
            }
            catch (Exception ex)
            {
                
                response = StatusCode(StatusCodes.Status500InternalServerError, "LiveSpectrumCache"+System.Environment.NewLine+ ex);
            }

            return response;
        }
    }
}
