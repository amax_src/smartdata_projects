﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AmaxIns.Web.API.Controllers.AlpaBolQuotes
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AlpaBolQuotesController : ControllerBase
    {
        protected readonly IAlpaBolQuotesService _IBolQuoteService;
        protected readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        public AlpaBolQuotesController( IAlpaBolQuotesService IAlpaBolQuoteService, ILogger logger, IHostingEnvironment hostingEnvironment)
        {
            _IBolQuoteService = IAlpaBolQuoteService;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/AlpaBolQuotes/AlpaBolCarrierName")]
        public async Task<IActionResult> AlpaBolCarrierName()
        {
            return Ok(_IBolQuoteService.GetAlpaCarrierName());
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/AlpaBolQuotes/GetAlpaDataBolData_Quotes")]
        public async Task<IActionResult> GetAlpaDataBolData_Quotes([FromBody] BolPayLoad data)
        {
            List<BOLQuoteData> _result = new List<BOLQuoteData>();
            string state = string.Empty;
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            if (data.state != null && data.state.Any())
            {
                state = string.Join(",", data.state);
            }
            IEnumerable<BOLQuoteData> _BOLQuoteData = await _IBolQuoteService.GetAlpaDataBolData_Quotes(data.date, state);

            if (data.carriers != null && data.carriers.Count > 0)
            {
                foreach (var x in _BOLQuoteData)
                {
                    if (data.carriers.Any(z => z == AlpaCarrierName.commonwealthgeneral.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("commonwealth"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == AlpaCarrierName.americanaccess.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("american") && x.CarrierName.ToLower().Contains("access"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == AlpaCarrierName.seaharbor.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("seaharbor"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == AlpaCarrierName.unitedauto.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("united") && x.CarrierName.ToLower().Contains("auto"))
                    {
                        _result.Add(x);
                    }
                }
            }
            else
            {
                _result = _BOLQuoteData.ToList();
            }
            return Ok(_result);
        }
    }
}