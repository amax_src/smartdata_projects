﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Planner;
using AmaxIns.ServiceContract.Planner;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Planner
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PlannerController : BaseController
    {
        protected readonly IPlannerService _plannerService;

        public PlannerController(IPlannerService plannerService) : base(plannerService)
        {
            _plannerService = plannerService;
        }

        [HttpGet]
        [Route("/api/Planner/GetAllActivity")]
        public async Task<IActionResult> GetAllActivity()
        {
            var result = await _plannerService.GetAllActivity();
            return Ok(result);
        }
        
        [HttpGet]
        [Route("/api/Planner/GetAllHours")]
        public async Task<IActionResult> GetAllHours()
        {
            var result = await _plannerService.GetAllHours();
            return Ok(result);
        }

        [HttpPost]
        [Route("/api/Planner/SaveUpdatePlanner")]
        public async Task<IActionResult> SaveUpdatePlanner([FromBody] List<PlannerBudget> listPlannerBudget)
        {
            var result = await _plannerService.SaveUpdatePlanner(listPlannerBudget);
            return Ok(result);
        }

        [HttpGet]
        [Route("/api/Planner/DeleteBudgetPlanner/{plannerBudgetId}")]
        public async Task<IActionResult> DeleteBudgetPlanner(int plannerBudgetId)
        {
            var result = await _plannerService.DeleteBudgetPlanner(plannerBudgetId);
            return Ok(result);
        }

        [HttpGet]
        [Route("/api/Planner/GetAllPlanner/{agencyId}/{monthName}")]
        public async Task<IActionResult> GetAllPlanner(int agencyId,string monthName)
        {
            var result = await _plannerService.GetAllPlanner(agencyId,monthName);
            return Ok(result);
        }
        [HttpGet]
        [Route("/api/Planner/GetActualPlanner/{agencyId}/{monthName}/{year}")]
        public async Task<IActionResult> GetActualPlanner(int agencyId, string monthName,string year)
        {
            var result = await _plannerService.GetActualPlanner(agencyId, monthName, year);
            return Ok(result);
        }
        [HttpGet]
        [Route("/api/Planner/GetSingelActual/{plannerBudgetId}")]
        public async Task<IActionResult> GetSingelActual(int plannerBudgetId)
        {
            var result = await _plannerService.GetSingelActual(plannerBudgetId);
            return Ok(result);
        }

        [HttpPost]
        [Route("/api/Planner/SaveUpdateActual")]
        public async Task<IActionResult> SaveUpdateActual(PlannerActualDetails plannerActualDetails)
        {
            var result = await _plannerService.SaveUpdateActual(plannerActualDetails);
            return Ok(result);
           
        }

        [HttpGet]
        [Route("/api/Planner/GetDashboardReport/{monthName}/{year}")]
        public async Task<IActionResult> GetDashboardReport(string monthName,string year)
        {
            var result = await _plannerService.GetDashboardReport(monthName,year);
            return Ok(result);

        }
        [HttpGet]
        [Route("/api/Planner/GetDailyViewReport/{monthName}/{agencyId}/{year}")]
        public async Task<IActionResult> GetDailyViewReport(string monthName, int agencyId,string year)
        {
            var result = await _plannerService.GetDailyViewReport(monthName, agencyId, year);
            return Ok(result);

        }
        //GetDataForWeekly

        [HttpPost]
        [Route("/api/Planner/GetDataForWeekly")]
        public async Task<IActionResult> GetDataForWeekly(QueryPlanner queryPlanner)
        {
            var result = await _plannerService.GetDataForWeekly(queryPlanner);
            return Ok(result);

        }

        [HttpPost]
        [Route("/api/Planner/GetPlannerActivityTotal/{monthName}/{year}")]
        public async Task<IActionResult> GetPlannerActivityTotal(string monthName,string year, [FromBody] List<int> agencyId)
        {
            var result = await _plannerService.GetPlannerActivityTotal(monthName, year, agencyId);
            return Ok(result);

        }
        [HttpGet]
        [Route("/api/Planner/GetPlannerActivityAgencyWise/{monthName}/{year}")]
        public async Task<IActionResult> GetPlannerActivityAgencyWise(string monthName,string year)
        {
            var result = await _plannerService.GetPlannerActivityAgencyWise(monthName,year);
            return Ok(result);

        }
    }
}