﻿using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.PayRoll;
using AmaxIns.DataContract.User;
using AmaxIns.ServiceContract.Payroll;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Web.API.Controllers.Payroll
{
    /// <summary>
    /// Controller to work with Payroll related APIs
    /// </summary>    
    [Produces("application/json")]
    public class PayrollController : BaseController
    {
        protected readonly IPayrollService _payrollService;
        private IMemoryCache _cache;

        public PayrollController(IPayrollService payrollService, IMemoryCache memoryCache) : base(payrollService)
        {
            _payrollService = payrollService;
            _cache = memoryCache;
        }

        /// <summary>
        /// This API is used to do a healthcheck on the Payroll Controller.
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        // GET: api/payroll/healthcheck/
        [HttpGet]
        [Route("/api/payroll/healthcheck")]
        public async Task<IActionResult> HealthCheckAsync()
        {
            var result = await _payrollService.HealthCheckAsync();
            var response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/payroll/users")]
        public async Task<IActionResult> GetUserListAsync()
        {
            IActionResult response = null;

            IEnumerable<UserModel> userList = await _payrollService.GetUserListAsync();

            response = Ok(userList);

            return response;
        }

        [HttpGet]
        [Authorize]
        [Route("/api/payroll/GetPayRoleBudgetedActualData")]
        public async Task<IActionResult> GetPayRoleBudgetedActualData([FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            CombinePayRollModel Payrollresult = new CombinePayRollModel();
            CombinePayRollModel Payrollresultdata = new CombinePayRollModel();
            if (!_cache.TryGetValue(CacheKeys.PayRoleBudgetedActualData.ToString(), out Payrollresult))
            {
                Payrollresult = await _payrollService.GetPayRoleActualAndBudgetedData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.PayRoleBudgetedActualData.ToString(), Payrollresult, cacheEntryOptions);
            }


            Payrollresultdata.Actual = Payrollresult.Actual.Where(m => m.Months == month && m.Year.ToString() == year);
            Payrollresultdata.Budgted = Payrollresult.Budgted.Where(m => m.Months == month && m.Year.ToString() == year);
            IActionResult response = null;
            var Result = Payrollresultdata;//await _payrollService.GetPayRoleActualAndBudgetedData();
            response = Ok(Result);
            return response;
        }


        [HttpPost]
        [Authorize]
        [Route("/api/payroll/GetPayrollTiles")]
        public async Task<IActionResult> GetPayrollTiles([FromBody] List<int> agencyId, [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            decimal OvertimeperPolicy = 0;
            decimal Overtimehoursperpolicy = 0;
            decimal PayrollperPolicy = 0;
            decimal Payrollpertransaction = 0;
            ConsolidatedTileData result = new ConsolidatedTileData();
            if (!_cache.TryGetValue(CacheKeys.PayrollTiles.ToString(), out result))
            {
                result = await _payrollService.GetPayRoleTilesConsolidatedData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.PayrollTiles.ToString(), result, cacheEntryOptions);
            }


            var OverTimePerPolicyresult = result.PayrollAndOverTimePerPolicy.Where(m => m.ActMonth == month && m.ActYear.ToString() == year);
            var PayrollperPolicyresult = result.PayrollPerPolicy.Where(m => m.ActMonth == month && m.ActYear.ToString() == year);
            var transPerPolicy = result.TransPerPolicy.Where(m => m.ActMonth == month && m.ActYear.ToString() == year);
            decimal sumofDbtAmount = 0;
            decimal sumofDbtHour = 0;
            int sumofpolicyCount = 0;


            decimal sumofDbtAmountTotal = 0;

            int sumofpolicyCountTotal = 0;


            decimal sumofDbtAmountTrans = 0;

            decimal sumofTrans = 0;

            foreach (var x in agencyId)
            {
                var otdata = OverTimePerPolicyresult.Where(m => m.AgencyID == x);
                sumofDbtAmount = sumofDbtAmount + otdata.Sum(m => m.DebitAmount);
                sumofDbtHour = sumofDbtHour + otdata.Sum(m => m.DebitHours);
                sumofpolicyCount = sumofpolicyCount + otdata.Sum(m => m.Actnewbusinesscount);


                var PayrollperPolicyData = PayrollperPolicyresult.Where(m => m.AgencyID == x);
                sumofDbtAmountTotal = sumofDbtAmountTotal + PayrollperPolicyData.Sum(m => m.DebitAmount);
                sumofpolicyCountTotal = sumofpolicyCountTotal + PayrollperPolicyData.Sum(m => m.Actnewbusinesscount);

                var TransperPolicyData = transPerPolicy.Where(m => m.AgencyID == x);
                sumofDbtAmountTrans = sumofDbtAmountTrans + TransperPolicyData.Sum(m => m.DebitAmount);
                sumofTrans = sumofTrans + TransperPolicyData.Sum(m => m.Transactions);
            }

            if (sumofpolicyCount > 0)
            {
                OvertimeperPolicy = sumofDbtAmount / sumofpolicyCount;
                Overtimehoursperpolicy = sumofDbtHour / sumofpolicyCount;

            }

            if (sumofpolicyCountTotal > 0)
            {
                PayrollperPolicy = sumofDbtAmountTotal / sumofpolicyCountTotal;
            }
            if (sumofTrans > 0)
            {
                Payrollpertransaction = sumofDbtAmountTrans / sumofTrans;
            }
            IActionResult response = null;

            var Result = new PayrollTileResult { Overtimehoursperpolicy = Overtimehoursperpolicy, OvertimeperPolicy = OvertimeperPolicy, PayrollperPolicy = PayrollperPolicy, Payrollpertransaction = Payrollpertransaction };

            response = Ok(Result);

            return response;
        }

    }
}