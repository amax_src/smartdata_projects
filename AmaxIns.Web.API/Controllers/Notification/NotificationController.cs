﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.ServiceContract.Notification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Notification
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : BaseController
    {
        INotificationService _Service;
        public NotificationController(INotificationService Service) : base(Service)
        {
            _Service = Service;
        }
        public async Task<IActionResult> Get()
        {
            var result = _Service.GetNotification();
            var response = Ok(result);
            return response;
        }
    }
}