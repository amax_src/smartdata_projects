﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Agency;
using AmaxIns.DataContract.CommanModel;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.LiveData;
using AmaxIns.DataContract.LiveData.BindOnlineQuoteLiveData;
using AmaxIns.DataContract.LiveData.DidConsolatedData;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.LiveDataApp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace AmaxIns.Web.API.Controllers.Livedata
{
    [Produces("application/json")]
    // [Authorize]
    public class LiveDataController : ControllerBase
    {
        protected readonly ILiveDataAppService service;
        private IMemoryCache _cache;
        protected readonly IAgencyService _agencyService;
        protected readonly IDidService _didservice;
        protected readonly ISpectrumLiveDataService _spectrumLiveDataService;
        protected readonly IBolQuoteService _IBolQuoteService;
        protected readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        public LiveDataController(ILiveDataAppService _service, IMemoryCache memoryCache, IAgencyService agencyService, IDidService didservice
            , ISpectrumLiveDataService spectrumLiveDataService, IBolQuoteService IBolQuoteService, ILogger logger, IHostingEnvironment hostingEnvironment)
        {
            service = _service;
            _cache = memoryCache;
            _agencyService = agencyService;
            _didservice = didservice;
            _spectrumLiveDataService = spectrumLiveDataService;
            _IBolQuoteService = IBolQuoteService;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Route("/api/Livedata")]
        public async Task<IActionResult> Get(string login)
        {
            List<LiveDataAgency> _livedata = null;
            List<LiveDataAgency> _livedataResult = null;
            List<AgentCountModel> _AmaxHourlyData;
            List<AgentCountModel> _AmaxHourlyDataAgencywise;
            if (!_cache.TryGetValue(CacheKeys.LiveData.ToString(), out _livedata))
            {
                _livedata = service.GetData(out _AmaxHourlyData, out _AmaxHourlyDataAgencywise);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.AmaxLiveData.ToString(), _AmaxHourlyData, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveDataAgencyWise.ToString(), _AmaxHourlyDataAgencywise, cacheEntryOptions);
                _cache.Set(CacheKeys.LiveData.ToString(), _livedata, cacheEntryOptions);
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.LiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
            }
            if (login == "undefined-undefined" || login == null)
            {
                _livedataResult = _livedata;
            }
            else
            {
                string[] details = login.Split('-');
                if (details[0] == "-1")
                {
                    _livedataResult = _livedata;
                }
                else
                {
                    List<int> user = new List<int>();
                    user.Add(Convert.ToInt32(details[0]));
                    _livedataResult = new List<LiveDataAgency>();
                    if (details[1].ToLower().Replace(" ", string.Empty) == Role.headofdepratment.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefexecutiveofficer.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chieffinancialofficer.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefOperatingofficer.ToString())
                    {
                        _livedataResult = _livedata;
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.saledirector.ToString())
                    {

                        var agency = _agencyService.GetSaleOfDirector(user).Result;
                        foreach (var x in agency)
                        {
                            List<LiveDataAgency> temp = new List<LiveDataAgency>();
                            temp = _livedata.Where(m => m.AgencyName == x.AgencyName).ToList();
                            foreach (var z in temp)
                            {
                                _livedataResult.Add(z);
                            }
                        }
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.regionalmanager.ToString())
                    {

                        var agency = _agencyService.Get(user).Result;
                        foreach (var x in agency)
                        {
                            List<LiveDataAgency> temp = new List<LiveDataAgency>();
                            temp = _livedata.Where(m => m.AgencyName == x.AgencyName).ToList();
                            foreach (var z in temp)
                            {
                                _livedataResult.Add(z);
                            }
                        }
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.zonalmanager.ToString())
                    {
                        var agency = _agencyService.Get(null, user).Result;
                        foreach (var x in agency)
                        {
                            List<LiveDataAgency> temp = new List<LiveDataAgency>();
                            temp = _livedata.Where(m => m.AgencyName == x.AgencyName).ToList();
                            foreach (var z in temp)
                            {
                                _livedataResult.Add(z);
                            }
                        }
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.bsm.ToString())
                    {
                        var agency = _agencyService.GetBSMAgency(user).Result;
                        foreach (var x in agency)
                        {
                            List<LiveDataAgency> temp = new List<LiveDataAgency>();
                            temp = _livedata.Where(m => m.AgencyName == x.AgencyName).ToList();
                            foreach (var z in temp)
                            {
                                _livedataResult.Add(z);
                            }
                        }
                    }
                    else if(details[1].ToLower().Replace(" ", string.Empty) == Role.storemanager.ToString())
                    {
                        var agency = _agencyService.GetStoreManagerAgency(user.FirstOrDefault()).Result;
                        foreach (var x in agency)
                        {
                            List<LiveDataAgency> temp = new List<LiveDataAgency>();
                            temp = _livedata.Where(m => m.AgencyName == x.AgencyName).ToList();
                            foreach (var z in temp)
                            {
                                _livedataResult.Add(z);
                            }
                        }
                    }
                }
            }
            //"Head of Depratment"

            var result = _livedataResult;
            return Ok(result);
        }

        [HttpGet]
        [Route("/api/Livedata/GetAmaxLiveApp")]
        public async Task<IActionResult> GetAmaxLiveApp()
        {
            List<AgentCountModel> AmaxHourlyData;
            List<AgentCountModel> AmaxHourlyDataAgencyWise;
            List<LiveDataAgency> _livedata = null;
            if (!_cache.TryGetValue(CacheKeys.AmaxLiveData.ToString(), out AmaxHourlyData))
            {
                _livedata = service.GetData(out AmaxHourlyData, out AmaxHourlyDataAgencyWise);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.LiveData.ToString(), _livedata, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveData.ToString(), AmaxHourlyData, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveDataAgencyWise.ToString(), AmaxHourlyDataAgencyWise, cacheEntryOptions);
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.LiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
            }

            var result = AmaxHourlyData;
            return Ok(result);
        }


        [Route("/api/Livedata/GetAmaxLiveAppAgencyWise")]
        public async Task<IActionResult> GetAmaxLiveAppAgencyWise()
        {
            List<AgentCountModel> AmaxHourlyData;
            List<AgentCountModel> AmaxHourlyDataAgencyWise;
            List<LiveDataAgency> _livedata = null;
            if (!_cache.TryGetValue(CacheKeys.AmaxLiveDataAgencyWise.ToString(), out AmaxHourlyDataAgencyWise))
            {
                _livedata = service.GetData(out AmaxHourlyData, out AmaxHourlyDataAgencyWise);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.LiveData.ToString(), _livedata, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveData.ToString(), AmaxHourlyData, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveDataAgencyWise.ToString(), AmaxHourlyDataAgencyWise, cacheEntryOptions);
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.LiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
            }
            var result = AmaxHourlyDataAgencyWise;
            return Ok(result);
        }

        [HttpGet]
        [Route("/api/Livedata/GetAngular")]
        public async Task<IActionResult> GetAngular()
        {
            List<LiveDataAgency> _livedata = null;
            List<AgentCountModel> AmaxHourlyData;
            List<AgentCountModel> AmaxHourlyAgencyWiseData;
            if (!_cache.TryGetValue(CacheKeys.LiveData.ToString(), out _livedata))
            {
                _livedata = service.GetData(out AmaxHourlyData, out AmaxHourlyAgencyWiseData);
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.LiveData.ToString(), _livedata, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveData.ToString(), AmaxHourlyData, cacheEntryOptions);
                _cache.Set(CacheKeys.AmaxLiveDataAgencyWise, AmaxHourlyAgencyWiseData, cacheEntryOptions);
                TimeZoneInfo targetZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                DateTime newDT = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, targetZone);
                string Refreshresult = newDT.ToShortTimeString();
                _cache.Set(CacheKeys.LiveDataRefreshTime.ToString(), Refreshresult, cacheEntryOptions);
            }

            var result = _livedata;
            return Ok(result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Livedata/GetRefreshTime")]
        public async Task<IActionResult> GetRefreshTime()
        {
            string result = string.Empty;
            _cache.TryGetValue(CacheKeys.LiveDataRefreshTime.ToString(), out result);
            return Ok(result);
        }

        [HttpGet]
        [Route("/api/Livedata/GetConsolidatedDIDData")]
        public IActionResult GetConsolidatedDIDData([FromQuery(Name = "day")]string day, [FromQuery(Name = "month")]string month, [FromQuery(Name = "year")]string year, [FromQuery(Name = "endday")]string endday, [FromQuery(Name = "endmonth")]string endmonth, [FromQuery(Name = "endyear")]string endyear, [FromQuery(Name = "login")]string login)
        {
            ConsolidatedDidData ConsolidatedDIDData;
            var result = service.GetConsolidatedDIDData(day, month, year, endday, endmonth, endyear);
            ConsolidatedDIDData = result;
            //if (login == "undefined-undefined" || login == null)
            //{
            //    ConsolidatedDIDData = result;
            //}
            //else
            //{

            //string[] details = login.Split('-');
            //if (details[0] == "-1")
            //{
            //    ConsolidatedDIDData = result;
            //}
            //else
            //{
            //    List<int> user = new List<int>();
            //    user.Add(Convert.ToInt32(details[0]));
            //    ConsolidatedDIDData = new List<ConsolidatedDidData>();
            //    if (details[1].ToLower().Replace(" ", string.Empty) == Role.headofdepratment.ToString())
            //    {
            //        ConsolidatedDIDData = result;
            //    }
            //    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.regionalmanager.ToString())
            //    {

            //        var agency = _agencyService.Get(user).Result;
            //        foreach (var x in agency)
            //        {
            //            List<ConsolidatedDidData> temp = new List<ConsolidatedDidData>();
            //            temp = result.Where(m => m.AgencyName == x.AgencyName).ToList();
            //            foreach (var z in temp)
            //            {
            //                ConsolidatedDIDData.Add(z);
            //            }
            //        }
            //    }
            //    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.zonalmanager.ToString())
            //    {
            //        var agency = _agencyService.Get(null, user).Result;
            //        foreach (var x in agency)
            //        {
            //            List<ConsolidatedDidData> temp = new List<ConsolidatedDidData>();
            //            temp = result.Where(m => m.AgencyName == x.AgencyName).ToList();
            //            foreach (var z in temp)
            //            {
            //                ConsolidatedDIDData.Add(z);
            //            }
            //        }
            //    }
            //    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.bsm.ToString())
            //    {
            //        var agency = _agencyService.GetBSMAgency(user).Result;
            //        foreach (var x in agency)
            //        {
            //            List<ConsolidatedDidData> temp = new List<ConsolidatedDidData>();
            //            temp = result.Where(m => m.AgencyName == x.AgencyName).ToList();
            //            foreach (var z in temp)
            //            {
            //                ConsolidatedDIDData.Add(z);
            //            }
            //        }
            //    }
            //}
            //}

            return Ok(ConsolidatedDIDData);
        }

        [HttpGet]
        [Route("/api/Livedata/GetSummaryDIDData")]
        public IActionResult GetSummaryDIDData([FromQuery(Name = "sday")]string sday, [FromQuery(Name = "smonth")]string smonth, [FromQuery(Name = "syear")]string syear, [FromQuery(Name = "eday")]string eday, [FromQuery(Name = "emonth")]string emonth, [FromQuery(Name = "eyear")]string eyear, [FromQuery(Name = "login")]string login)
        {
            List<DidNumberData> didNumberData = new List<DidNumberData>();
            var result = _didservice.GetSummaryDIDData(sday, smonth, syear, eday, emonth, eyear);
            didNumberData = result;
            return Ok(didNumberData);
        }

        [HttpGet]
        [Route("/api/Livedata/GetSpectrumLiveData")]
        public async Task<IActionResult> GetSpectrumLiveData([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            if (!_cache.TryGetValue(CacheKeys.SpectrumLiveData.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumLiveData.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        //[HttpGet]
        //[Route("/api/Livedata/GetSpectrumLiveDataV1")]
        //public async Task<IActionResult> GetSpectrumLiveDataV1([FromQuery(Name = "login")]string login)
        //{

        //    IActionResult response = null;
        //    IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
        //    if (!_cache.TryGetValue(CacheKeys.SpectrumLiveDataV1.ToString(), out spectrumLiveDataResult))
        //    {
        //        spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveDataV1();
        //        var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
        //        _cache.Set(CacheKeys.SpectrumLiveDataV1.ToString(), spectrumLiveDataResult, cacheEntryOptions);
        //    }
        //    var result = spectrumLiveDataResult;
        //    response = Ok(result);
        //    return response;
        //}

        //[HttpGet]
        //[Route("/api/Livedata/GetStatsLiveDataAgentWise")]
        //public async Task<IActionResult> GetStatsLiveDataAgentWise([FromQuery(Name = "login")]string login)
        //{

        //    IActionResult response = null;
        //    IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
        //    if (!_cache.TryGetValue(CacheKeys.StatsLiveDataAgentWise.ToString(), out spectrumLiveDataResult))
        //    {
        //        spectrumLiveDataResult = _spectrumLiveDataService.GetStatsLiveDataAgentWise();
        //        var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
        //        _cache.Set(CacheKeys.StatsLiveDataAgentWise.ToString(), spectrumLiveDataResult, cacheEntryOptions);
        //    }
        //    var result = spectrumLiveDataResult;
        //    response = Ok(result);
        //    return response;
        //}

        //[HttpGet]
        //[Route("/api/Livedata/GetStatsWeeklyDataAgentWise")]
        //public async Task<IActionResult> GetStatsWeeklyDataAgentWise([FromQuery(Name = "login")]string login, [FromQuery(Name = "weeklyId")]string weeklyId)
        //{

        //    IActionResult response = null;
        //    IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
        //    spectrumLiveDataResult = _spectrumLiveDataService.GetStatsWeeklyDataAgentWise(Convert.ToInt32(weeklyId));
        //    var result = spectrumLiveDataResult;
        //    response = Ok(result);
        //    return response;
        //}


        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Livedata/SpectrumLiveDataRefreshTime")]
        public async Task<IActionResult> SpectrumLiveDataRefreshTime()
        {
            string result = string.Empty;
            _cache.TryGetValue(CacheKeys.SpectrumLiveDataRefreshTime.ToString(), out result);
            return Ok(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/Livedata/BolQuotes")]
        public async Task<IActionResult> BolQuotes([FromBody] BolPayLoad data )
        {
            List<BOLQuoteData> _result = new List<BOLQuoteData>();
            string state = string.Empty;
            if(data.date==null )
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            if(data.state!=null && data.state.Any())
            {
                state= string.Join(",", data.state);
            }
            IEnumerable<BOLQuoteData> _BOLQuoteData = await _IBolQuoteService.GetDataBolData(data.date, state);

            if (data.carriers != null && data.carriers.Count > 0)
            {
                foreach (var x in _BOLQuoteData)
                {

                    if (data.carriers.Any(z => z == CarrierName.alinsco.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "alinsco enhanced"|| x.CarrierName.ToLower()== "alinsco"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.assuranceamerica.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "assuranceamerica"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.commonwealthgeneral.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "commonwealth ceneral rtr" || x.CarrierName.ToLower()== "commonwealthgeneral"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.americanaccess.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "american access" || x.CarrierName.ToLower()== "americanaccess"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.seaharbor.ToString()) && x.CarrierName != null 
                        && x.CarrierName.ToLower()== "seaharbor")
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.unitedauto.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "united auto limited 6 month" || x.CarrierName.ToLower()== "unitedauto"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.lamargeneralagency.ToString()) && x.CarrierName != null 
                        && (x.CarrierName.ToLower()== "lamar general agency" || x.CarrierName.ToLower()== "lamargeneralagency"))
                    {
                        _result.Add(x);
                    }
                }
            }
            else
            {
                _result = _BOLQuoteData.ToList();
            }
            return Ok(_result);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Livedata/BolCarrierName")]
        public async Task<IActionResult> BolCarrierName()
        {
            return Ok(_IBolQuoteService.GetCarrierName());
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/api/Livedata/GetSingleQuote")]
        public async Task<IActionResult> GetSingleQuote([FromQuery(Name = "quoteNumber")] string quoteNumber)
        {
            BOLQuoteData quoteData = await _IBolQuoteService.GetSingleQuote(quoteNumber);
            return Ok(quoteData);
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("/api/Livedata/GetDataBolData_Quotes")]
        public async Task<IActionResult> GetDataBolData_Quotes([FromBody] BolPayLoad data)
        {
            List<BOLQuoteData> _result = new List<BOLQuoteData>();
            string state = string.Empty;
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            if (data.state != null && data.state.Any())
            {
                state = string.Join(",", data.state);
            }
            IEnumerable<BOLQuoteData> _BOLQuoteData = await _IBolQuoteService.GetDataBolData_Quotes(data.date, state);

            if (data.carriers != null && data.carriers.Count > 0)
            {
                foreach (var x in _BOLQuoteData)
                {

                    if (data.carriers.Any(z => z == CarrierName.alinsco.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains(CarrierName.alinsco.ToString()))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.assuranceamerica.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("assurance") && x.CarrierName.ToLower().Contains("america"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.commonwealthgeneral.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("commonwealth"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.americanaccess.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("american") && x.CarrierName.ToLower().Contains("access"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.seaharbor.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("seaharbor"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.unitedauto.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("united") && x.CarrierName.ToLower().Contains("auto"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.stonegate.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("stonegate"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.evolution.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("evolution"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.bristolwest.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("bristol") && x.CarrierName.ToLower().Contains("west"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.unique.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("unique"))
                    {
                        _result.Add(x);
                    }
                    if (data.carriers.Any(z => z == CarrierName.lamargeneralagency.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("lamar") && x.CarrierName.ToLower().Contains("general") && x.CarrierName.ToLower().Contains("agency"))
                    {
                        _result.Add(x);
                    }
                    else if (data.carriers.Any(z => z == CarrierName.lamargeneralagency.ToString()) && x.CarrierName != null && x.CarrierName.ToLower().Contains("lamar"))
                    {
                        _result.Add(x);
                    }
                }
            }
            else
            {
                _result = _BOLQuoteData.ToList();
            }
            return Ok(_result);
        }

        [HttpPost]
        [Route("/api/Livedata/GetEODLiveReport")]
        public async Task<IActionResult> GetEODLiveReport([FromBody] BolPayLoad data)
        {
            List<EODLive> _result = new List<EODLive>();
            string state = string.Empty;
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            IEnumerable<EODLive> _BOLQuoteData = await _IBolQuoteService.GetEODLiveReport(data.date);
            _result = _BOLQuoteData.ToList();
            return Ok(_result);
        }

        [HttpGet]
        [Route("/api/Livedata/GetAgency")]
        public async Task<IActionResult> GetAgency([FromQuery(Name = "login")] string login)
        {

            IEnumerable<AgencyModel> _livedataResult = null;

            if (login == "undefined-undefined" || login == null)
            {
                _livedataResult = _agencyService.Get().Result;
            }
            else
            {
                string[] details = login.Split('-');
                List<int> user = new List<int>();
                user.Add(Convert.ToInt32(details[0]));

                if (details[1].ToLower().Replace(" ", string.Empty) == Role.headofdepratment.ToString()
                    || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefexecutiveofficer.ToString()
                    || details[1].ToLower().Replace(" ", string.Empty) == Role.chieffinancialofficer.ToString()
                    || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefOperatingofficer.ToString())
                {
                    _livedataResult = _agencyService.Get().Result;
                }
                else if (details[1].ToLower().Replace(" ", string.Empty) == Role.saledirector.ToString())
                {
                    _livedataResult = _agencyService.GetSaleOfDirector(user).Result;
                }
                else if (details[1].ToLower().Replace(" ", string.Empty) == Role.regionalmanager.ToString())
                {

                    _livedataResult = _agencyService.Get(user).Result;
                }
                else if (details[1].ToLower().Replace(" ", string.Empty) == Role.zonalmanager.ToString())
                {
                    _livedataResult = _agencyService.Get(null, user).Result;

                }
                else if (details[1].ToLower().Replace(" ", string.Empty) == Role.bsm.ToString())
                {
                    _livedataResult = _agencyService.GetBSMAgency(user).Result;

                }
                else if (details[1].ToLower().Replace(" ", string.Empty) == Role.storemanager.ToString())
                {
                    _livedataResult = _agencyService.GetStoreManagerAgency(user.FirstOrDefault()).Result;

                }
            }
            var result = _livedataResult;
            return Ok(result);
        }

        [Route("/api/Livedata/ExportHourlyData")]
        [HttpPost]
        public async Task<IActionResult> ExportHourlyData([FromBody] IEnumerable<AgentCountModel> list)
        {
            var result = await Task.Run(() =>
             {
                 string root = _hostingEnvironment.WebRootPath + "\\Export";
                 if (!Directory.Exists(root))
                 {
                     Directory.CreateDirectory(root);
                 }
                 string FileName = _IBolQuoteService.AmaxHourlyData(list, root + "\\");
                 var response = Ok(FileName);
                 return response;
             });
            return result;
        }
        [Route("/api/Livedata/ExportHourlyDataAgencyWise")]
        [HttpPost]
        public async Task<IActionResult> ExportHourlyDataAgencyWise([FromBody] IEnumerable<AgentCountModel> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _IBolQuoteService.AmaxHourlyDataAgencyWise(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/Livedata/GetCustomerJourneyInfoReport")]
        public async Task<IActionResult> GetCustomerJourneyInfoReport([FromBody] BolPayLoad data)
        {
            List<CustomerJourneyInfo> _result = new List<CustomerJourneyInfo>();
            string state = string.Empty;
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            IEnumerable<CustomerJourneyInfo> BOLQuoteData = await _IBolQuoteService.GetCustomerJourneyInfoReport(data.date);
            _result = BOLQuoteData.ToList();
            return Ok(_result);
        }

        [Route("/api/Livedata/ExportCustomerJourneyInfoReport")]
        [HttpPost]
        public async Task<IActionResult> ExportCustomerJourneyInfoReport([FromBody] BolPayLoad data)
        {
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _IBolQuoteService.ExportCustomerJourneyInfo(data.date, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/Livedata/GetRetailCustomerJourneyInfoReport")]
        public async Task<IActionResult> GetRetailCustomerJourneyInfoReport([FromBody] BolPayLoad data)
        {
            List<RetailCustomerJourneyInfo> _result = new List<RetailCustomerJourneyInfo>();
            string state = string.Empty;
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            IEnumerable<RetailCustomerJourneyInfo> BOLQuoteData = await _IBolQuoteService.GetRetailCustomerJourneyInfoReport(data.date);
            _result = BOLQuoteData.ToList();
            return Ok(_result);
        }

        [Route("/api/Livedata/ExportOnlineQuote")]
        [HttpPost]
        public async Task<IActionResult> ExportOnlineQuote([FromBody] IEnumerable<ExportBOLModel> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _IBolQuoteService.ExportOnlineQuote(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }



        #region SpectrumV2
        [HttpGet]
        [Route("/api/Livedata/GetSpectrumLiveDataV2")]
        public async Task<IActionResult> GetSpectrumLiveDataV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            List<SpectrumLiveData> _spectrumLiveDataResult = new List<SpectrumLiveData>();
            if (!_cache.TryGetValue(CacheKeys.SpectrumLiveDataV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveDataV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumLiveDataV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }
            IEnumerable<AgencyModel> agencyList = FilterAgency(login);
            if (agencyList.Any())
            {
                SpectrumLiveData _sd = new SpectrumLiveData();
                foreach (AgencyModel x in agencyList)
                {
                    _sd = new SpectrumLiveData();
                    _sd.Agencyid = x.AgencyId;
                    _sd.Location = x.AgencyName;
                    var dataValues = spectrumLiveDataResult.Where(m => m.Agencyid == x.AgencyId).FirstOrDefault();
                    if (dataValues != null)
                    {
                        _sd.InBoundCalls = dataValues.InBoundCalls;
                        _sd.OutboundCalls = dataValues.OutboundCalls;
                        _sd.totalInBoundsCalls = dataValues.totalInBoundsCalls;
                        _sd.totalOutBoundsCall = dataValues.totalOutBoundsCall;
                        _sd.MissedCalls = dataValues.MissedCalls;
                        _sd.CallTransfer = dataValues.CallTransfer;
                        _sd.TransferCallDuration = dataValues.TransferCallDuration;
                        _sd.Date = dataValues.Date;
                    }
                    _spectrumLiveDataResult.Add(_sd);
                }

            }

            if (_spectrumLiveDataResult.Any())
            {
                var result = _spectrumLiveDataResult;
                response = Ok(result);
            }
            else if(string.IsNullOrEmpty(login))
            {
                var result = spectrumLiveDataResult;
                response = Ok(result);
            }
            return response;
        }
        private IEnumerable<AgencyModel> FilterAgency(string login)
        {
            IEnumerable<AgencyModel> agencyList = null;
            if (login == "undefined-undefined" || login == null)
            {
                agencyList = _agencyService.Get().Result;
            }
            else
            {
                string[] details = login.Split('-');
                if (details[0] == "-1")
                {
                    agencyList = _agencyService.Get().Result;
                }
                else
                {
                    List<int> user = new List<int>();
                    user.Add(Convert.ToInt32(details[0]));
                    if (details[1].ToLower().Replace(" ", string.Empty) == Role.headofdepratment.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefexecutiveofficer.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chieffinancialofficer.ToString()
                   || details[1].ToLower().Replace(" ", string.Empty) == Role.chiefOperatingofficer.ToString())
                    {
                        agencyList = _agencyService.Get().Result;
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.saledirector.ToString())
                    {
                        agencyList = _agencyService.GetSaleOfDirector(user).Result;
                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.regionalmanager.ToString())
                    {

                        agencyList = _agencyService.Get(user).Result;

                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.zonalmanager.ToString())
                    {
                        agencyList = _agencyService.Get(null, user).Result;

                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.bsm.ToString())
                    {
                        agencyList = _agencyService.GetBSMAgency(user).Result;

                    }
                    else if (details[1].ToLower().Replace(" ", string.Empty) == Role.storemanager.ToString())
                    {
                        agencyList = _agencyService.GetStoreManagerAgency(user.FirstOrDefault()).Result;
                    }
                }
            }
            return agencyList;
        }
        [HttpGet]
        [Route("/api/Livedata/GetStatsLiveDataAgentWiseV2")]
        public async Task<IActionResult> GetStatsLiveDataAgentWiseV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            List<SpectrumLiveData> _spectrumLiveDataResult = new List<SpectrumLiveData>();
            if (!_cache.TryGetValue(CacheKeys.StatsLiveDataAgentWiseV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetStatsLiveDataAgentWiseV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.StatsLiveDataAgentWiseV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }

            IEnumerable<AgencyModel> agencyList = FilterAgency(login);
            if (agencyList.Any())
            {
                foreach (AgencyModel x in agencyList)
                {
                    var dataValues = spectrumLiveDataResult.Where(m => m.Agencyid == x.AgencyId).ToList();
                    foreach (SpectrumLiveData obj in dataValues)
                    {
                        _spectrumLiveDataResult.Add(obj);
                    }
                }
            }

            if (_spectrumLiveDataResult.Any())
            {
                var result = _spectrumLiveDataResult;
                response = Ok(result);
            }
            else if (string.IsNullOrEmpty(login))
            {
                var result = spectrumLiveDataResult;
                response = Ok(result);
            }
            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetSpectrumLiveAgentLogV2")]
        public async Task<IActionResult> GetSpectrumLiveAgentLogV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<AgencyAgentLogLive> spectrumLiveDataResult = null;
            List<AgencyAgentLogLive> _spectrumLiveDataResult = new List<AgencyAgentLogLive>();
            if (!_cache.TryGetValue(CacheKeys.StatsLiveDataAgentLogV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveAgentLogV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.StatsLiveDataAgentLogV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }

            IEnumerable<AgencyModel> agencyList = FilterAgency(login);
            if (agencyList.Any())
            {
                foreach (AgencyModel x in agencyList)
                {
                    var dataValues = spectrumLiveDataResult.Where(m => m.AgencyId == x.AgencyId).ToList();
                    foreach (AgencyAgentLogLive obj in dataValues)
                    {
                        _spectrumLiveDataResult.Add(obj);
                    }
                }
            }

            if (_spectrumLiveDataResult.Any())
            {
                var result = _spectrumLiveDataResult;
                response = Ok(result);
            }
            else if (string.IsNullOrEmpty(login))
            {
                var result = spectrumLiveDataResult;
                response = Ok(result);
            }
            return response;
        }
        [HttpPost]
        [Route("/api/Livedata/GetStatsWeeklyDataAgentWiseV2")]
        public async Task<IActionResult> GetStatsWeeklyDataAgentWiseV2([FromBody] SpectrumParam data)
        {
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetStatsWeeklyDataAgentWiseV2(data);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetSpectrumAgentCallDetails")]
        public async Task<IActionResult> GetSpectrumAgentCallDetails([FromQuery(Name = "login")]string login, [FromQuery(Name = "extension")]string extension)
        {

            IActionResult response = null;
            IEnumerable<SpectrumAgentCall> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumAgentCallDetails(extension);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetSpectrumLiveAgentExtensionLogV2")]
        public async Task<IActionResult> GetSpectrumLiveAgentExtensionLogV2([FromQuery(Name = "login")]string login, [FromQuery(Name = "extension")]string extension)
        {

            IActionResult response = null;
            IEnumerable<SpectrumAgentExtensionLog> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveAgentExtensionLogV2(extension);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetCallTransferDetails")]
        public async Task<IActionResult> GetCallTransferDetails([FromQuery(Name = "login")]string login, [FromQuery(Name = "extension")]string extension, [FromQuery(Name = "agencyId")]int agencyid)
        {

            IActionResult response = null;
            IEnumerable<AgentCallTransfer> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetCallTransferDetails(extension, agencyid);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [Route("/api/Livedata/ExportSpectrumCallLocationWise")]
        [HttpPost]
        public async Task<IActionResult> ExportSpectrumCallLocationWise([FromBody] IEnumerable<SpectrumLiveData> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _spectrumLiveDataService.ExportSpectrumCallLocationWise(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }
        [Route("/api/Livedata/ExportSpectrumCallExtensionWise")]
        [HttpPost]
        public async Task<IActionResult> ExportSpectrumCallExtensionWise([FromBody] IEnumerable<SpectrumLiveData> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _spectrumLiveDataService.ExportSpectrumCallExtensionWise(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Livedata/ExportLiveStratusAgentLog")]
        [HttpPost]
        public async Task<IActionResult> ExportLiveStratusAgentLog([FromBody] IEnumerable<AgencyAgentLogLive> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _spectrumLiveDataService.ExportLiveStratusAgentLog(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Livedata/ExportStratusWeeklyDataAgentWise")]
        [HttpPost]
        public async Task<IActionResult> ExportStratusWeeklyDataAgentWise([FromBody] IEnumerable<SpectrumLiveData> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _spectrumLiveDataService.ExportStratusWeeklyDataAgentWise(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Livedata/ExportStratusDataAgentExtensionWise")]
        [HttpPost]
        public async Task<IActionResult> ExportStratusDataAgentExtensionWise([FromBody] IEnumerable<SpectrumLiveData> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _spectrumLiveDataService.ExportStratusDataAgentExtensionWise(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }


        [HttpGet]
        [Route("/api/Livedata/GetStatsLiveDataAgentWiseVSTeamV2")]
        public async Task<IActionResult> GetStatsLiveDataAgentWiseVSTeamV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            List<SpectrumLiveData> _spectrumLiveDataResult = new List<SpectrumLiveData>();
            if (!_cache.TryGetValue(CacheKeys.StratusVSTeam.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetStatsLiveDataAgentWiseVSTeam_selV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.StratusVSTeam.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }


            var result = spectrumLiveDataResult;
            response = Ok(result);

            return response;
        }


        [HttpPost]
        [Route("/api/Livedata/GetSpectrumExtensionWiseCallDetailsLogV2")]
        public async Task<IActionResult> GetSpectrumExtensionWiseCallDetailsLogV2([FromBody] StratusExtensionParam data)
        {

            IActionResult response = null;
            IEnumerable<SpectrumAgentCall> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumExtensionWiseCallDetails(data);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }
        #endregion

        #region CA_Spectrum

        [HttpGet]
        [Route("/api/Livedata/GetCASpectrumLiveDataV2")]
        public async Task<IActionResult> GetCASpectrumLiveDataV2([FromQuery(Name = "login")]string login)
        {
            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            List<SpectrumLiveData> _spectrumLiveDataResult = new List<SpectrumLiveData>();
            if (!_cache.TryGetValue(CacheKeys.SpectrumLiveDataCAV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveDataCAV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.SpectrumLiveDataCAV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }
            var result = spectrumLiveDataResult;
            response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetCallTransferDetailsCA")]
        public async Task<IActionResult> GetCallTransferDetailsCA([FromQuery(Name = "login")]string login, [FromQuery(Name = "extension")]string extension, [FromQuery(Name = "agencyId")]int agencyid)
        {

            IActionResult response = null;
            IEnumerable<AgentCallTransfer> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetCallTransferDetailsCA(extension, agencyid);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetStatsLiveDataAgentWiseCAV2")]
        public async Task<IActionResult> GetStatsLiveDataAgentWiseCAV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            List<SpectrumLiveData> _spectrumLiveDataResult = new List<SpectrumLiveData>();
            if (!_cache.TryGetValue(CacheKeys.StatsLiveDataAgentWiseCAV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetStatsLiveDataAgentWiseCAV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.StatsLiveDataAgentWiseCAV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }
            var result = spectrumLiveDataResult;
            response = Ok(result);

            return response;
        }

        [HttpGet]
        [Route("/api/Livedata/GetSpectrumLiveAgentLogCAV2")]
        public async Task<IActionResult> GetSpectrumLiveAgentLogCAV2([FromQuery(Name = "login")]string login)
        {

            IActionResult response = null;
            IEnumerable<AgencyAgentLogLive> spectrumLiveDataResult = null;
            List<AgencyAgentLogLive> _spectrumLiveDataResult = new List<AgencyAgentLogLive>();
            if (!_cache.TryGetValue(CacheKeys.StatsLiveDataAgentLogCAV2.ToString(), out spectrumLiveDataResult))
            {
                spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumLiveAgentLogCAV2();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.StatsLiveDataAgentLogCAV2.ToString(), spectrumLiveDataResult, cacheEntryOptions);
            }
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpPost]
        [Route("/api/Livedata/GetStatsWeeklyDataAgentWiseCAV2")]
        public async Task<IActionResult> GetStatsWeeklyDataAgentWiseCAV2([FromBody] SpectrumParam data)
        {
            if (data.date == null)
            {
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
                data.date.Add(DateTime.Now.AddDays(-1).ToShortDateString());
            }
            IActionResult response = null;
            IEnumerable<SpectrumLiveData> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetStatsWeeklyDataAgentWiseCAV2(data);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }

        [HttpPost]
        [Route("/api/Livedata/GetSpectrumExtensionWiseCallDetailsLogCAV2")]
        public async Task<IActionResult> GetSpectrumExtensionWiseCallDetailsLogCAV2([FromBody] StratusExtensionParam data)
        {

            IActionResult response = null;
            IEnumerable<SpectrumAgentCall> spectrumLiveDataResult = null;
            spectrumLiveDataResult = _spectrumLiveDataService.GetSpectrumExtensionWiseCallDetailsCA(data);
            var result = spectrumLiveDataResult;
            response = Ok(result);
            return response;
        }
        #endregion
    }
}



