﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.AlphaTop5Agent;
using AmaxIns.ServiceContract.AlphaTop5Agent;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.AlphaTop5Agent
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TopAgentController : BaseController
    {
        ITopAgentService _service;
        public TopAgentController(ITopAgentService service) : base(service)
        {
            _service = service;
        }

        [Route("/api/TopAgent/GetAgency")]
        [HttpGet]
        public async Task<IActionResult> GetAgencyName()
        {
            IActionResult response = null;
            var result = await _service.GetAgency();
            response = Ok(result);
            return response;
        }

        [Route("/api/TopAgent/GetYear")]
        [HttpGet]
        public async Task<IActionResult> GetYear()
        {
            IActionResult response = null;
            var result = await _service.GetYear();
            response = Ok(result);
            return response;
        }

        [Route("/api/TopAgent/GetPremiumData")]
        [HttpPost]
        public async Task<IActionResult> GetPremiumData([FromBody] TopFiveAgentUserModel model)
        {
            IActionResult response = null;
            var result = await _service.GetPremiumData(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/TopAgent/GetAgencyFee")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyFee([FromBody] TopFiveAgentUserModel model)
        {
            IActionResult response = null;
            var result = await _service.GetAgencyFeeData(model);
            response = Ok(result);
            return response;
        }


        [Route("/api/TopAgent/GetPolicyCount")]
        [HttpPost]
        public async Task<IActionResult> GetPolicyCount([FromBody] TopFiveAgentUserModel model)
        {
            IActionResult response = null;
            var result = await _service.GetPolicyCount(model);
            response = Ok(result);
            return response;
        }
    }
}