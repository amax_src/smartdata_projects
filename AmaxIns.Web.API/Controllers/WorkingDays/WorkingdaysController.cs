﻿using AmaxIns.ServiceContract.WorkingDays;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.WorkingDays
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkingDaysController : BaseController
    {
        protected readonly IWorkingDaysService _workingdayService;

        public WorkingDaysController(IWorkingDaysService workingdaysService) : base(workingdaysService)
        {
            _workingdayService = workingdaysService;
        }

        [Route("/api/WorkingDays")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _workingdayService.GetWorkingDaysOfMonth();
            var response = Ok(result);
            return response;
        }

    }
}