﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.AplhaCustomerInfo;
using AmaxIns.DataContract.Enums;
using AmaxIns.ServiceContract.AlphaCustomerInfo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace AmaxIns.Web.API.Controllers.AlphaCustomerInfo
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AlphaCustomerInfoController : BaseController
    {
        protected readonly ICustomerInfoService _customerInfoService;
        private IMemoryCache _cache;

        public AlphaCustomerInfoController(ICustomerInfoService customerInfoService, IMemoryCache memoryCache) : base(customerInfoService)
        {
            _customerInfoService = customerInfoService;
            _cache = memoryCache;
        }

        [Route("/api/AlphaCustomerInfo/GetAgencyName")]
        [HttpGet]
        public async Task<IActionResult> GetAgencyName()
        {
            IActionResult response = null;
            var result = await _customerInfoService.GetAgencyName();
            response = Ok(result);
            return response;
        }

        [Route("/api/AlphaCustomerInfo/GetclientName")]
        [HttpGet]
        public async Task<IActionResult> GetclientName([FromQuery(Name = "SearchText")] string SearchText)
        {
            IActionResult response = null;
            var result = await _customerInfoService.GetclientName(SearchText);
            response = Ok(result);
            return response;
        }

        [Route("/api/AlphaCustomerInfo/GetCity")]
        [HttpGet]
        public async Task<IActionResult> GetCity()
        {
            IActionResult response = null;
            var result = await _customerInfoService.GetCity();
            response = Ok(result);
            return response;
        }

        [Route("/api/AlphaCustomerInfo/GetPolicyType")]
        [HttpGet]
        public async Task<IActionResult> GetPolicyType()
        {
            IActionResult response = null;
            var result = await _customerInfoService.GetPolicyType();
            response = Ok(result);
            return response;
        }


        [Route("/api/AlphaCustomerInfo/GetPolicyNumber")]
        [HttpGet]
        public async Task<IActionResult> GetPolicyNumber([FromQuery(Name = "SearchText")] string SearchText)
        {
            IActionResult response = null;
            var result = await _customerInfoService.GetPolicyNumber(SearchText);
            response = Ok(result);
            return response;
        }

        [Route("/api/AlphaCustomerInfo/CustomerInfoData")]
        [HttpPost]
        public async Task<IActionResult> CustomerInfoData([FromQuery(Name = "Year")] string Year, [FromBody] CustmoreInfoUserModel model)
        {
            IActionResult response = null;
            IEnumerable<CustomerInfoModel> CustomerInfoResult = new List<CustomerInfoModel>();
            if (!_cache.TryGetValue(CacheKeys.CustomerInfoAlpha.ToString(), out CustomerInfoResult))
            {
                CustomerInfoResult = await _customerInfoService.CustomerInfoData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.CustomerInfoAlpha.ToString(), CustomerInfoResult, cacheEntryOptions);
            }
            if(!string.IsNullOrWhiteSpace(model.policyNumber))
            {
                CustomerInfoResult = CustomerInfoResult.Where(m => Convert.ToDateTime(m.PolicyEffectiveDate).Year.ToString() == Year && m.PolicyNumber.ToLower() == model.policyNumber.ToLower());
            }
            else if(!string.IsNullOrWhiteSpace(model.clientName))
            {
                CustomerInfoResult = CustomerInfoResult.Where(m => Convert.ToDateTime(m.PolicyEffectiveDate).Year.ToString() == Year && m.ClientName.ToLower()==model.clientName.ToLower());
            }
            else
            {
                CustomerInfoResult = CustomerInfoResult.Where(m => Convert.ToDateTime(m.PolicyEffectiveDate).Year.ToString() == Year);
                CustomerInfoResult = CustomerInfoResult.Where(m => m.agencyname.ToLower() == model.agency.Trim().ToLower());
                CustomerInfoResult = CustomerInfoResult.Where(m => m.PolicyType == model.policyType.Trim());
                CustomerInfoResult = CustomerInfoResult.Where(m => m.City == model.city.Trim());
            }
            response = Ok(CustomerInfoResult);
            return response;
        }
    }
}