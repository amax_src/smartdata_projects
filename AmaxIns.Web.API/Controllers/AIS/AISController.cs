﻿using AmaxIns.DataContract.AIS;
using AmaxIns.DataContract.Enums;
using AmaxIns.ServiceContract.AIS;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.EPR
{
    [Produces("application/json")]
    [Authorize]
    public class AISController : BaseController
    {
        protected readonly IAISService _service;
        private IMemoryCache _cache;

        public AISController(IAISService service, IMemoryCache memoryCache) : base(service)
        {
            _service = service;
            _cache = memoryCache;
        }

        [HttpGet]
        [Route("/api/AIS/GetAISData")]
        public async Task<IActionResult> GetAISData()
        {
            IActionResult response = null;
            IEnumerable<AISModel> AisResult = null;

            if (!_cache.TryGetValue(CacheKeys.AISData.ToString(), out AisResult))
            {
                AisResult = await _service.GetAISData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.AISData.ToString(), AisResult, cacheEntryOptions);
            }

            var Result = AisResult;

            response = Ok(Result);

            return response;
        }

        [HttpGet]
        [Route("/api/AIS/GetAISDataCSV")]
        public async Task<IActionResult> GetAISDataCSV()
        {
           // IActionResult response = null;
            IEnumerable<AISModel> AisResult = null;
            AisResult = await _service.GetAISDataCsv();
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Year,Month,Location,Policies,AgencyFee,Premium,Transactions,AvgAgencyFeeByPolicy");
            foreach (var x in AisResult)
            {
                sb.AppendLine(x.Year + "," + x.Month + "," + x.Location + "," + x.Policies + "," + x.AgencyFee + "," + x.Premium + "," + x.Transactions + "," + x.AvgAgencyFeeByPolicy );
            }
            
            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
           
            return File(bytes, "application/octet-stream", "AISReport.csv"); 
        }


    }
}