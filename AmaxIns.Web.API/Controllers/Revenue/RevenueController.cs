﻿using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.Revenue;
using AmaxIns.ServiceContract.Revenue;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Web.API.Controllers.Revenue
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RevenueController : BaseController
    {
        protected readonly IRevenueService _revenueService;
        private IMemoryCache _cache;

        public RevenueController(IRevenueService revenueService, IMemoryCache memoryCache) : base(revenueService)
        {
            _revenueService = revenueService;
            _cache = memoryCache;
        }


        [Route("/api/Revenue")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IActionResult response = null;
            IEnumerable<RevenueModel> Revenueresult = null;
            if (!_cache.TryGetValue(CacheKeys.RevenueMonthly.ToString(), out Revenueresult))
            {
                Revenueresult = await _revenueService.GetRevenueData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.RevenueMonthly.ToString(), Revenueresult, cacheEntryOptions);
            }
            var result = Revenueresult;// await _revenueService.GetRevenueData();
            response = Ok(result);
            return response;
        }


        [Route("/api/RevenueDaily")]
        [HttpGet]
        public async Task<IActionResult> GetDaily([FromQuery(Name = "month")] string month)
        {
            IActionResult response = null;
            IEnumerable<RevenueDailyModel> Revenueresult = null;
            if (!_cache.TryGetValue(CacheKeys.RevenueDaily.ToString(), out Revenueresult))
            {
                Revenueresult = await _revenueService.GetRevenueDailyData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.RevenueDaily.ToString(), Revenueresult, cacheEntryOptions);
            }

            var filterResult = Revenueresult.Where(m => m.Actmonth == month).ToList();

            var result = filterResult;// await _revenueService.GetRevenueDailyData();
            response = Ok(result);
            return response;
        }

        [Route("/api/Revenue/GetRefreshDate")]
        [HttpGet]
        public async Task<IActionResult> GetRefreshDate()
        {
            var result = await _revenueService.GetDataRefreshDate();
            var response = Ok(result);
            return response;
        }

        [Route("/api/Revenue/GetTierData")]
        [HttpGet]
        public async Task<IActionResult> GetTierData()
        {
            var result = await _revenueService.GetTierPercentage();
            var response = Ok(result);
            return response;
        }

        [Route("/api/Revenue/GetMonth")]
        [HttpGet]
        public async Task<IActionResult> GetMonth([FromQuery(Name ="year")] string year )
        {
            var result = await _revenueService.Getmonths(year);
            var response = Ok(result);
            return response;
        }
    }
}