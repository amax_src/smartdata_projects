﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Referral;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Winback
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class WinbackController : BaseController
    {
        protected readonly IWinbackService _winbackService;
        public WinbackController(IWinbackService winbackService) : base(winbackService)
        {
            _winbackService = winbackService;
        }

        [HttpGet]
        [Route("/api/Winback/GetWinbackData")]
        public IActionResult GetWinbackData([FromQuery(Name = "month")]int month, [FromQuery(Name = "year")]int year)
        {
            IActionResult response = null;
            IEnumerable<WinbackAPIModel> winbackdataResult = null;
            winbackdataResult = _winbackService.GetWinBackDataForAPI(month, year);
            var result = winbackdataResult;
            response = Ok(result);
            return response;
        }
    }
}