﻿using AmaxIns.DataContract.APILoad;
using AmaxIns.ServiceContract.APILoad;
using AmaxIns.ServiceContract.Dates;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.APILoad
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class APILoadController : BaseController
    {
        protected readonly IApiLoadService _service;

        public APILoadController(IApiLoadService service) : base(service)
        {
            _service = service;
        }


        [HttpPost]
        [Route("/api/APILoad/Save")]
        public async Task<IActionResult> Save(ApiLoadModel model)
        {
             _service.Save(model);

            var response = Ok("Saved");

            return response;
        }
    }
}