﻿using AmaxIns.DataContract.Authentication;
using AmaxIns.ServiceContract.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.Authentication
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : BaseController
    {
        protected readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService) : base(authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [Route("/api/Authentication/ValidateUsingOTPRequest")]
        public async Task<IActionResult> ValidateUsingOTPRequest(LoginModel loginModel)
        {
            //var result = await _authenticationService.LogInAsync(new DataContract.Authentication.LoginModel { LoginType = "headofdepratment", Password = "admin", UserName = "admin@admin.com" });
            var result = await _authenticationService.LogInAsyncUsingOTPRequest(loginModel);

            if (result == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            //result.UserName = "Indu Bhushan";
            var response = Ok(result);

            return response;
        }

        [HttpPost]
        [Route("/api/Authentication/Validate")]
        public async Task<IActionResult> Validate(LoginModel loginModel)
        {
            //var result = await _authenticationService.LogInAsync(new DataContract.Authentication.LoginModel { LoginType = "headofdepratment", Password = "admin", UserName = "admin@admin.com" });
            var result = await _authenticationService.LogInAsync(loginModel);

            if (result == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            //result.UserName = "Indu Bhushan";
            var response = Ok(result);

            return response;
        }

        [HttpPost]
        [Route("/api/Authentication/FDValidate")]
        public async Task<IActionResult> FDValidate(LoginModel loginModel)
        {
            //var result = await _authenticationService.LogInAsync(new DataContract.Authentication.LoginModel { LoginType = "headofdepratment", Password = "admin", UserName = "admin@admin.com" });
            var result = await _authenticationService.FDLogInAsync(loginModel);

            if (result == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            //result.UserName = "Indu Bhushan";
            var response = Ok(result);

            return response;
        }

        [Route("/api/Authentication/ResetPassword")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPassword login)
        {
            var result = _authenticationService.UpdatePassword(login);
            var response = Ok(result);

            return response;
        }

        [Route("/api/Authentication/CheckUserName/{email}")]
        [HttpGet]
        public async Task<IActionResult> CheckUserName(string email,[FromQuery(Name = "loginType")] string loginType)
        {
            var result = await _authenticationService.ValidateUserName(email, loginType);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Authentication/ForgotPassword/{email}")]
        [HttpGet]
        public async Task<IActionResult> ForgotPassword(string email, [FromQuery(Name = "loginType")] string loginType)
        {
            var result = await _authenticationService.SendPasswordEmail(email, loginType); //await _authenticationService.ValidateUserName(email);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Authentication/ValidateDesktop")]
        [HttpPost]
        public async Task<IActionResult> ValidateDesktop([FromBody] LoginModel _login)
        {
            var result = await _authenticationService.LogInDesktopAsync(_login.UserName, _login.Password);
            var response = Ok(result);
            return response;
        }

    }
}