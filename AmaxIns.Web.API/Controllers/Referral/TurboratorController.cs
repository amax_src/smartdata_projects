﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Referral.Turborator;
using AmaxIns.Service.Refferal;
using AmaxIns.ServiceContract.Referral;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Referral
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TurboratorController : BaseController
    {
        private readonly ITurboratorService _turboratorService;
        public TurboratorController(ITurboratorService turboratorService) : base(turboratorService)
        {

            _turboratorService = turboratorService;
        }


        [HttpPost]
       
        public  IActionResult Post([FromBody] TurboratorModel turboratorModel)
        {
            try
            {
                if (turboratorModel == null)
                {
                    return BadRequest("model cannot be null");
                }
                if(turboratorModel.Customer==null)
                {
                    return BadRequest("Customer details cannot be null");
                }
                if (turboratorModel.Drivers == null)
                {
                    return BadRequest("Drivers information cannot be null");
                }
                
                _turboratorService.SaveData(turboratorModel);
                return Ok("Saved Success");

            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
           
        }
    }
}