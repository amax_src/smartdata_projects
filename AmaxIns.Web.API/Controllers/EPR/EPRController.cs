﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.Reports;
using AmaxIns.ServiceContract.EPR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace AmaxIns.Web.API.Controllers.EPR
{
    [Produces("application/json")]
   [Authorize]
    public class EPRController : BaseController
    {
        protected readonly IEPRService _service;
        private IMemoryCache _cache;

        public EPRController(IEPRService service, IMemoryCache memoryCache) : base(service)
        {
            _service = service;
            _cache = memoryCache;
        }

        [HttpPost]
        [Route("/api/EPR/GetEPRData")]
        public async Task<IActionResult> GetEPRData([FromQuery(Name = "month")] string month, [FromBody] List<int> AgencyId,[FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;

            IEnumerable<EPRModel> Eprresult = null;
            List<EPRModel> FilterEprresult = new List<EPRModel>();
            if (!_cache.TryGetValue(CacheKeys.GetEPRData.ToString(), out Eprresult))
            {
                Eprresult = await _service.GetEPRData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.GetEPRData.ToString(), Eprresult, cacheEntryOptions);
            }

            //var eprFilterData=
            var Result = Eprresult.Where(m=>m.Month==month && m.Year==year); //await _service.GetEPRData();
            if (AgencyId != null && AgencyId.Count > 0)
            {
                foreach (var x in AgencyId)
                {
                    var resultByAgency = Result.Where(m => m.AgencyID == x);
                    foreach(var z in resultByAgency)
                    {
                        FilterEprresult.Add(z);
                    }
                }
                response = Ok(FilterEprresult);
            }
            else
            {
                response = Ok(Result);
            }
           
            return response;
        }

        [HttpGet]
        [Route("/api/EPR/GetEPRMonth")]
        public async Task<IActionResult> GetEPRMonth()
        {
            IEnumerable<string> months = null;
            if (!_cache.TryGetValue(CacheKeys.EPRMonth.ToString(), out months))
            {
                months = await _service.GetEPRmonths();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.EPRMonth.ToString(), months, cacheEntryOptions);
            }

            IActionResult response = null;

            var Result = months;//await _payrollService.GetPayRoleActualAndBudgetedData();

            response = Ok(Result);

            return response;
        }


          [HttpPost]
        [Route("/api/EPR/GetEPRTotalData")]
        public async Task<IActionResult> GetEPRTotalData([FromQuery(Name = "month")] string month, [FromBody] List<int> AgencyId,[FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;

            IEnumerable<EPRModel> Eprresult = null;
            List<EPRModel> FilterEprresult = new List<EPRModel>();
            if (!_cache.TryGetValue(CacheKeys.GetEPRTotalData.ToString(), out Eprresult))
            {
                Eprresult = await _service.GetEPRTotalData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.GetEPRTotalData.ToString(), Eprresult, cacheEntryOptions);
            }

            //var eprFilterData=
            var Result = Eprresult.Where(m=>m.Month==month && m.Year==year); //await _service.GetEPRData();
            if (AgencyId != null && AgencyId.Count > 0)
            {
                foreach (var x in AgencyId)
                {
                    var resultByAgency = Result.Where(m => m.AgencyID == x);
                    foreach(var z in resultByAgency)
                    {
                        FilterEprresult.Add(z);
                    }
                }
                response = Ok(FilterEprresult);
            }
            else
            {
                response = Ok(Result);
            }
           
            return response;
        }

        [Route("/api/EPR/GetEPRGraph/")]
        [HttpPost]
        public async Task<IActionResult> GetEPRGraph([FromBody] paramFilterepr listOfArray)
        {
            string _selectedagent = null;

            string year = string.Join(",", listOfArray.year);
            string location = string.Join(",", listOfArray.location);
            if (listOfArray.selectedagent != null)
            {
                _selectedagent = string.Join(",", listOfArray.selectedagent);
            }

            var result = await _service.GetEPRGraph(year, location, _selectedagent);
            var response = Ok(result);
            return response;
        }
    }
}