﻿using AmaxIns.DataContract.User;
using AmaxIns.ServiceContract.Agency;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Web.API.Controllers.Agency
{
    [Route("api/[controller]")]
    [ApiController]
   // [Authorize]
    public class AgencyController : BaseController
    {
        protected readonly IAgencyService _agencyService;

        public AgencyController(IAgencyService agencyService) : base(agencyService)
        {
            _agencyService = agencyService;
        }

        [Route("/api/Agency/GetByRegionalManager/")]
        [HttpPost]
        public async Task<IActionResult> GetByRegionalManager([FromBody] List<int> id)
        {
            var result = await _agencyService.Get(id);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }

        [Route("/api/Agency")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _agencyService.Get();
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);

            return response;
        }

        [Route("/api/Agency/GetByRegionalAndZonalManager/")]
        [HttpPost]
        public async Task<IActionResult> GetByRegionalAndZonalManager([FromBody] ZonalRegionalManagerModel ids )
        {
            var result = await _agencyService.Get(ids.Rmid, ids.Zmid);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }

        [Route("/api/Agency/GetByBsmId")]
        [HttpPost]
        public async Task<IActionResult> GetByBsmId([FromBody] List<int> ids)
        {
            var result = await _agencyService.GetBSMAgency(ids);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }

        [Route("/api/Agency/GetStoreManagerAgency")]
        [HttpPost]
        public async Task<IActionResult> GetStoreManagerAgency([FromBody] int id)
        {
            var result = await _agencyService.GetStoreManagerAgency(id);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }
    }
}