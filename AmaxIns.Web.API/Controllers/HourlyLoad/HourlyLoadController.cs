﻿using AmaxIns.DataContract.HourlyLoad;
using AmaxIns.DataContract.Quotes;
using AmaxIns.ServiceContract.HourlyLoad;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace AmaxIns.Web.API.Controllers.HourlyLoad
{
    [Route("api/[controller]")]
    [ApiController]
    public class HourlyLoadController : BaseController
    {
        protected readonly IHourlyLoadService _service;

        public HourlyLoadController(IHourlyLoadService service) : base(service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("/api/HourlyLoad/GetMonth/{year}")]
        public async Task<IActionResult> GetMonth(string year)
        {
            IActionResult response = null;
            var months = await _service.GetMonth(year);
            response = Ok(months);
            return response;
        }

        [HttpGet]
        [Route("/api/HourlyLoad/GetDate")]
        public async Task<IActionResult> GetDate([FromQuery(Name = "month")] int month, [FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;
            var dates = await _service.GetDates(month, year);
            response = Ok(dates);
            return response;
        }

        [HttpGet]
        [Route("/api/HourlyLoad/GetData")]
        public async Task<IActionResult> GetData([FromQuery(Name = "Date")] string date, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            IActionResult response = null;
            var data = await _service.GetData(date, AgencyId);
            response = Ok(data);
            return response;
        }

        //Hourly new and modified quotes 
        [HttpGet]
        [Route("/api/HourlyLoad/GetHourlyNewModifiedQuotes")]
        public async Task<IActionResult> GetHourlyNewModifiedQuotes([FromQuery(Name = "Date")] string date, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            IActionResult response = null;
            var data = await _service.GetHourlynewModifiedQuotes(date, AgencyId);
            response = Ok(data);
            return response;
        }

        [HttpGet]
        [Route("/api/HourlyLoad/MergeData")]
        public async Task<IActionResult> MergeData([FromQuery(Name = "Date")] string date, [FromQuery(Name = "AgencyId")] int AgencyId)
        {
            IActionResult response = null;
            var data = _service.MergeData(date, AgencyId);
            response = Ok(data);
            return response;
        }
        
        //Hourly load export
        //url -api/HourlyLoad/ExportHourlyData?startdate=2020-01-12&enddate=2020-01-12

        [HttpGet]
        [Route("/api/HourlyLoad/ConsolidatedHourlyData")]
        public async Task<IActionResult> ConsolidatedHourlyData([FromQuery(Name = "StartDate")] string startdate, [FromQuery(Name = "EndDate")] string enddate)
        {
            IActionResult response = null;
            var data = _service.MergeData(startdate, enddate).OrderBy(m => m.Agencyid);
            response = Ok(data);
            return response;
        }

        [HttpGet]
        [Route("/api/HourlyLoad/ExportHourlyData")]
        public FileResult ExportHourlyData([FromQuery(Name = "StartDate")] string startdate, [FromQuery(Name = "EndDate")] string enddate)
        {
            var data = _service.MergeData(startdate, enddate).OrderBy(m => m.Agencyid);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Date,Location,Agents,Agencyfee,Policies,HourIntervel,Premium,Transactions,ModifiedQuotes,NewQuotes");
            foreach (var x in data)
            {
                sb.AppendLine(DateTime.Parse(x.Date).ToShortDateString() + "," + x.Location + "," +x.AgentCount+","+ x.Agencyfee + "," + x.Policies + "," + x.HourIntervel + "," + x.Premium + "," + x.Transactions + "," + x.ModifiedQuotes + "," + x.NewQuotes);
            }

            return new FileContentResult(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv")
            {
                FileDownloadName = "HourlyLoad.csv"
            };
        }




        //Peak Hours
        //url -api/HourlyLoad/PeakHoursData?startdate=2020-01-12&enddate=2020-01-13&AgencyId=24&AgencyId=70 &AgencyId=80

        [HttpPost]
        [Route("/api/HourlyLoad/PeakHoursData")]
        public async Task<IActionResult> PeakHoursData([FromQuery(Name = "StartDate")] string startdate, [FromQuery(Name = "EndDate")] string enddate, [FromBody] List<int> AgencyId)
        {
            IActionResult response = null;
            var data = await _service.GetPeakHoursData(startdate, enddate,AgencyId);
            response = Ok(data);
            return response;
        }
        [HttpGet]
        [Route("/api/HourlyLoad/ExportPeakData")]
        public async Task<FileResult> ExportPeakDataAsync([FromQuery(Name = "StartDate")] string startdate, [FromQuery(Name = "EndDate")] string enddate)
        {
            List<int> agencyId = new List<int>();
            var data = await _service.GetPeakHoursData(startdate, enddate, agencyId);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("PayDate,Location,Peak Hour Premium,Premium,Peak Hour Agency Fee,Agency Fee,Peak Hour Policies,Policies,Peak Hour Transactions,Transactions,Peak Hour Modified Quotes,	Modified Quotes,Peak Hour New Quotes,New Quotes");
            foreach (var x in data)
            {
                sb.AppendLine(DateTime.Parse(x.PayDate).ToShortDateString() + "," 
                    + x.Location + "," +
                    x.peakhourpremium + "," +
                    x.premium + "," +
                    x.peakhourafee + "," +
                    x.agencyfee + "," +
                    x.peakhourpolicies + "," +
                    x.policies + "," +
                    x.peakhourtransactions + "," +
                    x.transactions + "," +
                    x.peakhourmodifiedquotes + "," +
                    x.modifiedquotes + "," +
                    x.peakhournewquotes + "," +
                    x.newquotes);
            }

            return new FileContentResult(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv")
            {
                FileDownloadName = "PeakData.csv"
            };
        }
    }
}