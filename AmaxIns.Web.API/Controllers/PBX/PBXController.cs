﻿using AmaxIns.DataContract.Enums;
using AmaxIns.DataContract.PBX;
using AmaxIns.ServiceContract.PBX;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace AmaxIns.Web.API.Controllers.PBX
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PBXController : BaseController
    {
        protected readonly IPbxService _pbxService;
        private IMemoryCache _cache;

        public PBXController(IPbxService pbxService, IMemoryCache memoryCache) : base(pbxService)
        {
            _pbxService = pbxService;
            _cache = memoryCache;
        }

        [Route("/api/PBX/GetExtention")]
        [HttpGet]
        public async Task<IActionResult> GetExtention()
        {
            var result = await _pbxService.GetExtention();
            var response = Ok(result);
            return response;
        }

        [Route("/api/PBX/GetMonth")]
        [HttpGet]
        public async Task<IActionResult> GetMonth()
        {
            var result = await _pbxService.GetMonth();
            var response = Ok(result);
            return response;
        }

        [Route("/api/PBX/GetPBXData")]
        [HttpGet]
        public async Task<IActionResult> GetPBXData([FromQuery(Name = "month")] string month)
        {

            IEnumerable<PbxModel> PbxResult = null;
            if (!_cache.TryGetValue(CacheKeys.PBX.ToString(), out PbxResult))
            {
                PbxResult = await _pbxService.GetPBXData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.PBX.ToString(), PbxResult, cacheEntryOptions);
            }

            var result = PbxResult.Where(m => m.Month == month);

            var response = Ok(result);
            return response;
        }


        [Route("/api/PBX/GetPBXDailyData")]
        [HttpGet]
        public async Task<IActionResult> GetPBXDailyData()
        {

            IEnumerable<PbxModel> PbxDailyResult = null;
            if (!_cache.TryGetValue(CacheKeys.PBXDaily.ToString(), out PbxDailyResult))
            {
                PbxDailyResult = await _pbxService.GetPBXDailyData();
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(24));
                _cache.Set(CacheKeys.PBXDaily.ToString(), PbxDailyResult, cacheEntryOptions);
            }

            var response = Ok(PbxDailyResult);
            return response;
        }
    }
}