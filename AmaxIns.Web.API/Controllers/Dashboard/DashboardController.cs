﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.ServiceContract.Dashboard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Dashboard
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : BaseController
    {
        protected readonly IDashboardService _services;

        public DashboardController(IDashboardService services) : base(services)
        {
            _services = services;
          
        }

        [HttpPost]
        [Route("/api/Dashboard/GetQuotesDashboard/{month}/{year}")]
        public async Task<IActionResult> GetQuotesDashboard(int month, int year,[FromBody]List<int> agencyid)
        {
            var result = _services.GetQuoteData(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }


        [HttpPost]
        [Route("/api/Dashboard/GetRevenueDashboard/{month}/{year}")]
        public async Task<IActionResult> GetRevenueDashboard(int month, int year, [FromBody]List<int> agencyid)
        {
            var result = _services.GetSalesData(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }

        [HttpPost]
        [Route("/api/Dashboard/GetEPRDashboard/{month}/{year}")]
        public async Task<IActionResult> GetEPRDashboard(int month, int year, [FromBody]List<int> agencyid)
        {
            var result = _services.GetEprData(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }

        [HttpPost]
        [Route("/api/Dashboard/GetCallsDashboard/{month}/{year}")]
        public async Task<IActionResult> GetCallsDashboard(int month, int year, [FromBody]List<int> agencyid)
        {
            var result = _services.GetCallsData(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }


        [HttpPost]
        [Route("/api/Dashboard/GetPayrollDashboardData/{month}/{year}")]
        public async Task<IActionResult> GetPayrollDashboardData(int month, int year, [FromBody]List<int> agencyid)
        {
            var result = _services.GetPayrollDashboardData(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }

        [HttpPost]
        [Route("/api/Dashboard/GetEPRDashboardDetails/{month}/{year}")]
        public async Task<IActionResult> GetEPRDashboardDetails(int month, int year, [FromBody]List<int> agencyid)
        {
            var result = _services.GetEprDetails(agencyid, month, year);
            var response = Ok(result.Result);
            return response;
        }
        
    }


    



}