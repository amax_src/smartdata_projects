﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.EPR;
using AmaxIns.DataContract.Reports;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.ALPA
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ALPAEPRController : BaseController
    {
        protected readonly IAlpaEprService _service;
        public ALPAEPRController(IAlpaEprService service) : base(service)
        {
            _service = service;
           
        }

        [HttpPost]
        [Route("/api/ALPAEPR/GetAlpaEPRData")]
        public async Task<IActionResult> GetEPRData([FromQuery(Name = "month")] string month, [FromBody] List<int> AgencyId, [FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;

            IEnumerable<EPRModel> Eprresult = null;
            List<EPRModel> FilterEprresult = new List<EPRModel>();
                Eprresult = await _service.GetEPRData();
            //var eprFilterData=
            var Result = Eprresult.Where(m => m.Month == month && m.Year == year); //await _service.GetEPRData();
            if (AgencyId != null && AgencyId.Count > 0)
            {
                foreach (var x in AgencyId)
                {
                    var resultByAgency = Result.Where(m => m.AgencyID == x);
                    foreach (var z in resultByAgency)
                    {
                        FilterEprresult.Add(z);
                    }
                }
                response = Ok(FilterEprresult);
            }
            else
            {
                response = Ok(Result);
            }

            return response;
        }

        [HttpGet]
        [Route("/api/ALPAEPR/GetAlpaEPRMonth")]
        public async Task<IActionResult> GetEPRMonth()
        {
            IEnumerable<string> months = null;
                months = await _service.GetEPRmonths();
            IActionResult response = null;

            var Result = months;//await _payrollService.GetPayRoleActualAndBudgetedData();

            response = Ok(Result);

            return response;
        }


        [HttpPost]
        [Route("/api/ALPAEPR/GetAlpaEPRTotalData")]
        public async Task<IActionResult> GetEPRTotalData([FromQuery(Name = "month")] string month, [FromBody] List<int> AgencyId, [FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;

            IEnumerable<EPRModel> Eprresult = null;
            List<EPRModel> FilterEprresult = new List<EPRModel>();
                Eprresult = await _service.GetEPRTotalData();
            //var eprFilterData=
            var Result = Eprresult.Where(m => m.Month == month && m.Year == year); //await _service.GetEPRData();
            if (AgencyId != null && AgencyId.Count > 0)
            {
                foreach (var x in AgencyId)
                {
                    var resultByAgency = Result.Where(m => m.AgencyID == x);
                    foreach (var z in resultByAgency)
                    {
                        FilterEprresult.Add(z);
                    }
                }
                response = Ok(FilterEprresult);
            }
            else
            {
                response = Ok(Result);
            }

            return response;
        }

        [Route("/api/ALPAEPR/GetAlpaEPRGraph/")]
        [HttpPost]
        public async Task<IActionResult> GetEPRGraph([FromBody] paramFilterepr listOfArray)
        {
            string _selectedagent = null;

            string year = string.Join(",", listOfArray.year);
            string location = string.Join(",", listOfArray.location);
            if (listOfArray.selectedagent != null)
            {
                _selectedagent = string.Join(",", listOfArray.selectedagent);
            }

            var result = await _service.GetEPRGraph(year, location, _selectedagent);
            var response = Ok(result);
            return response;
        }
    }
}