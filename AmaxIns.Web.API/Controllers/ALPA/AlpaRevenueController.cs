﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Quotes;
using AmaxIns.DataContract.Revenue;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.ALPA
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AlpaRevenueController : BaseController
    {
        protected readonly IAlpaRevenueService _revenueService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public AlpaRevenueController(IAlpaRevenueService revenueService,  IHostingEnvironment hostingEnvironment) : base(revenueService)
        {
            _revenueService = revenueService;
            _hostingEnvironment = hostingEnvironment;
        }


        [Route("/api/AlpaRevenue/GetAlpaRevenueData")]
        [HttpGet]
        public async Task<IActionResult> GetAlpaRevenueData()
        {
            IActionResult response = null;
            IEnumerable<RevenueModel> Revenueresult = null;

            Revenueresult = await _revenueService.GetAlpaRevenueData();

            var result = Revenueresult;// await _revenueService.GetRevenueData();
            response = Ok(result);
            return response;
        }

        [Route("/api/AlpaRevenue/AlpaRevenueDaily")]
        [HttpGet]
        public async Task<IActionResult> AlpaRevenueDaily([FromQuery(Name = "month")] string month)
        {
            IActionResult response = null;
            IEnumerable<RevenueDailyModel> Revenueresult = null;
            Revenueresult = await _revenueService.GetRevenueDailyData();
            var filterResult = Revenueresult.Where(m => m.Actmonth == month).ToList();

            var result = filterResult;// await _revenueService.GetRevenueDailyData();
            response = Ok(result);
            return response;
        }


        [Route("/api/AlpaRevenue/DailyTransactionPayments")]
        [HttpPost]
        public async Task<IActionResult> DailyTransactionPayments([FromBody] QuotesParam quotesParam, [FromQuery(Name = "month")] string month, [FromQuery(Name = "year")] string year)
        {
            IActionResult response = null;
            IEnumerable<DailyTransactionPayments> Revenueresult = null;
            Revenueresult = await _revenueService.DailyTransactionPayments(quotesParam, month, year);
            var result = Revenueresult;
            response = Ok(result);
            return response;
        }

        [Route("/api/AlpaRevenue/ExportDailyTransactonPayment/")]
        [HttpPost]
        public async Task<IActionResult> ExportDailyTransactonPayment([FromBody] IEnumerable<DailyTransactionPayments> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _revenueService.ExportDailyTransactonPayment(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
    }
}