﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.AIS;
using AmaxIns.ServiceContract.ALPA;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.ALPA
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AISAplaController : BaseController
    {
        protected readonly IAISAlpaService _service;
       

        public AISAplaController(IAISAlpaService service) : base(service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("/api/AISApla/GetAISAplaData")]
        public async Task<IActionResult> GetAISAplaData()
        {
            IActionResult response = null;
            IEnumerable<AISModel> AisResult = null;
            AisResult = await _service.GetAISData();
            var Result = AisResult;
            response = Ok(Result);
            return response;
        }
    }
}