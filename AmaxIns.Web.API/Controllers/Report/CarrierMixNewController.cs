﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.ServiceContract.Report;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Report
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CarrierMixNewController : BaseController
    {
        protected readonly ICarrierMixNewService _reportService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public CarrierMixNewController(ICarrierMixNewService reportService, IHostingEnvironment hostingEnvironment) : base(reportService)
        {
            _reportService = reportService;
            this._hostingEnvironment = hostingEnvironment;
        }

        #region CarrierMix
        [Route("/api/CarrierMixNew/CarrierMixI/{year}/{month}")]
        [HttpGet]
        public async Task<IActionResult> CarrierMixI(int year, string month)
        {
            var result = await _reportService.CarrierMixI(year, month);
            var response = Ok(result);
            return response;
        }

        [Route("/api/CarrierMixNew/CarrierMixPremium/{year}/{month}")]
        [HttpGet]
        public async Task<IActionResult> CarrierMixPremium(int year, string month)
        {
            var result = await _reportService.CarrierMixPremium(year, month);
            var response = Ok(result);
            return response;
        }

        [Route("/api/CarrierMixNew/CarrierMixAgencyFee/{year}/{month}")]
        [HttpGet]
        public async Task<IActionResult> CarrierMixAgencyFee(int year, string month)
        {
            var result = await _reportService.CarrierMixAgencyFee(year, month);
            var response = Ok(result);
            return response;
        }

        [Route("/api/CarrierMixNew/ExportExcelCarrierMix/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelCarrierMix([FromBody] IEnumerable<IDictionary<string, object>> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportExcelCarrierMix(list, root + "\\",0);
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/CarrierMixNew/ExportExcelCarrierMixPremium/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelCarrierMixPremium([FromBody] IEnumerable<IDictionary<string, object>> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportExcelCarrierMix(list, root + "\\",1);
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/CarrierMixNew/ExportExcelCarrierMixAgencyFee/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelCarrierMixAgencyFee([FromBody] IEnumerable<IDictionary<string, object>> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportExcelCarrierMix(list, root + "\\",2);
                var response = Ok(FileName);
                return response;
            });
            return result;
        }
        #endregion
    }
}
