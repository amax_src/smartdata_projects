﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace AmaxIns.Web.API.Controllers.Report
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportController : BaseController
    {
        protected readonly IReportService _reportService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public ReportController(IReportService reportService, IHostingEnvironment hostingEnvironment) : base(reportService)
        {
            _reportService = reportService;
            this._hostingEnvironment = hostingEnvironment;
        }

        #region AvgAgencyFees
        [Route("/api/Report/GetAvgAgencyFees/")]
        [HttpPost]
        public async Task<IActionResult> GetAvgAgencyFees([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year);  string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAvgAgencyFees(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetAvgAgencyFeesII/")]
        [HttpPost]
        public async Task<IActionResult> GetAvgAgencyFeesII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year);  string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAvgAgencyFeesII(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetAvgAgencyFeesIII/")]
        [HttpPost]
        public async Task<IActionResult> GetAvgAgencyFeesIII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year);  string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAvgAgencyFeesIII(year, month, location);
            var response = Ok(result);
            return response;
        }
        #endregion


        #region AgencyBreakdown
        [Route("/api/Report/GetAgencyBreakdownI/")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyBreakdownI([FromBody] Filter listOfArray)
        {

            string year= string.Join(",", listOfArray.year); ; string month= string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAgencyBreakdownI(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetAgencyBreakdownII/")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyBreakdownII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAgencyBreakdownII(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetAgencyBreakdownIII/")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyBreakdownIII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAgencyBreakdownIII(year, month, location);
            var response = Ok(result);
            return response;
        }
        #endregion


        #region Overtime
        [Route("/api/Report/GetOvertimeI/")]
        [HttpPost]
        public async Task<IActionResult> GetOvertimeI([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetOvertimeI(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetOvertimeII/")]
        [HttpPost]
        public async Task<IActionResult> GetOvertimeII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetOvertimeII(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetOvertimeIII/")]
        [HttpPost]
        public async Task<IActionResult> GetOvertimeIII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetOvertimeIII(year, month, location);
            var response = Ok(result);
            return response;
        }
        #endregion     

        #region RenterTracker
        [Route("/api/Report/GetRenterTracker/")]
        [HttpPost]
        public async Task<IActionResult> GetRenterTracker([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetRenterTracker(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetProductionTracker/")]
        [HttpPost]
        public async Task<IActionResult> GetProductionTracker([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetProductionTracker(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetActiveCustomerTracker/")]
        [HttpPost]
        public async Task<IActionResult> GetActiveCustomerTracker([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetActiveCustomerTracker(year, month, location);
            var response = Ok(result);
            return response;
        }
        #endregion

        #region Agent Performance
        [Route("/api/Report/GetAgentPerformaceI/")]
        [HttpPost]
        public async Task<IActionResult> GetAgentPerformaceI([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAgentPerformaceI(year, month, location);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Report/GetAgentPerformaceII/")]
        [HttpPost]
        public async Task<IActionResult> GetAgentPerformaceII([FromBody] Filter listOfArray)
        {
            string year = string.Join(",", listOfArray.year); ; string month = string.Join(",", listOfArray.months);
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetAgentPerformaceII(year, month, location);
            var response = Ok(result);
            return response;
        }
        #endregion

        #region ExcelReport

        #region Avg_Agency_Fee
        [Route("/api/Report/ExportExcelAgencyFeeI/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyFeeI([FromBody] IEnumerable<AvgAgencyFee> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelAgencyFeeI(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        [Route("/api/Report/ExportExcelAgencyFeeII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyFeeII([FromBody] IEnumerable<AvgAgencyFeeII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelAgencyFeeII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        [Route("/api/Report/ExportExcelAgencyFeeIII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyFeeIII([FromBody] IEnumerable<AvgAgencyFeeIII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelAgencyFeeIII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        #endregion

        #region AgencyBreakDown
        [Route("/api/Report/ExportExcelAgencyBreakDownByOfficeI/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyBreakDownByOfficeI([FromBody] IEnumerable<AgencyBreakdownI> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            string FileName = _reportService.ExportExcelAgencyBreakDownByOfficeI(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }

        [Route("/api/Report/ExportExcelAgencyBreakDownByOfficeII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyBreakDownByOfficeII([FromBody] IEnumerable<AgencyBreakdownII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            string FileName = _reportService.ExportExcelAgencyBreakDownByOfficeII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }

        [Route("/api/Report/ExportExcelAgencyBreakDownByOfficeIII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelAgencyBreakDownByOfficeIII([FromBody] IEnumerable<AgencyBreakdownIII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            string FileName = _reportService.ExportExcelAgencyBreakDownByOfficeIII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        #endregion

        #region OverTime_Excel
        [Route("/api/Report/ExportExcelGetOvertimeI/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelGetOvertimeI([FromBody] IEnumerable<OvertimeI> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelGetOvertimeI(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        [Route("/api/Report/ExportExcelGetOvertimeII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelGetOvertimeII([FromBody] IEnumerable<OvertimeII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelGetOvertimeII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        [Route("/api/Report/ExportExcelGetOvertimeIII/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelGetOvertimeIII([FromBody] IEnumerable<OvertimeIII> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelGetOvertimeIII(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        #endregion

        #region Tracker_Excel
        [Route("/api/Report/ExportRenterTracker/")]
        [HttpPost]
        public async Task<IActionResult> ExportRenterTracker([FromBody] IEnumerable<TrackerRender> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportRenterTracker(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Report/ExportProductionTracker/")]
        [HttpPost]
        public async Task<IActionResult> ExportProductionTracker([FromBody] IEnumerable<TrackerProduction> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportProductionTracker(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Report/ExportActiveCustomerTracker/")]
        [HttpPost]
        public async Task<IActionResult> ExportActiveCustomerTracker([FromBody] IEnumerable<TrackerActiveCustomer> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportActiveCustomerTracker(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }
        #endregion

        #region AgentPerformaceExcel

        [Route("/api/Report/ExportAgentPerformaceI/")]
        [HttpPost]
        public async Task<IActionResult> ExportAgentPerformaceI([FromBody] IEnumerable<AgentPerformance> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportAgentPerformaceI(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/Report/ExportAgentPerformaceII/")]
        [HttpPost]
        public async Task<IActionResult> ExportAgentPerformaceII([FromBody] IEnumerable<AgentPerformance> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _reportService.ExportAgentPerformaceII(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        #endregion

        #endregion
    }
}