﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AmaxIns.DataContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using System.IO;

namespace AmaxIns.Web.API.Controllers.Report
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RentersReportController : BaseController
    {
        protected readonly IRentersReportService _reportService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public RentersReportController(IRentersReportService reportService, IHostingEnvironment hostingEnvironment) : base(reportService)
        {
            _reportService = reportService;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Route("/api/RentersReport/GetRentersData/")]
        [HttpPost]
        public async Task<IActionResult> GetRenterData([FromBody] paramFilter listOfArray)
        {
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetRenterData(listOfArray.year, listOfArray.months, location);
            var response = Ok(result);
            return response;
        }

        [Route("/api/RentersReport/GetActiveCustomerData/")]
        [HttpPost]
        public async Task<IActionResult> GetActiveCustomerData([FromBody] paramFilter listOfArray)
        {
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetActiveCustomerData(listOfArray.year, listOfArray.months, location);
            var response = Ok(result);
            return response;
        }

        [Route("/api/RentersReport/ExportExcelRenterData/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelRenterData([FromBody] IEnumerable<RentersData> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            string FileName = _reportService.ExportExcelRenterData(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }

        [Route("/api/RentersReport/ExportExcelActiveCustomer/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelActiveCustomer([FromBody] IEnumerable<ActiveCustomerData> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            string FileName = _reportService.ExportExcelActiveCustomer(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }

    }
}
