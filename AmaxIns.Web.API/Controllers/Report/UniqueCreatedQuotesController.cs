﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Reports;
using AmaxIns.ServiceContract.Report;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Report
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UniqueCreatedQuotesController : BaseController
    {
        protected readonly IUniqueCreatedQuotesService _reportService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public UniqueCreatedQuotesController(IUniqueCreatedQuotesService reportService, IHostingEnvironment hostingEnvironment) : base(reportService)
        {
            _reportService = reportService;
            this._hostingEnvironment = hostingEnvironment;
        }

        #region Quotes
        [Route("/api/UniqueCreatedQuotes/GetAllUniqueCreatedQuotes/")]
        [HttpPost]
        public async Task<IActionResult> GetUniqueCreatedQuotes([FromBody] paramFilter listOfArray)
        {
            string location = string.Join(",", listOfArray.location);
            var result = await _reportService.GetUniqueCreatedQuotes(listOfArray.year, listOfArray.months, location);
            var response = Ok(result);
            return response;
        }

        [Route("/api/UniqueCreatedQuotes/GetAllMarket/")]
        [HttpGet]
        public async Task<IActionResult> GetAllMarket()
        {
            var result = await _reportService.GetAllMarket();
            var response = Ok(result);
            return response;
        }

        [Route("/api/UniqueCreatedQuotes/ExportExcelUniqueCretedQuotes/")]
        [HttpPost]
        public async Task<IActionResult> ExportExcelUniqueCretedQuotes([FromBody] IEnumerable<UniqueCreatedQuotes> list)
        {
            string root = _hostingEnvironment.WebRootPath + "\\Export";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string FileName = _reportService.ExportExcelUniqueCretedQuotes(list, root + "\\");
            var response = Ok(FileName);
            return response;
        }
        #endregion
    }
}
