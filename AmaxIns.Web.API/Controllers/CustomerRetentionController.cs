﻿using AmaxIns.DataContract.CustomerRetention;
using AmaxIns.ServiceContract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class CustomerRetentionController : BaseController
    {
        private readonly ICustomerRetentionService _customerRetentionService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public CustomerRetentionController(ICustomerRetentionService customerRetentionService, IHostingEnvironment hostingEnvironment) : base(customerRetentionService)
        {
            _customerRetentionService = customerRetentionService;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Route("/api/CustomerRetention")]
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] ListQueryModel model)
        {
            var resultService = await _customerRetentionService.GetAllPagingAsync(model);

            var response = Ok(resultService);
            return response;
        }

        [Route("/api/CustomerRetentionExport")]
        [HttpGet]
        public async Task<IActionResult> GetAllExport([FromQuery] ListQueryModel model)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _customerRetentionService.GetAllPagingExportAsync(model, root + "\\").GetAwaiter().GetResult();
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/CustomerRetentionCountForZone")]
        [HttpPost]
        public async Task<IActionResult> GetAllCountForZone([FromBody] CountListForZoneQueryModel model)
        {
            var resultService = await _customerRetentionService.GetAllCountForZonePagingAsync(model);

            var response = Ok(resultService);
            return response;
        }

        [Route("/api/CustomerRetentionCountForZoneExport")]
        [HttpPost]
        public async Task<IActionResult> CustomerRetentionCountForZoneExport([FromBody] CountListForZoneQueryModel model)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _customerRetentionService.GetAllCountForZonePagingAsyncExport(model, root + "\\").GetAwaiter().GetResult();
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/CustomerRetentionCountForRegion")]
        [HttpPost]
        public async Task<IActionResult> GetAllCountForRegion([FromBody] CountListForRegionQueryModel model)
        {
            var resultService = await _customerRetentionService.GetAllCountForRegionPagingAsync(model);

            var response = Ok(resultService);
            return response;
        }

        [Route("/api/CustomerRetentionCountForRegionExport")]
        [HttpPost]
        public async Task<IActionResult> CustomerRetentionCountForRegionExport([FromBody] CountListForRegionQueryModel model)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _customerRetentionService.GetAllCountForRegionPagingAsyncExport(model, root + "\\").GetAwaiter().GetResult();
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

        [Route("/api/CustomerRetentionCountForHod")]
        [HttpPost]
        public async Task<IActionResult> GetAllCountForHod([FromBody] CountListForHodQueryModel model)
        {
            var resultService = await _customerRetentionService.GetAllCountForHodPagingAsync(model);

            var response = Ok(resultService);
            return response;
        }

        [Route("/api/CustomerRetentionCountForHodExport")]
        [HttpPost]
        public async Task<IActionResult> CustomerRetentionCountForHodExport([FromBody] CountListForHodQueryModel model)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _customerRetentionService.GetAllCountForHodPagingAsyncExport(model, root + "\\").GetAwaiter().GetResult();
                var response = Ok(FileName);
                return response;
            });
            return result;
        }
    }
}
