﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AmaxIns.ServiceContract.User;

namespace AmaxIns.Web.API.Controllers.User
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleDirectorController : BaseController
    {
        protected readonly ISaleDirectorService _saleDirectorService;

        public SaleDirectorController(ISaleDirectorService saleDirectorService) : base(saleDirectorService)
        {
            _saleDirectorService = saleDirectorService;
        }

        [Route("/api/SaleDirector/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _saleDirectorService.GetById(id);
            var response = Ok();
            return response;
        }

        [Route("/api/SaleDirector")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _saleDirectorService.GetAll();
            var response = Ok(result);
            return response;
        }
    }
}
