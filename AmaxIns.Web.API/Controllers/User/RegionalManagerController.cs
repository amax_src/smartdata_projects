﻿using AmaxIns.ServiceContract.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.User
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class RegionalManagerController : BaseController
    {
        protected readonly IRegionalManagerService _regionalManagerService;

        public RegionalManagerController(IRegionalManagerService regionalManagerService) : base(regionalManagerService)
        {
            _regionalManagerService = regionalManagerService;
        }

        [Route("/api/RegionalManager/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var result= await _regionalManagerService.GetById(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/RegionalManager")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _regionalManagerService.GetAll();
            var response = Ok(result);
            return response;
        }

        [Route("/api/RegionalManager/BySaleDirector/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetBySaleDirector(int id)
        {
            var result = await _regionalManagerService.GetBySaleDirectorId(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/RegionalManager/ByMultipleSaleDirector/")]
        [HttpPost]
        public async Task<IActionResult> ByMultipleSaleDirector([FromBody] List<int> id)
        {
            var result = await _regionalManagerService.GetBySaleDirectorId(id);
            var response = Ok(result);
            return response;
        }

    }
}