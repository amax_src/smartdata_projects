﻿using AmaxIns.ServiceContract.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.User
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class ZonalManagerController : BaseController
    {
        protected readonly IZonalManagerService _zonalManagerService;

        public ZonalManagerController(IZonalManagerService zonalManagerService) : base(zonalManagerService)
        {
            _zonalManagerService = zonalManagerService;
        }

        [Route("/api/ZonalManager/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var result= await _zonalManagerService.GetById(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/ZonalManager")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _zonalManagerService.GetAll();
            var response = Ok(result);
            return response;
        }

        [Route("/api/ZonalManager/ByRegionalManager/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetByRegionalManager(int id)
        {
            var result = await _zonalManagerService.GetByRegionalManagerId(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/ZonalManager/ByMultipleRegionalManager/")]
        [HttpPost]
        public async Task<IActionResult> ByMultipleRegionalManager([FromBody] List<int> id)
        {
            var result = await _zonalManagerService.GetByRegionalManagerId(id);
            var response = Ok(result);
            return response;
        }
    }
}