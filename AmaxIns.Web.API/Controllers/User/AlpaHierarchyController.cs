﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.User;
using AmaxIns.ServiceContract.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.User
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class AlpaHierarchyController : BaseController
    {
        protected readonly IAlpaHierarchyService _alpaHierarchyService;
        public AlpaHierarchyController(IAlpaHierarchyService alpaHierarchyService) : base(alpaHierarchyService)
        {
            _alpaHierarchyService = alpaHierarchyService;
        }


        #region RegionalManager
        [Route("/api/AlpaHierarchy/RegionalManagerALPA")]
        [HttpGet]
        public async Task<IActionResult> RegionalManagerALPA()
        {
            var result = await _alpaHierarchyService.RegionalManagerALPA();
            var response = Ok(result);
            return response;
        }
        #endregion

        #region SaleDirector
        [Route("/api/AlpaHierarchy/AlpaSaleDirector")]
        [HttpGet]
        public async Task<IActionResult> AlpaSaleDirector()
        {
            var result = await _alpaHierarchyService.GetAllAlpaSaleDirector();
            var response = Ok(result);
            return response;
        }
        #endregion

        #region ZonalManager
        [Route("/api/AlpaHierarchy/ZonalManagerALPA")]
        [HttpGet]
        public async Task<IActionResult> ZonalManagerALPA()
        {
            var result = await _alpaHierarchyService.ZonalManagerALPA();
            var response = Ok(result);
            return response;
        }
        #endregion

        [Route("/api/AlpaHierarchy/GetAllAgency")]
        [HttpGet]
        public async Task<IActionResult> GetAllAgency()
        {
            var result = await _alpaHierarchyService.GetAllAgency();
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);

            return response;
        }

        [Route("/api/AlpaHierarchy/ByMultipleSaleDirector/")]
        [HttpPost]
        public async Task<IActionResult> ByMultipleSaleDirector([FromBody] List<int> id)
        {
            var result = await _alpaHierarchyService.GetBySaleDirectorId(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/AlpaHierarchy/ByMultipleRegionalManager/")]
        [HttpPost]
        public async Task<IActionResult> ByMultipleRegionalManager([FromBody] List<int> id)
        {
            var result = await _alpaHierarchyService.GetByRegionalManagerId(id);
            var response = Ok(result);
            return response;
        }

        [Route("/api/AlpaHierarchy/GetByRegionalAndZonalManager/")]
        [HttpPost]
        public async Task<IActionResult> GetByRegionalAndZonalManager([FromBody] ZonalRegionalManagerModel ids)
        {
            var result = await _alpaHierarchyService.GetAllLocationsRegionalZonalManager(ids.Rmid, ids.Zmid);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }

        [Route("/api/AlpaHierarchy/GetByRegionalManager/")]
        [HttpPost]
        public async Task<IActionResult> GetByRegionalManager([FromBody] List<int> id)
        {
            var result = await _alpaHierarchyService.GetAllLocationsRegionalZonalManager(id);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }

        [Route("/api/AlpaHierarchy/GetStoreManagerAgency")]
        [HttpPost]
        public async Task<IActionResult> GetStoreManagerAgency([FromBody] int id)
        {
            var result = await _alpaHierarchyService.GetStoreManagerAgency(id);
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response = Ok(activeresult);
            return response;
        }
    }

    
}