﻿using AmaxIns.ServiceContract.Agents;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.Agent
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgentController : BaseController
    {
        protected readonly IAgentService _Service;

        public AgentController(IAgentService Service) : base(Service)
        {
            _Service = Service;
        }

        [Route("/api/Agent")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result =  await _Service.GetAgents();
            var activeresult = result;//.Where(m => m.IsActive == true);
            var response =  Ok(activeresult);
            return response;
        }
    }
}