﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.AMAX30DB;
using AmaxIns.ServiceContract.AMAX30DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.AMAX30DB
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class Amax30APIController : BaseController
    {
        protected readonly IAgencyInfoService _agencyInfoService;
        public Amax30APIController(IAgencyInfoService agencyInfoService) : base(agencyInfoService)
        {
            _agencyInfoService = agencyInfoService;
           
        }


        [Route("/api/Amax30API/GetAgencyInfo/")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyInfo(CountListAgencyQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetAgencyInfo(queryModel);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Amax30API/GetAgentInfo/")]
        [HttpPost]
        public async Task<IActionResult> GetAgentInfo(CountListAgentQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetAgentInfo(queryModel);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Amax30API/GetClientInfo/")]
        [HttpPost]
        public async Task<IActionResult> GetClientInfo(CountListClientInfoQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetClientInfo(queryModel);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Amax30API/GetClientPaymentInfo/")]
        [HttpPost]
        public async Task<IActionResult> GetClientPaymentInfo(CountListPaymentInfoQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetClientPaymentInfo(queryModel);
            var response = Ok(result);
            return response;
        }

        //-------------------------------Demo-----------------------------------
        [Route("/api/Amax30API/GetAgencyInfo_V1/")]
        [HttpPost]
        public async Task<IActionResult> GetAgencyInfo_V1(CountListAgencyQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetAgencyInfo_V1(queryModel);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Amax30API/GetAgentInfo_V1/")]
        [HttpPost]
        public async Task<IActionResult> GetAgentInfo_V1(CountListAgentQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetAgentInfo_V1(queryModel);
            var response = Ok(result);
            return response;
        }

        [Route("/api/Amax30API/GetClientInfo_V1/")]
        [HttpPost]
        public async Task<IActionResult> GetClientInfo_V1(CountListClientInfoQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetClientInfo_V1(queryModel);
            var response = Ok(result);
            return response;
        }
        [Route("/api/Amax30API/GetClientPaymentInfo_V1/")]
        [HttpPost]
        public async Task<IActionResult> GetClientPaymentInfo_V1(CountListPaymentInfoQueryModel queryModel)
        {
            var result = await _agencyInfoService.GetClientPaymentInfo_V1(queryModel);
            var response = Ok(result);
            return response;
        }
    }
}
