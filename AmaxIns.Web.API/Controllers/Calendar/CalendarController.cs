﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AmaxIns.DataContract.Calendar;
using AmaxIns.ServiceContract.Calendar;
using Microsoft.AspNetCore.Authorization;

namespace AmaxIns.Web.API.Controllers.Calendar
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CalendarController : BaseController
    {
        protected readonly ICalendarService _calService;
        
        public CalendarController(ICalendarService calService) :base(calService)
        {
            _calService = calService;
        }
        

        //[HttpPost]
        //[Route("/api/Calendar/GetEvents")]
        //public async Task<IActionResult> GetEvents([FromBody]List<int> agencyId)
        //{
        //    var result = await _calService.GetEvents(agencyId);
        //    return Ok(result);
        //}
        [HttpPost]
        [Route("/api/Calendar/GetEvents")]
        public async Task<IActionResult> GetEvents([FromBody]List<int> agencyId, [FromQuery]string month)
        {
            var result=_calService.GetEvents(agencyId, month);
            return Ok(result);
        }

           [HttpPost]
           [Route("/api/Calendar/AddEvents")]
        public async Task<IActionResult> AddEvents ([FromBody] CalendarEvents addEvent)
        {
            var result = _calService.AddEvents(addEvent);
            return Ok(result);
        }
    }
}
