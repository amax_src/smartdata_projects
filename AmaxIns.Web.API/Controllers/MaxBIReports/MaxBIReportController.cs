﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.Common;
using AmaxIns.DataContract.MaxBIReports;
using AmaxIns.ServiceContract.MaxBIReports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace AmaxIns.Web.API.Controllers.MaxBIReports
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class MaxBIReportController : BaseController
    {
        protected readonly IMaxBIReportService _reportService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public MaxBIReportController(IMaxBIReportService reportService, IHostingEnvironment hostingEnvironment) : base(reportService)
        {
            _reportService = reportService;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Route("/api/MaxBIReport/GetCatalogs")]
        [HttpPost]
        public async Task<IActionResult> GetCatalogs([FromBody] Request request)
        {
            var result = await _reportService.GetCatalogs(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetHierarchyLevel")]
        [HttpPost]
        public async Task<IActionResult> GetHierarchyLevel([FromBody] HierarchyRequest request)
        {
            var result = await _reportService.GetHierarchyLevel(request);
            var response = Ok(result);
            return response;
        }

        #region Carrier Agents / AFee Per Policy / Premium Per Policy

        [Route("/api/MaxBIReport/GetCarrierAgents")]
        [HttpPost]
        public async Task<IActionResult> GetCarrierAgents([FromBody] Request request)
        {
            request.ReportType = Enumeraion.MaxBIReport.CarrierAgent;
            var result = await _reportService.GetAgentCarrierAFeePremium(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetAFeePremiumPerPolicy")]
        [HttpPost]
        public async Task<IActionResult> GetAFeePremiumPerPolicy([FromBody] Request request)
        {
            var result = await _reportService.GetAFeePremiumPerPolicy(request);
            var response = Ok(result);
            return response;
        }
        [Route("/api/MaxBIReport/GetAFeePremiumPerPolicySummary")]
        [HttpPost]
        public async Task<IActionResult> GetAFeePremiumPerPolicySummary([FromBody] Request request)
        {
            var result = await _reportService.GetAgencyPerPolicyReportSummary(request);
            var response = Ok(result);
            return response;
        }
        #endregion

        #region New Modified Quotes

        [Route("/api/MaxBIReport/GetNewModifiedQuotes")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotes([FromBody] Request request)
        {
            var result = await _reportService.GetNewModifiedQuotes(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetNewModifiedQuotesSummary")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotesSummary([FromBody] Request request)
        {
            var result = await _reportService.GetNewModifiedQuotesSummary(request);
            var response = Ok(result);
            return response;
        }

        #endregion

        #region New Modified Quotes Closing Ratios

        [Route("/api/MaxBIReport/GetNewModifiedQuotesClosingRatios")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotesClosingRatios([FromBody] Request request)
        {
            request.IsClosingRatio = true;
            var result = await _reportService.GetNewModifiedQuotes(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetNewModifiedQuotesClosingRatiosSummary")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotesClosingRatiosSummary([FromBody] Request request)
        {
            var result = await _reportService.GetNewModifiedQuotesSummary(request);
            var response = Ok(result);
            return response;
        }

        #endregion

        #region Inbound / Outbound Calls

        [Route("/api/MaxBIReport/GetInboundOutboundCalls")]
        [HttpPost]
        public async Task<IActionResult> GetInboundOutboundCalls([FromBody] Request request)
        {
            var result = await _reportService.GetInboundOutboundCalls(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetInboundOutboundCallsSummary")]
        [HttpPost]
        public async Task<IActionResult> GetInboundOutboundCallsSummary([FromBody] Request request)
        {
            var result = await _reportService.GetInboundOutboundCallsSummary(request);
            var response = Ok(result);
            return response;
        }

        #endregion

        #region Payroll Dashboad
        [Route("/api/MaxBIReport/GetPayrollDashboardFilterData")]
        [HttpPost]
        public async Task<IActionResult> GetPayrollDashboardFilterData([FromBody] PayrollRequest request)
        {
            var result = await _reportService.GetPayrollDashboardFilterData(request);
            var response = Ok(result);
            return response;
        }
        [Route("/api/MaxBIReport/GetPayrollDashboardData")]
        [HttpPost]
        public async Task<IActionResult> GetPayrollDashboardData([FromBody] PayrollRequest request)
        {
            var result = await _reportService.GetPayrollDashboardData(request);
            var response = Ok(result);
            return response;
        }
        #endregion

        [Route("/api/MaxBIReport/GetEmployeeSensitiveInfo")]
        [HttpPost]
        public async Task<IActionResult> GetEmployeeSensitiveInfo([FromBody] Request request)
        {
            var result = await _reportService.GetmployeeSensitiveInfo(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetAgentHourlyProduction")]
        [HttpPost]
        public async Task<IActionResult> GetAgentHourlyProduction([FromBody] Request request)
        {
            var result = await _reportService.GetAgentHourlyProduction(request);
            var response = Ok(result);
            return response;
        }

        [Route("/api/MaxBIReport/GetNewModifiedQuotesClosingRatiosForEPR/")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotesClosingRatiosForEPR([FromBody] Request request)
        {
            request.IsClosingRatio = true;
            var result = await _reportService.GetNewModifiedQuotesForEPR(request);
            var response = Ok(result);
            return response;
        }
        [Route("/api/MaxBIReport/GetNewModifiedQuotesClosingRatiosForEPRGraph/")]
        [HttpPost]
        public async Task<IActionResult> GetNewModifiedQuotesClosingRatiosForEPRGraph([FromBody] Request request)
        {
            request.IsClosingRatio = true;
            var result = await _reportService.GetNewModifiedQuotesForEPRGraph(request);
            var response = Ok(result);
            return response;
        }

    }
}