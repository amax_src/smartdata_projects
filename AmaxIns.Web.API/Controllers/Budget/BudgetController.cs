﻿using AmaxIns.DataContract.Budget;
using AmaxIns.ServiceContract.Budget;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Web.API.Controllers.Budget
{
    [Route("api/[controller]")]
    [ApiController]
    public class BudgetController : BaseController
    {
        IBudgetService _budgetService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public BudgetController(IBudgetService budgetService, IHostingEnvironment hostingEnvironment) : base(budgetService)
        {
            _budgetService = budgetService;
            _hostingEnvironment = hostingEnvironment;
        }


        [Route("/api/Budget/Categories")]
        [HttpPost]
        public async Task<IActionResult> Categories(BudgetSearch budgetSearch)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetCategories(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/Department/{userId}")]
        [HttpGet]
        public async Task<IActionResult> Department(int userId)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetDept(userId);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetName")]
        [HttpPost]
        public async Task<IActionResult> BudgetName(BudgetSearch budgetSearch)
        {

            IActionResult response = null;
            var result = await _budgetService.GetBudgetBudgetName(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetByMonth")]
        [HttpPost]
        public async Task<IActionResult> BudgetByMonth(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetByMonth(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetAnnual")]
        [HttpPost]
        public async Task<IActionResult> BudgetAnnual(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetAnnual(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetTrx")]
        [HttpPost]
        public async Task<IActionResult> BudgetTrx(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetTrx(model);
            response = Ok(result);
            return response;
        }


        [Route("/api/Budget/BudgetSpend")]
        [HttpPost]
        public async Task<IActionResult> BudgetSpend(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetSpending(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetTrxCsv")]
        [HttpPost]
        public IActionResult BudgetTrxCsv(BudgetFormModel model)
        {
            StringBuilder sb = new StringBuilder();
            var result = _budgetService.GetBudgetTrx(model).Result;
            sb.AppendLine(@"Date,Category Name,Budget Code,Description,VendorName,Location,Source,User,User Posted,Actual");
            foreach (var x in result)
            {
                sb.AppendLine(x.ToString());
            }
            byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new MemoryStream(bytes));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "Report-Trans.csv"
            };

            return Ok(response);
            //return File(bytes, "application/octet-stream", "Reports.csv");
        }

        [Route("/api/Budget/User")]
        [HttpPost]
        public new async Task<IActionResult> User(BudgetSearch budgetSearch)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetUser(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/Location")]
        [HttpPost]
        public async Task<IActionResult> Location(BudgetSearch budgetSearch)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetLocation(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/Source")]
        [HttpPost]
        public async Task<IActionResult> Source(BudgetSearch budgetSearch)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetSource(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/BudgetCode")]
        [HttpPost]
        public async Task<IActionResult> BudgetCode(BudgetSearch budgetSearch)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetCode(budgetSearch);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/GetBudgetTranc")]
        [HttpPost]
        public async Task<IActionResult> GetBudgetTranc(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetTranc(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/GetBudgetSummary")]
        [HttpPost]
        public async Task<IActionResult> GetBudgetSummary(BudgetFormModel model)
        {
            IActionResult response = null;
            var result = await _budgetService.GetBudgetSummary(model);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/UploadExcel")]
        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public async Task<IActionResult> UploadExcel()
        {
            IActionResult response = null;
            var formCollection = await Request.ReadFormAsync();

            var result = _budgetService.UploadExcelFile(formCollection);
            response = Ok(result);
            return response;
        }

        [Route("/api/Budget/ExportAccountDetals")]
        [HttpPost]
        public async Task<IActionResult> ExportAccountDetals([FromBody] IEnumerable<BudgetOverspendModel> list)
        {
            var result = await Task.Run(() =>
            {
                string root = _hostingEnvironment.WebRootPath + "\\Export";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                string FileName = _budgetService.ExportAccountDetals(list, root + "\\");
                var response = Ok(FileName);
                return response;
            });
            return result;
        }

    }
}
