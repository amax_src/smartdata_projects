﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AmaxIns.DataContract.Authentication;
using AmaxIns.ServiceContract.Budget;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AmaxIns.Web.API.Controllers.Budget
{
    [Route("api/[controller]")]
    [ApiController]
    public class BudgetAuthenticationController : BaseController
    {
        protected readonly IBudgetAuthenticationService _authenticationService;

        public BudgetAuthenticationController(IBudgetAuthenticationService authenticationService) : base(authenticationService)
        {
            _authenticationService = authenticationService;
        }


        [HttpPost]
        [Route("/api/BudgetAuthentication/Validate")]
        public async Task<IActionResult> Validate(LoginModel loginModel)
        {
            var result = await _authenticationService.LogInAsync(loginModel);

            if (result == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            //result.UserName = "Indu Bhushan";
            var response = Ok(result);

            return response;
        }
    }
}