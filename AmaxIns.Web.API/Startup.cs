﻿using AmaxIns.Common;
using AmaxIns.Common.Interface;
using AmaxIns.Repository.Agency;
using AmaxIns.Repository.Agents;
using AmaxIns.Repository.AIS;
using AmaxIns.Repository.ALPA;
using AmaxIns.Repository.AlphaCustomerInfo;
using AmaxIns.Repository.AlphaTop5Agent;
using AmaxIns.Repository.AMAX30DB;
using AmaxIns.Repository.APILoad;
using AmaxIns.Repository.Authentication;
using AmaxIns.Repository.Budget;
using AmaxIns.Repository.Calendar;
using AmaxIns.Repository.CarrierMix;
using AmaxIns.Repository.CustomerRetention;
using AmaxIns.Repository.Dashboard;
using AmaxIns.Repository.EPR;
using AmaxIns.Repository.HourlyLoad;
using AmaxIns.Repository.LiveDataApp;
using AmaxIns.Repository.MaxBIReports;
using AmaxIns.Repository.Notification;
using AmaxIns.Repository.Payroll;
using AmaxIns.Repository.PBX;
using AmaxIns.Repository.Planner;
using AmaxIns.Repository.Quotes;
using AmaxIns.Repository.Referral;
using AmaxIns.Repository.Reports;
using AmaxIns.Repository.Revenue;
using AmaxIns.Repository.Spectrum;
using AmaxIns.Repository.User;
using AmaxIns.Repository.WorkingDays;
using AmaxIns.RepositoryContract.Agency;
using AmaxIns.RepositoryContract.Agents;
using AmaxIns.RepositoryContract.AIS;
using AmaxIns.RepositoryContract.ALPA;
using AmaxIns.RepositoryContract.AlphaCustomerInfo;
using AmaxIns.RepositoryContract.AlphaTop5Agent;
using AmaxIns.RepositoryContract.AMAX30DB;
using AmaxIns.RepositoryContract.APILoad;
using AmaxIns.RepositoryContract.Authentication;
using AmaxIns.RepositoryContract.Budget;
using AmaxIns.RepositoryContract.Calendar;
using AmaxIns.RepositoryContract.CarrierMix;
using AmaxIns.RepositoryContract.CustomerRetention;
using AmaxIns.RepositoryContract.Dashboard;
using AmaxIns.RepositoryContract.EPR;
using AmaxIns.RepositoryContract.HourlyLoad;
using AmaxIns.RepositoryContract.LiveDataApp;
using AmaxIns.RepositoryContract.MaxBIReports;
using AmaxIns.RepositoryContract.Notification;
using AmaxIns.RepositoryContract.Payroll;
using AmaxIns.RepositoryContract.PBX;
using AmaxIns.RepositoryContract.Planner;
using AmaxIns.RepositoryContract.Quotes;
using AmaxIns.RepositoryContract.Referral;
using AmaxIns.RepositoryContract.Reports;
using AmaxIns.RepositoryContract.Revenue;
using AmaxIns.RepositoryContract.Spectrum;
using AmaxIns.RepositoryContract.User;
using AmaxIns.RepositoryContract.WorkingDays;
using AmaxIns.Service;
using AmaxIns.Service.Agency;
using AmaxIns.Service.Agents;
using AmaxIns.Service.AIS;
using AmaxIns.Service.ALPA;
using AmaxIns.Service.AlphaCustomerInfo;
using AmaxIns.Service.AMAX30DB;
using AmaxIns.Service.APILoad;
using AmaxIns.Service.Authentication;
using AmaxIns.Service.Budget;
using AmaxIns.Service.Calendar;
using AmaxIns.Service.CarrierMix;
using AmaxIns.Service.Dates;
using AmaxIns.Service.EPR;
using AmaxIns.Service.HourlyLoad;
using AmaxIns.Service.LiveDataApp;
using AmaxIns.Service.MaxBIReports;
using AmaxIns.Service.Notification;
using AmaxIns.Service.Payroll;
using AmaxIns.Service.PBX;
using AmaxIns.Service.Planner;
using AmaxIns.Service.Quotes;
using AmaxIns.Service.Refferal;
using AmaxIns.Service.Report;
using AmaxIns.Service.Revenue;
using AmaxIns.Service.Service;
using AmaxIns.Service.Spectrum;
using AmaxIns.Service.User;
using AmaxIns.Service.WorkingDays;
using AmaxIns.ServiceContract;
using AmaxIns.ServiceContract.Agency;
using AmaxIns.ServiceContract.Agents;
using AmaxIns.ServiceContract.AIS;
using AmaxIns.ServiceContract.ALPA;
using AmaxIns.ServiceContract.AlphaCustomerInfo;
using AmaxIns.ServiceContract.AlphaTop5Agent;
using AmaxIns.ServiceContract.AMAX30DB;
using AmaxIns.ServiceContract.APILoad;
using AmaxIns.ServiceContract.Authentication;
using AmaxIns.ServiceContract.Budget;
using AmaxIns.ServiceContract.Calendar;
using AmaxIns.ServiceContract.CarrierMix;
using AmaxIns.ServiceContract.Dashboard;
using AmaxIns.ServiceContract.Dates;
using AmaxIns.ServiceContract.EPR;
using AmaxIns.ServiceContract.HourlyLoad;
using AmaxIns.ServiceContract.LiveDataApp;
using AmaxIns.ServiceContract.MaxBIReports;
using AmaxIns.ServiceContract.Notification;
using AmaxIns.ServiceContract.Payroll;
using AmaxIns.ServiceContract.PBX;
using AmaxIns.ServiceContract.Planner;
using AmaxIns.ServiceContract.Quotes;
using AmaxIns.ServiceContract.Referral;
using AmaxIns.ServiceContract.Report;
using AmaxIns.ServiceContract.Revenue;
using AmaxIns.ServiceContract.Spectrum;
using AmaxIns.ServiceContract.User;
using AmaxIns.ServiceContract.WorkingDays;
using AmaxIns.Web.API.GlobalErrorHandling;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;

namespace AmaxIns.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public IConfiguration Configuration { get; }
        public ILogger<Startup> Logger;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddMvc()
                .AddJsonOptions(a => a.SerializerSettings.NullValueHandling = NullValueHandling.Ignore)
                .AddJsonOptions(a => a.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc);
            var apikey = Configuration.GetSection("Keys:Secret").GetSection("Value").Value;
            var key = Encoding.ASCII.GetBytes(apikey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(x =>
           {
               x.RequireHttpsMetadata = false;
               x.SaveToken = true;
               x.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(key),
                   ValidateIssuer = false,
                   ValidateAudience = false
               };
               x.Events = new JwtBearerEvents
               {
                   OnAuthenticationFailed = context =>
                   {
                       if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                       {
                           context.Response.Headers.Add("Token-Expired", "true");
                       }
                       else
                       {
                           return Task.FromException(new Exception(context.Exception.Message));
                       }
                       return Task.CompletedTask;

                   },
                   OnTokenValidated = context =>
                   {
                       return Task.CompletedTask;
                   }



               };
           });

            CorsPolicy.ConfigureServices(services);
            ConfigureDependencyInjection(services);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            //Register the Swagger services      
            services.AddOpenApiDocument(c =>
            {
                c.Title = "My API";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.ConfigureExceptionHandler(Logger);
            app.UseAuthentication();
            app.UseCors(CorsPolicy.ALLOW_ALL);
            app.UseCors("CorsPolicy");
            app.UseMvc();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseHttpsRedirection();

            //Register the Swagger generator      
            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            // Add application services.
            services.AddTransient<IPayrollService, PayrollService>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IRegionalManagerService, RegionalManagerService>();
            services.AddTransient<IZonalManagerService, ZonalManagerService>();
            services.AddTransient<IAgencyService, AgencyService>();
            services.AddTransient<IYearService, YearService>();
            services.AddTransient<IMonthService, MonthService>();
            services.AddTransient<IRevenueService, RevenueService>();
            services.AddTransient<IWorkingDaysService, WorkingDaysService>();
            services.AddTransient<IEPRService, EPRService>();
            services.AddTransient<IAISService, AISService>();
            services.AddTransient<ISpectrumService, SpectrumService>();
            services.AddTransient<IPbxService, PbxService>();
            services.AddTransient<ICustomerInfoService, CustomerInfoService>();
            services.AddTransient<ITopAgentService, TopAgentService>();
            services.AddTransient<ICarrierMixService, CarrierMixService>();
            services.AddTransient<IQuotesService, QuotesService>();
            services.AddTransient<ILiveDataAppService, LiveDataAppService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IApiLoadService, ApiLoadService>();
            services.AddTransient<IHourlyLoadService, HourlyLoadService>();
            services.AddTransient<IAgentService, AgentService>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IDidService, DidService>();
            services.AddTransient<IDashboardService, DashboardService>();
            services.AddTransient<ISpectrumLiveDataService, SpectrumLiveDataService>();
            services.AddTransient<ICalendarService, CalendarService>();
            services.AddTransient<IBudgetService, BudgetService>();
            services.AddTransient<ITurboratorService, TurboratorService>();
            services.AddTransient<IBolQuoteService, BolQuoteService>();
            services.AddTransient<IBudgetAuthenticationService, BudgetAuthenticationService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IUniqueCreatedQuotesService, UniqueCreatedQuotesService>();
            services.AddTransient<ICarrierMixNewService, CarrierMixNewService>();
            services.AddTransient<IRentersReportService, RentersReportService>();
            services.AddTransient<IAgencyInfoService, AgencyInfoService>();
            services.AddTransient<ICustomerRetentionService, CustomerRetentionService>();
            services.AddTransient<IPlannerService, PlannerService>();
            services.AddTransient<ISaleDirectorService, SaleDirectorService>();
            services.AddTransient<IMaxBIReportService, MaxBIReportService>();

            services.AddTransient<IAlpaHierarchyService, AlpaHierarchyService>();
            services.AddTransient<IAlpaRevenueService, AlpaRevenueService>();
            services.AddTransient<IAlpaEprService, AlpaEprService>();
            services.AddTransient<IAISAlpaService, AISAlpaService>();
            services.AddTransient<IWinbackService, WinbackService>();

            services.AddTransient<IAlpaBolQuotesService, AlpaBolQuotesService>();

            // Add repos as scoped dependency so they are shared per request.
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IPayrollRepository, PayrollRepository>();
            services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
            services.AddScoped<IRegionalManagerRepository, RegionalManagerRepository>();
            services.AddScoped<IZonalManagerRepository, ZonalManagerRepository>();
            services.AddScoped<IAgencyRepository, AgencyRepository>();
            services.AddScoped<IRevenueRepository, RevenueRepository>();
            services.AddScoped<IWorkingDaysRepository, WorkingDaysRepository>();
            services.AddScoped<IEPRRepository, EPRRepository>();
            services.AddScoped<IAISRepository, AISRepository>();
            services.AddScoped<ISpectrumRepository, SpectrumRepository>();
            services.AddScoped<IPbxRepository, PbxRepository>();
            services.AddScoped<ICustomerInfoRepository, CustomerInfoRepository>();
            services.AddScoped<ITopAgentRepository, TopAgentRepository>();
            services.AddScoped<ICarrierMixRepository, CarrierMixRepository>();
            services.AddScoped<IQuotesRepository, QuotesRepository>();
            services.AddScoped<ILivedataRepository, LivedataRepository>();
            services.AddScoped<IAPILoadRepository, APILoadRepository>();
            services.AddScoped<IHourlyLoadRepository, HourlyLoadRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IDidRepository, DidRepository>();
            services.AddScoped<IDashboardRepository, DashboardRepository>();
            services.AddScoped<ICalendarRepository, CalendarRepository>();
            services.AddScoped<ISpectrumLiveDataRepository, SpectrumLiveDataRepository>();
            services.AddScoped<ITurboratorRepository, TurboratorRepository>();
            services.AddScoped<IBolQuoteRepository, BolQuoteRepository>();
            services.AddScoped<IBudgetRepository, BudgetRepository>();
            services.AddScoped<IBudgetAuthenticationRepository, BudgetAuthenticationRepository>();
            services.AddScoped<IReportRepository, ReportRepository>();
            services.AddScoped<IUniqueCreatedQuotesRepository, UniqueCreatedQuotesRepository>();
            services.AddScoped<ICarrierMixNewRepository, CarrierMixNewRepository>();
            services.AddScoped<IRentersReportRepository, RentersReportRepository>();
            services.AddScoped<IAgencyInfoRepository, AgencyInfoRepository>();
            services.AddTransient<ICustomerRetentionRepository, CustomerRetentionRepository>();
            services.AddTransient<IPlannerRepository, PlannerRepository>();
            services.AddTransient<ISaleDirectorRepository, SaleDirectorRepository>();
            services.AddScoped<IMaxBIReportRepository, MaxBIReportRepository>();
            services.AddTransient<IAlpaHierarchyRepository, AlpaHierarchyRepository>();
            services.AddTransient<IAlpaRevenueRepository, AlpaRevenueRepository>();
            services.AddTransient<IAlpaEprRepository, AlpaEprRepository>();
            services.AddTransient<IAISAlpaRepository, AISAlpaRepository>();
            services.AddTransient<IWinbackRepository, WinbackRepository>();

            services.AddTransient<IAlpaBolQuotesRepository, AlpaBolQuotesRepository>();

            services.AddSingleton<ILogger>(Logger);
            services.AddSingleton<IConfiguration>(Configuration);
        }

    }
}
